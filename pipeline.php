<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

if ($result = $db_connect->query("SHOW COLUMNS FROM forms WHERE Field = 'status';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$status_list = $out[1];
	$result->close();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "0: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

//print_r($status_list);

echo '<div>Фильтры: с <input id="dfrom" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d", mktime(0, 0, 0, date('m')-6, date('d'), date('Y'))) . '"> по <input id="dto" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d") . '">';

echo ' <button onclick="javascript:filterApply();">Применить фильтр</button>';
echo '</div>';

echo '<div id="tab_container">';
	echo '<div id="list_table"><div class="row_head">';
	foreach($status_list as $v)
		echo '<div class="list_cell">' . $status_description[$v] . '</div>';
	echo '</div></div>';
echo '</div>';
//echo '<div id="list_table"></div>';
?>

