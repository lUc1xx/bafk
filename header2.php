<?php
if (!defined('SYSTEM_START_9876543210')) exit;  
$date_ = ", неделя:" . date('W') . ', день:' . date('d') . ', день недели:' . date('w');
$today_week = date('W');
//$today_day = date('j');
$today_day = date('d');
//$today_mon = date('n');
$today_mon = date('m');
$today_year = date('Y');
$today_wday = date('w');
$today = date("d.m.Y <b>H:i:s</b>");

?>
	<input type="checkbox" id="nav-toggle" hidden>
    <nav class="nav">
        <label for="nav-toggle" class="nav-toggle" onclick></label>
        <h2 class="logo"> 
            <a href="/"><?=$system_name;?></a> 
        </h2>
        <ul>
	<?
			echo '<li><a href="/">Рабочий стол</a>';
			echo '<li><a href="/create/">Создать анкету</a>';
			echo '<li><a href="/list/">Список заявок</a>';
			if (check_access("list_fa"))
				echo '<li><a href="/list_fa/">ЗА/План работ</a>';
			if (check_access("mstat"))
				echo '<li><a href="/mstat/">Статистика</a>';
			if (check_access("listmetrika"))
				echo '<li><a href="/listmetrika/">Метрика</a>';
			if (check_access("listsotr"))
				echo '<li><a href="/listsotr/">Сотрудничество</a>';
			if (check_access("staff"))
				echo '<li><a href="/staff/">Сотрудники</a>';
			if (check_access("agents"))
				echo '<li><a href="/agents/">Агенты</a>';
			if (check_access("asettings"))
				echo '<li><a href="/asettings/">Настр. анкеты</a>';
			if (check_access("services"))
				echo '<li><a href="/services/">Сервисы</a>';
			echo '<li><a href="/checks/">Проверки</a>';
			if (check_access("bankrequests"))
				echo '<li><a href="/bankrequests/">Заявки в банки</a>';
			if (check_access("banks"))
				echo '<li><a href="/banks/">Банки</a>';
			if (check_access("filters"))
				echo '<li><a href="/filters/">Фильтры</a>';
			if (check_access("settings"))
				echo '<li><a href="/settings/">Настройки</a>';
?>
        </ul>
    </nav>

<div id="header">
<div class="greeting">
<?
	$busy = ($staff_status == 'busy') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
	echo "<span onclick=\"javascript:ch_my_status(this);\" style='cursor:pointer;'>$busy</span>";
	echo " $staff_lastname $staff_firstname "; 
	echo "<span id=\"time_t\">$today</span>";
?>
</div>
<div class="top_menu"> <a href="/create/"><img src="/img/create.png" height="30px" title="Создать анкету" alt="Создать анкету"></a> <a href="/list/"><img src="/img/list_big.png" height="30px" title="Список анкет" alt="Список анкет"></a></div>
<div id="in_work_h"></div>
<?

	if ($_page == 'details')
		echo '<button onclick="javascript:back2list();" style="position:absolute; left:30px; top:30px;">< Назад</button>';

//<div id="in_work_h">Загрузка данных...</div>
//<?
if ($_page == 'list')
{
	echo '<div class="search_div"><input type="text" id="search_input" oninput="javascript:calc_result(this.value);"> <button id="search_button" style="z-index:999;" onclick="javascript:gsearch()">Поиск</button></div>';
}

if ($staff_office_type == 'main')
{
	$bal_mass = array();
	
	$dfrom_b = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), 1, date('Y')));
	$sql = "SELECT type, COUNT(*) as cp FROM idx_log WHERE datereq >'$dfrom_b' GROUP BY type;";
	$result = $db_connect->query($sql);
	$bal_idx = 11800;
	$c_verifyPhone = 0;
	$c_finScoring = 0;
	if ($result->num_rows)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			if ($row['type'] == 'finScoring')
			{
				if ($c_finScoring < 1001)
					$bal_idx -= 23.6 * (0 + $row['cp']);
				else
					$bal_idx -= (23.6 * 1000 + 17.7 * (0 + $row['cp'] - 1000));
			}
			else if ($row['type'] == 'verifyPhone')
			{
				$c_verifyPhone++;
				if ($c_verifyPhone < 1001)
					$bal_idx -= 11.8 * (0 + $row['cp']);
				else
					$bal_idx -= (11.8 * 1000 + 10.62 * (0 + $row['cp'] - 1000));
			}
		}
	}
	$result->close();
	
	$sql = "SELECT pval, sname FROM services_info WHERE (sname='atompark' OR sname='promoney' OR sname='yandex') AND pname='balance';";
	$result = $db_connect->query($sql);
	if ($result->num_rows)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$bal_mass[$row['sname']] = $row['pval'];
		}
	}
	$result->close();
	echo '<div class="balance_list">Баланс смс: ' . $bal_mass['atompark'] . ' руб. Баланс Промани: ' . $bal_mass['promoney'] . ' руб. Баланс Yandex: ' . $bal_mass['yandex'] . ' руб. Баланс FICO: ' . $bal_idx . ' руб.</div>';
}

$stlogout = "";
if ($staff_id == 1)
	$stlogout = ' style="width:500px;"';
?>
<div class="logout"<? echo $stlogout; ?>>
<?
if ($staff_position_debug == 'super-admin')
{
	echo '<div style="width:150px; float: left">';
	echo '<select onchange="javascript:debug(this);">';
	foreach ($offices_mass as $k => $v)
	{
		echo '<optgroup label="' . $v['name'] . '">';
		foreach ($staff_mass as $key => $val)
		{
			if (($val['office'] != $k) || $val['deleted'] == 1)
				continue;
			
			$fio_a = $val['lastname'];
			if ($val['firstname'] != '')
				$fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
			if ($val['patronymic'] != '')
				$fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';
			
			$selected = ($val['id'] == $staff_id) ? " selected" : "";
			
			echo '<option value="' . $val['id'] . '"' . $selected . '>' . $fio_a . '</option>';
		}
		echo '</optgroup>';
	}
	echo "</select> ";
	echo "</div> ";
}
//if ($staff_id == 1)
if (true)
{
?>
	<div style="width:280px; float:left">
	<span id="task_header_span" class="green_header_task" onclick="javascript:show_tasks_list();">Нет задач</span><br>
	<span id="call_header_span" class="green_header_task" onclick="javascript:show_call_calendar();">- звонков сегодня</span><br>
	<span id="meet_header_span" class="green_header_task" onclick="javascript:show_meet_calendar();">- встреч сегодня</span>
	</div>
	<div style="width:60px; float:right"><button onclick="javascript:logout();">Выход</button></div>
</div>
</div>
<?
}
else
{
?>
<span id="task_header_span" class="green_header_task" onclick="javascript:show_tasks_list();">Нет задач</span>&nbsp;<button onclick="javascript:logout();">Выход</button></div>
</div>
<?
}
?>



<div onclick="show_cal('none')" id="wrap_cal"></div>
<div id="window_cal" class="window_cal">
<img class="close" onclick="show_cal('none')" src="/img/close.png">

<div id="calendar_call" style="display:none;"></div>
<div id="calendar_meet" style="display:none;"></div>
<div id="task_wrap" style="display:none;">
<?
if ($staff_office_type == 'main')
	echo '<div id="tasks_buttonst"><button onclick="javascript:show_task_form();">Добавить задачу</button></div>';
echo '<div id="task_form" style="display:none;">';
echo '<div>Анкета <input type="text" name="form_id"';
if ($_page == 'details')
{
	if (isset($_GET['id']))
	{
		$_id = 0 + $_GET['id'];
		echo " value='$_id'";
	}
}
echo '> или сотрудник <select id="task_sel_staff_id" name="staff_id">';
echo '<option value="-1">Выберите</option>';
foreach ($offices_mass as $k => $v)
{
	//if ($v['type'] != 'tm')
	//	continue;
	
	echo '<optgroup label="' . $v['name'] . '">';
	//echo '<option value="of_' . $v['id'] . '">Передать в офис</option>';
	foreach ($staff_mass as $key => $val)
	{
		if (($val['office'] != $k) || $val['deleted'] == 1)
			continue;
		
		$fio_a = $val['lastname'];
		if ($val['firstname'] != '')
			$fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
		if ($val['patronymic'] != '')
			$fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';
		
		echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
	}
	
	echo '</optgroup>';
}
echo '</select>';
echo '</div>';
echo '<div>Тема: <input type="text" name="task_head"></div>';
echo '<div>Задача: <textarea name="task_text"></textarea></div>';
echo '<button onclick="javascript:create_task();">Создать задачу</button>';
echo '</div>';

?>
<div id="tasks_list_wrap"><div id="tasks_list"></div></div>
</div>
</div>