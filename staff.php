<?php
if (!defined('SYSTEM_START_9876543210')) exit;  
//$res = array();
if ($result = $db_connect->query("SHOW COLUMNS FROM offices WHERE Field = 'type';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$offices_types = $out[1];
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if ($result = $db_connect->query("SHOW COLUMNS FROM staff WHERE Field = 'position';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$staff_types = $out[1];
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$list_offices = array();
$list_offices_types = array();

if ($result = $db_connect->query("SELECT * FROM offices WHERE deleted='0' ORDER BY id ASC;"))
{
	$row_odd = "row_odd";	//нечет
	$row_even = "row_even";	//чет
	$c = false;
	echo '<div id="offices_wrap">';
	echo '<div id="cont4offices">';
	echo "<div class=\"head_table\">";
	echo '<div class="list_cell">Id</div>';
	echo '<div class="list_cell">Тип</div>';
	echo '<div class="list_cell">Название</div>';
	echo '<div class="list_cell">Ред.</div>';
	echo '<div class="list_cell">Уд.</div>';
	echo "</div>";
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//array_push($res, $row);
		$list_offices[$row['id']] = $row['name'];
		$list_offices_types[$row['id']] = $row['type'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div class=\"$cl\">";
		echo '<div class="list_cell">' . $row['id'] . '</div>';
		//$ch = ($row['en']) ? "checked" : "";
		echo '<div class="list_cell">' . $row['type'] . '</div>';
		echo '<div class="list_cell">' . $row['name'] . '</div>';
		echo '<div class="list_cell"><a title="Редактировать" href="#" onclick="javasccript:edit_office(' . $row['id'] . ')"><img src="/img/edit.png" width="16" height="16"></a></div>';
		echo '<div class="list_cell"><a title="Удалить" href="#" onclick="javasccript:del_office(' . $row['id'] . ')"><img src="/img/delete.png" width="16" height="16"></a></div>';
		echo "</div>";
	}
	echo "</div>";
	echo '<div><span id="add_office_b" onclick="javascript:add_office();">Добавить офис</span></div>';
	echo "</div>";
	$result->close();
	
	echo '<div id="window_of" class="window">';
	echo '<img class="close" onclick="show_of(\'none\')" src="/img/close.png">';
	echo '<b>Добавление нового офиса</b>';
	
		echo '<div id="new_office_wrap">';
			echo '<div class="row_a"><div class="fname">Наименование:</div><div class="fval"><input class="nwinp0" type="text" name="name"></div></div>';
			echo '<div class="row_a"><div class="fname">Тип:</div><div class="fval"><select class="nwinp1" name="type">';
			echo '<option value="">Выберите</option>';
			foreach ($offices_types as $k => $v)
				echo '<option value="' . $v . '">' . $v . '</option>';
			echo '</select></div></div>';
			echo '<div class="row_a"><div class="fname">Описание:</div><div class="fval"><input class="nwinp0" type="text" name="description"></div></div>';
			
			echo '<div class="row_a">';
			echo '<div class="cval"><button id="noffice_add_b" onclick="javascript:office_add();">Добавить</button></div>';
			echo '<div class="cval"><button id="noffice_clear_b" onclick="javascript:office_clear();">Очистить</button></div>';
			echo '</div>';
		
		echo '</div>';
	
	echo '</div>';
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if ($result = $db_connect->query("SELECT * FROM staff WHERE deleted=0 ORDER BY id ASC;"))
{
	//$res = "Select вернул " . $result->num_rows . " строк.\n";
	
	$row_odd = "row_odd";	//нечет
	$row_even = "row_even";	//чет
	$c = false;
	echo '<div id="staff_wrap">';
	echo '<div id="cont4staff">';
	echo "<div class=\"head_table\">";
	echo '<div class="list_cell">Id</div>';
	echo '<div class="list_cell">Статус</div>';
	echo '<div class="list_cell">Ред.</div>';
	echo '<div class="list_cell">Уд.</div>';
	echo '<div class="list_cell">Логин</div>';
	echo '<div class="list_cell">Пароль</div>';
	echo '<div class="list_cell">Фамилия</div>';
	echo '<div class="list_cell">Имя</div>';
	echo '<div class="list_cell">Отчество</div>';
	echo '<div class="list_cell">Офис</div>';
	echo '<div class="list_cell">Должность</div>';
	echo '<div class="list_cell">Р. ТМ</div>';
	echo '<div class="list_cell">Р. ОЗС</div>';
	echo '<div class="list_cell">ЗА</div>';
	echo '<div class="list_cell">ПР</div>';
	echo '<div class="list_cell">Банки</div>';
	echo '<div class="list_cell">Почта</div>';
	echo '<div class="list_cell">Раб. почта</div>';
	echo '<div class="list_cell">Моб. телефон</div>';
	echo '<div class="list_cell">Раб. телефон</div>';
	echo '<div class="list_cell">Доб. телфин</div>';
	echo '<div class="list_cell">IP доступа</div>';
	echo "</div>";
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//array_push($res, $row);
		$of_id = ($row['office'] == 0) ? 1 : $row['office'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div id=\"staff_row_{$row['id']}\" class=\"$cl\">";
		echo '<div class="list_cell">' . $row['id'] . '</div>';
		if ($row['online'] == 0)
			$busy = '<img src="/img/ball_grey.png">';
		else
			$busy = ($row['status'] == 'busy') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
		echo '<div class="list_cell" style="text-align:center;">' . $busy . '</div>';
		echo '<div class="list_cell"><a title="Редактировать" href="#" onclick="javasccript:edit_staff(' . $row['id'] . ')"><img src="/img/edit.png" width="16" height="16"></a></div>';
		echo '<div class="list_cell"><a title="Удалить" href="#" onclick="javasccript:del_staff(' . $row['id'] . ')"><img src="/img/delete.png" width="16" height="16"></a></div>';
		echo '<div class="list_cell">' . $row['login'] . '</div>';
		echo '<div class="list_cell">' . 'Сбросить' . '</div>';
		echo '<div class="list_cell">' . $row['lastname'] . '</div>';
		echo '<div class="list_cell">' . $row['firstname'] . '</div>';
		echo '<div class="list_cell">' . $row['patronymic'] . '</div>';
		$of = (array_key_exists($of_id, $list_offices)) ? $list_offices[$of_id] : '<font color="red">Офис расформирован</font>';
		echo '<div class="list_cell">' . $of . '</div>';
		echo '<div class="list_cell">' . $work_position[$row['position']] . '</div>';
		$tm_sort = ($row['in_tm_sort'] != '1') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
		if ($list_offices_types[$of_id] != 'tm')
			echo '<div class="list_cell"></div>';
		else
			echo '<div class="list_cell" style="text-align:center;"><span style="cursor:pointer" onclick="javascript:ch_tm_sort(this, ' . $row['id'] . ');">' . $tm_sort . '</span></div>';
		if ($list_offices_types[$of_id] != 'ozs')
			echo '<div class="list_cell"></div>';
		else
			echo '<div class="list_cell" style="text-align:center;"><span style="cursor:pointer" onclick="javascript:ch_tm_sort(this, ' . $row['id'] . ');">' . $tm_sort . '</span></div>';
		
		
		$fin_an = ($row['fin_an'] != '1') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
		echo '<div class="list_cell" style="text-align:center;"><span style="cursor:pointer" onclick="javascript:ch_fin_an(this, ' . $row['id'] . ');">' . $fin_an . '</span></div>';
		
		$plan_check = ($row['plan_check'] != '1') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
		echo '<div class="list_cell" style="text-align:center;"><span style="cursor:pointer" onclick="javascript:ch_plan_check(this, ' . $row['id'] . ');">' . $plan_check . '</span></div>';
		
		$bc_check = ($row['staff_bc'] != '1') ? '<img src="/img/ball_red.png">' : '<img src="/img/ball_green.png">';
		echo '<div class="list_cell" style="text-align:center;"><span style="cursor:pointer" onclick="javascript:ch_bc_check(this, ' . $row['id'] . ');">' . $bc_check . '</span></div>';
		
		echo '<div class="list_cell">' . $row['email'] . '</div>';
		echo '<div class="list_cell">' . $row['email_corp'] . '</div>';
		echo '<div class="list_cell">' . $row['phone_mob'] . '</div>';
		echo '<div class="list_cell">' . $row['phone_work'] . '</div>';
		echo '<div class="list_cell"><input type="number" style="width:50px;" value="' . $row['phone_work_add'] . '" oninput="javascript:change_wadd(this, ' . $row['id'] . ');"></div>';
		//echo '<div class="list_cell">' . 'Настроить' . '</div>';
		$ipmass_len = (($row['ip_avail_val'] == '') || $row['ip_avail_val'] == null) ? 0 : sizeof(explode(';;;', $row['ip_avail_val']));
		
		if ($ipmass_len)
			$ipmass_len = "Настроить($ipmass_len)";
		else
			$ipmass_len = '<font color="green">Свободный доступ</font>';
		echo '<div class="list_cell">' . '<span style="cursor:pointer; font-style:italic;" onclick="javascript:ip_access_settings(' . $row['id'] . ');">' . $ipmass_len . '</span></div>';
		echo "</div>";
		
	}
	$result->close();
	echo "</div>";
	echo '<div><span id="add_worker_b" onclick="javascript:add_wordker();">Добавить сотрудника</span></div>';
	
	echo '<div onclick="show(\'none\')" id="wrap"></div>';
	
	echo '<div id="window_ip" class="window_ip">';
	echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
	echo '<div id="ip_values_wrap"></div>';
	echo '</div>';
	
	echo '<div id="window" class="window">';
	echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
	
	echo '<b>Добавление нового сотрудника</b>';
	echo '<div id="new_worker_wrap">';
		echo '<div class="row_a"><div class="fname">Логин:</div><div class="fval"><input class="nwinp0" type="text" name="login"></div></div>';
		echo '<div class="row_a"><div class="fname">Пароль:</div><div class="fval"><input class="nwinp1" type="password" name="passwd" autocomplete="new-password"></div></div>';
		echo '<div class="row_a"><div class="fname">Фамилия:</div><div class="fval"><input class="nwinp0" type="text" name="lastname"></div></div>';
		echo '<div class="row_a"><div class="fname">Имя:</div><div class="fval"><input class="nwinp1" type="text" name="firstname"></div></div>';
		echo '<div class="row_a"><div class="fname">Отчество:</div><div class="fval"><input class="nwinp0" type="text" name="patronymic"></div></div>';
		echo '<div class="row_a"><div class="fname">Офис:</div><div class="fval"><select class="nwinp1" name="office">';
		echo '<option value="">Выберите</option>';
		foreach ($list_offices as $k => $v)
			echo '<option value="' . $k . '">' . $v . '</option>';
		echo '</select></div></div>';
		echo '<div class="row_a"><div class="fname">Должность:</div><div class="fval"><select class="nwinp0" name="position">';
		echo '<option value="">Выберите</option>';
		foreach ($staff_types as $k => $v)
		{
			//if ($v == 'super-admin')
			//	continue;
			echo '<option value="' . $v . '">' . $work_position[$v] . '</option>';
		}
		echo '</select></div></div>';
		echo '<div class="row_a"><div class="fname">email:</div><div class="fval"><input class="nwinp1" type="text" name="email"></div></div>';
		echo '<div class="row_a"><div class="fname">Раб. email:</div><div class="fval"><input class="nwinp0" type="text" name="email_corp"></div></div>';
		echo '<div class="row_a"><div class="fname">Моб. телефон:</div><div class="fval"><input class="nwinp1" type="text" name="phone_mob"></div></div>';
		echo '<div class="row_a"><div class="fname">Раб. телефон:</div><div class="fval"><input class="nwinp0" type="text" name="phone_work"></div></div>';	
		echo '<div class="row_a"><div class="fname">Доверенность:</div><div class="fval"><input class="nwinp1" type="text" name="details"></div></div>';	
		
		echo '<div class="row_a">';
		echo '<div class="cval"><button id="nworker_add_b" onclick="javascript:nworker_add();">Добавить</button></div>';
		echo '<div class="cval"><button id="nworker_clear_b" onclick="javascript:nworker_clear();">Очистить</button></div>';
		echo '</div>';
		
	echo '</div>';
	
	
	
	echo '</div>';

	echo "</div>";
	
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
?>

