<?php

if (!$error)
{
    $show_data = false;
    $ch_anketa_status_tm_dir = false;
    $ch_anketa_manager_tm_dir = false;
    $filter = '';

    switch($anketa_mass['status'])
    {
        case "new_in_tm":
        case "in_tm_office":
        case "primary_work":
        case "secondary_work":
        case "assigned_meeting":
        case "contract_signed": $filter = " WHERE in_tm='1'"; $in_tm = true; break;

        case "new_in_ozs":
        case "processing_ozs":
        case "pre_approved":
        case "final_analysis":
        case "final_a_end":
        case "ready2bank":
        case "banks_review":
        case "send2bank":
        case "credit_approved":
        case "credit_declined":
        case "loan_issued": $filter = " WHERE in_ozs='1'"; $in_ozs = true; break;

        case "work_complete":
        {
            if ($man_of)
            {
                if (($man_of == $staff_office) && ($staff_position == 'director_tm'))
                {
                    $in_tm = true;
                    $ch_anketa_status_tm_dir = true;
                    $ch_anketa_manager_tm_dir = true;
                    $show_data = true;
                }

                if ($staff_position == 'director_ozs')
                    $in_ozs = true;

                if (($staff_id == $man_tm) || ($staff_id == $man_ozs))
                {
                    $show_data = true;
                }
            }
            else
                if ($staff_position == 'director_tm')
                {
                    $ch_anketa_status_tm_dir = true;
                    $ch_anketa_manager_tm_dir = true;
                    $show_data = true;
                }
            break;
        }
    }


    if ($result = $db_connect->query("SELECT * FROM form_status_mass$filter ORDER BY id ASC;"))
    {

        while ($row = $result->fetch_array(MYSQLI_ASSOC))
        {
            $status_mass[$row['id']] = $row;
        }
        $result->close();
    }
    else
    {
        $error = true;
        $res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
        echo $res;
    }

}

$num_files = 0;
$set_photo = false;
if ($result = $db_connect->query("SELECT id, name, type, hash_name, for_za, av4tm FROM files WHERE form_id='$_id' AND av='1' ORDER BY id ASC;"))
{
    if ($result->num_rows)
    {
        $num_files = $result->num_rows;
        while ($row = $result->fetch_array(MYSQLI_ASSOC))
        {
            if ($row['type'] == 'photo')
                $set_photo = true;
            $file_mass[$row['id']] = array($row['id'], $row['type'], $row['name'], $row['hash_name'], $row['for_za'], $row['av4tm']);

            /*$v = $row['name'];

            $fid = $file_mass[$v][0];
            $fname = $file_mass[$v][2];
            echo '<div class="loaded_file">';
            echo "<select id='file_t_$fid' onchange=\"javascript:ch_file_type(this);\">";
            echo '<option ' . (($file_mass[$v][1] == 'passport_scan') ? "selected " : '') . 'value="passport_scan">Скан паспорта</option>';
            echo '<option ' . (($file_mass[$v][1] == 'passport_reg') ? "selected " : '') . 'value="passport_reg">Скан прописки</option>';
            echo '<option ' . (($file_mass[$v][1] == 'agree_scan') ? "selected " : '') . 'value="agree_scan">Скан согласия</option>';
            echo '<option ' . (($file_mass[$v][1] == 'other') ? "selected " : '') . 'value="other">Другой</option>';
            echo "</select>";
            //echo " <a href='/".get_path($_id)."/$v' download>$v</a>";
            //echo get_path($_id);
            //echo " <a href='/ajax.php?block=anketa&action=getfile&id=$fname' download>$fname</a>";
            echo " <a href='/files/$_id/$fid' download>$fname</a>";
            echo '</div>';*/
        }
    }
    //else
    //echo 'Файлов нет';
    $result->close();
}
else
{
    $error = true;
    $res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
    echo $res;
}

if (!$error)
{
    $fav_t = "Добавить в избранное";
    $fav_v = 1;
    if ($result = $db_connect->query("SELECT * FROM favorite_forms WHERE formid='$_id' AND staffid='$staff_id' ORDER BY id ASC;"))
    {
        if ($result->num_rows)
        {
            $fav_t = "Удалить из избранного";
            $fav_v = 0;
        }
    }
}

$file_types = array();
$sqlf = "SELECT * FROM file_types;";
echo "<script>";
echo "var fileTypesMass = new Array();\n";
if ($resultf = $db_connect->query($sqlf))
{
    if ($resultf->num_rows)
    {
        while ($row = $resultf->fetch_array(MYSQLI_ASSOC))
        {
            $file_types[$row['name']] = $row;
            if ($row['deleted'] + 0 == 0)
                echo "fileTypesMass['{$row['name']}'] = " . json_encode($row, JSON_UNESCAPED_UNICODE) .";\n";
        }
    }
    $resultf->close();
}
echo "</script>";

if (!$error)
{

    $notes_mass = preg_split("/\<end_note\>/", $anketa_mass['notes']);
    foreach ($notes_mass as $key => $val)
    {
        if ($val == "")
            continue;

        preg_match('/\<(note_.*?)\>/', $val, $val_mass);

        $m_name = $val_mass[1];
        $m_val = preg_replace("/\<$m_name\>/", '', $val);
        //$notes_mass_a[$m_name] = preg_replace("/\\n/", "<br>", htmlspecialchars($m_val));
        $notes_mass_a[$m_name] = htmlspecialchars($m_val);
    }


    $data_mass = preg_split("/\<br\>/", $anketa_mass['data']);

    foreach ($data_mass as $key => $val)
    {
        if ($val == "")
            continue;
        $value = preg_split("/::/", $val);
        //$data_mass_a[$value[0]] = $value[1];
        //$data_mass_a[$value[0]] = str_replace("\"", "'", $value[1]);
        $data_mass_a[$value[0]] = htmlspecialchars($value[1]);
    }

    $data_mass_a['lastname'] = $anketa_mass['lastname'];
    $data_mass_a['firstname'] = $anketa_mass['firstname'];
    $data_mass_a['middlename'] = $anketa_mass['middlename'];
    $data_mass_a['sum'] = $anketa_mass['sum'];
    $data_mass_a['sum_val'] = $anketa_mass['sum_val'];

    $fico_score = '';

    if (isset($data_mass_a['field180']))
        $fico_score = 0 + $data_mass_a['field180'];
    /*echo "<pre>";
    print_r($data_mass_a);
    echo "</pre>";
    */
    echo '<div id="system_data_wrap">';

    echo '<div id="system_data">';

    if ($anketa_mass['discount'] == 1)
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:110%;">По данной анкете установлена скидка: ' . $anketa_mass['discount_val'] . '</span></div>';
        echo '</div>';
    }

    if ($anketa_mass['source'] == 'wantresult')
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:110%;">Анкета создана на основе интересов посетителя сайта. Заявку он не оставлял!!!</span></div>';
        echo '</div>';
    }

    if ($anketa_mass['source'] == 'tel')
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:110%;">Анкета создана на основе неотвеченного входящего звонка. Заявку клиент не оставлял!!!</span></div>';
        echo '</div>';
    }

    if ($anketa_mass['isexpress'] == 1)
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:130%;">ВНИМАНИЕ! Данная заявка отправлена на экспресс кредит за 1 день!</span></div>';
        echo '</div>';
    }

    
    if ($anketa_mass['quiz'] == 1)
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:130%;">Заявка на основании прохождения опроса!</span></div>';
        echo '</div>';
    }

    if ($anketa_mass['zalog'] == 1)
    {
        echo '<div class="row_a">';
        echo '<div class="fname"></div>';
        echo '<div class="fval"><span style="color:red; font-weight: bold; font-size:110%;"><img src="/img/icon.mortgage.png" height="17px;" style="top:5px;"> Заявка с рекламы под залог!</span></div>';
        echo '</div>';
    }
    echo '<div class="row_a">';
    $red_text = ($anketa_mass['status'] == 'work_complete') ? ' style="color:red; font-weight:bold;"' : '';
    echo '<div class="fname">Статус:</div><div class="fval"' . $red_text . '>' . $status_description[$anketa_mass['status']];
    echo '</div>';
    //if (($anketa_mass['manager'] == $staff_id) || ($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')))
    if (check_permission('change_status'))
    {
        if (($staff_office_type == 'main') || ((($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs'))) && ($anketa_mass['office'] == $staff_office)) || $ch_anketa_status_tm_dir)
        {
            echo '<span id="ch_astatus" onclick="javascript:change_ast();"><button>Сменить статус анкеты</button></span>';
            echo '<select id="select_status" onchange="javascript:select_status();" class="hide_ch_st">';
            foreach ($status_mass as $k => $v)
            {
                $descr = $v['description'];
                if (($v['name'] == 'new_in_ozs') && $in_tm)
                    $descr = 'Отправить в ОЗС';
                $sel = ($v['name'] == $anketa_mass['status']) ? " selected" : "";
                echo '<option value="' . $v['name'] . "\"$sel>" . $descr . '</option>';
            }
            echo '</select>';
        }
        else if ($staff_id == $anketa_mass['manager'])
        {
            //if ($staff_id_debug == 1)
            //echo '<span id="close_form" onclick="javascript:show_close_form();" data-toggle="modal" data-target="#exampleModalCenter"><button>Закрыть</button></span>';
            //else if ($anketa_mass['status'] != 'work_complete')
            //echo '<span id="close_form" onclick="javascript:close_form();"><button>Закрыть анкету</button></span>';
            if ($staff_office_type != 'ozs')
            echo '<span id="close_form" onclick="javascript:show_close_form();"><button>Закрыть анкету</button></span>';
        }
    }
    echo '</div>';

    echo '<div class="row_a">';
    echo '<div class="fname">В работе у:</div>';
    if (($anketa_mass['office'] == 0) && ($anketa_mass['manager'] == 0))
        echo '<div class="fval">-';
    else if ($anketa_mass['manager'] == 0)
        echo '<div class="fval">' . $offices_mass[$anketa_mass['office']]['name'];
    else
    {
        $fio_a = $staff_mass[$anketa_mass['manager']]['lastname'];
        if ($staff_mass[$anketa_mass['manager']]['firstname'] != '')
            $fio_a .= ' ' . substr($staff_mass[$anketa_mass['manager']]['firstname'], 0, 2) . '.';
        if ($staff_mass[$anketa_mass['manager']]['patronymic'] != '')
            $fio_a .= ' ' . substr($staff_mass[$anketa_mass['manager']]['patronymic'], 0, 2) . '.';
        echo '<div class="fval">' . $fio_a . '(' . $offices_mass[$anketa_mass['office']]['name'] . ')';
    }
    echo '</div>';

    if (check_permission('form_move'))
    {
        if (($staff_office_type == 'main') || ((($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')))&& ($anketa_mass['office'] == $staff_office)) || ($ch_anketa_manager_tm_dir))
        {
            echo ' <span id="move_a" onclick="javascript:move_anketa();"><button>Передать анкету</button></span>';
            echo '<select id="select_staff" name="select_staff" onchange="javascript:select_staff();" class="hide_ch_st">';
            echo '<option value="-1">Выберите</option>';

            foreach ($offices_mass as $k => $v)
            {
                if ($v['type'] == 'main')
                    continue;

                /*if ($office_type != 'main')
                {
                    if ($in_ozs && ($v['type'] == 'tm'))
                        continue;

                    if ($in_tm && ($v['type'] == 'ozs'))
                        continue;

                    if ((($staff_position == 'director_tm') || ($staff_position == 'director_ozs')) && ($v['id'] != $staff_office))
                        continue;
                }*/

//                if ((($staff_position == 'director_tm') || ($staff_position == 'director_ozs')) && ($v['id'] != $staff_office))
                if ((($staff_position == 'director_tm')) && ($v['id'] != $staff_office))
                    continue;

                echo '<optgroup label="' . $v['name'] . '">';
                echo '<option value="of_' . $v['id'] . '">Передать в офис</option>';
                foreach ($staff_mass as $key => $val)
                {
                    if (($val['office'] != $k) || $val['deleted'] == 1)
                        continue;

                    $fio_a = $val['lastname'];
                    if ($val['firstname'] != '')
                        $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                    if ($val['patronymic'] != '')
                        $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                    $red_c = '';
                    if ($val['id'] == $anketa_mass['tm_man'])
                        $red_c = ' style=color:red;';

                    echo '<option' . $red_c . ' value="st_' . $val['id'] . '">' . $fio_a . '</option>';
                }

                echo '</optgroup>';
            }
            echo '</select>';
        }
        else if ($in_ozs && ($anketa_mass['manager'] == $staff_id))
        {
            echo ' <span id="move_a" onclick="javascript:move_anketa();"><button>Передать анкету в ТМ</button></span>';
            echo '<select id="select_staff" name="select_staff" onchange="javascript:select_staff();" class="hide_ch_st">';
            echo '<option value="-1">Выберите</option>';

            foreach ($offices_mass as $k => $v)
            {
                if ($v['type'] == 'main')
                    continue;

                if ($v['type'] == 'ozs')
                    continue;

                /*if ($in_ozs && ($v['type'] == 'tm'))
                    continue;

                if ($in_tm && ($v['type'] == 'ozs'))
                    continue;*/

                echo '<optgroup label="' . $v['name'] . '">';
                echo '<option value="of_' . $v['id'] . '">Передать в офис</option>';
                foreach ($staff_mass as $key => $val)
                {
                    if (($val['office'] != $k) || $val['deleted'] == 1)
                        continue;

                    if (($val['id'] != $anketa_mass['tm_man']) && ($val['position'] != 'director_tm'))
                        continue;

                    $fio_a = $val['lastname'];
                    if ($val['firstname'] != '')
                        $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                    if ($val['patronymic'] != '')
                        $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                    echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
                }

                echo '</optgroup>';
            }
            echo '</select>';
        }
    }
    echo '</div>';




    echo '<div class="row_a">';
    echo '<div class="fname">Дата:</div><div class="fval">' . $anketa_mass['date_add'] . '</div>';
    if ((($staff_office_type == 'main' || $staff_position == 'director_ozs') && check_permission('set_work')) || $staff_id == '39')
        echo '<span id="set_task_sp" onclick="javascript:show_tasks_list();"><button>Поставить задачу</button></span>';
    echo '</div>';
    /*echo '<div class="row_a">';
        echo '<div class="fname">Тип:</div><div class="fval">' . $anketa_mass['type'] . '</div>';
    echo '</div>';
    echo '<div class="row_a">';
    $source = ($anketa_mass['source'] == 'site') ? $anketa_mass['source_param'] : 'Сотрудник';
        echo '<div class="fname">Источник:</div><div class="fval">' . $source . '</div>';
    echo '</div>';
    echo '<div class="row_a">';
        echo '<div class="fname">IP:</div><div class="fval">' . $anketa_mass['ip'] . '</div>';
    echo '</div>';*/

    echo '<div class="row_a">';
    echo '<div class="fname">Анкета №:</div><div class="fval"><b>' . $_id . '</b></div>';
    if (($staff_office_type == 'main') || ($staff_position == 'director_ozs') || ($staff_id == $anketa_mass['manager']) || ($fav_v == 0))
        echo '<span id="favorite_b" onclick="javascript:set_favorite(' . $fav_v . ');"><button>' . $fav_t . '</button></span>';
    echo '</div>';

    echo '<div class="row_a">';
    echo '<div class="fname">ФИО:</div><div class="fval" style="font-weight:bold; font-style:italic;">' . $anketa_mass['lastname'] . ' ' . $anketa_mass['firstname'] . ' ' . $anketa_mass['middlename'] . '</div>';

    if (check_permission('form_return'))
    {
        if (($staff_id == $anketa_mass['manager']) || ($staff_office_type == 'main') || ($staff_position == 'director_ozs') || ($in_tm && ($staff_position == 'director_tm') && ($anketa_mass['office'] == $staff_office)) || ($in_ozs && ($staff_id == $anketa_mass['manager'])))
        {
            echo ' <span id="ret_a" onclick="javascript:ret_anketa();"><button>Вернуть анкету</button></span>';
            echo '<select id="ret_select_staff" onchange="javascript:select_staff(\'ret_select_staff\');" class="hide_ch_st">';
            echo '<option value="-1">Выберите</option>';

            $staff_ret_mass = preg_split("/;;/", $anketa_mass['man_history']);

            $list_ret_st = array();
            for($k=count($staff_ret_mass)-1; $k>=0; $k--)
            {

                $val = preg_replace('/;/', '', $staff_ret_mass[$k]);
                $val_m = preg_split("/\|/", $val);

                if (in_array($val_m[1], $list_ret_st))
                    continue;

                if ($val_m[1] == $anketa_mass['manager'])
                    continue;

                array_push($list_ret_st, $val_m[1]);

                $val = $staff_mass[$val_m[1]];
                $fio_a = $val['lastname'];
                if ($val['firstname'] != '')
                    $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                if ($val['patronymic'] != '')
                    $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                echo '<option value="st_' . $val_m[1] . '">' . $fio_a . '(' . $offices_mass[$val_m[2]]['name'] . ')</option>';

                /*echo '<optgroup label="' . $v['name'] . '">';
                echo '<option value="of_' . $v['id'] . '">Передать в офис</option>';
                foreach ($staff_mass as $key => $val)
                {
                    if (($val['office'] != $k) || $val['deleted'] == 1)
                        continue;

                    $fio_a = $val['lastname'];
                    if ($val['firstname'] != '')
                        $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                    if ($val['patronymic'] != '')
                        $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                    $red_c = '';
                    if ($val['id'] == $anketa_mass['tm_man'])
                        $red_c = ' style=color:red;';

                    echo '<option' . $red_c . ' value="st_' . $val['id'] . '">' . $fio_a . '</option>';
                }

                echo '</optgroup>';*/
            }
            echo '</select>';
        }
    }

    echo '</div>';

    echo '<div class="row_a">';
    echo '<div class="fname">Дата рождения:</div>';
    if(array_key_exists('birth_day', $data_mass_a))
        echo '<div class="fval" style="font-weight:bold; font-style:italic;">' . $data_mass_a['birth_day'] . '</div>';
    else
        echo '<div class="fval" style="color:red; font-style:italic;">не заполнено</div>';

    if (check_permission('form_2za'))
    {
        if ((($anketa_mass['status'] == 'processing_ozs') || ($anketa_mass['status'] == 'pre_approved') || (($anketa_mass['status'] == 'final_a_end') && ($anketa_mass['substatus'] == 'reject'))) && (($in_ozs && ($anketa_mass['manager'] == $staff_id)) || ($staff_office_type == 'main' || $staff_position == 'director_ozs')))
            echo ' <span class="but_r" onclick="javascript:sent2fa();"><button>Отправить на ЗА</button></span>';
        else if (($anketa_mass['status'] == 'final_analysis') && ($staff_za || ($staff_office_type == 'main')))
        {
            echo ' <span class="but_r"><select id="finfasel"><option value="-1">Выберите</option><option value="approve">Одобрено</option><option value="reject">Отказ</option></select> <button onclick="javascript:fin_fa();">Завершить анализ</button></span>';
        }
    }
    echo '</div>';

    if (($anketa_mass['manager'] == $staff_id) || ($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')))
    {
        echo '<div class="row_a">';
        echo '<div class="fname">Паспорт:</div>';
        echo '<div class="fval">';
        if(array_key_exists('field18', $data_mass_a))
            echo $data_mass_a['field18'];
        else
            echo '<font style="color:red; font-style:italic;">не заполнено</font>';
        echo '-';
        if(array_key_exists('field19', $data_mass_a))
            echo $data_mass_a['field19'];
        else
            echo '<font style="color:red; font-style:italic;">не заполнено</font>';
        echo ', выдан ';
        if(array_key_exists('field21', $data_mass_a))
            echo $data_mass_a['field21'];
        else
            echo '<font style="color:red; font-style:italic;">не заполнено</font>';
        $ln_c = (isset ($anketa_mass['lastname'])) ? $anketa_mass['lastname'] : '';
        $fn_c = (isset ($anketa_mass['firstname'])) ? $anketa_mass['firstname'] : '';
        $mn_c = (isset ($anketa_mass['middlename'])) ? $anketa_mass['middlename'] : '';
        $bd_c = (isset ($data_mass_a['birth_day'])) ? $data_mass_a['birth_day'] : '';
        $ps_c = (isset ($data_mass_a['field18'])) ? $data_mass_a['field18'] : '';
        $pn_c = (isset ($data_mass_a['field19'])) ? $data_mass_a['field19'] : '';
        $pd_c = (isset ($data_mass_a['field21'])) ? $data_mass_a['field21'] : '';
        $ph_c = (isset ($data_mass_a['field29'])) ? $data_mass_a['field29'] : '';
        //$text2copy = $anketa_mass['lastname'] . ' ' . $anketa_mass['firstname'] . ' ' . $anketa_mass['middlename'] . ', ' . $data_mass_a['birth_day'] . ', паспорт ' . $data_mass_a['field18'] . ' ' . $data_mass_a['field19'] . ' выдан ' . $data_mass_a['field21'] . ', тел +' . $data_mass_a['field29'];
        $text2copy = $ln_c . ' ' . $fn_c . ' ' . $mn_c . ', ' . $bd_c . ', паспорт ' . $ps_c . ' ' . $pn_c . ' выдан ' . $pd_c . ', тел ' . $ph_c;
        echo ' <span style="cursor:pointer; font-style:italic;" onclick="javascript:copy_in_buf(\'' . $text2copy . '\');">Скопировать в буфер</span>';
        echo '</div>';

        echo '</div>';
    }

    echo '<div class="row_a">';
    if(array_key_exists('field29', $data_mass_a))
        echo '<div class="fname">Телефон:</div><div class="fval">' . $data_mass_a['field29'];
    else
        echo '<div class="fname">Телефон:</div><div class="fval">-';
    echo '</div>';
    echo '</div>';

    if (($anketa_mass['status'] == 'final_analysis') && ($staff_za || ($staff_office_type == 'main')))
        $fza = ' <span class="but_r"><button onclick="javascript:show_za_files();">Файлы ЗА</button></span>';
    else
        $fza = '';

    echo '<div class="row_a">';
    if(array_key_exists('email', $data_mass_a))
        echo '<div class="fname">email:</div><div class="fval"><a href="mailto:' . $data_mass_a['email'] . '">' . $data_mass_a['email'] . '</a></div>' . $fza;
    else
        echo '<div class="fname">email:</div><div class="fval">-</div>' . $fza;
    echo '</div>';

    $data_mass_or = preg_split("/\<br\>/", $anketa_mass['data_orig']);
    $data_mass_or_a = array();

    foreach ($data_mass_or as $key => $val)
    {
        if ($val == "")
            continue;

        $value = preg_split("/::/", $val);

        if ($value[0] == "track")
            continue;
        if ($value[0] == "seo")
            continue;
        if ($value[0] == "sum_val")
            continue;
        if ($value[0] == "ip")
            continue;
        if ($value[0] == "type")
            continue;
        if ($value[0] == "phone_history")
            continue;

        if (isset($all_fields_mass_or[$value[0]]))
            $data_mass_or_a[$all_fields_mass_or[$value[0]]] = htmlspecialchars($value[1]);
        else
        {
            if ($value[0] == 'city')
                $value[0] = 'Регион';
            $data_mass_or_a[$value[0]] = htmlspecialchars($value[1]);
        }
    }
    echo '<div class="row_a">';
    echo '<div class="fname">Исходные данные:</div><div class="fval"><span style="cursor:pointer; font-style:italic;" data-tooltip="';
    foreach ($data_mass_or_a as $kor => $vor)
        echo "$kor:$vor<br>";
    echo '">посмотреть</span></div>';
    echo '</div>';
    if ($staff_office_type == 'main') {
        echo '<div class="row_a">';
        echo '<div class="fname">Метрика:</div><div class="fval"><span style="cursor:pointer; font-style:italic;" data-tooltip="';
        $track = explode('&', $anketa_mass['track']);
        foreach ($track as $kor => $vor)
            echo "$vor<br>";
        echo '">посмотреть</span></div>';
        echo '</div>';

    }


//	if ($staff_office != BROKER_OFFICE1)
    if (($staff_office != BROKER_OFFICE1) && ($staff_office_type != 'broker'))
    {
        echo '<div class="row_a">';
        echo '<div class="fname">Активная анкета:</div>';
        echo '<div class="fval">';
        $double_main = $anketa_mass['double_main'];
        if ($double_main != '')
        {
            $amass = explode(';;', $double_main);
            foreach($amass as $v)
            {
                $val = preg_replace('/[^0-9]/', '', $v);
                echo '<a target=_blank href="/details/' . $val . '/">' . $val . '</a> ';
            }
        }
        echo '</div>';
        echo '</div>';

        echo '<div class="row_a">';
        echo '<div class="fname">Дубли:</div>';
        echo '<div class="fval" style="width:400px;">';
        $double_double = $anketa_mass['double_sub'];
        if ($double_double != '')
        {
            $amass = explode(';;', $double_double);
            foreach($amass as $v)
            {
                $val = preg_replace('/[^0-9]/', '', $v);
                echo '<a target=_blank href="/details/' . $val . '/">' . $val . '</a> ';
            }
        }
        echo '</div>';
        echo '</div>';

        echo '<div class="row_a">';
        echo '<div class="fname">Основная анкета:</div><div id="main_form_div" class="fval">';
        if($anketa_mass['main_form'])
        {
            $fio_main = '';
            $val = preg_replace('/[^0-9]/', '', $anketa_mass['main_form']);
            if ($res_ = $db_connect->query("SELECT lastname, firstname, middlename FROM forms WHERE id='$val';"))
            {
                if ($res_->num_rows)
                {
                    $val_ = $res_->fetch_array(MYSQLI_ASSOC);
                    $fio_main = $val_['lastname'] . ' ' . $val_['firstname'] . ' ' . $val_['middlename'];
                }
            }
            echo '<a target=_blank href="/details/' . $anketa_mass['main_form'] . '/">' . $anketa_mass['main_form'] . '</a> ' . $fio_main;
            if (check_permission('form_double'))
                echo ' <img style="cursor:pointer;" onclick="javascript:savelinkform(this, \'mainform\', \'' . $anketa_mass['main_form'] . '\')" src="/img/delete.png">';
        }
        else if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')) || ($staff_id == $anketa_mass['manager']))
        {
            if (check_permission('form_double'))
                echo '<span style="cursor:pointer; font-style:italic;" onclick="javascript:show_link_form(this, \'mainform\');"><button>Указать анкету</button></span>';
        }
        echo '</div>';
        echo '</div>';

        echo '<div class="row_a">';
        echo '<div class="fname">Поданкеты:</div><div id="sub_forms_div" class="fval">';
        if($anketa_mass['sub_forms'])
        {
            $amass = explode(';;', $anketa_mass['sub_forms']);
            foreach($amass as $v)
            {
                $val = preg_replace('/[^0-9]/', '', $v);

                $fio_sub = '';

                if ($res_ = $db_connect->query("SELECT lastname, firstname, middlename FROM forms WHERE id='$val';"))
                {
                    if ($res_->num_rows)
                    {
                        $val_ = $res_->fetch_array(MYSQLI_ASSOC);
                        $fio_sub = $val_['lastname'] . ' ' . $val_['firstname'] . ' ' . $val_['middlename'];
                    }
                }
                echo '<a target=_blank href="/details/' . $val . '/">' . $val . '</a> ' . $fio_sub;
                if (check_permission('form_double'))
                    echo ' <img style="cursor:pointer;" onclick="javascript:savelinkform(this, \'subform\', \'' . $val . '\')" src="/img/delete.png"><br>';
            }
        }
        if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')) || ($staff_id == $anketa_mass['manager']))
        {
            if (check_permission('form_double'))
                echo ' <span style="cursor:pointer; font-style:italic;" onclick="javascript:show_link_form(this, \'subform\');"><button>Указать анкету</button></span>';
        }
        echo '</div>';
        echo '</div>';
    }

    echo '</div>';

    if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')) || ($staff_id == $anketa_mass['manager']))
    {

        if (($staff_office != BROKER_OFFICE1) && ($staff_office_type != 'broker'))
        {
            echo '<div id="double_wrap">';
            echo '<div class="row_a"><div class="fname">Дубли по ФИО:</div><div class="fval"><span id=double_fio>Идет поиск...</span></div></div>';
            echo '<div class="row_a"><div class="fname">Дубли по телефонам:</div><div class="fval"><span id=double_phone>Идет поиск...</span></div></div>';
            echo '<div class="row_a"><div class="fname">Дубли по IP:</div><div class="fval"><span id=double_ip>Идет поиск...</span></div></div>';
            echo '</div>';
        }

        echo '<br>';
        $prb_res = 0;
        if (check_permission('form_prb'))
        {
            echo '<div><span style="color:red; font-size: 1.5em;">Предварительные решения:</span>';
            echo '<span style="cursor:pointer; font-style:italic;" onclick="javascript:show_prb();">';
            include_once("services/cPRB.php");


            try {
                $prb = new cPRB($db_connect);
                $prb_res = 0 + $prb->count_products($data_mass_a);
                if ($prb_res == 0)
                    echo " <span style='color:orange; font-size: 1.5em;'>подходящих продуктов не найдено</span>";
                else if (($prb_res == 10) || ($prb_res == 11) || ($prb_res == 12) || ($prb_res == 13) || ($prb_res == 14))
                    echo " <span style='color:green; font-size: 1.5em;'>найдено $prb_res продуктов</span>";
                else if (($prb_res % 10) == 1)
                    echo " <span style='color:green; font-size: 1.5em;'>найден $prb_res продукт</span>";
                else if (($prb_res % 10) < 5)
                    echo " <span style='color:green; font-size: 1.5em;'>найдено $prb_res продукта</span>";
                else
                    echo " <span style='color:green; font-size: 1.5em;'>найдено $prb_res продуктов</span>";
            } catch (Exception $e) {
                echo "<span style='color:red; font-size: 1.5em;' >Исправьте ошибку в данных!</span>";
            }


            echo '</span>';

            echo '</div>';
            echo "<br>";
        }


        $now = new DateTime;
        if ($anketa_mass['call_event'] != null)
        {
            $call_event = $anketa_mass['call_event'];
            $sdcb = '';
            if ($anketa_mass['call_event'] < $now->format('Y-m-d H:i:s'))
                $miss_call = '<font color="red">ВНИМАНИЕ! Звонок пропущен!</font>';
            else
                $miss_call = '';
        }
        else
        {
            //$call_event = "Запланировать звонок";
            $call_event = "<button>Запланировать звонок</button>";
            $sdcb = 'style="display:none;"';
            $miss_call = '';
        }

        if ($anketa_mass['meet_event'] != null)
        {
            $meet_event = $anketa_mass['meet_event'];
            $sdmb = '';
            if ($anketa_mass['meet_event'] < $now->format('Y-m-d H:i:s'))
                $miss_meet = '<font color="red">ВНИМАНИЕ! Встреча пропущена!</font>';
            else
                $miss_meet = '';
        }
        else
        {
//		if ($staff_office == BROKER_OFFICE1)
            if (($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
                $prb_res = 1;
            //$meet_event = "Запланировать встречу";
            if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')))
                $meet_event = "<button>Запланировать встречу</button>";
            else if ($prb_res > 0)
                $meet_event = "<button>Запланировать встречу</button>";
            else
                $meet_event = "";
            $sdmb = 'style="display:none;"';
            $miss_meet = '';
        }

        echo '<div>Звонок: <span style="cursor:pointer; font-style:italic;" onclick="javascript:show_cal_form(this);">' . $call_event . '</span> <input id="call_event" class="datepicker-here" data-timepicker="true" type="text" style="display:none;"> <button style="display:none;" name="save_ev_b" id="call_b" onclick="javascript:save_event(this);">Сохранить</button> <img class="del_event_button" ' . $sdcb . ' onclick="javascript:del_event(\'call_event\', this)" src="/img/delete.png"> ' . $miss_call . '</div>';
        echo '<div>Встреча: <span style="cursor:pointer; font-style:italic;" onclick="javascript:show_cal_form(this);">' . $meet_event . '</span> <input id="meet_event" class="datepicker-here" data-timepicker="true" type="text" style="display:none;"> <button style="display:none;" name="save_ev_b" id="meet_b" onclick="javascript:save_event(this);">Сохранить</button> ';
        echo '<label style="display:none;"><input type="checkbox" id="meet_out_b" value="1">Выездная встреча?</label>';
        if (($staff_office_type == 'main') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker') || (($staff_position == 'director_tm') && $staff_office == $anketa_mass['office']))
            echo '<img class="del_event_button" ' . $sdmb . 'onclick="javascript:del_event(\'meet_event\', this)" src="/img/delete.png"> ';
        echo $miss_meet . '</div>';
    }
    echo '</div>';


    echo '<div onclick="show(\'none\')" id="wrap"></div>';
    echo '<div id="window" class="window">';
    echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
    echo '</div>';

    echo '<div id="za_files_wrap" class="window">';
    echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
    echo '</div>';

    if (check_permission('form_photo'))
    {
        $set_photo = ($set_photo) ? " установлено" : " не установлено";
        echo "<details><summary class=\"summary\">Фото$set_photo</summary>";
        if (false)
        {
            echo '<button onclick="show_photo_wrap();">Сделать фото</button>';
            echo '<div id="make_photo_wrap" style="display:none;">';
            ?>
            <div class="container">

                <div class="app">

                    <a href="#" id="start-camera" class="visible">Touch here to start the app.</a>
                    <video id="camera-stream"></video>
                    <img id="snap">

                    <p id="error-message"></p>

                    <div class="controls">
                        <a href="#" id="delete-photo" title="Delete Photo" class="disabled"><i class="material-icons">delete</i></a>
                        <a href="#" id="take-photo" title="Take Photo"><i class="material-icons">camera_alt</i></a>
                        <a href="#" id="download-photo" download="selfie.png" title="Save Photo" class="disabled"><i class="material-icons">file_download</i></a>
                    </div>

                    <!-- Hidden canvas element. Used for taking snapshot of video. -->
                    <canvas></canvas>

                </div>

            </div>
            <script src="/scripts/cam.js"></script>

            <?
            //<link rel="stylesheet" href="/css/cam.css">
            echo '</div>';
        }
        echo '<div id="photo_wrap">';
        foreach($file_mass as $key => $val)
        {
            if ($file_mass[$key][1] == 'photo')
            {
                //echo insert_base64_encoded_image(get_path($_id) . "/" . $file_mass[$key][3]);
                echo '<img src="/photo/' . $key . '/" title="test">';
            }

        }
        /*foreach($file_mass as $key => $val)
        {
            if ($file_mass[$key][1] == 'photo')
            {
                $image = imagecreatefromjpeg(get_path($_id) . "/" . $file_mass[$key][3]);
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        // Поворот на 180 градусов
                        case 3: {
                            $image = imagerotate($image,180,0);
                            break;
                        }
                        // Поворот вправо на 90 градусов
                        case 6: {
                            $image = imagerotate($image,-90,0);
                            break;
                        }
                        // Поворот влево на 90 градусов
                        case 8: {
                            $image = imagerotate($image,90,0);
                            break;
                        }
                    }
                    $file = TMP_DIR . $exif['FileName'];
                    if(imagepng($image, $file))
                    {
                        echo insert_base64_encoded_image($file);
                    }
                }

                //echo insert_base64_encoded_image(imagejpeg($image));
                //imagejpeg($image);
            }

                //echo insert_base64_encoded_image(get_path($_id) . "/" . $file_mass[$key][3]);
        }*/

        echo '</div>';
        echo '</details>';
    }

    if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && ($staff_position == 'director_ozs')) || ($staff_id == $anketa_mass['manager']) || $show_data)
    {
        if (check_permission('form_action'))
        {
            echo '<details><summary class="summary">Действия с анкетой</summary>';
            echo '<div id="actions_wrap">';
            //echo '<div>Действия с анкетой:</div><br>';
            echo '<div>';
            //echo '<button onclick="javascript:contract();">Договор</button> ';
            //if($staff_id == 1)
            if(true)
            {

                echo '<button onclick="javascript:contract_prepare();">Договор</button> ';
                echo '<div id="window_cp" class="window_cp">';
                echo '<div id="contract_prepare_wr">';
                $v1 = (isset($data_mass_a["lastname"])) ? $data_mass_a["lastname"] : "НЕ ЗАПОЛНЕНО!";
                $v2 = (isset($data_mass_a["firstname"])) ? $data_mass_a["firstname"] : "НЕ ЗАПОЛНЕНО!";
                $v3 = (isset($data_mass_a["middlename"])) ? $data_mass_a["middlename"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">ФИО</div>';
                echo "<div class=\"dog_div2\">$v1 $v2 $v3</div>";
                echo '</div>';

                $v = (isset($data_mass_a["birth_day"])) ? $data_mass_a["birth_day"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Дата рождения</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field27"])) ? $data_mass_a["field27"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Домашний телефон</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field29"])) ? $data_mass_a["field29"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Мобильный телефон</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["email"])) ? $data_mass_a["email"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">E-mail</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v1 = (isset($data_mass_a["field18"])) ? $data_mass_a["field18"] : "НЕ ЗАПОЛНЕНО!";
                $v2 = (isset($data_mass_a["field19"])) ? $data_mass_a["field19"] : "НЕ ЗАПОЛНЕНО!";
                $v3 = (isset($data_mass_a["field21"])) ? $data_mass_a["field21"] : "НЕ ЗАПОЛНЕНО!";
                $v4 = (isset($data_mass_a["field20"])) ? $data_mass_a["field20"] : "НЕ ЗАПОЛНЕНО!";

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Паспорт серия</div>';
                echo "<div class=\"dog_div2\">$v1</div>";
                echo '</div>';

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Паспорт номер</div>';
                echo "<div class=\"dog_div2\">$v2</div>";
                echo '</div>';

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Дата выдачи</div>';
                echo "<div class=\"dog_div2\">$v3</div>";
                echo '</div>';

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Кем выдан</div>';
                echo "<div class=\"dog_div2\">$v4</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field24"])) ? $data_mass_a["field24"] : "";
                $addr_data_m = preg_split("/;;;/", $v);
                $add_s = '';
                if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
                {
                    $addr_data_m[0] = '';
                    $addr_data_m[1] = '';
                    $addr_data_m[2] = '';
                    $addr_data_m[3] = '';
                    $addr_data_m[4] = '';
                    $addr_data_m[5] = '';
                    $addr_data_m[6] = '';
                    $addr_data_m[7] = '';
                }
                else
                {
                    foreach($addr_data_m as $k=>$v)
                    {
                        if ($v != '')
                        {
                            if ($add_s != '')
                                $add_s .= ', ';
                            if($k < 3)
                            {
                                $mv = preg_split("/, /", $v);
                                if (is_array($mv) && (count($mv) == 2))
                                {
                                    if ($mv[1] == 'обл')
                                        $v = $mv[0] . " " . $mv[1];
                                }
                                //else
                                //$v = $mv[1] . ". " . $mv[0];
                            }

                            if ($k == 3)
                                $v = "ул. $v";
                            if ($k == 4)
                                $v = "д. $v";
                            if ($k == 5)
                                $v = "стр. $v";
                            if ($k == 6)
                                $v = "к. $v";
                            if ($k == 7)
                                $v = "кв. $v";
                            $add_s .= $v;
                        }
                    }
                }

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Адрес регистрации по месту жительства</div>';
                echo "<div class=\"dog_div2\">$add_s</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field123"])) ? $data_mass_a["field123"] : "";
                $addr_data_m = preg_split("/;;;/", $v);
                $add_s = '';
                if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
                {
                    $addr_data_m[0] = '';
                    $addr_data_m[1] = '';
                    $addr_data_m[2] = '';
                    $addr_data_m[3] = '';
                    $addr_data_m[4] = '';
                    $addr_data_m[5] = '';
                    $addr_data_m[6] = '';
                    $addr_data_m[7] = '';
                }
                else
                {
                    foreach($addr_data_m as $k=>$v)
                    {
                        if ($v != '')
                        {
                            if ($add_s != '')
                                $add_s .= ', ';
                            if($k < 3)
                            {
                                $mv = preg_split("/, /", $v);

                                if (is_array($mv) && (count($mv) == 2))
                                {
                                    if ($mv[1] == 'обл')
                                        $v = $mv[0] . " " . $mv[1];
                                }
                                //else
                                //$v = $mv[1] . ". " . $mv[0];
                            }
                            if ($k == 3)
                                $v = "ул. $v";
                            if ($k == 4)
                                $v = "д. $v";
                            if ($k == 5)
                                $v = "стр. $v";
                            if ($k == 6)
                                $v = "к. $v";
                            if ($k == 7)
                                $v = "кв. $v";
                            $add_s .= $v;
                        }
                    }
                }

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Адрес фактического места жительства</div>';
                echo "<div class=\"dog_div2\">$add_s</div>";
                echo '</div>';


                $v = (isset($data_mass_a["field164"])) ? $data_mass_a["field164"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Кредитная программа</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field3"])) ? $data_mass_a["field3"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Цель кредита</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v1 = (isset($data_mass_a["field162"])) ? $data_mass_a["field162"] : "НЕ ЗАПОЛНЕНО!";
                $v2 = (isset($data_mass_a["field163"])) ? $data_mass_a["field163"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Сумма кредита</div>';
                echo "<div class=\"dog_div2\">От $v1 до $v2</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field165"])) ? $data_mass_a["field165"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Комиссия</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field6"])) ? $data_mass_a["field6"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Максимальный ежемесячный платеж</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field127"])) ? $data_mass_a["field127"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Максимальное кол-во кредитных продуктов</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field166"])) ? $data_mass_a["field166"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">минимальный срок кредита</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field167"])) ? $data_mass_a["field167"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">максимальный срок кредита</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                $v = (isset($data_mass_a["field168"])) ? $data_mass_a["field168"] : "НЕ ЗАПОЛНЕНО!";
                echo '<div class="dog_row">';
                echo '<div class="dog_div1">Макс. первоначальный взнос(если применимо)</div>';
                echo "<div class=\"dog_div2\">$v</div>";
                echo '</div>';

                echo '<div class="dog_row">';
                echo '<div class="dog_div1">';
                echo '<button onclick="javascript:contract();">Скачать договор</button> ';
                echo '<button onclick="javascript:contract_ur();">Скачать договор на юрлицо</button> ';
                echo '</div>';
                echo '<div class="dog_div2">';
                echo '<button onclick="javascript:edit_contract_fields();">Редактировать поля</button> ';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
                echo '</div>';
            }
            if (($staff_office_type == 'ozs') || ($staff_office_type == 'main'))
                echo '<button onclick="javascript:act();">Акт выполненных работ</button> ';
            //echo '<button disabled onclick="javascript:contract_ur();">Договор на юр. лицо</button> ';
            //echo '<button disabled onclick="javascript:send_mail();">Отправить email</button> ';
            echo '<button onclick="javascript:send_sms();">Отправить смс</button> ';
            //echo '<button onclick="javascript:send_filkos()" data-tooltip="filkos" >Передача анкеты Filkos</button>';
            echo '</div>';

            echo '<br><div>';
            /*if (($staff_office_type == 'ozs') || ($staff_office_type == 'main'))
            {
                $bos_text = '';
                if ($result = $db_connect->query("SELECT answer, count(answer) as can FROM bank_sent_online WHERE form_id = '$_id' GROUP BY answer;"))
                {

                    if ($result->num_rows)
                    {
                        $on_a = 0;
                        $on_s = 0;
                        $on_r = 0;
                        while ($row = $result->fetch_array(MYSQLI_ASSOC))
                        {
                            if ($row['answer'] == 'approve')
                                $on_a = $row['can'] + 0;
                            else if ($row['answer'] == 'reject')
                                $on_r = $row['can'] + 0;
                            else
                                $on_s = $row['can'] + 0;
                        }
                        $bos_text = " <font color='green'>$on_a</font>/<font color='orange'>$on_s</font>/<font color='red'>$on_r</font>";

                        //$res['online_history'] = $bank_sent_mass;
                    }
                    $result->close();
                }
                echo '<button onclick="javascript:online_sents();">Онлайн подача в банк' . $bos_text . '</button> ';
            }*/

            /*if ($staff_pc)
                echo '<button onclick="javascript:check_work_plan();">План работ</button> ';
            else if ($status_num_descr[$anketa_mass['status']] > 9)
                echo '<button onclick="javascript:set_work_plan();">План работ</button> ';
            if ($staff_id_debug == 1)
            {*/
            if ($status_num_descr[$anketa_mass['status']] > 7)
            {
                echo '<button onclick="javascript:set_work_plan();">Сформировать план работ</button> ';
                if ($staff_pc)
                    echo '<button onclick="javascript:check_work_plan();">Проверить план работ</button> ';
            }

            //}
            echo '<button onclick="javascript:set_pre_decision();">Верификация ТМ</button>';
            echo ' <button onclick="javascript:set_work_plan_dayli();">План на день</button>';


            $sql = "SELECT COUNT(msg) AS cm FROM form_log WHERE form_id='$_id' AND type='contract';";
            $show_contr_butt = false;
            if ($result = $db_connect->query($sql))
            {
                if ($result->num_rows)
                {
                    $val = $result->fetch_array(MYSQLI_ASSOC);
                    if ($val['cm'] > 0)
                        $show_contr_butt = true;
                }
                $result->close();
            }

            $dis_b = '';
            $b_conf = '? (да/нет)';
            if ($status_num_descr[$anketa_mass['status']] > 5)
            {
                $b_conf = '!';
                $dis_b = ' disabled';
            }

            if ($show_contr_butt)
                echo " <button onclick=\"javascript:contract_wroted();\"$dis_b>Договор заключен$b_conf</button>";

            if ($anketa_mass['status'] == 'contract_signed')
            {
                $task_hit = true;
                $sqltask = "SELECT * FROM tasks WHERE formid='$_id' AND type='task' AND whoset='0';";
                if ($resulttask = $db_connect->query($sqltask))
                {
                    if ($resulttask->num_rows)
                    {
                        while ($row = $resulttask->fetch_array(MYSQLI_ASSOC))
                        {
                            if (preg_match("/^Верификация трудоустройства/", $row['header']))
                            {
                                if (($row['status'] == 'new') || ($row['status'] == 'work'))
                                    $task_hit = false;
                            }
                        }
                    }
                    else
                        $task_hit = false;
                    $resulttask->close();
                }
                if ($task_hit)
                    echo " <button onclick=\"javascript:send2ozs();\">Отправить в ОЗС</button>";
                else
                    echo " <button disabled>Отправить в ОЗС <font color=\"red\">Не завершена верификация!</font></button>";
            }


            echo '</div>';

            echo "</div>";
            echo '</details>';
        }

        if (check_permission('form_checks'))
        {
            if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && (($staff_position == 'director_ozs') )) || ($status_num_descr[$anketa_mass['status']] > 1))
            {

                $all_checks = 0;
                if ($result = $db_connect->query("SELECT COUNT(id) AS cid FROM promoney_checks WHERE form_id='$_id';"))
                {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $all_checks = $row['cid'];
                    $result->close();
                }

                $mod_checks = 0;
                if ($result = $db_connect->query("SELECT COUNT(id) AS cid FROM promoney_checks WHERE form_id='$_id' AND status='MODERATE';"))
                {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $mod_checks = $row['cid'];
                    $result->close();
                }


                if ($result = $db_connect->query("SELECT COUNT(id) AS cid FROM idx_log WHERE formid='$_id' AND status!='failed';"))
                {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $all_checks += $row['cid'];
                    $result->close();
                }

                if ($result = $db_connect->query("SELECT COUNT(id) AS cid FROM fssp_checks WHERE formid='$_id' AND status!='failed';"))
                {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $all_checks += $row['cid'];
                    $result->close();
                }

                if ($result = $db_connect->query("SELECT COUNT(id) AS cid FROM idxTasks WHERE formID='$_id' AND status = 'getAnswer';"))
                {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
                    $all_checks += $row['cid'];
                    $result->close();
                }

                echo "<details><summary class=\"summary\">Проверки (Всего $all_checks, на модерации $mod_checks)</summary>";

                echo '<div id="check_wrap">';
                echo '<div>';
                echo '<a href="https://service.nalog.ru/bi.do" target=_blank>Ограничение счета в налоговой</a><br>';
                echo '<a href="https://service.nalog.ru/inn.do" target=_blank>Сведения о Физ.лице (ИНН)</a><br>';
                echo '<a href="https://xn--90adear.xn--p1ai/check/auto" target=_blank>проверка ограничений авто в гаи</a><br>';
                echo '<a href="https://www.cian.ru/kalkulator-nedvizhimosti/" target=_blank>оценка квартир</a><br>';
                echo '<a href="https://www.rusprofile.ru/" target=_blank>руспрофайл проверка компаний</a><br>';
                echo '<a href="http://moskva.regreestr.com/" target=_blank>Компании Москвы</a><br>';
                echo '<a href="http://fssprus.ru/" target=_blank>задолженности на фссп</a><br>';
                echo '<a href="http://kodtelefona.ru/" target=_blank>Проверка телефона</a><br>';
                echo '<a href="https://rosreestr.ru/wps/portal/online_request" target=_blank>Проверка обременений на недвижимость</a><br>';
                echo '<a href="https://www.list-org.com/" target=_blank>List Org</a><br>';
                echo '<a href="https://zachestnyibiznes.ru/" target=_blank>За чеcтный бизнес</a><br>';
                echo '<a href="https://sbis.ru/contragents/" target=_blank>СБИС Контрагенты</a><br>';
                echo '</div>';
                echo "<br>";
                include_once("services/checks.php");
                echo "</div>";
                echo '</details>';
            }
        }

        if (check_permission('form_files'))
        {
            echo '<details><summary class="summary">Файлы (<span id="files_num_s">' . $num_files . '</span>)</summary>';
            echo '<div id="files_wrap">';
            //echo '<div>Файлы:</div>';
            echo '<div id="list_files">';

            if (sizeof($file_mass))
            {
                foreach($file_mass as $key => $val)
                {
                    $v = $key;

                    $fid = $file_mass[$v][0];
                    $fname = $file_mass[$v][2];
                    $av4tm = $file_mass[$v][5];

                    $file_not_read = false;

                    if (is_null($av4tm))
                    {
                        //if (($staff_position == 'manager_tm') && ($file_mass[$v][1] == 'report') && (preg_match("/НБКИ-отчет/i", $fname)))
                        if (($staff_position == 'manager_tm') && ($file_mass[$v][1] == 'usert_21') && (preg_match("/НБКИ-отчет/i", $fname)))
                        {
                            $file_not_read = true;
                        }
                    }
                    else
                    {
                        $file_not_read = (0 + $av4tm) ? false : true;
                    }
                    /*if (($staff_position == 'manager_tm') && ($file_mass[$v][1] == 'report') && (preg_match("/НБКИ-отчет/i", $fname)))
                    {
                        $test = 0 + $file_mass[$v][5];
                        if ($test)
                            $file_not_read = false;
                        else
                            $file_not_read = true;
                    }*/


                    $ch_f_type = '';
                    if ($file_not_read)
                        $ch_f_type = ' disabled';


                    $act_type = $file_mass[$v][1];
                    //if ($staff_id == 1) {
                    if (true) {

                        echo '<div class="loaded_file">';
                        echo "<select id='file_t_$fid' onchange=\"javascript:ch_file_type(this);\"$ch_f_type>";

                        if ($file_types[$act_type]['deleted'] + 0) {
                            echo '<option selected value="' . $act_type . '">' . $file_types[$act_type]['description'] . '</option>';
                        }

                        forEach($file_types as $fk => $fv) {
                            if(!($fv['deleted'] + 0))
                                echo '<option ' . (($act_type == $fk) ? "selected " : '') . 'value="' . $fk . '">' . $fv['description'] . '</option>';
                        }

                        echo "</select>";

                    }
                    else {
                        echo '<div class="loaded_file">';
                        echo "<select id='file_t_$fid' onchange=\"javascript:ch_file_type(this);\"$ch_f_type>";
                        echo '<option ' . (($file_mass[$v][1] == 'passport_scan') ? "selected " : '') . 'value="passport_scan">Скан паспорта</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'passport_reg') ? "selected " : '') . 'value="passport_reg">Скан прописки</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'agree_scan') ? "selected " : '') . 'value="agree_scan">Скан согласия</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'passport_all') ? "selected " : '') . 'value="passport_all">Паспорт (полный)</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'photo') ? "selected " : '') . 'value="photo">Фото профиля</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'report') ? "selected " : '') . 'value="report">Отчет</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'tk') ? "selected " : '') . 'value="tk">Копия ТК</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'dohod') ? "selected " : '') . 'value="dohod">Справка о доходе</option>';
                        echo '<option ' . (($file_mass[$v][1] == '2ndfl') ? "selected " : '') . 'value="2ndfl">2-НДФЛ</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'declar') ? "selected " : '') . 'value="declar">Декларация</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'rs') ? "selected " : '') . 'value="rs">Выписка по р/с</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'ls') ? "selected " : '') . 'value="ls">Выписка по л/с</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'vdk') ? "selected " : '') . 'value="vdk">ВДК</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'td') ? "selected " : '') . 'value="td">Трудовой Договор</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'company_card') ? "selected " : '') . 'value="company_card">Карточка компании</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'verification') ? "selected " : '') . 'value="verification">Файл верификации</option>';
                        echo '<option ' . (($file_mass[$v][1] == 'other') ? "selected " : '') . 'value="other">Другой</option>';
                        echo "</select>";
                    }


                    $checked_f = (0 + $file_mass[$v][4]) ? " checked" : "";
                    if (($staff_office_type == 'ozs') || ($staff_office_type == 'main'))
                        echo ' <label data-tooltip="Файл для заключительного анализа?"><input type="checkbox" value="1" onclick="javascript:set_for_za(this, ' . $fid . ');"' . $checked_f . ' data-tooltip="Файл для заключительного анализа?"></label>';
                    //echo " <a id=\"fileid_$fid\" href='/files/$_id/$fid' download>$fname</a>";

                    if ($staff_office_type == 'main')
                        //if (false)
                    {
                        $test = 0 + $file_mass[$v][5];

                        if (is_null($av4tm))
                        {
                            if (preg_match("/НБКИ-отчет/i", $fname))
                                $test = 0;
                            else
                                $test = 1;
                        }
                        $f_checked = ' checked';
                        if (!$test)
                            $f_checked = '';
                        echo '<span> <input onclick="javascript:ch_av4tm(this, ' . $fid . ');" data-tooltip="Файл доступен менеджерам тм для скачивания?" type="checkbox" class="ios8-switch ios8-switch-sm"' . $f_checked . ' id="checkbox-' . $fid . '">' . "<label for='checkbox-$fid' data-tooltip='Файл доступен менеджерам тм для скачивания?'></label></span>";
                    }
                    if ($file_not_read)
                        echo " <span data-tooltip='Для получения информации по файлу свяжитесь с руководством!'>$fname</span>";
                    else
                        echo " <a id=\"fileid_$fid\" href='/files/$_id/$fid' target=_blank>$fname</a>";
                    if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs))
                        echo ' <a style="cursor:pointer;" onclick="javascript:del_file(' . $fid . ', \'' . $fname . '\', this);"><img src="/img/delete.png" width="16" height="16"></a>';
                    echo '</div>';
                }
            }
            else
                echo 'Файлов нет';

            echo '</div><div id="add_file_wrap"><div>';
            echo '<form method="post" enctype="multipart/form-data" name="files_mass" id="files_mass"></form>';
            echo '</div>';
            echo '<div>';
            echo '<span id="add_file_button" style="cursor:pointer; font-style:italic;" onclick="javascript:add_file_form(this.parentNode);"><button>Добавить файл</button></span> ';
            echo '<span id="send_file_button" style="cursor:pointer; font-style:italic; display:none; color:red; font-weight:bold;" onclick="javascript:send_files();">Загрузить файлы</span>';
            echo '</div></div>';
            echo "</div>";
            echo '</details>';
        }

        echo '<div id="window_prb">';
        echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
        echo '<div id="prod_ctrl"></div>';
        echo '<div id="prod_result_wrap">';
        echo '<div id="prod_result"></div>';
        echo '<div id="prod_miss" style="display:none;">miss</div><br>';
        echo '<div id="prod_miss2" style="display:none;">miss</div>';
        echo '</div>';
        echo '</div>';

        if (check_permission('form_banks'))
        {
            //Работа с банками
            if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && (($staff_position == 'director_ozs') || ($anketa_mass['manager'] == $staff_id))))
            {
                echo '<details><summary class="summary">Работа с банками</summary>';
                echo '<div id="bank_wrap">';
                //echo '<div>Работа с банками:</div>';
                //echo '<div><a href="/prb/' . $_id . '/" target=\"_blank\">Предварительные решения банков</a></div>';
                echo '<div id="bank_list_wrap"></div>';
                echo '<div id="bank_add_wrap"></div>';
                //echo '<span style="cursor:pointer; font-style:italic;" onclick="javascript:add_bank_send_fields();">Отправить в банк</span>';
                echo "</div>";
                echo "</details>\n";
            }
        }


        if (check_permission('form_ki'))
        {
            echo '<details id="cred_his_det"><summary class="summary">Кредитная история</summary>';
            echo '<div id="cred_his_wrap">';
            echo '<div id="cred_his_data">';
            add_anketa_data($block_mass, true);
            /*	foreach ($all_fields_mass as $key => $val)
                {
                    if ($val['block'] != 128)
                        continue;
                }
    */
            echo "</div>";
            echo "</div>";
            echo "</details>\n";
        }


        echo '<div id="anketa_data_wrap">';
        echo '<div><input id="filter_input" oninput="javascript:filter_mass(this.value);" placeholder="Фильтр полей"> ';
        echo '<span style="display:none;" id="clear_filter_button" onclick="javascript:clear_filer();">Убрать фильтр</span> ';
        //echo '<span style="display:none;" id="all_filkos_button" onclick="javascript:show_all_filkos();">Показать все поля Filkos</span> ';
        echo '<span style="display:none;" id="save_data_button_top" onclick="javascript:save_data();">Сохранить изменения</span>';
        echo '</div>';
        echo '<div id="anketa_data">';
        //echo '<div>блок1 блок2 блок3 безблока все</div>';

        array_push($block_mass, array("id" => '', "description" => "Дополнительная информация", "goal" => "miss", "name" => "dop_block"));

        add_anketa_data($block_mass, false);

        $fias = '';
        if (isset($data_mass_a["field1"]))
        {
            if (isset($fias_mass[$data_mass_a["field1"]]))
                $fias = $fias_mass[$data_mass_a["field1"]];
        }

        echo "<div id=\"field1\" style=\"display:none;\">$fias</div>";


        echo "</div>";


        echo "</div>";

        $data_mass_np = preg_split("/\<br\>/", $anketa_mass['not_parsed']);

        if (sizeof($anketa_mass['not_parsed']) > 0)
        {

            echo '<div id="not_parsed_wrap">';
            echo "Нераспарсенные данные:";
            echo '<div id="not_parsed">';
            foreach ($data_mass_np as $key => $val)
            {
                if ($val == "")
                    continue;
                $value = preg_split("/::/", $val);
                echo '<div class="row_a">';
                echo '<div class="fname">';
                echo $value[0];
                echo '</div>';

                echo '<div class="fval">';
                echo $value[1];
                echo '</div>';
                echo '</div>';
                //$data_mass_a[$value[0]] = $value[1];
            }
            echo "</div></div>";
        }

    }

    echo '<div id="log_wrap">';
    echo '<div id="log_control">';
    echo '<div class="lc_div" id="log_msg"   onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Сообщения<span></span></div>';
    echo '<div class="lc_div" id="log_calls" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Звонки<span></span></div>';
    echo '<div class="lc_div" id="log_robot" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Системные<span></span></div>';
    echo '<div class="lc_div" id="log_sms"   onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Смс<span></span></div>';
    echo '<div class="lc_div" id="log_other" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Другие<span></span></div>';
    echo '</div>';
    echo '<div id="log_window"></div>';
    echo '<div id="input_window"><textarea></textarea>';
    echo '<label><input type="checkbox" name="toagent" id="msgtoagent" value="1">Сообщение Агенту</label>';
    echo '<button id="msg_button" onclick="javascript:send_msg2log();">Отправить</button></div>';
    echo "</div>";


    echo '<div style="display:none; position:fixed; bottom:5px; left:35%;" id="save_data_button" onclick="javascript:save_data();">Сохранить изменения</div>';

    echo "<script type=\"text/javascript\">\n";
    $atype = $anketa_mass['type'];
    echo "var anketa_type='$atype';\n";
    if($anketa_mass['main_form'])
        $smain_form = $anketa_mass['main_form'];
    else
        $smain_form = 0;
    echo "var anketa_main_form='$smain_form';\n";
    echo "var mass_fields = new Array();\n";

    $tmpf1p = '0';
    if (isset($data_mass_a['field1']))
    {

        $tmp_f1 = preg_split("/\; регион\: /", $data_mass_a['field1']);
        if (is_array($tmp_f1) && (count($tmp_f1) == 2))
            $tmp_f1 = $tmp_f1[1];
        else if (is_array($tmp_f1))
            $tmp_f1 = $tmp_f1[0];
        else
            $tmp_f1 = $data_mass_a['field1'];

        $tmpf1p = (($tmp_f1 == 'Санкт-Петербург, г') || ($tmp_f1 == 'Ленинградская, обл')) ? 1 : 0;
    }

    echo "var is_piter = " . $tmpf1p . ";\n";

    $tmpf1p = '0';
    if (isset($data_mass_a['field1']))
    {

        $tmp_f1 = preg_split("/\; регион\: /", $data_mass_a['field1']);
        if (is_array($tmp_f1) && (count($tmp_f1) == 2))
            $tmp_f1 = $tmp_f1[1];
        else if (is_array($tmp_f1))
            $tmp_f1 = $tmp_f1[0];
        else
            $tmp_f1 = $data_mass_a['field1'];

        $tmpf1p = (($tmp_f1 == 'Тюмень, г') || ($tmp_f1 == 'Тюменская, обл')) ? 1 : 0;
    }

    echo "var is_tumen = " . $tmpf1p . ";\n";

    /*$tmpf1p = '0';
    if (isset($data_mass_a['field1']))
    {

        $tmp_f1 = preg_split("/\; регион\: /", $data_mass_a['field1']);
        if (is_array($tmp_f1) && (count($tmp_f1) == 2))
            $tmp_f1 = $tmp_f1[1];
        else if (is_array($tmp_f1))
            $tmp_f1 = $tmp_f1[0];
        else
            $tmp_f1 = $data_mass_a['field1'];

        $tmpf1p = (($tmp_f1 == 'Казань, г') || ($tmp_f1 == 'Татарстан, Респ')) ? 1 : 0;
    }
*/
    //echo "var is_kazan = " . $tmpf1p . ";\n";
    echo "var is_kazan = 0;\n";


    foreach ($mass_for_js as $key => $val)
        echo "mass_fields[\"$key\"] = \"$val\";\n";

    reset($fields_mass);

    echo 'var field_types = new Array();' . "\n";
    foreach ($fields_mass as $key => $val)
    {
        $k = $val['name'];
        $v = $val['type'];
        echo "field_types[\"$k\"] = \"$v\";\n";
    }


    $ln = $anketa_mass['lastname'];
    $fn = $anketa_mass['firstname'];
    $mn = $anketa_mass['middlename'];

    echo "document.title = '$system_name :: №$_id :: $ln $fn $mn';\n";
    //echo "var staff_office_type = '$staff_office_type';\n";

    //echo "var selects = document.querySelectorAll(\"div[class=fval]>select\");\n";
    //echo "selects.forEach(function(item2, i, arr){\n old_input_value[item2.name] = item2.options[item2.selectedIndex].value;\n sec_input_value[item2.name] = true;\n });";

    if (($staff_office_type == 'main') || ($in_tm && ($staff_position == 'director_tm')) || ($in_ozs && (($staff_position == 'director_ozs'))))
    {

        $man2task = $anketa_mass['manager'];
        echo "select_man_task('$man2task');\n";


    }

    echo "console.log('==================');\n";
    echo "console.log($staff_office);\n";
    echo "console.log($man_of_act);\n";
    if ((($staff_position == 'director_tm') || ($staff_position == 'director_ozs')) && ($staff_office == $man_of || $staff_office == $man_of_act)) {
        echo "may_see_audio_js = true;\n";
        
    }
        

    if ($staff_office_type == 'main')
        echo "var only_online_banks = false;\n";
    else if ($status_num_descr[$anketa_mass['status']] < 10)
        echo "var only_online_banks = true;\n";
    else
        echo "var only_online_banks = false;\n";
    echo "</script>";
}

function add_anketa_data($block_mass, $is_cre)
{
    global $staff_office_type, $all_fields_mass, $fields_mass, $data_mass_a, $banks_mass, $algo_mass, $staff_id, $notes_mass_a, $cr_his_0_name, $subids_mass, $staff_office, $staff_position;
    $write_permission = check_permission('edit_form_field');
    foreach ($block_mass as $keyb => $valb)
    {
        if ($is_cre)
        {
            if ($valb['id'] != 128)
                continue;
        }
        else
        {
            if ($valb['id'] == 128)
                continue;
        }

        $show_block = '';
        /*if (!$is_cre)
        {
            if ($valb['goal'] != null)
                $show_block = 'style="display:none;"';

            if ($staff_office_type == 'main')
                 $show_block = '';
        }*/
        echo "<div class=\"block_wrap\"$show_block>";

        //if (!$is_cre)
        //{
        echo '<div class="head_a">';
        echo '<div class="head_d1">';
        echo $valb['description'];
        echo '</div><div class="head_d2">';
        //if ($valb['id'] == 124)
        //echo "<span id=\"algo_first\" class=\"algo_red\" data-tooltip=\"algo_1\">Алгоритм</span>";
        
        /*if ((array_key_exists($valb['name'], $algo_mass)) && ($staff_office != BROKER_OFFICE1) && ($staff_office_type != 'broker'))
        {
            echo "<span title=\"Нажмите для отображения алгоритма\" class=\"algo_ algo_red\" data-tooltip=\"algo\" tooltip-id=\"{$valb['name']}\">{$algo_mass[$valb['name']]['name_desc']}</span>";
        }*/
        echo "</div>";
        echo "</div>";
        //}

        if ($is_cre)
        {
            if (isset($data_mass_a['field96']))
                $vval = $data_mass_a['field96'];
            else
                $vval = 'Нет данных';
            echo '<div class="row_a">';
            echo '<div class="fname">' . $cr_his_0_name . ':</div><div class="fval">';
            echo "$vval</div>";
            echo "</div>\n";
        }

        foreach ($all_fields_mass as $key => $val)
        {
            if ($val['block'] != $valb['id'])
                continue;

            $miss = true;
            $readonly = " disabled";
            if ($write_permission)
            {
                foreach ($fields_mass as $v)
                {
                    if($val['name'] == $v['name'])
                    {
                        $miss = false;
                        $readonly = "";
                        break;
                    }
                }
            }



            $hide_row = '';
            $show_value = '';
            $row_is_main = false;

            //if ($staff_id == 1)
            //{
            if ($subids_mass[$val['name']]["mainid"] != '')
            {
                $mainid = $subids_mass[$val['name']]["mainid"];
                if(!array_key_exists($mainid, $data_mass_a))
                {
                    $hide_row = ' style="display:none;"';
                }
                else if ($subids_mass[$mainid]["value"] != mb_strtolower($data_mass_a[$mainid]))
                    $hide_row = ' style="display:none;"';
            }

            $show_value = $subids_mass[$val['name']]['value'];


            if (is_array($subids_mass[$val['name']]["subids"]) && (count($subids_mass[$val['name']]["subids"]) > 0))
                $row_is_main = true;
            else if ($subids_mass[$val['name']]["subids"] != '')
                $row_is_main = true;

            if ($row_is_main)
            {
                $row_is_main = '';
                $subs = $subids_mass[$val['name']]["subids"];
                foreach ($subs as $vsub)
                {
                    if ($row_is_main != '')
                        $row_is_main .= ';';
                    $row_is_main .= $vsub;
                }
            }
            else
                $row_is_main = '';

            //}
            if (!$row_is_main)
                $row_is_main = '';
            //if (0 + $val['goal'] > 0)
            //$hide_row = ' style="display:none;"';

            echo '<div class="row_a"' . $hide_row . ' id="tab_row_' . $val['name'] . '">';

            $style_descr = '';

            if ($val['font_color'] != null)
            {
                $style_descr = ' style="color:' . $val['font_color'] . ';';
            }

            if (($val['back_color'] != null) && ($val['back_color'] != 'transparent'))
            {
                $style_descr = ' style="background-color:' . $val['back_color'] . ';';
            }

            if ($style_descr != '')
                $style_descr .= '"';

            echo '<div class="fname"' . $style_descr . '>' . $val['description'] . ':</div><div class="fval">';
            $inp_show = 'display:none;';
            $spanshow = '';
            $hit_data = false;
            if(array_key_exists($val['name'], $data_mass_a))
            {
                $inp_show = '';
                $spanshow = 'display:none;';
                $hit_data = true;
            }

            $multi = '';
            $focus = '';

            $addr_span = "";
            if ($val['type'] == 'addr')
                $addr_span = ' class="addr_span"';
            else if ($val['type'] == 'banks')
                $addr_span = ' class="banks"';

            if (!$hit_data)
                echo "<span$addr_span style=\"cursor:pointer; font-style:italic; color:deeppink; $spanshow\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">Ввести данные</span>";

            $oninputscript = "";
            switch($val['type'])
            {
                case 'addr':
                {
                    if ($hit_data)
                    {
                        $vval = $data_mass_a[$val['name']];
                        $addr_data = preg_replace("/;;;/", ", ", $vval);
                        $addr_data_m = preg_split("/;;;/", $vval);

                        //if (!is_array($addr_data_m) || sizeof($addr_data_m != 8))
                        if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
                        {

                            $addr_data_m[0] = $vval;
                            $addr_data_m[1] = '';
                            $addr_data_m[2] = '';
                            $addr_data_m[3] = '';
                            $addr_data_m[4] = '';
                            $addr_data_m[5] = '';
                            $addr_data_m[6] = '';
                            $addr_data_m[7] = '';
                        }
                        echo "<span class=\"addr_span\" style=\"cursor:pointer; font-style:italic; color:black; $inp_show\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">$addr_data</span>";
                    }
                    else
                    {
                        $addr_data_m[0] = '';
                        $addr_data_m[1] = '';
                        $addr_data_m[2] = '';
                        $addr_data_m[3] = '';
                        $addr_data_m[4] = '';
                        $addr_data_m[5] = '';
                        $addr_data_m[6] = '';
                        $addr_data_m[7] = '';
                        $vval = '';
                    }

                    $faname = $val['name'];

                    $js = 'onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" ';

                    $ap = 'Квартира';
                    if (preg_match("/организации/i", $val['description']))
                        $ap = 'Офис';

                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Регион: </span><input autocomplete="disabled" class="fil2" type="text" name="' 			. $faname . '_reg"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[0] . '" oninput="javascript:reg_help(this);">';
                    echo '<span id="sp_' . $val['name'] . '_reg"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Район: </span><input autocomplete="disabled" type="text" name="' 			. $faname . '_ray"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[1] . '" oninput="javascript:reg_help(this);">';
                    echo '<span id="sp_' . $val['name'] . '_ray"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Нас.&nbsp;пункт: </span><input autocomplete="disabled" type="text" name="' . $faname . '_city" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[2] . '" oninput="javascript:reg_help(this);">';
                    echo '<span id="sp_' . $val['name'] . '_city"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Улица:</span><input autocomplete="disabled" type="text" name="' 			. $faname . '_street"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[3] . '">';
                    echo '<span id="sp_' . $val['name'] . "_street\"></span></div>";
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Дом:</span><input autocomplete="disabled" type="text" name="' 				. $faname . '_dom"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[4] . '">';
                    echo '<span id="sp_' . $val['name'] . "_dom\"></span></div>";
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Строение:</span><input autocomplete="disabled" type="text" name="' 		. $faname . '_str"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[5] . '">';
                    echo '<span id="sp_' . $val['name'] . "_str\"></span></div>";
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Корпус:</span><input autocomplete="disabled" type="text" name="' 			. $faname . '_corp" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[6] . '">';
                    echo '<span id="sp_' . $val['name'] . "_corp\"></span></div>";
                    echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">' . $ap . ':</span><input autocomplete="disabled" type="text" name="' 		. $faname . '_apar" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[7] . '">';
                    echo '<span id="sp_' . $val['name'] . "_apar\"></span></div>";

                    break;
                }
                case 'date_field':
                {
                    if ($hit_data)
                        $vval = $data_mass_a[$val['name']];
                    else
                        $vval = '';

                    $ddate = 'onclick="javascript:date_format(this);" onkeyup="javascript:date_format(this);"';
                    echo '<input autocomplete="disabled" ' . $ddate . ' class="datepicker-here" data-date-format="dd.mm.yyyy" style="' . $inp_show . '" type="text" onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."><span id="sp_' . $val['name'] . "\"></span>";
                    break;
                }
                case 'telephone':
                {
                    if ($hit_data)
                        $vval = $data_mass_a[$val['name']];
                    else
                        $vval = '';

                    if(strlen($vval) == 10)
                        $vval = "7$vval";

                    if ($vval != '')
                    {
                        $vval_tmp = $vval;
                        $vval = $vval_tmp[0] . '(' . substr($vval_tmp, 1, 3) . ") " . substr($vval_tmp, 4, 3) . "-" . substr($vval_tmp, 7, 2) . "-" . substr($vval_tmp, 9, 2);
                    }

                    $ttel = 'onclick="javascript:tel_format(this);" onkeyup="javascript:tel_format(this);"';
                    echo '<input class="tel" autocomplete="disabled" ' . $ttel . ' style="' . $inp_show . '" type="text" onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."><input data-tel="' . $vval . '" onclick="javascript:pcall(this);" readonly class="phone_forwarded" style="' . $inp_show . '"><span id="sp_' . $val['name'] . "\"></span>";
                    break;
                }
                case 'kred_prod':
                {
                    echo '<div id="' . $val['name'] . '" class="kred_prod">';
                    if ($hit_data)
                    {
                        $values = explode(';;;', $data_mass_a[$val['name']]);
                        foreach($values as $v)
                        {
                            echo '<div><span>' . $v . '</span><a title="Удалить" href="#" onclick="javasccript:del_form_prod(this.parentNode);"><img src="/img/delete.png" width="16" height="16"></a></div>';
                        }
                    }
                    //echo "<span class=\"banks\" style=\"cursor:pointer; font-style:italic; color:black;\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">{$data_mass_a[$val['name']]}</span>";


                    echo '</div>';
                    echo '<div>';
                    $av_val = explode('::', $val['av_values']);
                    echo '<select' . $readonly . '><option value="">Выберите продукт</option>';
                    foreach ($av_val as $k => $v)
                    {
                        echo "<option value=\"$v\">$v</option>";
                    }
                    echo '</select>';

                    echo " <select$readonly>";
                    echo "<option value=\"\">Выберите</option>";
                    foreach ($banks_mass as $k => $v)
                        echo "<option value=\"$v\">$v</option>";
                    echo "</select> ";
                    echo "<input autocomplete=\"disabled\" name=\"test_summmm\" placeholder=\"Сумма\"$readonly> ";
                    echo "<span style=\"cursor:pointer; font-style:italic;\" onclick=\"javascript:add_form_prod(this, '{$val['name']}');\">Добавить ВВЕДЕННЫЙ продукт</span>";



                    echo '</div>';


                    /*echo '<div class="bank_val" style="display:none;" name="' . $val['name'] . '">';
                    if ($hit_data)
                    {
                        $mass_val = explode(',', $data_mass_a[$val['name']]);
                        foreach ($mass_val as $k => $v)
                        {
                            echo '<input syze="40" type="text" value="' . $v . '" oninput="javascript:bank_inp_s(this);"><br>';
                        }
                    }

                    echo "</div><div name=\"bank_p\" style=\"display:none\"><select>";
                    echo "<option value=\"\">Выберите</option>";
                    foreach ($banks_mass as $k => $v)
                        echo "<option value=\"$v\">$v</option>";
                    echo "</select> <span style=\"cursor:pointer; font-style:italic;\" onclick=\"javascript:add_bp(this);\">Добавить ВЫБРАННЫЙ банк</span></div>";*/
                    break;
                }
                case 'banks':
                {
                    if ($hit_data)
                        echo "<span class=\"banks\" style=\"cursor:pointer; font-style:italic; color:black;\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">{$data_mass_a[$val['name']]}</span>";

                    echo '<div class="bank_val" style="display:none;" name="' . $val['name'] . '">';
                    if ($hit_data)
                    {
                        $mass_val = explode(',', $data_mass_a[$val['name']]);
                        foreach ($mass_val as $k => $v)
                        {
                            echo '<input autocomplete="disabled" syze="40" type="text" value="' . $v . '" oninput="javascript:bank_inp_s(this);"$readonly><br>';
                        }
                    }

                    echo "</div><div name=\"bank_p\" style=\"display:none\"><select$readonly>";
                    echo "<option value=\"\">Выберите</option>";
                    foreach ($banks_mass as $k => $v)
                        echo "<option value=\"$v\">$v</option>";
                    echo "</select> <span style=\"cursor:pointer; font-style:italic;\" onclick=\"javascript:add_bp(this);\">Добавить ВЫБРАННЫЙ банк</span></div>";
                    break;
                }
                case 'numbers':
                {
                    $oninputscript = ' onkeyup="javascript:format_numbers(this);"';
                    if ($hit_data)
                        $vval = ($data_mass_a[$val['name']] == '') ? '' : number_format($data_mass_a[$val['name']], 0, '.', ' ');
                    else
                        $vval = '';
                }
                case 'reg':
                case 'input':
                {
                    if (($val['type'] == 'input') || ($val['type'] == 'reg'))
                    {
                        if ($hit_data)
                            $vval = $data_mass_a[$val['name']];
                        else
                            $vval = '';
                    }

                    $sc = '';
                    $ul = '';
                    $type = 'text';
                    $tt = '';
                    $ttt = '';

                    //if ($val['name'] == 'field1')
                    if ($val['type'] == 'reg')
                    {
                        $tt = 'class="addr_div"';
                        //$tt = 'class="fil2" id="txt"';
                        //$ttt = '<i class="fil5"></i><ul id="rsl" class="fil2"></ul>';
                        $sc = ' oninput="javascript:reg_help_obr(this);"';
                        $ul = '<i class="fil5"></i><ul name="rsl" class="fil2"></ul>';
                    }
                    else if ($val['name'] == 'field127')
                    {
                        $tt = '';
                        $ttt = '';
                        if ($vval == '')
                            $vval = 100;
                    }
                    else if (($val['name'] == 'field180') && ($staff_office_type == 'tm'))
                    {
                        $type = 'hidden';
                        if ($vval + 0 > 0)
                            echo 'Проверка FICO проведена. Данные у руководства.';
                    }
                    else
                    {
                        $tt = '';
                        $ttt = '';
                    }


                    //echo '<input ' . $tt . ' style="' . $inp_show . '" type="text"' . $oninputscript . ' onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."' . $sc . '>' . $ttt . '<span id="sp_' . $val['name'] . "\"></span>$ul";
                    echo '<input autocomplete="disabled" ' . $tt . ' style="' . $inp_show . '" type="' . $type . '"' . $oninputscript . ' onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..." data-tooltip="' . $vval . '"' . $sc . '><span id="sp_' . $val['name'] . "\"></span>$ul";
                    break;
                }
                case 'checkbox':
                {
                    $ch = "";
                    if(array_key_exists($val['name'], $data_mass_a))
                        $ch = (mb_strtolower($data_mass_a[$val['name']]) == 'да') ? " checked" : "";
                    echo '<input style="' . $inp_show . '" type="checkbox" value="да" onclick="javascript:select_field_new(this, \'' . $row_is_main . '\', \'' . $show_value. '\');" name="' . $val['name'] . "\" $ch$readonly><span id=\"sp_" . $val['name'] . '"></span>';
                    break;
                }
                case 'multiselect':
                {
                    if ($hit_data)
                        echo "<span style=\"cursor:pointer; font-style:italic; color:black;\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">{$data_mass_a[$val['name']]}</span>";

                    $is_banks = false;
                    if ($val['av_values'] == "***banks***")
                    {
                        $is_banks = true;
                        $av_val = $banks_mass;
                    }
                    else
                        $av_val = explode('::', $val['av_values']);

                    if ($hit_data)
                        $mass_val = explode(',', mb_strtolower($data_mass_a[$val['name']]));
                    //print_r($av_val);
                    //print_r($mass_val);
                    $cc = 0;
                    echo "<span id = \"inp_sp_{$val['name']}\" style=\"display:none;\">";
                    foreach ($av_val as $k => $v)
                    {
                        $ch = '';

                        //$v1 = ($is_banks) ? $k : $v;
                        $v1 = $v;
                        $v2 = $v;
                        //print_r(mb_strtolower($v));
                        if ($hit_data)
                            if (array_search(mb_strtolower($v), $mass_val) !== false)
                                $ch = " checked";
                        echo "<label><input type=\"checkbox\" onclick=\"javascript:select_field(this);\" name=\"" . $val['name'] . '[' . $cc . "]\" value='$v1' $ch $readonly> $v2<span id=\"sp_" . $val['name'] . '[' . $cc . "]\"></span></label><br>\n";
                        $cc++;
                    }
                    echo "</span>";
                    break;
                }
                case 'select':
                {

                    if ($hit_data)
                        echo "<span style=\"cursor:pointer; font-style:italic; color:black;\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">{$data_mass_a[$val['name']]}</span>";

                    $av_val = explode('::', $val['av_values']);

                    echo "<select style=\"display:none;\" name=\"" . $val['name'] . "\" onchange=\"javascript:change_select_new(this, '$row_is_main', '$show_value');\"$readonly>";
                    echo '<option value="">Выберите</option>';
                    foreach ($av_val as $v)
                    {
                        $sel = '';
                        if ($hit_data)
                            $sel = (mb_strtolower($v) == mb_strtolower($data_mass_a[$val['name']])) ? " selected" : "";
                        echo "<option value=\"$v\"$sel>$v</option>";
                    }
                    echo "</select><span id=\"sp_" . $val['name'] . '"></span>';
                    break;
                }
                case 'radio':
                {
                    $vname = $val['name'];
                    $av_val = explode('::', $val['av_values']);

                    if ($hit_data)
                        $nhit = '';
                    else
                        $nhit = ' style="display:none;"';

                    echo "<span id = \"inp_sp_{$val['name']}\"$nhit>";
                    foreach ($av_val as $v)
                    {
                        $ch = '';
                        if ($hit_data)
                            $ch = (mb_strtolower($v) == mb_strtolower($data_mass_a[$val['name']])) ? " checked" : "";
                        echo "<label><input type=\"radio\" name=\"$vname\" value=\"$v\" $ch onclick=\"javascript:ch_radio(this);\"$readonly>$v</label>";
                    }
                    //echo "</select><span id=\"sp_" . $val['name'] . '"></span>';
                    echo "<span id=\"sp_" . $val['name'] . '"></span>';

                    echo "</span>";
                }
            }
            echo "</div>";


            if (array_key_exists('note_' . $val['name'], $notes_mass_a))
            {
                $note_val = $notes_mass_a['note_' . $val['name']];
                echo '<a href="#" onclick="javascript:edit_note(this, \'note_' . $val['name'] . '\', true);"><img id="note_' . $val['name'] . '" src="/img/editred.png" height="15px" style="float:right;" data-tooltip="' . $note_val . '"></a>';
            }
            else
                echo '<a href="#" onclick="javascript:edit_note(this, \'note_' . $val['name'] . '\', false);"><img id="note_' . $val['name'] . '" src="/img/add.png" height="15px" style="float:right;"></a>';

            echo "</div>\n";

            unset($all_fields_mass[$key]);
        }
        echo '</div>';
    }
}
?>
