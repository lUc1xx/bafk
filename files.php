<?php
if (!defined('SYSTEM_START_9876543210')) exit;

if (isset($_GET['file']))
{
	$_file = $_GET['file'];
}
else die();

if ($_id == 'forms')
{
	switch ($_file)
	{
		case 0:
		{
			file_force_download(FORMS_DIR . 'contract.docx', 'contract.docx');
			break;
		}
		case 1:
		{
			file_force_download(FORMS_DIR . 'contract_ur.docx', 'contract_ur.docx');
			break;
		}
		case 2:
		{
			file_force_download(FORMS_DIR . 'act.docx', 'act.docx');
			break;
		}
		default: break;
	}
	die();
}


$path = get_path($_id);

if ($result = $db_connect->query("SELECT id, name, type, hash_name FROM files WHERE form_id='$_id' AND id='$_file' ORDER BY name ASC;"))
{
	if ($result->num_rows)
	{
		$row = $result->fetch_array(MYSQLI_ASSOC);
		//file_force_download($path . '/' . $row['name']);

		file_force_download(FILES_DIR . $path . '/' . $row['hash_name'], $row['name']);
	}
	
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

?>

