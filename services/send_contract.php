<?php
ob_start();
$data = $anketa_data;

// Загружаем шаблон
//$tmp_file = dirname(__FILE__).'/../../tmp/contract_'.$id.'.docx';
//$file = 'trace.txt';
$tmp_file = dirname(__FILE__).'/../../tmp/contract_'.$id.'.docx';

//file_put_contents($file, "tmp_file : $tmp_file...\n", FILE_APPEND | LOCK_EX);
copy(dirname(__FILE__).'/../forms/contract.docx', $tmp_file);


$fp = fopen("../logs/contract.log", "a+");
//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text'
$data_ = date("Y-m-d H:i:s", time());
$data_ .= " => " . print_r($data, true) . "\n";
$test = fwrite($fp, $data_);
fclose($fp);

$archive = new ZipArchive;
$archive->open($tmp_file);
$content = $archive->getFromName('word/document.xml');

$fp = fopen("../logs/test_dog_edit.log", "a+");
//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text'
$data_ = date("Y-m-d H:i:s", time());
$data_ .= " => " . print_r($content, true) . "\n";
$test = fwrite($fp, $data_);
fclose($fp);

$pasport ='';
if (isset($data['field18']) && isset($data['field19']))
	$pasport = $data['field18'] . ' ' . $data['field19'];

if (isset($data['field20']) && isset($data['field21']))
	$pasport .= ' выдан ' . $data['field20'] . ' ' . $data['field21'];

// Всталяем данные
assign('NUMBER', 		$data['id'], $content);
assign('DATE', 			format_date(/*$row['date_add']*/date('Y-m-d H:i:s')), $content);
assign('FULLNAME', 		"{$data['lastname']} {$data['firstname']} {$data['middlename']}", $content);
assign('LASTNAME', 		$data['lastname'], $content);
assign('FIRSTNAME', 		$data['firstname'], $content);
assign('MIDDLENAME', 		$data['middlename'], $content);
assign('PASSPORT', 		$pasport, $content);


$v = (isset($data["field123"])) ? $data["field123"] : "";
$addr_data_m = preg_split("/;;;/", $v);
$add_s = '';
if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
{
	$addr_data_m[0] = '';
	$addr_data_m[1] = '';
	$addr_data_m[2] = '';
	$addr_data_m[3] = '';
	$addr_data_m[4] = '';
	$addr_data_m[5] = '';
	$addr_data_m[6] = '';
	$addr_data_m[7] = '';
}
else
{
	
	foreach($addr_data_m as $k=>$v)
	{
		if ($v != '')
		{
			if ($add_s != '')
				$add_s .= ', ';
			if($k < 3)
			{
				$mv = preg_split("/, /", $v);
				
				if (is_array($mv) && (count($mv) == 2))
				{
					if ($mv[1] == 'обл')
						$v = $mv[0] . " " . $mv[1];
				}
				else
					$v = $mv[1] . ". " . $mv[0];
			}
			if ($k == 3)
				$v = "ул. $v";
			if ($k == 4)
				$v = "д. $v";
			if ($k == 5)
				$v = "стр. $v";
			if ($k == 6)
				$v = "к. $v";
			if ($k == 7)
				$v = "кв. $v";
			
			$add_s .= $v;
		}
	}
}
assign('REALADDRESS', 	$add_s, $content);
//assign('REALADDRESS', 	isset($data['field123']) ? $data['field123'] : '', $content);

$v = (isset($data["field24"])) ? $data["field24"] : "";
$addr_data_m = preg_split("/;;;/", $v);
$add_s = '';
if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
{
	$addr_data_m[0] = '';
	$addr_data_m[1] = '';
	$addr_data_m[2] = '';
	$addr_data_m[3] = '';
	$addr_data_m[4] = '';
	$addr_data_m[5] = '';
	$addr_data_m[6] = '';
	$addr_data_m[7] = '';
}
else
{
	foreach($addr_data_m as $k=>$v)
	{
		if ($v != '')
		{
			if ($add_s != '')
				$add_s .= ', ';
			if($k < 3)
			{
				$mv = preg_split("/, /", $v);
				
				if (is_array($mv) && (count($mv) == 2))
				{
					if ($mv[1] == 'обл')
						$v = $mv[0] . " " . $mv[1];
				}
				else
					$v = $mv[1] . ". " . $mv[0];
			}
			if ($k == 3)
				$v = "ул. $v";
			if ($k == 4)
				$v = "д. $v";
			if ($k == 5)
				$v = "стр. $v";
			if ($k == 6)
				$v = "к. $v";
			if ($k == 7)
				$v = "кв. $v";
			
			$add_s .= $v;
		}
	}
}
						
//assign('ADDRESS', 		isset($data['field24']) ? $data['field24'] : '', $content);
assign('ADDRESS', 		$add_s, $content);

//assign('BIRTHDAY', 		isset($data['birth_day']) ? format_date("{$data[ДР_год]}-{$data[ДР_месяц]}-{$data[ДР_день]}") : '', $content);
assign('BIRTHDAY', 		isset($data['birth_day']) ? $data['birth_day'] : '', $content);
//assign('HOMEPHONE', 	isset($data['Код_города_жит']) && isset($data['Телефон_жит']) ? format_phone($data['Код_города_жит'], $data['Телефон_жит']) : '', $content);
assign('HOMEPHONE', 	isset($data['field27']) ? $data['field27'] : '', $content);
//assign('MOBILEPHONE', 	isset($data['field29']) ? format_phone($data['Код_мобильный'], $data['Телефон_мобильный']) : '', $content);
assign('MOBILEPHONE', 	isset($data['field29']) ? $data['field29'] : '', $content);
assign('EMAIL', 		isset($data['email']) ? $data['email'] : '', $content);


assign('CREDIT_PROGRAMM', 		isset($data['field164']) ? $data['field164'] : '', $content);
assign('CREDIT_GOAL', 		isset($data['field3']) ? $data['field3'] : '', $content);
assign('CREDIT_MIN_SUM', 		isset($data['field162']) ? number_format($data['field162'], 0, '.', ' ') : '', $content);
assign('CREDIT_MAX_SUM', 		isset($data['field163']) ? number_format($data['field163'], 0, '.', ' ') : '', $content);
assign('MAX_CRED_NUM', 		isset($data['field127']) ? $data['field127'] : '', $content);
assign('CREDIT_MIN_TIME', 		isset($data['field166']) ? $data['field166'] : '', $content);
assign('CREDIT_MAX_TIME', 		isset($data['field167']) ? $data['field167'] : '', $content);
assign('MAX_FIRST_PAY', 		isset($data['field168']) ? $data['field168'] : '', $content);
assign('MAX_MONTH_PAY', 		isset($data['field6']) ? $data['field6'] : '', $content);
assign('OTHERINFO', 		isset($data['field181']) ? $data['field181'] : '', $content);


//$fn_s = ' ' . substr($data['firstname'], 0, 2) . '.';
$fn_s = substr($data['firstname'], 0, 2) . '.';
//$mn_s = ' ' . substr($data['middlename'], 0, 2) . '.';
$mn_s = substr($data['middlename'], 0, 2) . '.';
assign('FN_S',              $fn_s, $content);
assign('MN_S',              $mn_s, $content);


$man_id = $data['man_id'];
$in_work = '';
$staff_det = '';
$sql = "SELECT lastname, firstname, patronymic, details FROM staff WHERE id='$man_id';";
if ($result = $db_connect->query($sql))
{
	if ($result->num_rows)
	{
		$val = $result->fetch_array(MYSQLI_ASSOC);
		if ($val['details'] != null)
			$staff_det = $val['details'];
		
		$in_work = $val['lastname'];
		if ($val['firstname'] != '')
			$in_work .= ' ' . substr($val['firstname'], 0, 2) . '.';
		if ($val['patronymic'] != '')
			$in_work .= ' ' . substr($val['patronymic'], 0, 2) . '.';
	}
	else
		$in_work = "$man_id";
}

assign('INWORK',			$in_work, $content);
assign('STAFFDET',         $staff_det, $content);

if (isset($data['field164']))
{
	$data2 = explode(',', $data['field164']);
	
	$tmp_str_z = "";
	if (isset($data['field165']))
		$tmp_str_z = $data['field165'] . "% от суммы предоставленного Кредита";
	else
		$tmp_str_z = "__% от суммы предоставленного Кредита";
		
	
	if (in_array("Кредитная карта", $data2))
	{
		assign('NECEL_WO_COM',         "☐ кредитная карта", $content);			
		assign('COMISSION', 		$tmp_str_z, $content);
		if (in_array("Нецелевой кредит без залога", $data2))
			assign('NECEL_WO_ZAL',         "☐ нецелевой кредит без залога", $content);
	}
	else
		if (in_array("Нецелевой кредит без залога", $data2))
		{
			assign('NECEL_WO_COM',         "☐ нецелевой кредит без залога", $content);
			assign('COMISSION', 		$tmp_str_z, $content);
		}
		
	$tmp_str_z = "";
	if (isset($data['field179']))
		$tmp_str_z = $data['field179'] . "% от суммы предоставленного Кредита";
	else
		$tmp_str_z = "__% от суммы предоставленного Кредита";
	
	if (in_array("Нецелевой кредит обеспеченный залогом", $data2))
	{
		assign('NECEL_W_ZAL',           "☐ нецелевой кредит, обеспеченный залогом:", $content);
		assign('NECEL_ZAL_NED',         "☐ залог недвижимого имущества", $content);
		assign('NECEL_ZAL_AVT',         "☐ залог движимого имущества", $content);
		assign('NECEL_ZAL_POR',         "☐ поручительство", $content);
		assign('COMISSZAL', 			$tmp_str_z, $content);
		assign('COM_ZALOG', 			$tmp_str_z, $content);
	}
	
	assign('NECEL_WO_COM',          "", $content);
	assign('NECEL_WO_ZAL',          "", $content);
	assign('NECEL_W_ZAL',           "", $content);
	assign('NECEL_ZAL_NED',         "", $content);
	assign('NECEL_ZAL_AVT',         "", $content);
	assign('NECEL_ZAL_POR',         "", $content);
	assign('COMISSZAL',         	"", $content);
	assign('COM_ZALOG',         	"", $content);
	assign('COMISSION',         	"", $content);
}





// Создаем новый договор
$archive->addFromString('word/document.xml', $content);
$archive->close();

ob_end_clean();
// Отправляем клиенту
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Disposition: attachment; filename="contract.docx"');
//header("Content-Length: ".sizeof($tmp_file));
readfile($tmp_file);
unlink($tmp_file);

// Вспомогательные ф-ии
function assign($name, $value, &$pattern) {
	//$pattern = str_replace($name, iconv('CP1251', 'UTF-8//IGNORE', $value), $pattern);
	$pattern = str_replace($name, $value, $pattern);
}
function format_date($value)
{
	list($v, $t) = explode(' ', $value, 2);
	$d = explode('-', $v, 3);
	$m = array(null, 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
	return "{$d[2]} ".$m[(int)$d[1]]." {$d[0]} г.";
}
function format_phone($code, $number)
{
	return "+7 ($code) ".substr($number, 0, 3).'-'.substr($number, 3, 2).'-'.substr($number, 5, 2);
}