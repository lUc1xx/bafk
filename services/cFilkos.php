<?php



class cFilkos
{
	protected $db_connect;
	protected $post_fields = array();
	
	function __construct() 
    { 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    }
	
	function __construct1($db_c)
	{
		$this->db_connect = $db_c;
	}
	
	function send_form($data)
	{
		//print_r($data);
		$this->post_fields["fields[sid]"] = '99';
		foreach ($data as $k=>$v)
		{
			
			if (($k == 'block') || ($k == 'action') || ($k == 'id'))
				continue;

			/*if ($k == 'surname')
				$this->post_fields["fields[$k]"] = "";
			else*/ if ($k == 'trud')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Постоянная работа по ТК' : 	$tmp = 1; 	break;
					case 'Госслужащий' : 				$tmp = 2;	break;
					case 'По трудовому договору' : 		$tmp = 3;	break;
					case 'Владелец бизнеса' : 			$tmp = 4;	break;
					case 'ИП, ПБОЮЛ' : 					$tmp = 5;	break;
					case 'Учусь' : 						$tmp = 6;	break;
					case 'Не трудоустроен' : 			$tmp = 7;	break;
					case 'Пенсионер' : 					$tmp = 8;	break;
					case 'Неофициально' : 				$tmp = 9;	break;
					case 'Иное' : 						$tmp = 10;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'podtverjdenie')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Без подтверждения' : 			$tmp = 1; 	break;
					case 'В устной форме по телефону' : $tmp = 2;	break;
					case 'Справка по форме банка' : 	$tmp = 3;	break;
					case 'Справка 2-НДФЛ' : 			$tmp = 4;	break;
					case 'Налоговая декларация' : 		$tmp = 5;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'education')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Среднее' : 				$tmp = 1; 	break;
					case 'Среднее специальное' : 	$tmp = 2;	break;
					case 'Неполное высшее' : 		$tmp = 3;	break;
					case 'Высшее' : 				$tmp = 4;	break;
					case 'Ученая степень' : 		$tmp = 5;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'credit_history')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Никогда не брал кредитов' : 	$tmp = 1; 	break;
					case 'Кредиты есть просрочек нет' : $tmp = 2;	break;
					case 'Просрочки были, сейчас нет' : $tmp = 3;	break;
					case 'Просрочки сейчас есть' : 		$tmp = 4;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'property')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Нет' : 						$tmp = 1; 	break;
					case 'Недвижимость' : 				$tmp = 2;	break;
					case 'Отечественный автомобиль' : 	$tmp = 3;	break;
					case 'Иномарка' : 					$tmp = 4;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'zagranpass')
			{
				$tmp = 0;
				switch($v)
				{
					case 'Нет' : 									$tmp = 1; 	break;
					case 'Есть без выездов' : 						$tmp = 2;	break;
					case 'Есть с выездами в последние 6 мес.' : 	$tmp = 3;	break;
					case 'Есть с выездами более 6 мес. назад' : 	$tmp = 4;	break;
				}
				$this->post_fields["fields[$k]"] = $tmp;
			}
			else if ($k == 'telefon')
				$this->post_fields["fields[$k]"] = "+$v";
			else
				$this->post_fields["fields[$k]"] = $v;
		}
		
		//print_r($this->post_fields);
		return($this->make_request());
	}
	
	function make_request()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "---https:/ /o ld.filkos.com/?option=com_fkfilkos&task=outer.store");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $this->post_fields);
		$out0 = curl_exec($curl);
		$out = json_decode($out0, true);
		//$out = curl_exec($curl);
		curl_close($curl);
		
		return $out0;
	}
}
