<?php
/**
 *	Отправка сообщения через SMS сервис
 */

$sms_config = array(
	'key' => '17dccb2 3ce454c129859b525c330594d',
	'privateKey' => '7ae 8f481054a283cf6b06011c49834ff',
	'service' => 'http://api.at ompark.com',
	'uri' => '/sms/3.0/',
	'sender' => 'KREDITS MART',
	'method' => 'POST'
);

/*
метод getCampaignInfo()
Где:
sent – отправлено смс
delivered – доставлено смс
not_delivered – недоставлено смс
price – стоимость рассылки
status – состояние рассылки

Переменная status может принимать следующие значения:

0	В очереди отправки
1	Недостаточно денег для рассылки
2	В процессе рассылки
3	Отправлено
4	Нет правильных номеров получателей
5	Частично отправлено
6	Спам
7	Недействительное имя отправителя
8	Пауза
9	Запланирована
10	Ожидает модерации
11	Дубликат рассылки
12	Отклонена

метод getCampaignDeliveryStats()
Где:
phone – массив телефонов
sentdate – массив времен отправки
donedate – массив времен установления финального статуса
status – массив состояния смс.

Переменные  в status могут принимать следующие значения:

0	В очереди отправки
SENT	Отправлено
DELIVERED	Доставлено
NOT_DELIVERED	Не доставлено
INVALID_PHONE_NUMBER	Неверный номер
SPAM	Спам
Если поле sentdate  содержит значение "0000-00-00 00:00:00", значит, смс еще в очереди отправки. Так же, если donedate  содержит "0000-00-00 00:00:00", значит, финальный статус еще не получен от оператора
*/

class MY_SMS
{
	protected $key_ = "";
	protected $privateKey = "";
	protected $service = "";
	protected $uri = "";
	protected $method = "";
	protected $act = "";
	protected $control_sum = "";
	protected $params = array();
	protected $post_data = "";
	protected $db_connect;
	protected $staff_id = 0;
	protected $form_id = 0;
	protected $sql = "";
	protected $phone = "";
	protected $text = "";
	
	function __construct() 
    { 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    }
	
	function __construct2($db_c, $config)
	{
		$this->db_connect = $db_c;
		$this->setConfig($config);
	}
	
	function __construct3($stid, $db_c, $config)
	{
		$this->staff_id = $stid;
		$this->db_connect = $db_c;
		$this->setConfig($config);
	}
	
	function __construct4($aid, $stid, $db_c, $config)
	{
		$this->form_id = $aid;
		$this->staff_id = $stid;
		$this->db_connect = $db_c;
		$this->setConfig($config);
	}
	
	public function setConfig($config)
	{
		$this->key_ = $config["key"];
		$this->privateKey = $config["privateKey"];
		$this->service = $config["service"];
		$this->uri = $config["uri"];
		$this->method = $config["method"];
		$this->sender = $config["sender"];
	}
	
	public function getUserBalance()
	{
		
		$this->params ['currency'] = "RUB";
		$this->act = "getUserBalance";
	
		return $this->make_request();
	}
	
	public function getBalance()
	{
		
		$this->params ['currency'] = "RUB";
		$this->act = "getUserBalance";
	
		return $this->make_request(true);
	}
	
	public function sendSMS($p, $t)
	{
		$this->phone = preg_replace("/[^0-9]/", '', $p);
		if ((strlen($this->phone) == 11) && (substr($this->phone, 0, 1) == '8'))
		{
			$this->phone = "7" . substr($this->phone, 1);
		}
		$this->text = $t;
		$this->params ['currency'] = "RUB";
		$this->act = "sendSMS";
		
		$this->params ['sender'] = $this->sender;
		$this->params ['text'] = $this->text;
		$this->params ['phone'] = $this->phone;
		$this->params ['datetime'] = "";
		$this->params ['sms_lifetime'] = "0";


		return $this->make_request();
	}
	
	public function getCampaignInfo($id)
	{
		$this->act = "getCampaignInfo";
		$this->params ['id'] = $id;
		return $this->make_request();
	}
	
	public function getSMSstatus($id)
	{
		$this->act = "getSMSstatus";
		$this->params ['messageid'] = $id;
		return $this->make_request();
	}
	public function getSenderStatusAll()
	{
		$this->act = "getSenderStatusAll";
		//$this->params ['messageid'] = $id;
		return $this->make_request();
	}
	
	protected function make_request($bot = false)
	{
		$this->params ['version'] ="3.0";
		$this->params ['action'] = $this->act;
		$this->params ['key'] = $this->key_;
		$this->post_data = "&" . http_build_query($this->params);
		
		ksort ($this->params);
		$sum='';
		foreach ($this->params as $k=>$v)
		{
			$sum.= $v;
		}
			
		$sum .= $this->privateKey; //your private key
		$this->control_sum =  md5($sum);
		
		if( $curl = curl_init() ) 
		{
			curl_setopt($curl, CURLOPT_URL, $this->service . $this->uri . $this->act);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION,false);
			curl_setopt($curl, CURLOPT_HEADER,false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, "key=$this->key_&sum=$this->control_sum" . $this->post_data);

				if(false === ($out = curl_exec($curl))) {
					
					$fp = fopen("../logs/sms_error.log", "a+");
					//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text'
					$data_ = date("Y-m-d H:i:s", time());
					$data_ .= " => " . print_r($this->post_data, true) . " => " . print_r($out, true) . "\n";
					$test = fwrite($fp, $data_);
					fclose($fp);
			
				return '{"failed": "Сервис не отвечает."}';
			}

			curl_close($curl);
			$res = json_decode($out);

			$hit = true;
			switch($this->act)
			{
				case "sendSMS":
				{
					if(isset($res->error))
					{
						$ans = $res->error;
						$this->sql = "INSERT INTO sms_log(staff_id, form_id, date, command, phone, text, status, answer) VALUES('$this->staff_id', '$this->form_id', NOW(), 'sendSMS', '$this->phone', '$this->text', 'error', '$ans');";
					}
					else
					{
						$sms_id = $res->result->id;
						$price = $res->result->price;					
						$this->sql = "INSERT INTO sms_log(staff_id,form_id,date,command,phone,text,status,sms_id,price) VALUES('$this->staff_id', '$this->form_id', NOW(), 'sendSMS', '$this->phone', '$this->text', 'sent', '$sms_id', '$price');";
					}
					break;
				}
				
				case "getSMSstatus":
				{
					if(isset($res->error))
					{
						$ans = $res->error;
						$this->sql = "INSERT INTO sms_log(date, command, status, answer) VALUES(NOW(), 'getSMSstatus', 'error', '$ans');";
					}
					else
					{
						//$bal = $res->result->balance_currency;
						$this->sql = "INSERT INTO sms_log(date, command, status, answer) VALUES(NOW(), 'getSMSstatus', 'ok', '$out');";
					}
					break;
				}
				
				case "getCampaignInfo":
				{
					if(isset($res->error))
					{
						$ans = $res->error;
						$this->sql = "INSERT INTO sms_log(date, command, status, answer) VALUES(NOW(), 'getSMSstatus', 'error', '$ans');";
					}
					else
					{
						//$bal = $res->result->balance_currency;
						$this->sql = "INSERT INTO sms_log(date, command, status, answer) VALUES(NOW(), 'getSMSstatus', 'ok', '$out');";
					}
					break;
				}
				
				case "getUserBalance":
				{
					if(isset($res->error))
					{
						$ans = $res->error;
						$this->sql = "INSERT INTO sms_log(date, command, status, answer) VALUES(NOW(), 'getUserBalance', 'error', '$ans');";
					}
					else
					{
						$bal = $res->result->balance_currency;
						$this->sql = "INSERT INTO sms_log(date, command, status, balance) VALUES(NOW(), 'getUserBalance', 'ok', '$bal');";
					}
								
					break;
				}

				default: $hit = false; break;
			}
			
			if ($bot)
				$hit = false;
			
			if ($hit)
			{
				$db = $this->db_connect;
				if ($result = $db->query($this->sql))
				{
					if (!$db->affected_rows)
					{
						$fp = fopen("../logs/sms_error.log", "a+");
						//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text'
						$data_ = date("Y-m-d H:i:s", time());
						$data_ .= " => " . print_r($this->staff_id, true) . " => " . print_r($this->form_id, true) . " => " . print_r($this->phone, true) . " => " . substr(strip_tags($this->text), 0, 300) . "\n";
						$test = fwrite($fp, $data_);
						fclose($fp);
					}
					//$res2 = "Не удалось получить данные: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
				}
				else
					$res2 = "Не удалось получить данные: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
			}
			//return array($out, $res->result->body);
			return $out;
		}
	}	
}
