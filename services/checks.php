<?php
/*[CWICreditHistoryIndividual] => НБКИ-отчет для ФЛ
[CWICreditRatingIndividual] => Кредитный рейтинг для ФЛ
[CWIOkbReportPrivate] => Аналитический отчет
[CWICreditHistoryPrivate] => Кредитная история физического лица НБКИ


    [CWICreditRatingPrivate] => Кредитный рейтинг
    [CWICreditHistoryLegal] => Кредитная история юридического лица НБКИ
    [CWINbkiReportPrivate] => НБКИ-отчет
    [CWISecurityPrivate] => Проверка СБ по ФЛ
    [CWIBusinessLegal] => Бизнес-справка
    [CWIEgrulLegal] => ЕГРЮЛ
    [CWICkkiPrivate] => ЦККИ на физическое лицо
    [CWICkkiLegal] => ЦККИ на юридическое лицо
    [CWIScorringPrivate] => Скорринг FICO на физическое лицо
    [CWIEquifaxCreditReportPrivateAuto] => Кредитный отчет Equifax
    [CWINbkiFicoReportPrivate] => НБКИ+FICO-отчёт
    [CWINbkiCarPledge] => Автозалог НБКИ
    [CWICreditHistoryRsbPrivate] => КИ Русский Стандарт по ФЛ
    [CWISpark] => СПАРК
*/
//include_once("services/cPromoney.php");
//$promoney = new cPromoney($db_connect) or die('f');

$check_status_desc = array("FINAL" => "Завершен", "MODERATE" => "Проверяется", "ERROR" => "Ошибка");

//echo "<div><span style=\"cursor:pointer;\" data-tooltip=\"CWICreditHistoryIndividual\" onclick=\"javascript:promoney_check('CWICreditHistoryIndividual');\"><button>НБКИ-отчет для ФЛ</button></span>";

echo "<div>";
if (!(($staff_position == 'manager_tm') || ($staff_position == 'manager_ozs')))
echo "<button data-tooltip=\"CWICreditHistoryIndividual\" onclick=\"javascript:promoney_check('CWICreditHistoryIndividual');\">НБКИ-отчет для ФЛ</button>";
//echo " <span style=\"cursor:pointer;\" data-tooltip=\"CWICreditRatingIndividual\" onclick=\"javascript:promoney_check('CWICreditRatingIndividual');\"><button>Кредитный рейтинг для ФЛ</button></span>";

if (!(($staff_position == 'manager_tm') || ($staff_position == 'manager_ozs')))
echo " <button data-tooltip=\"CWICreditRatingIndividual\" onclick=\"javascript:promoney_check('CWICreditRatingIndividual');\">Кредитный рейтинг для ФЛ</button>";
//echo " <span style=\"cursor:pointer;\" data-tooltip=\"CWIOkbReportPrivate\" onclick=\"javascript:promoney_check('CWIOkbReportPrivate');\"><button>Аналитический отчет</button></span>";
echo " <button data-tooltip=\"CWIOkbReportPrivate\" onclick=\"javascript:promoney_check('CWIOkbReportPrivate');\">Аналитический отчет</button>";
//echo " <span style=\"cursor:pointer;\" data-tooltip=\"CWICreditHistoryPrivate\" onclick=\"javascript:promoney_check('CWICreditHistoryPrivate');\"><button>Кредитная история физического лица НБКИ</button></span></div>";
echo " <button data-tooltip=\"CWICreditHistoryPrivate\" onclick=\"javascript:promoney_check('CWICreditHistoryPrivate');\">Кредитная история физического лица НБКИ</button>";
echo " <button data-tooltip=\"finScoring\" onclick=\"javascript:finScoring();\">FICO</button>";
//echo " <button data-tooltip=\"verifyPhone\" onclick=\"javascript:check_idx_phone();\">Проверка контактного телефона</button>";

//if ($staff_office_type == 'main')
	echo " <button data-tooltip=\"searchphysical\" onclick=\"javascript:check_iidx_fssp();\">ФССП (iidx): Физическое лицо</button>";

if ($staff_id == 1)
{

    echo " <button data-tooltip=\"searchphysical\" onclick=\"javascript:searchphysical();\">ФССП: Физическое лицо</button>";
    //echo " <button data-tooltip=\"personDebt\" onclick=\"javascript:personDebt();\">Налоговая заолженность (IDX)</button>";
	echo " <button data-tooltip=\"personLoanRating\" onclick=\"javascript:personLoanRating();\">ККО (IDX)</button>";
	
}

if (($staff_office_type == 'main') || (($staff_office_type == 'tm') && ($staff_position == 'director_tm')))
	echo " <button data-tooltip=\"finScoringOkbQiwi\" onclick=\"javascript:finScoringOkbQiwi();\">ОКБ (IDX)</button>";

if (($staff_office_type == 'main') || ($staff_office_type == 'ozs'))
    echo " <button data-tooltip=\"personDebt\" onclick=\"javascript:personDebt();\">Налоговая заолженность (IDX)</button>";

echo '</div><div>Проверки:</div>';

if ($result = $db_connect->query("SELECT date, form_id, class, ch_id, status, file, error_text FROM promoney_checks WHERE form_id='$_id';"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//$url = preg_replace("/\\/i", '', $row['file']);
		$url = stripcslashes($row['file']);
		//echo "<div>{$row['date']} {$promoney_class_desc[$row['class']]} {$row['status']} <a href=\"$url\" target=\"_blank\">Скачать</a></div>";
		
		$check_status = (array_key_exists($row['status'], $check_status_desc)) ? $check_status_desc[$row['status']] : $row['status'];
		
		if (($row['status'] == "ERROR") && ($row['error_text'] != null))
			$check_status = $check_status . ' <span style="color:red;">' . $row['error_text'] . "</span>";
		if (($row['status'] == "ERROR_F") && ($row['error_text'] != null))
			$check_status = $check_status . ' <span style="color:red;">' . $row['error_text'] . "</span>";
		if ($row['class'] == 'CWICreditHistoryIndividual')
			echo "<div><span data-tooltip=\"get_promoney_res\" promoney_id=\"{$row['ch_id']}\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} {$promoney_class_desc[$row['class']]} $check_status</span></div>";
		else
			echo "<div><span style=\"font-style:italic;\">{$row['date']} {$promoney_class_desc[$row['class']]} $check_status</span></div>";
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

//if ($result = $db_connect->query("SELECT datereq AS date, type, status, value, answer FROM idx_log WHERE formid='$_id' AND status!='failed';"))
if ($result = $db_connect->query("SELECT datereq AS date, type, status, value, answer FROM idx_log WHERE formid='$_id';"))
{
	
	$except_code = array(
	0 => "Ошибок нет",
	1 => "Системная ошибка, свяжитесь с администратором",
	2 => "Субъект помечен в БД как скончавшийся.",
	3 => "Подозрение на мошенничество.",
	4 => "В кредитном отчете недостаточно информации для расчета.",
	5 => "В кредитном отчете недостаточно информации для расчета.",
	6 => "Информация устарела. Расчет не проводится."
	);

	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{	
		if($row['type'] == 'finScoring')
		{
			if ($row['status'] == 'failed')
				continue;
			$answer = json_decode($row['answer']);
			//if ($staff_id == 1)
			//{
				//print_r($row['answer']);
				//$answer = json_decode(addcslashes($row['answer'], '?'));
				//print_r($answer);
			//}
			if (0 + $answer->resultCode != 0)
				$check_status = 'Ошибка FICO <span style="color:red;">' . $answer->resultMessage . "</span>";
			else if ($row['status'] == "exception")
			{
				if (isset($answer->exclusionCode))
				{
					$desc_f = '';
					if ($answer->exclusionCode + 0 == 2)
						$desc_f = 'data-tooltip="Субъект помечен в БД как скончавшийся. Расчет скорингового балла производиться не будет." ';
					else if ($answer->exclusionCode + 0 == 3)
						$desc_f = 'data-tooltip="Подозрение на мошенничество. Обнаружена информация,<br>указывающая на возможное мошенничество. Расчет скорингового<br>балла производиться не будет." ';
					else if ($answer->exclusionCode + 0 == 5)
						$desc_f = 'data-tooltip="В кредитном отчете недостаточно информации для расчета скорингового балла. Расчет производиться не будет.<br> Рекомендуется оценка платежеспособности субъекта другими средствами." ';
					else if ($answer->exclusionCode + 0 == 6)
						$desc_f = 'data-tooltip="Информация устарела. Данные в кредитном отчете обновлялись слишком давно.<br>Расчет скорингового балла производиться не будет.<br>Рекомендуется оценка платежеспособности субъекта другими средствами." ';
					$check_status = 'FICO: <span ' . $desc_f .'style="color:red;">' . $except_code[$answer->exclusionCode] . '</span>';
				}
				else
					$check_status = 'Ошибка FICO <span style="color:red;">Не получилось посчитать балл</span>';
			}
			else if ($row['status'] != "SUCCESS")
				$check_status = 'Ошибка FICO <span style="color:red;">' . $row['status'] . "</span>";
			else
			{
				$check_status = ($staff_office_type == 'tm') ? 'Произведена проверка FICO. Для получения обращайтесь к руководству.' : 'FICO: ' . $row['value'];
				if ($fico_score != 0 + $row['value'])
					$check_status .= ' <font color="red">Не совпадает со значение в анкете!</font>';
			}
				
			
			$dt = 'Результат запроса:';
			if (isset($answer->score))
				$dt .= '<br> Балл:' . 			$answer->score;
			$dt .= '<br> Сообщение:' . 		$answer->resultMessage;
			$dt .= '<br> Подробности:';
			if (isset($answer->reasonCode0Desc))
				$dt .= '<br> -' . $answer->reasonCode0Desc;
			if (isset($answer->reasonCode1Desc))
				$dt .= '<br> -' . $answer->reasonCode1Desc;

			if (isset($answer->reasonCode2Desc))
				$dt .= '<br> -' . $answer->reasonCode2Desc;

			if (isset($answer->reasonCode3Desc))
				$dt .= '<br> -' . $answer->reasonCode3Desc;

			if (isset($answer->reasonCode4Desc))
				$dt .= '<br> -' . $answer->reasonCode4Desc;
			if (isset($answer->reasonCode5Desc))
				$dt .= '<br> -' . $answer->reasonCode5Desc;
			if (isset($answer->reasonCode6Desc))
				$dt .= '<br> -' . $answer->reasonCode6Desc;
			
			if ($staff_office_type == 'tm')
				echo "<div><span style=\"cursor:pointer; font-style:italic;\">{$row['date']} $check_status</span></div>";
			else
				echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} $check_status</span></div>";
		}
		else if($row['type'] == 'verifyPhone')
		{
			if ($row['status'] == 'failed')
				continue;
			$dt = '';
			$phone_test_mass1 = array(
			
			"5" => 'Высокий уровень соответствия',
			"4" => 'Средний уровень соответствия',
			"3" => 'Низкий уровень соответствия' ,
			"2" => 'Средний риск несоответствия' ,
			"1" => 'Высокий риск несоответствия' ,
			"0" => "Нет данных" ,
			"-1" => "Ошибка"
			);
			
			$phone_test_mass2 = array(
			
			"5" => "Высокий уровень соответствия, подтверждение в период 180+ дней",
			"4" => "Средний уровень соответствия, подтверждение в период 90-180 дней, отсутствие несоответствий за последние 60 дней" ,
			"3" => "Низкий уровень соответствия, подтверждение в период 0-90 дней, наличие несоответствий за последние 60 дней" ,
			"2" => "Средний риск несоответствия, подтверждение в период 0-90 дней, наличие несоответствий за последние 60 дней" ,
			"1" => "Высокий риск несоответствия, отсутствие подтверждения в период 0+ дней и наличие несоответствий за последние 60 дней" ,
			"0" => "Нет данных" ,
			"-1" => "Ошибка"
			);
			$answer = json_decode($row['answer']);
			if (0 + $answer->resultCode != 0)
				$check_status = '<span style="color:red;">Ошибка проверки телефона</span>';
			else
			{
				$check_status = $phone_test_mass1[0 + $row['value']];
				$dt = 'Результат запроса:<br>' . $phone_test_mass2[0 + $row['value']];
				
				switch(0 + $row['value'])
				{
					case "5":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:green;\">$check_status</span></span></div>";
						break;
					case "4":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:lime;\">$check_status</span></span></div>";
						break;
					case "3":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:orange;\">$check_status</span></span></div>";
						break;
					case "2":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:red;\">$check_status</span></span></div>";
						break;
					case "1":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:maroon;\">$check_status</span></span></div>";
						break;
					case "0":
						echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Результат проверки телефона: <span data-tooltip=\"$dt\" style=\"color:black;\">$check_status</span></span></div>";
						break;
					default:
						echo "<div><span data-tooltip=\"Неизвестный результат. Свяжитесь с администратором.\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} Неизвестная ошибка</span></div>";
						break;
				}
			}
			
			
			//echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} <span data-tooltip=\"$dt\">$check_status</span></span></div>";
		}
		else if($row['type'] == 'checkFssp')
		{
			$dt = '';
			
			if ($row['status'] == 'NEW')
			{
				$check_status = 'ФССП физик(iidx): Выполняется проверка';
			}
			else if ($row['status'] == 'SUCCESS')
			{

				$answer = json_decode($row['answer']);
				if ($answer->resultCode + 0 == 0)
				{
					if ($answer->found + 0 == 1)
					{
						$mass = $answer->data;
						$len = count($mass);
						
						foreach($mass as $k=>$v)
						{
							$dt .= $v->date . " " . $v->subject . " " . str_replace('"', '&#34;', $v->executiveDocument->document) . "<br>";
						}
						$check_status = 'ФССП физик(iidx): <span data-tooltip="' . $dt . '" style="color:red;">Найдено ' . $len . " записей</span>";
						//$dt = print_r($mass, true);
					}
					else
						$check_status = 'ФССП физик(iidx): <span style="color:green;">Информация не найдена</span>';
				}
				else
					$check_status = ' <span style="color:red;">Ошибка ФССП. Обратитесь к программисту в чат (' . $answer->resultMessage . ")</span>";
			}
			else if ($row['status'] == 'failed')
			{
				$answer = json_decode($row['answer']);
				$check_status = '<span style="color:red;"> Ошибка ФССП. Обратитесь к программисту в чат (' . $answer->resultMessage . ')</span>';
			}
			else
				$check_status = '<span style="color:red;">Ошибка ФССП. Обратитесь к программисту в чат</span>';
			
			echo "<div><span data-tooltip=\"$dt\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} $check_status</span></div>";			
			//print_r($row);
		}
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}


if ($result = $db_connect->query("SELECT lastCheck, answerData FROM idxTasks WHERE formID='$_id' AND type='finScoringOkbQiwi';")) {
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$res = json_decode($row['answerData']);
		if ($row['answerData']) {
			if (strtolower($res->status) == 'success') {
				$color = 'green';
				if (0 + $res->score < 601)
					$color = 'red';
				else if (0 + $res->score < 723)
					$color = 'orange';
				else if (0 + $res->score < 856)
					$color = 'yellowgreen';
				echo "<div><span style=\"font-style:italic;\">{$row['lastCheck']} Результат проверки ОКБ: <span style=\"color:$color\">{$res->score}</span></span></div>";
			}
		}
		else {
			echo "<div><span style=\"font-style:italic;\">{$row['lastCheck']} Ожидается проверка ОКБ.</span></div>";
		}
		
	}
	$result->close();
}

if ($result = $db_connect->query("SELECT lastCheck, answerData FROM idxTasks WHERE formID='$_id' AND type='personDebt' AND status='getAnswer';")) {
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $res = json_decode($row['answerData']);
        if (strtolower($res->status) == 'not_found') {
            echo "<div><span style=\"font-style:italic;\">{$row['lastCheck']} Налоговая(IDX): <span style=\"color:green\">Задолженностей не найдено</span></span></div>";
        }
        if (strtolower($res->status) == 'found') {
            $msg = '<ul>';
            foreach ($res->list as $el) {
                $msg .= '<li>' . $el->price . ' - ' . $el->info . '</li>';
            }
            $msg .= '</ul>';
            echo "<div><span style=\"font-style:italic;\">{$row['lastCheck']} Налоговая(IDX): <span style=\"color:red\">{$msg}</span></span></div>";

        }
//            echo "<div><span style=\"font-style:italic;\">{$row['lastCheck']} Результат проверки ОКБ: <span style=\"color:$color\">{$res->score}</span></span></div>";


    }
    $result->close();
}


if ($staff_id == 1)
{
if ($result = $db_connect->query("SELECT * FROM fssp_checks WHERE formid='$_id';"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$type = $row['type'];
		$color = '';
		$js = '';
		$fsspid = $row['id'];
		
		switch($row['type'])
		{
			case "physical" :	$type = "физлица"; break;
			case "legal" :		$type = "фирмы"; break;
			case "ip" :			$type = "делопроизводства"; break;
			default: 			$type = "Неизвестный тип"; $color = 'color:red;'; break;
		}
		
		switch($row['status'])
		{
			case "new" :		$status = "Новая"; break;
			case "error" :		$status = "Ошибка"; $color = 'color:red;'; break;
			case "success" :	$status = "Завершена"; $js = "onclick=\"javacript:show_res_fssp(this, $fsspid);\" data-tooltip=\"Для просмотра кликните\""; break;
			case "inwork" :		$status = "В работе"; break;
			default: 			$status = "Неизвестный статус"; break;
		}

		$date = $row['timeset'];
		$responsestatus = $row['responsestatus'];
		$answer = $row['answer'];
		
		echo "<div><span style=\"cursor:pointer; font-style:italic; $color\" $js>$date Проверка $type (ФССП). Результат: $status</span></div>";
		
		
		//echo "<div><span data-tooltip=\"get_promoney_res\" promoney_id=\"{$row['ch_id']}\" style=\"cursor:pointer; font-style:italic;\">{$row['date']} {$promoney_class_desc[$row['class']]} $check_status</span></div>";
		
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
}
?>