<?php
ob_start();
$data = $anketa_data;

// Загружаем шаблон
//$tmp_file = dirname(__FILE__).'/../../tmp/contract_'.$id.'.docx';
//$file = 'trace.txt';
$tmp_file = dirname(__FILE__).'/../../tmp/act_'.$id.'.docx';

//file_put_contents($file, "tmp_file : $tmp_file...\n", FILE_APPEND | LOCK_EX);
copy(dirname(__FILE__).'/../forms/act.docx', $tmp_file);


$fp = fopen("../logs/act.log", "a+");
//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text'
$data_ = date("Y-m-d H:i:s", time());
$data_ .= " => " . print_r($data, true) . "\n";
$test = fwrite($fp, $data_);
fclose($fp);

$archive = new ZipArchive;
$archive->open($tmp_file);
$content = $archive->getFromName('word/document.xml');
$content2 = $archive->getFromName('word/footer2.xml');

$pasport ='';
if (isset($data['field18']) && isset($data['field19']))
	$pasport = $data['field18'] . ' ' . $data['field19'];

if (isset($data['field20']) && isset($data['field21']))
	$pasport .= ' выдан ' . $data['field20'] . ' ' . $data['field21'];

// Всталяем данные
assign('NUMBER', 		$data['id'], $content);
assign('DATE', 			format_date(/*$row['date_add']*/date('Y-m-d H:i:s')), $content);
assign('FULLNAME', 		"{$data['lastname']} {$data['firstname']} {$data['middlename']}", $content);
assign('FULLNAME', 		"{$data['lastname']} {$data['firstname']} {$data['middlename']}", $content2);
assign('LASTNAME', 		$data['lastname'], $content);
assign('LASTNAME', 		$data['lastname'], $content2);
assign('FIRSTNAME', 		$data['firstname'], $content);
assign('FIRSTNAME', 		$data['firstname'], $content2);
assign('MIDDLENAME', 		$data['middlename'], $content);
assign('MIDDLENAME', 		$data['middlename'], $content2);
assign('PASSPORT', 		$pasport, $content);


$v = (isset($data["field123"])) ? $data["field123"] : "";
$addr_data_m = preg_split("/;;;/", $v);
$add_s = '';
if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
{
	$addr_data_m[0] = '';
	$addr_data_m[1] = '';
	$addr_data_m[2] = '';
	$addr_data_m[3] = '';
	$addr_data_m[4] = '';
	$addr_data_m[5] = '';
	$addr_data_m[6] = '';
	$addr_data_m[7] = '';
}
else
{
	
	foreach($addr_data_m as $k=>$v)
	{
		if ($v != '')
		{
			if ($add_s != '')
				$add_s .= ', ';
			if($k < 3)
			{
				$mv = preg_split("/, /", $v);
				
				if (is_array($mv) && (count($mv) == 2))
				{
					if ($mv[1] == 'обл')
						$v = $mv[0] . " " . $mv[1];
				}
				else
					$v = $mv[1] . ". " . $mv[0];
			}
			if ($k == 3)
				$v = "ул. $v";
			if ($k == 4)
				$v = "д. $v";
			if ($k == 5)
				$v = "стр. $v";
			if ($k == 6)
				$v = "к. $v";
			if ($k == 7)
				$v = "кв. $v";
			
			$add_s .= $v;
		}
	}
}
assign('REALADDRESS', 	$add_s, $content);
//assign('REALADDRESS', 	isset($data['field123']) ? $data['field123'] : '', $content);

$v = (isset($data["field24"])) ? $data["field24"] : "";
$addr_data_m = preg_split("/;;;/", $v);
$add_s = '';
if (!is_array($addr_data_m) || (count($addr_data_m) < 8))
{
	$addr_data_m[0] = '';
	$addr_data_m[1] = '';
	$addr_data_m[2] = '';
	$addr_data_m[3] = '';
	$addr_data_m[4] = '';
	$addr_data_m[5] = '';
	$addr_data_m[6] = '';
	$addr_data_m[7] = '';
}
else
{
	foreach($addr_data_m as $k=>$v)
	{
		if ($v != '')
		{
			if ($add_s != '')
				$add_s .= ', ';
			if($k < 3)
			{
				$mv = preg_split("/, /", $v);
				
				if (is_array($mv) && (count($mv) == 2))
				{
					if ($mv[1] == 'обл')
						$v = $mv[0] . " " . $mv[1];
				}
				else
					$v = $mv[1] . ". " . $mv[0];
			}
			if ($k == 3)
				$v = "ул. $v";
			if ($k == 4)
				$v = "д. $v";
			if ($k == 5)
				$v = "стр. $v";
			if ($k == 6)
				$v = "к. $v";
			if ($k == 7)
				$v = "кв. $v";
			
			$add_s .= $v;
		}
	}
}
						
//assign('ADDRESS', 		isset($data['field24']) ? $data['field24'] : '', $content);
assign('ADDRESS', 		$add_s, $content);

//assign('BIRTHDAY', 		isset($data['birth_day']) ? format_date("{$data[ДР_год]}-{$data[ДР_месяц]}-{$data[ДР_день]}") : '', $content);
assign('BIRTHDAY', 		isset($data['birth_day']) ? $data['birth_day'] : '', $content);
//assign('HOMEPHONE', 	isset($data['Код_города_жит']) && isset($data['Телефон_жит']) ? format_phone($data['Код_города_жит'], $data['Телефон_жит']) : '', $content);
assign('HOMEPHONE', 	isset($data['field27']) ? $data['field27'] : '', $content);
//assign('MOBILEPHONE', 	isset($data['field29']) ? format_phone($data['Код_мобильный'], $data['Телефон_мобильный']) : '', $content);
assign('MOBILEPHONE', 	isset($data['field29']) ? $data['field29'] : '', $content);
assign('EMAIL', 		isset($data['email']) ? $data['email'] : '', $content);


assign('CNAME',      isset($data['field49']) ? $data['field49'] : '', $content);
assign('CPOS',       isset($data['field57']) ? $data['field57'] : '', $content);
assign('FACT_WORK_ADR',     isset($data['field55']) ? $data['field55'] : '', $content);
assign('INN_COMPANY',       isset($data['field50']) ? $data['field50'] : '', $content);
assign('WORK_PHONE',        isset($data['field60']) ? $data['field60'] : '', $content);


assign('CREDIT_PROGRAMM', 		isset($data['field164']) ? $data['field164'] : '', $content);
assign('CREDIT_GOAL', 		isset($data['field3']) ? $data['field3'] : '', $content);
assign('CREDIT_MIN_SUM', 		isset($data['field162']) ? number_format($data['field162'], 0, '.', ' ') : '', $content);
assign('CREDIT_MAX_SUM', 		isset($data['field163']) ? number_format($data['field163'], 0, '.', ' ') : '', $content);
assign('COMISSZAL', 		isset($data['field179']) ? $data['field179'] : '', $content);
assign('COMISSION', 		isset($data['field165']) ? $data['field165'] : '', $content);
assign('COM_ZALOG', 		isset($data['field179']) ? $data['field179'] : '', $content);
assign('MAX_CRED_NUM', 		isset($data['field127']) ? $data['field127'] : '', $content);
assign('CREDIT_MIN_TIME', 		isset($data['field166']) ? $data['field166'] : '', $content);
assign('CREDIT_MAX_TIME', 		isset($data['field167']) ? $data['field167'] : '', $content);
assign('MAX_FIRST_PAY', 		isset($data['field168']) ? $data['field168'] : '', $content);
assign('MAX_MONTH_PAY', 		isset($data['field6']) ? $data['field6'] : '', $content);


$man_id = $data['man_id'];
$in_work = '';
$staff_det = '';
$sql = "SELECT lastname, firstname, patronymic, details FROM staff WHERE id='$man_id';";
if ($result = $db_connect->query($sql))
{
	if ($result->num_rows)
	{
		$val = $result->fetch_array(MYSQLI_ASSOC);
		if ($val['details'] != null)
			$staff_det = $val['details'];
		
		$in_work = $val['lastname'];
		if ($val['firstname'] != '')
			$in_work .= ' ' . substr($val['firstname'], 0, 2) . '.';
		if ($val['patronymic'] != '')
			$in_work .= ' ' . substr($val['patronymic'], 0, 2) . '.';
	}
}

assign('INWORK',			$in_work, $content);
assign('INWORK',			$in_work, $content2);
assign('STAFFDET',         $staff_det, $content);
assign('STAFFDET',         $staff_det, $content2);

$fn_s = substr($data['firstname'], 0, 2) . '.';
$mn_s = substr($data['middlename'], 0, 2) . '.';
assign('FN_S',              $fn_s, $content);
assign('FN_S',              $fn_s, $content2);
assign('MN_S',              $mn_s, $content);
assign('MN_S',              $mn_s, $content2);

$full_pass = $pasport . ", код подразделения ";
$full_pass .= isset($data['field22']) ? $data['field22'] : '';
assign('PASSP_FULL', 		$full_pass, $content);	//«Паспорт серия» «Паспорт номер» «Кем выдан» «Дата выдачи», код подразделения «Код подразделения»

if (isset($data['contract_date']))
{
	assign('CONTRACT_D',  format_date($data['contract_date']), $content);
	assign('CONTRACT_D',  format_date($data['contract_date']), $content2);
}
else
{
	assign('CONTRACT_D',  '', $content);
	assign('CONTRACT_D',  '', $content2);
}

if (isset($data['contract_date']))
{
	assign('CONTRACTD',  format_date($data['contract_date']), $content);
	assign('CONTRACTD',  format_date($data['contract_date']), $content2);
}
else
{
	assign('CONTRACTD',  '', $content);
	assign('CONTRACTD',  '', $content2);
}


// Создаем новый договор
$archive->addFromString('word/document.xml', $content);
$archive->addFromString('word/footer2.xml', $content2);
$archive->close();

ob_end_clean();
// Отправляем клиенту
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Disposition: attachment; filename="Акт к договору ' . $data['id'] . '.docx"');
//header("Content-Length: ".sizeof($tmp_file));
readfile($tmp_file);
unlink($tmp_file);

// Вспомогательные ф-ии
function assign($name, $value, &$pattern) {
	//$pattern = str_replace($name, iconv('CP1251', 'UTF-8//IGNORE', $value), $pattern);
	$pattern = str_replace($name, $value, $pattern);
}
function format_date($value)
{
	list($v, $t) = explode(' ', $value, 2);
	$d = explode('-', $v, 3);
	$m = array(null, 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
	return "{$d[2]} ".$m[(int)$d[1]]." {$d[0]} г.";
}
function format_phone($code, $number)
{
	return "+7 ($code) ".substr($number, 0, 3).'-'.substr($number, 3, 2).'-'.substr($number, 5, 2);
}