<?php
class cTelphin
{
	
	/*
	App Name: bsmart
	App Redirect URLs: None
	App ID: 3be1f9a3c1d94e73aeac7983e064d3f8
	App Secret: 6307f13f6d204f1da7a224774422076e
	App Type: trusted
	App Access: call_api
	Created: 2018-09-12 16:29:10
	*/

	protected $db_connect;
	protected $error_desc = '';
	protected $error = false;
	protected $error_num = 0;
	protected $grant_type = 'client_credentials';
	protected $client_id = '';
	protected $client_secret = '';
	protected $access_token = "";
	protected $expires_in = 3600;
	protected $token_type = 'Bearer';
	
	protected $REQ_HOST_TOKEN = 'https://apiproxy.telphin.ru';
	protected $REQ_URL_TOKEN = '/oauth/token';
	
	protected $REQ_HOST = 'https://apiproxy.telphin.ru/api/ver1.0';
	protected $REQ_URI = '';
	
	protected $staff_data = array();
	protected $post_data = array();
	
	function __construct() 
    { 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    }
	
	function __construct1($db_c)
	{
		$this->db_connect = $db_c;
		
		$sql = "SELECT param_name, param_val FROM telphin_params WHERE 1;";
		$db = $this->db_connect;		
		if ($result = $db->query($sql))
		{
			if ($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					switch($row['param_name'])
					{
						case "client_id" : 		$this->client_id 		= $row['param_val']; break;
						case "client_secret" : 	$this->client_secret 	= $row['param_val']; break;
						case "access_token" : 	$this->access_token 	= $row['param_val']; break;
						default: break;
					}
				}
			}
			else
			{
				$error = true;
				$error_desc = "num_rows=0";
			}
		}
		else
		{
			$error = true;
			$error_desc = "Не удалось получить данные: (" . $db->errno . ") " . $db->error;
		}
		
		$this->update_staff();
	}
	
	function check_error() 	{ return $this->error; }
	function get_error() 	{ return $this->error_desc; }
	function clear_error() 	{ $this->error_desc = ''; $this->error = false; }
	
	function update_staff()
	{
		$sql = "SELECT id, phone_work_add FROM staff WHERE deleted='0';";
		$db = $this->db_connect;		
		if ($result = $db->query($sql))
		{
			if ($result->num_rows)
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					if ($row['phone_work_add'] + 0 > 0)
						$this->staff_data[$row['phone_work_add']] = $row['id'];
				}
					
		}
		//print_r($this->staff_data);
	}
	
	function update_token()
	{
		//"status": 401,
		//print_r('update_token');
		$this->post_data['client_id'] = $this->client_id;
		$this->post_data['client_secret'] = $this->client_secret;
		$this->post_data['grant_type'] = $this->grant_type;
		$res_ = json_decode($this->make_auth_request(false));
		$this->access_token = $res_->access_token;
		$at = $this->access_token;
		$sql = "UPDATE telphin_params SET param_val='$at' WHERE param_name='access_token';";
		$db = $this->db_connect;		
		$result = $db->query($sql);
	}
		
	function get_records($y, $m, $d = false)
	{
		if ($d === false)
		{
			$d1 = "01";
			$a_date = "$y-$m-10";
			$d2 = date("t", strtotime($a_date));
		}
		else
		{
			$d = 0 + $d;
			if ($d < 10)
				$d = "0$d";
			$d1 = $d;
			$d2 = $d;
		}
		
		
		$m = 0 + $m;
		if ($m < 10)
			$m = "0$m";
		
		$y = 0 + $y;
		if ($y < 100)
			$y = "20$y";
						   //https://apiproxy.telphin.ru:443/api/ver1.0/client/%40me/record/?start_datetime=2018-06-01%2000%3A00%3A00&end_datetime=2018-06-30%2023%3A59%3A59&order=asc
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/record/?";
		$this->REQ_HOST .= "start_datetime=$y-$m-$d1%2000%3A00%3A00&";
		$this->REQ_HOST .= "order=asc&";
		$this->REQ_HOST .= "end_datetime=$y-$m-$d2%2023%3A59%3A58&";
		$this->REQ_HOST .= "per_page=100000&page=1";
		
		$res_ = json_decode($this->make_request(false));
		$res2 = $res_;
		//print_r($res_);
		//die();
		
		//$calls = $res_->calls;
		//print_r(count($res_) . "\n");
		
		$c = 0;
		if (is_array($res_) || is_object($res_))
		{
			foreach($res_ as $call)
			{
				$c++;
				//if ($c % 100 == 0)
				//	print_r($c . "\n");
				
				
				
				//print_r($call);
				//die();
				$res = array();
				$res['status'] = 'ok';
				try{
					$call_uuid = $call->call_uuid;
					$record_uuid = $call->record_uuid;
					$sql = "UPDATE calls_history SET record_uuid='$record_uuid' WHERE call_uuid='$call_uuid';";
					if ($result = $this->db_connect->query($sql))
					{
						if ($this->db_connect->affected_rows)
						{
							$res['status'] = 'ok';
						}
					}
					else
					{
						$res['status'] = 'failed';
						$res['sql'] = $sql;
						$res['msg'] = "2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
					}
				}
				catch (Exception $e) {
					echo '<font color="red">Исправьте ошибку в данных1!</font>';
					print_r($res_);
					print_r($call);
				}
				
				//print_r(json_encode($res,JSON_UNESCAPED_UNICODE) . "\n");
				//if ($res['status'] == 'failed')
					//die();
			}
		}
		else
		{
			print_r('foreach($res_ as $call) : 159 :');
			print_r($res_);
		}
		return $res2;
	}
	
	function get_records2($y, $m, $d = false)
	{
		if ($d === false)
		{
			$d1 = "01";
			$a_date = "$y-$m-10";
			$d2 = date("t", strtotime($a_date));
		}
		else
		{
			$d = 0 + $d;
			if ($d < 10)
				$d = "0$d";
			$d1 = $d;
			$d2 = $d;
		}
		
		
		$m = 0 + $m;
		if ($m < 10)
			$m = "0$m";
		
		$y = 0 + $y;
		if ($y < 100)
			$y = "20$y";
						   //https://apiproxy.telphin.ru:443/api/ver1.0/client/%40me/record/?start_datetime=2018-06-01%2000%3A00%3A00&end_datetime=2018-06-30%2023%3A59%3A59&order=asc
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/record/?";
		$this->REQ_HOST .= "start_datetime=$y-$m-$d1%2000%3A00%3A00&";
		$this->REQ_HOST .= "order=asc&";
		$this->REQ_HOST .= "end_datetime=$y-$m-$d2%2023%3A59%3A58&";
		$this->REQ_HOST .= "per_page=100000&page=1";
		
		$res_ = json_decode($this->make_request(false));
		$res2 = $res_;
		//print_r($res_);
		//die();
		
		//$calls = $res_->calls;
		//print_r(count($res_) . "\n");
		
		$c = 0;
		foreach($res_ as $call)
		{
			$c++;
			//if ($c % 100 == 0)
			//	print_r($c . "\n");
			
			
			
			//print_r($call);
			//die();
			$res = array();
			$res['status'] = 'ok';
			$call_uuid = $call->call_uuid;
			$record_uuid = $call->record_uuid;
			$sql = "UPDATE calls_history SET record_uuid='$record_uuid' WHERE call_uuid='$call_uuid';";
			
			print_r($sql . "\n");
			if ($result = $this->db_connect->query($sql))
			{
				if ($this->db_connect->affected_rows)
				{
					$res['status'] = 'ok';
				}
			}
			else
			{
				$res['status'] = 'failed';
				$res['sql'] = $sql;
				$res['msg'] = "2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
			}
			//print_r(json_encode($res,JSON_UNESCAPED_UNICODE) . "\n");
			//if ($res['status'] == 'failed')
				//die();
		}
		return $res2;
	}
	
	function get_records3($y, $m, $d = false)
	{
		if ($d === false)
		{
			$d1 = "01";
			$a_date = "$y-$m-10";
			$d2 = date("t", strtotime($a_date));
		}
		else
		{
			$d = 0 + $d;
			if ($d < 10)
				$d = "0$d";
			$d1 = $d;
			$d2 = $d;
		}
		
		
		$m = 0 + $m;
		if ($m < 10)
			$m = "0$m";
		
		$y = 0 + $y;
		if ($y < 100)
			$y = "20$y";
						   //https://apiproxy.telphin.ru:443/api/ver1.0/client/%40me/record/?start_datetime=2018-06-01%2000%3A00%3A00&end_datetime=2018-06-30%2023%3A59%3A59&order=asc
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/record/?";
		$this->REQ_HOST .= "start_datetime=$y-$m-$d1%2010%3A40%3A00&";
		$this->REQ_HOST .= "order=asc&";
		$this->REQ_HOST .= "end_datetime=$y-$m-$d2%2011%3A09%3A58&";
		$this->REQ_HOST .= "per_page=100000&page=1";
		
		$res_ = json_decode($this->make_request(false));
		$res2 = $res_;
		return $res2;
	}
	
	function get_record($record_uuid)
	{
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/record/$record_uuid/storage_url/";
		$res_ = json_decode($this->make_request(false));

		return $res_;
		//storage.telphin.ru/lq0xYBf1gsNve1PEY9IurG4SFeL4cZj854yyYZLxIncYMLiqcB6Fd7jgKfiKlppo
		//return $res_->record_url;
	}
	
	function get_calls($y, $m, $d1 = false, $d2 = false, $t1 = false)
	{
		if ($d1 === false)
		{
			$d1 = "01";
		}
		else
		{
			$d1 = 0 + $d1;
			if ($d1 < 10)
				$d1 = "0$d1";
		}
		
		if ($d2 === false)
		{
			$a_date = "$y-$m-10";
			$d2 = date("t", strtotime($a_date));
		}
		else
		{
			$d2 = 0 + $d2;
			if ($d2 < 10)
				$d2 = "0$d2";
		}
		
		if ($t1 === false)
		{
			$t1 = "%2000%3A00%3A00";
		}
		else
		{
			$tmpt = preg_replace("/\:/", "%3A", $t1);
			$t1 = "%20$tmpt";
		}
		$t2 = "%2023%3A59%3A59";
		
		$m = 0 + $m;
		if ($m < 10)
			$m = "0$m";
		
		$y = 0 + $y;
		if ($y < 100)
			$y = "20$y";
		//https://apiproxy.telphin.ru:443/api/ver1.0/client/9675/calls/?start_datetime=2018-09-14%2000%3A00%3A00&order=asc&end_datetime=2018-09-14%2023%3A59%3A58&per_page=100000&page=1
		$this->REQ_HOST = "https://apiproxy.telphin.ru:443/api/ver1.0/client/9675/calls/?";
		$this->REQ_HOST .= "start_datetime=$y-$m-$d1$t1&";
		$this->REQ_HOST .= "order=asc&";
		
		$ny = date("Y");
		$nm = date("m");
		$nd = date("d");
		
		if ((0 + $y) < (0 + $ny))
			$this->REQ_HOST .= "end_datetime=$ny-$nm-$nd$t2&";
		else if ((0 + $m) < (0 + $nm))
			$this->REQ_HOST .= "end_datetime=$ny-$nm-$nd$t2&";
		else if ((0 + $d2) < (0 + $nd))
			$this->REQ_HOST .= "end_datetime=$ny-$nm-$nd$t2&";
		else		
			$this->REQ_HOST .= "end_datetime=$y-$m-$d2$t2&";
		$this->REQ_HOST .= "per_page=100000&page=1";
		//print_r($this->make_request(false));
		
		//print_r($this->REQ_HOST);
		$res_ = json_decode($this->make_request(false));
		//print_r($res_);
		
		if (!is_array($res_) && !is_object($res_))
		{
			print_r('$calls = res_->calls : 377 :');
			print_r($res_);
		}
			
		try{
			$calls = $res_->calls;
		}
		catch(Exception $e) {
			echo '<font color="red">Исправьте ошибку в данных2!</font>';
			print_r($res_);
			//print_r($calls);
		}
		
		//print_r(count($calls));
		
		if (is_array($calls) || is_object($calls))
		{
			foreach($calls as $call)
			{
				$flow = $call->flow;
				$staff_id = 'NULL';
				if ($flow == 'in')
				{
					$client_phone = $call->from_username;
					//"bridged_username": "16574*105",
					if (preg_match("/\d*\*\d*/", $call->bridged_username))
					{
						$sp = preg_split("/\*/", $call->bridged_username);
						$staff_phone = $sp[1];
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
					else
						$staff_phone = $call->bridged_username;
					
				}
				else if ($flow == 'out')
				{
					$client_phone = $call->to_username;
					$staff_phone = $call->from_screen_name;
					//$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					
					if (preg_match("/\d*\*\d*/", $staff_phone))
					{
						$sp = preg_split("/\*/", $staff_phone);
						$staff_phone = $sp[1];
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
					else
					{
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
				}
				
				if (!array_key_exists($staff_phone, $this->staff_data))
					$staff_id = 'NULL';
				else
					$staff_id = "'" . $this->staff_data[$staff_phone] . "'";
				
				$call_uuid = $call->call_uuid;
				$result = $call->result;
				$duration = $call->duration;
				$init_time_gmt = $call->init_time_gmt;
				$hangup_time_gmt = $call->hangup_time_gmt;
				$hangup_cause = $call->hangup_cause;
				$hangup_disposition = $call->hangup_disposition;
				$client_phone = preg_replace("/[^0-9]/", '', $client_phone);
				
				
				
				$res = array();
				$sql = "INSERT INTO calls_history(staff_id, staff_phone, client_phone, call_uuid, flow, init_time_gmt, duration, hangup_time_gmt, result, hangup_cause, hangup_disposition) ";
				$sql .= " VALUES($staff_id,'$staff_phone','$client_phone','$call_uuid','$flow','$init_time_gmt','$duration','$hangup_time_gmt','$result','$hangup_cause','$hangup_disposition');";
				if ($result = $this->db_connect->query($sql))
				{
					if ($this->db_connect->affected_rows)
					{
						$res['status'] = 'ok';
					}
					else
					{
						$res['status'] = 'failed';
						$res['msg'] = $sql;
					}
				}
				else
				{
					$res['status'] = 'failed';
					$res['sql'] = $sql;
					$res['msg'] = "2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
				}
				//print_r(json_encode($res,JSON_UNESCAPED_UNICODE) . " $init_time_gmt\n");
			}
		}
		else
		{
			print_r('foreach($calls as $call) : 377 :');
			print_r($res_);
		}
	}
	
	function get_calls2($y, $m, $d1 = false, $d2 = false, $t1 = false)
	{
		if ($d1 === false)
		{
			$d1 = "01";
		}
		else
		{
			$d1 = 0 + $d1;
			if ($d1 < 10)
				$d1 = "0$d1";
		}
		
		if ($d2 === false)
		{
			$a_date = "$y-$m-10";
			$d2 = date("t", strtotime($a_date));
		}
		else
		{
			$d2 = 0 + $d2;
			if ($d2 < 10)
				$d2 = "0$d2";
		}
		
		if ($t1 === false)
		{
			$t1 = "%2012%3A05%3A00";
		}
		else
		{
			$tmpt = preg_replace("/\:/", "%3A", $t1);
			$t1 = "%20$tmpt";
		}
		$t2 = "%2012%3A10%3A59";
		
		$m = 0 + $m;
		if ($m < 10)
			$m = "0$m";
		
		$y = 0 + $y;
		if ($y < 100)
			$y = "20$y";
		//https://apiproxy.telphin.ru:443/api/ver1.0/client/9675/calls/?start_datetime=2018-09-14%2000%3A00%3A00&order=asc&end_datetime=2018-09-14%2023%3A59%3A58&per_page=100000&page=1
		$this->REQ_HOST = "https://apiproxy.telphin.ru:443/api/ver1.0/client/9675/calls/?";
		$this->REQ_HOST .= "start_datetime=$y-$m-$d1$t1&";
		$this->REQ_HOST .= "order=asc&";
		
		$ny = date("Y");
		$nm = date("m");
		$nd = date("d");
				
		$this->REQ_HOST .= "end_datetime=$y-$nm-$nd$t2&";
		$this->REQ_HOST .= "per_page=100000&page=1";
		//print_r($this->make_request(false));
		
		//print_r($this->REQ_HOST);
		$res_ = json_decode($this->make_request(false));
		//return $res_;
		if (!is_array($res_) && !is_object($res_))
		{
			print_r('$calls = res_->calls : 377 :');
			print_r($res_);
		}
			
		try{
			$calls = $res_->calls;
		}
		catch(Exception $e) {
			echo '<font color="red">Исправьте ошибку в данных2!</font>';
			print_r($res_);
			//print_r($calls);
		}
		
		//print_r(count($calls));
		
		if (is_array($calls) || is_object($calls))
		{
			foreach($calls as $call)
			{
				$flow = $call->flow;
				print_r($call);
				echo "\n";
				$staff_id = 'NULL';
				if ($flow == 'in')
				{
					$client_phone = $call->from_username;
					//"bridged_username": "16574*105",
					if (preg_match("/\d*\*\d*/", $call->bridged_username))
					{
						$sp = preg_split("/\*/", $call->bridged_username);
						$staff_phone = $sp[1];
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
					else
						$staff_phone = $call->bridged_username;
					
				}
				else if ($flow == 'out')
				{
					$client_phone = $call->to_username;
					$staff_phone = $call->from_screen_name;
					//$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					
					if (preg_match("/\d*\*\d*/", $staff_phone))
					{
						$sp = preg_split("/\*/", $staff_phone);
						$staff_phone = $sp[1];
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
					else
					{
						$staff_phone = preg_replace("/[^0-9]/", '', $staff_phone);
					}
				}
				
				if (!array_key_exists($staff_phone, $this->staff_data))
					$staff_id = 'NULL';
				else
					$staff_id = "'" . $this->staff_data[$staff_phone] . "'";
				
				
				
				
				$call_uuid = $call->call_uuid;
				$result = $call->result;
				$duration = $call->duration;
				$init_time_gmt = $call->init_time_gmt;
				$hangup_time_gmt = $call->hangup_time_gmt;
				$hangup_cause = $call->hangup_cause;
				$hangup_disposition = $call->hangup_disposition;
				$client_phone = preg_replace("/[^0-9]/", '', $client_phone);
				
				
				
				$res = array();
				$sql = "INSERT INTO calls_history(staff_id, staff_phone, client_phone, call_uuid, flow, init_time_gmt, duration, hangup_time_gmt, result, hangup_cause, hangup_disposition) ";
				$sql .= " VALUES($staff_id,'$staff_phone','$client_phone','$call_uuid','$flow','$init_time_gmt','$duration','$hangup_time_gmt','$result','$hangup_cause','$hangup_disposition');";
				
				print_r($sql);
				echo "\n";
				
				/*if ($result = $this->db_connect->query($sql))
				{
					if ($this->db_connect->affected_rows)
					{
						$res['status'] = 'ok';
					}
					else
					{
						$res['status'] = 'failed';
						$res['msg'] = $sql;
					}
				}
				else
				{
					$res['status'] = 'failed';
					$res['sql'] = $sql;
					$res['msg'] = "2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error;
				}*/
				//print_r(json_encode($res,JSON_UNESCAPED_UNICODE) . " $init_time_gmt\n");
			}
		}
		else
		{
			print_r('foreach($calls as $call) : 377 :');
			print_r($res_);
		}
	}
	
	
	function update_calls()
	{
		$hit = false;
		$init_time_gmt = '';
		$sql = "SELECT call_uuid, init_time_gmt FROM calls_history WHERE id=(select max(id) from calls_history);";
		$db = $this->db_connect;		
		if ($result = $db->query($sql))
		{
			if ($result->num_rows)
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					//print_r($row);
					$init_time_gmt = $row['init_time_gmt'];
					$hit = true;
				}
					
		}
		else
		{
			print_r("2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error);
		}
		
		if ($hit)
		{
			$d = date_parse_from_format("Y-m-d H:i:s", $init_time_gmt);
			$h = (0 + $d['hour'] < 10)   ? ("0" . $d['hour']) : $d['hour'];
			$m = (0 + $d['minute'] < 10) ? ("0" . $d['minute']) : $d['minute'];
			$s = (0 + $d['second'] < 10) ? ("0" . $d['second']) : $d['second'];
			$t = "$h:$m:$s";
			//print_r($t);
			$this->get_calls($d['year'], $d['month'], $d['day'], date("d"), $t);
			//print_r($init_time_gmt);
			//print_r(date_parse_from_format("Y-m-d H:i:s", $init_time_gmt));
			//$tdiff = date("Y-m-d H:i:s", mktime(date('H'), date('i')-5, date('s'), date('m'), date('d'), date('Y')));
		}
		
	}
	
	
	function update_calls2()
	{
		$hit = false;
		$init_time_gmt = '';
		$sql = "SELECT call_uuid, init_time_gmt FROM calls_history WHERE id=(select max(id) from calls_history);";
		$db = $this->db_connect;		
		if ($result = $db->query($sql))
		{
			if ($result->num_rows)
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					//print_r($row);
					$init_time_gmt = $row['init_time_gmt'];
					$hit = true;
				}
					
		}
		else
		{
			print_r("2: Не удалось выполнить запрос: (" . $this->db_connect->errno . ") " . $this->db_connect->error);
		}
		
		if ($hit)
		{
			$d = date_parse_from_format("Y-m-d H:i:s", $init_time_gmt);
			$h = (0 + $d['hour'] < 10)   ? ("0" . $d['hour']) : $d['hour'];
			$m = (0 + $d['minute'] < 10) ? ("0" . $d['minute']) : $d['minute'];
			$s = (0 + $d['second'] < 10) ? ("0" . $d['second']) : $d['second'];
			$t = "$h:$m:$s";
			//print_r($t);
			$this->get_calls($d['year'], $d['month'], $d['day'], date("d"));
			//$this->get_calls($d['year'], $d['month'], $d['day'], date("d"), $t);
			//print_r($init_time_gmt);
			//print_r(date_parse_from_format("Y-m-d H:i:s", $init_time_gmt));
			//$tdiff = date("Y-m-d H:i:s", mktime(date('H'), date('i')-5, date('s'), date('m'), date('d'), date('Y')));
		}
		
	}
	
	function call_history()
	{
		//$this->REQ_URI = "/client/@me/client/";
		//$this->REQ_URI = ":443/api/ver1.0/client/%40me/limit/";
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/call_history/";
		print_r($this->make_request(false));
		
	}
		
	function client()
	{
		//$this->REQ_URI = "/client/@me/client/";
		//$this->REQ_URI = ":443/api/ver1.0/client/%40me/limit/";
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/client/%40me/client/";
		print_r($this->make_request(false));
		
	}
	
	function user()
	{
		//$this->REQ_URI = "/client/@me/client/";
		//$this->REQ_URI = ":443/api/ver1.0/client/%40me/limit/";
		$this->REQ_HOST = "https://apiproxy.telphin.ru/api/ver1.0/user/";
		print_r($this->make_request(false));
		
	}
	
	function limit()
	{
		//$this->REQ_URI = "/client/@me/client/";
		//$this->REQ_URI = ":443/api/ver1.0/client/%40me/limit/";
		$this->REQ_HOST = "https://apiproxy.telphin.ru:443/api/ver1.0/client/%40me/limit/";
		print_r($this->make_request(false));
		
	}
	
	function make_request($post)
	{
		if( $curl = curl_init() ) 
		{
			$authorization = "Authorization: Bearer " . $this->access_token;
			curl_setopt($curl, CURLOPT_URL, $this->REQ_HOST.$this->REQ_URI);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, $post);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', $authorization));
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
			//curl_setopt($curl, CURLINFO_HEADER_OUT, true);
			//curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			//curl_setopt($curl, CURLOPT_POSTFIELDS, "KEY=$this->key&FORMAT=$this->format&ACTION=$this->action$this->class$this->data$this->sort");
			if ($post)
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->post_data));
			$out = curl_exec($curl);
			//print_r(curl_getinfo($curl));
			curl_close($curl);
			
			$relogin = false;
			$out_a = json_decode($out);
			print_r($out_a);
			if (isset($out_a->status))
			{
				if ($out_a->status == '401')
				{
					$this->update_token();
					$out = $this->make_request($post);
				}
			}
			
			return $out;
		}
		
	
	}

    function make_requestt($post)
    {
        if( $curl = curl_init() )
        {
            $authorization = "Authorization: Bearer " . $this->access_token;
            curl_setopt($curl, CURLOPT_URL, $this->REQ_HOST.$this->REQ_URI);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, $post);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', $authorization));
            //curl_setopt($curl, CURLINFO_HEADER_OUT, true);
            //curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            //curl_setopt($curl, CURLOPT_POSTFIELDS, "KEY=$this->key&FORMAT=$this->format&ACTION=$this->action$this->class$this->data$this->sort");
            if ($post)
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->post_data));
            $out = curl_exec($curl);

            echo "<pre>";
            print_r(curl_getinfo($curl));
            curl_close($curl);
            echo "</pre>";
            $relogin = false;
            $out_a = json_decode($out);
            if (isset($out_a->status))
            {
                if ($out_a->status == '401')
                {
                    $this->update_token();
                    $out = $this->make_request($post);
                }
            }

            return $out;
        }


    }
	
	function make_auth_request()
	{
		if( $curl = curl_init() ) 
		{
			curl_setopt($curl, CURLOPT_URL, $this->REQ_HOST_TOKEN.$this->REQ_URL_TOKEN);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			//curl_setopt($curl, CURLOPT_POSTFIELDS, "KEY=$this->key&FORMAT=$this->format&ACTION=$this->action$this->class$this->data$this->sort");
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->post_data));
			$out = curl_exec($curl);
			curl_close($curl);
			return $out;
		}
	
	}
}