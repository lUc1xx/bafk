<?php
class cFinScoring
{
	protected $endpoint = "https://api.id-x.org/idx/api2/";
	protected $accessKey ='kreditmart-268801d76dc79139bba58b23f7db29b4b59eb340';
	protected $secretKey = 'e5fbbb649c75fbd6d97ff4d7250ed1b6a2e31e46';
	protected $format='json';	//php, xml, json
	protected $post_data = array();
	protected $db;
	
	public function __construct($db_)
	{
		$this->db = $db_;
	}

	
	function verifyPhone($data)
	{
		$this->post_data = array();
		if (isset($data['lastname']))
			$this->post_data['personLastName'] = $data['lastname'];
		if (isset($data['firstname']))
			$this->post_data['personFirstName'] = $data['firstname'];
		if (isset($data['middlename']))
			$this->post_data['personMidName'] = $data['middlename'];
		if (isset($data['birth_day']))
			$this->post_data['personBirthDate'] = $data['birth_day'];
		if (isset($data['contact_phone']))
			$this->post_data['phone'] = $data['contact_phone'];
		
		
		return $this->make_request('verifyPhone');
	}
	
	function getFinScoring($data)
	{
		$this->post_data = array();
		if (isset($data['lastname']))
			$this->post_data['lastName'] = $data['lastname'];
		if (isset($data['firstname']))
			$this->post_data['firstName'] = $data['firstname'];
		if (isset($data['middlename']))
			$this->post_data['midName'] = $data['middlename'];
		if (isset($data['birth_day']))
			$this->post_data['birthDate'] = $data['birth_day'];
		if (isset($data['field18']) && isset($data['field19']))
			$this->post_data['passportNumber'] = $data['field18'].$data['field19'];
		if (isset($data['field21']))
			$this->post_data['passportDate'] = $data['field21'];
		
		
		return $this->make_request('finScoring');
	}
	
	function checkFssp($data)
	{
		$this->post_data = array();
		if (isset($data['lastname']))
			$this->post_data['lastName'] = $data['lastname'];
		if (isset($data['firstname']))
			$this->post_data['firstName'] = $data['firstname'];
		if (isset($data['middlename']))
			$this->post_data['midName'] = $data['middlename'];
		if (isset($data['birth_day']))
			$this->post_data['birthDate'] = $data['birth_day'];
		return $this->make_request('checkFssp');
	}
		
	function make_request($req)
	{
		$this->post_data['accessKey'] = $this->accessKey;
		$this->post_data['secretKey'] = $this->secretKey;
		if( $curl = curl_init() ) 
		{
			curl_setopt($curl, CURLOPT_URL, $this->endpoint.$req);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			//curl_setopt($curl, CURLOPT_POSTFIELDS, "KEY=$this->key&FORMAT=$this->format&ACTION=$this->action$this->class$this->data$this->sort");
			//curl_setopt($curl, CURLOPT_POSTFIELDS, $this->post_data);
			//curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->post_data));
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->post_data));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
			$out = curl_exec($curl);
			curl_close($curl);
			
			$fp = fopen(LOGS_DIR . "verifyPhone.log", "a+");
			$data_ = date("Y-m-d H:i:s", time());
			$data_ .= print_r($req, true) . " => ";
			$data_ .= print_r($this->endpoint.$req, true) . " => ";
			$data_ .= print_r(unicode_decode($out), true) . " => ";
			$data_ .= "\n" . print_r($this->post_data, true) . "\n";
			$test = fwrite($fp, $data_);
			fclose($fp);
		
			return $out;
		}
		else return false;
	}
}