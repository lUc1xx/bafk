<?php
class cFirebase
{
	protected $url = 'https://fcm.googleapis.com/fcm/send';
	protected $YOUR_API_KEY = 'AAAAsmxtdmA:APA91bHZFkueidh6DB_1ZqjNN0IdImSZ7S5l2F1rM00O_2T4DTL0G18iGnwxbZJsED7m7y83UGLwQEouE5FpKJy4IUaERn6Mkt0dpkaqMd6S0tRQmd6Pc1BmKvDwBauyDl0_sumRlPDU';
	protected $request_body = array();
	protected $input_data = array();
	protected $db;
	
	public function __construct($db_)
	{
		$this->db = $db_;
	}

	
	function send_msg($id, $data)
	{
		$data['stid'] = $id;
		if ($result = $this->db->query("SELECT firebase_token, lastname FROM staff WHERE id='$id' AND firebase_token IS NOT NULL;"))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$tokens = explode('<br>', $row['firebase_token']);
				$data['lastname'] = $row['lastname'];
				
				foreach ($tokens as $token)
				{
					if ($token == '')
						continue;
					
					$this->push_data($token, $data);
					usleep(200000);
				}
			}			
			$result->close();
		}
	}
	
	function push_data($token, $data)
	{
		$icon = 'https://b2cbafk.club/img/anketa.png';
		switch($data['action'])
		{
			case "za_start" 	:
			case "wpc_start" 	:
			case "new_form" 	: $action = 'https://b2cbafk.club/details/' . $data['form_id'] . '/'; break;
			case "login" 		: $action = 'https://b2cbafk.club/staf/'; break;
			case "wp_conf" 		: 
				$action = 'https://b2cbafk.club/details/' . $data['form_id'] . '/';
				$icon = 'https://b2cbafk.club/img/Accept.png';
				break;
			case "wp_reject" 	: 
				$action = 'https://b2cbafk.club/details/' . $data['form_id'] . '/';
				$icon = 'https://b2cbafk.club/img/Block.png';
				break;
			case "za_end" 		: 
			{
				$action = 'https://b2cbafk.club/details/' . $data['form_id'] . '/';
				if (preg_match("/Одобрено/", $data['st']))
					$icon = 'https://b2cbafk.club/img/Accept.png';
				else
					$icon = 'https://b2cbafk.club/img/Block.png';
				break;
			}
			default 			: $action = 'https://b2cbafk.club/list/'; break;
		}
		
		$this->request_body = array();
		$this->input_data = $data;
		$this->request_body = [
			'to' => $token,
			'notification' => [
				'title' => $data['title'],
				'body' => $data['body'],
				'icon' => $icon,
				'requireInteraction' => true,
				'click_action' => $action,
			],
		];
		return $this->make_request($token);
	}
		
	function make_request($token)
	{
		$fields = json_encode($this->request_body);

		$request_headers = [
			'Content-Type: application/json',
			'Authorization: key=' . $this->YOUR_API_KEY,
		];
				
		
		if( $curl = curl_init() ) 
		{
			curl_setopt($curl, CURLOPT_URL, $this->url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			$out = curl_exec($curl);
			curl_close($curl);
			
			$fp = fopen(LOGS_DIR . "firebase.log", "a+");
			$data_ = date("Y-m-d H:i:s", time()) . "\n";
			$data_ .= print_r($token, true) . "\n";
			$data_ .= print_r($this->input_data, true) . "\n";
			$data_ .= print_r(unicode_decode($out), true) . "\n\n";
			$test = fwrite($fp, $data_);
			fclose($fp);
		
			return $out;
		}
		else return false;
	}
}