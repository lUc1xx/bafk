<?php
//error_reporting(E_ALL);
if (!defined('SYSTEM_START_9876543210')) exit; 

$show_f = false;
?>



<div onclick="show('none')" id="wrap"></div>
<div id="window">
	<img class="close" onclick="show('none')" src="/new_products/close.png">
<div id="prod_detail" style="position:fixed; height:530px; width:1100px; margin-top:50px; overflow: auto;"></div>
</div>




<div id="tabs">
<span id="head_1">Предварительные решения банков для для анкеты <b><?=$_id;?></b>:<br>
<button type="button" onclick="import_pod(<?=$_id;?>);">Импорт из анкеты</button>
<button type="button" onclick="save_pod(<?=$_id;?>);">Сохранить решение</button>
<button type="button" onclick="load_pod(<?=$_id;?>);">Загрузить решение</button>
<br>
<?
//<div id="msg_4_staff3"></div>
?>
<br>
</span>
<form action="" method="post" id="pr_form">
<input name="act" type="hidden" value="get_prod">
<input name="find_results" type="hidden" value="0">
<input name="fill_reds" type="hidden" value="0">
<input name="anketa_id" type="hidden" value="<?=$_id;?>">
<div class="menu1">
  <br id="tab2"/><br id="tab3"/><br id="tab4"/><br id="tab5"/><br id="tab6"/><br id="tab7"/>
  <a href="#tab1">Основные</a>
  <a href="#tab2">Трудоустройство и активы</a>
  <a href="#tab3">Обеспечение</a>
  <a href="#tab4" id="not_show_u" class="not_show_u">Юрлицо</a>
  <a href="#tab5" id="not_show_c">Кредитная история</a>
  <a href="#tab6" id="not_show_s">Стоп-факторы</a>
  <a href="#tab7"><span id="res_num">Результаты</span> <span id="msg_4_staff3"></span></a>





<div id="tab_1_css">

<span id="goal_text">Цель кредита:
<label class = "tab2" ><input form="pr_form" name="goal[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Нецелевой</label>
<label class = "tab3"><input form="pr_form" name="goal[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Ипотека</label>
<label class = "tab4"><input form="pr_form" name="goal[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Гарантия</label>
<label class = "tab5"><input form="pr_form" name="goal[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Лизинг</label>
<label class = "tab6"><input form="pr_form" name="goal[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Рефинансирование</label>
<label class = "tab8"><input form="pr_form" name="goal[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Карты</label><br>
</span>
<label><input class="tab3" form="pr_form" id="cr_his" name="cr_his" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"><span id="cr_his_text">Наличие кредитной истории</span></label><br>
<label><input class="tab3" form="pr_form" id="res" name="res" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"><span id="res_text">Резидент?</span></label><br>

<span id="credit_summ_text">Сумма кредита, р :</span> <input class="tab3" form="pr_form" name="credit_summ" type="text" placeholder="Сумма, р" size="15" value="" oninput="add_space(this); set_value(this.value, this.name, 0); show_products();">
<label><input class="tab8" form="pr_form" name="client" id="client" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"><span id="client_text" class="tab5">Клиент банка-партнера</span> </label> <span class = "tab8c2">(Физ/Юрлицо/ИП)</span><br>

<label><input class="tab3" form="pr_form" id="income" name="income" type="text" placeholder="Доход, р" size="15" value="" oninput="add_space(this); set_value(this.value, this.name, 0); show_products();"><span id="income_text">Доход, р : </span></label>
<label id="k_h_miss_text" class="tab5"><b>Не анализировать кредитную историю!</b><input class="tab3" form="pr_form" name="k_h_miss" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>

<span id="inc_4_cre_text">Максимальный платеж, р : </span><input class="tab3" form="pr_form" id="inc_4_cre" name="inc_4_cre" type="text" placeholder="Рублей" size="15" value="" oninput="add_space(this); set_value(this.value, this.name, 0); show_products();">
<label id="delay_act_text" class="tab5"><b>Есть текущая просрочка</b><input class="tab3" form="pr_form" name="delay_act" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>

Доход супруги/супруга <input class="tab3" form="pr_form" name="a_ds" type="text" size="15" onchange="set_value(this.value, this.name, 0); show_products();">
<label id="ent_text" class="tab5"><b>Участие юрлица в сделке в качестве</b></label>
<label class = "tab8"><input class="inp" form="pr_form" name="ent[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">заемщика</label>
<label class = "tab9"><input class="inp" form="pr_form" name="ent[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">поручителя</label>
<label class = "tab10_2"><input class="inp" form="pr_form" name="ent[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">залогодателя</label>
<br>


<label><input class="tab3" form="pr_form" id="a_izdeti" name="a_izdeti" type="text" size="15" onchange="set_value(this.value, this.name, 0); show_products();"><span id="a_izdeti_text">Количество детей</span></label><br>
<span id="a_skred_text">Кредитные обязательства</span> <input class="tab3" form="pr_form" id="a_skred" name="a_skred" type="text" size="15" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<?
//Прожиточный минимум <input class="tab4" form="pr_form" name="pm" type="text" disabled size="15" value="15000" onchange="set_value(this.value, this.name, 0); show_products();"><br>
?>
<span id="bd_a_text">Дата рождения:</span><input class="tab3" form="pr_form" name="bd_a" id="bd_a" type="text" placeholder="через точки" size="10" value="<?
//echo "$a_drd.$a_drm.$a_dry";
?>" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Возраст : <input class="tab3" form="pr_form" name="age" disabled type="text" placeholder="Лет" size="5" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<span id="sex_text">Пол :</span>
	<select class="tab3" form="pr_form" name="sex" style="color:red;" onchange="javascript: this.style.color = (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
		<option value="-1">Выберите</option>
		<option value="0" >Мужской</option>
		<option value="1" >Женский</option>
	</select><br>
Желаемая ставка, % : <input class="tab3" form="pr_form" name="cr_percent" type="text" placeholder="Целое число" size="10" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Первонач. взнос по ИПОТЕКЕ, % <input class="tab3" form="pr_form" name="first_deb" type="text" placeholder="%" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Крайний срок, дней : <input class="tab3" form="pr_form" name="lim_time" type="text" placeholder="крайний срок, дней" size="25" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<span id="arm_text">Армия (для мужчин)</span>
<select class="tab3" form="pr_form" name="arm" style="color:red;" onchange="javascript: this.style.color = (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">Военный билет</option>
	<option value="1">Старше призывного</option>
	<option value="2">Отсрочка</option>
	<option value="3">Подлежит призыву</option>
	<option value="4">НЕ подлежит призыву</option>
	<option value="5">Военнослужащий</option>
</select><br>
<span id="adr_text">Прописка:</span>
<select class="tab3" form="pr_form" name="adr" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">Москва</option>
	<option value="1">Московская обл</option>
	<option value="2">Иной регион</option>
</select><br>
Cрок прописки, мес : <input class="tab3" form="pr_form" name="adr_time" type="text" placeholder="Cрок прописки, мес" size="25" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Скоринговый бал НБКИ: <input class="tab3" form="pr_form" name="nbki" type="text" size="25" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<?
/*<select class="tab" form="pr_form" name="nbki" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">0-250</option>
	<option value="1">251-500</option>
	<option value="2">501-750</option>
	<option value="3">751-999</option>
</select><br>*/
?>



</div>

<div id="tab_2_css">

Активы
<label class="tab2"><input form="pr_form" name="activs[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Птс на автомобиль</label>
<label class="tab5"><input form="pr_form" name="activs[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Документы на квартиру</label>
<label class="tab8"><input form="pr_form" name="activs[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Нет активов</label><br>
<label class="tab2"><input form="pr_form" name="activs[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Иное</label>
<label class="tab5"><input form="pr_form" name="activs[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Документы на дом</label>
<label class="tab8"><input form="pr_form" name="activs[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Док-ты на коммерческую недвижимость</label><br>

<label>Производятся отчисления в ПФР<input class="tab4" form="pr_form" name="pfr" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>
Трудоустройство:
<select class="tab4" form="pr_form" name="work" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="5">Госслужащий</option>
	<option value="9">Владелец АО</option>
	<option value="7">По найму в АО</option>
	<option value="11">По срочному контракту</option>
	<option value="4">Пенсионер</option>
	<option value="8">В НКО</option>
	<option value="2">ИП</option>
	<option value="1">Владелец бизнеса (ООО)</option>
	<option value="0">По найму в ООО/ИП</option>
	<option value="10">Частная практика</option>
	<option value="3">Неофициально</option>
	<option value="6">Отсутствует</option>
</select><br>



Подтверждение трудоустройства:
<select class="tab4" form="pr_form" name="c_work" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">Книжка</option>
	<option value="1">Договор</option>
	<option value="2">Свидетельство</option>
	<option value="3">По телефону</option>
	<option value="4">Нет</option>
	<option value="5">Договор аренды помещения</option>
</select><br>


Подтверждение дохода:
<select class="tab4" form="pr_form" name="c_inc" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">ф.банка</option>
	<option value="1">2ндфл</option>
	<option value="2">3ндфл</option>
	<option value="3">декларация</option>
	<option value="4">патент</option>
	<option value="5">договор</option>
	<option value="6">Нет</option>
	<option value="7">Выписка по личному счету</option>
</select><br>


Стаж работы на текущем месте, мес
<select class="tab4" form="pr_form" name="exp_now" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">менее 3</option>
	<option value="1">3-6</option>
	<option value="2">7-12</option>
	<option value="3">13-24</option>
	<option value="4">25-36</option>
	<option value="5">37 и более</option>
</select><br>

Дата регистрации предприятия, мес
<select class="tab4" form="pr_form" name="reg_date" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0" >менее 6</option>
	<option value="1" >6-12</option>
	<option value="2" >13-24</option>
	<option value="3" >25-36</option>
	<option value="4" >37 и более</option>
</select><br>

Сфера деятельности:
<select class="tab4" form="pr_form" name="work_type" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();" >
<option value="-1">Выберите</option>
<?

if ($result = $db_connect->query("SELECT * FROM work_types ORDER BY id ASC;"))
{
		
	while ($a = $result->fetch_array(MYSQLI_ASSOC))
	{
		$wid = $a["id"]-1;
		echo '<option value="'.$wid.'">'.$a["descr"].'</option>';
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
?>
</select><br>

Фактический адрес места работы: <select class="tab4" form="pr_form" name="work_adr" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0" >Москва</option>
	<option value="1" >Московская обл</option>
	<option value="2" >Иной регион</option>
	<option value="3" >Нет адреса</option>
</select> 


</div>



<div id="tab_3_css">
<?
/*
Поручитель 
<label class = "tab4"><input class="inp" form="pr_form" name="guarantor[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">физлицо</label>
<label class = "tab5"><input class="inp" form="pr_form" name="guarantor[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">юрлицо</label><br>
Залог от третьего лица
<label class = "tab4"><input class="inp" form="pr_form" name="bail[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">физлицо</label>
<label class = "tab5"><input class="inp" form="pr_form" name="bail[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">юрлицо</label><br>


Первоначальный взнос, % <input class="tab4" form="pr_form" name="first_deb" type="text" placeholder="%" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
*/
?>
Удаленность от мкад не более, км <input class="tab4" form="pr_form" name="mkad" type="text" placeholder="км" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Документы на право собственности:
<select class="tab4" form="pr_form" name="doc_sob" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">ДДУ</option>
	<option value="1">свидетельство</option>
	<option value="2">не стандарт</option>
	<option value="3">Выписка ЕГРП</option>
</select><br>
Владелец:
<select class="tab4" form="pr_form" name="owner" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">1 собственник</option>
	<option value="1">долевая собственность</option>
	<option value="2">юрлицо</option>
</select><br>
Вид залога:<br>
<label><input class="inp" form="pr_form" name="obj_type[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">дом</label>
<label class = "tab1"><input class="inp" form="pr_form" name="obj_type[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">коттедж</label>
<label class = "tab2"><input class="inp" form="pr_form" name="obj_type[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">таунхаус</label>
<label class = "tab3"><input class="inp" form="pr_form" name="obj_type[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">земля</label>
<label class = "tab4"><input class="inp" form="pr_form" name="obj_type[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">дачный дом</label>
<label class = "tab5"><input class="inp" form="pr_form" name="obj_type[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">квартира</label>
<label class = "tab6"><input class="inp" form="pr_form" name="obj_type[6]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">комната</label>
<label class = "tab7"><input class="inp" form="pr_form" name="obj_type[7]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">доля в квартире</label>
<br><label><input class="inp" form="pr_form" name="obj_type[8]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">помещение коммерческого назначения</label>
<label class = "tab3"><input class="inp" form="pr_form" name="obj_type[9]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">гараж</label>
<label class = "tab4"><input class="inp" form="pr_form" name="obj_type[10]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">автомобиль</label>
<label class = "tab5"><input class="inp" form="pr_form" name="obj_type[11]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">спецтехника</label>
<label class = "tab7"><input class="inp" form="pr_form" name="obj_type[12]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">прочее движимое имущество</label><br>
<label><input class="inp" form="pr_form" name="obj_type[13]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">оборудование</label>
<label class = "tab2"><input class="inp" form="pr_form" name="obj_type[14]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">товары</label><br>
<?
/*
<select class="tab4" form="pr_form" name="obj_type" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">дом</option>
	<option value="1">коттедж</option>
	<option value="2">таунхаус</option>
	<option value="3">земля</option>
	<option value="4">дачный дом</option>
	<option value="5">квартира</option>
	<option value="6">комната</option>
	<option value="7">доля в квартире</option>
	<option value="8">помещение коммерческого назначения</option>
	<option value="9">гараж</option>
</select><br>
*/
?>
Перепланировки:
<select class="tab4" form="pr_form" name="replan" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">нет</option>
	<option value="1">узаконена</option>
	<option value="2">не узаконена</option>
	<option value="3">невозможно узаконить</option>
</select><br>

Материал стен дома
<select class="tab4" form="pr_form" name="house_mat" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">цельное дерево</option>
	<option value="1">деревянный каркас</option>
	<option value="2">каркас из иного материала</option>
	<option value="3">кирпич</option>
	<option value="4">бетон</option>
	<option value="5">шлакоблоки</option>
	<option value="6">камень</option>
</select><br>

Фундамент дома
<select class="tab4" form="pr_form" name="found" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">нет</option>
	<option value="1">ленточный</option>
	<option value="2">свайный</option>
	<option value="3">монолитный</option>
</select><br>
Возраст постройки, полных лет <input class="tab4" form="pr_form" name="obj_age" type="text" placeholder="полных лет" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Документально установленный % износа <input class="tab4" form="pr_form" name="wear" type="text" placeholder="%" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>



Коммуникации:<br>
<label><input form="pr_form" name="comm[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">электричество</label>
<label class = "tab2"><input class="inp" form="pr_form" name="comm[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">вода</label>
<label class = "tab3"><input class="inp" form="pr_form" name="comm[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">отопление</label>
<label class = "tab4"><input class="inp" form="pr_form" name="comm[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">газ</label>
<label class = "tab5"><input class="inp" form="pr_form" name="comm[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">канализация</label>
<label class = "tab7"><input class="inp" form="pr_form" name="comm[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">нет</label>
<br>
Собственники:<br>
<span style="display:none;">
<label><input class="inp" form="pr_form" name="sob[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только 1 Совершеннолетний</label>
<label class = "tab3"><input class="inp" form="pr_form" name="sob[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только Совершеннолетние (2 и более)</label>
<label class = "tab6"><input class="inp" form="pr_form" name="sob[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Совершенн. + несовершеннолетние</label>
<label class = "tab9"><input class="inp" form="pr_form" name="sob[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">1 несовершеннолетний</label>
<br>
</span>
<label><input class="inp" form="pr_form" name="sob[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Несовершеннолетние (1 и более)</label>
<label class = "tab3"><input class="inp" form="pr_form" name="sob[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Физ.лицо -Нерезидент</label>
<label class = "tab6"><input class="inp" form="pr_form" name="sob[6]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Юр.лицо -Нерезидент</label>
<label class = "tab9" style="display:none;"><input class="inp" form="pr_form" name="sob[7]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">ПИФ</label>

<label  style="display:none;"><input class="inp" form="pr_form" name="sob[8]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">НКО</label>
<label class = "tab9"><input class="inp" form="pr_form" name="sob[9]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Пенсионер</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="sob[10]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 1-й гр</label>
<label class = "tab3"><input class="inp" form="pr_form" name="sob[11]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 2-й гр</label>

<label class = "tab6"><input class="inp" form="pr_form" name="sob[12]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 3-й гр</label>
<label class = "tab9"><input class="inp" form="pr_form" name="sob[13]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете ПНД</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="sob[14]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете НД</label>
<label class = "tab3"><input class="inp" form="pr_form" name="sob[15]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Банкрот/В процессе</label>

<label class = "tab6"><input class="inp" form="pr_form" name="sob[16]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Текущая просрочка</label>
<label class = "tab3" style="display:none;"><input class="inp" form="pr_form" name="sob[17]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">ФССП на сумму бол. 5% от ст-ти залога</label>
<label class = "tab6" style="display:none;"><input class="inp" form="pr_form" name="sob[18]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Отбывающий заключение</label>
<label class = "tab9" style="display:none;"><input class="inp" form="pr_form" name="sob[19]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Находящийся под следствием</label>

<br>


Зарегистрированные лица:<br>
<label class = "tab0" style="display:none;"><input class="inp" form="pr_form" name="zar[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только 1 Совершеннолетний</label>
<label class = "tab3" style="display:none;"><input class="inp" form="pr_form" name="zar[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только Совершеннолетние (2 и более)</label>
<label class = "tab0"><input class="inp" form="pr_form" name="zar[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Совершенн. + несовершеннолетние</label>
<label class = "tab3"><input class="inp" form="pr_form" name="zar[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Отказники от приватизации</label>

<label class = "tab6"><input class="inp" form="pr_form" name="zar[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Пенсионер</label>
<label class = "tab9"><input class="inp" form="pr_form" name="zar[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 1-й гр</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="zar[6]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 2-й гр</label>
<label class = "tab3"><input class="inp" form="pr_form" name="zar[7]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 3-й гр</label>

<label class = "tab6"><input class="inp" form="pr_form" name="zar[8]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете ПНД</label>
<label class = "tab9"><input class="inp" form="pr_form" name="zar[9]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете НД</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="zar[10]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Отбывающий заключение</label>
<label class = "tab3"><input class="inp" form="pr_form" name="zar[11]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Находящийся под следствием</label>

<br>
Временно зарегистрированные:<br>
<label class = "tab0" style="display:none;"><input class="inp" form="pr_form" name="vzar[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только 1 Совершеннолетний</label>
<label class = "tab3" style="display:none;"><input class="inp" form="pr_form" name="vzar[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Только Совершеннолетние (2 и более)</label>
<label class = "tab0"><input class="inp" form="pr_form" name="vzar[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Совершенн. + несовершеннолетние</label>
<label class = "tab3"><input class="inp" form="pr_form" name="vzar[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Отказники от приватизации</label>
<label class = "tab6"><input class="inp" form="pr_form" name="vzar[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Пенсионер</label>
<label class = "tab9"><input class="inp" form="pr_form" name="vzar[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 1-й гр</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="vzar[6]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 2-й гр</label>
<label class = "tab3"><input class="inp" form="pr_form" name="vzar[7]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Инвалид 3-й гр</label>
<label class = "tab6"><input class="inp" form="pr_form" name="vzar[8]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете ПНД</label>
<label class = "tab9"><input class="inp" form="pr_form" name="vzar[9]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">На учете НД</label>
<br>
<label class = "tab0"><input class="inp" form="pr_form" name="vzar[10]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Отбывающий заключение</label>
<label class = "tab3"><input class="inp" form="pr_form" name="vzar[11]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Находящийся под следствием</label>
<br>

</div>

<div id ="ur_pod">

<?/*
<select class="tab4" form="pr_form" name="ent" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">заемщика</option>
	<option value="1">поручителя</option>
	<option value="2">залогодателя</option>
</select><br>
*/
?>
<label>Имеется расчетный счет у ИП/юрлица<input class="tab4" form="pr_form" name="ca" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>
Доля в уставном капитале, %: <input class="tab4" form="pr_form" name="firm_per" type="text" placeholder="Доля в уставном капитале, %" size="25" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Система налогообложения:
<select class="tab4" form="pr_form" name="nal" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">обычная</option>
	<option value="1">усн6%</option>
	<option value="2">усн15%</option>
	<option value="3">патент для ИП</option>
	<option value="4">ЕНВД</option>
</select><br>

Общая выручка за последние 4 квартала, руб: <input class="tab4" form="pr_form" name="proc" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

Прибыль за последние 4 квартала по нал. отчетности:
<select class="tab4" form="pr_form" name="pr_nal" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">0</option>
	<option value="1">прибыль</option>
	<option value="2">убыток</option>
</select><br>
Прибыль реальная за последние 4 квартала, руб: <input class="tab4" form="pr_form" name="pr_4" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

Имущество организации в М/МО:<br>
<label><input class="inp" form="pr_form" name="est[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">здания</label>
<label class="tab1"><input class="inp" form="pr_form" name="est[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">помещения</label>
<label class="tab2"><input class="inp" form="pr_form" name="est[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">техника</label>
<label class="tab3"><input class="inp" form="pr_form" name="est[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">оборудование</label>
<label class="tab5"><input class="inp" form="pr_form" name="est[4]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">товары</label>
<label class="tab6"><input class="inp" form="pr_form" name="est[5]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">земля</label>
<label class="tab7"><input class="inp" form="pr_form" name="est[6]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">незавершенное строительство</label><br>
<label class="tab7"><input class="inp" form="pr_form" name="est[7]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">нет имущества</label><br>

Количество активных кредитных обязательств юрлица: <input class="tab4" form="pr_form" name="co_umax" type="text" placeholder="Всего" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Сумма действующих кредитных обязательств юрлица: <input class="tab4" form="pr_form" name="co" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<label>Есть текущая просрочка<input class="tab4" form="pr_form" name="delay_29" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>

Худший платежный статус по активным об. юрлица:
<select class="tab4" form="pr_form" name="ps" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select>
<label class="tab6">Худший платежный статус по закрытым об. юрлица:</label>
<select class="tab10" form="pr_form" name="psc" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select><br>
Количество просрочек от 1 до 29 : <input class="tab4" form="pr_form" name="ps_u0" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 1 до 29 :</label> <input class="tab10" form="pr_form" name="psc_u0" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 30 до 59 : <input class="tab4" form="pr_form" name="ps_u1" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 30 до 59 :</label> <input class="tab10" form="pr_form" name="psc_u1" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 60 до 89 : <input class="tab4" form="pr_form" name="ps_u2" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 60 до 89 :</label> <input class="tab10" form="pr_form" name="psc_u2" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 90 до 119 : <input class="tab4" form="pr_form" name="ps_u3" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 90 до 119 :</label> <input class="tab10" form="pr_form" name="psc_u3" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек более 120 : <input class="tab4" form="pr_form" name="ps_u4" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек более 120 :</label> <input class="tab10" form="pr_form" name="psc_u4" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

</div>


<div id = "cre_his">
Активных кредитных обязательств: <input class="tab4" form="pr_form" name="co_max" type="text" placeholder="Всего" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<label>Есть активные займы МФО (на суммы до 100 т.р)<input class="tab4" form="pr_form" name="zaim_mfo" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>

Общий лимит: <input class="tab4" form="pr_form" name="lim_all" type="text" placeholder="рублей" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Текущий баланс: <input class="tab4" form="pr_form" name="bal_now" type="text" placeholder="рублей" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

Наихудший платежный статус по закрытым:
<select class="tab4" form="pr_form" name="ps_c" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select>
<label class="tab6">Наихудший платежный статус за 6 месяцев:</label>
<select class="tab10" form="pr_form" name="ps_c_6" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select>
<br>
Количество просрочек от 1 до 29 : <input class="tab4" form="pr_form" name="ps_v0" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 1 до 29 : </label><input class="tab10" form="pr_form" name="ps_v0_6" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 30 до 59 : <input class="tab4" form="pr_form" name="ps_v1" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 30 до 59 : </label><input class="tab10" form="pr_form" name="ps_v1_6" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 60 до 89 : <input class="tab4" form="pr_form" name="ps_v2" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 60 до 89 : </label><input class="tab10" form="pr_form" name="ps_v2_6" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 90 до 119 : <input class="tab4" form="pr_form" name="ps_v3" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 90 до 119 : </label><input class="tab10" form="pr_form" name="ps_v3_6" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек более 120 : <input class="tab4" form="pr_form" name="ps_v4" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек более 120 : </label><input class="tab10" form="pr_form" name="ps_v4_6" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

Наихудший платежный статус по активным:
<select class="tab4" form="pr_form" name="psa_c" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select>
<label class="tab6">Наихудший платежный статус за 12 месяцев:</label>
<select class="tab10" form="pr_form" name="ps_c_12" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0">просрочка от 1 до 29</option>
	<option value="1">просрочка от 30 до 59</option>
	<option value="2">просрочка от 60 до 89</option>
	<option value="3">просрочка от 90 до 119</option>
	<option value="4">просрочка более 120</option>
</select>
<br>
Количество просрочек от 1 до 29 : <input class="tab4" form="pr_form" name="psa_v0" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 1 до 29 : </label><input class="tab10" form="pr_form" name="ps_v0_12" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 30 до 59 : <input class="tab4" form="pr_form" name="psa_v1" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 30 до 59 : </label><input class="tab10" form="pr_form" name="ps_v1_12" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 60 до 89 : <input class="tab4" form="pr_form" name="psa_v2" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 60 до 89 : </label><input class="tab10" form="pr_form" name="ps_v2_12" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек от 90 до 119 : <input class="tab4" form="pr_form" name="psa_v3" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек от 90 до 119 : </label><input class="tab10" form="pr_form" name="ps_v3_12" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество просрочек более 120 : <input class="tab4" form="pr_form" name="psa_v4" type="text" size="15" placeholder="по активным" value="" onchange="set_value(this.value, this.name, 0); show_products();">
<label class="tab6">Количество просрочек более 120 : </label><input class="tab10" form="pr_form" name="ps_v4_12" type="text" placeholder="по закрытым" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
<label>Была реструктуризация</label><input class="tab4" form="pr_form" name="restr" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"><br>

Количество обращений за последние 7 дней <input class="tab4" form="pr_form" name="obr_7" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество обращений за последние 14 дней <input class="tab4" form="pr_form" name="obr_14" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>
Количество обращений за последний месяц <input class="tab4" form="pr_form" name="obr_mon" type="text" placeholder="" size="15" value="" onchange="set_value(this.value, this.name, 0); show_products();"><br>

</div>

<div id="stop_f">
<label>Не анализировать данные о доходах и трудоустройстве<input class="tab4" form="pr_form" name="inc_true" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();"></label><br>
<label><input class="inp" form="pr_form" name="stop[0]" id="stop[0]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">100</label>
<label><input class="inp" form="pr_form" name="stop[1]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">101</label>
<label><input class="inp" form="pr_form" name="stop[2]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">102</label>
<label><input class="inp" form="pr_form" name="stop[3]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">103</label>
<label><input class="inp" form="pr_form" name="stop[4]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">104</label>
<label><input class="inp" form="pr_form" name="stop[5]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">105</label>
<label><input class="inp" form="pr_form" name="stop[6]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">106</label>
<label><input class="inp" form="pr_form" name="stop[7]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">107</label>
<label><input class="inp" form="pr_form" name="stop[8]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">108</label>
<label><input class="inp" form="pr_form" name="stop[9]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">109</label>
<label><input class="inp" form="pr_form" name="stop[10]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">110</label>
<label><input class="inp" form="pr_form" name="stop[11]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">111</label>
<label><input class="inp" form="pr_form" name="stop[12]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">121</label>
<label><input class="inp" form="pr_form" name="stop[13]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">122</label>
<label><input class="inp" form="pr_form" name="stop[14]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">123</label>
<label><input class="inp" form="pr_form" name="stop[15]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">124</label>
<label><input class="inp" form="pr_form" name="stop[16]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">130</label>
<label><input class="inp" form="pr_form" name="stop[17]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">140</label>
<label><input class="inp" form="pr_form" name="stop[18]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">145</label>
<label><input class="inp" form="pr_form" name="stop[19]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">141</label><br>
<label><input class="inp" form="pr_form" name="stop[20]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">146</label>
<label><input class="inp" form="pr_form" name="stop[21]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">147</label>
<label><input class="inp" form="pr_form" name="stop[22]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">148</label>
<label><input class="inp" form="pr_form" name="stop[23]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">149</label>
<label><input class="inp" form="pr_form" name="stop[24]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">200</label>
<label><input class="inp" form="pr_form" name="stop[25]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">150</label>
<label><input class="inp" form="pr_form" name="stop[26]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">151</label>
<label><input class="inp" form="pr_form" name="stop[27]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">152</label>
<label><input class="inp" form="pr_form" name="stop[28]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">153</label>
<label><input class="inp" form="pr_form" name="stop[29]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">154</label>
<label><input class="inp" form="pr_form" name="stop[30]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">160</label>
<label><input class="inp" form="pr_form" name="stop[31]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">161</label>
<label><input class="inp" form="pr_form" name="stop[32]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">171</label>
<label><input class="inp" form="pr_form" name="stop[33]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">174</label>
<label><input class="inp" form="pr_form" name="stop[34]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">175</label>
<label><input class="inp" form="pr_form" name="stop[35]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">180</label>
<label><input class="inp" form="pr_form" name="stop[36]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">190</label>
<label><input class="inp" form="pr_form" name="stop[37]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">191</label>
<label><input class="inp" form="pr_form" name="stop[38]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">500</label>
<label><input class="inp" form="pr_form" name="stop[39]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">193</label><br>
<label><input class="inp" form="pr_form" name="stop[40]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">194</label>
<label><input class="inp" form="pr_form" name="stop[41]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">195</label>
<label><input class="inp" form="pr_form" name="stop[42]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">196</label>
<label><input class="inp" form="pr_form" name="stop[43]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">197</label>
<label><input class="inp" form="pr_form" name="stop[44]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">198</label>
<label><input class="inp" form="pr_form" name="stop[45]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">199</label>
<label><input class="inp" form="pr_form" name="stop[46]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">207</label>
<label><input class="inp" form="pr_form" name="stop[47]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">208</label>
<label><input class="inp" form="pr_form" name="stop[48]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">209</label>
<label><input class="inp" form="pr_form" name="stop[49]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">210</label>
<label><input class="inp" form="pr_form" name="stop[50]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">211</label>
<label><input class="inp" form="pr_form" name="stop[51]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">212</label>
<label><input class="inp" form="pr_form" name="stop[52]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">213</label>
<label><input class="inp" form="pr_form" name="stop[53]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">214</label>
<label><input class="inp" form="pr_form" name="stop[54]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">215</label>
<label><input class="inp" form="pr_form" name="stop[55]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">216</label>
<label><input class="inp" form="pr_form" name="stop[56]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">217</label>
<label><input class="inp" form="pr_form" name="stop[57]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">218</label>
<label><input class="inp" form="pr_form" name="stop[58]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">219</label>
<label><input class="inp" form="pr_form" name="stop[59]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">220</label><br>
<label><input class="inp" form="pr_form" name="stop[60]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">221</label>
<label><input class="inp" form="pr_form" name="stop[61]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">222</label>
<label><input class="inp" form="pr_form" name="stop[62]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">223</label>
<label><input class="inp" form="pr_form" name="stop[63]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">224</label>
<label><input class="inp" form="pr_form" name="stop[64]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">225</label>
<label><input class="inp" form="pr_form" name="stop[65]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">226</label>
<label><input class="inp" form="pr_form" name="stop[66]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">227</label>
<label><input class="inp" form="pr_form" name="stop[67]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">228</label>
<label><input class="inp" form="pr_form" name="stop[68]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">301</label>
<label><input class="inp" form="pr_form" name="stop[69]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">302</label>
<label><input class="inp" form="pr_form" name="stop[70]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">303</label>
<label><input class="inp" form="pr_form" name="stop[71]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">304</label>
<label><input class="inp" form="pr_form" name="stop[72]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">305</label>
<label><input class="inp" form="pr_form" name="stop[73]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">306</label>
<label><input class="inp" form="pr_form" name="stop[74]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">307</label>
<label><input class="inp" form="pr_form" name="stop[75]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">308</label>
<label><input class="inp" form="pr_form" name="stop[76]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">309</label>
<label><input class="inp" form="pr_form" name="stop[77]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">501</label>
<label><input class="inp" form="pr_form" name="stop[78]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">311</label>
<label><input class="inp" form="pr_form" name="stop[79]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">312</label><br>
<label><input class="inp" form="pr_form" name="stop[80]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">313</label>
<label><input class="inp" form="pr_form" name="stop[81]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">314</label>
<label><input class="inp" form="pr_form" name="stop[82]" onchange="set_value(this.value, this.name, this.checked); show_products();" type="checkbox" value="1">502</label>

<br>Стоп-факторы категории Долги (сумма долга менее 15000 руб):
<br><label><input class="inp" form="pr_form" name="st_cr[0]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">приставы</label>
<label><input class="inp" form="pr_form" name="st_cr[1]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">штрафы ГИБДД</label>
<label><input class="inp" form="pr_form" name="st_cr[2]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">коммуналка</label>
<label><input class="inp" form="pr_form" name="st_cr[3]" type="checkbox" value="1" onchange="set_value(this.value, this.name, this.checked); show_products();">Р/С клиента заблокирован</label>

<br>Уровень риска по СПАРК<select class="tab2" form="pr_form" name="spark" style="color:red;" onchange="javascript: this.style.color= (this.value == '-1') ? 'red' : 'black'; set_value(this.value, this.name, 0); show_products();">
	<option value="-1">Выберите</option>
	<option value="0" >низкий</option>
	<option value="1" >средний</option>
	<option value="2" >высокий</option>
</select> 

</div>

<div>
<div id="msg_4_staff"></div>
<div id="msg_4_staff2"></div>

<div id="results"><div id="result1">Здесь будут результаты</div><div id="result2"></div></div>

</div>

</form>


</div>
<div id="f_param_b" style="display:<?=($show_f) ? 'block' : 'none';?>">
<span id="fp_text">Параметры формул:</span>
<div id="f_param"></div>
</div>
<form action="" method="post" id="get_form">
<input name="act" type="hidden" value="load_pod">
<input name="anketa_id" type="hidden" value="<?=$_id;?>">
</form>

<script>
if(document.querySelector('[form="pr_form"] option[value="-1"]').selected)
{
	document.querySelector('[form="pr_form"]').style.color = 'red';
}
</script>

