<?php
if (!defined('SYSTEM_START_9876543210')) exit;
$date_ = ", неделя:" . date('W') . ', день:' . date('d') . ', день недели:' . date('w');
$today_week = date('W');
//$today_day = date('j');
$today_day = date('d');
//$today_mon = date('n');
$today_mon = date('m');
$today_year = date('Y');
$today_wday = date('w');
$today = date("d.m.Y <b>H:i:s</b>");

?>
<input type="checkbox" id="nav-toggle" hidden>
<section class="main-sidebar">
    <section class="sidebar">
        <!--        <label for="nav-toggle" class="nav-toggle" onclick></label>-->
        <!--        <h2 class="logo">-->
        <!--            <a href="/">--><? //= $system_name; ?><!--</a>-->
        <!--        </h2>-->
        <!--        -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><? echo "$staff_lastname $staff_firstname" ?></p>
                <a href="#" onclick="javascript:ch_my_status(this);">
                    <? echo ($staff_status == 'busy') ? '<i class="fa fa-circle text-success"></i>' : '<i class="fa fa-circle text-warning"></i> ';
                    echo $today ?>
                </a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <?
            echo '<li><a href="/"><i class="fa fa-dashboard"></i><span>Рабочий стол</span></a></li>';
            echo '<li><a href="/create/"><i class="fa fa-plus"></i><span>Создать анкету</span></a></li>';
            echo '<li><a href="/list/"><i class="fa fa-list-ul"></i><span>Список заявок</span></a></li>';
            if (check_access("list_fa"))
                echo '<li><a href="/list_fa/"><span>ЗА/План работ</span></a></li>';;
            if (check_access("mstat"))
                echo '<li><a href="/mstat/"><i class="fa fa-chart-pie"></i><span>Статистика</span></a></li>';;
            if (check_access("metriks"))
                echo '<li><a href="/metriks/"><i class="fa fa-chart-bar"></i><span>Метрика</span></a></li>';;
            if (check_access("analitics"))
                echo '<li><a href="/analitics/"><i class="fa fa-chart-line"></i><span>Аналитика</span></a></li>';;
            if (check_access("listmetrika"))
                echo '<li><a href="/listmetrika/"><i class="fa fa-chart-bar"></i><span>Метрика(old)</span></a></li>';;
            if (check_access("metriks"))
                echo '<li><a href="/pf/"><i class="fa fa-circle"></i><span>Воронка</span></a></li>';;
            if (check_access("listsotr"))
                echo '<li><a href="/listsotr/"><i class="fa fa-handshake"></i><span>Сотрудничество</span></a></li>';;
            if (check_access("staff"))
                echo '<li><a href="/staff/"><i class="fa fa-user"></i><span>Сотрудники</span></a></li>';;
            if (check_access("agents"))
                echo '<li><a href="/agents/"><span>Агенты</span></a></li>';;
            if (check_access("asettings"))
                echo '<li><a href="/asettings/"><span>Настр. анкеты</span></a></li>';;
            if (check_access("services"))
                echo '<li><a href="/services/"><span>Сервисы</span></a></li>';;
            if (check_permission('form_checks'))
                echo '<li><a href="/checks/"><span>Проверки</span></a></li>';;
            if (check_access("bankrequests"))
                echo '<li><a href="/bankrequests/"><span>Заявки в банки</span></a></li>';;
            if (check_access("banks"))
                echo '<li><a href="/banks/"><span>Банки</span></a></li>';;
            if (check_access("filters"))
                echo '<li><a href="/filters/"><span>Фильтры</span></a></li>';;
            if (check_access("settings"))
                echo '<li><a href="/settings/"><span>Настройки</span></a></li>';;
            if (check_access("bpmn"))
                echo '<li><a href="/bpmn/"><span>BPMN</span></a></li>';;
            echo algoUrlsShow();
            ?>
        </ul>
    </section>
</section>
</aside>

<header class="main-header">
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini" style="text-transform: uppercase"><?= mb_strimwidth($system_name, 0 , 2); ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?= $system_name; ?></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu" style="height: 50px; display: flex;align-items: center;gap: 10px; padding-right: 15px;">
            <a href="/create/">
                <img src="/img/create.png" height="30px" title="Создать анкету" alt="Создать анкету">
            </a>
            <a href="/list/">
                <img src="/img/list_big.png" height="30px" title="Список анкет" alt="Список анкет">
            </a>
            <?
            if ($_page == 'details')
                echo '<button onclick="javascript:back2list();" style="position:absolute; left:5px; top:30px;">< Назад</button>';
            if ($_page == 'list') {
                echo '<div class="search_div"><input type="text" id="search_input" oninput="javascript:calc_result(this.value);"> <button id="search_button" style="z-index:999;" onclick="javascript:gsearch()">Поиск</button></div>';
            }
            ?>
            <div style="width:60px; ">
                <button class="btn btn-default btn-flat" onclick="javascript:logout();">Выход</button>
            </div>
        </div>
        <div id="in_work_h"></div>

        <style>
            .header__info {
                display: flex;
                min-height: 50px;
                align-items: center;
                flex-flow: row wrap;
            }
            .header__info .balance_list {
                margin-right: 30px;
            }
        </style>

        <div class="header__info">
            <?

            if (($staff_office_type == 'main') && (check_permission('see_balance'))) {
                $bal_mass = array();

                /*$dfrom_b = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), 1, date('Y')));
                $sql = "SELECT type, COUNT(*) as cp FROM idx_log WHERE datereq >'$dfrom_b' AND status != 'failed' GROUP BY type;";
                $result = $db_connect->query($sql);
                $bal_idx = 11800;
                $c_verifyPhone = 0;
                $c_finScoring = 0;
                if ($result->num_rows)
                {
                    while($row = $result->fetch_array(MYSQLI_ASSOC))
                    {
                        if ($row['type'] == 'finScoring')
                        {
                            if ($c_finScoring < 1001)
                                $bal_idx -= 23.6 * (0 + $row['cp']);
                            else
                                $bal_idx -= (23.6 * 1000 + 17.7 * (0 + $row['cp'] - 1000));
                        }
                        else if ($row['type'] == 'verifyPhone')
                        {
                            $c_verifyPhone++;
                            if ($c_verifyPhone < 1001)
                                $bal_idx -= 11.8 * (0 + $row['cp']);
                            else
                                $bal_idx -= (11.8 * 1000 + 10.62 * (0 + $row['cp'] - 1000));
                        }
                        else if ($row['type'] == 'checkFssp')
                        {
                            $bal_idx -= 5 * (0 + $row['cp']);
                        }
                    }
                }
                $result->close();*/

                $sql = "SELECT pval, sname, pname FROM services_info WHERE (sname='atompark' OR sname='promoney' OR sname='yandex' OR sname='telphin') AND (pname='balance' OR pname='balance2');";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        if ($row['pname'] == 'balance2') {
                            if ($row['sname'] == 'yandex') {
                                $bal_mass[$row['sname'] . '2'] = $row['pval'];
                            }
                            continue;
                        }
                        $bal_mass[$row['sname']] = $row['pval'];
                    }
                }

                if (0 + $bal_mass['atompark'] < 500)
                    $bal_mass['atompark'] = '<font color="red">' . $bal_mass['atompark'] . '</font>';

                if (0 + $bal_mass['promoney'] < 1000)
                    $bal_mass['promoney'] = '<font color="red">' . $bal_mass['promoney'] . '</font>';

                $result->close();
                echo '<div style="display: flex; gap: 10px;" class="balance_list"><span onmouseover="javascript:show_chart_sms();" onmouseout="javascript:close_charts();">Баланс смс: ' . round($bal_mass['atompark']) . ' руб.</span> ';
                echo '<span onmouseover="javascript:show_chart_prom();" onmouseout="javascript:close_charts();">Промани: ' . round($bal_mass['promoney']) . ' руб.</span> ';
                echo '<span onmouseover="javascript:show_chart_ya();" onmouseout="javascript:close_charts();">Yandex: ' . round($bal_mass['yandex']) . ' руб.</span> ';
                echo '<span onmouseover="javascript:show_chart_ya2();" onmouseout="javascript:close_charts();">Yandex2: ' . round($bal_mass['yandex2']) . ' руб.</span> ';
                echo '<span onmouseover="javascript:show_chart_tel();" onmouseout="javascript:close_charts();">Telphin: ' . $bal_mass['telphin'] . ' руб.</span> ';
                echo '</div>';

                echo '<script type="text/javascript">';

                $ya_mass = array();
                $ya_mass_s = '';
                $sql = "SELECT ctime, value FROM service_info_log WHERE sid='1' ORDER BY id DESC LIMIT 100;";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($ya_mass, $row['value']);
                    }
                }

                $ya_mass = array_reverse($ya_mass);
                foreach ($ya_mass as $v) {
                    if ($ya_mass_s != '')
                        $ya_mass_s .= ', ';
                    $ya_mass_s .= $v;

                }
                echo "var sms_bal_mass = [$ya_mass_s];";

                $ya_mass = array();
                $ya_mass_s = '';
                $sql = "SELECT ctime, value FROM service_info_log WHERE sid='2' ORDER BY id DESC LIMIT 100;";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($ya_mass, $row['value']);
                    }
                }

                $ya_mass = array_reverse($ya_mass);
                foreach ($ya_mass as $v) {
                    if ($ya_mass_s != '')
                        $ya_mass_s .= ', ';
                    $ya_mass_s .= $v;

                }
                echo "var prom_bal_mass = [$ya_mass_s];";

                $ya_mass = array();
                $ya_mass_s = '';
                $sql = "SELECT ctime, value FROM service_info_log WHERE sid='3' ORDER BY id DESC LIMIT 100;";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($ya_mass, $row['value']);
                    }
                }

                $ya_mass = array_reverse($ya_mass);
                foreach ($ya_mass as $v) {
                    if ($ya_mass_s != '')
                        $ya_mass_s .= ', ';
                    $ya_mass_s .= $v;

                }
                echo "var ya_mass = [$ya_mass_s];";


                $ya_mass = array();
                $ya_mass_s = '';
                $sql = "SELECT ctime, value FROM service_info_log WHERE sid='5' ORDER BY id DESC LIMIT 100;";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($ya_mass, $row['value']);
                    }
                }

                $ya_mass = array_reverse($ya_mass);
                foreach ($ya_mass as $v) {
                    if ($ya_mass_s != '')
                        $ya_mass_s .= ', ';
                    $ya_mass_s .= $v;

                }
                echo "var ya_mass2 = [$ya_mass_s];";


                $tel_mass = array();
                $tel_mass_s = '';
                $sql = "SELECT ctime, value FROM service_info_log WHERE sid='4' ORDER BY id DESC LIMIT 100;";
                $result = $db_connect->query($sql);
                if ($result->num_rows) {
                    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                        array_push($tel_mass, $row['value']);
                    }
                }

                $tel_mass = array_reverse($tel_mass);
                foreach ($tel_mass as $v) {
                    if ($tel_mass_s != '')
                        $tel_mass_s .= ', ';
                    $tel_mass_s .= $v;

                }
                echo "var tel_mass = [$tel_mass_s];";

                echo "</script>";
            }

            $stlogout = "";
            if ($staff_id == 1)
                $stlogout = ' style="width:600px;"';
            ?>
            <div style="display: flex; gap: 10px" class="logout"<? echo $stlogout; ?>>
                <?
                if ($staff_position_debug == 'super-admin') {
                    echo '<div style="width:150px; float: left">';
                    echo '<select onchange="javascript:debug(this);">';
                    foreach ($offices_mass as $k => $v) {
                        echo '<optgroup label="' . $v['name'] . '">';
                        foreach ($staff_mass as $key => $val) {
                            if (($val['office'] != $k) || $val['deleted'] == 1)
                                continue;

                            $fio_a = $val['lastname'];
                            if ($val['firstname'] != '')
                                $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                            if ($val['patronymic'] != '')
                                $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                            $selected = ($val['id'] == $staff_id) ? " selected" : "";

                            echo '<option value="' . $val['id'] . '"' . $selected . '>' . $fio_a . '</option>';
                        }
                        echo '</optgroup>';
                    }
                    echo "</select> ";
                    echo "</div> ";
                }
                //if ($staff_id == 1)
                if (true)
                {
                ?>

                <span id="task_header_span" onclick="javascript:show_tasks_list();">Нет задач</span><br>
                <span id="call_header_span" onclick="javascript:show_call_calendar();"><img src="/img/tel1.png"
                                                                                            height="12px" ;> нет сегодня</span><br>
                <?
                if ($staff_office_type == 'main') {
                    ?>
                    <span id="bank_header_span" onclick="javascript:open_list_ba();">Нет одобрений в банках</span>
                    <?
                }
                ?>
                <span id="meet_header_span" onclick="javascript:show_meet_calendar();"><img src="/img/meet3.png"
                                                                                            height="13px" ;> нет сегодня</span>

            </div>
        </div>
    </nav>
</header>
<?
}
else {
    ?>
    <span id="task_header_span" class="green_header_task" onclick="javascript:show_tasks_list();">Нет задач</span>&nbsp;
    <button onclick="javascript:logout();">Выход</button></div>
    </nav>
    </header>
    <?
}
?>


<div onclick="show_cal('none')" id="wrap_cal"></div>
<div id="window_cal" class="window_cal">
    <img class="close" onclick="show_cal('none')" src="/img/close.png">

    <div id="calendar_call" style="display:none;"></div>
    <div id="calendar_meet" style="display:none;"></div>
    <div id="task_wrap" style="display:none;">
        <?
        if (($staff_office_type == 'main') || ($staff_id == '39') || ($staff_position == 'director_ozs'))
            echo '<div id="tasks_buttonst"><button onclick="javascript:show_task_form();">Добавить задачу</button></div>';
        echo '<div id="task_form" style="display:none;">';
        echo '<div>Анкета <input type="text" name="form_id"';
        if ($_page == 'details') {
            if (isset($_GET['id'])) {
                $_id = 0 + $_GET['id'];
                echo " value='$_id'";
            }
        }
        echo '> или сотрудник <select id="task_sel_staff_id" name="staff_id">';
        echo '<option value="-1">Выберите</option>';
        foreach ($offices_mass as $k => $v) {
            //if ($v['type'] != 'tm')
            //	continue;

            echo '<optgroup label="' . $v['name'] . '">';
            //echo '<option value="of_' . $v['id'] . '">Передать в офис</option>';
            foreach ($staff_mass as $key => $val) {
                if (($val['office'] != $k) || $val['deleted'] == 1)
                    continue;

                $fio_a = $val['lastname'];
                if ($val['firstname'] != '')
                    $fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
                if ($val['patronymic'] != '')
                    $fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';

                echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
            }

            echo '</optgroup>';
        }
        echo '</select>';
        echo '</div>';
        echo '<div>Тема: <input type="text" name="task_head"></div>';
        echo '<div>Задача: <textarea name="task_text"></textarea></div>';
        echo '<button onclick="javascript:create_task();">Создать задачу</button>';
        echo '</div>';

        ?>
        <div id="tasks_list_wrap">
            <div id="tasks_list"></div>
        </div>
    </div>
</div>