<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$all_fields_mass = array();
$all_fields_mass2 = array();
$fields_mass = array();
$banks_mass = array();
$block_mass = array();

if ($result = $db_connect->query("SELECT id, name FROM old_banks"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$banks_mass[$row['id']] = $row['name'];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$office_filter = '';
if ($staff_office_type != 'main')
{
	if ($staff_office_type == 'tm')
	{
		if ($staff_position == 'director_tm')
			$office_filter = " AND av_tm_dir='1'";
		else if ($staff_position == 'manager_tm')
			$office_filter = " AND av_tm_man='1'";
	}
	else if ($staff_office_type == 'ozs')
	{
		if ($staff_position == 'director_ozs')
			$office_filter = " AND av_ozs_dir='1'";
		else if ($staff_position == 'manager_ozs')
			$office_filter = " AND av_ozs_man='1'";
	}
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block'$office_filter ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($all_fields_mass, $row);
		$all_fields_mass2[$row['name']] = $row;
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($block_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$ddate = 'onclick="javascript:date_format(this);" onkeyup="javascript:date_format(this);"';
$ttel = 'onclick="javascript:tel_format(this);" onkeyup="javascript:tel_format(this);"';

echo '<div id="anketa_data_wrap">';
	echo '<div id="anketa_data">';
		echo "<div class=\"block_wrap\">";
			echo '<div class="row_a">';
				echo '<div class="fname"><font color="red">' . $all_fields_mass2['sum']['description'] . '</font>:</div>';
				echo '<div class="fval">';
					$oninputscript = ' onkeyup="javascript:format_numbers(this);"';
					echo '<input type="text" name="sum"' . $oninputscript . ' placeholder="..."><span id="sp_sum"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['firstname']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="firstname" placeholder="..."><span id="sp_firstname"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['middlename']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="middlename" placeholder="..."><span id="sp_middlename"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['lastname']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="lastname" placeholder="..."><span id="sp_lastname"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['birth_day']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="birth_day" placeholder="..." ' . $ddate . ' class="datepicker-here" data-date-format="dd.mm.yyyy"><span id="sp_birth_day"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['field49']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="field49" placeholder="..."><span id="sp_field49"></span>';
				echo "</div>";
			echo "</div>";
			echo '<div class="row_a">';
				echo '<div class="fname"><font color="red">' . $all_fields_mass2['field29']['description'] . '</font>:</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="field29" placeholder="..." ' . $ttel . '><span id="sp_field29"></span>';
				echo "</div>";
			echo "</div>";
			
			echo '<div class="row_a">';
				echo '<div class="fname">' . $all_fields_mass2['field60']['description'] . ':</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="field60" placeholder="..." ' . $ttel . '><span id="sp_field60"></span>';
				echo "</div>";
			echo "</div>";
			
			echo '<div class="row_a">';
				echo '<div class="fname">Это поданкета?</div>';
				echo '<div class="fval">';
					echo '<input type="checkbox" name="f_pod_ch" value="Да" onclick="javascript:show_poda_field(this);"><span id="sp_f_pod_ch"></span>';
				echo "</div>";
			echo "</div>";
			
			echo '<div class="row_a" id="mainform_row" style="display:none;">';
				echo '<div class="fname">Основная анкета:</div>';
				echo '<div class="fval">';
					echo '<input type="text" name="mainform" style="border: 1px solid black; width:50%;" oninput="javascript:findforms(this);"><span id="sp_mainform"><i class="fil5"></i><ul name="rsl"></ul></span>';
				echo "</div>";
			echo "</div>";
			
		echo "</div>";
	echo "</div>";
	
	echo '<div><button id="b_create_f" onclick="javascript:save_data();">Создать анкету и сохранить данные</button>';
	
	if ($staff_office_type == 'main')
	{
		echo '<span style="float:right;">Направить анкету ';
		echo '<select id="cr_form_sel_staff">';
		foreach ($offices_mass as $k => $v)
		{
			if ($v['type'] == 'main')
				continue;

			echo '<optgroup label="' . $v['name'] . '">';
			echo '<option value="of_' . $v['id'] . '">Передать в офис ' . $v['name'] . '</option>';
			foreach ($staff_mass as $key => $val)
			{
				if (($val['office'] != $k) || $val['deleted'] == 1)
					continue;
				
				$fio_a = $val['lastname'];
				if ($val['firstname'] != '')
					$fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
				if ($val['patronymic'] != '')
					$fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';
				
				echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
			}
			
			echo '</optgroup>';
		}
		echo '</select></span>';
	}
	else if ($staff_position == 'director_tm')
	{
		echo '<span style="float:right;">Направить анкету ';
		echo '<select id="cr_form_sel_staff">';
		echo '<option value="st_' . $staff_id . '">Передать мне</option>';
		foreach ($offices_mass as $k => $v)
		{
			if ($v['type'] == 'main')
				continue;
			if ($v['type'] == 'ozs')
				continue;
			
			if ($v['id'] != $staff_office)
				continue;

			//echo '<optgroup label="' . $v['name'] . '">';
			
			echo '<option value="of_' . $v['id'] . '">Передать в офис ' . $v['name'] . '</option>';
			foreach ($staff_mass as $key => $val)
			{
				if (($val['office'] != $k) || $val['deleted'] == 1)
					continue;
				
				$fio_a = $val['lastname'];
				if ($val['firstname'] != '')
					$fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
				if ($val['patronymic'] != '')
					$fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';
				
				echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
			}
			
			//echo '</optgroup>';
		}
		echo '</select></span>';
	}
	else if ($staff_office_type == 'ozs')
	{
		echo '<span style="float:right;">Направить анкету ';
		echo '<select id="cr_form_sel_staff">';
		echo '<option value="st_' . $staff_id . '">Передать мне</option>';
		foreach ($offices_mass as $k => $v)
		{
			if ($v['type'] == 'main')
				continue;
			if ($v['type'] == 'ozs')
				continue;

			//echo '<optgroup label="' . $v['name'] . '">';
			
			echo '<option value="of_' . $v['id'] . '">Передать в офис ' . $v['name'] . '</option>';
			/*foreach ($staff_mass as $key => $val)
			{
				if (($val['office'] != $k) || $val['deleted'] == 1)
					continue;
				
				$fio_a = $val['lastname'];
				if ($val['firstname'] != '')
					$fio_a .= ' ' . substr($val['firstname'], 0, 2) . '.';
				if ($val['patronymic'] != '')
					$fio_a .= ' ' . substr($val['patronymic'], 0, 2) . '.';
				
				echo '<option value="st_' . $val['id'] . '">' . $fio_a . '</option>';
			}*/
			
			//echo '</optgroup>';
		}
		echo '</select></span>';
	}
	else
	{
		echo '<span style="float:right;">Направить анкету ';
		echo '<select id="cr_form_sel_staff">';
		echo '<option value="st_' . $staff_id . '">Передать мне</option>';
		foreach ($offices_mass as $k => $v)
		{
			if ($v['id'] != $staff_office)
				continue;
			echo '<option value="of_' . $v['id'] . '">Передать в офис ' . $v['name'] . '</option>';
		}
		echo '</select></span>';
	}
	echo '</div>';
	
echo "</div>";

echo '<div id="cr_double_wrap">';
	echo '<div id="cr_double_list_h">';
	echo "</div>";
	echo '<div id="cr_double_list">';
	echo "</div>";
echo "</div>";

	echo '<script type="text/javascript">';
	echo "var staff_office_type = '$staff_office_type';\n";
	echo "</script>";
?>