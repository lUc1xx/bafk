<?php
if (!defined('SYSTEM_START_9876543210')) exit;  
// $db_host = "localhost";
// $db_user = "cb_test";
// $db_pass = "cbtest";
// $db_name = "cb_test";
$db_host = "localhost";
$db_user = "root";
$db_pass = "root";
$db_name = "testbafk";

$link = dbconnect($db_host, $db_user, $db_pass, $db_name);

function dbconnect($db_host, $db_user, $db_pass, $db_name) {
	global $db_connect;
	$db_connect = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
	if (!$db_connect)
	{
		echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
		echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
	return $db_connect;
}

function check_token($login, $token)
{
	global $db_connect;
	if($result = mysqli_query($db_connect, "SELECT * FROM authorized WHERE login='$login' AND token='$token' AND status = '1' ORDER BY id DESC LIMIT 1"))
	{
		if (mysqli_num_rows($result))
		{
			$row = mysqli_fetch_assoc($result);
			
			$st_id = $row['staff_id'];
			mysqli_free_result($result);
			
			if($result2 = mysqli_query($db_connect, "SELECT * FROM staff WHERE login='$login' AND id='$st_id' AND deleted = '0' ORDER BY id DESC LIMIT 1"))
			{
				if (mysqli_num_rows($result2))
				{
					$row2 = mysqli_fetch_assoc($result2);
					$mass['status'] = true;
					$mass['staff_id'] = $st_id;
					$mass['staff_login'] = $login;
					$mass['staff_office'] = $row2['office'];
					$mass['staff_position'] = $row2['position'];
					$mass['lastname'] = $row2['lastname'];
					$mass['firstname'] = $row2['firstname'];
					$mass['email_corp'] = $row2['email_corp'];
					$mass['phone_work'] = $row2['phone_work'];
					$mass['status'] = $row2['status'];
					$mass['for_za'] = $row2['fin_an'];
					$mass['plan_check'] = $row2['plan_check'];
					$mass['staff_bc'] = $row2['staff_bc'];
				}
				else
				{
					$mass['status'] = false;
					$mass['error'] = "Не найден пользователь $login";
				}
				mysqli_free_result($result2);
			}
			else
			{
				echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
				
				return false;
			}
			
		}
		else
		{
			$mass['status'] = false;
			$mass['error'] = "Не найдена сессия для пользователя $login";
			mysqli_free_result($result);
		}
		//mysqli_free_result($result);
		return $mass;
	}
	else
	{
		echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
		
		return false;
	}
}

function check_ip($stid)
{
	//global $staff_id;
	//if ($login == 'alexey')
		//return false;
	return true;
}

function check_pass($login, $passwd)
{
	global $db_connect;
	if($result = mysqli_query($db_connect, "SELECT * FROM staff WHERE login='$login' AND password='$passwd' AND deleted = '0' LIMIT 1"))
	{
		if (mysqli_num_rows($result))
		{
			$row = mysqli_fetch_assoc($result);
			$mass['status'] = true;
			$mass['staff_id'] = $row['id'];
			$mass['staff_login'] = $login;
			$mass['staff_office'] = $row['office'];
			$mass['staff_position'] = $row['position'];
			$mass['lastname'] = $row['lastname'];
			$mass['firstname'] = $row['firstname'];
			$mass['email_corp'] = $row['email_corp'];
			$mass['phone_work'] = $row['phone_work'];
			$mass['status'] = $row['status'];
			$mass['for_za'] = $row['fin_an'];
			$mass['plan_check'] = $row['plan_check'];
			$mass['staff_bc'] = $row['staff_bc'];
			$mass['ipmass'] = $row['ip_avail_val'];
		}
		else
		{
			$mass['status'] = false;
			$mass['error'] = "Неправильный логин или пароль";
		}
		mysqli_free_result($result);
		return $mass;
	}
	else
	{
		echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
		
		return false;
	}
}

function start_new_session($staff_id, $staff_login, $ip, $config)
{
	global $db_connect;
	
	//Закрыть предыдущие сессии, если есть
	if(!mysqli_query($db_connect, "UPDATE authorized SET status = '0' WHERE status = '1' AND login='$staff_login';"))
	{
		echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
		
		return false;
	}
	
	$token = md5($ip . md5($config . rand(0, PHP_INT_MAX)));
	$config_ = mysqli_real_escape_string($db_connect, $config);
	//$sql = "INSERT INTO authorized VALUES('0', '$staff_id', '$staff_login', '$token', '$ip', 'NOW()', '1', '0', '$config_');";
	$sql = "INSERT INTO authorized(staff_id, login, token, ip, adate, status, config) VALUES('$staff_id', '$staff_login', '$token', '$ip', NOW(), '1', '$config_');";
	
	if($result = mysqli_query($db_connect, $sql))
	{
		return $token;
	}
	else
	{
		echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
		
		return false;
	}
}

?>
