<?php
if (!defined('SYSTEM_START_9876543210')) exit;

if (isset($_GET['id']))
{
	$pid = $_GET['id'];
}
else die();

header('Content-Type: image/jpeg');
//$path = get_path($pid);
//print_r('test');
if ($result = $db_connect->query("SELECT id, form_id, name, type, hash_name FROM files WHERE id='$pid' ORDER BY name ASC;"))
{
	if ($result->num_rows)
	{
		$row = $result->fetch_array(MYSQLI_ASSOC);
		$result->close();
		$path = FILES_DIR . get_path($row['form_id']);
		$image = imagecreatefromjpeg($path . "/" . $row['hash_name']); 

		
		$exif = exif_read_data($path . "/" . $row['hash_name']); 
		if (!empty($exif['Orientation'])) { 
			switch ($exif['Orientation']) { 
				// Поворот на 180 градусов 
				case 3: { 
					$image = imagerotate($image,180,0); 
					break; 
				} 
				// Поворот вправо на 90 градусов 
				case 6: { 
					$image = imagerotate($image,-90,0); 
					break; 
				} 
				// Поворот влево на 90 градусов 
				case 8: { 
					$image = imagerotate($image,90,0); 
					break; 
				} 
			}
			imagejpeg($image);
			imagedestroy($image);
		}
	}
	else	
		$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

?>

