<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$agents_offices = array();
$list_offices = array();
if ($result = $db_connect->query("SELECT * FROM agents_offices WHERE active='1' ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
		$agents_offices[$row['id']] = $row['name'];
	$result->close();
}

?>
<div style="display: inline-block; border-left: 1px solid rgb(36,43,51); margin-left:25px; margin-bottom:25px;">
<a href="#agents" class="button18" tabindex="0" onclick="javascript:show_agent_list();">Список агентов</a>
<a href="#req" class="button18" tabindex="0" onclick="javascript:show_agent_requests();">Заявки в агенты</a>
<a href="#logs" class="button18" tabindex="0" onclick="javascript:show_agent_logs();">Логи агентов</a>
</div>
<?
echo '<div onclick="show(\'none\')" id="wrap"></div>';

echo '<div id="window_of" class="window">';
	echo '<img class="close" onclick="show_of(\'none\')" src="/img/close.png">';
	echo '<b>Добавление нового офиса</b>';
	echo '<div id="new_office_wrap">';
		echo '<div class="row_a"><div class="fname">Наименование:</div><div class="fval"><input class="nwinp0" type="text" name="name"></div></div>';
		echo '<div class="row_a"><div class="fname">Описание:</div><div class="fval"><input class="nwinp1" type="text" name="description"></div></div>';
		echo '<div class="row_a">';
			echo '<div class="cval"><button id="noffice_add_b" onclick="javascript:office_add();">Добавить</button></div>';
			echo '<div class="cval"><button id="noffice_clear_b" onclick="javascript:office_clear();">Очистить</button></div>';
		echo '</div>';
	echo '</div>';
echo '</div>';

echo '<div id="window" class="window">';
	echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
	
	echo '<b>Добавление нового Агента</b>';
	echo '<div id="new_agent_wrap">';
		echo '<div class="row_a"><div class="fname">Логин:</div><div class="fval"><input class="nwinp0" type="text" name="login"></div></div>';
		echo '<div class="row_a"><div class="fname">Пароль:</div><div class="fval"><input class="nwinp1" type="password" name="passwd"></div></div>';
		echo '<div class="row_a"><div class="fname">Имя:</div><div class="fval"><input class="nwinp0" type="text" name="name"></div></div>';
		echo '<div class="row_a"><div class="fname">Офис:</div><div class="fval"><select class="nwinp1" name="office">';
		echo '<option value="">Выберите</option>';
			foreach ($agents_offices as $k=>$v)
				echo '<option value="' . $k . '">' . $v . '</option>';
		echo '</select></div></div>';
		echo '<div class="row_a"><div class="fname">Телефон:</div><div class="fval"><input class="nwinp0" type="text" name="phone"></div></div>';
		echo '<div class="row_a"><div class="fname">email:</div><div class="fval"><input class="nwinp1" type="text" name="email"></div></div>';
		echo '<div class="row_a">';
			echo '<div class="cval"><button id="nagent_add_b" onclick="javascript:nagent_add();">Добавить</button></div>';
			echo '<div class="cval"><button id="nagent_clear_b" onclick="javascript:nagent_clear();">Очистить</button></div>';
		echo '</div>';
	echo '</div>';
echo '</div>';



echo '<div id="offices_wrap">';
	echo '<div id="cont4offices">';
		echo "<div class=\"head_table\">";
			echo '<div class="list_cell">Id</div>';
			echo '<div class="list_cell">Название</div>';
			echo '<div class="list_cell" style="width:200px;">Описание</div>';
			echo '<div class="list_cell">Ред.</div>';
			echo '<div class="list_cell">Уд.</div>';
		echo "</div>";
if ($result = $db_connect->query("SELECT * FROM agents_offices WHERE active='1' ORDER BY id ASC;"))
{
	$row_odd = "row_odd";	//нечет
	$row_even = "row_even";	//чет
	$c = false;
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$list_offices[$row['id']] = $row['name'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div class=\"$cl\">";
			echo '<div class="list_cell">' . $row['id'] . '</div>';
			echo '<div class="list_cell">' . $row['name'] . '</div>';
			echo '<div class="list_cell" style="width:200px;">' . $row['description'] . '</div>';
			echo '<div class="list_cell"><a title="Редактировать" href="#" onclick="javasccript:edit_office(' . $row['id'] . ')"><img src="/img/edit.png" width="16" height="16"></a></div>';
			echo '<div class="list_cell"><a title="Удалить" href="#" onclick="javasccript:del_office(' . $row['id'] . ')"><img src="/img/delete.png" width="16" height="16"></a></div>';
		echo "</div>";
	}
	
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
	echo "</div>";
	echo '<div><span id="add_office_b" onclick="javascript:add_office();">Добавить офис</span></div>';
echo '</div>';

//$res = array();
echo '<div id="agent_wrap">';
	echo '<div id="cont4agents">';
		echo "<div class=\"head_table\">";
			echo '<div class="list_cell">Id</div>';
			echo '<div class="list_cell">Логин</div>';
			echo '<div class="list_cell">Пароль</div>';				
			echo '<div class="list_cell">Офис</div>';
			echo '<div class="list_cell">Имя</div>';
			echo '<div class="list_cell">Телефон</div>';
			echo '<div class="list_cell">Почта</div>';
			echo '<div class="list_cell">Дата создания</div>';
			echo '<div class="list_cell">Посл. авторизация</div>';
			echo '<div class="list_cell">Ред.</div>';
			echo '<div class="list_cell">Удал.</div>';
		echo "</div>";
			
if ($result = $db_connect->query("SELECT agents.id AS id, agents.login AS login, agents.name AS name, phone, email, date_create, last_auth, agents_offices.name AS ofname FROM agents LEFT JOIN agents_offices ON agents.office=agents_offices.id WHERE agents.deleted='0' ORDER BY agents.id ASC;"))
{
	//$res = "Select вернул " . $result->num_rows . " строк.\n";
	
	$row_odd = "row_odd";	//нечет
	$row_even = "row_even";	//чет
	$c = false;

		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//$of_id = ($row['office'] == 0) ? 1 : $row['office'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div id=\"agent_row_{$row['id']}\" class=\"$cl\">";
			echo '<div class="list_cell">' . $row['id'] . '</div>';
			echo '<div class="list_cell">' . $row['login'] . '</div>';
			echo '<div class="list_cell">' . '-' . '</div>';				
			echo '<div class="list_cell">' . $row['ofname'] . '</div>';
			echo '<div class="list_cell">' . $row['name'] . '</div>';
			echo '<div class="list_cell">' . $row['phone'] . '</div>';
			echo '<div class="list_cell">' . $row['email'] . '</div>';
			echo '<div class="list_cell">' . $row['date_create'] . '</div>';
			echo '<div class="list_cell">' . $row['last_auth'] . '</div>';
			echo '<div class="list_cell">' . '<span style="cursor:pointer;" onclick="javascript:edit_agent(' . $row['id'] . ');"><img src="/img/edit.png"></span>' . '</div>';
			echo '<div class="list_cell">' . '<span style="cursor:pointer;" onclick="javascript:del_agent(' . $row['id'] . ');"><img src="/img/delete.png"></span>' . '</div>';
		echo '</div>';
	}
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
	echo "</div>";
	echo '<div id="add_w_b_wrap"><span id="add_worker_b" onclick="javascript:add_agent();">Добавить агента</span></div>';

echo '</div>';

//echo "</div>";


echo '<div id="arequests_wrap" style="display:none;">';
	echo '<div id="cont4agreq">';
		echo "<div class=\"head_table\">";
			echo '<div class="list_cell">Дата</div>';
			echo '<div class="list_cell">Номер</div>';
			echo '<div class="list_cell">Фио</div>';				
			echo '<div class="list_cell">Телефон</div>';
			echo '<div class="list_cell">Статус</div>';
			echo '<div class="list_cell">СК</div>';
		echo "</div>";

if ($result = $db_connect->query("SELECT id, date_add, firstname, contact_phone, status FROM forms WHERE type='agent' ORDER BY id DESC;"))
{
	//$res = "Select вернул " . $result->num_rows . " строк.\n";
	
	$row_odd = "row_odd";	//нечет
	$row_even = "row_even";	//чет
	$c = false;

		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//$of_id = ($row['office'] == 0) ? 1 : $row['office'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div id=\"areq_row_{$row['id']}\" class=\"$cl\">";
			echo '<div class="list_cell">' . $row['date_add'] . '</div>';
			echo '<div class="list_cell">' . $row['id'] . '</div>';
			echo '<div class="list_cell">' . $row['firstname'] . '</div>';
			echo '<div class="list_cell">' . $row['contact_phone'] . '</div>';
			
			//echo '<div class="list_cell">' . $row['status'] . '</div>';
			$o1 = ($row['status'] == 'new') ? "selected" : "";
			$o2 = ($row['status'] == 'primary_work') ? "selected" : "";
			$o3 = ($row['status'] == 'credit_approved') ? "selected" : "";
			$o4 = ($row['status'] == 'credit_declined') ? "selected" : "";
			$o5 = ($row['status'] == 'loan_issued') ? "selected" : "";
			
			echo '<div class="list_cell"><select onchange="javascript:ch_areq_status(this, ' . $row['id'] . ');">';
				echo '<option value="new" ' 	. $o1 . '>Новая</option>';
				echo '<option value="in_work" ' . $o2 . '>На рассмотрении</option>';
				echo '<option value="approve" ' . $o3 . '>Одобрена</option>';
				echo '<option value="reject" ' 	. $o4 . '>Отклонена</option>';
				echo '<option value="make" ' 	. $o5 . '>Создан кабинет</option>';
			echo '</select></div>';
			
			if ($o5 == '')
				echo '<div class="list_cell"><img onclick="javascript:make_acab(this);" src="/img/create.png" height="18px" style="cursor:pointer; margin:0px; padding:0px; vertical-align:middle;" title="Создать кабинет" alt="Создать кабинет"></div>';
			else
				echo '<div class="list_cell"><img src="/img/apply.png" height="18px" style="margin:0px; padding:0px; vertical-align:middle;" title="Кабинет есть" alt="Кабинет есть"></div>';
		echo '</div>';
	}
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
	echo "</div>";
echo "</div>";

echo '<div id="alogs_wrap" style="display:none;">';
echo 'тут будут логи';
echo "</div>";
?>

