<?php
/**
 *	ИСКА - Информационная система обмена анкет.
 *
 *	Журнал работы сотрудников.	
 */
/*
 include_once ('../_config.php');

// Подсоединямся к БД (кодировка windows-1251)
mysql_connect($host, $user, $password);
//mysql_select_db($bd_name);
mysql_select_db('control');
mysql_query(
	'SET character_set_results=cp1251, character_set_database=cp1251, '.
  'character_set_client=cp1251, character_set_connection=cp1251');
  */
if (!defined('SYSTEM_START_9876543210')) exit; 

$banks = array();
$banks2 = array();


if ($result = $db_connect->query("SELECT id, name FROM old_banks ORDER BY name ASC;"))
{
		
	while ($a = $result->fetch_array(MYSQLI_ASSOC))
	{
		$banks[$a["id"]] = $a["name"];
		$banks2[] = array('id' => $a["id"], 'name' => $a["name"]);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
//echo '<script type="text/javascript" src="/scripts/vue.min.js"></script>';
//	echo '<script type="text/javascript" src="/scripts/quasar.umd.min.js"></script>';
//	echo '<script type="text/javascript" src="/scripts/ru.umd.min.js"></script>';
//	echo '<script type="text/javascript" src="/scripts/banks.js?' . rand() . '"></script>';
?>

<form action="" method="post" id="be_form">
<input type="hidden" value="add" name="be_type">
<input type="hidden" value="0" name="be_prod_id">
</form>

<form action="" method="post" id="be_del_form">
<input type="hidden" value="del" name="be_type">
<input type="hidden" value="0" name="be_prod_id">
</form>

<div onclick="show('none')" id="wrap"></div>
<div id="window">
	<img class="close" onclick="show('none')" src="/img/close.png">


<br>
	
<div class="korpus">
  <input type="radio" name="odin" checked="checked" id="vkl1"/>	<label class="lm" for="vkl1">Основные</label>
  <input type="radio" name="odin" id="vkl2"/>					<label class="lm" for="vkl2">Трудоустройство и активы</label>
  <input type="radio" name="odin" id="vkl3"/>					<label class="lm" for="vkl3">Обеспечение</label>
  <input type="radio" name="odin" id="vkl4"/>					<label class="lm" for="vkl4">Юрлицо</label>
  <input type="radio" name="odin" id="vkl5"/>					<label class="lm" for="vkl5">Кр. история</label>
  <input type="radio" name="odin" id="vkl6"/>					<label class="lm" for="vkl6">Стоп-факторы</label>

<div>
  
<select class="inp" form="be_form" name="be_bank_name" >
<option value="-1">Выберите банк</option>
<? 
$post_fail = false;
foreach ($banks2 as $key=>$value) {
	$name = $value['name'];
	$id = $value['id'];
	$checked_ = "";
	//if ($post_fail)
	//	$checked_ = ($key==$be_bank_name) ? "selected" : $checked_ ; 

    echo '<option value="'.$id.'" '."$checked_>$name</option>";
};
?>
</select>
&nbsp;&nbsp;<input class="inp" form="be_form" name="be_product_name" type="text" placeholder="Наименование кредитного продукта" size="50" value="">
&nbsp;&nbsp;<label><input class="inp" form="be_form" name="be_active" type="checkbox" value="1"> Активный?</label>
<br><label><input class="inp" form="be_form" name="be_online_av" type="checkbox" value="1"> Доступна онлайн подача?</label>

<br>Список регионов: <label><input class="inp" form="be_form" name="be_reg_miss" type="checkbox" value="1" onclick="javascript:reg_flags(this, 'be_reg_all', 'be_reg_miss');"> Кроме регионов</label>
<ul name="reg_params" id="reg_params"></ul>
<input name="reg_name" id="reg_name" type="text" size="50" oninput="javascript:reg_help(this);"> <button onclick="javascript:add_reg();">Добавить регион</button>
<i class="fil5" style="display:none;"></i><ul name="rsl" class="fil2" style="display:none;"></ul>
<br>Тип продукта:
<br><label><input form="be_form" name="be_goal[0]" type="checkbox" value="1">Нецелевой</label>
<label class = "tab2"><input form="be_form" name="be_goal[1]" type="checkbox" value="1">Ипотека</label>
<label class = "tab3"><input form="be_form" name="be_goal[2]" type="checkbox" value="1">Гарантия</label>
<label class = "tab4"><input form="be_form" name="be_goal[3]" type="checkbox" value="1">Лизинг</label>
<label class = "tab5"><input form="be_form" name="be_goal[4]" type="checkbox" value="1">Рефинансирование</label>
<label class = "tab7"><input form="be_form" name="be_goal[5]" type="checkbox" value="1">Карты</label>
<br>Цель кредита:
<br><label><input form="be_form" name="be_goalc[0]" type="checkbox" value="1">Открытие нового бизнеса</label>
<label class = "tab2"><input form="be_form" name="be_goalc[1]" type="checkbox" value="1">Развитие действующего бизнеса</label>
<label class = "tab5"><input form="be_form" name="be_goalc[2]" type="checkbox" value="1">Рефинансирование имеющихся кредитов</label><br>
<label class = "tab0"><input form="be_form" name="be_goalc[3]" type="checkbox" value="1">Покупка недвижимости</label>
<label class = "tab2"><input form="be_form" name="be_goalc[4]" type="checkbox" value="1">Другие цели</label>
<br>Прожиточный минимум : <input class="tab" form="be_form" name="be_pm" type="text" placeholder="Рублей" size="10" value="">
<br>П/Д, % : <input class="tab" form="be_form" name="be_pd" type="text" placeholder="П/Д, %" size="10" value="">

<br><label><input class="inp" form="be_form" name="be_pd_set" type="checkbox" value="1"> Настраиваемый П/Д</label>
<br>
<span>
	<span id="be_pd_wrap_data"></span>
	<span id="be_pd_wrap_set">Новое условие: 
			<input id="set_pd_min" type="text" size="4" placeholder="мин">
			<select id="set_pd_min_sel">
				<option value="-1">Условие</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="=">=</option>
			</select>
			FICO
			<select id="set_pd_max_sel">
				<option value="-1" >Условие</option>
				<option value="<" ><</option>
				<option value="<=" ><=</option>
				<option value="=" >=</option>
			</select>
			<input id="set_pd_max" type="text" size="4" placeholder="макс"> соответствует 
		<input id="set_pd_val" type="text" size="4" placeholder="П/Д, %">
		<button onclick="javascript:add_pd_set();">Добавить условие</button>
	</span>
</span>

<br><label><input class="inp" form="be_form" name="be_pd_miss" type="checkbox" value="1"> Рассчитывать лимит по "Максимально возможному платежу"</label>
<br>LTV, % : <input class="tab" form="be_form" name="be_discont" type="text" placeholder="Дисконт, %" size="10" value="">
<br>Максимальный срок кредита <input class="tab" form="be_form" name="be_max_cre_time" type="text" placeholder="месяцев" size="10" value="">
<br>Минимальный срок кредита <input class="tab" form="be_form" name="be_min_cre_time" type="text" placeholder="месяцев" size="10" value="">
<br><label><input class="inp" form="be_form" name="be_res" type="checkbox" value="1"> Кредитует неграждан РФ?</label>
<br>Кредитует "улицу"?<select class="tab" form="be_form" name="be_cr4other" >
	<option value="-1">Выберите</option>
	<option value="1" >Кредитует "улицу"</option>
	<option value="0" >Не кредитует "улицу"</option>
</select>
<br><font color="red">Только</font> для клиентов банка, имеющих:
<br><label><input class="inp" form="be_form" name="be_bank_zp" type="checkbox" value="1"> з/п счета в банке</label>
<br><label><input class="inp" form="be_form" name="be_bank_acc" type="checkbox" value="1"> личные счета в банке (кроме з/п счетов)</label>
<br>Мин. ставка, % : <input class="tab" form="be_form" name="be_min_percent" type="text" placeholder="Мин. ставка, %" size="10" value="">
<br>Макс. ставка, % : <input class="tab" form="be_form" name="be_max_percent" type="text" placeholder="Макс. ставка, %" size="10" value="">
<br>Мин. кредит, р : <input class="tab" form="be_form" name="be_min_credit" type="text" placeholder="Мин. кредит, р" size="15" value="">
<br>Макс. кредит, р : <input class="tab" form="be_form" name="be_max_credit" type="text" placeholder="Макс. кредит, р" size="15" value="">
<br>Мин. доход, р : <input class="tab" form="be_form" name="be_min_income" type="text" placeholder="Мин. доход, р" size="15" value="">
<br>Мин. м. возраст : <input class="tab" form="be_form" name="be_min_age_m" type="text" placeholder="Мин. м. возраст" size="15" value="">
<br>Мин. ж. возраст : <input class="tab" form="be_form" name="be_min_age_w" type="text" placeholder="Мин. ж. возраст" size="15" value="">
<br>Макс. м. возраст : <input class="tab" form="be_form" name="be_max_age_m" type="text" placeholder="Макс. м. возраст" size="15" value="">
<label class = "tab4">На момент погашения, м : </label><input class="tab6" form="be_form" name="be_max_end_age_m" type="text" placeholder="лет" size="15" value="">
<br>Макс. ж. возраст : <input class="tab" form="be_form" name="be_max_age_w" type="text" placeholder="Макс. ж. возраст" size="15" value="">
<label class = "tab4">На момент погашения, ж : </label><input class="tab6" form="be_form" name="be_max_end_age_w" type="text" placeholder="лет" size="15" value="">
<div style="display:none;"><br>Мин. срок получения, дней : <input class="tab" form="be_form" name="be_lim_time" type="text" placeholder="Мин. срок получения, дней" size="25" value=""></div>
<br>Минимальный балл FICO: <input class="tab" form="be_form" name="be_nbki" type="text" placeholder="" size="25" value="">
<br><span>
	<span id="be_fico_wrap_data"></span>
	<span id="be_fico_wrap_set">Новое условие: 
			<input id="set_fico_min" type="text" size="4" placeholder="мин">
			<select id="set_fico_min_sel">
				<option value="-1">Условие</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="=">=</option>
			</select>
			FICO
			<select id="set_fico_max_sel">
				<option value="-1" >Условие</option>
				<option value="<" ><</option>
				<option value="<=" ><=</option>
				<option value="=" >=</option>
			</select>
			<input id="set_fico_max" type="text" size="4" placeholder="макс">
		<button onclick="javascript:add_fico_set();">Добавить условие</button>
	</span>
</span>
<?
/*
<br><label><input form="be_form" name="be_nbki[0]" type="checkbox" value="1">0-250</label>
<label class = "tab1"><input form="be_form" name="be_nbki[1]" type="checkbox" value="1">251-500</label>
<label class = "tab2"><input form="be_form" name="be_nbki[2]" type="checkbox" value="1">501-750</label>
<label class = "tab3"><input form="be_form" name="be_nbki[3]" type="checkbox" value="1">751-999</label>
*/
?>
<br>Армия (для мужчин)
<label style="display:none;"><input class="inp" form="be_form" name="be_arm[0]" type="checkbox" value="1">Военный билет</label>
<label class = "tab2" style="display:none;"><input class="inp" form="be_form" name="be_arm[1]" type="checkbox" value="1">Старше призывного</label>
<label class="tab4" style="display:none;"><input form="be_form" name="be_arm[2]" type="checkbox" value="1">Отсрочка</label>
<br><label class = "inp"><input class="inp" form="be_form" name="be_arm[3]" type="checkbox" value="1">Подлежит призыву</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_arm[4]" type="checkbox" value="1">НЕ подлежит призыву</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_arm[5]" type="checkbox" value="1">Военнослужащий</label>
<br>Прописка: <label><input class="inp" form="be_form" name="be_adr_miss" type="checkbox" value="1" onclick="javascript:reg_flags(this, 'be_adr_all', 'be_adr_miss');"> Кроме регионов</label>
<?
/*<br><label><input class="inp" form="be_form" name="be_adr[0]" type="checkbox" value="1">Москва</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_adr[1]" type="checkbox" value="1">Московская обл</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_adr[2]" type="checkbox" value="1">Иной регион</label>*/
?>
<ul name="adr_params" id="adr_params"></ul>
<span><input name="adr_name" id="adr_name" type="text" size="50" oninput="javascript:reg_help(this);"> <button onclick="javascript:add_reg_adr();">Добавить регион</button>
<i class="fil5" style="display:none;"></i><ul name="rsl" class="fil2" style="display:none;"></ul></span>
<br><label><input class="inp" form="be_form" name="be_adr_nan" type="checkbox" value="1"> Допускается отсутствие прописки</label>
<br>Мин. срок прописки, мес : <input class="tab" form="be_form" name="be_adr_time" type="text" placeholder="Мин. срок прописки, мес" size="25" value="">
<br><textarea class="inp" form="be_form" rows="10" cols="100" name="be_description" placeholder="Описание продукта: ставки, сроки, прочие условия..."><?=($post_fail)?$be_description:""?></textarea>
</div>
  

<div>
<label><input class="inp" form="be_form" name="be_pfr" type="checkbox" value="1">Требуются отчисления в ПФР по фонду ЗП</label>
<br>Трудоустройство:
<br><label><input class="inp" form="be_form" name="be_work[0]" type="checkbox" value="1">Госслужащий</label>
<label><input class="inp" form="be_form" name="be_work[1]" type="checkbox" value="1">Владелец бизнеса</label>
<label><input class="inp" form="be_form" name="be_work[2]" type="checkbox" value="1">Работа по найму</label>
<label><input class="inp" form="be_form" name="be_work[3]" type="checkbox" value="1">Индивидуальный предприниматель</label><br> 
<label><input class="inp" form="be_form" name="be_work[4]" type="checkbox" value="1">Неофициально</label> 
<label><input class="inp" form="be_form" name="be_work[5]" type="checkbox" value="1">Не трудоустроен</label> 
<label><input class="inp" form="be_form" name="be_work[6]" type="checkbox" value="1">Пенсионер</label>
<label><input class="inp" form="be_form" name="be_work[7]" type="checkbox" value="1">По трудовому договору</label> 
<label><input class="inp" form="be_form" name="be_work[8]" type="checkbox" value="1">Учусь</label> 
<label><input class="inp" form="be_form" name="be_work[9]" type="checkbox" value="1">Иное</label> 
<label style="display:none;"><input class="inp" form="be_form" name="be_work[10]" type="checkbox" value="1">Частная практика</label> 
<label style="display:none;"><input class="inp" form="be_form" name="be_work[11]" type="checkbox" value="1">По срочному контракту</label> 
<label><input class="inp" form="be_form" name="be_work[12]" type="checkbox" value="1">Работа по найму в ИП</label> 

<br>Активы
<br><label><input class="inp" form="be_form" name="be_activs[0]" type="checkbox" value="1">Квартира</label>
<br><label><input class="inp" form="be_form" name="be_activs[1]" type="checkbox" value="1">Дом</label>
<br><label><input class="inp" form="be_form" name="be_activs[2]" type="checkbox" value="1">Нет активов</label> 
<br><label><input class="inp" form="be_form" name="be_activs[3]" type="checkbox" value="1">Спецтехника</label> 
<br><label><input class="inp" form="be_form" name="be_activs[4]" type="checkbox" value="1">Иномарка</label>
<br><label><input class="inp" form="be_form" name="be_activs[5]" type="checkbox" value="1">Коттедж</label>
<br><label><input class="inp" form="be_form" name="be_activs[6]" type="checkbox" value="1">Таунхаус</label>
<br><label><input class="inp" form="be_form" name="be_activs[7]" type="checkbox" value="1">Коммерческое помещение</label>

<br>Подтверждение трудоустройства:
<br><label><input class="inp" form="be_form" name="be_c_work[0]" type="checkbox" value="1">Трудовая книжка</label>
<br><label><input class="inp" form="be_form" name="be_c_work[1]" type="checkbox" value="1">Договор/Контракт</label>
<br><label><input class="inp" form="be_form" name="be_c_work[2]" type="checkbox" value="1">Свидетельство ИП</label>
<br><label><input class="inp" form="be_form" name="be_c_work[3]" type="checkbox" value="1">По телефону</label> 
<br><label><input class="inp" form="be_form" name="be_c_work[4]" type="checkbox" value="1">Нет</label> 
<label style="display:none;"><input class="inp" form="be_form" name="be_c_work[5]" type="checkbox" value="1">Договор аренды помещения</label> 

<br>Подтверждение дохода:
<br><label><input class="inp" form="be_form" name="be_c_inc[0]" type="checkbox" value="1">Справка по форме банка</label>
<br><label><input class="inp" form="be_form" name="be_c_inc[1]" type="checkbox" value="1">Справка 2-НДФЛ</label>
<br><label><input class="inp" form="be_form" name="be_c_inc[2]" type="checkbox" value="1">В устной форме по телефону</label>
<br><label><input class="inp" form="be_form" name="be_c_inc[3]" type="checkbox" value="1">Налоговая декларация</label> 
<br><label><input class="inp" form="be_form" name="be_c_inc[4]" type="checkbox" value="1">патент</label> 
<br><label><input class="inp" form="be_form" name="be_c_inc[5]" type="checkbox" value="1">договор</label> 
<br><label><input class="inp" form="be_form" name="be_c_inc[6]" type="checkbox" value="1">Без подтверждения</label> 
<br><label><input class="inp" form="be_form" name="be_c_inc[7]" type="checkbox" value="1">Выписка по личному счету</label> 

<br>Стаж работы на текущем месте, мес <input class="tab3" form="be_form" name="be_exp_now_new" type="text" placeholder="максимум" size="15" value="">
<span style ="display:none;">
<br><label><input class="inp" form="be_form" name="be_exp_now[0]" type="checkbox" value="1">менее 3</label>
<br><label><input class="inp" form="be_form" name="be_exp_now[1]" type="checkbox" value="1">3-6</label>
<br><label><input class="inp" form="be_form" name="be_exp_now[2]" type="checkbox" value="1">7-12</label> 
<br><label><input class="inp" form="be_form" name="be_exp_now[3]" type="checkbox" value="1">13-24</label> 
<br><label><input class="inp" form="be_form" name="be_exp_now[4]" type="checkbox" value="1">25-36</label> 
<br><label><input class="inp" form="be_form" name="be_exp_now[5]" type="checkbox" value="1">37 и более</label> 
</span>
<?
/*
<br><select class="inp" form="be_form" name="be_exp_now" >
	<option value="-1">Выберите</option>
	<option value="0" >менее 3</option>
	<option value="1" >3-6</option>
	<option value="2" >7-12</option>
	<option value="3" >13-24</option>
	<option value="4" >25-36</option>
	<option value="5" >37 и более</option>

</select>*/
?>
<br>Дата регистрации предприятия, мес
<input class="tab3" form="be_form" name="be_reg_date_new" type="text" placeholder="максимум" size="15" value="">
<span style ="display:none;">
<br><label><input class="inp" form="be_form" name="be_reg_date[0]" type="checkbox" value="1">менее 6</label>
<br><label><input class="inp" form="be_form" name="be_reg_date[1]" type="checkbox" value="1">6-12</label> 
<br><label><input class="inp" form="be_form" name="be_reg_date[2]" type="checkbox" value="1">13-24</label> 
<br><label><input class="inp" form="be_form" name="be_reg_date[3]" type="checkbox" value="1">25-36</label> 
<br><label><input class="inp" form="be_form" name="be_reg_date[4]" type="checkbox" value="1">37 и более</label> 
</span>
<?
/*
<br><select class="inp" form="be_form" name="be_reg_date" >
	<option value="-1">Выберите</option>
	<option value="0" >менее 6</option>
	<option value="1" >6-12</option>
	<option value="2" >13-24</option>
	<option value="3" >25-36</option>
	<option value="4" >37 и более</option>
</select>*/
?>
<br>Сфера деятельности: <span style="cursor: pointer;" onclick="all_w_types();"><i>Выбрать все/снять выделение</i></span>
<?

if ($result = $db_connect->query("SELECT * FROM work_types ORDER BY id ASC;"))
{
		
	while ($a = $result->fetch_array(MYSQLI_ASSOC))
	{
		$wid = $a["id"]-1;
		echo '<br><label><input class="inp" form="be_form" name="be_work_type['.$wid.']" type="checkbox" value="1">'.$a["descr"].'</label>';
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

?>
<br>
Фактический адрес места работы <label><input class="inp" form="be_form" name="be_wa_miss" type="checkbox" value="1" onclick="javascript:reg_flags(this, 'be_work_adr_all', 'be_wa_miss');"> Кроме регионов</label>
<?
/*<br><label><input class="inp" form="be_form" name="be_work_adr[0]" type="checkbox" value="1">Москва</label>
<br><label><input class="inp" form="be_form" name="be_work_adr[1]" type="checkbox" value="1">Московская обл</label>
<br><label><input class="inp" form="be_form" name="be_work_adr[2]" type="checkbox" value="1">Иной регион</label> 
*/
?>
<label style="display:none;"><input class="inp" form="be_form" name="be_work_adr[3]" type="checkbox" value="1">Нет адреса</label> 

<ul name="work_adr_params" id="work_adr_params"></ul>
<span><input name="work_adr_name" id="work_adr_name" type="text" size="50" oninput="javascript:reg_help(this);"> <button onclick="javascript:add_reg_work_adr();">Добавить регион</button>
<i class="fil5" style="display:none;"></i><ul name="rsl" class="fil2" style="display:none;"></ul>
</span>
<br><br>

  
</div>


<div>
<label>Не анализировать залог<input class="tab3" form="be_form" name="be_bail_miss" type="checkbox" value="1"></label>
<br>
<label>Продукт с залогом?<input class="tab3" form="be_form" name="be_bailn" type="checkbox" value="1"></label>
<br>Необходимость поручительства
<label class="tab3" ><input form="be_form" name="be_guarantor[0]" type="checkbox" value="1">физлицо</label>
<label class="tab4"><input form="be_form" name="be_guarantor[1]" type="checkbox" value="1">юрлицо</label>
<br>Возможность залога от третьего лица
<label class="tab3" ><input class="inp" form="be_form" name="be_bail[0]" type="checkbox" value="1">физлицо</label>
<label class="tab4" ><input class="inp" form="be_form" name="be_bail[1]" type="checkbox" value="1">юрлицо</label> 
<br>Первоначальный взнос не менее <input class="tab3" form="be_form" name="be_first_deb" type="text" placeholder="минимум" size="15" value=""> %
<br>Удаленность от мкад не более <input class="tab3" form="be_form" name="be_mkad" type="text" placeholder="максимум" size="15" value=""> км
<br>Документы на право собственности:
<label class="tab3"><input class="inp" form="be_form" name="be_doc_sob[0]" type="checkbox" value="1">ДДУ</label>
<label class="tab4"><input class="inp" form="be_form" name="be_doc_sob[1]" type="checkbox" value="1">свидетельство</label>
<label class="tab6"><input class="inp" form="be_form" name="be_doc_sob[2]" type="checkbox" value="1">не стандарт</label>
<label class="tab7"><input class="inp" form="be_form" name="be_doc_sob[3]" type="checkbox" value="1">Выписка ЕГРП</label>
<br>Владелец:
<br><label><input class="inp" form="be_form" name="be_owner[0]" type="checkbox" value="1">1 собственник</label>
<label><input class="inp" form="be_form" name="be_owner[1]" type="checkbox" value="1">долевая собственность</label>
<label><input class="inp" form="be_form" name="be_owner[2]" type="checkbox" value="1">юрлицо</label>
<br>Вид залога:
<br><label><input class="inp" form="be_form" name="be_obj_type[0]" type="checkbox" value="1">дом</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_obj_type[1]" type="checkbox" value="1">коттедж</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_obj_type[2]" type="checkbox" value="1">таунхаус</label>
<label class = "tab3" style="display:none;"><input class="inp" form="be_form" name="be_obj_type[3]" type="checkbox" value="1">земля</label>
<label class = "tab4" style="display:none;"><input class="inp" form="be_form" name="be_obj_type[4]" type="checkbox" value="1">дачный дом</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_obj_type[5]" type="checkbox" value="1">квартира</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_obj_type[6]" type="checkbox" value="1">комната</label>
<label class = "tab5"><input class="inp" form="be_form" name="be_obj_type[7]" type="checkbox" value="1">доля в квартире</label>
<br><label><input class="inp" form="be_form" name="be_obj_type[8]" type="checkbox" value="1">помещение коммерческого назначения</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_obj_type[9]" type="checkbox" value="1">гараж</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_obj_type[10]" type="checkbox" value="1">Иномарка</label>
<label class = "tab5"><input class="inp" form="be_form" name="be_obj_type[11]" type="checkbox" value="1">спецтехника</label>
<label class = "tab7"><input class="inp" form="be_form" name="be_obj_type[12]" type="checkbox" value="1">отечественный автомобиль</label>
<br><label style="display:none;"><input class="inp" form="be_form" name="be_obj_type[13]" type="checkbox" value="1">оборудование</label>
<label class = "tab2" style="display:none;"><input class="inp" form="be_form" name="be_obj_type[14]" type="checkbox" value="1">товары</label>
<br>Перепланировки:
<br><label><input class="inp" form="be_form" name="be_replan[0]" type="checkbox" value="1">нет</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_replan[1]" type="checkbox" value="1">узаконена</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_replan[2]" type="checkbox" value="1">не узаконена</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_replan[3]" type="checkbox" value="1">невозможно узаконить</label>
<br>Материал стен частного дома
<br><label><input class="inp" form="be_form" name="be_house_mat[0]" type="checkbox" value="1">цельное дерево</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_house_mat[1]" type="checkbox" value="1">деревянный каркас</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_house_mat[2]" type="checkbox" value="1">каркас из иного материала</label>
<label class = "tab7"><input class="inp" form="be_form" name="be_house_mat[3]" type="checkbox" value="1">кирпич</label>
<br><label><input class="inp" form="be_form" name="be_house_mat[4]" type="checkbox" value="1">бетон</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_house_mat[5]" type="checkbox" value="1">шлакоблоки</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_house_mat[6]" type="checkbox" value="1">камень</label>
<br>Фундамент
<br><label><input class="inp" form="be_form" name="be_found[0]" type="checkbox" value="1">нет</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_found[1]" type="checkbox" value="1">ленточный</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_found[2]" type="checkbox" value="1">свайный</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_found[3]" type="checkbox" value="1">монолитный</label>
<br>Возраст постройки, полных лет <input class="tab4" form="be_form" name="be_obj_age" type="text" placeholder="максимум" size="15" value="">
<br>Документально установленный процент износа <input class="tab4" form="be_form" name="be_wear" type="text" placeholder="максимум, %" size="15" value="">
<br>Обязательные коммуникации
<br><label><input class="inp" form="be_form" name="be_comm[0]" type="checkbox" value="1">электричество</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_comm[1]" type="checkbox" value="1">вода</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_comm[2]" type="checkbox" value="1">отопление</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_comm[3]" type="checkbox" value="1">газ</label>
<label class = "tab5"><input class="inp" form="be_form" name="be_comm[4]" type="checkbox" value="1">канализация</label>
<label class = "tab7"><input class="inp" form="be_form" name="be_comm[5]" type="checkbox" value="1">Нет</label>
<br>Параметры по видам многоквартирных домов:
<br><label><input class="inp" form="be_form" name="be_bh_type[0]" type="checkbox" value="1">1</label>
<label class = "tab1"><input class="inp" form="be_form" name="be_bh_type[1]" type="checkbox" value="1">2</label>
<label class = "tab2"><input class="inp" form="be_form" name="be_bh_type[2]" type="checkbox" value="1">3</label>
<label class = "tab3"><input class="inp" form="be_form" name="be_bh_type[3]" type="checkbox" value="1">4</label>
<label class = "tab4"><input class="inp" form="be_form" name="be_bh_type[4]" type="checkbox" value="1">5</label>
<label class = "tab5"><input class="inp" form="be_form" name="be_bh_type[5]" type="checkbox" value="1">6</label>
<label class = "tab6"><input class="inp" form="be_form" name="be_bh_type[6]" type="checkbox" value="1">7</label>


</div>


<div>
<label>Не анализировать данные по юрлицу<input class="inp" form="be_form" name="be_ul_miss" type="checkbox" value="1"></label>
<br>Участие юрлица в сделке в качестве
<br><label><input class="inp" form="be_form" name="be_ent[0]" type="checkbox" value="1">заемщика</label>
<label><input class="inp" form="be_form" name="be_ent[1]" type="checkbox" value="1">поручителя</label>
<label><input class="inp" form="be_form" name="be_ent[2]" type="checkbox" value="1">залогодатель</label>
<br><label><input class="inp" form="be_form" name="be_ca" type="checkbox" value="1">Требуется расчетный счет у ИП/юрлица</label>
<br>Доля в уставном капитале, %: <input class="tab4" form="be_form" name="be_firm_per" type="text" placeholder="Доля в уставном капитале, %" size="25" value="">
<br>Максимально допустимая доля владения бизнесом, %: <input class="tab4" form="be_form" name="be_firm_per_max" type="text" placeholder="Максимум, %" size="25" value="">
<br>Минимально допустимая доля владения бизнесом, %: <input class="tab4" form="be_form" name="be_firm_per_min" type="text" placeholder="Минимум, %" size="25" value="">
<br>Система налогообложения:
<br><label><input class="inp" form="be_form" name="be_nal[0]" type="checkbox" value="1">обычная</label>
<label><input class="inp" form="be_form" name="be_nal[1]" type="checkbox" value="1">усн6%</label>
<label><input class="inp" form="be_form" name="be_nal[2]" type="checkbox" value="1">усн15%</label>
<label><input class="inp" form="be_form" name="be_nal[3]" type="checkbox" value="1">патент для ИП</label>
<label><input class="inp" form="be_form" name="be_nal[4]" type="checkbox" value="1">ЕНВД</label>
<br>Общая выручка за посл. 4 квартала в рублях (мин): <input class="inp" form="be_form" name="be_proc" type="text" placeholder="Минимальная" size="15" value="">
<br>Общая выручка за посл. 4 квартала в рублях (макс): <input class="inp" form="be_form" name="be_proc_max" type="text" placeholder="Максимальная" size="15" value="">
<br>Прибыль за последние 4 квартала по налоговой отчетности:
<br><label><input class="inp" form="be_form" name="be_pr_nal[0]" type="checkbox" value="1">0</label>
<label><input class="inp" form="be_form" name="be_pr_nal[1]" type="checkbox" value="1">прибыль</label>
<label><input class="inp" form="be_form" name="be_pr_nal[2]" type="checkbox" value="1">убыток</label>
<br>Прибыль реальная за последние 4 квартала в рублях <input class="inp" form="be_form" name="be_pr_4" type="text" placeholder="минимальная" size="15" value="">
<br>Имущество организации в М/МО:
<br><label><input class="inp" form="be_form" name="be_est[0]" type="checkbox" value="1">здания</label>
<br><label><input class="inp" form="be_form" name="be_est[1]" type="checkbox" value="1">помещения</label>
<br><label><input class="inp" form="be_form" name="be_est[2]" type="checkbox" value="1">техника</label>
<br><label><input class="inp" form="be_form" name="be_est[3]" type="checkbox" value="1">оборудование</label>
<br><label><input class="inp" form="be_form" name="be_est[4]" type="checkbox" value="1">товары</label>
<br><label><input class="inp" form="be_form" name="be_est[5]" type="checkbox" value="1">земля</label>
<br><label><input class="inp" form="be_form" name="be_est[6]" type="checkbox" value="1">незавершенное строительство</label>
<br><label><input class="inp" form="be_form" name="be_est[7]" type="checkbox" value="1">Нет имущества</label>
<br>Действующие кредитные обязательства юрлица (лимит) <input class="inp" form="be_form" name="be_co" type="text" placeholder="максимальный" size="15" value=""> 
<br>Количество активных кредитных обязательств юрлица всего <input class="inp" form="be_form" name="be_co_umax" type="text" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_delay_29" type="checkbox" value="1">Допускается текущая просрочка</label>
<br>Наихудший  платежный статус юрлица:
<br><label><input class="inp" form="be_form" name="be_psc[0]" type="checkbox" value="1">просрочки от 1 до 29 по закрытым</label>
<input class="tab3" form="be_form" name="be_psc_u0" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_psc[1]" type="checkbox" value="1">просрочки от 30 до 59 по закрытым</label>
<input class="tab3" form="be_form" name="be_psc_u1" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_psc[2]" type="checkbox" value="1">просрочки от 60 до 89 по закрытым</label>
<input class="tab3" form="be_form" name="be_psc_u2" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_psc[3]" type="checkbox" value="1">просрочки от 90 до 119 по закрытым</label>
<input class="tab3" form="be_form" name="be_psc_u3" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_psc[4]" type="checkbox" value="1">просрочки более 120 по закрытым</label>
<input class="tab3" form="be_form" name="be_psc_u4" type="number" placeholder="максимум" size="15" value="">
<br>
<br><label><input class="inp" form="be_form" name="be_ps[0]" type="checkbox" value="1">просрочки от 1 до 29 по активным</label>
<input class="tab3" form="be_form" name="be_ps_u0" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_ps[1]" type="checkbox" value="1">просрочки от 30 до 59 по активным</label>
<input class="tab3" form="be_form" name="be_ps_u1" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_ps[2]" type="checkbox" value="1">просрочки от 60 до 89 по активным</label>
<input class="tab3" form="be_form" name="be_ps_u2" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_ps[3]" type="checkbox" value="1">просрочки от 90 до 119 по активным</label>
<input class="tab3" form="be_form" name="be_ps_u3" type="number" placeholder="максимум" size="15" value="">
<br><label><input class="inp" form="be_form" name="be_ps[4]" type="checkbox" value="1">просрочки более 120 по активным</label>
<input class="tab3" form="be_form" name="be_ps_u4" type="number" placeholder="максимум" size="15" value="">


  
  
</div>






<div>
<label><input class="inp" form="be_form" name="be_delay_act" type="checkbox" value="1">Допускается текущая просрочка</label><br>
<label><input class="inp" form="be_form" name="be_beznD" type="checkbox" value="1">Допускается наличие в прошлом безнадежный долг/передано на взыскание</label><br>
<label><input class="inp" form="be_form" name="be_cr_his" type="checkbox" value="1">Наличие кредитной истории</label>
<br>Количество активных кредитных обязательств всего <input class="inp" form="be_form" name="be_co_max" type="text" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_zaim_mfo" type="checkbox" value="1">Активные займы МФО (на суммы до 100 т.р) (допускаются или нет)</label>
<br>Общий лимит <input class="inp" form="be_form" name="be_lim_all" type="text" placeholder="Общий лимит, максимум" size="15" value="">
<select class="inp" form="be_form" name="be_lim_val" >
	<option value="-1">Выберите</option>
	<option value="0" >руб</option>
	<option value="1" >дол</option>
	<option value="2" >евро</option>
	<option value="3" >другая валюта</option>
</select>
<br>Текущий балланс
<input class="inp" form="be_form" name="be_bal_now" type="text" placeholder="Текущий баланс, максимум" size="15" value="">
<select class="inp" form="be_form" name="be_bal_val" >
	<option value="-1">Выберите</option>
	<option value="0" >руб</option>
	<option value="1" >дол</option>
	<option value="2" >евро</option>
	<option value="3" >другая валюта</option>
</select>
<br>Наихудший платежный статус по закрытым

<br>
<table id="t_close">
	<tr>
		<td></td>
		<td class="t_close_td"><label>За 3 мес. <input type="checkbox" form="be_form" name="be_psct[0]" value="1"  onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 6 мес. <input type="checkbox" form="be_form" name="be_psct[1]" value="1"  onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 12 мес. <input type="checkbox" form="be_form" name="be_psct[2]" value="1" onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 18 мес. <input type="checkbox" form="be_form" name="be_psct[3]" value="1" onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 24 мес. <input type="checkbox" form="be_form" name="be_psct[4]" value="1" onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 36 мес. <input type="checkbox" form="be_form" name="be_psct[5]" value="1" onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>За 60 мес. <input type="checkbox" form="be_form" name="be_psct[6]" value="1" onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>>60 мес. <input type="checkbox" form="be_form" name="be_psct[7]" value="1"   onchange="set_psct();"></label></td>
		<td class="t_close_td"><label>Общее <input type="checkbox" form="be_form" name="be_psct[8]" value="1"      onchange="set_psat();"></label></td>
	</tr>
	<tr>
		<td>1-29</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct3[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct6[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct12[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct18[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct24[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct36[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60p[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psctall[0]"></td>
	</tr>
	<tr>
		<td>30-59</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct3[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct6[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct12[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct18[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct24[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct36[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60p[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psctall[1]"></td>
	</tr>
	<tr>
		<td>60-89</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct3[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct6[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct12[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct18[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct24[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct36[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60p[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psctall[2]"></td>
	</tr>
	<tr>
		<td>90-119</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct3[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct6[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct12[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct18[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct24[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct36[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60p[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psctall[3]"></td>
	</tr>
	<tr>
		<td>120+</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct3[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct6[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct12[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct18[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct24[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct36[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psct60p[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psctall[4]"></td>
	</tr>
	<tr>
		<td></td>
		<td class="t_close_td"><button id="but_psct3"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct6"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct12"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct18"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct24"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct36"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct60"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psct60p"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psctall"	onclick="copy_column(this.id, 0, this.value);" value="copy">Скопировать</button></td>
	</tr>
</table>

<br>


<label class="tab5">Наихудший платежный статус за 6 месяцев</label>

<br><label><input class="inp" form="be_form" name="be_ps_c[0]" type="checkbox" value="1">просрочки от 1 до 29 по закрытым</label>
<input class="tab3" form="be_form" name="be_ps_v0" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_6[0]" type="checkbox" value="1">просрочки от 1 до 29</label>
<input class="tab7" form="be_form" name="be_ps_6v0" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_ps_c[1]" type="checkbox" value="1">просрочки от 30 до 59 по закрытым</label>
<input class="tab3" form="be_form" name="be_ps_v1" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_6[1]" type="checkbox" value="1">просрочки от 30 до 59</label>
<input class="tab7" form="be_form" name="be_ps_6v1" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_ps_c[2]" type="checkbox" value="1">просрочки от 60 до 89 по закрытым</label>
<input class="tab3" form="be_form" name="be_ps_v2" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_6[2]" type="checkbox" value="1">просрочки от 60 до 89</label>
<input class="tab7" form="be_form" name="be_ps_6v2" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_ps_c[3]" type="checkbox" value="1">просрочки от 90 до 119 по закрытым</label>
<input class="tab3" form="be_form" name="be_ps_v3" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_6[3]" type="checkbox" value="1">просрочки от 90 до 119</label>
<input class="tab7" form="be_form" name="be_ps_6v3" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_ps_c[4]" type="checkbox" value="1">просрочки более 120 по закрытым</label>
<input class="tab3" form="be_form" name="be_ps_v4" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_6[4]" type="checkbox" value="1">просрочки более 120</label>
<input class="tab7" form="be_form" name="be_ps_6v4" type="number" placeholder="максимум" size="15" value="">

<br>Наихудший платежный статус по активным

<br>

<table id="t_open">
	<tr>
		<td></td>
		<td class="t_close_td"><label>За 3 мес. <input type="checkbox" form="be_form" name="be_psat[0]" value="1"  onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 6 мес. <input type="checkbox" form="be_form" name="be_psat[1]" value="1"  onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 12 мес. <input type="checkbox" form="be_form" name="be_psat[2]" value="1" onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 18 мес. <input type="checkbox" form="be_form" name="be_psat[3]" value="1" onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 24 мес. <input type="checkbox" form="be_form" name="be_psat[4]" value="1" onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 36 мес. <input type="checkbox" form="be_form" name="be_psat[5]" value="1" onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>За 60 мес. <input type="checkbox" form="be_form" name="be_psat[6]" value="1" onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>>60 мес. <input type="checkbox" form="be_form" name="be_psat[7]" value="1"   onchange="set_psat();"></label></td>
		<td class="t_close_td"><label>Общее <input type="checkbox" form="be_form" name="be_psat[8]" value="1"      onchange="set_psat();"></label></td>
	</tr>
	<tr>
		<td>1-29</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat3[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat6[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat12[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat18[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat24[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat36[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60p[0]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psatall[0]"></td>
	</tr>
	<tr>
		<td>30-59</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat3[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat6[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat12[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat18[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat24[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat36[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60p[1]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psatall[1]"></td>
	</tr>
	<tr>
		<td>60-89</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat3[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat6[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat12[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat18[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat24[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat36[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60p[2]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psatall[2]"></td>
	</tr>
	<tr>
		<td>90-119</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat3[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat6[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat12[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat18[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat24[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat36[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60p[3]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psatall[3]"></td>
	</tr>
	<tr>
		<td>120+</td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat3[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat6[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat12[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat18[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat24[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat36[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psat60p[4]"></td>
		<td class="t_close_td"><input type="text" form="be_form" name="be_psatall[4]"></td>
	</tr>
	<tr>
		<td></td>
		<td class="t_close_td"><button id="but_psat3"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat6"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat12"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat18"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat24"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat36"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat60"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psat60p"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
		<td class="t_close_td"><button id="but_psatall"	onclick="copy_column(this.id, 1, this.value);" value="copy">Скопировать</button></td>
	</tr>
</table>

<br>

<label class="tab5">Наихудший платежный статус за 12 месяцев</label>

<br><label><input class="inp" form="be_form" name="be_psa_c[0]" type="checkbox" value="1">просрочки от 1 до 29 по активным</label>
<input class="tab3" form="be_form" name="be_psa_v0" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_12[0]" type="checkbox" value="1">просрочки от 1 до 29</label>
<input class="tab7" form="be_form" name="be_ps_12v0" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_psa_c[1]" type="checkbox" value="1">просрочки от 30 до 59 по активным</label>
<input class="tab3" form="be_form" name="be_psa_v1" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_12[1]" type="checkbox" value="1">просрочки от 30 до 59</label>
<input class="tab7" form="be_form" name="be_ps_12v1" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_psa_c[2]" type="checkbox" value="1">просрочки от 60 до 89 по активным</label>
<input class="tab3" form="be_form" name="be_psa_v2" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_12[2]" type="checkbox" value="1">просрочки от 60 до 89</label>
<input class="tab7" form="be_form" name="be_ps_12v2" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_psa_c[3]" type="checkbox" value="1">просрочки от 90 до 119 по активным</label>
<input class="tab3" form="be_form" name="be_psa_v3" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_12[3]" type="checkbox" value="1">просрочки от 90 до 119</label>
<input class="tab7" form="be_form" name="be_ps_12v3" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_psa_c[4]" type="checkbox" value="1">просрочки более 120 по активным</label>
<input class="tab3" form="be_form" name="be_psa_v4" type="number" placeholder="максимум" size="15" value="">
<label class="tab5"><input class="inp" form="be_form" name="be_ps_12[4]" type="checkbox" value="1">просрочки более 120</label>
<input class="tab7" form="be_form" name="be_ps_12v4" type="number" placeholder="максимум" size="15" value="">

<br><label><input class="inp" form="be_form" name="be_res_en" type="checkbox" value="1">Допускается реструктуризация</label>


<br>Количество обращений за последние 7 дней <input class="tab5" form="be_form" name="be_obr_7" type="text" placeholder="максимум" size="15" value="">
<br>Количество обращений за последние 14 дней <input class="tab5" form="be_form" name="be_obr_14" type="text" placeholder="максимум" size="15" value="">
<br>Количество обращений за последний месяц <input class="tab5" form="be_form" name="be_obr_mon" type="text" placeholder="максимум" size="15" value="">
</div>
<div>
Стоп-факторы: 
<br><label><input class="inp" form="be_form" name="be_inc_true" type="checkbox" value="1">Достоверность данных о доходах и трудоустройстве</label><br>
Набор стоп-факторов <select name="stop_settings" id="stop_settings" onchange="javascript:stop_select(this.value);">
	<option value="-1">Выберите</option>
	<option value="0">MINIMAL</option>
	<option value="1">LIGHT</option>
	<option value="2">HARD</option>
</select><br>
Допустимые стоп-факторы:<br>
<label><input class="inp" form="be_form" name="be_stop[0]" type="checkbox" value="1">100</label>
<label><input class="inp" form="be_form" name="be_stop[1]" type="checkbox" value="1">101</label>
<label><input class="inp" form="be_form" name="be_stop[2]" type="checkbox" value="1">102</label>
<label><input class="inp" form="be_form" name="be_stop[3]" type="checkbox" value="1">103</label>
<label><input class="inp" form="be_form" name="be_stop[4]" type="checkbox" value="1">104</label>
<label><input class="inp" form="be_form" name="be_stop[5]" type="checkbox" value="1">105</label>
<label><input class="inp" form="be_form" name="be_stop[6]" type="checkbox" value="1">106</label>
<label><input class="inp" form="be_form" name="be_stop[7]" type="checkbox" value="1">107</label>
<label><input class="inp" form="be_form" name="be_stop[8]" type="checkbox" value="1">108</label>
<label><input class="inp" form="be_form" name="be_stop[9]" type="checkbox" value="1">109</label>
<label><input class="inp" form="be_form" name="be_stop[10]" type="checkbox" value="1">110</label>
<label><input class="inp" form="be_form" name="be_stop[11]" type="checkbox" value="1">111</label>
<label><input class="inp" form="be_form" name="be_stop[12]" type="checkbox" value="1">121</label>
<label><input class="inp" form="be_form" name="be_stop[13]" type="checkbox" value="1">122</label>
<label><input class="inp" form="be_form" name="be_stop[14]" type="checkbox" value="1">123</label>
<label><input class="inp" form="be_form" name="be_stop[15]" type="checkbox" value="1">124</label>
<label><input class="inp" form="be_form" name="be_stop[16]" type="checkbox" value="1">130</label>
<label><input class="inp" form="be_form" name="be_stop[17]" type="checkbox" value="1">140</label>
<label><input class="inp" form="be_form" name="be_stop[18]" type="checkbox" value="1">145</label>
<label><input class="inp" form="be_form" name="be_stop[19]" type="checkbox" value="1">141</label><br>
<label><input class="inp" form="be_form" name="be_stop[20]" type="checkbox" value="1">146</label>
<label><input class="inp" form="be_form" name="be_stop[21]" type="checkbox" value="1">147</label>
<label><input class="inp" form="be_form" name="be_stop[22]" type="checkbox" value="1">148</label>
<label><input class="inp" form="be_form" name="be_stop[23]" type="checkbox" value="1">149</label>
<label><input class="inp" form="be_form" name="be_stop[24]" type="checkbox" value="1">200</label>
<label><input class="inp" form="be_form" name="be_stop[25]" type="checkbox" value="1">150</label>
<label><input class="inp" form="be_form" name="be_stop[26]" type="checkbox" value="1">151</label>
<label><input class="inp" form="be_form" name="be_stop[27]" type="checkbox" value="1">152</label>
<label><input class="inp" form="be_form" name="be_stop[28]" type="checkbox" value="1">153</label>
<label><input class="inp" form="be_form" name="be_stop[29]" type="checkbox" value="1">154</label>
<label><input class="inp" form="be_form" name="be_stop[30]" type="checkbox" value="1">160</label>
<label><input class="inp" form="be_form" name="be_stop[31]" type="checkbox" value="1">161</label>
<label><input class="inp" form="be_form" name="be_stop[32]" type="checkbox" value="1">171</label>
<label><input class="inp" form="be_form" name="be_stop[33]" type="checkbox" value="1">174</label>
<label><input class="inp" form="be_form" name="be_stop[34]" type="checkbox" value="1">175</label>
<label><input class="inp" form="be_form" name="be_stop[35]" type="checkbox" value="1">180</label>
<label><input class="inp" form="be_form" name="be_stop[36]" type="checkbox" value="1">190</label>
<label><input class="inp" form="be_form" name="be_stop[37]" type="checkbox" value="1">191</label>
<label><input class="inp" form="be_form" name="be_stop[38]" type="checkbox" value="1">500</label>
<label><input class="inp" form="be_form" name="be_stop[39]" type="checkbox" value="1">193</label><br>
<label><input class="inp" form="be_form" name="be_stop[40]" type="checkbox" value="1">194</label>
<label><input class="inp" form="be_form" name="be_stop[41]" type="checkbox" value="1">195</label>
<label><input class="inp" form="be_form" name="be_stop[42]" type="checkbox" value="1">196</label>
<label><input class="inp" form="be_form" name="be_stop[43]" type="checkbox" value="1">197</label>
<label><input class="inp" form="be_form" name="be_stop[44]" type="checkbox" value="1">198</label>
<label><input class="inp" form="be_form" name="be_stop[45]" type="checkbox" value="1">199</label>
<label><input class="inp" form="be_form" name="be_stop[46]" type="checkbox" value="1">207</label>
<label><input class="inp" form="be_form" name="be_stop[47]" type="checkbox" value="1">208</label>
<label><input class="inp" form="be_form" name="be_stop[48]" type="checkbox" value="1">209</label>
<label><input class="inp" form="be_form" name="be_stop[49]" type="checkbox" value="1">210</label>
<label><input class="inp" form="be_form" name="be_stop[50]" type="checkbox" value="1">211</label>
<label><input class="inp" form="be_form" name="be_stop[51]" type="checkbox" value="1">212</label>
<label><input class="inp" form="be_form" name="be_stop[52]" type="checkbox" value="1">213</label>
<label><input class="inp" form="be_form" name="be_stop[53]" type="checkbox" value="1">214</label>
<label><input class="inp" form="be_form" name="be_stop[54]" type="checkbox" value="1">215</label>
<label><input class="inp" form="be_form" name="be_stop[55]" type="checkbox" value="1">216</label>
<label><input class="inp" form="be_form" name="be_stop[56]" type="checkbox" value="1">217</label>
<label><input class="inp" form="be_form" name="be_stop[57]" type="checkbox" value="1">218</label>
<label><input class="inp" form="be_form" name="be_stop[58]" type="checkbox" value="1">219</label>
<label><input class="inp" form="be_form" name="be_stop[59]" type="checkbox" value="1">220</label><br>
<label><input class="inp" form="be_form" name="be_stop[60]" type="checkbox" value="1">221</label>
<label><input class="inp" form="be_form" name="be_stop[61]" type="checkbox" value="1">222</label>
<label><input class="inp" form="be_form" name="be_stop[62]" type="checkbox" value="1">223</label>
<label><input class="inp" form="be_form" name="be_stop[63]" type="checkbox" value="1">224</label>
<label><input class="inp" form="be_form" name="be_stop[64]" type="checkbox" value="1">225</label>
<label><input class="inp" form="be_form" name="be_stop[65]" type="checkbox" value="1">226</label>
<label><input class="inp" form="be_form" name="be_stop[66]" type="checkbox" value="1">227</label>
<label><input class="inp" form="be_form" name="be_stop[67]" type="checkbox" value="1">228</label>
<label><input class="inp" form="be_form" name="be_stop[68]" type="checkbox" value="1">301</label>
<label><input class="inp" form="be_form" name="be_stop[69]" type="checkbox" value="1">302</label>
<label><input class="inp" form="be_form" name="be_stop[70]" type="checkbox" value="1">303</label>
<label><input class="inp" form="be_form" name="be_stop[71]" type="checkbox" value="1">304</label>
<label><input class="inp" form="be_form" name="be_stop[72]" type="checkbox" value="1">305</label>
<label><input class="inp" form="be_form" name="be_stop[73]" type="checkbox" value="1">306</label>
<label><input class="inp" form="be_form" name="be_stop[74]" type="checkbox" value="1">307</label>
<label><input class="inp" form="be_form" name="be_stop[75]" type="checkbox" value="1">308</label>
<label><input class="inp" form="be_form" name="be_stop[76]" type="checkbox" value="1">309</label>
<label><input class="inp" form="be_form" name="be_stop[77]" type="checkbox" value="1">501</label>
<label><input class="inp" form="be_form" name="be_stop[78]" type="checkbox" value="1">311</label>
<label><input class="inp" form="be_form" name="be_stop[79]" type="checkbox" value="1">312</label><br>
<label><input class="inp" form="be_form" name="be_stop[80]" type="checkbox" value="1">313</label>
<label><input class="inp" form="be_form" name="be_stop[81]" type="checkbox" value="1">314</label>
<label><input class="inp" form="be_form" name="be_stop[82]" type="checkbox" value="1">502</label>
<span style="cursor: pointer;" onclick="all_stop_types();"><i>Выбрать все/снять выделение</i></span>
<?
/*
<br>Стоп-факторы категории Криминал:
<br><label><input class="inp" form="be_form" name="be_crim[0]" type="checkbox" value="1">1</label>
<label><input class="inp" form="be_form" name="be_crim[1]" type="checkbox" value="1">2</label>
<label><input class="inp" form="be_form" name="be_crim[2]" type="checkbox" value="1">3</label>
<br>Стоп-факторы категории Межбанк:
<br><label><input class="inp" form="be_form" name="be_mbank[0]" type="checkbox" value="1">1</label>
<label><input class="inp" form="be_form" name="be_mbank[1]" type="checkbox" value="1">2</label>
<label><input class="inp" form="be_form" name="be_mbank[2]" type="checkbox" value="1">3</label>
*/
?>
<br>Допустимые стоп-факторы категории Долги (сумма долга менее 15000 руб):
<br><label><input class="inp" form="be_form" name="be_st_cr[0]" type="checkbox" value="1">приставы</label>
<label><input class="inp" form="be_form" name="be_st_cr[1]" type="checkbox" value="1">штрафы ГИБДД</label>
<label><input class="inp" form="be_form" name="be_st_cr[2]" type="checkbox" value="1">коммуналка</label>
<label><input class="inp" form="be_form" name="be_st_cr[3]" type="checkbox" value="1">Р/С клиента заблокирован</label>
<?
/*
<br>Недопустимые совокупности стоп-факторов. <red>Будет позже</red>
*/
?>
<br>Уровень риска по СПАРК
<br><label><input class="inp" form="be_form" name="be_spark[0]" type="checkbox" value="1">низкий</label>
<label><input class="inp" form="be_form" name="be_spark[1]" type="checkbox" value="1">средний</label>
<label><input class="inp" form="be_form" name="be_spark[2]" type="checkbox" value="1">высокий</label>

<br><br>
Стоп-факторы по собственникам:
<br>
<label><input class="inp" form="be_form" name="be_sf[0]" type="checkbox" value="1">Нет стоп-факторов</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[1]" type="checkbox" value="1">Несовершеннолетний</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[2]" type="checkbox" value="1">Физ. лицо -Нерезидент</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[3]" type="checkbox" value="1">Юр. Лицо - Нерезидент</label>
<br>
<label><input class="inp" form="be_form" name="be_sf[4]" type="checkbox" value="1">Пенсионер</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[5]" type="checkbox" value="1">Инвалид 1-й гр</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[6]" type="checkbox" value="1">Инвалид 2-й гр</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[7]" type="checkbox" value="1">Инвалид 3-й гр</label>
<br>
<label><input class="inp" form="be_form" name="be_sf[8]" type="checkbox" value="1">На учете ПНД</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[9]" type="checkbox" value="1">На учете НД</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[10]" type="checkbox" value="1">Банкрот/В процессе</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[11]" type="checkbox" value="1">Текущая просрочка</label>
<br>
<label><input class="inp" form="be_form" name="be_sf[12]" type="checkbox" value="1">ФССП на сумму более 5% от стоимости залога</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[13]" type="checkbox" value="1">Отбывающий заключение</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[14]" type="checkbox" value="1">Находящийся под следствием</label>

<br><br>
Стоп-факторы по зарегистрированным:
<br>

<label><input class="inp" form="be_form" name="be_sf[15]" type="checkbox" value="1">Нет стоп-факторов</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[16]" type="checkbox" value="1">Несовершеннолетние</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[17]" type="checkbox" value="1">Отказник от приватизации</label>
<br>
<label><input class="inp" form="be_form" name="be_sf[18]" type="checkbox" value="1">Пенсионер</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[19]" type="checkbox" value="1">Инвалид 1-й гр</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[20]" type="checkbox" value="1">Инвалид 2-й гр</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[21]" type="checkbox" value="1">Инвалид 3-й гр</label>
<br>
<label><input class="inp" form="be_form" name="be_sf[22]" type="checkbox" value="1">На учете ПНД</label>
<label class="tab2"><input class="inp" form="be_form" name="be_sf[23]" type="checkbox" value="1">На учете НД</label>
<label class="tab4"><input class="inp" form="be_form" name="be_sf[24]" type="checkbox" value="1">Отбывающий заключение</label>
<label class="tab6"><input class="inp" form="be_form" name="be_sf[25]" type="checkbox" value="1">Находящийся под следствием</label>


</div>

</div>



<br>
<button class="inp" name="be_clr" onClick="document.forms.be_form.reset()">Очистить</button>
<button class="inp" id = "be_butt" name="be_add2" onClick="send_form(document.forms.be_form.be_type.value)">Добавить</button>
<button class="cp_but_h" id = "be_b_copy" name="be_copy" onClick="send_form('add')">Скопировать в новый продукт</button>
				
</div>

<div onclick="bank_editor('none')" id="wrap_be"></div>
<div id="window_be" class="window"><img class="close" onclick="bank_editor('none')" src="/img/close.png"><div id="be_wrap"><div id="loading"></div><div id="banks_list"></div></div></div>
<button id="bank_editor_id" onclick="javascript:bank_editor('block');">Редактор банков</button> 
<button id="bank_points_id" onclick="javascript:points_editor('block');">Точки контроля</button> 
<button class="myButton" onclick="document.querySelector('#be_b_copy').style = 'display:none'; document.forms.be_form.reset(); document.forms.be_form.be_type.value = 'add'; document.querySelector('#be_butt').innerHTML = 'Добавить'; document.querySelector('#stop_settings').value='-1'; show('block')">Добавить продукт</button>
<select style="margin-top:10px" id="prod_selector" onchange="prod_filter(this.value);">
	<option value="-1">Показать все</option>
</select>
<label><span id="ch_text" name="ch_text">Показать таблицы</span><input type="checkbox" value="1" onClick="javascript:ch_table_v(this, this.checked);"></label>
<br><br><br>

<table id="products_table" cellspacing="1" cellpadding="4">
<tr bgcolor="#cccccc">
<td class="td0"><b>id</b></td>
<td class="td1"><b>Ред.</b></td>
<td class="td2"><b>Удал.</b></td>
<td class="td3"><b>Акт?</b></td>
<td class="td4"><b>Продукт</b></td>
<td class="td5"><b>Банк</b></td>
<td class="td6"><b>Ставка</b></td>
<td class="td7"><b>Макс. кредит</b></td>
<td><b>Описание</b></td>
</tr>

<?
if ($result = $db_connect->query("SELECT id, bank_name, product_name, min_percent, max_percent, active, description, max_credit, psct3, psct6, psct12, psct18, psct24, psct36, psct60, psct60p, psctall, psat3, psat6, psat12, psat18, psat24, psat36, psat60, psat60p, psatall, psct, psat FROM old_products ORDER BY id ASC;"))
{	
	if ($result->num_rows)
	{
		$is = 0;
		while ($a = $result->fetch_array(MYSQLI_ASSOC))
		{
			$be_id = $a["id"];
			//echo '<tr id="be_id'.$be_id.'" style = "border: 4px double red; padding:5px;">';
			echo '<tr id="be_id' . $be_id . '" class="border_bottom">';
			echo "<td>{$a["id"]}</td>";
			echo '<td align="center"><a title="редактировать" class="edit" href="#" onclick="edit('.$be_id.')"><img src="/img/edit.png" width="16" height="16"></a></td>';
			echo '<td align="center"><a title="удалить" class="delete" href="#" onclick="del('.$be_id.')"><img src="/img/delete.png" width="16" height="16"></a></td>';
			if ($a["active"])
				echo '<td align="center"><img src="/img/ball_green.png" width="16" height="16"></td>';
			else	
				echo '<td align="center"><img src="/img/ball_red.png" width="16" height="16"></td>';
			echo "<td>{$a["product_name"]}</td>";
			if (isset($banks[$a["bank_name"]]))
				echo "<td>{$banks[$a["bank_name"]]}</td>";
			else
				echo "<td>Банк удален</td>";
			echo "<td>От {$a["min_percent"]}%<br>До {$a["max_percent"]}%</td>";
			echo "<td>{$a["max_credit"]}</td>";
			echo "<td><span id='descr_$be_id'>{$a["description"]}</span><span id='cred_tables_$be_id' class='cr_tabl_h'>";
			echo "Наихудший платежный статус по закрытым<br>";
			
			
			
			//$null_array  = array("", "", "", "", "", "", "", "");
			$null_array = array("", "", "", "", "", "", "", "", "", "");
			
			$be_psct	= ($a["psct"]   != "" ) ? preg_split("/\|/", $a["psct"])   : $null_array;
			
			//print_r($be_psct);
			
			foreach ($be_psct as $bk=>$bv)
				$be_psct[$bk] = ($bv == "1") ? "" : "disabled";
			//echo "<br>";	
			//print_r($be_psct);
			
			$be_psct3	= ($a["psct3"]   != "" ) ? preg_split("/\|/", $a["psct3"])   : $null_array;
			$be_psct6	= ($a["psct6"]   != "" ) ? preg_split("/\|/", $a["psct6"])   : $null_array;
			$be_psct12	= ($a["psct12"]  != "" ) ? preg_split("/\|/", $a["psct12"])  : $null_array;
			$be_psct18	= ($a["psct18"]  != "" ) ? preg_split("/\|/", $a["psct18"])  : $null_array;
			$be_psct24	= ($a["psct24"]  != "" ) ? preg_split("/\|/", $a["psct24"])  : $null_array;
			$be_psct36	= ($a["psct36"]  != "" ) ? preg_split("/\|/", $a["psct36"])  : $null_array;
			$be_psct60	= ($a["psct60"]  != "" ) ? preg_split("/\|/", $a["psct60"])  : $null_array;
			$be_psct60p	= ($a["psct60p"] != "" ) ? preg_split("/\|/", $a["psct60p"]) : $null_array;
			$be_psctall	= ($a["psctall"] != "" ) ? preg_split("/\|/", $a["psctall"]) : $null_array;

			
			echo "<table id='t_close'>";
			echo "	<tr>";
			echo "		<td></td>";
			echo "		<td class='t_close_td'>За 3 мес.</td>";
			echo "		<td class='t_close_td'>За 6 мес.</td>";
			echo "		<td class='t_close_td'>За 12 мес.</td>";
			echo "		<td class='t_close_td'>За 18 мес.</td>";
			echo "		<td class='t_close_td'>За 24 мес.</td>";
			echo "		<td class='t_close_td'>За 36 мес.</td>";
			echo "		<td class='t_close_td'>За 60 мес.</td>";
			echo "		<td class='t_close_td'>>60 мес.</td>";
			echo "		<td class='t_close_td'>Общее</td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>1-29</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[1]} value='{$be_psct3[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[2]} value='{$be_psct6[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[3]} value='{$be_psct12[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[4]} value='{$be_psct18[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[5]} value='{$be_psct24[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[6]} value='{$be_psct36[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[7]} value='{$be_psct60[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[8]} value='{$be_psct60p[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[9]} value='{$be_psctall[1]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>30-59</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[1]} value='{$be_psct3[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[2]} value='{$be_psct6[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[3]} value='{$be_psct12[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[4]} value='{$be_psct18[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[5]} value='{$be_psct24[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[6]} value='{$be_psct36[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[7]} value='{$be_psct60[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[8]} value='{$be_psct60p[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[9]} value='{$be_psctall[2]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>60-89</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[1]} value='{$be_psct3[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[2]} value='{$be_psct6[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[3]} value='{$be_psct12[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[4]} value='{$be_psct18[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[5]} value='{$be_psct24[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[6]} value='{$be_psct36[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[7]} value='{$be_psct60[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[8]} value='{$be_psct60p[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[9]} value='{$be_psctall[3]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>90-119</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[1]} value='{$be_psct3[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[2]} value='{$be_psct6[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[3]} value='{$be_psct12[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[4]} value='{$be_psct18[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[5]} value='{$be_psct24[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[6]} value='{$be_psct36[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[7]} value='{$be_psct60[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[8]} value='{$be_psct60p[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[9]} value='{$be_psctall[4]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>120+</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[1]} value='{$be_psct3[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[2]} value='{$be_psct6[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[3]} value='{$be_psct12[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[4]} value='{$be_psct18[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[5]} value='{$be_psct24[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[6]} value='{$be_psct36[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[7]} value='{$be_psct60[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[8]} value='{$be_psct60p[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psct[9]} value='{$be_psctall[5]}'></td>";
			echo "	</tr>";
			echo "</table>";
			
			
			echo "<br><br>Наихудший платежный статус по активным<br>";
			
			$be_psat	= ($a["psat"]   != "" ) ? preg_split("/\|/", $a["psat"])   : $null_array;
			
			//print_r($be_psct);
			
			foreach ($be_psat as $bk=>$bv)
				$be_psat[$bk] = ($bv == "1") ? "" : "disabled";
			//echo "<br>";	
			//print_r($be_psct);
			
			$be_psat3	= ($a["psat3"]   != "" ) ? preg_split("/\|/", $a["psat3"])   : $null_array;
			$be_psat6	= ($a["psat6"]   != "" ) ? preg_split("/\|/", $a["psat6"])   : $null_array;
			$be_psat12	= ($a["psat12"]  != "" ) ? preg_split("/\|/", $a["psat12"])  : $null_array;
			$be_psat18	= ($a["psat18"]  != "" ) ? preg_split("/\|/", $a["psat18"])  : $null_array;
			$be_psat24	= ($a["psat24"]  != "" ) ? preg_split("/\|/", $a["psat24"])  : $null_array;
			$be_psat36	= ($a["psat36"]  != "" ) ? preg_split("/\|/", $a["psat36"])  : $null_array;
			$be_psat60	= ($a["psat60"]  != "" ) ? preg_split("/\|/", $a["psat60"])  : $null_array;
			$be_psat60p	= ($a["psat60p"] != "" ) ? preg_split("/\|/", $a["psat60p"]) : $null_array;
			$be_psatall	= ($a["psatall"] != "" ) ? preg_split("/\|/", $a["psatall"]) : $null_array;

			
			echo "<table id='t_open'>";
			echo "	<tr>";
			echo "		<td></td>";
			echo "		<td class='t_close_td'>За 3 мес.</td>";
			echo "		<td class='t_close_td'>За 6 мес.</td>";
			echo "		<td class='t_close_td'>За 12 мес.</td>";
			echo "		<td class='t_close_td'>За 18 мес.</td>";
			echo "		<td class='t_close_td'>За 24 мес.</td>";
			echo "		<td class='t_close_td'>За 36 мес.</td>";
			echo "		<td class='t_close_td'>За 60 мес.</td>";
			echo "		<td class='t_close_td'>>60 мес.</td>";
			echo "		<td class='t_close_td'>Общее</td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>1-29</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[1]} value='{$be_psat3[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[2]} value='{$be_psat6[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[3]} value='{$be_psat12[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[4]} value='{$be_psat18[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[5]} value='{$be_psat24[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[6]} value='{$be_psat36[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[7]} value='{$be_psat60[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[8]} value='{$be_psat60p[1]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[9]} value='{$be_psatall[1]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>30-59</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[1]} value='{$be_psat3[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[2]} value='{$be_psat6[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[3]} value='{$be_psat12[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[4]} value='{$be_psat18[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[5]} value='{$be_psat24[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[6]} value='{$be_psat36[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[7]} value='{$be_psat60[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[8]} value='{$be_psat60p[2]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[9]} value='{$be_psatall[2]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>60-89</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[1]} value='{$be_psat3[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[2]} value='{$be_psat6[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[3]} value='{$be_psat12[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[4]} value='{$be_psat18[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[5]} value='{$be_psat24[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[6]} value='{$be_psat36[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[7]} value='{$be_psat60[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[8]} value='{$be_psat60p[3]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[9]} value='{$be_psatall[3]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>90-119</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[1]} value='{$be_psat3[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[2]} value='{$be_psat6[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[3]} value='{$be_psat12[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[4]} value='{$be_psat18[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[5]} value='{$be_psat24[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[6]} value='{$be_psat36[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[7]} value='{$be_psat60[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[8]} value='{$be_psat60p[4]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[9]} value='{$be_psatall[4]}'></td>";
			echo "	</tr>";
			echo "	<tr>";
			echo "		<td>120+</td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[1]} value='{$be_psat3[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[2]} value='{$be_psat6[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[3]} value='{$be_psat12[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[4]} value='{$be_psat18[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[5]} value='{$be_psat24[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[6]} value='{$be_psat36[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[7]} value='{$be_psat60[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[8]} value='{$be_psat60p[5]}'></td>";
			echo "		<td class='t_close_td'><input type='text' {$be_psat[9]} value='{$be_psatall[5]}'></td>";
			echo "	</tr>";
			echo "</table>";
			
			echo "</span></td>";
		/*	
			//echo "<td>{$a["active"]}</td>";
			echo "<td>{$a["pd"]}</td>";
			echo "<td>{$a["discont"]}</td>";
			//echo "<td>{$a["cr_other"]}</td>";
			if ($a["cr_other"])
				echo '<td align="center">Да</td>';
			else
				echo '<td align="center">Нет</td>';
						
			echo "<td>{$a["max_age"]}</td>";*/
			
			
			echo "</tr>";
		}
	}
	else
		echo '<tr><td colspan="13">Нет записей</td></tr>';
	
		$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
?>


</table>
