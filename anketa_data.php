<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$error = false;
$in_tm = false;
$in_ozs = false;
$all_fields_mass = array();
$fields_mass = array();
$offices_mass = array();
$anketa_mass = array();
$data_mass_a = array();
$mass_for_js = array();
$status_mass = array();

if ($result = $db_connect->query("SHOW COLUMNS FROM forms WHERE Field = 'status';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$status_list = $out[1];
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$office_filter = '';
if ($staff_office_type != 'main')
{
	if ($staff_office_type == 'tm')
	{
		if ($staff_position == 'director_tm')
			$office_filter = " AND av_tm_dir='1'";
		else if ($staff_position == 'manager_tm')
			$office_filter = " AND av_tm_man='1'";
	}
	else if ($staff_office_type == 'ozs')
	{
		if ($staff_position == 'director_ozs')
			$office_filter = " AND av_ozs_dir='1'";
		else if ($staff_position == 'manager_ozs')
			$office_filter = " AND av_ozs_man='1'";
	}
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1'$office_filter ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($all_fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

if (!$error)
{
	/*echo "<pre>";
	print_r($fields_mass);
	echo "</pre>";
	*/
	if ($result = $db_connect->query("SELECT * FROM forms WHERE id = '$_id';"))
	{
		if ($result->num_rows)
		{
			$anketa_mass = $result->fetch_array(MYSQLI_ASSOC);
		}
		else
		{
			$res = "Не найдена анкета";
			echo $res;
			$error = true;
		}
		
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}



if (!$error)
{
	$filter = '';
	
	switch($anketa_mass['status'])
	{
		case "new_in_tm": 
		case "in_tm_office": 
		case "primary_work": 
		case "secondary_work": 
		case "assigned_meeting": 
		case "contract_signed": $filter = " WHERE in_tm='1'"; $in_tm = true; break;
		
		case "new_in_ozs": 
		case "processing_ozs": 
		case "ready2bank": 
		case "send2bank": 
		case "credit_approved": 
		case "credit_declined": 
		case "loan_issued": $filter = " WHERE in_ozs='1'"; $in_ozs = true; break;
	}
	
	
	if ($result = $db_connect->query("SELECT * FROM form_status_mass$filter ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$status_mass[$row['id']] = $row;
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}
?>

