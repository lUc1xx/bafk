<?php
set_error_handler('err_handler');
function err_handler($errno, $errmsg, $filename, $linenum) {
	$date = date('Y-m-d H:i:s (T)');
	$f = fopen('errors.txt', 'a');
	if (!empty($f)) {
		$filename  = str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename);
		$err  = "$errmsg = $filename = $linenum\r\n";
		fwrite($f, $err);
		fclose($f);
	}
}

define('SYSTEM_START_9876543210', true);
date_default_timezone_set('Europe/Moscow');

$fp = fopen("raw.log", "a+");
$data_ = date("Y-m-d H:i:s", time());
$data_ .= "\n" . print_r($_REQUEST, true);
$data_ .= "\n" . print_r($_SERVER, true);
$data_ .= "\n" . print_r($_GET, true);
$data_ .= "\n" . print_r($_POST, true) . "\n\n";
$test = fwrite($fp, $data_);
fclose($fp);

if (!isset($_POST))
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(0)"}');

if (sizeof($_POST) == 0)
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(1)"}');

if (!isset($_POST['token']))
	die('{"STATUS":"failed", "msg":"Некорректный запрос (не указан token)"}');

$token_mass = array(
	"63427a8a3af25fd2c0bf76b2995512b7___",
	"5d94b4249e0367dbf0dc5c95fadc61c4"
);

$token = $_POST['token'];

if (!in_array($token, $token_mass))
	die('{"STATUS":"failed", "msg":"Недействительный token"}');

if (!isset($_POST['action']))
	die('{"STATUS":"failed", "msg":"Некорректный запрос (не указан action)"}');

$action = $_POST['action'];
if ($action == 'add')
{
	$mass = array();
	foreach ($_POST as $k => $v)
	{
		$mass[$k] = htmlspecialchars($v, ENT_QUOTES);
	}
	
	$fail = false;
	$msg  = '';
	
	if (!isset($mass['id'])) {
		$fail = true;
		$msg .= 'Не указан параметр id;';
	}
	
	if (!isset($mass['sum'])) {
		$fail = true;
		$msg .= 'Не указан параметр sum;';
	}
	
	if (!isset($mass['hash'])) {
		$fail = true;
		$msg .= 'Не указан параметр hash;';
	}

	if ($fail) {
		die('{"STATUS":"failed", "msg":"' . $msg . '"}');
	}
	
	$fail = false;
	$msg  = '';
	
	
	if (preg_replace("/[^0-9]/", "", $mass['id']) != $mass['id']) {
		$fail = true;
		$msg .= 'Параметр id может быть только числом;';
	}
	
	if (preg_replace("/[^0-9 ]/", "", $mass['sum']) != $mass['sum']) {
		$fail = true;
		$msg .= 'В параметре sum допустимы только цифры и пробел;';
	}
	
	if (strlen($mass['hash']) != 32) {
		$fail = true;
		$msg .= 'Передан некорректный hash;';
	}

	if ($fail) {
		die('{"STATUS":"failed", "msg":"' . $msg . '"}');
	}
	
	$fid = $mass['id'];
	$sum = preg_replace("/[^0-9]/", "", $mass['sum']);
	$hash = $mass['hash'];
	
	include_once "../../_bdc.php";
	
	$sql = "INSERT INTO iidx_signals(dtime, fid, summ, hash, deleted) VALUES(NOW(), '$fid', '$sum', '$hash', '0');";
	if ($result = $db_connect->query($sql))
	{
		$fp = fopen("access.log", "a+");
		$data_ = date("Y-m-d H:i:s", time());
		$data_ .= "\n" . print_r($_REQUEST, true);
		$data_ .= "\n" . print_r($_SERVER, true);
		$data_ .= "\n" . print_r($_GET, true);
		$data_ .= "\n" . print_r($_POST, true);
		$data_ .= "\n" . print_r($db_connect->errno . " => " . $db_connect->error, true) . "\n\n";
		$test = fwrite($fp, $data_);
		fclose($fp);
		mysqli_close($db_connect);
		die('{"STATUS":"ok"}');
	}
	else
	{
		$fp = fopen("error.log", "a+");
		$data_ = date("Y-m-d H:i:s", time());
		$data_ .= "\n" . print_r($_REQUEST, true);
		$data_ .= "\n" . print_r($_SERVER, true);
		$data_ .= "\n" . print_r($_GET, true);
		$data_ .= "\n" . print_r($_POST, true);
		$data_ .= "\n" . print_r($sql, true);
		$data_ .= "\n" . print_r($db_connect->errno . " => " . $db_connect->error, true) . "\n\n";
		$test = fwrite($fp, $data_);
		fclose($fp);
		mysqli_close($db_connect);
		die('{"STATUS":"error", "msg":"Свяжитесь с администратором!"}');
		
	}
	
}
else
{
	die('{"STATUS":"failed", "msg":"Некорректный запрос (Неверный action)"}');
}
?>