<?php
set_error_handler('err_handler');
function err_handler($errno, $errmsg, $filename, $linenum) {
$date = date('Y-m-d H:i:s (T)');
$f = fopen('errors.txt', 'a');
if (!empty($f)) {
$filename  =str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename);
$err  = "$errmsg = $filename = $linenum\r\n";
fwrite($f, $err);
fclose($f);
}
}


define('SYSTEM_START_9876543210', true);
date_default_timezone_set('Europe/Moscow');

include_once "../../_bdc.php";
include_once "../../_functions.php";

if (!isset($_POST))
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(0)"}');

if (sizeof($_POST) == 0)
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(1)"}');

if (!isset($_POST['secure_token']))
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(2)"}');

if ($_POST['secure_token'] != '3434')
	die('{"STATUS":"failed", "msg":"Ошибка обработки. Свяжитесь с администратором(3)"}');


if (!isset($_POST['token']))
	die('{"STATUS":"failed", "msg":"Некорректный запрос (не указан token)"}');

$token_mass = array(
	"63427a8a3af25fd2c0bf76b2995512b7",
	"34567"
);

$token = $_POST['token'];

if (!in_array($token, $token_mass))
	die('{"STATUS":"failed", "msg":"Недействительный token"}');



if (!isset($_POST['action']))
	die('{"STATUS":"failed", "msg":"Некорректный запрос (не указан action)"}');

$action = $_POST['action'];
if ($action == 'add')
{
	$mass = array();
	foreach ($_POST as $k => $v)
	{
		$mass[$k] = htmlspecialchars($v, ENT_QUOTES);
	}
	
	$data2sql = array();
	$raw_data = '';
	$raw_data_orig = '';
	
	$data2sql['source'] 		= "api";
	$data2sql['source_param'] 	= 'filkos|' . $mass['id_req'] . '|' . $mass['id_source'];
	$data2sql['status'] 		= "new";

	$data2sql['lastname'] 		= $mass['lastname'];
	$data2sql['firstname'] 		= $mass['firstname'];
	$data2sql['middlename'] 	= $mass['middlename'];
	$data2sql['sum'] 			= $mass['sum'];
	$data2sql['sum_val'] 		= 'rub';
	$data2sql['city'] 			= $mass['city'];
	$data2sql['type'] 			= 'mini';
	
	$value						= $mass['birth_day'];
	$raw_data 					.= "birth_day::$value<br>";
	$raw_data_orig 				.= "birth_day::$value<br>";
	
	$value						= $mass['phone'];
	$raw_data 					.= "field29::$value<br>";
	$raw_data_orig 				.= "field29::$value<br>";
	$data2sql['contact_phone'] 	= "$value";
	$data2sql['phone_history'] 	= "$value;;;";
	
	
	$data2sql2 = array();
	$data2sql2['field1'] 		= $mass['city'] . ', г; регион: ' . $mass['region'];	// city + region
	$data2sql2['field75'] 		= $mass['income'];
	$data2sql2['field47'] 		= $mass['work'];
	$data2sql2['field76'] 		= ($mass['poi'] == 1) ? 'да' : 'нет';
	//$data2sql2['field58'] 		= ($mass['poi'] = 1) ? 'да' : 'нет';
		
	if (!isset($mass['work_time']))
		$mass['work_time'] = 1;

	$date = new DateTime();
	$date->modify('-' . $mass['work_time'] . ' month');	
	$data2sql2['field59'] 		= $date->format('d.m.Y');
		
	//$data2sql2['field59'] 		= $mass['work_time'];	//перевести мес в дату
	//$data2sql2[''] 		= $mass['history'];	
	
	if ($mass['property'] == 'Автомобиль')
		$data2sql2['field8'] = 'Иномарка';
	else if ($mass['property'] == 'Нет')
		$data2sql2['field8'] = 'Нет';
		
	//$data2sql2['field8'] 		= $mass['property']; ($mass['property'] = 'Автомобиль') ? 'Иномарка' : (($mass['property'] = 'Нет') ? '' : );
	$data2sql2['field19'] 		= $mass['passport_num'];
	$data2sql2['field18'] 		= $mass['passport_ser'];
	$data2sql2['field21'] 		= $mass['passport_date'];
	$data2sql2['field22'] 		= $mass['passport_cod'];
	$data2sql2['field20'] 		= $mass['passport_who'];
	$data2sql2['field23'] 		= $mass['birth_place'];
	$data2sql2['field24'] 		= $mass['address'];
	$data2sql2['field177'] 		= ($mass['address_match'] == 1) ? 'да' : 'нет';
	$data2sql2['field49'] 		= $mass['work_name'];
	$data2sql2['field51'] 		= $mass['work_dir_name'];
	$data2sql2['field60'] 		= $mass['work_phone'];
	$data2sql2['field55'] 		= $mass['work_address'];
	
	
	foreach($data2sql2 as $k=>$v)
	{
		$raw_data 					.= "$k::$v<br>";
		$raw_data_orig 				.= "$k::$v<br>";
	}
	
	
	
	$data2sql['data_orig'] 		= $raw_data_orig;
	$data2sql['data'] 			= $raw_data;
	
	$res = print_r(insert_into_sql('forms', $data2sql),true);
	
	if ($res == 'OK')
		print_r('{"STATUS":"success"}');
	
	$fp = fopen("api.log", "a+");
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= "\n" . print_r($data2sql,true) . "\n";
	$data_ .= "\n" . $res . "\n\n";
	$test = fwrite($fp, $data_);
	fclose($fp);
	
	//print_r($data2sql);
	
	

}
?>