<?php
header('Access-Control-Allow-Origin: *');
set_error_handler('err_handler');
function err_handler($errno, $errmsg, $filename, $linenum) {
	$date = date('Y-m-d H:i:s (T)');
	$f = fopen('errors.txt', 'a');
	if (!empty($f)) {
		$filename  = str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename);
		$err  = "$date: $errmsg = $filename = $linenum\r\n";
		fwrite($f, $err);
		fclose($f);
	}
}

define('SYSTEM_START_9876543210', true);
date_default_timezone_set('Europe/Moscow');

$data_or = json_decode(file_get_contents('php://input'), true);

$data = array();

foreach ($data_or as $k=>$v)
{
	if (is_array($v))
	{
		$data[htmlspecialchars($k, ENT_QUOTES)] = array();
		foreach ($v as $k2=>$v2)
		{
			$data[htmlspecialchars($k, ENT_QUOTES)][htmlspecialchars($k2, ENT_QUOTES)] = htmlspecialchars($v2, ENT_QUOTES);
		}
	}
	else
		$data[htmlspecialchars($k, ENT_QUOTES)] = htmlspecialchars($v, ENT_QUOTES);
}

$fp = fopen("raw.log", "a+");
$data_ = date("Y-m-d H:i:s", time());
$data_ .= "\n" . print_r($data_or, true) . "\n\n";

$n_k = 'firstname';
$n_v = "''";
if (isset($data['name']))
{
	$name = preg_split("/ /", $data['name']);
	$data_ .= "\n" . print_r($name, true) . "\n\n";

	if (is_array($name))
	{
		$n_k = 'firstname';
		$n_v = "'" . $name[0] . "'";
		
		if (count($name) > 2)
		{
			$n_k .= ', middlename';
			$n_v .= ", '" . $name[1] . "'";
			$n_k .= ', lastname';
			$n_v .= ", '" . $name[2] . "'";
		}	
		else if (count($name) > 1)
		{
			$n_k .= ', lastname';
			$n_v .= ", '" . $name[1] . "'";
		}
	}
	else
	{
		$n_k = 'lastname';
		$n_v = $name;
	}
}

$test = fwrite($fp, $data_);
fclose($fp);

$phones_m = $data['phones'];
$phones = '';
if (count($phones_m) < 1)
	die();

foreach($phones_m as $v)
{
	if ($phones != '')
		$phones .= ';;;';
	
	$phones .= $v;
}

$rawdata = '';
foreach($data as $k=>$v)
{
	if ($rawdata != '')
		$rawdata .= '<br>';
	
	if (is_array($v))
	{
		$rawdata .= "$k==>";
		foreach($v as $ak=>$av)
		{
			$rawdata .= "$ak=>$av;;;";
		}
	}
	else
		$rawdata .= "$k==>$v";
}

include_once "../../_bdc.php";
	
$sql = "INSERT INTO wantresult_forms(date, source, $n_k, phones, rawdata) VALUES(NOW(), 'wantresult', $n_v, '$phones', '$rawdata');";
if ($result = $db_connect->query($sql))
{
	$fp = fopen("input.log", "a+");
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= "\n" . print_r($sql, true) . "\n\n";
	$test = fwrite($fp, $data_);
	fclose($fp);
	mysqli_close($db_connect);
}
else
{
	$fp = fopen("error.log", "a+");
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= "\n" . print_r($sql, true);
	$data_ .= "\n" . print_r($db_connect->errno . " => " . $db_connect->error, true) . "\n\n";
	$test = fwrite($fp, $data_);
	fclose($fp);
	mysqli_close($db_connect);
}
?>