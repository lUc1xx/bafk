<?php
define('SYSTEM_START_9876543210', true);
date_default_timezone_set('Europe/Moscow');


/*
App Name: bsmart
App Redirect URLs: None
App ID: 3be1f9a3c1d94e73aeac7983e064d3f8
App Secret: 6307f13f6d204f1da7a224774422076e
App Type: trusted
App Access: call_api
Created: 2018-09-12 16:29:10
*/

include_once "../../_bdc.php";
include_once "../../_functions.php";

$key = '1234';

//if (!isset($_POST))
	//die('miss data');

//if (sizeof($_POST) == 0)
	//die('size 0');
$incoming_mass_p = $_POST;
$incoming_mass_g = $_GET;
$req = $_REQUEST;
$ser = $_SERVER;

$fp = fopen("test.log", "a+");
$data_ = date("Y-n-j H:i:s", time());

$data_ .= "\n" . print_r($req, true);
$data_ .= "\n" . print_r($ser, true);
$data_ .= "\n" . print_r($incoming_mass_p, true);
$data_ .= "\n" . print_r($incoming_mass_g, true);

$data_ .= "\n\n";
$test = fwrite($fp, $data_);
fclose($fp);

if (!isset($incoming_mass_p['key']))
{
	$res['status'] = 'error';
	$res['error'] = 'Не передан ключ';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	mysqli_close($db_connect);
	die();
}

$key_i = $incoming_mass_p['key'];

if ($key_i != $key)
{
	$res['status'] = 'error';
	$res['error'] = 'Передан неверный ключ';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	mysqli_close($db_connect);
	die();
}

if (!isset($incoming_mass_p['action']))
{
	$res['status'] = 'error';
	$res['error'] = 'Не выбрано действие';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	mysqli_close($db_connect);
	die();
}

$agent_id = '';
$agent_name = '';
if ($incoming_mass_p['action'] != 'login')
{
	$token = $incoming_mass_p['token'];
	$login = $incoming_mass_p['l'];
	$sql = "SELECT * FROM agents WHERE login='$login' AND token='$token';";
	//$res['sql'] = $sql;
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$deleted = 0 + $row['deleted'];
			$agent_id = 0 + $row['id'];
			$agent_name = $row['name'];
			
			if ($deleted)
			{
				$error = true;
				$res['error'] = 'Аккаунт заблокирован!';
			}
		}
		else
		{
			$res['error'] = 'Аккаунт не найден!';
			$error = true;
		}
		$result->close();
	}
	else
	{
		$res['error'] = 'Ошибка базы данных. Свяжитесь с администратором!';
		$error = true;
	}
	
	if ($error)
	{
		$res['status'] = 'error';
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		mysqli_close($db_connect);
		die();
	}
}

switch($incoming_mass_p['action'])
{
	case "login" :
	{
		$res['action'] = 'login';
		$error = false;
		$login = $incoming_mass_p['l'];
		$pass = $incoming_mass_p['p'];
		$sql = "SELECT * FROM agents WHERE login='$login' AND password='$pass';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$deleted = 0 + $row['deleted'];
				
				if ($deleted)
				{
					$error = true;
					$res['error'] = 'Аккаунт заблокирован!';
				}
				$token = $row['token'];
				$name = $row['name'];
			}
			else
			{
				$res['error'] = 'Аккаунт не найден!';
				$error = true;
			}
			$result->close();
		}
		else
		{
			$res['error'] = 'Ошибка базы данных. Свяжитесь с администратором!';
			$error = true;
		}
		
		if ($error)
		{
			$res['status'] = 'error';
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		$res['token'] = $token;
		$res['name'] = $name;
		
		break;
	}
	
	case "list" :
	{
		$res['action'] = 'list';
		$error = false;
		$data = array();
		$sql = "SELECT id, date_add, lastname, firstname, middlename, sum, status FROM forms WHERE source='agent' AND source_param='$agent_id' ORDER BY id DESC;";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				while($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$now = new DateTime;
				
					$yesterday = new DateTime;
					$yesterday = $yesterday->modify('-1 day');
					
					$otherDate = new DateTime($row['date_add']);
					$dsplit = preg_split("/ /", $row['date_add']);
					
					if ($now->format('Y-m-d') == $otherDate->format('Y-m-d'))
						$row['date_add'] = $dsplit[1];
					else if ($yesterday->format('Y-m-d') == $otherDate->format('Y-m-d'))
						$row['date_add'] = "Вчера " . $dsplit[1];
					else
						$row['date_add'] = date("d.m.y <b>H:i</b>",$otherDate->getTimestamp());
				
					array_push($data, $row);
				}
			}
			$result->close();
		}
		else
		{
			$res['error'] = 'Ошибка базы данных. Свяжитесь с администратором!';
			$error = true;
		}
		
		if ($error)
		{
			$res['status'] = 'error';
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		$res['data'] = $data;
		
		break;
	}
	
	case "details" :
	{
		$fid = unserialize($incoming_mass_p['data']);
		$res['action'] = 'details';
		$error = false;
		$data = array();
		$log = array();
		$data_s = array();
		
		
		$all_fields_mass = array();
	
		$sql = "SELECT name, type, description FROM form_fields_settings WHERE type!='block' AND agent_read='1' ORDER BY position ASC;";
		if ($result = $db_connect->query($sql))
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$all_fields_mass[$row['name']] = array("type" => $row['type'], "desc" => $row['description']);
			}
			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		
		$sql = "SELECT * FROM forms WHERE id='$fid';";
		
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				
				if (($row['source'] != 'agent') || ($row['source_param'] + 0 != $agent_id + 0))
				{
					$res['status'] = 'error';
					$res['error'] = "Нет доступа к данной анкете";
					print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
					$result->close();
					mysqli_close($db_connect);
					die();
				}
				
				$data_mass_a = array();
				$data_mass = preg_split("/\<br\>/", $row['data']);
				
				foreach ($data_mass as $key => $val)
				{
					if ($val == "")
						continue;
					$value = preg_split("/::/", $val);
					$data_mass_a[$value[0]] = htmlspecialchars($value[1]);
				}
				
				$data_mass_a['lastname'] = $row['lastname'];
				$data_mass_a['firstname'] = $row['firstname'];
				$data_mass_a['middlename'] = $row['middlename'];
				$data_mass_a['sum'] = $row['sum'];
				$data_mass_a['sum_val'] = $row['sum_val'];
				
			//	foreach ($data_mass_a as $key => $val)
				foreach ($all_fields_mass as $key => $t)
				{
					if (array_key_exists($key, $data_mass_a))
					{
						$val = $data_mass_a[$key];
						array_push($data, array("key" => $key, "val" => $val, "type" => $t['type'], "description" => $t['desc']));
					}
				}
				
				$data_s['status'] = $row['status'];
				$data_s['date'] = $row['date_add'];
				
				$res['data_s'] = $data_s;
				
				//$data = $row;
				
			}
			else
			{
				$res['status'] = 'error';
				$res['error'] = "Нет доступа к данной анкете";
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				$result->close();
				mysqli_close($db_connect);
				die();
			}
			
			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		
		$res['data'] = $data;
		
		
		$sql = "SELECT date FROM sms_log WHERE form_id='$fid' AND command='sendSMS';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					array_push($log, array('timestamp' => strtotime($row['date']), 'time' => $row['date'], 'type' => 'sms', 'msg' => 'Сообщение клиенту отправлено'));
				}
			}

			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "4: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		$sql = "SELECT flow, init_time_gmt, result, hangup_cause, hangup_disposition FROM calls_history WHERE form_id='$fid';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$row['date'] = date("Y-m-d H:i:s", strtotime($row['init_time_gmt']) + 3600*3);
					$tmp_msg = '';
					
					if ($row['result'] == 'bridged')
						$tmp_msg = 'Совершен звонок';
					else if (($row['result'] == 'answered') && ($row['hangup_cause'] == 'NORMAL_CLEARING'))
						$tmp_msg = 'Совершен звонок';
					else if ($row['flow'] == 'out')
					{
						$tmp_msg = 'Недозвон до клиента';
					}
					
					if ($tmp_msg != '')
						array_push($log, array('timestamp' => strtotime($row['date']), 'time' => $row['date'], 'type' => 'call', 'msg' => $tmp_msg));
				}
			}

			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "4: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			//$res['msg2'] = $sql;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		$sql = "SELECT date, msg, type FROM form_log WHERE form_id='$fid' AND (type='agent' OR type='toagent' OR type='robot');";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					if ($row['type'] == 'robot')
					{
						if (!preg_match('/Анкета закрыта/', $row['msg']))
							continue;
						$tmp_msg = "Робот: " . $row['msg'];
					}
					else if ($row['type'] == 'agent')
						$tmp_msg = "$agent_name: " . $row['msg'];
					else
						$tmp_msg = "Сотрудник: " . $row['msg'];
					array_push($log, array('timestamp' => strtotime($row['date']), 'time' => $row['date'], 'type' => 'msg', 'msg' => $tmp_msg));
				}
			}

			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "4: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
	
		
		$res['log'] = $log;
		break;
	}
	
	case "create" :
	{
		$res['action'] = 'create';
		$error = false;
		$data = array();
		$sql = "SELECT * FROM form_fields_settings WHERE agent_edit='1';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				while($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$str['type'] 			= $row['type'];
					$str['name'] 			= $row['name'];
					$str['block'] 			= $row['block'];
					$str['goal'] 			= $row['goal'];
					$str['mainid'] 			= $row['mainid'];
					$str['position'] 		= $row['position'];
					$str['description'] 	= $row['description'];
					$str['av_values'] 		= $row['av_values'];
					
					array_push($data, $str);
				}
			}
			$result->close();
		}
		else
		{
			$res['error'] = 'Ошибка базы данных. Свяжитесь с администратором!';
			$error = true;
		}
		
		if ($error)
		{
			$res['status'] = 'error';
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		$res['data'] = $data;
		
		break;
	}
	
	case "add" :
	{
		$res['action'] = 'add';
		$data2sql = unserialize($_POST['data']);
		$res['data2sql'] = $data2sql;
		
		
		$all_fields_mass = array();
		$phone_list = array();
	
		$sql = "SELECT name, type FROM form_fields_settings WHERE type!='block' AND agent_edit='1' ORDER BY position ASC;";
		if ($result = $db_connect->query($sql))
		{
				
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$all_fields_mass[$row['name']] = $row['type'];
			}
			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		if ($result = $db_connect->query("SELECT name, type FROM form_fields_settings WHERE type='telephone';"))
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$phone_list[] = $val['name'];
			}
			
			$result->close();
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		$phone_history = '';
		$phone_test = '';
		
		foreach ($data2sql as $name => $value)
		{
			//проверка на допустимый ключ из БД?
			if (isset($all_fields_mass[$name]))
			{
				if ($all_fields_mass[$name] == 'numbers')
				{
					$value = preg_replace("/[^0-9]/", '', $value);
				}
			}
			
			switch($name)
			{
				case 'block':
				case 'action':
				case 'f_pod_ch':
				case 'sssel':
				case 'id': continue; break;
				case 'lastname':
				case 'firstname':
				case 'middlename':
				case 'sum':
				case 'sum_val':
				{
					if ($keys != '')
						$keys .= ', ';
					$keys .= $name;
					
					if ($vals != '')
						$vals .= ', ';
					$vals .= "'$value'";
					break;
				}
				default: 
				{
					if ($value != '')
					{
						if (is_array($value))
						{
							//print_r($value);
							$tmp_raw_data = "$name::";
							$ff = true;
							$hit_array = false;
							foreach ($value as $v)
							{
								
								if ($v != '0')
								{
									//echo $v . "\n";
									$hit_array = true;
									if ($ff)
									{
										$ff = false;
										$tmp_raw_data .= "$v";
									}
									else
										$tmp_raw_data .= ",$v";
								}

								
							}
							$tmp_raw_data .= "<br>";
							//print_r($tmp_raw_data);
							//print_r("\n" . $hit_array . "\n");
							
							if ($hit_array)
								$raw_data .= $tmp_raw_data;
							//print_r($raw_data);
							
						}
						else if (array_search($name, $phone_list) !== false)
						{
							$value = preg_replace("/[^0-9]/", '', $value);
							
							if (strlen($value) > 7)
							{
								$raw_data .= "$name::$value<br>";
								$phone_history .= "$value;;;";
								$phone_test = $value;
							}
							
							if ($name == 'field29')
							{
								$ddd .= ", contact_phone='$value'";
								if ($keys != '')
									$keys .= ', ';
								$keys .= 'contact_phone';
								
								if ($vals != '')
									$vals .= ', ';
								$vals .= "'$value'";
							}
						}
						else
							$raw_data .= "$name::$value<br>";
					}
					break;
				}
			}
			
		}
		$raw_data = addslashes($raw_data);
		
		$status ='new';
		$office = 0;
		$manager = 0;
		
		$afs = md5(time() . $raw_data);
		$sql = "INSERT INTO forms(date_add, type, sum_val, status, office, manager, city, phone_history, source, source_param, $keys, data, afs_key) VALUES(NOW(), 'mini', 'rub', '$status', '$office', '$manager', 'Москва', '$phone_history', 'agent', '$agent_id', $vals, '$raw_data', '$afs');";
		
		$res['sql'] = $sql;
		
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				$res['status'] = 'error';
				$res['sql'] = $sql;
				$res['error'] = "Ошибка базы данных";
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				mysqli_close($db_connect);
				die();
			}
		}
		else
		{
			$res['status'] = 'error';
			$res['error'] = "Ошибка базы данных";
			$res['sql'] = "12: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			mysqli_close($db_connect);
			die();
		}
		
		
		
		$sql = "SELECT id FROM forms WHERE afs_key = '$afs';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$res['id'] = $row['id'];
			}
			$result->close();
		}
		
		check_double($res['id'], $phone_test);
		
		break;
	}
	
	case "add_msg" :
	{
		$res['action'] = 'add_msg';
		$data2sql = unserialize($_POST['data']);
		$fid = $data2sql['id'];
		$msg = $data2sql['msg'];
		
		$sql = "INSERT INTO form_log(form_id, date, type, creator, msg) VALUES ('$fid', NOW(), 'agent', '$agent_id', '$msg');";
		
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				$res['status'] = 'error';
				$res['error'] = $sql;
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				mysqli_close($db_connect);
				die();
			}
		}
		break;
	}
	
	default:
	{
		$res['status'] = 'error';
		$res['error'] = 'Неверное действие: ' . $incoming_mass_p['action'];
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		mysqli_close($db_connect);
		die();
	}
}


$res['status'] = 'OK';
print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
mysqli_close($db_connect);
?>