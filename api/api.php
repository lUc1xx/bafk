<?php
define('SYSTEM_START_9876543210', true);
date_default_timezone_set('Europe/Moscow');

include_once "../_config.php";
include_once "../_bdc.php";
include_once "../_functions.php";

if (!isset($_POST))
	die();

if (sizeof($_POST) == 0)
	die();
$incoming_mass = $_POST;

if ($incoming_mass == "")
	die();
$fp = fopen("../../logs/api.log", "a+");
$data_ = date("Y-m-d H:i:s", time());
$data_ .= "\n " . print_r($_POST,true) . "\n";

if (isset($_POST['old_format']) && ($_POST['old_format'] == '1'))
//if (true)
{

	
	$fname = $_POST['form_name'];
	$ip = $_POST['client_ip'];
	$site = $_POST['host_url'];

	$client_city = '';	

	if($geo_info = file_get_contents(sprintf('http://api.sypexgeo.net/json/%s', $ip)))
	{
		$res = json_decode($geo_info);
		$client_city = $res->city->name_ru;
	}
	$data_ .= "\n " . print_r($client_city,true) . "\n";
	
	
	if ($fname == 'Сотрудничество')
	{
		$fields = array(
		'LAST_NAME' => $_POST['person_name'],
		'FIRST_NAME' => $_POST['creditor_name'],
		'MIDDLE_NAME' => $_POST['creditor_type'],
		'proposal_content' => $_POST['proposal_content'],
		'ip' => $ip,
		'city' => $client_city,
		'sum' => (isset($_POST['Сумма'])) ? $_POST['Сумма'] : '0',
		'sum_val' => 'rub',
		'type' => ($fname == 'Сотрудничество') ? 'cooperate' : 'mini',
		'mob_tel' => preg_replace("/[^0-9]/", '', $_POST['person_phone'])
		);
	}
	else if ($fname == 'заявка_агент')
	{
		$fields = array(
		'LAST_NAME' => '',
		'FIRST_NAME' => $_POST['Имя'],
		'MIDDLE_NAME' => '',
		'ip' => $ip,
		'city' => $client_city,
		'sum' => '0',
		'sum_val' => 'rub',
		'type' => 'agent',
		'mob_tel' => preg_replace("/[^0-9]/", '', $_POST['Телефон_мобильный'])
		);
	}
	else
	{
		$fields = array(
		'LAST_NAME' => $_POST['Фамилия'],
		'FIRST_NAME' => $_POST['Имя'],
		'MIDDLE_NAME' => $_POST['Отчество'],
		'ip' => $ip,
		'city' => $client_city,
		'BIRTH_DATE' => $_POST['ДР'],
		'dohod' => preg_replace("/[^0-9]/", '', $_POST['Доход']),
		'goal' => $_POST['Цель_кредита'],
		'pri' => $_POST['Приоритет'],
		'oldf' => $_POST['Прежняя_фамилия'],
		'gr' => $_POST['Гражданство'],
		'ac' => $_POST['Адрес_город'],
		'email' => $_POST['e-mail'],
		'podt' => $_POST['Подтип_анкеты'],
		'step' => $_POST['шаг'],
		'rec' => $_POST['рекламная_кампания'],
		'search' => $_POST['поисковая_фраза'],
		'sum' => $_POST['Сумма'],
		'sum_val' => 'rub',
		'type' => ($fname == 'Сотрудничество') ? 'cooperate' : 'mini',
		'mob_tel' => preg_replace("/[^0-9]/", '', $_POST['Телефон_мобильный'])
		);
	}
	
	if (isset($_POST['seo']))
		$fields['seo'] = $_POST['seo'];
	
	if (isset($_POST['express']))
		$fields['express'] = $_POST['express'];
	
	if (isset($_POST['zalog']))
		$fields['zalog'] = $_POST['zalog'];
	
	if (isset($_POST['sms']))
		$fields['sms'] = $_POST['sms'];
	
	if (isset($_POST['discount']))
		$fields['discount'] = $_POST['discount'];
	
	if (isset($_POST['Коммент']))
		$fields['comment'] = $_POST['Коммент'];

	if (isset($_POST['quiz']))
		$fields['quiz'] = $_POST['quiz'];
	
	$incoming_mass = array(
	'KEY' => 123456789,
	'site' => $site,
	'fields' => $fields
	);
}



$data_ .= "\n " . print_r($incoming_mass,true) . "\n\n";


$data2sql = array();
$raw_data = '';
$raw_data_orig = '';

$miss_form = true;
foreach ($incoming_mass['fields'] as $key => $value)
{
	if ($key == 'LAST_NAME')
	{
		$data2sql['lastname'] = $value;
	}
	else if ($key == 'FIRST_NAME')
	{
		$data2sql['firstname'] = $value;
	}
	else if ($key == 'MIDDLE_NAME')
	{
		$data2sql['middlename'] = $value;
	}
	else if ($key == 'sum')
	{
		$data2sql['sum'] = 0 + preg_replace("/[^0-9]/", '', $value);
	}
	else if ($key == 'sum_val')
	{
		$data2sql['sum_val'] = $value;
	}
	else if ($key == 'city')
	{
		$data2sql['city'] = $value;
	}
	else if ($key == 'type')
	{
		$data2sql['type'] = $value;
	}
	else if ($key == 'sms')
	{
		$data2sql['sms'] = $value;
	}
	else if ($key == 'seo')
	{
		$data2sql['seo'] = $value;
	}
	else if ($key == 'express')
	{
		if (($value == 'true') || ($value + 0 == 1))
			$data2sql['isexpress'] = 1;
	}
	else if ($key == 'quiz')
	{
		if ((strtolower($value) == 'yes') || ($value == 'true') || ($value + 0 == 1))
			$data2sql['quiz'] = 1;
	}
	else if ($key == 'zalog')
	{
		$data2sql['zalog'] = 0 + $value;
	}
	else if ($key == 'discount')
	{
		$data2sql['discount'] = ($value == 'yes') ? 1 : 0;
		$data2sql['discount_val'] = 10000;
	}
	else if ($key == 'ip')
	{
		$data2sql['ip'] = $value;
	}
	else if ($key == 'BIRTH_DATE')
	{
		//$data2sql['field14'] = $value;
		$raw_data .= "birth_day::$value<br>";
		$raw_data_orig .= "birth_day::$value<br>";
	}
	else if ($key == 'mob_tel')
	{
		if ($value != '')
		{
			$miss_form = false;
			//$data2sql['field29'] = $value;
			$raw_data .= "field29::$value<br>";
			$raw_data_orig .= "field29::$value<br>";
			$data2sql['contact_phone'] = "$value";
			$data2sql['phone_history'] = "$value;;;";
		}
	}
	else if ($key == 'dohod')
	{
		if (0 + $value > 0)
		{
			$raw_data .= "field75::$value<br>";
			$raw_data_orig .= "field75::$value<br>";
		}
	}
	else if ($key == 'oldf')
	{
		if (($value != '') && ($value != 'нет'))
		{
			$raw_data .= "field11::$value<br>";
			$raw_data_orig .= "field11::$value<br>";
		}
	}
	else if ($key == 'email')
	{
		if (($value != '') && ($value != 'нет'))
		{
			$miss_form = false;
			$raw_data .= "email::$value<br>";
			$raw_data_orig .= "email::$value<br>";
		}
	}
	else if ($key == 'proposal_content')
	{
		if (($value != '') && ($value != 'нет'))
		{
			$raw_data .= "proposal_content::$value<br>";
			$raw_data_orig .= "proposal_content::$value<br>";
		}
	}
	else if ($key == 'comment')
	{
		$data2sql['comment'] = $value;
	}
}

if (isset($_POST['track']) && ($_POST['track'] != ''))
	$data2sql['track'] = urldecode($_POST['track']);
else if (isset($_POST['req_qs']) && ($_POST['req_qs'] != ''))
	$data2sql['track'] = urldecode($_POST['req_qs']);

foreach ($data2sql as $name => $value)
{
	if ($value)
		$raw_data_orig .= "$name::$value<br>";
}
$raw_data = addslashes($raw_data);
$raw_data_orig = addslashes($raw_data_orig);

$data2sql['source'] = "site";
if (isset($incoming_mass['site']))
	$data2sql['source_param'] = $incoming_mass['site'];
else
	$data2sql['source_param'] ="-";

$data2sql['status'] = "new";


$data2sql['data_orig'] = $raw_data_orig;
$data2sql['data'] = $raw_data;

//print_r($data2sql);
$data_ .= "\nt" . print_r($data2sql,true) . "\n\n";
//print_r($_SERVER);
//print_r($_REQUEST);

if($miss_form)
	$res = 'miss phone and email';
else
	$res = print_r(insert_into_sql('forms', $data2sql),true);
$data_ .= "\n" . $res . "\n\n";

$test = fwrite($fp, $data_);
fclose($fp);

echo $res;

mysqli_close($db_connect);
?>