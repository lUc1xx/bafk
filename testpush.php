<?php
if (!defined('SYSTEM_START_9876543210')) exit;
?>
<script src="/scripts/firebase/jquery.js"></script>
<script src="/scripts/firebase/firebase-app.js"></script>
<script src="/scripts/firebase/firebase-messaging.js"></script>
<script src="/scripts/firebase/init.js"></script>
<script src="/scripts/firebase/firebase_subscribe.js"></script>