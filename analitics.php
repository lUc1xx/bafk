<?
if (!defined('SYSTEM_START_9876543210')) exit;
echo '<script type="text/javascript" src="/scripts/analitics.js?' . rand() . '"></script>';
?>

<div id="mapp">
    <template>
        <div class="q-pa-md">
            <div class="q-gutter-y-md">
                <q-card>
                    <q-tabs
                        v-model="tab"
                        dense
                        class="text-grey"
                        active-color="primary"
                        indicator-color="primary"
                        align="justify"
                        narrow-indicator
                    >
                        <q-tab name="dailyConv" label="Ежедневная конверсия"></q-tab>
                        <q-tab name="t2" label="Заготовка" disable></q-tab>
                        <q-tab name="t3" label="Заготовка" disable></q-tab>
                    </q-tabs>

                    <q-separator></q-separator>

                    <q-tab-panels v-model="tab" animated>
                        <q-tab-panel name="dailyConv">
                            <template>
                                <div class="q-pa-md">
                                    <q-table
                                            :data="dailyConvTable.data"
                                            :columns="dailyConvTable.columns"
                                            row-key="name"
                                            hide-bottom
                                            :pagination.sync="dailyConvTable.pagination"
                                            :rows-per-page-options="[0]"
                                    />
                                </div>
                            </template>
                        </q-tab-panel>

                        <q-tab-panel name="t2" disable>
                            <div class="text-h6">Alarms</div>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </q-tab-panel>

                        <q-tab-panel name="t3" disable>
                            <div class="text-h6">Movies</div>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </q-tab-panel>
                    </q-tab-panels>
                </q-card>
            </div>
        </div>
    </template>
</div>


