<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$res['status'] = 'OK';
$data = array();

switch($_POST['action'])
{
	case "get_signals":
	{
		if ($result = $db_connect->query("SELECT * FROM wantresult_forms ORDER BY ID DESC;"))
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$row['timestamp'] = strtotime($row['date']);
				$data[] = $row;
			}
			$res['data'] = $data;
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	default:
	{
		$res['status'] = 'failed';
		$res['msg'] = 'Недопустимое действие ' . $_POST['action'];
		break;
	}
}



print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
?>