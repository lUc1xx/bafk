<?php
if (!defined('SYSTEM_START_9876543210')) exit;  

udpate_staff_onilne($staff_id_debug);

//print_r($_POST);

//echo boolval($_POST['en']) . "\n";

$add_worker = false;
$edit_worker = false;
$get_worker = false;
$update_data = false;
$del_worker = false;
$get_office = false;
$edit_office = false;
$add_office = false;
$del_office = false;
$set_ip_avalues = false;
$get_ip_avalues = false;
$update_tm_sort_en = false;
$res = array();
$mass = array();


$list_offices = array();

if ($result = $db_connect->query("SELECT id, name FROM offices ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
		$list_offices[$row['id']] = $row['name'];
	$result->close();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

if ($_POST['action'] == 'add_worker')
{
	$t_key = "";
	$t_val = "";
	foreach ($_POST as $k => $v)
	{
		$key = $k;
		switch($k)
		{
			case 'passwd':
				$key = 'password';
			case 'login':
			case 'lastname':
			case 'firstname':
			case 'patronymic':
			case 'email':
			case 'email_corp':
			case 'phone_mob':
			case 'phone_work':
			case 'office':
			case 'details':
			case 'position':
			{
				if ($t_key)
					$t_key .= ", ";
				$t_key .= "$key";
				
				if ($t_val)
					$t_val .= ", ";
				$t_val .= "'$v'";
				break;
			}
			default: break;
		}
	}
	$sql = "INSERT INTO staff($t_key) VALUES ($t_val);";
	$add_worker = true;
	//echo $sql . "\n";
}
else if ($_POST['action'] == 'edit_staff_param')
{
	$id = 0 + $_POST['id'];
	$val = $_POST['val'];
	$name = $_POST['name'];
	$error = false;
	
	switch($name)
	{
		case "edit_phone_add" :
		{
			$sql = "UPDATE staff SET phone_work_add='$val' WHERE id='$id';";
			$edit_worker = true;
			break;
		}
		default :
		{
			$error = true;
			break;
		}
	}
	
	if ($error)
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не найден параметр $name";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
}
else if ($_POST['action'] == 'edit_worker')
{
	$values = "";
	foreach ($_POST as $k => $v)
	{
		$key = $k;
		switch($k)
		{
			case 'passwd':
			{
				if ($v != "")
					$key = 'password';
				else
					break;
			}
			case 'login':
			case 'lastname':
			case 'firstname':
			case 'patronymic':
			case 'email':
			case 'email_corp':
			case 'phone_mob':
			case 'phone_work':
			case 'office':
			case 'details':
			case 'position':
			{
				if ($values)
					$values .= ", ";
				$values .= "$key='$v'";

				break;
			}
			default: break;
		}
	}
	
	$id = 0 + $_POST['id'];
	$sql = "UPDATE staff SET $values WHERE id='$id';";
	
	$edit_worker = true;
}
else if ($_POST['action'] == 'set_ip_avalues')
{
	$id = 0 + $_POST['id'];
	$data = '';
	foreach ($_POST as $key => $value)
	{
		if (preg_match("/key\d+/", $key))
		{
			$v = $value;
			if ($data != '')
				$data .= ";;;$v";
			else
				$data .= "$v";
		}
			
	}
	
	/*if ($data == '')
	{
		$res['status'] = 'failed';
		$res['msg'] = 'Не введено ни одного IP';
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}*/
	
	$sql = "UPDATE staff SET ip_avail_val='$data' WHERE id = '$id';";
	$set_ip_avalues = true;
}
else if ($_POST['action'] == 'tm_sort_en')
{
	$id = 0 + $_POST['stid'];
	$st = 0 + $_POST['st'];
	$sql = "UPDATE staff SET in_tm_sort='$st' WHERE id='$id';";
	$update_tm_sort_en = true;
}
else if ($_POST['action'] == 'fin_an_en')
{
	$id = 0 + $_POST['stid'];
	$st = 0 + $_POST['st'];
	$sql = "UPDATE staff SET fin_an='$st' WHERE id='$id';";
	$update_tm_sort_en = true;
}
else if ($_POST['action'] == 'plan_check_en')
{
	$id = 0 + $_POST['stid'];
	$st = 0 + $_POST['st'];
	$sql = "UPDATE staff SET plan_check='$st' WHERE id='$id';";
	$update_tm_sort_en = true;
}
else if ($_POST['action'] == 'bc_check_en')
{
	$id = 0 + $_POST['stid'];
	$st = 0 + $_POST['st'];
	$sql = "UPDATE staff SET staff_bc='$st' WHERE id='$id';";
	$update_tm_sort_en = true;
}
else if ($_POST['action'] == 'get_worker_ip')
{
	$id = 0 + $_POST['id'];
	$sql = "SELECT ip_avail_val FROM staff WHERE id='$id';";
	$get_ip_avalues = true;
}
else if ($_POST['action'] == 'get_worker')
{
	$id = 0 + $_POST['id'];
	$sql = "SELECT * FROM staff WHERE id='$id';";
	$get_worker = true;
}
else if ($_POST['action'] == 'del_worker')
{
	$id = 0 + $_POST['id'];
	$sql = "UPDATE staff SET deleted=1 WHERE id='$id';";
	$del_worker = true;
}
else if ($_POST['action'] == 'update_data')
{
	$sql = "SELECT * FROM staff WHERE deleted='0';";
	$update_data = true;
}
else if ($_POST['action'] == 'get_office')
{
	$id = 0 + $_POST['id'];
	$sql = "SELECT * FROM offices WHERE id='$id';";
	$get_office = true;
}
else if ($_POST['action'] == 'edit_office')
{
	$values = "";
	foreach ($_POST as $k => $v)
	{
		$key = $k;
		switch($k)
		{
			case 'type':
			case 'subtype':
			case 'name':
			case 'description':
			case 'access':
			{
				if ($values)
					$values .= ", ";
				$values .= "$key='$v'";

				break;
			}
			default: break;
		}
	}
	
	$id = 0 + $_POST['id'];
	$sql = "UPDATE offices SET $values WHERE id='$id';";
	
	$edit_office = true;
}
else if ($_POST['action'] == 'del_office')
{
	$id = 0 + $_POST['id'];
	//$sql = "DELETE FROM offices WHERE id='$id';";
	$sql = "UPDATE offices SET deleted='1' WHERE id='$id';";
	$del_office = true;
}
else if ($_POST['action'] == 'add_office')
{
	$t_key = "";
	$t_val = "";
	foreach ($_POST as $k => $v)
	{
		$key = $k;
		switch($k)
		{
			case 'type':
			case 'subtype':
			case 'name':
			case 'description':
			case 'access':
			{
				if ($t_key)
					$t_key .= ", ";
				$t_key .= "$key";
				
				if ($t_val)
					$t_val .= ", ";
				$t_val .= "'$v'";
				break;
			}
			default: break;
		}
	}
	$sql = "INSERT INTO offices($t_key) VALUES ($t_val);";
	$add_office = true;
	//echo $sql . "\n";
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "Не найдена задача " . $_POST['action'];
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

if ($result = $db_connect->query($sql))
{
	if ($add_worker || $del_worker || $edit_worker || $edit_office || $del_office || $add_office || $set_ip_avalues || $update_tm_sort_en)
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			//$res['msg'] = $sql;
			//$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
			//$res['msg'] = "Не найден id: $id";
			//$result->close();
		}
	}
	else if ($update_data || $get_worker)
	{
		if ($result->num_rows)
		{
			$res['status'] = 'ok';
			$res['data_len'] = $result->num_rows;
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				//array_push($mass, $row);
				$of_id = $row['office'];
				$of = (array_key_exists($of_id, $list_offices)) ? $list_offices[$of_id] : '<font color="red">Офис расформирован</font>';
				$row['office'] = $of;
				$row['position'] = $work_position[$row['position']];
				$id = $row['id'];
				unset($row['id']);
				unset($row['password']);
				$mass[$id] = $row;
			}
			$res['data'] = $mass;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Select вернул " . $result->num_rows . " строк.\n";
		}
		$result->close();
	}
	else if ($get_ip_avalues)
	{
		$res['status'] = 'ok';
		$res['len'] = 0;
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$data = explode(';;;', $row['ip_avail_val']);
			$res['len'] = ($row['ip_avail_val'] == '') ? 0 : sizeof($data);
			$res['data'] = $data;
		}
		$result->close();
	}
	else if ($get_office)
	{
		if ($result->num_rows)
		{
			$res['status'] = 'ok';
			$res['data_len'] = $result->num_rows;
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				//array_push($mass, $row);
				//$row['office'] = $list_offices[$row['office']];
				//$row['position'] = $work_position[$row['position']];
				$id = $row['id'];
				unset($row['id']);
				$mass[$id] = $row;
			}
			$res['data'] = $mass;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Select вернул " . $result->num_rows . " строк.\n";
		}
		$result->close();
	}
	else
	{
		$res['status'] = 'ok';
		//$res = "Select вернул " . $result->num_rows . " строк.\n";
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($res, $row);
		}
		$result->close();
	}
	
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
}
//print_r($res);
print_r(json_encode($res,JSON_UNESCAPED_UNICODE));

?>