<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

udpate_staff_onilne($staff_id_debug);

$error = false;
if (!$error)
{
	if ($result = $db_connect->query("SELECT id, name FROM offices WHERE 1 ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$offices_mass[$row['id']] = $row['name'];
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}
if (!$error)
{
	if ($result = $db_connect->query("SELECT type FROM offices WHERE id='$staff_office';"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$staff_office_type = $row['type'];
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}

if (!$error)
{
	if ($result = $db_connect->query("SELECT id, lastname, firstname, patronymic FROM staff ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$fio_a = $row['lastname'];
			if ($row['firstname'] != '')
				$fio_a .= ' ' . substr($row['firstname'], 0, 2) . '.';
			if ($row['patronymic'] != '')
				$fio_a .= ' ' . substr($row['patronymic'], 0, 2) . '.';
					
			$staff_mass[$row['id']] = $fio_a;
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}

if ($error)
	die();

$agent_mass = array();

switch($_POST['action'])
{
	case 'calc_result':
	{
		$val = $_POST['val'];
		$man = '';
		if (($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
			$filter = "(lastname LIKE '%$val%' OR data LIKE '%$val%' OR id LIKE '%$val%') AND manager = '$staff_id';";
		else		
			$filter = "lastname LIKE '%$val%' OR data LIKE '%$val%' OR id LIKE '%$val%';";
		//$sql = "SELECT COUNT(*) FROM forms WHERE lastname LIKE '%$val%' OR firstname LIKE '%$val%' OR middlename LIKE '%$val%' OR data LIKE '%$val%';";
		$sql = "SELECT COUNT(*) FROM forms WHERE $filter";
		if ($result = $db_connect->query($sql))
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$res['status'] = 'ok';
			$res['count'] = $row['COUNT(*)'];
			//$res['sql'] = $sql;
			$result->close();
		}
		else
		{
			//$res['sql'] = $sql;
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			echo $res;
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	case 'show_result':
	{
		$val = $_POST['val'];
		$man = '';
		if (($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
			$filter = "(lastname LIKE '%$val%' OR data LIKE '%$val%' OR id LIKE '%$val%') AND manager = '$staff_id'";
		else		
			$filter = "lastname LIKE '%$val%' OR data LIKE '%$val%' OR id LIKE '%$val%'";
		//$sql = "SELECT id, date_add, type, lastname, firstname, middlename, sum, sum_val, status, city, source, source_param, office, manager, call_event, meet_event FROM forms WHERE lastname LIKE '%$val%' OR firstname LIKE '%$val%' OR middlename LIKE '%$val%' OR data LIKE '%$val%' ORDER BY date_add DESC;";
		$sql = "SELECT id, date_add, type, lastname, firstname, middlename, sum, sum_val, status, city, source, source_param, office, manager, call_event, meet_event, data FROM forms WHERE $filter ORDER BY date_add DESC;";
		
		if ($result = $db_connect->query($sql))
		{
			//$res = "Select вернул " . $result->num_rows . " строк.\n";
			$data = array();
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$now = new DateTime;
				
				$yesterday = new DateTime;
				$yesterday = $yesterday->modify('-1 day');
				
				$otherDate = new DateTime($row['date_add']);
				$dsplit = preg_split("/ /", $row['date_add']);
				
				if ($now->format('Y-m-d') == $otherDate->format('Y-m-d'))
					$row['date_add'] = $dsplit[1];
				else if ($yesterday->format('Y-m-d') == $otherDate->format('Y-m-d'))
					$row['date_add'] = "Вчера " . $dsplit[1];
				else
					$row['date_add'] = date("d.m.y <b>H:i</b>",$otherDate->getTimestamp());
				
				$nd = date("Y-m-d H:i:s");
				if (($row['call_event'] != null) && ($row['call_event'] < $nd))
					$row['call_event'] = '<font color="red">' . $row['call_event'] . '</font>';
				
				if (($row['meet_event'] != null) && ($row['meet_event'] < $nd))
					$row['meet_event'] = '<font color="red">' . $row['meet_event'] . '</font>';
				
				if ($row['source'] == 'site')
				{
					if (strncmp($row['source_param'], 'xn--', 4) == 0)
						$row['source_param'] = idn_to_utf8($row['source_param']);
					else if (preg_match("/xn--p1ai/i", $row['source_param']))
						$row['source_param'] = idn_to_utf8($row['source_param']);
					
					$row['source'] = "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}</a>";
				}
				else if ($row['source'] == 'system')
				{
					if ($row['source_param'])
						$row['source'] = $staff_mass[$row['source_param']];
					else
						$row['source'] = '';
				}
				else if ($row['source'] == 'tel')
				{
					if ($row['source_param'])
						$row['source'] = "Тел. " . $row['source_param'];
					else
						$row['source'] = '';
				}
				else if ($row['source'] == 'api')
				{
					$mmm = preg_split("/\|/", $row['source_param']);
					$row['source'] = '<span data-tooltip="id завки:' . $mmm[1] . '<br>id источника:' . $mmm[2] . '">' . $mmm[0] . "</span>";
				}

				$row['is_party_deal'] = 'unknown';
				$data_mass = preg_split("/\<br\>/", $row['data']);
				$filed8_miss = true;
				
				foreach ($data_mass as $key => $val)
				{
					if ($val == "")
						continue;
					$value = preg_split("/::/", $val);
					if ($value[0] == 'field47')
					{
						$v = htmlspecialchars($value[1]);
						
						if ($v == 'Владелец бизнеса')
						{
							//print_r($val);
							$row['biz_owner'] = 1;
						}
						else if ($v == 'Индивидуальный предприниматель')
							$row['ip_owner'] = 1;
						else
							$row['other_work'] = 1;
					}
					
					if ($value[0] == 'field67')
					{
						$v = htmlspecialchars($value[1]);
						
						if ($v == 'Владелец бизнеса')
						{
							//print_r($val);
							$row['biz_owner2'] = 1;
						}
						else if (($v == 'Индивидуальный предприниматель') || ($v == 'ИП'))
							$row['ip_owner2'] = 1;
						else
							$row['other_work2'] = 1;
					}
					
					if ($value[0] == 'field8')
					{
						$filed8_miss = false;
						$v = htmlspecialchars($value[1]);
						$value8 = preg_split("/,/", $v);
						$v8_not = true;
						$v8_res = '';
						$v8_num = 0;
						foreach ($value8 as $k8 => $v8)
						{
							if (mb_strtolower($v8) != 'нет')
							{
								$v8_not = false;
								$v8_res = 'активы';
								$v8_num++;
							}
						}
						$v8_str = $v;
						
						if ($v8_not)
						{
							$v8_res = 'нет';
						}
						
						$row['filed8_val'] = $v8_res;
						$row['filed8_num'] = $v8_num;
						$row['filed8_str'] = $v8_str;
					}

					if ($value[0] == 'field111') {
                        $v = htmlspecialchars($value[1]);

                        if (mb_strtolower($v) == 'анкета заполняется участником сделки')
                        {
                            $row['is_party_deal'] = 'yes';
                        }
                        else
                            $row['is_party_deal'] = 'no';
                    }

				}
				
				if ($filed8_miss)
				{
					$row['filed8_val'] = 'miss';
				}
					
				$row['status_desc'] = $status_description[$row['status']];
				$row['officce_desc'] = ($row['office'] == 0) ? "" : $offices_mass[$row['office']];
				$row['manager_desc'] = ($row['manager'] == 0) ? "" : $staff_mass[$row['manager']];
				array_push($data, $row);
			}
			$res['status'] = 'ok';
			$res['data'] = $data;
			$res['sql'] = $sql;
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	case 'get_list';
	{
		if ($result = $db_connect->query("SELECT id, name FROM agents ORDER BY id ASC;"))
		{
				
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$agent_mass[$row['id']] = $row['name'];
			}
			$result->close();
		}
		else
		{
			$error = true;
			$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			echo $res;
		}
		break;
	}
	default:
	{
		die();
	}
}

$filter = '';
//$filter = " WHERE";

if (isset($_POST['dfrom']) && isset($_POST['dto']))
{
	$dfrom = $_POST['dfrom'];
	$dto = $_POST['dto'];
	$filter .= " (date_add BETWEEN '$dfrom 00:00:00.000000' AND '$dto 23:59:59.999999')";
	$date_filter = "date_add BETWEEN '$dfrom 00:00:00.000000' AND '$dto 23:59:59.999999'";
}
else if (!isset($_POST['dfrom']) && !isset($_POST['dto']))
{
	
	$dfrom = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') -5, date('Y')));
	$today = date("Y-m-d H:i:s", mktime(23, 59, 59, date('m'), date('d'), date('Y')));
	$filter .= " (date_add BETWEEN '$dfrom.000000' AND '$today.999999')";
	$date_filter = "date_add BETWEEN '$dfrom.000000' AND '$today.999999'";
}
else
{
	$date_filter = "";
}

$have_events = "call_event IS NOT NULL OR meet_event IS NOT NULL";

$st = '';
if (isset($_POST['dopwork']) && ($_POST['dopwork'] == '1'))
{
	
	$filter = '';
	$staff_fa = false;
	$staff_pc = false;
	$fa = isset($_POST['fa']) ? $_POST['fa'] : false;
	$pc = isset($_POST['pc']) ? $_POST['pc'] : false;
	
	if ($result = $db_connect->query("SELECT fin_an, plan_check FROM staff WHERE id='$staff_id';"))
	{
		$row = $result->fetch_array(MYSQLI_ASSOC);
		$staff_fa = ($row['fin_an'] + 0) ? true : false;
		$staff_pc = ($row['plan_check'] + 0) ? true : false;
		$result->close();
	}
	//$filter .= " AND status='final_analysis'";
	//$status_filter = "status='final_analysis'";
	
	
	if ($staff_fa && $staff_pc && $fa && $pc)
	{
		//$filter .= " AND (status='final_analysis' OR workplanst='new')";
		$filter .= " (status='final_analysis' OR workplanst='new')";
		$status_filter = "(status='final_analysis' OR workplanst='new')";
	}
	else if ($staff_fa && $fa)
	{
		//$filter .= " AND status='final_analysis'";
		$filter .= " status='final_analysis'";
		$status_filter = "status='final_analysis'";
	}
	else if ($staff_pc && $pc)
	{
		//$filter .= " AND workplanst='new'";
		$filter .= " workplanst='new'";
		$status_filter = "workplanst='new'";
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Нет доступа";
		$res['sql'] = $sql;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
}
else if (isset($_POST['s_status']) && ($_POST['s_status'] != '-1'))
{
	$st = $_POST['s_status'];
	if ($st == 'in_work')
	{
		//$filter .= " AND status!='new' AND status!='new_in_tm' AND status!='new_in_ozs' AND status!='work_complete'";
		
		$filter = "status!='new' AND status!='work_complete'";
		//$filter .= " AND status!='new' AND status!='work_complete'";
		$status_filter = "status!='new' AND status!='work_complete";
	}
	else
	{
		$filter .= " AND status='$st'";
		$status_filter = "status='$st'";
	}
}

if($filter != '')
{
	if ($st == 'in_work')
		$filter = "(($filter) OR call_event IS NOT NULL OR meet_event IS NOT NULL)";
}


if (isset($_POST['s_office']) && ($_POST['s_office'] != '-1'))
{
	$st = $_POST['s_office'];
	$filter .= " AND office='$st'";
	$of_filter = "office='$st'";
}

if (isset($_POST['s_staff']) && ($_POST['s_staff'] != '-1'))
{
	$st = $_POST['s_staff'];
	$filter .= " AND manager='$st'";
	$st_filter = "manager='$st'";
}
	

if ($staff_office_type != 'main')
{
	//$filter = " WHERE";
	if (($staff_position == 'director_tm') || ($staff_position == 'director_ozs')  || ($staff_position == 'secretary'))
	{
		//$date_filter = "office='$staff_office'";
		$filter .= " AND office='$staff_office'";
	}
	else
	{
		//$date_filter = "office='$staff_office' AND manager='$staff_id'";
		$filter .= " AND office='$staff_office' AND manager='$staff_id'";
	}
}
else
{
}

if (isset($_POST['source']) && ($_POST['source'] != 'all'))
{
	$source_s = $_POST['source'];
	switch($source_s)
	{
		case 'all' :
		case 'tel' :
		case 'api' :
		case 'agent' :
		case 'system' :
		case 'wantresult' :
		case 'iidx' :
			if ($filter == '')
				$filter .= " source='$source_s'";
			else
				$filter .= " AND source='$source_s'";
			break;
		default :
		{
			$source_m = preg_split("/;_;/", $source_s);
			if ($source_m[0] != 'site')
			{
				$res['status'] = 'failed';
				$res['msg'] = "Некорректный источник";
				$res['sql'] = $sql;
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				die();
			}
			$source_s = $source_m[1];
			if ($filter == '')
				$filter .= " source='site' AND source_param='$source_s'";
			else
				$filter .= " AND source='site' AND source_param='$source_s'";
			break;
		}
	}
	
}

if ($filter == '')
	$filter = "type='mini' OR call_event IS NOT NULL OR meet_event IS NOT NULL";
else
	$filter = "type='mini' AND (" . $filter . ")";

	$dw = (isset($_POST['dopwork']) && ($_POST['dopwork'] == '1')) ? true : false;
	$res['dw'] = $dw;

$res = array();

$filf = '';
$st = $_POST['s_status'];
if (($st == 'in_work') && !$dw)
{
	$sql = "SELECT formid FROM favorite_forms WHERE staffid='$staff_id';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$favorite = array();
			array_push($favorite, "0");
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				array_push($favorite, $row['formid']);
				
				if ($filf != '')
					$filf .= " OR";
				
				$filf .= " id=" .  $row['formid'];
			}
			
			$res['filf'] = $filf;
			$res['staff_id'] = $staff_id;
			
			$filter = "($filter) OR ($filf)";
				
			$res['favorite'] = $favorite;
		}
		$result->close();
	}
}



$form_last_action_time = array();

$data = array();
$miss_forms = array();
//if ($staff_id_debug == 1)
//$sql = "SELECT forms.id AS id, date_add, type, lastname, firstname, middlename, sum, sum_val, status, city, source, source_param, seo, office, manager, call_event, meet_event, last_msg_time, last_action_time, msgs_text, favorite_forms.id AS ffid FROM forms LEFT JOIN favorite_forms ON favorite_forms.formid = forms.id WHERE ($filter) OR favorite_forms.formid = forms.id ORDER BY date_add DESC;";
//else
$sql = "SELECT id, date_add, type, lastname, firstname, middlename, sum, sum_val, status, city, source, source_param, seo, zalog, office, manager, call_event, meet_event, last_msg_time, last_action_time, msgs_text, workplanst, main_form, sub_forms, wait_man, isexpress, contract_signed, contract_date, data, track FROM forms WHERE $filter ORDER BY date_add DESC;";

//echo $sql;
if ($result = $db_connect->query($sql))
{
	//$res = "Select вернул " . $result->num_rows . " строк.\n";
	
	$minform = 999999999999;
	$maxform = 0;
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$now = new DateTime;
		
		$yesterday = new DateTime;
		$yesterday = $yesterday->modify('-1 day');
		
		$otherDate = new DateTime($row['date_add']);
		$dsplit = preg_split("/ /", $row['date_add']);
		
		if ($now->format('Y-m-d') == $otherDate->format('Y-m-d'))
			$row['date_add'] = $dsplit[1];
		else if ($yesterday->format('Y-m-d') == $otherDate->format('Y-m-d'))
			$row['date_add'] = "Вчера " . $dsplit[1];
		else
			$row['date_add'] = date("d.m.y <b>H:i</b>",$otherDate->getTimestamp());
			
		$row['status_desc'] = $status_description[$row['status']];
		$row['officce_desc'] = ($row['office'] == 0) ? "" : $offices_mass[$row['office']];
		$row['manager_desc'] = ($row['manager'] == 0) ? "" : $staff_mass[$row['manager']];
		
		$nd = date("Y-m-d H:i:s");
		if (($row['call_event'] != null) && ($row['call_event'] < $nd))
			$row['call_event'] = '<font color="red">' . $row['call_event'] . '</font>';
		
		if (($row['meet_event'] != null) && ($row['meet_event'] < $nd))
			$row['meet_event'] = '<font color="red">' . $row['meet_event'] . '</font>';

        $url_img = '';
		if ($row['track'] != '') {
		    $tmp_track = explode('&', $row['track']);


            if (is_array($tmp_track) && count($tmp_track)) {
                foreach ($tmp_track as $tmp_tr) {
                    if ($tmp_tr == '')
                        continue;

                    $tmp_tr_m = explode('=', $tmp_tr);

                    if (is_array($tmp_tr_m) && count($tmp_tr_m)) {
                        if (isset($tmp_tr_m[0])) {
                            if ($tmp_tr_m[0] == 'cabinet') {
                                if (isset($tmp_tr_m[1]))
                                    $url_img = $tmp_tr_m[1];
                                break;
                            }
                        }
                    }
                }
            }
        }
		$row['url_img'] = $url_img;

		if ($row['source'] == 'site')
		{
			$row['seo'] = 0 + $row['seo'];
			$row['source'] = '';
			
			if (0 + $row['seo'] == 1)
			
			if (preg_match("/xn--p1ai/i", $row['source_param']))
				$row['source_param'] = idn_to_utf8($row['source_param']);
		
			if (strncmp($row['source_param'], 'xn--', 4) == 0)
				$row['source_param'] = idn_to_utf8($row['source_param']);
			
			/*if (strncmp($row['source_param'], 'xn--', 4) == 0)
				$row['source_param'] = idn_to_utf8($row['source_param']);
			else if (preg_match("/xn--p1ai/i", $row['source_param']))
				$row['source_param'] = idn_to_utf8($row['source_param']);*/
			
			
			
			$tmp_source = '';
			if (0 + $row['zalog'] == 1)
			{
				$tmp_source = "<img src=\"/img/icon.mortgage.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"Заявка с рекламы под залог\" title=\"Заявка с рекламы под залог\">";
			}

            if ($row['url_img'] != '') {
                $tmp_source .= "<img src=\"/img/$url_img.png\" style=\"height:22px; float:right; margin-right:12px; margin-top:4px;\" alt=\"С рекламы\" title=\"С рекламы\">";
            }
            else if (0 + $row['isexpress'] == 1)
			{
				$tmp_source .= "<img src=\"/img/direct-express.png\" style=\"height:22px; float:right; margin-right:12px; margin-top:4px;\" alt=\"С рекламы\" title=\"С рекламы\">";
			}
			else if (0 + $row['seo'] == 1)
				$tmp_source .= "<img src=\"/img/yandex_direct_logo_0.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"С рекламы\" title=\"С рекламы\">";
				//$row['source'] = "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']} (<font color=\"green\">Рек</font>)</a>";
				//$row['source'] = "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}<img src=\"/img/seo-icon.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"С рекламы\" title=\"С рекламы\"></a>";
				//$row['source'] .= "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}<img src=\"/img/yandex_direct_logo_0.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"С рекламы\" title=\"С рекламы\"></a>";
			else if (0 + $row['seo'] == 2)
				$tmp_source .= "<img src=\"/img/ga.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"С рекламы\" title=\"С рекламы\">";
				//$row['source'] .= "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}<img src=\"/img/ga.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"С рекламы\" title=\"С рекламы\"></a>";
			//else
				//$row['source'] .= "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}</a>";
            else if (0 + $row['seo'] == 3)
                $tmp_source .= "<img src=\"/img/social-icon.png\" style=\"height:25px; float:right; margin-right:10px; margin-top:2px;\" alt=\"Соцсети\" title=\"Соцсети\">";



            $row['source'] = "<a href=\"http://{$row['source_param']}\" target=\"_blank\">{$row['source_param']}$tmp_source</a>";
		}
		else if ($row['source'] == 'system')
		{
			if ($row['source_param'])
				$row['source'] = $staff_mass[$row['source_param']];
			else
				$row['source'] = '';
		}
		else if ($row['source'] == 'tel')
		{
			if ($row['source_param'])
				$row['source'] = "Тел. " . $row['source_param'];
			else
				$row['source'] = '';
		}
		else if ($row['source'] == 'agent')
		{
			if ($row['source_param'])
				$row['source'] = $agent_mass[$row['source_param']] . "(агент)";
			else
				$row['source'] = 'Анент';
		}
		else if ($row['source'] == 'api')
		{
			//print_r($row);
			$mmm = preg_split("/\|/", $row['source_param']);
			if (is_array($mmm) && (count($mmm) == 3))
				$row['source'] = '<span data-tooltip="id завки:' . $mmm[1] . '<br>id источника:' . $mmm[2] . '">' . $mmm[0] . "</span>";
		}
		
		$na = 0;
		
		
		if (($row['last_msg_time'] != null) && ($row['last_action_time'] != null))
		{
			$d1 = new DateTime($row['last_msg_time']);
			$d2 = new DateTime($row['last_action_time']);
			
			if ($d1 > $d2)
				$na = 1;
		
		}
		
		if ($row['last_action_time'] != null)
			$form_last_action_time[$row['id']] = array($row['last_action_time'], false);
		else
			$form_last_action_time[$row['id']] = array(false, false);

		$row['need_attention'] = $na;


        $row['is_party_deal'] = 'unknown';
		
		$data_mass = preg_split("/\<br\>/", $row['data']);
		$filed8_miss = true;
		
		foreach ($data_mass as $key => $val)
		{
			if ($val == "")
				continue;
			$value = preg_split("/::/", $val);
			if ($value[0] == 'field47')
			{
				$v = htmlspecialchars($value[1]);
				
				if ($v == 'Владелец бизнеса')
				{
					//print_r($val);
					$row['biz_owner'] = 1;
				}
				else if ($v == 'Индивидуальный предприниматель')
					$row['ip_owner'] = 1;
				else
					$row['other_work'] = 1;
			}
			
			if ($value[0] == 'field67')
			{
				$v = htmlspecialchars($value[1]);
				
				if ($v == 'Владелец бизнеса')
				{
					//print_r($val);
					$row['biz_owner2'] = 1;
				}
				else if (($v == 'Индивидуальный предприниматель') || ($v == 'ИП'))
					$row['ip_owner2'] = 1;
				else
					$row['other_work2'] = 1;
			}
			
			if ($value[0] == 'field8')
			{
				$filed8_miss = false;
				$v = htmlspecialchars($value[1]);
				$value8 = preg_split("/,/", $v);
				$v8_not = true;
				$v8_res = '';
				$v8_num = 0;
				foreach ($value8 as $k8 => $v8)
				{
					if (mb_strtolower($v8) != 'нет')
					{
						$v8_not = false;
						$v8_res = 'активы';
						$v8_num++;
					}
				}
				$v8_str = $v;
				
				if ($v8_not)
				{
					$v8_res = 'нет';
				}
				
				$row['filed8_val'] = $v8_res;
				$row['filed8_num'] = $v8_num;
				$row['filed8_str'] = $v8_str;
			}
			
			if (($row['manager'] + 0 == 0) || ($row['wait_man'] + 0 == 1))
			{
				$row['hide_sum'] = 1;
			}
			else
				$row['hide_sum'] = 0;

            if ($value[0] == 'field111') {
                $v = htmlspecialchars($value[1]);

                if (mb_strtolower($v) == 'анкета заполняется участником сделки')
                {
                    $row['is_party_deal'] = 'yes';
                }
                else
                    $row['is_party_deal'] = 'no';
            }
			
		}
		
		if ($filed8_miss)
		{
			$row['filed8_val'] = 'miss';
		}
		
		//$row['debug'] = $row['source_param'];
		unset ($row['source_param']);
		array_push($data, $row);
		
		if (0 + $row['id'] < $minform)
			$minform = 0 + $row['id'];
		
		if (0 + $row['id'] > $maxform)
			$maxform = 0 + $row['id'];
		
	}
	
	$result->close();
	
	

	
	$minform--;
	$maxform++;
	


	if (($st != 'in_work') && !$dw)
	{
		$res['fh'] = 1;
		$sql = "SELECT formid FROM favorite_forms WHERE formid>'$minform' AND formid<'$maxform' AND staffid='$staff_id';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$favorite = array();
				array_push($favorite, "0");
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
					array_push($favorite, $row['formid']);
				$res['favorite'] = $favorite;
			}
			$result->close();
		}
	}
	
	
	
	$res['status'] = 'OK';
	$res['data'] = $data;
	
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
}
$res['sql'] = $sql;

if ($staff_office_type == 'main')
{
	$bs = array();
	$sql = "SELECT status, form_id, bank_answer FROM bank_send WHERE form_id>'$minform' AND form_id<'$maxform';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				if (!isset($bs[$row['form_id']]))
				{
					$bs[$row['form_id']] = array('all' => 0, 'sent' => 0, 'approve' => 0, 'reject' => 0);
				}
				$bs[$row['form_id']]['all']++;
				
				if ($row['status'] == 'sent')
					$bs[$row['form_id']]['sent']++;
				if ($row['status'] == 'get_answer')
				{
					if ($row['bank_answer'] == 'approve')
						$bs[$row['form_id']]['approve']++;
					if ($row['bank_answer'] == 'reject')
						$bs[$row['form_id']]['reject']++;
					if ($row['bank_answer'] == 'techReject')
						$bs[$row['form_id']]['reject']++;
				}
			}
			$res['bsm'] = $bs;
		}
		$result->close();
	}
}

if (($staff_office_type == 'main') || ($staff_position == 'director_tm') || ($staff_position == 'director_ozs'))
{
	
	$source_m = array();
	if ($date_filter == '')
		$sql = "SELECT source, source_param FROM forms WHERE type='mini' GROUP BY source, source_param;";
	else
		$sql = "SELECT source, source_param FROM forms WHERE type='mini' AND ($date_filter) GROUP BY source, source_param;";
	$hit_tel = false;
	$hit_agents = false;
	$hit_api = false;
	$hit_wr = false;
	$hit_iidx = false;
	$hit_sys = false;
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				if ($row['source'] == '')
					continue;

				
				switch($row['source'])
				{
					case "tel" :
						if ($hit_tel)
							break;
						$hit_tel = true;
						if (!array_search('tel', $source_m))
							$source_m[] = 'tel';
						break;
					case "agent" :
						if ($hit_agents)
							break;
						$hit_agents = true;
						if (!array_search('agent', $source_m))
							$source_m[] = 'agent';
						break;
					case "wantresult" :
						if ($hit_wr)
							break;
						$hit_wr = true;
						if (!array_search('wantresult', $source_m))
							$source_m[] = 'wantresult';
						break;
					case "iidx" :
						if ($hit_iidx)
							break;
						$hit_iidx = true;
						if (!array_search('iidx', $source_m))
							$source_m[] = 'iidx';
						break;
					case "api" :
					if ($hit_api)
							break;
						$hit_api = true;
					if (!array_search('api', $source_m))
						$source_m[] = 'api';
						break;
					case "system" :
					if ($hit_sys)
							break;
						$hit_sys = true;
					if (!array_search('system', $source_m))
						$source_m[] = 'system';
						break;
					default :
										
						if ($row['source_param'] == '') 
							continue;
						$source_param_ = $row['source_param'];
						if (preg_match("/xn--p1ai/i", $row['source_param']))
							$source_param_ = idn_to_utf8($row['source_param']);
					
						if (strncmp($row['source_param'], 'xn--', 4) == 0)
							$source_param_ = idn_to_utf8($row['source_param']);
						$source_m[] = $row['source'] . ';_;' . $row['source_param'] . ';_;' . $source_param_;
						break;
				}
			}
			$res['source_m'] = $source_m;
		}
		$result->close();
	}
}

$fico_mass = array();
$sql = "SELECT value, answer, formid FROM idx_log WHERE type='finScoring' AND formid>'$minform' AND formid<'$maxform' AND status='SUCCESS' ORDER BY id DESC;";
if ($result = $db_connect->query($sql))
{
	if ($result->num_rows)
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$fico_form_id = $row['formid'];
			
			if (isset($fico_mass[$fico_form_id]))
				continue;
			
			
			$answer = json_decode($row['answer']);
			
			if (0 + $answer->resultCode != 0)
				continue;
			
			
			//print_r($row);
			$dt = 'Результат запроса:';
			if (isset($answer->score))
				$dt .= '<br> Балл:' . 			$answer->score;
			$dt .= '<br> Сообщение:' . 		$answer->resultMessage;
			$dt .= '<br> Подробности:';
			if (isset($answer->reasonCode0Desc))
				$dt .= '<br> -' . $answer->reasonCode0Desc;
			if (isset($answer->reasonCode1Desc))
				$dt .= '<br> -' . $answer->reasonCode1Desc;

			if (isset($answer->reasonCode2Desc))
				$dt .= '<br> -' . $answer->reasonCode2Desc;

			if (isset($answer->reasonCode3Desc))
				$dt .= '<br> -' . $answer->reasonCode3Desc;

			if (isset($answer->reasonCode4Desc))
				$dt .= '<br> -' . $answer->reasonCode4Desc;
			if (isset($answer->reasonCode5Desc))
				$dt .= '<br> -' . $answer->reasonCode5Desc;
			if (isset($answer->reasonCode6Desc))
				$dt .= '<br> -' . $answer->reasonCode6Desc;

			if ($staff_office_type == 'tm')
				$fico_mass[$fico_form_id] = array('val' => '<font color="green">Есть</font>', 'desc' => '');
			else
				$fico_mass[$fico_form_id] = array('val' => $row['value'], 'desc' => $dt);
		}
		$res['fico_mass'] = $fico_mass;
	}
	$result->close();
}

//if ($staff_id == 1)
if (true)
{
	$call_mass = array();
	$result_mass = array();
	$sql = "SELECT form_id, init_time_gmt FROM calls_history WHERE flow='in' AND (result='not answered' OR (result='answered' AND hangup_disposition='caller_bye')) AND form_id>'$minform' AND form_id<'$maxform' ORDER BY id DESC;";
	//date_default_timezone_set('Europe/Moscow');
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				//$date = new DateTime($row['hangup_time_gmt']);
				$date = date("Y-m-d H:i:s", strtotime($row['init_time_gmt']) + 3600*3);
				
				if (isset($form_last_action_time[$row['form_id']]))
				{
					if ($form_last_action_time[$row['form_id']][0] === false)
						$form_last_action_time[$row['form_id']][1] = true;
					else
					{
						if (strtotime($form_last_action_time[$row['form_id']][0]) < strtotime($date))
							$form_last_action_time[$row['form_id']][1] = true;
					}
					//$row['last_action_time'] = $form_last_action_time[$row['form_id']];
				}
				$call_mass[] = $row;
			}
			$res['call_mass'] = $call_mass;
		}
	}
	$result->close();
	/*foreach ($form_last_action_time as $k=>$v)
	{
		$date = new DateTime($v);
		$date->modify("-3 hour");
		//$v = gmdate("Y-m-d H:i:s", $date->getTimestamp());
		$form_last_action_time[$k] = $date;
	}*/
	$res['form_last_action_time'] = $form_last_action_time;
}

print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
?>