<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$monthArray = [
	'01' => 'Январь',
	'02' => 'Февраль',
	'03' => 'Март',
	'04' => 'Апрель',
	'05' => 'Май',
	'06' => 'Июнь',
	'07' => 'Июль',
	'08' => 'Август',
	'09' => 'Сентябрь',
	'10' => 'Октябрь',
	'11' => 'Ноябрь',
	'12' => 'Декабрь'
];

udpate_staff_onilne($staff_id_debug);

$error = false;
$sc = isset($_GET['action']) ? $_GET['action'] : $_POST['action'];

$df = isset($_GET['df']) ? $_GET['df'] : $_POST['df'];
$dto = isset($_GET['dto']) ? $_GET['dto'] : $_POST['dto'];

$df = strftime("%Y-%m-%d 00:00:00", strtotime($df));
$dfm = strftime("%m", strtotime($df));
$dfY = strftime("%Y", strtotime($df));
$dfmY = strftime("%Y-%m", strtotime($df));
$dto = strftime("%Y-%m-%d 23:59:59", strtotime($dto));
$res['df'] = $df;
$res['dto'] = $dto;
switch($sc)
{
	case 'getdata':
	{
		$resultArray = array();
		$resultArrayC = array();
		$res['status'] = 'ok';
		$decodenames = [
            'site' => 'С сайта',
            'men' => 'Менежер',
            'tel' => 'Телефон',
            'api' => 'API',
            'want' => 'Wantresult',
            'sum' => 'Всего',
            'dsite' => 'Договоров с сайта',
            'dmen' => 'Договоро от менеджера',
            'dtel' => 'Договоров с телефона',
            'dapi' => 'Договоров с API',
            'dwant' => 'Договоров с Wantresult',
            'dsum' => 'Договоров всего',
            'ksite' => 'Кредитов с сайта',
            'kmen' => 'Кредитов от менеджеров',
            'ktel' => 'Кредитов с телефонов',
            'kapi' => 'Кредитов с API',
            'kwant' => 'Кредитов с Wantresult',
            'ksum' => 'Кредитов всего'
        ];
		$mass = [
			'new' => 0,
			'site' => 0,
			'men' => 0,
			'tel' => 0,
			'api' => 0,
			'want' => 0,
			'sum' => 0,
			'dsite' => 0,
			'dmen' => 0,
			'dtel' => 0,
			'dapi' => 0,
			'dwant' => 0,
			'dsum' => 0,
			'ksite' => 0,
			'kmen' => 0,
			'ktel' => 0,
			'kapi' => 0,
			'kwant' => 0,
			'ksum' => 0
		];
		$error = false;
        $resultArrayC[] = 'name';
		$sql = "SELECT id, date_add, source FROM forms WHERE main_form IS NULL AND (date_add BETWEEN '$df' AND '$dto') ORDER BY id;";
		//$sql = "SELECT id, date_add, contract_signed, contract_date FROM forms WHERE main_form IS NULL AND (date_add BETWEEN '$df' AND '$dto') ORDER BY id;";
		if ($result = $db_connect->query($sql))
		{

			$lastCheckedDate = $dfmY;
			$lastCheckedDateStr = $monthArray[$dfm] . " $dfY";
			//$mass['name'] = $lastCheckedDateStr;
			//$resultArray[$lastCheckedDateStr] = $mass;
			//$resultArrayC[] = $lastCheckedDateStr;
            $resultArray = array(
                'sum' => array(),
                'site' => array(),
                'men' => array(),
                'tel' => array(),
                'api' => array(),
                'want' => array(),
                'dsum' => array(),
                'dsite' => array(),
                'dmen' => array(),
                'dtel' => array(),
                'dapi' => array(),
                'dwant' => array(),
                'ksum' => array(),
                'ksite' => array(),
                'kmen' => array(),
                'ktel' => array(),
                'kapi' => array(),
                'kwant' => array()
            );
			
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$dfmY_ = strftime("%Y-%m", strtotime($val['date_add']));
				$dfm_ = strftime("%m", strtotime($val['date_add']));
				$dfY_ = strftime("%Y", strtotime($val['date_add']));
				$lastCheckedDateStr = $monthArray[$dfm_] . " $dfY_";
				if (!array_key_exists($lastCheckedDateStr, $resultArray['sum'])) {
					//$mass['name'] = $lastCheckedDateStr;
					//$resultArray[$lastCheckedDateStr] = $mass;
                    foreach ($resultArray as $k => $v)
                        $resultArray[$k][$lastCheckedDateStr] = 0;
					$resultArrayC[] = $lastCheckedDateStr;
				}

				$resultArray['sum'][$lastCheckedDateStr]++;

				if ($val['source'] == 'site')
					$resultArray['site'][$lastCheckedDateStr]++;
				if ($val['source'] == 'system')
					$resultArray['men'][$lastCheckedDateStr]++;
				if ($val['source'] == 'tel')
					$resultArray['tel'][$lastCheckedDateStr]++;
				if ($val['source'] == 'api')
					$resultArray['api'][$lastCheckedDateStr]++;
				if ($val['source'] == 'wantresult')
					$resultArray['want'][$lastCheckedDateStr]++;
			}
			
			//$res['data'] = $data;
			//$res['resultArrayC'] = $resultArrayC;
			//$res['resultArray'] = $resultArray;
            $res['sql1'] = $sql;
			$result->close();
		}
		else
			$error = true;

		$sql = "SELECT id, contract_date, source FROM forms WHERE contract_signed = '1' AND (contract_date BETWEEN '$df' AND '$dto') ORDER BY id;";
		//$sql = "SELECT id, contract_date, source FROM forms WHERE main_form IS NULL AND contract_signed = '1' AND (contract_date BETWEEN '$df' AND '$dto') ORDER BY id;";
		//$sql = "SELECT id, date_add, contract_signed, contract_date FROM forms WHERE main_form IS NULL AND (date_add BETWEEN '$df' AND '$dto') ORDER BY id;";
		if ($result = $db_connect->query($sql))
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$dfmY_ = strftime("%Y-%m", strtotime($val['contract_date']));
				$dfm_ = strftime("%m", strtotime($val['contract_date']));
				$dfY_ = strftime("%Y", strtotime($val['contract_date']));
				$lastCheckedDateStr = $monthArray[$dfm_] . " $dfY_";
				if (!array_key_exists($lastCheckedDateStr, $resultArray['sum'])) {
					//$resultArray[$lastCheckedDateStr] = $mass;
					$resultArrayC[] = $lastCheckedDateStr;
				}


                $resultArray['dsum'][$lastCheckedDateStr]++;

                if ($val['source'] == 'site')
                    $resultArray['dsite'][$lastCheckedDateStr]++;
                if ($val['source'] == 'system')
                    $resultArray['dmen'][$lastCheckedDateStr]++;
                if ($val['source'] == 'tel')
                    $resultArray['dtel'][$lastCheckedDateStr]++;
                if ($val['source'] == 'api')
                    $resultArray['dapi'][$lastCheckedDateStr]++;
                if ($val['source'] == 'wantresult')
                    $resultArray['dwant'][$lastCheckedDateStr]++;
			}
			
			$res['sql2'] = $sql;
			$result->close();
		}
		else
			$error = true;

        $sql = "SELECT form_id, comFactDate, source FROM bank_send LEFT JOIN forms ON form_id=forms.id WHERE com_fact IS NOT NULL AND bank_answer='approve' AND (comFactDate BETWEEN '$df' AND '$dto') ORDER BY bank_send.id;";
        if ($result = $db_connect->query($sql))
        {
            while($val = $result->fetch_array(MYSQLI_ASSOC))
            {
                $dfmY_ = strftime("%Y-%m", strtotime($val['comFactDate']));
                $dfm_ = strftime("%m", strtotime($val['comFactDate']));
                $dfY_ = strftime("%Y", strtotime($val['comFactDate']));
                $lastCheckedDateStr = $monthArray[$dfm_] . " $dfY_";
                if (!array_key_exists($lastCheckedDateStr, $resultArray['sum'])) {
                    $resultArrayC[] = $lastCheckedDateStr;
                }

                $resultArray['ksum'][$lastCheckedDateStr]++;

                if ($val['source'] == 'site')
                    $resultArray['ksite'][$lastCheckedDateStr]++;
                if ($val['source'] == 'system')
                    $resultArray['kmen'][$lastCheckedDateStr]++;
                if ($val['source'] == 'tel')
                    $resultArray['ktel'][$lastCheckedDateStr]++;
                if ($val['source'] == 'api')
                    $resultArray['kapi'][$lastCheckedDateStr]++;
                if ($val['source'] == 'wantresult')
                    $resultArray['kwant'][$lastCheckedDateStr]++;
            }

            //$res['data'] = $data;
            $res['sql3'] = $sql;
            $result->close();
        }
        else
            $error = true;

		if ($error)
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			//print_r($res);
		}
		else {
			$res['resultArrayC'] = $resultArrayC;
			//$res['resultArray'] = $resultArray;
			$data = array();
			foreach ($resultArray as $k => $v) {
				$data[] = array_merge(array('name' => $decodenames[$k]), $v);
			}
			$res['resultArray'] = $data;
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	default:
	{
		$res['status'] = 'failed';
		$res['msg'] = "Нет такого метода";
		break;
	}
}
print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
?>