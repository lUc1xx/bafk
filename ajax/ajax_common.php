<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$staff_office_type = '';
$error = false;
if (!$error)
{
	if ($result = $db_connect->query("SELECT type FROM offices WHERE id='$staff_office';"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$staff_office_type = $row['type'];
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}

$res = array();

switch($_POST['action'])
{
	case 'get_form_ids_in_work':
	{
		
		/*$have_events = "call_event IS NOT NULL OR meet_event IS NOT NULL";

		$filter = "status!='new' AND status!='work_complete'";
		$filter = "(($filter) OR call_event IS NOT NULL OR meet_event IS NOT NULL)";

		$filter .= " AND office='$staff_office' AND manager='$staff_id'";
	
		if ($filter == '')
			$filter = " WHERE type='mini' OR call_event IS NOT NULL OR meet_event IS NOT NULL";
		else
			$filter = " WHERE type='mini' AND (" . $filter . ")";
*/
        $filter = " WHERE office='$staff_office' AND manager='$staff_id' AND type='mini'";
        $filter .= " AND ((status!='new' AND status!='work_complete') OR call_event IS NOT NULL OR meet_event IS NOT NULL)";
        $filter .= " ORDER BY office, manager ASC ";

		$sql = "SELECT id FROM forms$filter;";

//		$res['status'] = 'ok';
//		$res['sql'] = $sql;

		
		//$sql = "SELECT id FROM forms WHERE type='mini' AND (((status!='new' AND status!='work_complete') OR call_event IS NOT NULL OR meet_event IS NOT NULL) AND office='2' AND manager='14');";
		if ($result = $db_connect->query($sql))
		{
			$data = array();
			$len = 0;
			if ($result->num_rows)
			{
				while($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$len++;
					$data[] = $row['id'];
				}
			}
			
			$res['data'] = $data;
			$res['len'] = $len;
			$res['status'] = 'ok';
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();

	}
	case 'get_forms_c':
	{
		if ($staff_office_type == 'main')
			$off_filter = " AND (ozs_man IS NOT NULL OR tm_man IS NOT NULL)";
		else if ($staff_office_type == 'ozs')
			$off_filter = " AND ozs_man='$staff_id'";
		else if ($staff_office_type == 'tm')
			$off_filter = " AND tm_man='$staff_id'";
		$list_forms = array();
		$sql = "SELECT status, count(id) AS cforms FROM forms WHERE type='mini' AND (status='primary_work' OR status='secondary_work' OR status='assigned_meeting' OR status='contract_signed')$off_filter GROUP BY status ORDER BY cforms ASC;";
		//$res['sql'] = $sql;
		if ($result = $db_connect->query($sql))
		{
			$res['status'] = 'ok';
			if ($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$list_forms[$row['status']] = $row['cforms'];
				}
				//$res['data2'] = $list_forms;
			}
			$result->close();
		}
		else
		{
			//$res['sql'] = $sql;
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		$sql = "SELECT status, call_event FROM forms WHERE type='mini' AND (status='primary_work'$off_filter OR status='in_tm_office');";
		$res['sql'] = $sql;
		if ($result = $db_connect->query($sql))
		{
			$res['status'] = 'ok';
			if ($result->num_rows)
			{
				$ncall = 0;
				$in_tm = 0;
				$nd = date("Y-m-d H:i:s");
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					if ((($row['call_event'] == null) || ($row['call_event'] < $nd)) && ($row['status']=='primary_work'))
						$ncall++;
					
					if ($row['status']=='in_tm_office')
						$in_tm++;
				}
				$list_forms['ncall'] = $ncall;
				$list_forms['in_tm'] = $in_tm;
				
			}
			$result->close();
		}
		else
		{
			//$res['sql'] = $sql;
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		$res['data'] = $list_forms;
		
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	case "search_forms" :
	{
		$phone_list = array();
		if ($result = $db_connect->query("SELECT name, type FROM form_fields_settings WHERE type='telephone';"))
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$phone_list[] = $val['name'];
			}
			
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		$seach_str = '';
		$where_date = '';
		$where_tel1 = '';
		$where_tel2 = '';
		foreach($_POST as $k=>$v)
		{
			if ($v == '')
				continue;
			
			switch($k)
			{
				case 'firstname':
				{
					if ($seach_str != '')
						$seach_str .= ' AND ';
					$seach_str .= "firstname LIKE '%$v%'";
					break;
				}
				case 'middlename':
				{
					if ($seach_str != '')
						$seach_str .= ' AND ';
					$seach_str .= "middlename LIKE '%$v%'";
					break;
				}
				case 'lastname':
				{
					if ($seach_str != '')
						$seach_str .= ' AND ';
					$seach_str .= "lastname LIKE '%$v%'";
					break;
				}
				case 'birth_day':
				{
					$date_m = preg_split('/[\.\:\/]/', $v);
					if ($date_m[0] + 0 < 10)
						$db = "0*" . ($date_m[0]+0);
					else						
						$db = $date_m[0];
					
					if ($date_m[1] + 0 < 10)
						$dm = "0*" . ($date_m[1]+0);
					else						
						$dm = $date_m[1];
					
					if ($date_m[2] + 0 < 2000)
						$dy = "[01234567890]*" . ($date_m[2] - 1900);
					else						
						$dy = $date_m[2];
					
					if ($seach_str != '')
						$seach_str .= ' AND ';

					$seach_str = "data RLIKE 'birth_day::$db.$dm.$dy'";
					break;
				}
				case 'field49':
				{
					if ($seach_str != '')
						$seach_str .= ' AND ';
					$seach_str .= "data LIKE '%field49::%$v%'";
					break;
				}
				case 'field29':
				{
					$tel = preg_replace("/[^0-9]/", '', $v);
					if (strlen($tel) == 11)
					{
						$tel = substr($tel, 1);
						foreach ($phone_list as $pval)
						{
							if ($where_tel1)
								$where_tel1 .= ' OR ';
							$where_tel1 .= "data RLIKE '$pval::[78]*$tel'";
						}
						continue;
					}
					break;
				}
				case 'field60':
				{
					$tel = preg_replace("/[^0-9]/", '', $v);
					if (strlen($tel) == 11)
					{
						$tel = substr($tel, 1);
						foreach ($phone_list as $pval)
						{
							if ($where_tel2)
								$where_tel2 .= ' OR ';
							
							$where_tel2 .= "data RLIKE '$pval::[78]*$tel'";
						}
						continue;
					}
					break;
				}
				
				default: break;
			}
		}
		
		$where_tel = '';
		
		if (($where_tel1 != '') && ($where_tel2 != ''))
			$where_tel = "($where_tel1) AND ($where_tel2)";
		else if ($where_tel1 != '')
			$where_tel = "($where_tel1)";
		else if ($where_tel2 != '')
			$where_tel = "($where_tel2)";
		
		if (($seach_str != '') && ($where_tel != ''))
			$where_tel = " AND " . $where_tel;
		
		
		$data = array();
		$sql = "SELECT id, lastname, firstname, middlename FROM forms WHERE $seach_str$where_tel ORDER BY id DESC LIMIT 25;";
		if ($result = $db_connect->query($sql))
		{
			$res['status'] = 'ok';
			if ($result->num_rows)
			{
				$res['len'] = $result->num_rows;
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$data[] = $row;
				}
			}
			$result->close();
		}
		else
		{
			$res['sql'] = $sql;
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		$res['status'] = 'ok';
		$res['data'] = $data;
		$res['sql'] = $sql;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	
	default:
	{
		
	}
}
?>