<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

udpate_staff_onilne($staff_id_debug);

if ($_POST['action'] == 'get_points_list')
{
	$sql = "SELECT * FROM points_list WHERE deleted='0';";
	$points = array();
	$len = 0;
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$points[$val['id']] = $val;
				$len++;
			}
				
			$result->close();
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	$res['len'] = $len;
	$res['points_list'] = $points;
	
	
	$sql = "SELECT id, name FROM old_banks WHERE prb='1' AND deleted='0' ORDER BY name;";
	$banks = array();
	$len = 0;
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				$banks[$len] = array('id' => $val['id'], 'name' => $val['name']);
				$len++;
			}
				
			$result->close();
		}
		else
		{
			$result->close();
			$res['status'] = 'failed';
			//$res['msg'] = "Не найден банк " . $bank;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}

	$res['bank_list'] = $banks;

	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'add_point')
{
	$name 			= $_POST["name"];
	$bank 			= $_POST["bank"];
	$comm 			= $_POST["comm"];
	$com_type 		= $_POST["com_type"];
	$type 			= $_POST["type"];
	$analyst 		= $_POST["analyst"];
	$analyst_com 	= 0 + $_POST["add_comm_a"];
	$analyst_ct 	= $_POST["add_com_type_a"];
	$analyst_type 	= $_POST["add_type_a"];
	$comment 		= $_POST["comment"];
	
	if ($name == '')
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не заполнено название";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($comm == '')
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не заполнена комиссия";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($com_type == '-1')
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не выбран тип комиссии";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($type == '-1')
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не выбран тип";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($bank == '-1')
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не выбран тип";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$sql = "INSERT INTO points_list(name, bank, commission, com_type, type, analyst, analyst_com, analyst_ct, analyst_type, comment) VALUES('$name', '$bank', '$comm', '$com_type', '$type', '$analyst', '$analyst_com', '$analyst_ct', '$analyst_type', '$comment');";
	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_point')
{
	$id = 0 + $_POST['id'];
	$name = $_POST['name'];
	$val = $_POST['val'];
	$sql = "UPDATE points_list SET $name='$val' WHERE id='$id';";
	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'del_point')
{
	$id = 0 + $_POST['id'];
	$sql = "UPDATE points_list SET deleted='1' WHERE id='$id';";
	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'get_bank_list')
{
	$sql = "SELECT * FROM old_banks ORDER BY name ASC;";
	$banks = array();
	$len = 0;
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{
				array_push($banks, array('id' => $val['id'], 'name' => $val['name'], 'adata' => $val['adata'], 'prb' => $val['prb'], 'deleted' => $val['deleted']));
				$len++;
			}
				
			$result->close();
		}
		else
		{
			$result->close();
			$res['status'] = 'failed';
			//$res['msg'] = "Не найден банк " . $bank;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	$res['len'] = $len;
	$res['bank_list'] = $banks;

	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'ch_bank')
{
	$id = 0 + $_POST['id'];
	$name = $_POST['name'];
	$val = 0 + $_POST['val'];
	$sql = "UPDATE old_banks SET $name='$val' WHERE id='$id';";

	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'save_bank')
{
	$id = 0 + $_POST['id'];
	$name = $_POST['val'];
	$sql = "UPDATE old_banks SET name='$name' WHERE id='$id';";

	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'del_bank')
{
	$id = 0 + $_POST['id'];
	$sql = "UPDATE old_banks SET deleted=1 WHERE id='$id';";

	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'restore_bank')
{
	$id = 0 + $_POST['id'];
	$sql = "UPDATE old_banks SET deleted=0 WHERE id='$id';";

	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'add_new_bank')
{
	$new_bank_name = $_POST['new_bank_name'];
	$new_bank_adata = 0 + $_POST['new_bank_adata'];
	$new_bank_prb = 0 + $_POST['new_bank_prb'];
	$sql = "INSERT INTO old_banks(name, adata, prb) VALUES('$new_bank_name', '$new_bank_adata', '$new_bank_prb');";

	if (!($result = $db_connect->query($sql)))
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

$res['status'] = 'failed';
$res['msg'] = "Не найдена задача " . $_POST['action'];
print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
die();
?>