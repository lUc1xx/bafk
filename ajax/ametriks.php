<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

udpate_staff_onilne($staff_id_debug);

$error = false;
$sc = isset($_GET['action']) ? $_GET['action'] : $_POST['action'];

$df = isset($_GET['df']) ? $_GET['df'] : $_POST['df'];
$dto = isset($_GET['dto']) ? $_GET['dto'] : $_POST['dto'];

$df = strftime("%Y-%m-%d 00:00:00", strtotime($df));
$dto = strftime("%Y-%m-%d 23:59:59", strtotime($dto));
$res['df'] = $df;
$res['dto'] = $dto;
$listForms = array();
switch($sc)
{
	case 'getmetriks':
	{
		$massForms = array();
		$massSubForms = array();
		$mass = array();
		//$sql = "SELECT forms.id as fid, forms.status as status, forms.last_status as lstatus, forms.date_add as da, contract_signed, track, on_arm, com_fact FROM forms LEFT JOIN bank_send ON forms.id=bank_send.form_id WHERE (date_add BETWEEN '$df' AND '$dto') ORDER BY fid;";
		$sql = "SELECT forms.id as fid, close_reasons, forms.status as status, forms.last_status as lstatus, forms.date_add as da, forms.contract_signed, forms.track, on_arm, com_fact, forms.main_form, sub_forms FROM forms LEFT JOIN bank_send ON forms.id=bank_send.form_id WHERE forms.status != 'deleted' AND type='mini' AND main_form IS NULL AND (forms.date_add BETWEEN '$df' AND '$dto') ORDER BY fid;";
		if ($result = $db_connect->query($sql)) {
            while ($val = $result->fetch_array(MYSQLI_ASSOC)) {

                $massForms[] = $val;
                $subForm = $val['sub_forms'];
                $subFormMass = explode(';', $subForm);
                foreach ($subFormMass as $sf) {
                    if($sf == '')
                        continue;
                    $massSubForms[] = $sf;
                }
            }
            $result->close();
        }
        else
        {
            $res['status'] = 'failed';
            $res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
            //print_r($res);
        }
        $res['sql1'] = $sql;

        if (count($massSubForms)) {
            $listFormsSting = implode("','", $massSubForms);

            $sql = "SELECT f.id AS fid, f.close_reasons, f.status as status, f.last_status as lstatus, f.date_add as da, f.contract_signed, f2.track, on_arm, com_fact, sum_answer, f.main_form";
            $sql .= ", f2.last_status AS f2lst, f2.status AS f2status ";
            $sql .= "FROM forms as f ";
            $sql .= "LEFT JOIN bank_send ON f.id=bank_send.form_id ";
            $sql .= "LEFT JOIN forms AS f2 ON f2.id=f.main_form ";
            $sql .= "WHERE f.id IN ('$listFormsSting');";
            if ($result = $db_connect->query($sql)) {
                while ($val = $result->fetch_array(MYSQLI_ASSOC)) {

                    $massForms[] = $val;
                }
                $result->close();
            }
            else
            {
                $res['status'] = 'failed';
                $res['sql'] = $sql;
                $res['msg2'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
                //print_r($res);
            }
        }
        $res['sql2'] = $sql;


        foreach ($massForms as $val) {
            $track = $val['track'];
            if ($track == '' || $track == null) {
                $track = 'нет метки';
                $hit = true;
            }
            else {
                $trackm = preg_split("/\&/", $track);
                $hit = false;
                foreach($trackm as $v) {
                    $track = preg_split("/\=/", $v);
                    if ($track[0] == "utm_source") {
                        $track = $track[1];
                        $hit = true;
                        break;
                    }
                }
            }

            $track = trim($track);

            if (!$hit)
                $track = 'нет метки';
            //continue;
            if (in_array($val['fid'], $listForms)) {

                if ($val['com_fact'] + 0) {
                    $mass[$track]['kredit']++;
                    $mass[$track]['comm'] += $val['com_fact'];
                    if ($val['on_arm'] + 0) {
                        $mass[$track]['sum'] += $val['on_arm'];
                    }
                    else if ($val['on_arm'] + 0) {
                        $mass[$track]['sum'] += $val['sum_answer'];
                    }
                }
                continue;
            }

            $listForms[] = $val['fid'];

            if (!isset($mass[$track])) {

                $mass[$track] = ['num' => 0, 'second' => 0, 'dogovor' => 0, 'kredit' => 0, 'sum' => 0, 'comm' => 0, 'k1' => 0, 'k2' => 0, 'k3' => 0, 'cm_24' => 0];
            }

            $mass[$track]['num']++;

            $hit = false;
            if(isset($val['f2status']) && isset($val['f2lst'])) {
                if (($val['f2lst'] != null && $status_num_descr[$val['f2lst']] > 3) || ($status_num_descr[$val['f2status']] > 3 && $status_num_descr[$val['f2status']] < 17)) {
                    $hit = true;
                }
            }

            if (!$hit) {
                if (($val['lstatus'] != null && $status_num_descr[$val['lstatus']] > 3) || ($status_num_descr[$val['status']] > 3 && $status_num_descr[$val['status']] < 17))  {
                    $mass[$track]['second']++;
                }
            }


            if ($val['contract_signed'] + 0)
                $mass[$track]['dogovor']++;

            if ($val['com_fact'] + 0) {
                $mass[$track]['kredit']++;
                $mass[$track]['comm'] += $val['com_fact'];
                if ($val['on_arm'] + 0) {
                    $mass[$track]['sum'] += $val['on_arm'];
                }
                else if ($val['on_arm'] + 0) {
                    $mass[$track]['sum'] += $val['sum_answer'];
                }
            }

            if ($val['status'] == 'work_complete') {
                if (preg_match("/cm_24/", $val['close_reasons'])) {
                    $mass[$track]['cm_24']++;
                }
            }

            $mass[$track]['k1'] = round(100 * $mass[$track]['second'] / $mass[$track]['num']);
            $mass[$track]['k2'] = round(100 * $mass[$track]['dogovor'] / $mass[$track]['num']);
            $mass[$track]['k3'] = round(100 * $mass[$track]['kredit'] / $mass[$track]['num']);

            $mass[$track]['fid'][] = $val['fid'];

        }

        $res['status'] = 'ok';
        $data = array();
        foreach ($mass as $k => $v) {
            $data[] = array_merge(array('track' => $k), $v);
        }

        $res['data'] = $data;

		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	case 'getmetriks_old':
	{
		$mass = array();
		//$sql = "SELECT forms.id as fid, forms.status as status, forms.last_status as lstatus, forms.date_add as da, contract_signed, track, on_arm, com_fact FROM forms LEFT JOIN bank_send ON forms.id=bank_send.form_id WHERE (date_add BETWEEN '$df' AND '$dto') ORDER BY fid;";
		$sql = "SELECT forms.id as fid, forms.status as status, forms.last_status as lstatus, forms.date_add as da, forms.contract_signed, forms.track, on_arm, com_fact, forms.main_form, f2.track as track2 FROM forms LEFT JOIN bank_send ON forms.id=bank_send.form_id left join forms as f2 on f2.id = forms.main_form WHERE (forms.date_add BETWEEN '$df' AND '$dto') ORDER BY fid;";
		if ($result = $db_connect->query($sql))
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			{

				$track = $val['track'];
				$track2 = $val['track2'];
                if ($track2 != '' && $track2 != null) {
                    $trackm = preg_split("/\&/", $track2);
                    $hit = false;
                    foreach($trackm as $v) {
                        $track = preg_split("/\=/", $v);
                        if ($track[0] == "utm_source") {
                            $track = $track[1];
                            $hit = true;
                            break;
                        }
                    }
                }
                else if ($track == '' || $track == null) {
                    $track = 'нет метки';
                    $hit = true;
                }
                else {
                    $trackm = preg_split("/\&/", $track);
                    $hit = false;
                    foreach($trackm as $v) {
                        $track = preg_split("/\=/", $v);
                        if ($track[0] == "utm_source") {
                            $track = $track[1];
                            $hit = true;
                            break;
                        }
                    }
                }

                $track = trim($track);

				if (!$hit)
                    $track = 'нет метки';
					//continue;
                if (in_array($val['fid'], $listForms)) {

                    if ($val['com_fact'] + 0) {
                        $mass[$track]['kredit']++;
                        $mass[$track]['comm'] += $val['com_fact'];
                        if ($val['on_arm'] + 0) {
                            $mass[$track]['sum'] += $val['on_arm'];
                        }
                    }
                    continue;
                }

                $listForms[] = $val['fid'];

				if (!isset($mass[$track])) {

					$mass[$track] = ['num' => 0, 'second' => 0, 'dogovor' => 0, 'kredit' => 0, 'sum' => 0, 'comm' => 0, 'k1' => 0, 'k2' => 0, 'k3' => 0];
				}

				$mass[$track]['num']++;

				if (($val['lstatus'] != null && $status_num_descr[$val['lstatus']] > 3) || ($status_num_descr[$val['status']] > 3 && $status_num_descr[$val['status']] < 17))  {
                    $mass[$track]['second']++;
                }

				if ($val['contract_signed'] + 0)
					$mass[$track]['dogovor']++;

				if ($val['com_fact'] + 0) {
					$mass[$track]['kredit']++;
					$mass[$track]['comm'] += $val['com_fact'];
					if ($val['on_arm'] + 0) {
						$mass[$track]['sum'] += $val['on_arm'];
					}
				}

				$mass[$track]['k1'] = round(100 * $mass[$track]['second'] / $mass[$track]['num']);
				$mass[$track]['k2'] = round(100 * $mass[$track]['dogovor'] / $mass[$track]['num']);
				$mass[$track]['k3'] = round(100 * $mass[$track]['kredit'] / $mass[$track]['num']);

                $mass[$track]['fid'][] = $val['fid'];

//				$data[] = $val;
			}
			//$row = $result->fetch_array(MYSQLI_ASSOC);
			$res['status'] = 'ok';
			$data = array();
			foreach ($mass as $k => $v) {
				$data[] = array_merge(array('track' => $k), $v);
			}

			$res['data'] = $data;
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			//print_r($res);
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
		break;
	}
	default:
	{
		die();
	}
}
print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
?>