<?php
if (!defined('SYSTEM_START_9876543210')) exit;  

udpate_staff_onilne($staff_id_debug);

//print_r($_POST);

//echo boolval($_POST['en']) . "\n";

$list_offices = array();

function get_list_offices()
{
	global $db_connect, $list_offices;
	if ($result = $db_connect->query("SELECT id, name FROM agents_offices WHERE active='1' ORDER BY id ASC;"))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
			$list_offices[$row['id']] = $row['name'];
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
}
$status_mass = array(
					"new" => "new",
					"in_work" => "primary_work",
					"approve" => "credit_approved",
					"reject" => "credit_declined",
					"make" => "loan_issued"
					);

$res = array();
switch($_POST['action'])
{
	case 'add_worker':
	{
		$t_key = "";
		$t_val = "";
		foreach ($_POST as $k => $v)
		{
			$key = $k;
			switch($k)
			{
				case 'passwd':
					$key = 'password';
				case 'login':
				case 'name':
				case 'email':
				case 'phone':
				case 'office':
				{
					if ($t_key)
						$t_key .= ", ";
					$t_key .= "$key";
					
					if ($t_val)
						$t_val .= ", ";
					$t_val .= "'$v'";
					break;
				}
				default: break;
			}
		}
		
		$token = md5(time() . generateRandomString(128));
		$t_key .= ", token";
		$t_val .= ", '$token'";
		$sql = "INSERT INTO agents($t_key) VALUES ($t_val);";
		
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	case 'edit_worker':
	{
		$values = "";
		foreach ($_POST as $k => $v)
		{
			$key = $k;
			switch($k)
			{
				case 'passwd':
				{
					if ($v != "")
						$key = 'password';
					else
						break;
				}
				case 'login':
				case 'name':
				case 'email':
				case 'phone':
				case 'office':
				{
					if ($values)
						$values .= ", ";
					$values .= "$key='$v'";

					break;
				}
				default: break;
			}
		}
		
		$id = 0 + $_POST['id'];
		$sql = "UPDATE agents SET $values WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'ok';
				//$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	case 'get_worker':
	{
		get_list_offices();
		
		$id = 0 + $_POST['id'];
		$sql = "SELECT * FROM agents WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$res['status'] = 'ok';
				$res['data_len'] = $result->num_rows;
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					//array_push($mass, $row);
					$of_id = $row['office'];
					$of = (array_key_exists($of_id, $list_offices)) ? $list_offices[$of_id] : '<font color="red">Офис расформирован</font>';
					$row['office'] = $of;
					$id = $row['id'];
					unset($row['id']);
					unset($row['password']);
					$mass[$id] = $row;
				}
				$res['data'] = $mass;
			}
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = "Select вернул " . $result->num_rows . " строк.\n";
			}
			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		
		break;
	}
	
	case 'ch_areq_status':
	{
		$id = 0 + $_POST['id'];
		$val = $_POST['val'];
		
		if (!isset($status_mass[$val]))
		{
			$res['status'] = 'failed';
			$res['msg'] = "Неправильный статус $val";
			break;
		}
		
		$val = $status_mass[$val];
		$sql = "UPDATE forms SET status='$val' WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}

        insertIntoFormStatusLog($id, $val, '0');
		
		break;
	}
	
	case 'del_agent':
	{
		$id = 0 + $_POST['id'];
		$sql = "UPDATE agents SET deleted=1 WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	case 'add_office':
	{
		$t_key = "";
		$t_val = "";
		foreach ($_POST as $k => $v)
		{
			$key = $k;
			switch($k)
			{
				case 'name':
				case 'description':
				{
					if ($t_key)
						$t_key .= ", ";
					$t_key .= "$key";
					
					if ($t_val)
						$t_val .= ", ";
					$t_val .= "'$v'";
					break;
				}
				default: break;
			}
		}
		$sql = "INSERT INTO agents_offices($t_key) VALUES ($t_val);";
		
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	case 'edit_office':
	{
		$values = "";
		foreach ($_POST as $k => $v)
		{
			$key = $k;
			switch($k)
			{
				case 'name':
				case 'description':
				case 'access':
				{
					if ($values)
						$values .= ", ";
					$values .= "$key='$v'";

					break;
				}
				default: break;
			}
		}
		
		$id = 0 + $_POST['id'];
		$sql = "UPDATE agents_offices SET $values WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'ok';
				//$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	case 'get_office':
	{
	
		$id = 0 + $_POST['id'];
		$sql = "SELECT * FROM agents_offices WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$res['status'] = 'ok';
				$res['data_len'] = $result->num_rows;
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$id = $row['id'];
					unset($row['id']);
					$mass[$id] = $row;
				}
				$res['data'] = $mass;
			}
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = "Select вернул " . $result->num_rows . " строк.\n";
			}

			$result->close();
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		
		break;
	}
	
	case 'del_office':
	{
		$id = 0 + $_POST['id'];
		$sql = "UPDATE agents_offices SET active='0' WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
				$res['status'] = 'ok';
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		break;
	}
	
	default:
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не найдена задача " . $_POST['action'];
		break;
	}
}

print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
die();

?>