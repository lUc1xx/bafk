<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

udpate_staff_onilne($staff_id_debug);

$error = false;
if (!$error)
{
	if ($result = $db_connect->query("SELECT id, name FROM offices WHERE 1 ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$offices_mass[$row['id']] = $row['name'];
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}

if (!$error)
{
	if ($result = $db_connect->query("SELECT id, lastname, firstname, patronymic, office FROM staff ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$fio_a = $row['lastname'];
			if ($row['firstname'] != '')
				$fio_a .= ' ' . substr($row['firstname'], 0, 2) . '.';
			if ($row['patronymic'] != '')
				$fio_a .= ' ' . substr($row['patronymic'], 0, 2) . '.';
					
			$staff_mass[$row['id']] = array("fio" => $fio_a, "office" => $row['office']);
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}

if ($error)
	die();

$res = array();
switch($_POST['action'])
{
	case 'get_list';
	{
		if (isset($_POST['dfrom']) && isset($_POST['dto']))
		{
			$dfrom = $_POST['dfrom'];
			$dto = $_POST['dto'];
			$date_filter = "date_add BETWEEN '$dfrom 00:00:00.000000' AND '$dto 23:59:59.999999'";
		}
		else if (!isset($_POST['dfrom']) && !isset($_POST['dto']))
		{
			
			$dfrom = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') -5, date('Y')));
			$today = date("Y-m-d H:i:s", mktime(23, 59, 59, date('m'), date('d'), date('Y')));
			$date_filter = "date_add BETWEEN '$dfrom.000000' AND '$today.999999'";
		}
		else
		{
			die();
		}
		
		$form_list = array();
		$data = array();
		
		$date_filter = " WHERE type='mini' AND (" . $date_filter . ")";
		$sql = "SELECT id, date_add, sum, status, office, manager, tm_man, ozs_man FROM forms$date_filter ORDER BY date_add DESC;";
		
		$min_form_id = 99999999999;
		$max_form_id = 0;
		
		if ($result = $db_connect->query($sql))
		{
			//$res = "Select вернул " . $result->num_rows . " строк.\n";
			if($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$formid = 0 + $row['id'];
					$tm_man = $row['tm_man'];
					
					if(0 + $tm_man == 18)
						continue;
					
					if ($formid < $min_form_id)
						$min_form_id = $formid;
					
					if ($formid > $max_form_id)
						$max_form_id = $formid;
					
					
					
					$form_list[$formid] = $tm_man;
					
					if (isset($data[$tm_man]))
					{
						$data[$tm_man]['count']++;
						$data[$tm_man]['sum'] += 0 + $row['sum'];
						if (0 + $row['ozs_man'] > 0)
							$data[$tm_man]['contr_s']++;
					}
					else
					{
						$data[$tm_man]['count'] = 1;
						$data[$tm_man]['sum'] = 0 + $row['sum'];
						if (0 + $row['ozs_man'] > 0)
							$data[$tm_man]['contr_s'] = 1;
						else
							$data[$tm_man]['contr_s'] = 0;
						
						$data[$tm_man]['sum_a'] = 0;
					}
					
					if (!isset($data[$tm_man]['fio']))
					{
						if ($tm_man != '')
							$data[$tm_man]['fio'] = $staff_mass[$tm_man]['fio'];
					}
					//array_push($data, $row);
				}
			}

			$result->close();
			
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		$max_form_id++;
		if ($min_form_id > 0)
			$min_form_id--;
		
		$sql = "SELECT form_id, sum_answer FROM bank_send WHERE form_id > '$min_form_id' AND form_id < '$max_form_id' ORDER BY id DESC;";
		
		if ($result = $db_connect->query($sql))
		{
			if($result->num_rows)
			{
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$tm_man = $form_list[$row['form_id']];
					if (isset($data[$tm_man]))
					{
						if (isset($data[$tm_man]['sum_a']))
							$data[$tm_man]['sum_a'] += 0 + $row['sum_answer'];
						else
							$data[$tm_man]['sum_a'] = 0 + $row['sum_answer'];
					}
				}
			}

			$result->close();
			
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
		
		$res['status'] = 'OK';
		$res['data'] = $data;
		//$res['min_form_id'] = $min_form_id;
		//$res['max_form_id'] = $max_form_id;
		
		break;
	}
	
	default:
	{
		$res['status'] = 'failed';
		$res['msg'] = 'Неверный запрос (action)';
		break;
	}
}

print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
?>