<?php
if (!defined('SYSTEM_START_9876543210')) exit;

udpate_staff_onilne($staff_id_debug);


if ($_POST['action'] == 'get_algo_set')
{
	$block_list = array();
	
	if ($result = $db_connect->query("SELECT name, description FROM form_fields_settings WHERE type='block';"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$block_list[$val['name']] = array('description' => $val['description']);
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($result = $db_connect->query("SELECT position, name_desc, algo_text FROM work_algo;"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			//if (in_array($val['position'], $block_list))
			if (array_key_exists($val['position'], $block_list))
			{
				$tmp = $block_list[$val['position']];
				$tmp['algo_text'] = $val['algo_text'];
				$tmp['algo_name'] = $val['name_desc'];
				$block_list[$val['position']] = $tmp;
			}
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$res['status'] = 'ok';
	$res['block_list'] = $block_list;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'get_algo_url')
{
    $block_list = array();

    if ($result = $db_connect->query("SELECT * FROM algo_urls WHERE deleted IS NULL OR deleted='0';"))
    {
        while($val = $result->fetch_array(MYSQLI_ASSOC))
        {
            $block_list[$val['id']] = $val;
        }

        $result->close();
    }
    else
    {
        $res['status'] = 'failed';
        $res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
        print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
        die();
    }

    $res['status'] = 'ok';
    $res['list'] = $block_list;
    print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
    die();
}
else if ($_POST['action'] == 'get_smst_list')
{
	$smst_list = array();
	if ($result = $db_connect->query("SELECT * FROM sms_templates;"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$smst_list[$val['id']] = $val;
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	$res['status'] = 'ok';
	$res['data'] = $smst_list;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'get_list_filetypes')
{
	$list = array();
	if ($result = $db_connect->query("SELECT * FROM file_types WHERE deleted='0' OR deleted IS NULL;"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$list[] = $val;
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	$res['status'] = 'ok';
	$res['data'] = $list;
	
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'del_ftype')
{
	$id = 0 + $_POST["id"];
	$sql = "UPDATE file_types SET deleted='1' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_ftype')
{
	$id = 0 + $_POST["id"];
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$sql = "UPDATE file_types SET description='$val' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'add_ftype')
{
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$maxid = 0;
	if ($result = $db_connect->query("SELECT MAX(id) as mid FROM file_types;"))
	{
		$v = $result->fetch_array(MYSQLI_ASSOC);
		$maxid = $v['mid'] + 1;
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$name = "usert_" . $maxid;
	
	$sql = "INSERT INTO file_types(name, description, deleted) VALUES('$name', '$val', '0');";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}

	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'get_list_endcodes')
{
	$list = array();
	$list_of = array();
	$list_man = array();
	$list_sms_templ = array();
	if ($result = $db_connect->query("SELECT * FROM close_reasons_list WHERE deleted='0' OR deleted IS NULL;"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$list[] = $val;
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($result = $db_connect->query("SELECT * FROM offices WHERE deleted='0';"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$list_of[] = array('id' => $val['id'], 'name' => $val['name'], 'desc' => $val['description']);
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "2: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($result = $db_connect->query("SELECT id, lastname, firstname, patronymic, office FROM staff WHERE deleted='0' ORDER BY id ASC;"))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
			$list_man[] = $row;
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	if ($result = $db_connect->query("SELECT id, name FROM sms_templates WHERE en='1' AND type='close_spam';"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$list_sms_templ[] = $val;
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	
	$res['status'] = 'ok';
	$res['data'] = $list;
	$res['list_of'] = $list_of;
	$res['list_man'] = $list_man;
	$res['list_sms_templ'] = $list_sms_templ;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'del_endcode')
{
	$id = 0 + $_POST["id"];
	$sql = "UPDATE close_reasons_list SET deleted='1' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_endcode')
{
	$id = 0 + $_POST["id"];
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$sql = "UPDATE close_reasons_list SET description='$val' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_endcode_send')
{
	$id = 0 + $_POST["id"];
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$sql = "UPDATE close_reasons_list SET send_to='$val' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_sms_send')
{
	$id = 0 + $_POST["id"];
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$sql = "UPDATE close_reasons_list SET send_sms='$val' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'add_endcode')
{
	$val = htmlspecialchars($_POST["val"], ENT_QUOTES);
	$send = htmlspecialchars($_POST["send"], ENT_QUOTES);
	$smsto = htmlspecialchars($_POST["smsto"], ENT_QUOTES);
	$sql = "INSERT INTO close_reasons_list(description, send_to, send_sms, deleted) VALUES('$val', '$send', '$smsto', '0');";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	
	if ($res['status'] == 'ok')
	{
		if ($result = $db_connect->query("SELECT * FROM close_reasons_list ORDER BY ID DESC LIMIT 1;"))
		{
			$val = $result->fetch_array(MYSQLI_ASSOC);
			$res['id'] = $val['id'];
			$res['description'] = $val['description'];
			$result->close();
		}
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'edit_smst')
{
	$id = 0 + $_POST["id"];
	$name = $_POST["name"];
	$val = urldecode($_POST["val"]);
	$sql = "UPDATE sms_templates SET $name='$val' WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'del_smst')
{
	$id = 0 + $_POST["id"];
	$sql = "DELETE FROM sms_templates WHERE id = '$id';";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'add_smst')
{
	$name = $_POST["name"];
	$type = urldecode($_POST["type"]);
	$msg = urldecode($_POST["msg"]);
	$en = 0 + $_POST["en"];
	$av_tm = 0 + $_POST["av_tm"];
	$av_ozs = 0 + $_POST["av_ozs"];
	$sql = "INSERT INTO sms_templates(type, en, av_tm, av_ozs, name, msg) VALUES('$type', '$en', '$av_tm', '$av_ozs', '$name', '$msg');";
	if ($result = $db_connect->query($sql))
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else if ($_POST['action'] == 'make_new_algo')
{
	$text_algo = preg_replace("/\*and\*/m", '&', urldecode($_POST["new_algo_text"]));
	$pos = $_POST["pos"];
	$name = $_POST["name"];
	$edit = 0 + $_POST["edit"];
	
	$fp = fopen("../logs/algo.log", "a+");
	//date_default_timezone_set('Etc/GMT'); $this->phone', '$this->text' '$id', '$staff_id
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= " => " . print_r($staff_id, true) . " => " . print_r($pos, true) . " => ";
	$data_ .= " => " . print_r($name, true) . "\n\n" . print_r($text_algo, true) . "\n\n\n\n";
	$test = fwrite($fp, $data_);
	fclose($fp);
	
	if ($edit)
		$sql = "UPDATE work_algo SET update_time=NOW(), staffid='$staff_id', name_desc='$name', algo_text='$text_algo' WHERE position = '$pos';";
	else
		$sql = "INSERT INTO work_algo(update_time, staffid, position, name_desc, algo_text) VALUES (NOW(), '$staff_id', '$pos', '$name', '$text_algo');";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = 'affected_rows';
			$res['msg2'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = $sql;
	}
	//$res['status'] = 'ok';
	//$res['data'] = $text_algo;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
if ($_POST['action'] == 'update_blank')
{
	$i = 0 + $_POST['bid'];
	$num_files = 0 + $_POST['num_files'];
	if(empty($_FILES))
	{
		$res['status'] = 'failed';
		$res['msg'] = 'Нет файлов';
	}
	else
	{
		$data['files'] = $num_files;

			if (!isset($_FILES["uploadfile$i"]['name']))
			{
				$res['status'] = 'failed';
				$res['msg'] = 'У файла нет имени';
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				die();
			}

			if ($_FILES["uploadfile$i"]['error'])
			{
				$res['status'] = 'failed';
				$res['msg'] = $_FILES["uploadfile$i"]['error'];
				print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
				die();
			}
			
			$file_name = $_FILES["uploadfile$i"]['name'];
			$file_length = $_FILES["uploadfile$i"]['size'];
			
			if($_FILES["uploadfile$i"]['size']>0) 
			{
				$data[$i] = "bid:$i; Получен файл $file_name размером $file_length байт";
				$dir = FORMS_DIR2;
				
				if (!is_dir($dir)) {
					if(!mkdir($dir, 0777, true))
					{
						$res['status'] = 'failed';
						$res['msg'] = 'Ошибка создания дирректории';
						print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
						die();
					}
				}
					
				if(is_uploaded_file($_FILES["uploadfile$i"]["tmp_name"]))
				{
					// Если файл загружен успешно, перемещаем его
					// из временной директории в конечную
					//$hash_name = md5($file_name . $file_length);
					
					switch($i)
					{
						case 0:
						{
							move_uploaded_file($_FILES["uploadfile$i"]["tmp_name"], $dir . "/" . "contract.docx");
							$res['status'] = 'ok';
							break;
						}
						case 1:
						{
							move_uploaded_file($_FILES["uploadfile$i"]["tmp_name"], $dir . "/" . "contract_ur.docx");
							$res['status'] = 'ok';
							break;
						}
						case 2:
						{
							move_uploaded_file($_FILES["uploadfile$i"]["tmp_name"], $dir . "/" . "act.docx");
							$res['status'] = 'ok';
							break;
						}
						default: 
						{
							$res['status'] = 'failed';
							$res['msg'] = "Нет такой заготовки!";
							break;
						}
					}
					
					//move_uploaded_file($_FILES["uploadfile$i"]["tmp_name"], $dir."/".$_FILES["uploadfile$i"]["name"]);
					//$sql="INSERT INTO files VALUES(0, '$anket_id', '$filename', '$staff_id', 1, NOW());";
					//$sql_in = mysql_query($sql) or die("<br>DB_FAIL". mysql_error());
					
					
					
					
				}
				else 
				{
					$res['status'] = 'failed';
					$res['msg'] = "Ошибка загрузки файла $file_name";
					print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
					die();
				}
			
		}
		
		$res['status'] = 'ok';
		$res['msg'] = $data;
	}
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = 'Не найдена команда';
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}



function unicode_decode2($str) {
    return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
}
function unicode_decode3($str) {
    return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
}

?>