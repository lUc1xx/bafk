<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

$all_fields_mass = array();
$fields_mass = array();
$banks_mass = array();
$block_mass = array();

if ($result = $db_connect->query("SELECT id, name FROM old_banks"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$banks_mass[$row['id']] = $row['name'];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$office_filter = '';
if ($staff_office_type != 'main')
{
	if ($staff_office_type == 'tm')
	{
		if ($staff_position == 'director_tm')
			$office_filter = " AND av_tm_dir='1'";
		else if ($staff_position == 'manager_tm')
			$office_filter = " AND av_tm_man='1'";
	}
	else if ($staff_office_type == 'ozs')
	{
		if ($staff_position == 'director_ozs')
			$office_filter = " AND av_ozs_dir='1'";
		else if ($staff_position == 'manager_ozs')
			$office_filter = " AND av_ozs_man='1'";
	}
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block'$office_filter ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($all_fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($block_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}


echo '<div><button onclick="javascript:save_data();">Создать анкету и сохранить данные</button></div>';

echo '<div id="anketa_data_wrap">';
	echo '<div><input oninput="javascript:filter_mass(this);" placeholder="Фильтр полей">';
	//echo '<span style="display:none;" id="save_data_button" onclick="javascript:save_data();">Сохранить изменения</span>';
	echo '</div>';
	echo '<div id="anketa_data">';
	
	array_push($block_mass, array("id" => '', "description" => "Дополнительная информация", "goal" => "miss"));
	foreach ($block_mass as $keyb => $valb)
	{
		if ($valb['id'] != '124')
			continue;
		
		$show_block = '';
		if ($valb['goal'] != null)
			$show_block = 'style="display:none;"';
		
		if ($staff_office_type == 'main')
			 $show_block = '';
		
		echo "<div class=\"block_wrap\"$show_block>";
		echo '<div class="head_a">';
		echo '<div class="head_d1">';
		echo $valb['description'];
		echo '</div><div class="head_d2">';
		if ($valb['id'] == 124)
		echo "<span id=\"algo_first\" class=\"algo_red\" data-tooltip=\"algo_1\">Алгоритм</span>";
		echo "</div>";
		echo "</div>";
		foreach ($all_fields_mass as $key => $val)
		{
			if ($val['block'] != $valb['id'])
				continue;
			
			$miss = true;
			$readonly = " disabled";
			foreach ($fields_mass as $v)
			{
				if($val['name'] == $v['name'])
				{
					$miss = false;
					$readonly = "";
					break;
				}
			}
			
			echo '<div class="row_a">';
			echo '<div class="fname">' . $val['description'] . ':</div><div class="fval">';
			$inp_show = 'display:none;';
			$spanshow = '';
			
			$multi = '';
			$focus = '';
			
			$addr_span = "";
			if ($val['type'] == 'addr')
				$addr_span = ' class="addr_span"';
			
			echo "<span$addr_span style=\"cursor:pointer; font-style:italic; color:grey; $spanshow\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">Ввести данные</span>";
			
			$oninputscript = '';
			switch($val['type'])
			{
				case 'addr':
				{
						$addr_data_m[0] = '';
						$addr_data_m[1] = '';
						$addr_data_m[2] = '';
						$addr_data_m[3] = '';
						$addr_data_m[4] = '';
						$addr_data_m[5] = '';
						$addr_data_m[6] = '';
						$addr_data_m[7] = '';
						$vval = '';
					
					$faname = $val['name'];
					
					$js = 'onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" ';
					
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Регион: </span><input class="fil2" type="text" name="' 			. $faname . '_reg"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[0] . '" oninput="javascript:reg_help(this);">';
					echo '<span id="sp_' . $val['name'] . '_reg"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Район: </span><input type="text" name="' 			. $faname . '_ray"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[1] . '" oninput="javascript:reg_help(this);">';
					echo '<span id="sp_' . $val['name'] . '_ray"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Нас.&nbsp;пункт: </span><input type="text" name="' . $faname . '_city" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[2] . '" oninput="javascript:reg_help(this);">';
					echo '<span id="sp_' . $val['name'] . '_city"></span><i class="fil5"></i><ul name="rsl" class="fil2"></ul></div>';
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Улица:</span><input type="text" name="' 			. $faname . '_street"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[3] . '">';
					echo '<span id="sp_' . $val['name'] . "_street\"></span></div>";
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Дом:</span><input type="text" name="' 				. $faname . '_dom"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[4] . '">';
					echo '<span id="sp_' . $val['name'] . "_dom\"></span></div>";
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Строение:</span><input type="text" name="' 		. $faname . '_str"  ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[5] . '">';
					echo '<span id="sp_' . $val['name'] . "_str\"></span></div>";
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Корпус:</span><input type="text" name="' 			. $faname . '_corp" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[6] . '">';
					echo '<span id="sp_' . $val['name'] . "_corp\"></span></div>";
					echo '<div class="addr_div" style="display:none;"><span style="display:inline-block;width:75px;">Квартира:</span><input type="text" name="' 		. $faname . '_apar" ' . $js . $readonly . ' placeholder="..." value="' . 	$addr_data_m[7] . '">';
					echo '<span id="sp_' . $val['name'] . "_apar\"></span></div>";
					
					break;
				}
				case 'date_field':
				{
						$vval = '';
					
					$ddate = 'onclick="javascript:date_format(this);" onkeyup="javascript:date_format(this);"';
					echo '<input ' . $ddate . ' class="datepicker-here" data-date-format="dd.mm.yyyy" style="' . $inp_show . '" type="text" onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."><span id="sp_' . $val['name'] . "\"></span>";
					break;
				}
				case 'telephone':
				{
					$vval = '';
					
					if(strlen($vval) == 10)
						$vval = "7$vval";
					
					if ($vval != '')
					{
						$vval_tmp = $vval;
						$vval = $vval_tmp[0] . '(' . substr($vval_tmp, 1, 3) . ") " . substr($vval_tmp, 4, 3) . "-" . substr($vval_tmp, 7, 2) . "-" . substr($vval_tmp, 9, 2);
					}
					
					$ttel = 'onclick="javascript:tel_format(this);" onkeyup="javascript:tel_format(this);"';
					echo '<input ' . $ttel . ' style="' . $inp_show . '" type="text" onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."><span id="sp_' . $val['name'] . "\"></span>";
					break;
				}
				case 'banks':
				{
					if ($hit_data)
						echo "<span class=\"banks\" style=\"cursor:pointer; font-style:italic; color:black;\" onclick=\"javascript:show_field4edit(this);\" id=\"span_" . $val['name'] . "\">{$data_mass_a[$val['name']]}</span>";

					echo '<div class="bank_val" style="display:none;" name="' . $val['name'] . '">';
					if ($hit_data)
					{
						$mass_val = explode(',', $data_mass_a[$val['name']]);
						foreach ($mass_val as $k => $v)
						{
							echo '<input syze="40" type="text" value="' . $v . '" oninput="javascript:bank_inp_s(this);"><br>';
						}
					}
					
					echo "</div><div name=\"bank_p\" style=\"display:none\"><select>";
					echo "<option value=\"\">Выберите</option>";
					foreach ($banks_mass as $k => $v)
						echo "<option value=\"$v\">$v</option>";
					echo "</select> <span style=\"cursor:pointer; font-style:italic;\" onclick=\"javascript:add_bp(this);\">Добавить банк</span></div>";
					break;
				}
				case 'numbers':
				{
					$oninputscript = ' onkeyup="javascript:format_numbers(this);"';
				}
				case 'reg':
				case 'input':
				{
					$vval = '';
					
					$sc = '';
					$ul = '';
					
					//if ($val['name'] == 'field1')
					if ($val['type'] == 'reg')
					{
						$tt = 'class="addr_div"';
						//$tt = 'class="fil2" id="txt"';
						//$ttt = '<i class="fil5"></i><ul id="rsl" class="fil2"></ul>';
						$sc = ' oninput="javascript:reg_help(this);"';
						$ul = '<i class="fil5"></i><ul name="rsl" class="fil2"></ul>';
					}
					else if ($val['name'] == 'field127')
					{
						$tt = '';
						$ttt = '';
						if ($vval == '')
							$vval = 100;
					}
					else
					{
						$tt = '';
						$ttt = '';
					}
						
					//echo '<input ' . $tt . ' style="' . $inp_show . '" type="text"' . $oninputscript . ' onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."' . $sc . '>' . $ttt . '<span id="sp_' . $val['name'] . "\"></span>$ul";
					echo '<input ' . $tt . ' style="' . $inp_show . '" type="text"' . $oninputscript . ' onfocus="javascript:focus_field(this);" onblur="javascript:blur_field(this);" name="' . $val['name'] . "\"$readonly value=\"" . $vval . '" placeholder="..."' . $sc . '><span id="sp_' . $val['name'] . "\"></span>$ul";
					break;
				}
				case 'checkbox':
				{
					echo '<input style="' . $inp_show . '" type="checkbox" value="да" onclick="javascript:select_field(this);" name="' . $val['name'] . "\" $ch$readonly><span id=\"sp_" . $val['name'] . '"></span>';
					break;
				}
				case 'multiselect':
				{
					$is_banks = false;
					if ($val['av_values'] == "***banks***")
					{
						$is_banks = true;
						$av_val = $banks_mass;
					}
					else
						$av_val = explode('::', $val['av_values']);
					
					$cc = 0;
					echo "<span id = \"inp_sp_{$val['name']}\" style=\"display:none;\">";
					foreach ($av_val as $k => $v)
					{
						$ch = '';
						
						$v1 = ($is_banks) ? $k : $v;
						$v2 = $v;
						
						echo "<label><input type=\"checkbox\" onclick=\"javascript:select_field(this);\" name=\"" . $val['name'] . '[' . $cc . "]\" value=\"$v1\" $ch $readonly> $v2<span id=\"sp_" . $val['name'] . "\"></span></label><br>";
						$cc++;
					}
					echo "</span>";
					break;
				}
				case 'select':
				{
					$av_val = explode('::', $val['av_values']);

					echo "<select style=\"display:none;\" name=\"" . $val['name'] . "\" onchange=\"javascript:change_select(this);\"$readonly>";
					echo '<option value="">Выберите</option>';
					foreach ($av_val as $v)
					{
						$sel = '';
						echo "<option value=\"$v\"$sel>$v</option>";
					}
					echo "</select><span id=\"sp_" . $val['name'] . '"></span>';
					break;
				}
			}
			echo "</div></div>";
			
			unset($all_fields_mass[$key]);
		}
		echo '</div>';
	}
	echo "</div>";

	echo '<script type="text/javascript">';
	echo "var staff_office_type = '$staff_office_type';\n";
	echo "</script>";
?>