var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

window.onload = get_list;

var summ_of_summ = 0;
var summ_count = 0;

function createElementFromHTML(htmlString) {
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstChild; 
}

function add_row(table, mass)
{
	summ_of_summ += parseInt(mass['sum']);
	summ_count += 1;
	var row = document.createElement("div");
		row.setAttribute("class", "list-row");
		
	table.appendChild(add_cells(row, mass));
}

function add_cells(row, mass)
{
	row.appendChild(add_checbox());
	row.appendChild(add_data(mass['date_add']));
	row.appendChild(add_reg(mass['city']));
	row.appendChild(add_client(mass));
	//row.appendChild(add_contact(mass));
	row.appendChild(add_stage(mass));
	row.appendChild(add_summ(mass));
	return row;
}

function add_data(data)
{
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");
			
			var span = document.createElement("span");
				span.setAttribute("class", "block-selectable");
				span.innerHTML = data;
			d1.appendChild(span);
		div.appendChild(d1);
	return div;
}

function add_reg(data)
{
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");
			
			var span = document.createElement("span");
				span.setAttribute("class", "block-selectable");
				span.innerHTML = data;
			d1.appendChild(span);
		div.appendChild(d1);
	return div;
}

function add_checbox()
{
	var cb = document.createElement("div");
		cb.setAttribute("class", "list-row__cell  list-row__cell-template-id  ");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");
			d1.setAttribute("style", "overflow:visible");
			
			var l1 = document.createElement("label");
				l1.setAttribute("class", "control-checkbox   ");
				
				var d2 = document.createElement("div");
					d2.setAttribute("class", "control-checkbox__body");
					
					var inp = document.createElement("input");
						inp.type = "checkbox";
						inp.name = "";
						inp.setAttribute("class", "");
						//inp.setAttribute("class", "control-checkbox__body");
					d2.appendChild(inp);
					var sp = document.createElement("span");
						sp.setAttribute("class", "control-checkbox__helper ");
					d2.appendChild(sp);
				l1.appendChild(d2);
			d1.appendChild(l1);
		cb.appendChild(d1);
	return cb;
}

function add_client(mass)
{
	
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell list-row__cell-template-name list-row__cell-name");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner content-table__item__inner-template-name ");
			
			var d2 = document.createElement("div");
				d2.setAttribute("class", "list-row__template-name__name");
				
				var d3 = document.createElement("div");
					d3.setAttribute("class", "list-row__tasks");
					
					var span = document.createElement("span");
						span.setAttribute("class", "pipeline_leads__task-icon pipeline_leads__task-icon_yellow");
						span.setAttribute("title", "cДействий по сделке не запланировано");
					d3.appendChild(span);
				d2.appendChild(d3);	
				
				var a = document.createElement("a");
					a.setAttribute("href", "/details/" + mass['id'] + "/");
					a.setAttribute("class", "list-row__template-name__table-wrapper__name-link");
					a.setAttribute("title", mass['lastname'] + ' ' + mass['firstname'] + ' ' + mass['middlename']);
					a.innerHTML = mass['lastname'] + ' ' + mass['firstname'] + ' ' + mass['middlename'];
				d2.appendChild(a);
				
			d1.appendChild(d2);
		div.appendChild(d1);
	return div;
}

function add_contact(mass)
{
	
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell list-row__cell-template-name list-row__cell-name");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");

			var a = document.createElement("a");
				a.setAttribute("href", "/details/" + mass['id'] + "/");
				a.setAttribute("class", "js-navigate-link");
				a.setAttribute("title", mass['lastname']);
				a.innerHTML = mass['lastname'] + ' ' + mass['firstname'] + ' ' + mass['middlename'];
			d1.appendChild(a);
		div.appendChild(d1);
	return div;
}
function add_stage(mass)
{
	
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell list-row__cell-template-name list-row__cell-name");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");
			
			var span = document.createElement("span");
				span.setAttribute("class", "leads__status-label");
				span.setAttribute("style", "background-color: #99ccff");
			
				var span2 = document.createElement("span");
					span2.setAttribute("class", "block-selectable");
					span2.innerHTML = mass['status_desc'];
				span.appendChild(span2);
			d1.appendChild(span);
		div.appendChild(d1);
	return div;
}

function add_summ(mass)
{
	
	var div = document.createElement("div");
		div.setAttribute("class", "list-row__cell");
		
		var d1 = document.createElement("div");
			d1.setAttribute("class", "content-table__item__inner");
			
			var span = document.createElement("span");
				span.setAttribute("class", "block-selectable");
				span.innerHTML = mass['sum'].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			d1.appendChild(span);
		div.appendChild(d1);
	return div;
}

function get_list()
{

	summ_of_summ = 0;
	summ_count = 0;
	
	var table = document.querySelector('#list_table');
	var headt = table.querySelector('#head_table');
	
	var rows = table.querySelectorAll("#list_table>div[class=list-row]");
	
	
	var row_ = rows[0].innerHTML;
	table.removeChild(rows[0]);

	var data = "block=list&action=get_list";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		
		var Item = eval("obj = " + tmp_page);
		
		if (Item["status"] == 'OK')
		{
			var data = Item["data"];
		
			var row_odd = "row_odd";	//нечет
			var row_even = "row_even";	//чет
			var c = 0;
			for (var key in data)
			{
				c++;
				var tmp_add = "";
				var row_c = (c & 1) ? row_odd : row_even;
				tmp_add += '<div class="' + row_c + '">';
				
				var row = data[key];
				
				add_row(table, row);
				/*for (var key2 in row)
				{
					if ((key2 == 'id') || (key2 == 'firstname') || (key2 == 'middlename') || (key2 == 'sum_val') || (key2 == 'status') || (key2 == 'office') || (key2 == 'manager'))
						continue;
					
					
					if (key2 == 'lastname')
					{
						tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row[key2] + ' ' + row['firstname'] + ' ' + row['middlename'] + '</a></div>';
					}
					else if (key2 == 'sum')
					{
						var ssum = row[key2] + ((row['sum_val'] == "rub") ? " р." : "");
						tmp_add += '<div class="list_cell">' + ssum + '</div>';
					}
					else
						tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
				}
				tmp_add += '</div>';
				document.querySelector('#list_table').innerHTML += tmp_add;*/
			}
			document.querySelector('#sum_count').innerHTML = summ_count + " клиент(ов):";
			console.log(summ_count);
			document.querySelector('#sum_val').innerHTML = String(summ_of_summ).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			console.log(summ_of_summ);
		}
		
		//document.querySelector('#main');
	});
}

//4324234234
//423423423
//423423423