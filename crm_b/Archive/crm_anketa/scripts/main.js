$( function() {




    // input file
    $('.input-file').each(function() {
        var $input = $(this),
            $label = $input.next('.js-labelFile'),
            labelVal = $label.html();

        $input.on('change', function(element) {
            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
        });
    });

    //tabs anketa (Вкладки анкеты)
    if($(".card-tabs__item").length) {

        $(".card-tabs__item").on('click', function(){

            var newTab = $(this).data('id'),
                leftPos = $(this).position().left,
                newTabWidth = $(this).outerWidth(),
                slider = $('.card-tabs-slider');


            if(!$(this).hasClass('selected')) {

                //Анимация полоски под названием вкладки
                $('.selected').removeClass('selected');
                $(this).addClass('selected');
                slider.width(newTabWidth);
                slider.css('left', leftPos);


                //Смена вкладки
                $('.anketa-tab').fadeOut();
                $(newTab).fadeIn();
            }
        });
    }

    //selectize
    $('select').selectize();


    // split screen
    if($('#left-side').length) {
        console.log(1);
        Split(['#left-side', '#right-side'], {
            sizes: [45, 55],
            minSize: [150, 300]

        })
    }

    // Открыть / закрыть вкладку с комментариями
    if($('.icon-widgets-toggle-state').length) {
        var commentsBlock = $('.comments-side'),
            mainWrap = $('.anketa_wrap');


        $('.icon-widgets-toggle-state').on('click', function(){
            //открыть комметарии
            if(!commentsBlock.hasClass('active')) {
                commentsBlock.addClass('active');
                mainWrap.addClass('comments-open');
            } else {
                commentsBlock.removeClass('active');
                mainWrap.removeClass('comments-open');
            }
        })
    }


    //Изменить статус анкеты
    if($('#popupList').length) {


        var spisok = $('#popupList'),
            statusLink = $('.control--select--list--item'),
            statusLights =  $('.pipeline-select-view__colors-block');



        // открыть список этапов
        $('.card-cf-lead-status-select').on('click', function(){

            if(!spisok.is(":visible")) {
                spisok.css({"display" : "block", "opacity" : "1"});
            }

        });
        //выбрать этап сделки
        statusLink.on('click', function(){
            var statusNum = $(this).data('status-id');

            //Изменить выделенный этап в выпадающем списке
            statusLink.removeClass("control--select--list--item-selected");
            $(this).addClass("control--select--list--item-selected");

            //Закрыть выпадающий список и изменить название этапа
            spisok.css({"display" : "none", "opacity" : "0"});
            event.stopPropagation();
            var statusName = $(this).data('title');
            $('#statusName').text(statusName);


            //Подсветить полоску-светофор с этапами
            statusLights.css("background-color", "transparent");
            for( var i = 0; i < statusNum; i++) {
                var bgColor = $(statusLights[i]).data("bg-color");
                $(statusLights[i]).css("background-color",  bgColor);
            }
        })


    }

    //Popup functions

    function show(state)
    {

        document.getElementById('window').style.display = state;
        document.getElementById('wrap').style.display = state;
    }

    $( "#wrap, #window .close" ).on('click', function(){
        show('none');
    });


    // Назначить звонок / сделку


    //Функция отмены события
    function removeEvent(eventType) {
        if(eventType == "call") {
            $('.call_set').remove();

        } else if(eventType == "meet") {
            $('.meet_set').remove();
        }
    }

    //Отмена  события по клику на кнопку отмены нв уведомлении

    $('#notificationWrap ').on('click', '.set-event .remove', function(){
        $(this).parent().hasClass('call_set') ? removeEvent("call") : removeEvent("meet");
    });



    //Переменные для сохранения выбранного значения
    var eventDate = "",
        eventTime  = "",
        eventType = "";

    if($('#picker-date-wrap').length) {



        //Открыть окно с датой
        var $datePicker = $('.set-call, .set-meeting').pickadate({
            container: "#picker-date-wrap",
            min: new Date(),
            // Strings and translations
            monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            weekdaysFull: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            weekdaysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            today: '',
            clear: '',
            close: '',
            firstDay: "Понедельник",
            onClose: function () {

                //Сохранить дату в переменную
                eventDate = this.get();
                //Проверить тип события (звонок или встреча)
                eventType = this.$node;
                //Открыть окно с выбором времени
                setTimeout($timePicker.open, 0);

            }
        }).pickadate('picker');

        //Инициализация окна с выбором времени
        var $timePicker = $('#picker-date-wrap').pickatime({
            container: "#picker-date-wrap",
            min: [8, 30],
            max: [19, 0],
            format: "H:i",
            onSet: function () {
                eventTime = this.get();

            },
            onClose: function () {

                //Создание кнопки с уведомлением о событии Звонок
                if ($(eventType).hasClass('set-call')) {
                    //удаление предыдущего события
                    removeEvent("call");

                    //создание нового события
                    var notificationCall = '<span class="call_set set-event">Звонок назначен на ' + eventDate + '&nbsp;' + eventTime + '<i class="remove" ></i></span>';
                    $('#notificationWrap').append(notificationCall);
                }
                //Создание кнопки с уведомлением о событии Встреча
                else {
                    //удаление предыдущего события
                    removeEvent("meet");

                    //создание нового события
                    var notificationMeet = '<span class="meet_set set-event">Встреча назначена на ' + eventDate + '&nbsp;' + eventTime + '<i class="remove" ></i></span>';
                    $('#notificationWrap').append(notificationMeet);
                }

            }
        }).pickatime('picker');

    }




    //Страница списка анкет с перетаскиваемыми анкетами
    if($('.ui-sortable').length) {

        $( ".ui-sortable" ).sortable({
            revert: true
        });

        $( ".ui-draggable" ).draggable({
            connectToSortable: "#sortable",
            helper: "clone",
            revert: "invalid"
        });

        $( "ul, li" ).disableSelection();
    }

    //Валидаия телефона
    if($(".phone-num").length) {
        $(".phone-num").mask("8 (999) 999-9999");
    }




    //Поставить на контроль (временная функция)
    $('.set-control').on('click', function(){
        $(this).toggleClass('active');
    })

    //Открыть pop up с описанием продукта  (временная функция)
    $('.podbor-wrap .tc a ').click(function(){
        show('block');
        return false;
    })



    //Закрытие всплывашки с выбором статуса сделки
    // function hideListPopup() {
    //     document.getElementById('wrap2').style.display = "none";
    //     document.getElementById('popupList').style.display = "none";
    //     console.log("q");
    //
    // }
    // $('#wrap2').on('click', function(){
    //     hideListPopup();
    // })




   

} );