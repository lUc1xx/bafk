$( function() {

    if($('.ui-sortable').length) {

        $( ".ui-sortable" ).sortable({
            revert: true
        });

        $( ".ui-draggable" ).draggable({
            connectToSortable: "#sortable",
            helper: "clone",
            revert: "invalid"
        });

        $( "ul, li" ).disableSelection();
    }

   

} );