<?php

header('Content-Type: text/html; charset=UTF-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('SYSTEM_START_9876543210', true);
define('DEBUG', true);

include_once "_bdc.php";
include_once "_functions.php";
include_once "_config.php";

$login_ = "";
$token_ = "";
if (!empty ($_COOKIE['login']))
	$login_ = $_COOKIE['login'];	//Добавить удаление ненужных символов
if (!empty ($_COOKIE['token']))
	$token_ = $_COOKIE['token'];	//Добавить удаление ненужных символов

if (!authorized($login_, $token_))
{
	mysqli_close($db_connect);
	$res['status'] = 'failed';
	$res['msg'] = "Access denied";
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
	//die("Access denied");
}

$banks = array();
if ($result = $db_connect->query("SELECT id, name FROM old_banks ORDER BY id ASC;"))
{
		
	while ($a = $result->fetch_array(MYSQLI_ASSOC))
	{
		$banks[$a["id"]] = $a["name"];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if ($_POST['act'] == 'detail')
{
	$pr_id = $_POST['pr_id'] + 0;
	$req = "SELECT * FROM old_products WHERE id=$pr_id";
	
	if ($result = $db_connect->query($req))
	{
			
		$a = $result->fetch_array(MYSQLI_ASSOC);
		foreach ($a as $key => $value) {
		if ($key == 'bank_name')
			$value = $banks[$value];
		$a[$key]=iconv('cp1251', 'utf-8',$value);
		}
		$b = preg_replace('/\\n/', '<br>', $a);
		echo json_encode($b, JSON_UNESCAPED_UNICODE);
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
	die();
}


/*$fp = fopen("../logs/podbor.log", "a+");
date_default_timezone_set('Etc/GMT');
$data_ = date("Y-m-d H:i:s", time()+3*3600);
$data_ .= " " . print_r($_POST, true) . "\n\n";
$test = fwrite($fp, $data_);
fclose($fp);
*/
if (defined('DEBUG'))
{
	//echo "<pre>";
	//print_r($_POST);
//echo "</pre>";
}

$age = 0;
$res = $_POST['res'] + 0;
$client = $_POST['client'] + 0;
if ($_POST['income'] != '')
	$income = $_POST['income'] + 0;
if ($_POST['age'] != '')	
	$age = $_POST['age'] + 0;
$sex = $_POST['sex'] + 0;
if ($_POST['cr_percent'] != '')
	$cr_percent = $_POST['cr_percent'] + 0;
if ($_POST['credit_summ'] != '')
	$credit_summ = $_POST['credit_summ'] + 0;
if ($_POST['lim_time'] != '')
	$lim_time = $_POST['lim_time'] + 0;
$arm = $_POST['arm'] + 0;
$adr = $_POST['adr'] + 0;
if ($_POST['nbki'] != '')
	$nbki = $_POST['nbki'] + 0;
if ($_POST['adr_time'] != '')
	$adr_time = $_POST['adr_time'] + 0;

$cr_his = $_POST['cr_his'] + 0;	
$delay_act = $_POST['delay_act'] + 0;	

if ($_POST['co_max'] != '')
	$co_max = $_POST['co_max'] + 0;
if ($_POST['lim_all'] != '')
	$lim_all = $_POST['lim_all'] + 0;
//disable
if ($_POST['bal_now'] != '')
	$bal_now = $_POST['bal_now'] + 0;
$zaim_mfo = $_POST['zaim_mfo'] + 0;
$k_h_miss = $_POST['k_h_miss'] + 0;

$ps_c = $_POST['ps_c'] + 0;
if ($_POST['ps_v0'] != '')
	$ps_v0 = $_POST['ps_v0'] + 0;
if ($_POST['ps_v1'] != '')
	$ps_v1 = $_POST['ps_v1'] + 0;
if ($_POST['ps_v2'] != '')
	$ps_v2 = $_POST['ps_v2'] + 0;
if ($_POST['ps_v3'] != '')
	$ps_v3 = $_POST['ps_v3'] + 0;
if ($_POST['ps_v4'] != '')
	$ps_v4 = $_POST['ps_v4'] + 0;

$psa_c = $_POST['psa_c'] + 0;
if ($_POST['psa_v0'] != '')
	$psa_v0 = $_POST['psa_v0'] + 0;
if ($_POST['psa_v1'] != '')
	$psa_v1 = $_POST['psa_v1'] + 0;
if ($_POST['psa_v2'] != '')
	$psa_v2 = $_POST['psa_v2'] + 0;
if ($_POST['psa_v3'] != '')
	$psa_v3 = $_POST['psa_v3'] + 0;
if ($_POST['psa_v4'] != '')
	$psa_v4 = $_POST['psa_v4'] + 0;

$ps_c_6 = $_POST['ps_c_6'] + 0;
if ($_POST['ps_v0_6'] != '')
	$ps_v0_6 = $_POST['ps_v0_6'] + 0;
if ($_POST['ps_v1_6'] != '')
	$ps_v1_6 = $_POST['ps_v1_6'] + 0;
if ($_POST['ps_v2_6'] != '')
	$ps_v2_6 = $_POST['ps_v2_6'] + 0;
if ($_POST['ps_v3_6'] != '')
	$ps_v3_6 = $_POST['ps_v3_6'] + 0;
if ($_POST['ps_v4_6'] != '')
	$ps_v4_6 = $_POST['ps_v4_6'] + 0;

$ps_c_12 = $_POST['ps_c_12'] + 0;
if ($_POST['ps_v0_12'] != '')
	$ps_v0_12 = $_POST['ps_v0_12'] + 0;
if ($_POST['ps_v1_12'] != '')
	$ps_v1_12 = $_POST['ps_v1_12'] + 0;
if ($_POST['ps_v2_12'] != '')
	$ps_v2_12 = $_POST['ps_v2_12'] + 0;
if ($_POST['ps_v3_12'] != '')
	$ps_v3_12 = $_POST['ps_v3_12'] + 0;
if ($_POST['ps_v4_12'] != '')
	$ps_v4_12 = $_POST['ps_v4_12'] + 0;



$restr = $_POST['restr'] + 0;
if ($_POST['obr_7'] != '')
	$obr_7 = $_POST['obr_7'] + 0;
if ($_POST['obr_14'] != '')
	$obr_14 = $_POST['obr_14'] + 0;
if ($_POST['obr_mon'] != '')
	$obr_mon = $_POST['obr_mon'] + 0;

if ($_POST['first_deb'] != '')
	$first_deb = $_POST['first_deb'] + 0;
if ($_POST['mkad'] != '')
	$mkad = $_POST['mkad'] + 0;

$doc_sob = $_POST['doc_sob'] + 0;
$owner = $_POST['owner'] + 0;
//$obj_type = $_POST['obj_type'] + 0;
$replan = $_POST['replan'] + 0;
$house_mat = $_POST['house_mat'] + 0;
$found = $_POST['found'] + 0;

if ($_POST['obj_age'] != '')
	$obj_age = $_POST['obj_age'] + 0;
if ($_POST['wear'] != '')
	$wear = $_POST['wear'] + 0;


//$ent = $_POST['ent'] + 0;
$ca = $_POST['ca'] + 0;
$pfr = $_POST['pfr'] + 0;
if ($_POST['firm_per'] != '')
	$firm_per = $_POST['firm_per'] + 0;
$nal = $_POST['nal'] + 0;

if ($_POST['proc'] != '')
	$proc = $_POST['proc'] + 0;

$pr_nal = $_POST['pr_nal'] + 0;

if ($_POST['pr_4'] != '')
	$pr_4 = $_POST['pr_4'] + 0;

if ($_POST['co'] != '')
	$co = $_POST['co'] + 0;

if ($_POST['co_umax'] != '')
	$co_umax = $_POST['co_umax'] + 0;

$delay_29 = $_POST['delay_29'] + 0;

$psc = $_POST['psc'] + 0;
if ($_POST['psc_u0'] != '')
	$psc_u0 = $_POST['psc_u0'] + 0;
if ($_POST['psc_u1'] != '')
	$psc_u1 = $_POST['psc_u1'] + 0;
if ($_POST['psc_u2'] != '')
	$psc_u2 = $_POST['psc_u2'] + 0;
if ($_POST['psc_u3'] != '')
	$psc_u3 = $_POST['psc_u3'] + 0;
if ($_POST['psc_u4'] != '')
	$psc_u4 = $_POST['psc_u4'] + 0;

$ps = $_POST['ps'] + 0;
if ($_POST['ps_u0'] != '')
	$ps_u0 = $_POST['ps_u0'] + 0;
if ($_POST['ps_u1'] != '')
	$ps_u1 = $_POST['ps_u1'] + 0;
if ($_POST['ps_u2'] != '')
	$ps_u2 = $_POST['ps_u2'] + 0;
if ($_POST['ps_u3'] != '')
	$ps_u3 = $_POST['ps_u3'] + 0;
if ($_POST['ps_u4'] != '')
	$ps_u4 = $_POST['ps_u4'] + 0;



$work = $_POST['work'] + 0;
$c_work = $_POST['c_work'] + 0;
$c_inc = $_POST['c_inc'] + 0;
$exp_now = $_POST['exp_now'] + 0;
$reg_date = $_POST['reg_date'] + 0;
$work_type = $_POST['work_type'] + 0;
$work_adr = $_POST['work_adr'] + 0;
$spark = $_POST['spark'] + 0;

$inc_true = $_POST['inc_true'] + 0;


if ($_POST['act'] == 'save_pod')
{
	$anketa_id = 		($_POST['anketa_id'] == '') ? "" : $_POST['anketa_id'] + 0;
	$income = 			($_POST['income'] == '') ? "" : $_POST['income'] + 0;
	$age = 				($_POST['age'] == '') ? "" : $_POST['age'] + 0;
	$cr_percent = 		($_POST['cr_percent'] == '') ? "" : $_POST['cr_percent'] + 0;
	$credit_summ = 		($_POST['credit_summ'] == '') ? "" : $_POST['credit_summ'] + 0;
	$lim_time = 		($_POST['lim_time'] == '') ? "" : $_POST['lim_time'] + 0;
	$adr_time = 		($_POST['adr_time'] == '') ? "" : $_POST['adr_time'] + 0;
	$nbki = 			($_POST['nbki'] == '') ? "" : $_POST['nbki'] + 0;
	$co_max = 			($_POST['co_max'] == '') ? "" : $_POST['co_max'] + 0;
	$lim_all = 			($_POST['lim_all'] == '') ? "" : $_POST['lim_all'] + 0;
	$bal_now = 			($_POST['bal_now'] == '') ? "" : $_POST['bal_now'] + 0;
	$obr_14 = 			($_POST['obr_14'] == '') ? "" : $_POST['obr_14'] + 0;
	$obr_7 = 			($_POST['obr_7'] == '') ? "" : $_POST['obr_7'] + 0;
	$obr_mon = 			($_POST['obr_mon'] == '') ? "" : $_POST['obr_mon'] + 0;
	$first_deb = 		($_POST['first_deb'] == '') ? "" : $_POST['first_deb'] + 0;
	$mkad = 			($_POST['mkad'] == '') ? "" : $_POST['mkad'] + 0;
	$obj_age = 			($_POST['obj_age'] == '') ? "" : $_POST['obj_age'] + 0;
	$wear = 			($_POST['wear'] == '') ? "" : $_POST['wear'] + 0;
	$firm_per = 		($_POST['firm_per'] == '') ? "" : $_POST['firm_per'] + 0;
	$proc = 			($_POST['proc'] == '') ? "" : $_POST['proc'] + 0;
	$pr_4 = 			($_POST['pr_4'] == '') ? "" : $_POST['pr_4'] + 0;
	$co = 				($_POST['co'] == '') ? "" : $_POST['co'] + 0;
	$co_umax = 			($_POST['co_umax'] == '') ? "" : $_POST['co_umax'] + 0;
	$psa_c = 			($_POST['psa_c'] == '') ? "" : $_POST['psa_c'] + 0;
	$psc = 				($_POST['psc'] == '') ? "" : $_POST['psc'] + 0;
	$ps_c_6 = 			($_POST['ps_c_6'] == '') ? "" : $_POST['ps_c_6'] + 0;
	$ps_c_12 = 			($_POST['ps_c_12'] == '') ? "" : $_POST['ps_c_12'] + 0;
	$inc_4_cre = 		($_POST['inc_4_cre'] == '') ? "" : $_POST['inc_4_cre'] + 0;
	$a_ds = 			($_POST['a_ds'] == '') ? "" : $_POST['a_ds'] + 0;
	$a_izdeti = 		($_POST['a_izdeti'] == '') ? "" : $_POST['a_izdeti'] + 0;
	$a_skred = 			($_POST['a_skred'] == '') ? "" : $_POST['a_skred'] + 0;
	$find_results = 	($_POST['find_results'] == '') ? "" : $_POST['find_results'] + 0;
	$fill_reds = 		($_POST['fill_reds'] == '') ? "" : preg_replace ("/[^0-9\s\.\,\)\(\/\%]/","",$_POST['fill_reds']);

	
	
	$update = false;
	
	if ($result = $db_connect->query("SELECT * FROM old_prod_res WHERE anketa_id='$anketa_id'"))
	{
		if ($result->num_rows)
			$update = true;
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
	
	
	if (!$update)
	{
		$req = "INSERT INTO prod_res VALUES('0', '$anketa_id', '$res', '$income', '$age', '$sex', '$cr_percent', '$credit_summ', '$lim_time', '$arm', '$adr', '$adr_time', '$nbki', '$client', ";
		$req .= "'$cr_his', '$co_max', '$zaim_mfo', '$lim_all', '$bal_now', '$ps_c', '$restr', '$obr_7', '$obr_14', '$obr_mon', ";
		//$req .= make_str($_POST['guarantor']) . ", " . make_str($_POST['bail']) . ", ";	//guarantor, bail 2|xx
		$req .= "'$first_deb', '$mkad', '$doc_sob', '$owner', " . make_str($_POST['obj_type']) . ", '$replan', '$house_mat', '$found', '$obj_age', '$wear', ";
		$req .= make_str($_POST['comm']) . ", "; //comm 5|xxxxx
		$req .= make_str($_POST['ent']) . ", '$firm_per', '$nal', '$proc', '$pr_nal', '$pr_4', ";
		$req .= make_str($_POST['activs']) . ", ";	//est 7|xxxxxxx
		$req .= make_str($_POST['est']) . ", ";	//est 7|xxxxxxx
		$req .= "'$co', '$co_umax', '$delay_29', '$ps', ";
		$req .= "'$work', '$c_work', '$c_inc', '$exp_now', '$reg_date', '$work_type', '$work_adr', ";
		$req .= "'$ps_v0', '$ps_v1', '$ps_v2', '$ps_v3', '$ps_v4', ";
		$req .= "'$psa_c', '$psa_v0', '$psa_v1', '$psa_v2', '$psa_v3', '$psa_v4', ";
		$req .= "'$psc', '$psc_u0', '$psc_u1', '$psc_u2', '$psc_u3', '$psc_u4', ";
		$req .= "'$ps_u0', '$ps_u1', '$ps_u2', '$ps_u3', '$ps_u4', '$inc_true', " . make_str($_POST['stop']) . ", " . make_str($_POST['st_cr']) . ", '$spark', " . make_str($_POST['goal']) . ", '$ca', '$pfr', '$k_h_miss', '$delay_act', ";
		$req .= "'$ps_c_6', '$ps_v0_6', '$ps_v1_6', '$ps_v2_6', '$ps_v3_6', '$ps_v4_6', ";
		$req .= "'$ps_c_12', '$ps_v0_12', '$ps_v1_12', '$ps_v2_12', '$ps_v3_12', '$ps_v4_12', ";
		$req .= "'$inc_4_cre', '$a_ds', '$a_izdeti', '$a_skred', '$find_results', '$fill_reds', ";
		$req .= make_str($_POST['sob']) . ", ";
		$req .= make_str($_POST['zar']) . ", ";
		$req .= make_str($_POST['vzar']);
		$req .= ");";
	}
	else
	{
		$req = "UPDATE prod_res SET res = '$res', income = '$income', age = '$age', sex = '$sex', cr_percent = '$cr_percent', credit_summ = '$credit_summ', lim_time = '$lim_time', arm = '$arm', adr = '$adr', adr_time = '$adr_time', nbki = '$nbki', client = '$client', ";
		$req .= "cr_his = '$cr_his', co_max = '$co_max', zaim_mfo = '$zaim_mfo', lim_all = '$lim_all', bal_now = '$bal_now', ps_c = '$ps_c', restr = '$restr', obr_7 = '$obr_7', obr_14 = '$obr_14', obr_mon = '$obr_mon', ";
		//$req .= "guarantor = " . make_str($_POST['guarantor']) . ", bail = " . make_str($_POST['bail']) . ", ";	//guarantor, bail 2|xx
		$req .= "first_deb = '$first_deb', mkad = '$mkad', doc_sob = '$doc_sob', owner = '$owner', obj_type = " . make_str($_POST['obj_type']) . ", replan = '$replan', house_mat = '$house_mat', found = '$found', obj_age = '$obj_age', wear = '$wear', ";
		$req .= "comm = " . make_str($_POST['comm']) . ", "; //comm 5|xxxxx
		$req .= "ent = " . make_str($_POST['ent']) . ", firm_per = '$firm_per', nal = '$nal', proc = '$proc', pr_nal = '$pr_nal', pr_4 = '$pr_4', ";
		$req .= "activs = " . make_str($_POST['activs']) . ", ";	//est 7|xxxxxxx
		$req .= "est = " . make_str($_POST['est']) . ", ";	//est 7|xxxxxxx
		$req .= "co = '$co', co_umax = '$co_umax', delay_29 = '$delay_29', ps = '$ps', ";
		$req .= "work = '$work', c_work = '$c_work', c_inc = '$c_inc', exp_now = '$exp_now', reg_date = '$reg_date', work_type = '$work_type', work_adr = '$work_adr', ";
		$req .= "ps_v0 = '$ps_v0', ps_v1 = '$ps_v1', ps_v2 = '$ps_v2', ps_v3 = '$ps_v3', ps_v4 = '$ps_v4', ";
		$req .= "psa_c = '$psa_c', psa_v0 = '$psa_v0', psa_v1 = '$psa_v1', psa_v2 = '$psa_v2', psa_v3 = '$psa_v3', psa_v4 = '$psa_v4', ";
		$req .= "psc = '$psc', psc_u0 = '$psc_u0', psc_u1 = '$psc_u1', psc_u2 = '$psc_u2', psc_u3 = '$psc_u3', psc_u4 = '$psc_u4', ";
		$req .= "ps_u0 = '$ps_u0', ps_u1 = '$ps_u1', ps_u2 = '$ps_u2', ps_u3 = '$ps_u3', ps_u4 = '$ps_u4', ";
		$req .= "inc_true = '$inc_true', stop = " . make_str($_POST['stop']) . ", st_cr = " . make_str($_POST['st_cr']) . ", spark = '$spark', goal = " . make_str($_POST['goal']) . ", ";
		$req .= "ca = '$ca', pfr = '$pfr', k_h_miss = '$k_h_miss', delay_act = '$delay_act', ";
		$req .= "ps_c_6 = '$ps_c_6', ps_v0_6 = '$ps_v0_6', ps_v1_6 = '$ps_v1_6', ps_v2_6 = '$ps_v2_6', ps_v3_6 = '$ps_v3_6', ps_v4_6 = '$ps_v4_6', ";
		$req .= "ps_c_12 = '$ps_c_12', ps_v0_12 = '$ps_v0_12', ps_v1_12 = '$ps_v1_12', ps_v2_12 = '$ps_v2_12', ps_v3_12 = '$ps_v3_12', ps_v4_12 = '$ps_v4_12', ";
		$req .= "inc_4_cre = '$inc_4_cre', a_ds = '$a_ds', a_izdeti = '$a_izdeti', a_skred = '$a_skred', find_results = '$find_results', fill_reds = '$fill_reds', ";
		$req .= "sob = " . make_str($_POST['sob']) . ", ";
		$req .= "zar = " . make_str($_POST['zar']) . ", ";
		$req .= "vzar = " . make_str($_POST['vzar']) . " ";
		$req .= "WHERE anketa_id = '$anketa_id';";
	}
	
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			//$res['msg'] = $sql;
			//$result->close();
			echo "OK";
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
			//$res['msg'] = "Не найден id: $id";
			//$result->close();
		}
	}
	die();
}

if ($_POST['act'] == 'load_pod')
{
	$anketa_id = $_POST['anketa_id'] + 0;
	
	if ($result = $db_connect->query("SELECT * FROM prod_res WHERE anketa_id='$anketa_id'"))
	{
		if ($result->num_rows)
		{
			$a = $result->fetch_array(MYSQLI_ASSOC);
			echo json_encode($a);
		}
		else
			echo "MISS";

		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
	
	
	die();
}

if ($_POST['act'] == 'load_ank')
{
	$anketa_id = $_POST['anketa_id'] + 0;
/*	$sql = mysql_query("SELECT sum, str FROM control WHERE id='$anketa_id'") or die('DB_FAIL: '. mysql_error());
	$nrt = mysql_num_rows($sql);
	if ($nrt)
	{
		$a=mysql_fetch_assoc($sql);
		foreach ($a as $key=>$v)
			$a[$key] = iconv('cp1251', 'utf-8',$v);
		echo json_encode($a, JSON_UNESCAPED_UNICODE);
	}
	else
		echo "MISS";*/
	die();
}


$req = "SELECT * FROM old_products";

if (sizeof($_POST))
{
	$req .= " WHERE active = '1'";
	if ($res == 0)
		$req .= " AND res='1'";
	
	if ($client == 0)
		$req .= " AND cr4other='1'";
	
	
	if (isset($income))
		$req .= " AND min_income <= '$income'";
	
	if ($age > 0 && $sex == 0)
		$req .= " AND max_age_m >= '$age' AND min_age_m <= '$age'";
	else if ($age > 0 && $sex == 1)
		$req .= " AND max_age_w >= '$age' AND min_age_w <= '$age'";
	
	if (isset($cr_percent))
		$req .= " AND min_percent <= '$cr_percent'";
	if (isset($credit_summ))
		$req .= " AND min_credit <= '$credit_summ'";
		//$req .= " AND max_credit >= '$credit_summ' AND min_credit <= '$credit_summ'";
	if (isset($lim_time))
		$req .= " AND lim_time <= '$lim_time'";
	
	if ($sex == 0 && $arm != -1)
	{
		//4|1|1|1|0
		$arm = make_mask(6, $arm, 1);
		$req .= " AND arm LIKE '$arm'";
	}
	
	if ($adr != -1)
	{
		//4|1|1|1|0
		$adr = make_mask(3, $adr, 1);
		$req .= " AND adr LIKE '$adr'";
	}
	if (isset($adr_time) && $adr != -1)
		$req .= " AND adr_time <= '$adr_time'";
	
	if (isset($nbki))
		$req .= " AND nbki <= '$nbki'";
	
	/*if ($nbki != -1)
	{
		//4|1|1|1|0
		$nbki = make_mask(4, $nbki, 1);
		$req .= " AND nbki LIKE '$nbki'";
	}*/
	
	//Second
	
	if ($cr_his == 0)
		$req .= " AND cr_his='0'";
	
	if($k_h_miss == 0)
	{
		
		if ($delay_act == 1)
			$req .= " AND delay_act='1'";
		
		if (isset($co_max))
			$req .= " AND co_max >= '$co_max'";
		
		if ($zaim_mfo == 1)
			$req .= " AND zaim_mfo='1'";
			
		if (isset($lim_all))
			$req .= " AND lim_all >= '$lim_all'";
		if (isset($bal_now))
			$req .= " AND bal_now >= '$bal_now'";
			
			
		//if (isset($lim_val))
		//	$req .= " AND lim_val >= '$lim_val'";

		
		if ($ps_c != -1)
		{
			//4|1|1|1|0
			$ps_c = make_mask(5, $ps_c, 1);
			$req .= " AND ps_c LIKE '$ps_c'";
		}

		if (isset($ps_v0))
			$req .= " AND ps_v0 >= '$ps_v0'";
		if (isset($ps_v1))
			$req .= " AND ps_v1 >= '$ps_v1'";
		if (isset($ps_v2))
			$req .= " AND ps_v2 >= '$ps_v2'";
		if (isset($ps_v3))
			$req .= " AND ps_v3 >= '$ps_v3'";
		if (isset($ps_v4))
			$req .= " AND ps_v4 >= '$ps_v4'";

		
		if ($psa_c != -1)
		{
			//4|1|1|1|0
			$psa_c = make_mask(5, $psa_c, 1);
			$req .= " AND psa_c LIKE '$psa_c'";
		}
		
		if (isset($psa_v0))
			$req .= " AND psa_v0 >= '$psa_v0'";
		if (isset($psa_v1))
			$req .= " AND psa_v1 >= '$psa_v1'";
		if (isset($psa_v2))
			$req .= " AND psa_v2 >= '$psa_v2'";
		if (isset($psa_v3))
			$req .= " AND psa_v3 >= '$psa_v3'";
		if (isset($psa_v4))
			$req .= " AND psa_v4 >= '$psa_v4'";
		
		if ($ps_c_6 != -1)
		{
			//4|1|1|1|0
			$ps_c_6 = make_mask(5, $ps_c_6, 1);
			$req .= " AND ps_6 LIKE '$ps_c_6'";
		}

		if (isset($ps_v0_6))
			$req .= " AND ps_6v0 >= '$ps_v0_6'";
		if (isset($ps_v1_6))
			$req .= " AND ps_6v1 >= '$ps_v1_6'";
		if (isset($ps_v2_6))
			$req .= " AND ps_6v2 >= '$ps_v2_6'";
		if (isset($ps_v3_6))
			$req .= " AND ps_6v3 >= '$ps_v3_6'";
		if (isset($ps_v4_6))
			$req .= " AND ps_6v4 >= '$ps_v4_6'";

		if ($ps_c_12 != -1)
		{
			//4|1|1|1|0
			$ps_c_12 = make_mask(5, $ps_c_12, 1);
			$req .= " AND ps_12 LIKE '$ps_c_12'";
		}

		if (isset($ps_v0_12))
			$req .= " AND ps_12v0 >= '$ps_v0_12'";
		if (isset($ps_v1_12))
			$req .= " AND ps_12v1 >= '$ps_v1_12'";
		if (isset($ps_v2_12))
			$req .= " AND ps_12v2 >= '$ps_v2_12'";
		if (isset($ps_v3_12))
			$req .= " AND ps_12v3 >= '$ps_v3_12'";
		if (isset($ps_v4_12))
			$req .= " AND ps_12v4 >= '$ps_v4_12'";

		
		if ($restr == 1)
			$req .= " AND res_en='1'";	
			
		if (isset($obr_7))
			$req .= " AND obr_7 >= '$obr_7'";
		if (isset($obr_14))
			$req .= " AND obr_14 >= '$obr_14'";
		if (isset($obr_mon))
			$req .= " AND obr_mon >= '$obr_mon'";
	}
	//Third
	
	//$req .= make_2mask(2, $_POST['guarantor'], 'guarantor');
	//$req .= make_2mask(2, $_POST['bail'], 'bail');
	$req .= " AND (bail_miss = '1' OR (bail_miss = '0'";
	
	if (isset($first_deb))
		$req .= " AND first_deb <= '$first_deb'";
	if (isset($mkad))
		$req .= " AND mkad >= '$mkad'";

	if ($doc_sob != -1)
	{
		//4|1|1|1|0
		$doc_sob = make_mask(4, $doc_sob, 1);
		$req .= " AND doc_sob LIKE '$doc_sob'";
	}

	if ($owner != -1)
	{
		//4|1|1|1|0
		$owner = make_mask(3, $owner, 1);
		$req .= " AND owner LIKE '$owner'";
	}
	
	/*if ($obj_type != -1)
	{
		//4|1|1|1|0
		$obj_type = make_mask(10, $obj_type, 1);
		$req .= " AND obj_type LIKE '$obj_type'";
	}*/
	
	$req .= make_15mask(15, $_POST['obj_type'], 'obj_type');
	
	if ($replan != -1)
	{
		//4|1|1|1|0
		$replan = make_mask(4, $replan, 1);
		$req .= " AND replan LIKE '$replan'";
	}
	
	if ($house_mat != -1)
	{
		//4|1|1|1|0
		$house_mat = make_mask(7, $house_mat, 1);
		$req .= " AND house_mat LIKE '$house_mat'";
	}
	
	if ($found != -1)
	{
		//4|1|1|1|0
		$found = make_mask(4, $found, 1);
		$req .= " AND found LIKE '$found'";
	}
	
	$req .= make_lmask(6, $_POST['comm'], 'comm');

	if (isset($obj_age))
		$req .= " AND obj_age >= '$obj_age'";
	if (isset($wear))
		$req .= " AND wear >= '$wear'";
	
	$req .= "))";
	//Four

	/*if ($ent != -1)
	{
		//4|1|1|1|0
		$ent = make_mask(3, $ent, 1);
		$req .= " AND ent LIKE '$ent'";
	}*/
	
	$req .= " AND (ul_miss = '1' OR (ul_miss = '0'";
	
	$req .= make_3mask(3, $_POST['ent'], 'ent');
	//$req .= make_4mask(4, $_POST['activs'], 'activs');
	$req .= make_actives_mask(6, $_POST['activs'], 'activs');
	
	
	if ($ca == 0)
		$req .= " AND ca='0'";
	
	if ($pfr == 0)
		$req .= " AND pfr='0'";
	
	if (isset($firm_per))
		$req .= " AND firm_per <= '$firm_per'";

	
	if ($nal != -1)
	{
		//4|1|1|1|0
		$nal = make_mask(5, $nal, 1);
		$req .= " AND nal LIKE '$nal'";
	}
	
	
	if (isset($proc))
		$req .= " AND proc <= '$proc'";
	
	if ($pr_nal != -1)
	{
		//4|1|1|1|0
		$pr_nal = make_mask(3, $pr_nal, 1);
		$req .= " AND pr_nal LIKE '$pr_nal'";
	}
	
	
	
	if (isset($pr_4))
		$req .= " AND pr_4 <= '$pr_4'";
	
	
	$req .= make_8mask(8, $_POST['est'], 'est');
		
		
	if (isset($co))
		$req .= " AND co >= '$co'";	
	if (isset($co_umax))
		$req .= " AND co_umax >= '$co_umax'";
	
	if ($delay_29 == 1)
		$req .= " AND delay_29='1'";	
	
	
	if ($psc != -1)
	{
		//4|1|1|1|0
		$psc = make_mask(5, $psc, 1);
		$req .= " AND psc LIKE '$psc'";
	}
	if (isset($psc_u0))
		$req .= " AND psc_u0 >= '$psc_u0'";
	if (isset($psc_u1))
		$req .= " AND psc_u1 >= '$psc_u1'";
	if (isset($psc_u2))
		$req .= " AND psc_u2 >= '$psc_u2'";
	if (isset($psc_u3))
		$req .= " AND psc_u3 >= '$psc_u3'";
	if (isset($psc_u4))
		$req .= " AND psc_u4 >= '$psc_u4'";
	
	
	if ($ps != -1)
	{
		//4|1|1|1|0
		$ps = make_mask(5, $ps, 1);
		$req .= " AND ps LIKE '$ps'";
	}
	if (isset($ps_u0))
		$req .= " AND ps_u0 >= '$ps_u0'";
	if (isset($ps_u1))
		$req .= " AND ps_u1 >= '$ps_u1'";
	if (isset($ps_u2))
		$req .= " AND ps_u2 >= '$ps_u2'";
	if (isset($ps_u3))
		$req .= " AND ps_u3 >= '$ps_u3'";
	if (isset($ps_u4))
		$req .= " AND ps_u4 >= '$ps_u4'";	
	
	
	$req .= "))";
	
	//Five
	
	
	
	if ($work != -1)
	{
		//4|1|1|1|0
		$work = make_mask(12, $work, 1);
		$req .= " AND work LIKE '$work'";
	}
	
	if ($c_work != -1)
	{
		//4|1|1|1|0
		$c_work = make_mask(6, $c_work, 1);
		$req .= " AND c_work LIKE '$c_work'";
	}
	
	if ($c_inc != -1)
	{
		//4|1|1|1|0
		$c_inc = make_mask(8, $c_inc, 1);
		$req .= " AND c_inc LIKE '$c_inc'";
	}
	
	if ($exp_now != -1)
	{
		//4|1|1|1|0
		$exp_now = make_mask(6, $exp_now, 1);
		$req .= " AND exp_now LIKE '$exp_now'";
	}
	
	if ($reg_date != -1)
	{
		//4|1|1|1|0
		$reg_date = make_mask(5, $reg_date, 1);
		$req .= " AND reg_date LIKE '$reg_date'";
	}
	
	if ($work_type != -1)
	{
		//4|1|1|1|0
		$work_type = make_mask(60, $work_type, 1);
		$req .= " AND work_type LIKE '$work_type'";
	}
	
	if ($work_adr != -1)
	{
		//4|1|1|1|0
		$work_adr = make_mask(4, $work_adr, 1);
		$req .= " AND work_adr LIKE '$work_adr'";
	}
	
	//Six
	
	if ($inc_true == 1)
		$req .= " AND inc_true='1'";
	
	$str = "83";
	$miss_stop = true;
	for ($i=0; $i<83; $i++)
	{
		$str .= ($_POST['stop'][$i] > 0) ? "|1" : "|_";
		if ($_POST['stop'][$i] > 0)
			$miss_stop = false;
	}
	if (!$miss_stop)
		$req .= " AND stop LIKE '$str'";
	
	$str = "4";
	$miss_stop = true;
	for ($i=0; $i<4; $i++)
	{
		$str .= ($_POST['st_cr'][$i] > 0) ? "|1" : "|_";
		if ($_POST['st_cr'][$i] > 0)
			$miss_stop = false;
	}
	if (!$miss_stop)
		$req .= " AND st_cr LIKE '$str'";
	
	$req .= make_6mask(6, $_POST['goal'], 'goal');
	
	if ($spark != -1)
	{
		//4|1|1|1|0
		$spark = make_mask(3, $spark, 1);
		$req .= " AND spark LIKE '$spark'";
	}
	
}
$req .= " ORDER BY min_percent ASC;";
/*$fp = fopen("../../logs/podbor.log", "a+");
date_default_timezone_set('Etc/GMT');
$data_ = date("Y-m-d H:i:s", time()+3*3600);
$data_ .= " " . $req . "\n\n";
$test = fwrite($fp, $data_);
fclose($fp);
*/
//if (defined('DEBUG'))
//	echo "<br>$req<br>";


if ($result = $db_connect->query($req))
{
	$nrt = $result->num_rows;

	if (defined('DEBUG'))
	{
		$i=0;
		$ress = array();
		if ($nrt)
		{
			
			while($a = $result->fetch_array(MYSQLI_ASSOC))
			{
				if (($a["sf"] != '') && ($a['bailn'] == 1))
				{
					$sf_mass = make_mass($a["sf"]);
					if ($sf_mass[1] != 1)
					{
						if ((($_POST['sob'][2] == 1) || ($_POST['sob'][3] == 1) || ($_POST['sob'][4] == 1)) && ($sf_mass[2] != 1))
							continue;
						if (($_POST['sob'][5] == 1) && ($sf_mass[3] != 1))
							continue;
						if (($_POST['sob'][6] == 1) && ($sf_mass[4] != 1))
							continue;
						if (($_POST['sob'][9] == 1) && ($sf_mass[5] != 1))
							continue;
						if (($_POST['sob'][10] == 1) && ($sf_mass[6] != 1))
							continue;
						if (($_POST['sob'][11] == 1) && ($sf_mass[7] != 1))
							continue;
						if (($_POST['sob'][12] == 1) && ($sf_mass[8] != 1))
							continue;
						if (($_POST['sob'][13] == 1) && ($sf_mass[9] != 1))
							continue;
						if (($_POST['sob'][14] == 1) && ($sf_mass[10] != 1))
							continue;
						if (($_POST['sob'][15] == 1) && ($sf_mass[11] != 1))
							continue;
						if (($_POST['sob'][16] == 1) && ($sf_mass[12] != 1))
							continue;
						if (($_POST['sob'][17] == 1) && ($sf_mass[13] != 1))
							continue;
						if (($_POST['sob'][18] == 1) && ($sf_mass[14] != 1))
							continue;
						if (($_POST['sob'][19] == 1) && ($sf_mass[15] != 1))
							continue;
					}
					
					if ($sf_mass[16] != 1)
					{
						if ((($_POST['zar'][2] == 1) || ($_POST['vzar'][2] == 1)) && ($sf_mass[17] != 1))
							continue;
						if ((($_POST['zar'][3] == 1) || ($_POST['vzar'][3] == 1)) && ($sf_mass[18] != 1))
							continue;
						if ((($_POST['zar'][4] == 1) || ($_POST['vzar'][4] == 1)) && ($sf_mass[19] != 1))
							continue;
						if ((($_POST['zar'][5] == 1) || ($_POST['vzar'][5] == 1)) && ($sf_mass[20] != 1))
							continue;
						if ((($_POST['zar'][6] == 1) || ($_POST['vzar'][6] == 1)) && ($sf_mass[21] != 1))
							continue;
						if ((($_POST['zar'][7] == 1) || ($_POST['vzar'][7] == 1)) && ($sf_mass[22] != 1))
							continue;
						if ((($_POST['zar'][8] == 1) || ($_POST['vzar'][8] == 1)) && ($sf_mass[23] != 1))
							continue;
						if ((($_POST['zar'][9] == 1) || ($_POST['vzar'][9] == 1)) && ($sf_mass[24] != 1))
							continue;
						if ((($_POST['zar'][10] == 1) || ($_POST['vzar'][10] == 1)) && ($sf_mass[25] != 1))
							continue;
						if ((($_POST['zar'][11] == 1) || ($_POST['vzar'][11] == 1)) && ($sf_mass[26] != 1))
							continue;
					}
				}
				
				
				
				if (isset($_POST["mini_podbor"]) && ($_POST["mini_podbor"] == 1))
					$ress[$i++] = array('Ставка' => $a["min_percent"], 'id' => $a["id"], 'max_cre_time' => $a['max_cre_time'], 'max_credit' => $a['max_credit'], 'min_income' => $a['min_income'], 'pm' => $a['pm'], 'goal' => $a['goal'], 'max_percent' => $a["max_percent"], 'max_end_age_m' => $a['max_end_age_m'], 'max_end_age_w' => $a['max_end_age_w']);
				else
					$ress[$i++] = array('Наименование продукта' => $a["product_name"], 'Ставка' => $a["min_percent"], 'Банк' => $banks[$a["bank_name"]], 'id' => $a["id"], 'bailn' => $a['bailn'], 'guarantor' => $a['guarantor'], 'bail' => $a['bail'], 'max_cre_time' => $a['max_cre_time'], 'max_end_age_m' => $a['max_end_age_m'], 'max_end_age_w' => $a['max_end_age_w'], 'max_credit' => $a['max_credit'], 'min_income' => $a['min_income'], 'pm' => $a['pm'], 'goal' => $a['goal'], 'max_percent' => $a["max_percent"]);

			}
			$result->close();
		}

		print_r(json_encode($ress,JSON_UNESCAPED_UNICODE));
	}
	else
	{
		
		if ($nrt)
		{
			while($a = $result->fetch_array(MYSQLI_ASSOC))
			{
				$bank_name = $banks[$a["bank_name"]];
				$prod_name = $a["product_name"];
				echo "<b>$prod_name</b> от банка $bank_name;<br>";
			}
			$result->close();
		}
	}

}





function make_mass($tmp_str)
{
	$tmp_mass = preg_split("/\|/", $tmp_str);
	return $tmp_mass;
}

function make_str($tmp_arr)
{
	
	$sz = sizeof($tmp_arr);
	$tmp_ins = "'$sz";
	foreach ($tmp_arr as $vl)
		$tmp_ins .= "|$vl";
	return $tmp_ins."'";
}

function make_mask($num, $pos, $value)
{
	$str = "$num";
	for ($i=0; $i<$num; $i++)
		$str .= ($i==$pos) ? "|$value" : "|_";
	return $str;
}

function make_lmask($num, $arr, $name)
{
	$str='';
	$miss = true;
	for ($i=0; $i<$num; $i++)
	{
		if ($arr[$i] > 0)
			$miss = false;
		$str .= ($arr[$i] > 0) ? "|_" : "|0";
	}
	
	if ($miss)
		$str = '';
	
	if ($str !='')
		$str = " AND $name LIKE '$num$str'";
	return $str;
}

function make_2mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$str1 = '';
	$miss0 = true;
	$miss1 = true;
	if ($arr[0] > 0)
	{
		$miss0 = false;
		$str0 = "$name LIKE '$num|1|_'";
	}
	
	if ($arr[1] > 0)
	{
		$miss1 = false;
		$str1 = "$name LIKE '$num|_|1'";
	}
	
	if($miss0 && $miss1)
		$str = '';
	else if (!$miss0 && !$miss1)
		$str = " AND ($str0 OR $str1)";
	else if ($miss1)
		$str = " AND $str0";
	else
		$str = " AND $str1";

	return $str;
}

function make_3mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;
	
	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '3|1|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '3|_|1|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '3|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_4mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;
	
	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '4|1|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '4|_|1|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '4|_|_|1|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '4|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_actives_mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;
	
	if (($arr[0] == 0) && ($arr[1] == 0) && ($arr[2] == 0) && ($arr[3] == 0) && ($arr[4] == 0) && ($arr[5] == 0))
	{
		$str = " AND $name LIKE '6|0|0|0|0|0|0'";
		return $str;
	}
	
	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '6|1|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|1|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|1|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|1|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|_|1|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_5mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '5|1|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '5|_|1|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '5|_|_|1|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '5|_|_|_|1|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '5|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_6mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '6|1|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|1|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|1|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|1|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|_|1|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '6|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_7mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '7|1|_|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;		
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|1|_|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|_|1|_|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|_|_|1|_|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|_|_|_|1|_|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|_|_|_|_|1|_'";
	}
	
	if ($arr[6] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '7|_|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_8mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '8|1|_|_|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;		
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|1|_|_|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|1|_|_|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|_|1|_|_|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|_|_|1|_|_|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|_|_|_|1|_|_'";
	}
	
	if ($arr[6] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|_|_|_|_|1|_'";
	}
	
	if ($arr[7] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '8|_|_|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_13mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '13|1|_|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|1|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|1|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|1|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|1|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|1|_|_|_|_|_|_|_'";
	}
	
	if ($arr[6] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|1|_|_|_|_|_|_'";
	}
	
	if ($arr[7] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|1|_|_|_|_|_'";
	}
	
	if ($arr[8] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|_|1|_|_|_|_'";
	}
	
	if ($arr[9] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|_|_|1|_|_|_'";
	}
	
	if ($arr[10] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|_|_|_|1|_|_'";
	}
	
	if ($arr[11] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|_|_|_|_|1|_'";
	}
	
	if ($arr[12] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '13|_|_|_|_|_|_|_|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

function make_15mask($num, $arr, $name)
{
	$str = '';
	$str0 = '';
	$miss = true;

	$cc = 0;
	if ($arr[0] > 0)
	{
		$miss = false;
		$cc++;
		//$str0 = "$name LIKE '$num|1|_'";
		$str = "$name LIKE '15|1|_|_|_|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[1] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|1|_|_|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[2] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|1|_|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[3] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|1|_|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[4] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|1|_|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[5] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|1|_|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[6] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|1|_|_|_|_|_|_|_|_'";
	}
	
	if ($arr[7] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|1|_|_|_|_|_|_|_'";
	}
	
	if ($arr[8] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|1|_|_|_|_|_|_'";
	}
	
	if ($arr[9] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|1|_|_|_|_|_'";
	}
	
	if ($arr[10] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|_|1|_|_|_|_'";
	}
	
	if ($arr[11] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|_|_|1|_|_|_'";
	}
	
	if ($arr[12] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|_|_|_|1|_|_'";
	}
	
	if ($arr[13] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|_|_|_|_|1|_'";
	}
	
	if ($arr[14] > 0)
	{
		$cc++;
		$str0 = $miss ? "" : " OR ";
		$miss = false;
		$str .= "$str0$name LIKE '15|_|_|_|_|_|_|_|_|_|_|_|_|_|_|1'";
	}
	
	if($cc == 0)
		$str = '';
	else if ($cc == 1)
		$str = " AND $str";
	else
		$str = " AND ($str)";

	return $str;
}

?>