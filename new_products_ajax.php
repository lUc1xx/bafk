<?php
//print_r($_POST);
header('Content-Type: text/html; charset=UTF-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('SYSTEM_START_9876543210', true);

include_once "_bdc.php";
include_once "_functions.php";
include_once "_config.php";

$login_ = "";
$token_ = "";
if (!empty ($_COOKIE['login']))
	$login_ = $_COOKIE['login'];	//Добавить удаление ненужных символов
if (!empty ($_COOKIE['token']))
	$token_ = $_COOKIE['token'];	//Добавить удаление ненужных символов

if (!authorized($login_, $token_))
{
	mysqli_close($db_connect);
	$res['status'] = 'failed';
	$res['msg'] = "Access denied";
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
	//die("Access denied");
}

$be_type 					= $_POST["be_type"];
if ($be_type == 'del')
{
	$be_prod_id 				= $_POST["be_prod_id"];
	$sql_del = "DELETE FROM old_products WHERE id='$be_prod_id';";
	//echo $sql_del;	
	if ($result = $db_connect->query($sql_del))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	die();
}
else if ($be_type == 'read')
{
	//header('Content-type: application/json; charset=windows-1251');
	$be_prod_id 				= $_POST["be_prod_id"];

	if ($result = $db_connect->query("SELECT * FROM old_products WHERE id='$be_prod_id';"))
	{
			
		$a = $result->fetch_array(MYSQLI_ASSOC);
		print_r(php2js($a));
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

	die();
}

foreach ($_POST AS $k=>$v) {
    //$_POST[$k] = preg_replace(';amp;', '&', $v);
    $_POST[$k] = preg_replace_callback('/\;amp\;/',  function($match) { return '&'; }, $v);
}

$be_prod_id 				= $_POST["be_prod_id"];
$form["bank_name"] 			= $_POST["be_bank_name"];
$form["product_name"] 		= $_POST["be_product_name"];
$form["min_percent"] 		= $_POST["be_min_percent"];
$form["max_percent"] 		= $_POST["be_max_percent"];
$form["active"] 			= $_POST["be_active"];
$form["description"] 		= $_POST["be_description"];
$form["pd"] 				= 0 + $_POST["be_pd"];
$form["pd_set"] 			= 0 + $_POST["be_pd_set"];
$form["pd_set_data"] 		= $_POST["be_pd_set_data"];
$form["pd_miss"] 			= $_POST["be_pd_miss"];
$form["discont"] 			= 0 + $_POST["be_discont"];
$form["res"] 				= $_POST["be_res"];
$form["bank_zp"] 			= $_POST["be_bank_zp"];
$form["bank_acc"] 			= $_POST["be_bank_acc"];
$form["cr4other"] 			= $_POST["be_cr4other"];
$form["min_credit"] 		= 0 + $_POST["be_min_credit"];
$form["max_credit"] 		= 0 + $_POST["be_max_credit"];
$form["min_income"] 		= 0 + $_POST["be_min_income"];
$form["max_age_m"] 			= 0 + $_POST["be_max_age_m"];
$form["max_age_w"] 			= 0 + $_POST["be_max_age_w"];
$form["lim_time"] 			= 0 + $_POST["be_lim_time"];
$form["nbki"] 				= 0 + $_POST["be_nbki"];
$form["arm"] 				= $_POST["be_arm"];
//$form["adr"] 				= $_POST["be_adr"];
$form["adr_nan"] 			= 0 + $_POST["be_adr_nan"];
$form["adr_time"] 			= 0 + $_POST["be_adr_time"];
$form["work"] 				= $_POST["be_work"];
$form["c_work"] 			= $_POST["be_c_work"];
$form["c_inc"] 				= $_POST["be_c_inc"];
$form["inc_card"] 			= "";//$_POST["be_inc_card"];
$form["exp_now"]			= $_POST["be_exp_now"];
$form["reg_date"] 			= $_POST["be_reg_date"];
$form["work_type"] 			= $_POST["be_work_type"];
//$form["work_adr"] 			= $_POST["be_work_adr"];
$form["ent"] 				= $_POST["be_ent"];
$form["ca"] 				= $_POST["be_ca"];
$form["pfr"] 				= $_POST["be_pfr"];
$form["firm_per"] 			= 0 + $_POST["be_firm_per"];
$form["firm_per_max"] 			= 0 + $_POST["be_firm_per_max"];
$form["firm_per_min"] 			= 0 + $_POST["be_firm_per_min"];
$form["nal"] 				= $_POST["be_nal"];
$form["proc"] 				= 0 + $_POST["be_proc"];
$form["proc_max"] 			= 0 + $_POST["be_proc_max"];
$form["pr_nal"] 			= $_POST["be_pr_nal"];
$form["pr_4"] 				= 0 + $_POST["be_pr_4"];
$form["est"] 				= $_POST["be_est"];
$form["co"] 				= 0 + $_POST["be_co"];
$form["delay_29"] 			= $_POST["be_delay_29"];
$form["ps"] 				= $_POST["be_ps"];
$form["cr_his"] 			= $_POST["be_cr_his"];
$form["co_max"] 			= 0 + $_POST["be_co_max"];
$form["zaim_mfo"] 			= $_POST["be_zaim_mfo"];
$form["lim_all"] 			= 0 + $_POST["be_lim_all"];
$form["lim_val"] 			= 0 + $_POST["be_lim_val"];
$form["bal_now"] 			= 0 + $_POST["be_bal_now"];
$form["bal_val"] 			= 0 + $_POST["be_bal_val"];
$form["ps_c"] 				= $_POST["be_ps_c"];
$form["res_en"] 			= $_POST["be_res_en"];
$form["obr_14"] 			= 0 + $_POST["be_obr_14"];
$form["obr_mon"] 			= 0 + $_POST["be_obr_mon"];
$form["inc_true"] 			= $_POST["be_inc_true"];
$form["crim"] 				= "";//$_POST["be_crim"];
$form["mbank"] 				= "";//$_POST["be_mbank"];
$form["st_cr"] 				= $_POST["be_st_cr"];
$form["spark"] 				= $_POST["be_spark"];
$form["guarantor"] 			= $_POST["be_guarantor"];
$form["bail"] 				= $_POST["be_bail"];
$form["first_deb"] 			= 0 + $_POST["be_first_deb"];
$form["mkad"] 				= 0 + $_POST["be_mkad"];
$form["doc_sob"] 			= $_POST["be_doc_sob"];
$form["owner"] 				= $_POST["be_owner"];
$form["obj_type"] 			= $_POST["be_obj_type"];
$form["replan"] 			= $_POST["be_replan"];
$form["house_mat"] 			= $_POST["be_house_mat"];
$form["found"] 				= $_POST["be_found"];
$form["obj_age"] 			= 0 + $_POST["be_obj_age"];
$form["wear"] 				= 0 + $_POST["be_wear"];
$form["comm"] 				= $_POST["be_comm"];

$form["ps_v0"] 				= 0 + $_POST["be_ps_v0"];
$form["ps_v1"] 				= 0 + $_POST["be_ps_v1"];
$form["ps_v2"] 				= 0 + $_POST["be_ps_v2"];
$form["ps_v3"] 				= 0 + $_POST["be_ps_v3"];
$form["ps_v4"] 				= 0 + $_POST["be_ps_v4"];
$form["psa_c"] 				= $_POST["be_psa_c"];
$form["psa_v0"] 			= 0 + $_POST["be_psa_v0"];
$form["psa_v1"] 			= 0 + $_POST["be_psa_v1"];
$form["psa_v2"] 			= 0 + $_POST["be_psa_v2"];
$form["psa_v3"] 			= 0 + $_POST["be_psa_v3"];
$form["psa_v4"] 			= 0 + $_POST["be_psa_v4"];
$form["psc"] 				= $_POST["be_psc"];
$form["psc_u0"] 			= 0 + $_POST["be_psc_u0"];
$form["psc_u1"] 			= 0 + $_POST["be_psc_u1"];
$form["psc_u2"] 			= 0 + $_POST["be_psc_u2"];
$form["psc_u3"] 			= 0 + $_POST["be_psc_u3"];
$form["psc_u4"] 			= 0 + $_POST["be_psc_u4"];
$form["ps_u0"] 				= 0 + $_POST["be_ps_u0"];
$form["ps_u1"] 				= 0 + $_POST["be_ps_u1"];
$form["ps_u2"] 				= 0 + $_POST["be_ps_u2"];
$form["ps_u3"] 				= 0 + $_POST["be_ps_u3"];
$form["ps_u4"] 				= 0 + $_POST["be_ps_u4"];
$form["stop"] 				= $_POST["be_stop"];
$form["min_age_m"] 			= 0 + $_POST["be_min_age_m"];
$form["min_age_w"] 			= 0 + $_POST["be_min_age_w"];
$form["co_umax"] 			= 0 + $_POST["be_co_umax"];
$form["goal"] 				= $_POST["be_goal"];
$form["goalc"] 				= $_POST["be_goalc"];
$form["bailn"] 				= $_POST["be_bailn"];
$form["delay_act"] 			= $_POST["be_delay_act"];
$form["bh_type"] 			= $_POST["be_bh_type"];
$form["max_cre_time"] 		= 0 + $_POST["be_max_cre_time"];
$form["min_cre_time"] 		= 0 + $_POST["be_min_cre_time"];
$form["max_end_age_m"] 		= 0 + $_POST["be_max_end_age_m"];
$form["max_end_age_w"] 		= 0 + $_POST["be_max_end_age_w"];

$form["ps_6"] 				= $_POST["be_ps_6"];
$form["ps_6v0"] 			= 0 + $_POST["be_ps_6v0"];
$form["ps_6v1"] 			= 0 + $_POST["be_ps_6v1"];
$form["ps_6v2"] 			= 0 + $_POST["be_ps_6v2"];
$form["ps_6v3"] 			= 0 + $_POST["be_ps_6v3"];
$form["ps_6v4"] 			= 0 + $_POST["be_ps_6v4"];
$form["ps_12"] 				= $_POST["be_ps_12"];
$form["ps_12v0"] 			= 0 + $_POST["be_ps_12v0"];
$form["ps_12v1"] 			= 0 + $_POST["be_ps_12v1"];
$form["ps_12v2"] 			= 0 + $_POST["be_ps_12v2"];
$form["ps_12v3"] 			= 0 + $_POST["be_ps_12v3"];
$form["ps_12v4"] 			= 0 + $_POST["be_ps_12v4"];

$form["obr_7"] 				= 0 + $_POST["be_obr_7"];
$form["activs"] 			= $_POST["be_activs"];
$form["pm"] 				= 0 + $_POST["be_pm"];

$form["psct3"]				= $_POST["be_psct3"];
$form["psct6"]				= $_POST["be_psct6"];
$form["psct12"]				= $_POST["be_psct12"];
$form["psct18"]				= $_POST["be_psct18"];
$form["psct24"]				= $_POST["be_psct24"];
$form["psct36"]				= $_POST["be_psct36"];
$form["psct60"]				= $_POST["be_psct60"];
$form["psct60p"]			= $_POST["be_psct60p"];
$form["psctall"]			= $_POST["be_psctall"];

$form["psat3"]				= $_POST["be_psat3"];
$form["psat6"]				= $_POST["be_psat6"];
$form["psat12"]				= $_POST["be_psat12"];
$form["psat18"]				= $_POST["be_psat18"];
$form["psat24"]				= $_POST["be_psat24"];
$form["psat36"]				= $_POST["be_psat36"];
$form["psat60"]				= $_POST["be_psat60"];
$form["psat60p"]			= $_POST["be_psat60p"];
$form["psatall"]			= $_POST["be_psatall"];

$form["psct"]				= $_POST["be_psct"];
$form["psat"]				= $_POST["be_psat"];
$form["sf"]					= $_POST["be_sf"];

$form["bail_miss"]			= $_POST["be_bail_miss"];
$form["ul_miss"]			= $_POST["be_ul_miss"];

$form["exp_now_new"]		= 0 + $_POST["be_exp_now_new"];
$form["reg_date_new"] 		= 0 + $_POST["be_reg_date_new"];

/*$form["reg_all"] 			= 0 + $_POST["be_reg_all"];
$form["adr_all"] 			= 0 + $_POST["be_adr_all"];
$form["work_adr_all"] 		= 0 + $_POST["be_work_adr_all"];
*/
$form["reg_miss"] 			= 0 + $_POST["be_reg_miss"];
$form["adr_miss"] 			= 0 + $_POST["be_adr_miss"];
$form["wa_miss"] 			= 0 + $_POST["be_wa_miss"];


if (isset($_POST["be_params"]))
	$form["params"]				= $_POST["be_params"];

if (isset($_POST["adr_params"]))
	$form["adr_params"]				= $_POST["adr_params"];

if (isset($_POST["work_adr_params"]))
	$form["work_adr_params"]				= $_POST["work_adr_params"];

if (isset($_POST["be_fico"]))
	$form["fico"] 		= $_POST["be_fico"];

if (isset($_POST["be_online_av"]))
	$form["online_av"] 		= $_POST["be_online_av"];

if (isset($_POST["be_beznD"]))
	$form["beznD"] 		= $_POST["be_beznD"];



if (($be_type == "add") || ($be_type == "upd"))
{
	
	$upd = 0;
	if ($be_type == "upd")
		$upd = 1;
	//$sql_str = "INSERT INTO old_products VALUES(0"; 
	$sql_str = "INSERT INTO old_products("; 
	
	$str_keys = '';
	$str_vals = '';
	
	if ($upd)
		$sql_str = "UPDATE old_products SET ";
	
	
	
	
	foreach ($form as $key => $value)
	{
		$tmp_ins = "";
		if (gettype ($value) == "array")
		{
			$tmp_arr = $value;
			$sz = sizeof($tmp_arr);
			$tmp_ins .= "'$sz";
			foreach ($tmp_arr as $vl)
				$tmp_ins .= "|$vl";
			$tmp_ins .= "'";
		}
		else
			$tmp_ins .= "'$value'";

		//print_r($tmp_ins . " : " . $upd . "\n");
		//echo $upd;
		if ($upd)
		{
			$sql_str .= $key . " = " . $tmp_ins . ", ";
			
		}
		else
		{			
			//$sql_str .= ", " . $tmp_ins;
			if ($str_keys != '')				
				$str_keys .= ", ";
			$str_keys .= $key;
			if ($str_vals != '')				
				$str_vals .= ", ";
			$str_vals .= $tmp_ins;
		}
		
	}
	
	if ($upd)
	{
		//del , add WHERE id = '$be_pid';
		$sql_str .= ";";
		$sql_str = preg_replace("/, ;/", " WHERE id = '$be_prod_id'", $sql_str);
	}
	else
	{
		$sql_str = $sql_str . $str_keys . ") VALUES($str_vals);";
		//$sql_str .= ");";
	}
}
//print_r($_POST);
//print_r($sql_str);
//die();

//print_r($form);

if ($result = $db_connect->query($sql_str))
{
	if ($db_connect->affected_rows)
	{
		$res['status'] = 'ok';
	}
	else
	{
		$res['status'] = 'not need update';
		//$res['msg'] = $sql_str;
	}
	
	/*$fp = fopen(dirname(__FILE__) . "/../logs/prod_edit.log", "a+");
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= "\n staff_id " . print_r($staff_id,true);
	$data_ .= ", prod_id " . print_r($be_prod_id,true);
	$data_ .= "\n" . print_r(json_encode($form,JSON_UNESCAPED_UNICODE), true) . "\n\n";
	$test = fwrite($fp, $data_);
	fclose($fp);*/
}
else
{
	$res['status'] = 'failed';
	$res['sql'] = $sql_str;
	$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
}

//print_r($res);

$fp = fopen(LOGS_DIR . "prod_edit.log", "a+");
$data_ = date("Y-m-d H:i:s", time());
$data_ .= "\n staff_id " . print_r($staff_id,true);
$data_ .= ", prod_id " . print_r($be_prod_id,true);
$data_ .= ", res " . print_r(json_encode($res),true);
$data_ .= "\n" . print_r(json_encode($form,JSON_UNESCAPED_UNICODE), true) . "\n\n";
$test = fwrite($fp, $data_);
fclose($fp);

//print_r($res);
//print_r(json_encode($res,JSON_UNESCAPED_UNICODE));


function php2js($a=false)
{
    if (is_null($a) || is_resource($a)) {
        return 'null';
    }
    if ($a === false) {
        return 'false';
    }
    if ($a === true) {
        return 'true';
    }
    
    if (is_scalar($a)) {
        if (is_float($a)) {
            //Always use "." for floats.
            $a = str_replace(',', '.', strval($a));
        }

        // All scalars are converted to strings to avoid indeterminism.
        // PHP's "1" and 1 are equal for all PHP operators, but
        // JS's "1" and 1 are not. So if we pass "1" or 1 from the PHP backend,
        // we should get the same result in the JS frontend (string).
        // Character replacements for JSON.
        static $jsonReplaces = array(
            array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'),
            array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"')
        );

        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
    }

    $isList = true;

    for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
        if (key($a) !== $i) {
            $isList = false;
            break;
        }
    }

    $result = array();
    
    if ($isList) {
        foreach ($a as $v) {
            $result[] = php2js($v);
        }
    
        return '[ ' . join(', ', $result) . ' ]';
    } else {
        foreach ($a as $k => $v) {
            $result[] = php2js($k) . ': ' . php2js($v);
        }

        return '{ ' . join(', ', $result) . ' }';
    }
}




?>