<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('Europe/Moscow');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('SYSTEM_START_9876543210', true);
include_once "_config.php";
include_once "_bdc.php";
include_once "_functions.php";


/*if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit;
}*/

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") {
    $location = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit;
}

$login_ = "";
$token_ = "";
if (!empty ($_COOKIE['login']))
	//$login_ = $_COOKIE['login'];	//Добавить удаление ненужных символов
	$login_ = htmlspecialchars($_COOKIE['login'], ENT_QUOTES);	//Добавить удаление ненужных символов
if (!empty ($_COOKIE['token']))
	//$token_ = $_COOKIE['token'];	//Добавить удаление ненужных символов
	$token_ = htmlspecialchars($_COOKIE['token'], ENT_QUOTES);	//Добавить удаление ненужных символов

$old_req = '/';
if (isset($_SERVER['HTTP_REFERER']))
	$old_req = $_SERVER['HTTP_REFERER'];

if (!authorized($login_, $token_))
{
	$_login = "";
	$_passwd = "";
	if (!empty($_POST['action']) && $_POST['action'] == 'login')
	{
		if (!empty($_POST['login']))
			//$_login = $_POST['login'];		//Добавить удаление ненужных символов
			$_login = htmlspecialchars($_POST['login'], ENT_QUOTES);		//Добавить удаление ненужных символов
		if (!empty($_POST['passwd']))
			//$_passwd = $_POST['passwd'];	//Добавить удаление ненужных символов
			$_passwd = htmlspecialchars($_POST['passwd'], ENT_QUOTES);	//Добавить удаление ненужных символов
	}
	if (!authorize($_login, $_passwd))
	{
		include('login/login.php');
		mysqli_close($db_connect);
		exit;
	}
	print_r($_SERVER["REQUEST_URI"]);

	//header("Location: /list/");
	//header("Location: ".$_SERVER["REQUEST_URI"]);
	header("Location: " . $old_req);
}
//print_r($_REQUEST);
//print_r($_SERVER["REQUEST_URI"]);

$_page = 'worktable';
if (isset($_GET['sep']))
{
	switch($_GET['sep'])
	{
		case "create" : 
		case "list" :
		case "list_fa" :
		case "signals" :
		case "pipeline" :
		case "list2" :
		case "listsotr" :
		case "listmetrika" :
		case "pf" :
		case "mstat" :
		case "agents" :
		case "staff" : 
		case "asettings" :  
		case "filters" : 
		case "services" : 
		case "checks" : 
		case "bankrequests" : 
		case "bankrequests2" : 
		case "banks" : 
		case "details" : 
		case "details2" : 
		case "prb" : 
		case "files" : 
		case "audio" : 
		case "photo" : 
		case "metriks" : 
		case "analitics" :
		case "testpush" :
		case "bpmn" : 
		case "settings" : $_page = $_GET['sep']; break;
		default : $_page = 404;
	}
}
$_id = 'miss';
if (isset($_GET['id']))
{
	$_id = $_GET['id'];
}

if ($_page == 'files')
{
	include("files.php");
	die();
}
else if ($_page == 'audio')
{
	include("audio.php");
	die();
}
else if ($_page == 'photo')
{
	include("photo.php");
	die();
}
else if ($_page == 'details2')
{
	include("anketa_data.php");
	include('crm/anketa/index.html');
	die();
}
else if ($_page == 'list2')
{
	//include("anketa_data.php");
	include('crm/list/index.html');
	die();
}

$staff_position_debug = $staff_position;
$staff_id_debug = $staff_id;
?>
<!DOCTYPE html>
<?
//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 //        "http://www.w3.org/TR/html4/loose.dtd">
if ($_page != 'bpmn') {
    ?>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
    <head>
    <title><?= $system_name; ?>::<?= $staff_login; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="/scripts/jquery-3.5.0.min.js<? echo '?' . rand(); ?>"></script>
    <script type="text/javascript" src="/scripts/work.js<? echo '?' . rand(); ?>"></script>

    <script type="text/javascript" src="/scripts/datepicker.js<? echo '?' . rand(); ?>"></script>
    <link href="/css/datepicker.css<? echo "?" . rand(); ?>" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/scripts/script.js<? echo '?' . rand(); ?>"></script>
    <link href="/css/style.css<? echo "?" . rand(); ?>" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/css/left-nav-style.css<? echo "?" . rand(); ?>" type="text/css">
    <link rel="stylesheet" href="/css/fa_all.css<? echo "?" . rand(); ?>" type="text/css">
<?

//    <script src="/scripts/firebase/firebase-app.js"></script>
//    <script src="/scripts/firebase/firebase-messaging.js"></script>
//    <script src="/scripts/firebase/init.js"></script>
//    <script src="/scripts/firebase/firebase_subscribe.js"></script>

?>
    <script src="/plugins/chartlist/chartist.min.js"></script>
    <link rel="stylesheet" href="/plugins/chartlist/chartist.min.css" type="text/css">

    <?
    if (true) {
        ?>
<link href="/plugins/vuesax.min.css" rel="stylesheet">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <?
    }
}


udpate_staff_onilne($staff_id);

if (!empty ($_COOKIE['debug']))
{
	if ($_COOKIE['debug'] == 1)
	{
		$debug_id = 0 + $_COOKIE['debug_id'];
		
		if ($debug_id == $staff_id_debug)
		{
			setcookie('debug', 0, time()-3600*120, '/');
			setcookie('debug_id', 0, time()-3600*120, '/');
		}
		else
		{
			if($result2 = mysqli_query($db_connect, "SELECT * FROM staff WHERE id='$debug_id' AND deleted = '0' ORDER BY id DESC LIMIT 1"))
			{
				if (mysqli_num_rows($result2))
				{
					$check = mysqli_fetch_assoc($result2);
					
					$staff_id = $debug_id;
					$staff_login = $check['login'];
					$staff_office = $check['office'];
					$staff_position = $check['position'];
					$staff_lastname = $check['lastname'];
					$staff_firstname = $check['firstname'];
					$staff_email_corp = $check['email_corp'];
					$staff_phone_work = $check['phone_work'];
					$staff_status = $check['status'];
					$staff_za = $check['fin_an'];
					$staff_pc = $check['plan_check'];
					$staff_bc = $check['staff_bc'];
				}
				mysqli_free_result($result2);
			}
			else
			{
				echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
				
				return false;
			}
		}
	}
}

//<link rel="stylesheet" href="/css/style_menu.css" type="text/css">
if (!check_access($_page))
	die("Access denied!");

if ($_page != 'bpmn') {
    ?>
    <script type="text/javascript">
        var staff_id_js = <?=$staff_id;?>;
        var staff_id_debug = <?=$staff_id_debug;?>;
        var may_see_audio_js = false;
    </script>
    <?
}

$dbListUsers = '[]';

$db_filter = '';



$error = false;
if ($result = $db_connect->query("SELECT type FROM offices WHERE id='$staff_office';"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$staff_office_type = $row['type'];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if ($staff_office_type != 'main') {
	$db_filter = "AND o.type = '$staff_office_type'";
}

if ($result = $db_connect->query("SELECT st.lastname FROM staff st INNER JOIN offices o ON o.id=st.office WHERE st.deleted != '1' $db_filter;"))
{
	$dbListUsers = '[';
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		if ($dbListUsers != '[')
			$dbListUsers .= ', ';
		$dbListUsers .= $row['lastname'];
	}
	$result->close();

	$dbListUsers = $dbListUsers . ']';
}


if ($_page == 'list')
{
	echo '<link href="/css/list.css?' . rand() . '" rel="stylesheet" type="text/css">';
	
	//if ($staff_office_type == 'tm')
	if (($staff_position == 'manager_tm') || ($staff_position == 'manager_ozs'))
		echo '<script type="text/javascript" src="/scripts/list_tm.js?' . rand() . '"></script>';
	else	
		echo '<script type="text/javascript" src="/scripts/list.js?' . rand() . '"></script>';
	
}
else if ($_page == 'list_fa')
{
	echo '<script type="text/javascript" src="/scripts/list_fa.js?' . rand() . '"></script>';
	echo '<link href="/css/list.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'signals')
{
	echo '<script type="text/javascript" src="/scripts/signals.js?' . rand() . '"></script>';
	echo '<link href="/css/signals.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'pipeline')
{
	echo '<script type="text/javascript" src="/scripts/pipeline.js?' . rand() . '"></script>';
	echo '<link href="/css/pipeline.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'listsotr')
{
	echo '<script type="text/javascript" src="/scripts/listsotr.js?' . rand() . '"></script>';
	echo '<link href="/css/listsotr.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'listmetrika')
{
	echo '<script type="text/javascript" src="/scripts/listmetrika.js?' . rand() . '"></script>';
	echo '<link href="/css/listmetrika.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'mstat')
{
	echo '<script type="text/javascript" src="/scripts/mstat.js?' . rand() . '"></script>';
	echo '<link href="/css/mstat.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'asettings')
{
	echo '<script type="text/javascript" src="/scripts/asettings.js?' . rand() . '"></script>';
	echo '<link href="/css/asettings.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'staff')
{
	echo '<script type="text/javascript" src="/scripts/staff.js?' . rand() . '"></script>';
	echo '<link href="/css/staff.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'agents')
{
	echo '<script type="text/javascript" src="/scripts/agents.js?' . rand() . '"></script>';
	echo '<link href="/css/agents.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'create')
{
	echo '<link href="/css/anketa.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo '<link href="/css/spin.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo '<script type="text/javascript" src="/scripts/spin.js?' . rand() . '"></script>';
	echo '<script type="text/javascript" src="/scripts/cr_anketa.js?' . rand() . '"></script>';
	
}
else if ($_page == 'details')
{
	//if ($staff_id == '1')
		//echo '<script type="text/javascript" src="/scripts/anketa2.js?' . rand() . '"></script>';
	//else
	if ($staff_office_type == 'broker')
		echo '<script type="text/javascript" src="/scripts/anketa_broker.js?' . rand() . '"></script>';
	else
		echo '<script type="text/javascript" src="/scripts/anketa.js?' . rand() . '"></script>';
	echo '<link href="/css/anketa.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo "<script type=\"text/javascript\">var anketa_id=$_id;</script>";
}
else if ($_page == 'bankrequests')
{
	echo '<script type="text/javascript" src="/scripts/bankrequests.js?' . rand() . '"></script>';
	echo '<link href="/css/bankrequests.css?' . rand() . '" rel="stylesheet" type="text/css">';
	
	//echo '<script type="text/javascript" src="/plugins/multiselect-master/scripts/multiselect.js?' . rand() . '"></script>';
	//echo '<link href="/plugins/multiselect-master/styles/multiselect.css?' . rand() . '" rel="stylesheet" type="text/css">';
	
	//echo '<script type="text/javascript" src="/plugins/bootstrap-multi/bootstrap-multiselect.js?' . rand() . '"></script>';
	//echo '<link href="/plugins/bootstrap-multi/bootstrap-multiselect.css?' . rand() . '" rel="stylesheet" type="text/css">';
	
	/*echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>';
	echo '<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>';

	echo '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>';
	echo '<link rel="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>';
	*/
	
	
	/*echo '<script type="text/javascript" src="/plugins/bs/bootstrap-3.3.2.min.js?' . rand() . '"></script>';
	echo '<link href="/plugins/bs/bootstrap-3.3.2.min.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo '<script type="text/javascript" src="/plugins/bs/bootstrap-multiselect.js?' . rand() . '"></script>';
	echo '<link href="/plugins/bs/bootstrap-multiselect.css?' . rand() . '" rel="stylesheet" type="text/css">';
	*/
	
	//echo '<link href="/plugins/MDB-Free_4.8.1/css/bootstrap.min.css?' . rand() . '" rel="stylesheet" type="text/css">';
	//echo '<link href="/plugins/MDB-Free_4.8.1/css/mdb.css?' . rand() . '" rel="stylesheet" type="text/css">';
	//echo '<link href="/plugins/MDB-Free_4.8.1/css/style.css?' . rand() . '" rel="stylesheet" type="text/css">';
	//echo '<script type="text/javascript" src="/plugins/MDB-Free_4.8.1/js/bootstrap.min.js?' . rand() . '"></script>';
	//echo '<script type="text/javascript" src="/plugins/MDB-Free_4.8.1/js/mdb.js?' . rand() . '"></script>';
}
else if ($_page == 'bankrequests2')
{
	echo '<script type="text/javascript" src="/scripts/bankrequests2.js?' . rand() . '"></script>';
	echo '<link href="/css/bankrequests2.css?' . rand() . '" rel="stylesheet" type="text/css">';
}
else if ($_page == 'banks')
{
//	echo '<script type="text/javascript" src="/scripts/anketa.js?' . rand() . '"></script>';
//	echo '<link href="/css/anketa.css?' . rand() . '" rel="stylesheet" type="text/css">';
//	echo "<script type=\"text/javascript\">var anketa_id=$_id;</script>";
	echo '<script type="text/javascript" src="/products.js?' . mt_rand() . '"></script>';
	//echo '<link rel="stylesheet" type="text/css" href="_control.css">';
	echo '<link rel="stylesheet" type="text/css" href="/products.css?' . mt_rand() . '">';
	/*
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/quasar.min.css">
	*/
}
else if ($_page == 'checks')
{
}
else if ($_page == 'services')
{
}
else if ($_page == 'settings')
{
	echo '<script type="text/javascript" src="/scripts/settings.js?' . rand() . '"></script>';
	echo '<link href="/css/settings.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo '<link href="/plugins/CLEditor1_4_5/jquery.cleditor.css?' . rand() . '" rel="stylesheet" type="text/css">';
	echo '<script type="text/javascript" src="/plugins/CLEditor1_4_5/jquery.cleditor.js?' . rand() . '"></script>';
}
else if ($_page == 'prb')
{
	echo '<script type="text/javascript" src="/products3.js?' . rand() . '"></script>';

	echo '<link rel="stylesheet" type="text/css" href="/products_m.css?' . rand() . '">';
}
else if (($_page == 'metriks') || ($_page == 'pf') || ($_page == 'analitics'))
{

	//<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" type="text/css">
	//<?
	//echo '<link rel="stylesheet" type="text/css" href="/css/quasar.min.css">';
}
else if ($_page == 'testpush')
{
}
else if ($_page == 'bpmn')
{
    include('bpmn/index.php');
    die;
}
else
{
//	echo '<link rel="stylesheet" type="text/css" href="/css/fullcalendar.css?' . rand() . '">';
	echo '<link rel="stylesheet" type="text/css" href="/css/worktable.css?' . rand() . '">';
//	echo '<link rel="stylesheet" type="text/css" href="/css/jquery.qtip.css?' . rand() . '">';
//	echo '<script type="text/javascript" src="/scripts/moment.min.js?' . rand() . '"></script>';
//	echo '<script type="text/javascript" src="/scripts/fullcalendar.js?' . rand() . '"></script>';
//	echo '<script type="text/javascript" src="/scripts/ru.js?' . rand() . '"></script>';
	echo '<script type="text/javascript" src="/scripts/worktable.js?' . rand() . '"></script>';
//	echo '<script type="text/javascript" src="/scripts/jquery.qtip.js?' . rand() . '"></script>';
}

echo '<link rel="stylesheet" type="text/css" href="/css/jquery.qtip.css?' . rand() . '">';
echo '<script type="text/javascript" src="/scripts/jquery.qtip.js?' . rand() . '"></script>';
echo '<link rel="stylesheet" type="text/css" href="/css/fullcalendar.css?' . rand() . '">';
echo '<script type="text/javascript" src="/scripts/moment.min.js?' . rand() . '"></script>';
echo '<script type="text/javascript" src="/scripts/fullcalendar.js?' . rand() . '"></script>';
echo '<script type="text/javascript" src="/scripts/ru.js?' . rand() . '"></script>';


?>
    <style>
        .phone_forwarded {
            margin-top: 10px;
            margin-left: -16px;
            z-index: 9999999;
            padding:5px;
            width: 12px; /* Ширина кнопки */
            height: 12px; /* Высота кнопки */
            border: none; /* Убираем рамку */
            color: green;
            cursor:pointer;
            background: url(/img/call.png) no-repeat; /* Параметры фона */
            background-size: 20px 20px;
            outline: none;
        }
    </style>
<?



$offices_mass = array();
$staff_mass = array();

if (!$error)
{
	if ($result = $db_connect->query("SELECT * FROM offices WHERE deleted='0' ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$offices_mass[$row['id']] = $row;
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}

if (!$error)
{
	if ($result = $db_connect->query("SELECT * FROM staff ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$staff_mass[$row['id']] = $row;
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

}

echo "<script>var staff_office_type = '$staff_office_type';\n";
echo "var staff_position = '$staff_position';</script>\n";


if (isset($staff_id) && $staff_id == 1) {
    ?>
    <script type="text/javascript" src="/scripts/jquery.tabslideout.v1.2.js?' . rand() . '"></script>
    <script type="text/javascript">
        $(function(){
            $('.panel').tabSlideOut({							//Класс панели
                tabHandle: '.handle',						//Класс кнопки
                pathToTabImage: '/img/button.gif',				//Путь к изображению кнопки
                imageHeight: '122px',						//Высота кнопки
                imageWidth: '40px',						//Ширина кнопки
                tabLocation: 'right',						//Расположение панели top - выдвигается сверху, right - выдвигается справа, bottom - выдвигается снизу, left - выдвигается слева
                speed: 300,								//Скорость анимации
                action: 'click',								//Метод показа click - выдвигается по клику на кнопку, hover - выдвигается при наведении курсора
                topPos: '200px',							//Отступ сверху
                fixedPosition: false						//Позиционирование блока false - position: absolute, true - position: fixed
            });
        });
    </script>
<?
}
?>

<!--Admin LTE files-->
<?
    echo '<link rel="stylesheet" type="text/css" href="/css/admin_lte/bootstrap.min.css">';
    echo '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">';
    echo '<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">';
    echo '<link rel="stylesheet" type="text/css" href="/css/admin_lte/AdminLTE.min.css">';
    echo '<link rel="stylesheet" type="text/css" href="/css/admin_lte/_all-skins.min.css">';
    echo '<script type="text/javascript" src="/scripts/admin_lte/app.min.js"></script>';
?>

</head>
<!--<body id="body" style="font-size:80%; line-height:1em; font-family:Verdana, Arial, Helvetica, sans-serif">-->
<body id="body" class="skin-blue sidebar-mini">
<div class="wrapper">
<?
//echo '<script type="text/javascript" src="/scripts/vue.min.js"></script>';
//echo '<script type="text/javascript" src="/scripts/quasar.umd.min.js"></script>';
//echo '<script type="text/javascript" src="/scripts/ru.umd.min.js"></script>';
//echo '<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" type="text/css">';

//echo '<link rel="stylesheet" type="text/css" href="/css/quasar.min.css">';

include('header.php');

echo '<div id="chart_sms" class="chart" style="display:none;"></div>';
echo '<div id="chart_prom" class="chart" style="display:none;"></div>';
echo '<div id="chart_ya" class="chart" style="display:none;"></div>';
echo '<div id="chart_tel" class="chart" style="display:none;"></div>';

?>
<? //include('menu.php'); ?>
<? include('main.php'); ?>

<?
//if ($staff_id == 1) {
if (false) {
?>
    <script src="/scripts/popper.min.js"></script>
<!--    <link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <script src="/scripts/bootstrap.min.js"></script>
<?
}

if (($_page == 'metriks') || ($_page == 'pf') || ($_page == 'analitics')) {
    echo '<script type="text/javascript" src="/scripts/vue.min.js"></script>';
    echo '<script type="text/javascript" src="/scripts/quasar.umd.min.js"></script>';
    echo '<script type="text/javascript" src="/scripts/ru.umd.min.js"></script>';
    echo '<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" type="text/css">';

    echo '<link rel="stylesheet" type="text/css" href="/css/quasar.min.css">';
}
//else if (false) {
else if (($staff_office_type == 'main' || $staff_position == 'director_tm' || $staff_position == 'director_ozs') && ($_page == 'worktable')) {
//    echo '<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>';
    //echo '<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>';
    //echo '<script src="https://unpkg.com/tooltip.js@1.3.3/dist/umd/tooltip.min.js"></script>';
    //echo '<script src="/scripts/popper.min.js"></script>';
//    echo '<link rel="stylesheet" href="/css/bootstrap.min.css">';
    echo '<script src="/scripts/bootstrap.min.js"></script>';
}

if (true) {
    ?>


        <?
}
?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
</div>
</body>
</html>
<?php
mysqli_close($db_connect);
?>