<?php

echo '<div id="anketa_data_wrap">';
?>
<br>
<div class="grid">
    <vs-row >

        <vs-col offset="1" w="5">
            <vs-input
            label="Название"
            v-model="cName"
            placeholder="Название"
            />
        </vs-col>
        <vs-col offset="1" w="5">
            <vs-input
                    label="Имя"
                    v-model="name"
                    placeholder="Имя"
            />
        </vs-col>
    </vs-row>
<br><br>
    <vs-row>
        <vs-col offset="1" w="5">
            <vs-input
                label="Телефон"
                v-model="phone"
                placeholder="Телефон"
            />
        </vs-col>
        <vs-col offset="1" w="5">
            <vs-input
                    label="Email"
                    v-model="email"
                    placeholder="Email"
            />
        </vs-col>
    </vs-row><br><br>
    <vs-row >
        <vs-col offset="1" w="5">
            <textarea
            label = "test">

            </textarea>
        </vs-col>

        <vs-col offset="1" w="3">
            <vs-switch v-model="type">
                <template #off>
                    Другая организация
                </template>
                <template #on>
                    Банк
                </template>
            </vs-switch>
        </vs-col>
    </vs-row>
</div>
<?php
echo '</div>';
echo '<div id="log_wrap">';
echo '<div id="log_control">';
echo '<div class="lc_div" id="log_msg"   onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Сообщения<span></span></div>';
echo '<div class="lc_div" id="log_calls" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Звонки<span></span></div>';
echo '<div class="lc_div" id="log_robot" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Системные<span></span></div>';
echo '<div class="lc_div" id="log_sms"   onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Смс<span></span></div>';
echo '<div class="lc_div" id="log_other" onclick="javascript:ch_log_filter(this);"><input type="checkbox" onclick="javascript:ch_log_filter(this.parentNode);">Другие<span></span></div>';
echo '</div>';
echo '<div id="log_window"></div>';
echo '<div id="input_window"><textarea></textarea>';
echo '<label><input type="checkbox" name="toagent" id="msgtoagent" value="1">Сообщение Агенту</label>';
echo '<button id="msg_button" onclick="javascript:send_msg2log();">Отправить</button></div>';
echo "</div>";

?>

<script src="/plugins/vue.js"></script>
<script src="/plugins/vuesax.min.js"></script>

<script>
    new Vue({
        el: '#main',
        data :() => ({
            sel: [],
            name: '',
            cName: '',
            type: true
        }),
        methods: {
            test : function () {
                alert(this.sel);
                console.log(this.sel);
            },
            filterActivesVue: function() {
                var t = document.querySelector('#list_table');
                var r = t.querySelectorAll('.row_odd, .row_even');
                //console.log(r.length);

                for (let key=0; key<r.length; key++)
                {
                    var d = r[key].querySelectorAll('.list_cell');

                    let show = this.sel.length ? false : true;
                    for (let v of this.sel) {
                        let filter = '.*';
                        if (v == 'Без активов') {
                            filter = `data-tooltip="Нет активов"`;
                        }
                        else if (v == 'Не указаны') {
                            filter = `data-tooltip="Не указаны активы"`;
                        }
                        else {
                            filter = `data-tooltip="${v}"`;
                        }
                        let reg = new RegExp(filter, "ig");
                        show = show || reg.test(d[2].innerHTML);
                    }

                    if (show)
                        r[key].style.display = "table-row";
                    else
                        r[key].style.display = "none";
                }
            }


        }
    })
</script>
