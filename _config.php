<?php
if (!defined('SYSTEM_START_9876543210')) exit;

// define('HOME_DIR', '/home/admin/web/test-bafk.tmweb.ru/');
// define('HOME_DIR_OLD', '/home/admin/web/test-bafk.tmweb.ru/');
define('HOME_DIR', 'test-bafk.local');
define('HOME_DIR_OLD', 'test-bafk.local');
define('PUBLIC_DIR', HOME_DIR.'public_html/');
define('TMP_DIR', HOME_DIR.'tmp/');
define('SERVICES_DIR', HOME_DIR.'public_html/services/');
define('FORMS_DIR', HOME_DIR.'public_html/forms/');
define('FORMS_DIR2', HOME_DIR.'public_html/forms');
define('LOGS_DIR', HOME_DIR.'logs/');
define('FILES_DIR', HOME_DIR_OLD.'public_html/');

define('BROKER_OFFICE1', 10);

$system_name = "Брокер";

$work_position = array(
	'super-admin'		=> 'Админ',
	'admin'				=> 'Администратор',
	'secretary'			=> 'Секретарь',
    'director_tm'       => 'Руководитель ТМ',
	'manager_tm'		=> 'Менеджер ТМ',
	'dismissed'			=> 'Уволен(а)',
	'director_ozs'		=> 'Руководитель ОЗС',
	'manager_ozs'		=> 'Менеджер ОЗС'
);

$status_description = array(
	'new'				=> 'Новая',
	'new_in_tm'			=> 'Новая в ТМ',
	'in_tm_office'		=> 'Поступила в ТМ',
	'primary_work'		=> 'Первичная обработка ТМ',
	'secondary_work'	=> 'Вторичная обработка ТМ',
	'assigned_meeting'	=> 'Назначена встреча',
	'contract_signed'	=> 'Договор с клиентом подписан',
	'new_in_ozs'		=> 'Новая в ОЗС',
	'processing_ozs'	=> 'Обработка в ОЗС',
	'pre_approved'		=> 'Предодобрен',
	'final_analysis'	=> 'Заключительный анализ',
	'final_a_end'		=> 'ЗА завершен',
	'ready2bank'		=> 'Готов к подаче в банк',
	'banks_review'		=> 'Рассмотрение банками',
	'send2bank'			=> 'Заявка направлена в банк',
	'credit_approved'	=> 'Кредит одобрен',
	'credit_declined'	=> 'Кредит отклонен',
	'loan_issued'		=> 'Кредит выдан',
	'work_complete'		=> 'Работа с заявкой завершена'
);

$status_num_descr = array(
	'new'				=> 0,
	'new_in_tm'			=> 1,
	'in_tm_office'		=> 2,
	'primary_work'		=> 3,
	'secondary_work'	=> 4,
	'assigned_meeting'	=> 5,
	'contract_signed'	=> 6,
	'new_in_ozs'		=> 7,
	'processing_ozs'	=> 8,
	'pre_approved'		=> 8.5,
	'final_analysis'	=> 9,
	'final_a_end'		=> 10,
	'ready2bank'		=> 11,
	'banks_review'		=> 12,
	'send2bank'			=> 13,
	'credit_approved'	=> 14,
	'credit_declined'	=> 15,
	'loan_issued'		=> 16,
	'work_complete'		=> 17
);

$field_type_desc = array(
	'input'				=> 'Поле',
    'select'      		=> 'Список',
	'checkbox'			=> 'Галочка',
	'radio'				=> 'Переключатель',
	'multiselect'		=> 'Мультисписок',
	'date_field'		=> 'Дата',
	'telephone'			=> 'Телефон',
	'addr'				=> 'Адрес',
	'block'				=> 'Блок',
	'numbers'			=> 'Только цифры',
	'reg'				=> 'Регион',
	'banks'				=> 'Банки',
	'kred_prod'			=> 'Кр. продукты'
);

$file_type_desc = array(
	'passport_scan'		=> 'Скан паспорта',
    'passport_reg'      => 'Скан прописки',
	'agree_scan'		=> 'Скан согласия',
	'passport_all'		=> 'Паспорт (полный)',
	'other'				=> 'Другой',
	'photo'				=> 'Фото',
	'report'			=> 'Отчет',
	'tk'				=> 'Копия ТК',
	'dohod'				=> 'Справка о доходе',
	'2ndfl'				=> '2-НДФЛ',
	'declar'			=> 'Декларация',
	'rs'				=> 'Выписка по р/с',
	'ls'				=> 'Выписка по л/с',
	'vdk'				=> 'ВДК',
	'td'				=> 'Трудовой Договор',
	'company_card'		=> 'Карточка компании',
	'verification'		=> 'Файл верификации'
);

$promoney_class_desc = array(
	'CWICreditHistoryIndividual'			=> 'НБКИ-отчет для ФЛ',
	'CWICreditRatingIndividual'			 	=> 'Кредитный рейтинг для ФЛ',
	'CWIOkbReportPrivate'			 		=> 'Аналитический отчет',
	'CWICreditHistoryPrivate'			 	=> 'Кредитная история физического лица НБКИ',
    'CWICreditRatingPrivate'			 	=> 'Кредитный рейтинг',
    'CWICreditHistoryLegal'			 		=> 'Кредитная история юридического лица НБКИ',
    'CWINbkiReportPrivate'			 		=> 'НБКИ-отчет',
    'CWISecurityPrivate'			 		=> 'Проверка СБ по ФЛ',
    'CWIBusinessLegal'			 			=> 'Бизнес-справка',
    'CWIEgrulLegal'			 				=> 'ЕГРЮЛ',
    'CWICkkiPrivate'			 			=> 'ЦККИ на физическое лицо',
    'CWICkkiLegal'			 				=> 'ЦККИ на юридическое лицо',
    'CWIScorringPrivate'			 		=> 'Скорринг FICO на физическое лицо',
    'CWIEquifaxCreditReportPrivateAuto'		=> 'Кредитный отчет Equifax',
    'CWINbkiFicoReportPrivate'			 	=> 'НБКИ+FICO-отчёт',
    'CWINbkiCarPledge'			 			=> 'Автозалог НБКИ',
    'CWICreditHistoryRsbPrivate'			=> 'КИ Русский Стандарт по ФЛ',
    'CWISpark'			 					=> 'СПАРК'
);

$WORK_TEL = '84997035207';
$WORK_ADDRESS = 'м.Серпуховская, ул.Павловская, д 6. (Вход со стороны ул.Большая Серпуховская напротив дома 60)';

/*
Аккаунт:	
info@business-smart.space
Пароль:	
Ld1NqJmkm4
IMAP hostname:	
business-smart.space
IMAP port:	
143
IMAP security:	
STARTTLS
IMAP auth method:	
Normal password
SMTP hostname:	
business-smart.space
SMTP port:	
587
SMTP security:	
STARTTLS
SMTP auth method:	
Normal password
Webmail URL:	
http://188.225.27.111/webmail/
*/
?>
