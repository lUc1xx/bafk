<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

insert_into_action_log('list', $staff_id_debug);

if ($result = $db_connect->query("SHOW COLUMNS FROM forms WHERE Field = 'status';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$status_list = $out[1];
	$result->close();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "0: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

//print_r($status_list);

echo '<vs-row><vs-col w="6">Фильтры: с <input id="dfrom" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d", mktime(0, 0, 0, date('m')-6, date('d'), date('Y'))) . '"> по <input id="dto" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d") . '">';

echo ' <button onclick="javascript:filterApply();">Применить фильтр</button>';

$fav_view = isset($_COOKIE['fav_view']) ? $_COOKIE['fav_view'] : 0;
$f0 = $fav_view == 0 ? "selected" : "";
$f1 = $fav_view == 1 ? "selected" : "";
$f2 = $fav_view == 2 ? "selected" : "";
echo ' Показывать избранные: <select id="fav_view" onchange="javascript:sel_favorite_view(this);">';
echo '<option value="0" ' . $f0 . '>По умолчанию</option>';
echo '<option value="1" ' . $f1 . '>Избранное вверху</option>';
echo '<option value="2" ' . $f2 . '>Только избранные</option>';
echo '</select>';



if (true) {
    ?>
</vs-col>
    <vs-col w="6">
    <vs-select
            placeholder="Активы (заполнено участником сделки)"
            multiple
            filter
            v-model="sel"
            v-on:change="filterActivesVue"
            width="300px"
    >
        <vs-option label="Без активов" value="Без активов">
            Без активов
        </vs-option>
        <vs-option label="Не указаны" value="Не указаны">
            Не указаны
        </vs-option>
        <vs-option label="Иномарка" value="Иномарка">
            Иномарка
        </vs-option>
        <vs-option label="Квартира" value="Квартира">
            Квартира
        </vs-option>
        <vs-option label="Дом" value="Дом">
            Дом
        </vs-option>
    </vs-select>
    </vs-col>
    <?
}
if (false) {
	?>
	<select class="js-example-basic-multiple" name="actives[]" multiple="multiple">
		<option value="none">Без активов</option>
		<option value="unknown">Не указаны</option>
		<option value="ino">Иномарка</option>
		<option value="apart">Квартира</option>
		<option value="house">Дом</option>
	</select>

	<script>

		let selectedActives = new Array();
		$(document).ready(function() {
			$('.js-example-basic-multiple').select2({
				placeholder: "Активы (заполнено участником сделки)",
			});
			$('.js-example-basic-multiple').on('select2:select', function (e) {
				selectedActives.push(e.params.data.text);
				filterActives();
			});
			$('.js-example-basic-multiple').on('select2:unselect', function (e) {
				selectedActives.splice(selectedActives.indexOf(e.params.data.text));
				filterActives();
			});
		});

		function filterActives() {
			var t = document.querySelector('#list_table');
			var r = t.querySelectorAll('.row_odd, .row_even');
			//console.log(r.length);

			for (let key=0; key<r.length; key++)
			{
				var d = r[key].querySelectorAll('.list_cell');

				let show = selectedActives.length ? false : true;
				for (let v of selectedActives) {
					let filter = '.*';
					if (v == 'Без активов') {
						filter = `data-tooltip="Нет активов"`;
					}
					else if (v == 'Не указаны') {
						filter = `data-tooltip="Не указаны активы"`;
					}
					else {
						filter = `data-tooltip="${v}"`;
					}
					let reg = new RegExp(filter, "ig");
					show = show || reg.test(d[2].innerHTML);
				}

				if (show)
					r[key].style.display = "table-row";
				else
					r[key].style.display = "none";
			}
		}
	</script>
	<style>
		.js-example-basic-multiple {
			width: 350px;
		}
	</style>
<?php
}

echo '<vs-row>';
echo '<div id="list_table"><div class="row_head">';
echo '<div class="list_cell"><div onclick="javascript:sort_list(0);" class = "sort_d" style="cursor: pointer; border-bottom: 1px dashed #000080; width:50px; float:right; padding-right:15px;">Дата</div></div>';
//echo '<div class="list_cell">№ анкеты</div>';
echo '<div class="list_cell"><input type="text" placeholder="№ анк" oninput="javascript:num_filter(this.value)"></div>';
echo '<div class="list_cell"><span onclick="javascript:sort_list(2);" style="cursor: pointer; border-bottom: 1px dashed #000080; width:50px; float:left;">FICO</span> <input type="text" placeholder="ФИО" oninput="javascript:fio_filter(this.value)"></div>';
echo '<div class="list_cell" style="text-align:right; padding-right:10px;">Сумма</div>';
echo '<div class="list_cell"><input type="text" placeholder="Город" oninput="javascript:sity_filter(this.value)"></div>';
echo '<div class="list_cell" id="source_set">Источник</div>';
//echo '<div class="list_cell">from description</div>';
//<progress value="0" max="' + prt_count + '">Показать</progress>
echo '<div class="list_cell">';
echo '<select id="select_status" onchange="javascript:filterApply();">';
echo '<option value="-1">Все статусы</option>';
echo '<option value="in_work" selected>В работе</option>';
foreach ($status_list as $val)
{
    if ($val == 'delete')
        continue;
	echo "<option value=\"$val\">$status_description[$val]</option>";
}
echo '</select><progress id="progress_bar1" style="display:none; width:200px; height:20px;" value="0" max="100"> </progress></div>';

$hide_of = '';
//if ($staff_office_type != 'main')
	//$hide_of = ' style="display:none;"';
	//$hide_of = ' style="width:0px;"';
echo '<div class="list_cell"' . $hide_of . '>';
echo '<select id="select_office" onchange="javascript:ch_list_stat(this);">';
echo '<option value="-1">Все офисы</option>';
if ($result = $db_connect->query("SELECT id, name FROM offices WHERE type != 'main' AND deleted='0' ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$id = $row['id'];
		$name = $row['name'];
		echo "<option value=\"$id\">$name</option>";
	}
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
echo '</select></div>';
$hide_st = '';
//if (($staff_office_type != 'main') && ($staff_position != 'director_tm') && ($staff_position != 'director_ozs'))
//	$hide_st = ' style="display:none;"';
	//$hide_st = ' style="width:0px;"';
echo '<div class="list_cell"' . $hide_st . '>';
echo '<select id="select_staff" onchange="javascript:filterApply();">';
echo '<option value="-1">Все сотрудники</option>';
if ($result = $db_connect->query("SELECT id, lastname, firstname, patronymic, office FROM staff WHERE deleted = '0' AND position != 'admin' AND position != 'super-admin' ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		if ($staff_office_type != 'main')
		{
			if (($staff_position == 'director_tm') || ($staff_position == 'director_ozs'))
			{
				if ($staff_office != $row['office'])
					continue;
			}
			else
				continue;
		}
		$id = $row['id'];
		$fio_a = $row['lastname'];
		if ($row['firstname'] != '')
			$fio_a .= ' ' . substr($row['firstname'], 0, 2) . '.';
		if ($row['patronymic'] != '')
			$fio_a .= ' ' . substr($row['patronymic'], 0, 2) . '.';
		$of = $row['office'];
		echo "<option class=\"office_$of\" value=\"$id\">$fio_a</option>";
	}
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
echo '</select></div>';
echo '<div class="list_cell"><span onclick="javascript:sort_list(9);" style="cursor: pointer; border-bottom: 1px dashed #000080; width:150px; float:left;">Ближайшее событие</span></div>';
echo '</div></div>';

echo '<div id="itogo_forms"><span>Всего анкет:<span id="itogo_all_forms"></span></span></div>';

//echo '<div id="list_table"></div>';
?>

<script src="/plugins/vue.js"></script>
<script src="/plugins/vuesax.min.js"></script>

<script>
    new Vue({
        el: '#main',
        data :() => ({
            sel: []
        }),
        methods: {
            test : function () {
                alert(this.sel);
                console.log(this.sel);
            },
            filterActivesVue: function() {
                var t = document.querySelector('#list_table');
                var r = t.querySelectorAll('.row_odd, .row_even');
                //console.log(r.length);

                for (let key=0; key<r.length; key++)
                {
                    var d = r[key].querySelectorAll('.list_cell');

                    let show = this.sel.length ? false : true;
                    for (let v of this.sel) {
                        let filter = '.*';
                        if (v == 'Без активов') {
                            filter = `data-tooltip="Нет активов"`;
                        }
                        else if (v == 'Не указаны') {
                            filter = `data-tooltip="Не указаны активы"`;
                        }
                        else {
                            filter = `data-tooltip="${v}"`;
                        }
                        let reg = new RegExp(filter, "ig");
                        show = show || reg.test(d[2].innerHTML);
                    }

                    if (show)
                        r[key].style.display = "table-row";
                    else
                        r[key].style.display = "none";
                }
            }


        }
    })
</script>

