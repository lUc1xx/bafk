<?php
if (!defined('SYSTEM_START_9876543210')) exit;  
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Требуется авторизация</title>
	<link rel="stylesheet" href="/login/css/style.css<?echo "?" . rand();?>" media="screen" type="text/css" />
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>

    <div id="login-form">
        <h1>Авторизация в системе</h1>
<?
if ($login_msg != '')
	echo '<font color="red">' . $login_msg . '</font>';
?>
        <fieldset>
            <form action="/" method="post">
				<input type="hidden" name="action" value="login">
                <input type="text" name="login" required value="" placeHolder="Логин" onBlur="if(this.value=='')this.placeHolder='Логин'" onFocus="this.placeHolder='' ">
                <input type="password" name="passwd" required value="" placeHolder="Пароль">
                <input type="submit" value="ВОЙТИ">
                <footer class="clearfix">
                    <p><span class="info">?</span><a href="#">Забыли пароль?</a></p>
                </footer>
            </form>
        </fieldset>

    </div>

<div style="display: none">
    <?
    print_r($_SERVER);
    ?>
</div>
</body>
</html>