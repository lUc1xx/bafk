<?php
if (!defined('SYSTEM_START_9876543210')) exit;

function sendFormTo($formid, $type, $val, $man = array()) {
	global $db_connect, $staff_id, $staff_id_debug;
	
	if ($type == 'sotrud') {
        $sql = "UPDATE forms SET type='cooperate', status='new', office='0', manager='0', last_action=NULL, last_action_time=NOW(), close_nums='0' WHERE id='$formid';";
        $result = $db_connect->query($sql);

/*        $fp = fopen("test1.log", "a+");
        $data_ = date("Y-m-d H:i:s", time());
        $data_ .= print_r($sql, true) . "\n";
        $data_ .= print_r($result, true) . "\n";
        $test = fwrite($fp, $data_);
        fclose($fp);
*/
        $system_msg = "Передана в сотрудничество ";
        $sql = "INSERT INTO form_log(form_id, date, type, subtype, msg) VALUES ('$formid', NOW(), 'robot', 'new_status', '$system_msg');";
        $result = $db_connect->query($sql);
	    return;
    }

	$of_num = ($type == 'of') ? $val : $man['office'];
	
	
	if ($result = $db_connect->query("SELECT type, name FROM offices WHERE id = '$of_num';"))
	{
		$row = $result->fetch_array(MYSQLI_ASSOC);
		$of_type = $row['type'];
		$of_name = $row['name'];
		$result->close();
	}
	else return;
	
	
	$sql = '';
	$st_fil = '';
	$mwr = '';
	if ($type == 'of') {
		
		if (($of_type == 'tm') || ($of_type == 'broker') )
			$st_fil = "status='in_tm_office', last_status_time=NOW(), wait_man='0',";
		else if ($of_type == 'ozs')
			$st_fil = "status='new_in_ozs', last_status_time=NOW(),";
			
		$sql = "UPDATE forms SET $st_fil office='$of_num', manager='0', last_action=NULL, last_action_time=NOW(), close_nums='1' WHERE id='$formid';";
		$result = $db_connect->query($sql);
		
		$system_msg = "Передана в офис $val";
		$sql = "INSERT INTO form_log(form_id, date, type, subtype, msg) VALUES ('$formid', NOW(), 'robot', 'new_status', '$system_msg');";
		$result = $db_connect->query($sql);
	}
	else if ($type == 'man') {
		
		if (($of_type == 'tm') || ($of_type == 'broker') )
			$st_fil = "status='primary_work', last_status_time=NOW(), wait_man='0',";
		else if ($of_type == 'ozs')
			$st_fil = "status='processing_ozs', last_status_time=NOW(),";
		
		$m_id = $man['id'];
		
		if ($of_type == 'tm')
			$mwr = ", tm_man='$m_id'";
		else if ($of_type == 'ozs')
			$mwr = ", ozs_man='$m_id'";
		
		
			
		//$sql = "UPDATE forms SET $st_fil office='$of_num', manager='0', last_action=NULL, last_action_time=NOW(), close_nums='1' WHERE id='$formid';";
		$sql = "UPDATE forms SET $st_fil office='$of_num', wait_man='0', manager='$m_id'$mwr, close_nums='1', last_action=NULL, last_action_time=(NOW() - INTERVAL 1 SECOND) WHERE id='$formid';";
		$result = $db_connect->query($sql);
		
		
		$fio = $man['lastname'] . ' ' . $man['firstname'] . ' ' . $man['patronymic'];
		$system_msg = "Передана менеджеру $fio в офис $of_name";
		$sql = "INSERT INTO form_log(form_id, date, type, subtype, msg) VALUES ('$formid', NOW(), 'robot', 'new_status', '$system_msg');";
		$result = $db_connect->query($sql);
	}
} 

function sendSms($formid, $msg_) {
	global $db_connect;
	$tel = '';
	
	$sql = "SELECT contact_phone FROM forms WHERE id='$formid';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$tel = $row['contact_phone'];
		}
	}
	
	include_once SERVICES_DIR . "sms_serv.php";
	if ($sms = new MY_SMS($formid, 0, $db_connect, $sms_config))
		$resultsms = json_decode($sms->sendSMS($tel, $msg_));
	
	$fp = fopen("../logs/sms.log", "a+");
	$data_ = date("Y-m-d H:i:s", time());
	$data_ .= " => смс при закрытии => " . print_r($formid, true) . " => " . print_r($tel, true) . " => " . substr(strip_tags($msg_), 0, 300) . "\n";
	$test = fwrite($fp, $data_);
	fclose($fp);
	

}

function authorized($login, $token)
{
	global $staff_id, $staff_login, $staff_office, $staff_position, $staff_firstname, $staff_lastname, $staff_email_corp, $staff_phone_work, $staff_status, $staff_za, $staff_pc, $staff_bc;
	$check = check_token($login, $token);
	
	if (is_array($check))
	{
		if($check['status'])
		{
			$staff_id = $check['staff_id'];
			$staff_login = $check['staff_login'];
			$staff_office = $check['staff_office'];
			$staff_position = $check['staff_position'];
			$staff_lastname = $check['lastname'];
			$staff_firstname = $check['firstname'];
			$staff_email_corp = $check['email_corp'];
			$staff_phone_work = $check['phone_work'];
			$staff_status = $check['status'];
			$staff_za = $check['for_za'];
			$staff_pc = $check['plan_check'];
			$staff_bc = $check['staff_bc'];
			return true;
		}
		else
		{
			/*echo "<pre>";
			print_r ($check);
			echo "</pre>";*/
			return false;
		}
	}
	else
		return false;
}

function send_push_new_form($stid, $form_id)
{
    return;
	global $db_connect;
	$data = array('action' => 'new_form',
				'form_id' => $form_id,
				'title' => 'Новая заявка',
				'body' => 'Поступила новая заявка!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	$Firebase->send_msg($stid, $data);
}

function send_push_wp_confirm($form_id)
{
    return;
	global $db_connect;
	$fail = false;
	$stid = '';
	$sql = "SELECT manager FROM forms WHERE id='$form_id';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$val = $result->fetch_array(MYSQLI_ASSOC);
			$stid = $val['manager'];
		}
		else
			$fail = true;
	}
	else
		$fail = true;
	
	if ($fail)
		return;
	
	$data = array('action' => 'wp_conf',
				'form_id' => $form_id,
				'title' => 'Изменения в анкете',
				'body' => 'План работ подтвержден!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	$Firebase->send_msg($stid, $data);
}

function send_push_wp_reject($form_id)
{
    return;
	global $db_connect;
	$fail = false;
	$stid = '';
	$sql = "SELECT manager FROM forms WHERE id='$form_id';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$val = $result->fetch_array(MYSQLI_ASSOC);
			$stid = $val['manager'];
		}
		else
			$fail = true;
	}
	else
		$fail = true;
	
	if ($fail)
		return;
	$data = array('action' => 'wp_reject',
				'form_id' => $form_id,
				'title' => 'Изменения в анкете',
				'body' => 'План работ отклонен!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	$Firebase->send_msg($stid, $data);
}

function send_push_za_end($form_id, $st)
{
    return;
	global $db_connect;
	$fail = false;
	$stid = '';
	$sql = "SELECT manager FROM forms WHERE id='$form_id';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$val = $result->fetch_array(MYSQLI_ASSOC);
			$stid = $val['manager'];
		}
		else
			$fail = true;
	}
	else
		$fail = true;
	
	if ($fail)
		return;
	$data = array('action' => 'za_end',
				'form_id' => $form_id,
				'st' => $st,
				'title' => 'Изменения в анкете',
				'body' => 'Заключительный анализ завершен' . $st . '!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	$Firebase->send_msg($stid, $data);
}

function send_push_za($form_id)
{
    return;
	global $db_connect;
	$fail = false;
	$stid_mass = array();
	$sql = "SELECT id FROM staff WHERE fin_an='1';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			$stid_mass[] = $val['id'];
		}
		else
			$fail = true;
	}
	else
		$fail = true;
	
	if ($fail)
		return;

	$data = array('action' => 'za_start',
				'form_id' => $form_id,
				'title' => 'Изменения в анкете',
				'body' => 'Анкета направлена на ЗА!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	
	foreach ($stid_mass as $stid)
	{
		$Firebase->send_msg($stid, $data);
	}
	
}

function send_push_wp($form_id)
{
    return;
	global $db_connect;
	$fail = false;
	$stid_mass = array();
	$sql = "SELECT id FROM staff WHERE plan_check='1';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while($val = $result->fetch_array(MYSQLI_ASSOC))
			$stid_mass[] = $val['id'];
		}
		else
			$fail = true;
	}
	else
		$fail = true;
	
	if ($fail)
		return;

	$data = array('action' => 'wpc_start',
				'form_id' => $form_id,
				'title' => 'Изменения в анкете',
				'body' => 'Анкета направлена на проверку плана работ!'
			);
	include_once SERVICES_DIR . "cFirebase.php";

	$Firebase = new cFirebase($db_connect);
	
	foreach ($stid_mass as $stid)
	{
		$Firebase->send_msg($stid, $data);
	}
	
}

function send_offer2site($fid, $url)
{
	global $db_connect;
	
	$error = false;
	
	if ($result = $db_connect->query("SELECT * FROM client_offers WHERE url = '$url' AND form_id = '$fid';"))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$offer = $row['offer'];
			$url = $row['url'];
			$date_set = $row['date_set'];
		}
		else
			$error = true;
		$result->close();
	}
	else
		$error = true;
	
	if ($error)
		return 'error0';
	
	$error = false;
	
	if ($result = $db_connect->query("SELECT manager FROM forms WHERE id = '$fid';"))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$man = $row['manager'];
		}
		else
			$error = true;
		$result->close();
	}
	else
		$error = true;
	
	if ($error)
		return 'error1';
	
	if ($result = $db_connect->query("SELECT * FROM staff WHERE id = '$man';"))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$phone_work = $row['phone_work'];
			$email_corp = $row['email_corp'];
			$stid = $row['id'];
		}
		else
			$error = true;
		$result->close();
	}
	else
		$error = true;
	
	if ($error)
	{
		return "error2 SELECT * FROM staff WHERE id = '$man';";
	}
		
	
	if ($result = $db_connect->query("SELECT firstname FROM forms WHERE id = '$fid';"))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$name = $row['firstname'];
		}
		else
			$error = true;
		$result->close();
	}
	else
		$error = true;
	
	if ($error)
		return 'error3';

	
	//$site = 'http://www.alekskh.tmweb.ru/offer/get_command.php';
	$site = 'http://prb.bafk.ru/get_command.php';
	//$site = 'http://prb.finanskredit.ru/get_command.php';
	$key = "1234";
	
	$params['offer'] = $offer;
	$params['url'] = $url;
	$params['date_set'] = $date_set;
	$params['name'] = $name;
	$params['firstname'] = $firstname;
	$params['lastname'] = $lastname;
	$params['phone_work'] = $phone_work;
	$params['email_corp'] = $email_corp;
	$params['stid'] = $stid;
	$params['staff_position'] = 'Старший эксперт отдела кредитования';
	
	$post_data = "&" . http_build_query($params);
	
	
	ksort ($params);
	$sum=$key;
	foreach ($params as $k=>$v)
	{
		$sum.= $v;
	}
	$control_sum =  md5($sum);
	
	//return "key=$key&command=add_offer&sum=$control_sum" . $post_data;
	
	
	if( $curl = curl_init() ) 
	{
		curl_setopt($curl, CURLOPT_URL, $site);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION,false);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, "key=$key&command=add_offer&sum=$control_sum" . $post_data);

			if(false === ($out = curl_exec($curl))) {
			return 11;
		}
		curl_close($curl);
		$res = json_decode($out);

//		switch($this->act)
		
		return $out;
	}
}

function insert_into_action_log($type, $staff, $d = array())
{
	global $db_connect;
	
	switch($type)
	{
		case "list":
		case "login":
		case "logout":
			$sql = "INSERT INTO system_action_log(date, staff, type) VALUES(NOW(), '$staff', '$type');";
			break;
		case "open":
			$formid = 0 + $d['formid'];
			$sql = "INSERT INTO system_action_log(date, staff, type, formid) VALUES(NOW(), '$staff', '$type', '$formid');";
			break;
		case "formedit":
			$formid = 0 + $d['formid'];
			$subtype = $d['subtype'];
			$data = $d['data'];
			$sql = "INSERT INTO system_action_log(date, staff, type, subtype, formid, data) VALUES(NOW(), '$staff', '$type', '$subtype', '$formid', '$data');";
			break;
		case "settings":
			//break;
		case "banks":
			//break;
		default:
			return "-1";
			break;
	}
	$error = false;
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
			$error = true;
	}
	
	if (!$error)
	{
		$fp = fopen(dirname(__FILE__) . "/../logs/system_action_log_error.log", "a+");
		$data_ = date("Y-m-d H:i:s", time());
		$data_ .= " type " . print_r($type,true);
		$data_ .= " staff " . print_r($staff,true);
		$data_ .= "\n" . print_r($d,true) . "\n\n";
		$test = fwrite($fp, $data_);
		fclose($fp);
	}
}

function generateRandomString($length = 8)
{
  //$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
  //$chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_!*';
  //$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_?@!$*';
  //$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@!$*';
  $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

function logout($id, $login, $token)
{
	global $db_connect;
	$sql = "UPDATE staff SET online=0, last_action_time=NOW() WHERE id='$id'";
	$l = false;
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$l = true;
		}
		else
		{
			$l = false;
		}
		//$result->close();
	}
	
	if ($l)
	{
		$sql = "UPDATE authorized SET status=0, edate=NOW() WHERE login='$login' AND token='$token';";

		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
			{
				$l = true;
			}
			else
			{
				$l = false;
			}
			//$result->close();
		}
	}
	if ($l)
		insert_into_action_log('logout', $id);
	
	return $l;
}

function authorize($_login, $_passwd)
{
	global $login_msg, $staff_id, $staff_login, $staff_office, $staff_position, $staff_firstname, $staff_lastname, $staff_email_corp, $staff_phone_work, $staff_status, $staff_za, $staff_pc, $staff_bc;
	
	if (!$_login or !$_passwd) return false;
	
	$check = check_pass($_login, $_passwd);
	
	if (is_array($check))
	{
		if($check['status'])
		{
			$staff_id = $check['staff_id'];
			$staff_login = $check['staff_login'];
			$staff_office = $check['staff_office'];
			$staff_position = $check['staff_position'];
			$staff_lastname = $check['lastname'];
			$staff_firstname = $check['firstname'];
			$staff_email_corp = $check['email_corp'];
			$staff_phone_work = $check['phone_work'];
			$staff_status = $check['status'];
			$staff_za = $check['for_za'];
			$staff_pc = $check['plan_check'];
			$staff_bc = $check['staff_bc'];
			
			if ($check['ipmass'] == '')
				$check1 = true;
			else
			{
				$ipmass = explode(';;;', $check['ipmass']);
				
				$check1 = in_array(get_ip(), $ipmass);
			}
			if (!$check1)
			{
				$login_msg = "Запрещенный IP для данного логина!";
				return false;
			}
			
			$hash = start_new_session($staff_id, $staff_login, get_ip(), $_SERVER['HTTP_USER_AGENT']);
			if (!$hash)
				return false;
			setcookie('login', $staff_login, time()+3600*12, '/');
			setcookie('token', $hash, time()+3600*12, '/');
			
			
			insert_into_action_log('login', $staff_id);
			return true;
		}
		else
		{
			//echo "<pre>";
			//print_r ($check);
			$login_msg = $check['error'];
			//echo "</pre>";
			return false;
		}
	}
	else
		return false;
	
	/*	
	if ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		//echo "<br>testa2";
		$id_ 		= $row['id'];
    	$name_ 		= $row['name'];
		$login_		= $row['login'];
    	$office_	= $row['office'];
    	$firm_		= $row['company'];
		$status_ 	= $row['status'];
    	$hash = md5(rand(0, PHP_INT_MAX));
		$result = mysql_query("UPDATE staff SET token = '$hash' WHERE id = $id_") or die("Invalid query: " . mysql_error()); 
	
		setcookie('login', $login_, time()+3600*12, '/');
		setcookie('token', $hash, time()+3600*12, '/');
		return true;
	}
	return false;
	*/
}

function addman2history($aid, $man, $office)
{
	global $db_connect;
	/*$office = '';
	$sql = "SELECT staff.office AS office FROM staff LEFT JOIN offices ON staff.office=offices.id WHERE staff.id = '$man';";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$val = $result->fetch_array(MYSQLI_ASSOC);
			$office = $val['office'];
			$hit = true;
		}
	}*/
	$date = date("Y-m-d H:i:s");
	$data = "$date|$man|$office";
	$sql = "UPDATE forms SET man_history=CONCAT(COALESCE(man_history, ''),';$data;') WHERE id='$aid';";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
		return false;
}

function asend2office($office)
{
	global $db_connect;
	$list1 = array();
	$sql = "SELECT id, status FROM forms WHERE status='new' AND type='mini';";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($list1, $row['id']);
		}
		$result->close();
	}
	else
	{
		return -1;
	}
	
	foreach($list1 as $id)
	{
		$sql = "UPDATE forms SET office='$office', status='in_tm_office', last_status_time=NOW() WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
			//echo "OK";
		}
		else
		{
			return -1;
		}
        insertIntoFormStatusLog($id, 'in_tm_office', '0');
	}
	return "OK";
}

function asend2OZSoffice($office)
{
	global $db_connect;
	$list1 = array();
	$sql = "SELECT id, status FROM forms WHERE status='new_in_ozs' AND type='mini' AND office='0';";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($list1, $row['id']);
		}
		$result->close();
	}
	else
	{
		return -1;
	}
	
	foreach($list1 as $id)
	{
		$sql = "UPDATE forms SET office='$office' WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
			//echo "OK";
		}
		else
		{
			return -1;
		}
	}
	return "OK";
}

function check_office_work($office)
{
	global $db_connect;
	//$staff_list = array();
	$forms_list = array();
	/*$sql = "SELECT id, lastname, position, deleted, office, status FROM staff WHERE deleted='0' AND id !='19' AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			if ($row['id'] == '19')
				continue;
			array_push($staff_list, $row);
		}
		$result->close();
	}
	else
	{
		return -1;
	}*/
	
	$tdiff = date("Y-m-d H:i:s", mktime(date('H'), date('i')-2, date('s'), date('m'), date('d'), date('Y')));
	$sql = "SELECT id, date_add, type, status, last_status_time, last_action, last_action_time, office, wait_man, manager FROM forms WHERE status='primary_work' AND last_status_time<'$tdiff' AND call_event IS NULL AND last_action IS NULL AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			//if ($row['manager'] == '19')
				//continue;
			if (0 + $row['wait_man'] == 1)
				array_push($forms_list, $row['id']);
		}
		$result->close();
	}
	else
	{
		return -1;
	}
	
	$tdiff = date("Y-m-d H:i:s", mktime(date('H'), date('i')-30, date('s'), date('m'), date('d'), date('Y')));
	$sql = "SELECT forms.id AS id, date_add, type, forms.status AS status, manager, wait_man, staff.id AS staff_id, staff.online AS online FROM forms LEFT JOIN staff ON staff.id=forms.manager WHERE forms.status='primary_work' AND date_add<'$tdiff' AND call_event IS NULL AND last_action IS NULL AND forms.office='$office';";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			if ((0 + $row['online'] == 0) && (0 + $row['wait_man'] == 1))
			{
				if (!in_array($row['id'], $forms_list))
					array_push($forms_list, $row['id']);
			}
		}
		$result->close();
	}
	else
	{
		return -1;
	}

	
	
	foreach($forms_list as $row)
	{
		//$id = $row['id'];
		$id = $row;
		$sql = "UPDATE forms SET office='$office', manager='0', status='in_tm_office', wait_man='0', last_status_time=NOW() WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
			//echo "OK";
		}
		else
		{
			return -1;
		}
        insertIntoFormStatusLog($id, 'in_tm_office', '0');
		
		/*$msg = "send2TMoffice=2;;;new_status=in_tm_office;;;reason=inactivity";
		$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$id', NOW(), 'robot', '$msg');";
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
			{
				$res['status'] = 'ok';
				//$res['msg'] = $sql;
			}
			else
			{
				$res['status'] = 'failed';
				//$res['msg'] = $sql;
				return -1;
			}
		}
		else
		{
			$res['status'] = 'failed';
			//$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			return -1;
		}*/
		
	}
	$res['status'] = "OK";
	$res['data'] = $forms_list;
	return $res;
}

function insertIntoFormStatusLog($id, $status, $who) {
    global $db_connect;
    $sql = "INSERT INTO log_form_status_change(formid, date, status, who) VALUES('$id', NOW(), '$status', '$who')";
    if ($result = $db_connect->query($sql))
    {
        if (!$db_connect->affected_rows)
            return -1;
    }
    else
        return -1;

    if ($status == 'work_complete')
        return 0;

    $sql = "UPDATE forms SET last_status = '$status' WHERE id = '$id'";
    if ($result = $db_connect->query($sql))
    {
        if (!$db_connect->affected_rows)
            return -1;
    }
    else
        return -1;

    return 0;
}

function update_last_action($id, $action)
{
	global $db_connect;
	
	if ($action == '')
		$action = "NULL";
	else
		$action = "'$action'";
	
	$sql = "UPDATE forms SET last_action=$action, wait_man='0', last_action_time=NOW() WHERE id='$id';";
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
			return -1;
	}
	else
		return -1;

	return 0;
}

function assign_form2ozs_($office)
{
	global $db_connect;
	$data = array();
	$data0 = array();
	$data0_keys = array();
	$data1 = array();
	$data1_keys = array();
	$list1 = array();
	$forms = array();
	$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($list1, $row);
		}
		
		$result->close();
		
		//print_r($list1);
		
		foreach($list1 as $key => $val)
		{
			$man = $val['id'];
			$sql1 = "SELECT COUNT(id) AS cc FROM forms WHERE status!='work_complete' AND manager='$man' ORDER BY id ASC;";
			if ($result1 = $db_connect->query($sql1))
			{
				$row = $result1->fetch_array(MYSQLI_ASSOC);
				$list1[$key]['count_f'] = $row['cc'];
				$list1[$key]['count_all_f'] = 0;
				
				if ($list1[$key]['count_f'] < 1)
				{
					//array_push($data0, $list1[$key]);
					$data0[$list1[$key]['id']] = $list1[$key];
					$data0_keys[] = $list1[$key]['id'];
				}
				//else if ($list1[$key]['count_f'] < 2)
				else if ($list1[$key]['count_f'] > 0)
				{
					//array_push($data1, $list1[$key]);
					$data1[$list1[$key]['id']] = $list1[$key];
					$data1_keys[] = $list1[$key]['id'];
				}
				$result1->close();
			}
			else
			{
				return -1;
			}
		}
		
		//print_r($data0);
		//print_r($data1);
		
		//print_r($data0);
		if (count($data0) > 0)
		{
			if (count($data0) == 1)
			{
				//return $data0;
				$manager = array(current($data0));
			}
			else
			{
				//return array(current($data0));
				//$manager = array(current($data0));
				//print_r($data0_keys);
				$key = mt_rand(0, count($data0_keys) - 1);
				$manager = array($data0[$data0_keys[$key]]);
			}
		}
		else if (count($data1) > 0)
		{
			if (count($data1) == 1)
			{
				//return $data1;
				$manager = array(current($data1));
			}
			else
			{
				//return array(current($data1));
				//$manager = array(current($data1));
				$key = mt_rand(0, count($data1_keys) - 1);
				$manager = array($data1[$data1_keys[$key]]);
			}
		}
		else
			return 0;
	}
	else
	{
		return -1;
	}

	//print_r($manager);
	$sql = "SELECT id, sum, date_add, lastname, firstname, middlename FROM forms WHERE type='mini' AND status='new_in_ozs' AND office='$office' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$manager[] = $row;
		}
		else
		{
			return 2;
			$result->close();
		}
		$result->close();
	}
	else
	{
		//$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		//echo $res;
		return -1;
	}
	//return $forms;
	
	//print_r($manager);
	
	$man = $manager[0]['id'];
	$ozs_man = $manager[0]['id'];
	$id = $manager[1]['id'];
	$sql = "UPDATE forms SET manager='$man', ozs_man='$ozs_man', status='processing_ozs', last_status_time=NOW() WHERE id='$id';";
	//echo $sql;
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
		{
			return -1;
		}
	}
	else
	{
		//$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		//echo $res;
		return -1;
	}

    insertIntoFormStatusLog($id, 'processing_ozs', '0');

	$msg = "send2manager=$ozs_man;;;new_status=processing_ozs";
	$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$id', NOW(), 'robot', '$msg');";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			$res['msg'] = $sql;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	
	return $manager;
}

function assign_form2ozs($office)
{
	global $db_connect;
	$data = array();
	$data0 = array();
	$data0_keys = array();
	$data1 = array();
	$data1_keys = array();
	$list1 = array();
	$list2 = array();
	$man = -1;
	$man_num = 0;
	$forms = array();
	//$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND office='$office' ORDER BY id ASC;";
	$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND online='1' AND office='$office' AND status='ready' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		$man_num = $result->num_rows;
		if ($man_num)
		{			
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				array_push($list1, $row);
				$list2[$row['id']] = 0;
			}
		}
		$result->close();
	}
	else
	{
		return -1;
	}
	
	if (!$man_num)
		return "miss staff";
	
	$form = array();
	
	$sql = "SELECT id, sum, date_add, lastname, firstname, middlename FROM forms WHERE type='mini' AND status='new_in_ozs' AND office='$office' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$form = $result->fetch_array(MYSQLI_ASSOC);
		}
		else
		{
			$result->close();
			return  "miss forms";
		}
		$result->close();
	}
	else
		return -2;

	$limit = $man_num;
	
	$sql = "SELECT office, staffid FROM form_autosort WHERE type='ozs' AND office='$office' ORDER BY id DESC LIMIT $limit;";
		
	//print_r($list1);
	if ($result = $db_connect->query($sql))
	{
		if($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				if (array_key_exists($row['staffid'], $list2))
					$list2[$row['staffid']]++;
			}
			
			$min_val = 99999999;
			$min_id = 0;
			
			foreach($list2 as $k=>$v)
			{
				if ($v < $min_val)
				{
					$min_val = $v;
					$min_id = $k;
				}
				else if ($v == $min_val)
				{
					if (mt_rand(1, 100) > 50)
					{
						$min_val = $v;
						$min_id = $k;
					}
				}
			}
			$man = $min_id;
		}
		else
		{
			$sel_man = mt_rand(0, count($list1) - 1);
		}
		$result->close();
	}
	else
	{
		return -3;
	}
	
	//print_r($list2);
	
	
	$formid = $form['id'];
	$man = ($man == -1) ? $list1[$sel_man]['id'] : $man;
	
	//echo $man . "\n";
	//die();
	
	$sql = "INSERT INTO form_autosort(type, formid, datesort, office, staffid) VALUES('ozs', '$formid', NOW(), '$office', '$man');";
	$db_connect->query($sql);
	
	$sql = "UPDATE forms SET manager='$man', ozs_man='$man', status='processing_ozs', last_status_time=NOW() WHERE id='$formid';";
	//echo $sql;
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
		{
			return -4;
		}
		send_push_new_form($man, $formid);
	}
	else
	{
		//$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		//echo $res;
		return -4;
	}
    insertIntoFormStatusLog($formid, 'processing_ozs', '0');
	
	$msg = "send2manager=$man;;;new_status=processing_ozs";
	$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$formid', NOW(), 'robot', '$msg');";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			$res['msg'] = $sql;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}
		
	return "$msg;;;$formid";
}

function check_online()
{
	global $db_connect;
	$user_list = array();
	$today = date("Y-m-d H:i:s", mktime(date('H'), date('i')-14, date('s'), date('m'), date('d'), date('Y')));
	$sql = "SELECT id, online, last_action_time FROM staff WHERE deleted='0' AND online='1' AND last_action_time<'$today' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				array_push($user_list, $row['id']);
			}
		}
		$result->close();
	}
	
	
	foreach($user_list as $key => $val)
	{
		$sql = "UPDATE staff SET online='0', last_action_time=NOW() WHERE id='$val';";
		$result = $db_connect->query($sql);
	}
	return $user_list;
}

function assign_form($office)
{
	global $db_connect;
	$data = array();
	$data0 = array();
	$data0_keys = array();
	$data1 = array();
	$data1_keys = array();
	$list1 = array();
	$forms = array();
	
	$list_forms = array();
	$today = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d')-1, date('Y')));

	//$sql = "SELECT tm_man, count(id) AS cforms FROM forms WHERE date_add>'$today' AND tm_man IS NOT NULL AND in_tm_sort='1' GROUP BY tm_man ORDER BY cforms ASC;";
	$sql = "SELECT tm_man, count(id) AS cforms FROM forms WHERE date_add>'$today' AND tm_man IS NOT NULL GROUP BY tm_man ORDER BY cforms ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				//array_push($list1, $row);
				$list_forms[$row['tm_man']] = $row['cforms'];
			}
		}
		$result->close();
	}
	else die($db_connect->errno . " " . $db_connect->error);
	
	//print_r($list_forms);
	
	//$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND id !='19' AND status!='busy' AND online='1' AND office='$office' ORDER BY id ASC;";
	$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND online='1' AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($list1, $row);
		}
		$result->close();
		
		foreach($list1 as $key => $val)
		{
			$man = $val['id'];
			$sql1 = "SELECT COUNT(id) AS cc FROM forms WHERE status='primary_work' AND (call_event IS NULL OR call_event < NOW()) AND manager='$man' ORDER BY id ASC;";
			if ($result1 = $db_connect->query($sql1))
			{
				$row = $result1->fetch_array(MYSQLI_ASSOC);
				$list1[$key]['count_f'] = $row['cc'];
				$list1[$key]['count_all_f'] = 0;
				
				if ($list1[$key]['count_f'] < 1)
				{
					//array_push($data0, $list1[$key]);
					$data0[$list1[$key]['id']] = $list1[$key];
					$data0_keys[] = $list1[$key]['id'];
				}
				else if ($list1[$key]['count_f'] < 2)
				{
					//array_push($data1, $list1[$key]);
					$data1[$list1[$key]['id']] = $list1[$key];
					$data1_keys[] = $list1[$key]['id'];
				}
				$result1->close();
			}
			else
			{
				return -1;
			}
		}
		
		if (count($data0) > 0)
		{
			if (count($data0) == 1)
			{
				//return $data0;
				$manager = array(current($data0));
			}
			else
			{
				$mink = 0;
				$minv = -1;
				//return array(current($data0));
				foreach($data0 as $key => $val)
				{
					if (isset($list_forms[$val['id']]))
					{
						if ($minv == -1)
						{
							$mink = $key;
							$minv = $list_forms[$val['id']];
						}
						else if ($minv > $list_forms[$val['id']])
						{
							$mink = $key;
							$minv = $list_forms[$val['id']];
						}
					}
				}
				if ($minv == -1)
				{
					reset ($data0);
					$manager = array(current($data0));
				}
				else
					$manager = array($data0[$mink]);
				
				
			}
		}
		else if (count($data1) > 0)
		{
			if (count($data1) == 1)
			{
				//return $data1;
				$manager = array(current($data1));
			}
			else
			{
				//return array(current($data1));
				$mink = 0;
				$minv = -1;
				//return array(current($data0));
				foreach($data1 as $key => $val)
				{
					if (isset($list_forms[$val['id']]))
					{
						if ($minv == -1)
						{
							$mink = $key;
							$minv = $list_forms[$val['id']];
						}
						else if ($minv > $list_forms[$val['id']])
						{
							$mink = $key;
							$minv = $list_forms[$val['id']];
						}
					}
				}
				if ($minv == -1)
				{
					reset ($data1);
					$manager = array(current($data1));
				}
				else
					$manager = array($data1[$mink]);
			}
		}
		else
			return 0;
		
	}
	else
	{
		return -1;
	}

	$sql = "SELECT id, sum, date_add, lastname, firstname, middlename FROM forms WHERE type='mini' AND status='in_tm_office' AND office='$office' AND date_add > '2018-05-01 00:00:00' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$manager[] = $row;
		}
		else
		{
			return 2;
			$result->close();
		}
		$result->close();
	}
	else
	{
		//$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		//echo $res;
		return -1;
	}
	//return $forms;
	
	$man = $manager[0]['id'];
	$tm_man = $manager[0]['id'];
	$id = $manager[1]['id'];
	$sql = "UPDATE forms SET manager='$man', tm_man='$tm_man', status='primary_work', wait_man='1', last_status_time=NOW() WHERE id='$id';";
	//echo $sql;
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
		{
			return -1;
		}
	}
	else
	{
		//$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		//echo $res;
		return -1;
	}
    insertIntoFormStatusLog($id, 'primary_work', '0');
	
	/*$msg = "send2manager=$tm_man;;;new_status=primary_work";
	$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$id', NOW(), 'robot', '$msg');";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			$res['msg'] = $sql;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}*/
	
	return $manager;
}

function assign_form_fix($office)
{
	global $db_connect;
	$data = array();
	$data0 = array();
	$data0_keys = array();
	$data1 = array();
	$data1_keys = array();
	$list1 = array();
	$list2 = array();
	$forms = array();
	
	$list_forms = array();
	$today = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d')-1, date('Y')));
	
	
	
	$res = array();
	$man_mass = array();
	$man_miss_mass = array();
	$max_cforms = 0;
	
	
	//$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND office='2' AND online='1' AND office='$office' ORDER BY id ASC;";
	$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND online='1' AND status='ready' AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$man_mass[$row['id']] = array('id' => $row['id'], 'cforms' => 0, 'count_f' => 0, 'chance' => 0);
			}
			$res['status'] = 'ok';
		}
		$result->close();
	}
	else die($db_connect->errno . " " . $db_connect->error);
	
	if (count($man_mass) == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	//$sql = "SELECT id, sum, date_add, lastname, firstname, middlename FROM forms WHERE type='mini' AND source != 'agent' AND status='in_tm_office' AND office='$office' AND date_add > '2018-05-01 00:00:00' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	$sql = "SELECT id, sum, date_add, lastname, firstname, middlename, tm_man FROM forms WHERE type='mini' AND status='in_tm_office' AND office='$office' AND date_add > '2018-05-01 00:00:00' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$res['status'] = 'ok';
			$res['form'] = array('id' => $row['id'], 'sum' => $row['sum'], 'date_add' => $row['date_add'], 'manager' => $row['tm_man']);
		}
		else
		{
			$res['status'] = 'miss_form';
		}
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	
	if ($res['status'] != 'ok')
	{
		return $res;
	}

	$sql = "SELECT tm_man, count(id) AS cforms FROM forms WHERE date_add>'$today' AND tm_man IS NOT NULL AND main_form IS NULL GROUP BY tm_man ORDER BY cforms ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				if (isset($man_mass[$row['tm_man']]))
				{
					$man_mass[$row['tm_man']]['cforms'] = $row['cforms'];
					if ($max_cforms < $row['cforms'])
						$max_cforms = $row['cforms'];
				}
			}
		}
		$result->close();
	}
	else die($db_connect->errno . " " . $db_connect->error);

	$min_val = 9999999;
	$min_id = 0;
	foreach($man_mass as $man => $val)
	{
		$sql = "SELECT COUNT(id) AS cc FROM forms WHERE status='primary_work' AND (call_event IS NULL OR call_event < NOW()) AND manager='$man' AND main_form IS NULL ORDER BY id ASC;";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				
				if ($row['cc'] > 1)
				{
					$man_miss_mass[$man] = $man_mass[$man];
					$man_miss_mass[$man]['count_f'] = $row['cc'];
					unset($man_mass[$man]);
					continue;
				}
				$man_mass[$man]['count_f'] = $row['cc'];
				$max_mt_rand = $max_cforms + 3*$man_mass[$man]['cforms'];
				$man_mass[$man]['max_mt_rand'] = $max_mt_rand;
				$man_mass[$man]['max_cforms'] = $max_cforms;
				$man_mass[$man]['chance'] = mt_rand(0, $max_mt_rand);
				
				if ($man_mass[$man]['chance'] < $min_val && $res['manager'] != $man)
				{
					$min_val = $man_mass[$man]['chance'];
					$min_id = $man;
				}
				
			}
			$result->close();
		}
		else die($db_connect->errno . " " . $db_connect->error);
	}
	
	$res['man_mass'] = $man_mass;
	$res['man_miss_mass'] = $man_miss_mass;
	$res['min_val'] = $min_val;
	$res['sel_man'] = $min_id;
	
	
	if (count($man_mass) == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	if ($min_id == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	
	$man = $res['sel_man'];
	$tm_man = $res['sel_man'];
	$id = $res['form']['id'];
	$sql = "UPDATE forms SET manager='$man', tm_man='$tm_man', status='primary_work', wait_man='1', last_status_time=NOW() WHERE id='$id';";
	$res['sql'] = $sql;
	
	//return $res;
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось обновить анкету";
		}
		send_push_new_form($man, $id);
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	}
    insertIntoFormStatusLog($id, 'primary_work', '0');
	
	return $res;
}


function assign_form_fix2($office)
{
	global $db_connect;
	$data = array();
	$data0 = array();
	$data0_keys = array();
	$data1 = array();
	$data1_keys = array();
	$list1 = array();
	$list2 = array();
	$forms = array();
	
	$list_forms = array();
	$today = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d')-1, date('Y')));
	
	
	
	$res = array();
	$man_mass = array();
	$man_miss_mass = array();
	$max_cforms = 0;
	
	
	$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND office='2' AND online='1' AND office='$office' ORDER BY id ASC;";
	//$sql = "SELECT id, lastname, firstname, position, deleted, office, status FROM staff WHERE deleted='0' AND in_tm_sort='1' AND online='1' AND office='$office' ORDER BY id ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$man_mass[$row['id']] = array('id' => $row['id'], 'cforms' => 0, 'count_f' => 0, 'chance' => 0);
			}
			$res['status'] = 'ok';
		}
		$result->close();
	}
	else die($db_connect->errno . " " . $db_connect->error);
	
	if (count($man_mass) == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	$sql = "SELECT id, sum, date_add, lastname, firstname, middlename FROM forms WHERE type='mini' AND status='in_tm_office' AND office='$office' AND date_add > '2018-05-01 00:00:00' ORDER BY sum DESC, date_add DESC LIMIT 1;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$res['status'] = 'ok';
			$res['form'] = array('id' => $row['id'], 'sum' => $row['sum'], 'date_add' => $row['date_add']);
		}
		else
		{
			$res['status'] = 'miss_form';
		}
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	
	/*if ($res['status'] != 'ok')
	{
		return $res;
	}*/

	$sql = "SELECT tm_man, count(id) AS cforms FROM forms WHERE date_add>'$today' AND tm_man IS NOT NULL AND main_form IS NULL GROUP BY tm_man ORDER BY cforms ASC;";
	if ($result = $db_connect->query($sql))
	{
		if ($result->num_rows)
		{
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				if (isset($man_mass[$row['tm_man']]))
				{
					$man_mass[$row['tm_man']]['cforms'] = $row['cforms'];
					if ($max_cforms < $row['cforms'])
						$max_cforms = $row['cforms'];
				}
			}
		}
		$result->close();
	}
	else die($db_connect->errno . " " . $db_connect->error);

	$min_val = 9999999;
	$min_id = 0;
	foreach($man_mass as $man => $val)
	{
		$sql = "SELECT COUNT(id) AS cc FROM forms WHERE status='primary_work' AND (call_event IS NULL OR call_event < NOW()) AND manager='$man' AND main_form IS NULL ORDER BY id ASC;";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				
				if ($row['cc'] > 1)
				{
					$man_miss_mass[$man] = $man_mass[$man];
					$man_miss_mass[$man]['count_f'] = $row['cc'];
					unset($man_mass[$man]);
					continue;
				}
				$man_mass[$man]['count_f'] = $row['cc'];
				$max_mt_rand = $max_cforms + 3*$man_mass[$man]['cforms'];
				$man_mass[$man]['max_mt_rand'] = $max_mt_rand;
				$man_mass[$man]['chance'] = mt_rand(0, $max_mt_rand);
				
				if ($man_mass[$man]['chance'] < $min_val)
				{
					$min_val = $man_mass[$man]['chance'];
					$min_id = $man;
				}
				
			}
			$result->close();
		}
		else die($db_connect->errno . " " . $db_connect->error);
	}
	
	
	$res['man_mass'] = $man_mass;
	$res['man_miss_mass'] = $man_miss_mass;
	$res['min_val'] = $min_val;
	$res['sel_man'] = $min_id;
	
	
	return $res;
	
	if (count($man_mass) == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	if ($min_id == 0)
	{
		$res['status'] = 'miss';
		$res['msg'] = 'no staff';
		return $res;
	}
	
	
	$man = $res['sel_man'];
	$tm_man = $res['sel_man'];
	$id = $res['form']['id'];
	$sql = "UPDATE forms SET manager='$man', tm_man='$tm_man', status='primary_work', wait_man='1', last_status_time=NOW() WHERE id='$id';";
	$res['sql'] = $sql;
	
	//return $res;
	if ($result = $db_connect->query($sql))
	{
		if (!$db_connect->affected_rows)
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось обновить анкету";
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	}

    insertIntoFormStatusLog($id, 'primary_work', '0');
	
	return $res;
}


function udpate_staff_onilne($st)
{
	global $db_connect;
	$sql = "UPDATE staff SET online='1', last_action_time=NOW() WHERE id='$st';";
	$result = $db_connect->query($sql);
}

function check_permission($sel)
{
	global $staff_position, $staff_id, $staff_id_debug, $staff_office, $staff_office_type;
	
	$access_granted = false;
	switch($sel)
	{
		case "see_balance":
		case "save_form":
		case "save_note":
		case "change_status":
		case "form_move":
		case "form_2za":
		case "form_photo":
		case "form_double":
		{
			if ($staff_position == 'secretary')
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
        case "set_work": {
            if (($staff_position == 'secretary') || $staff_id == '39')
                $access_granted = false;
            else
                $access_granted = true;
            break;
        }
		case "read_form_field":
		{
			$access_granted = true;
			break;
		}
		case "edit_form_field":
		{
			if ($staff_position == 'secretary')
				$access_granted = false;
			else
				$access_granted = true;
			
			break;
		}
		case "form_prb":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_action":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_checks":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_banks":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_ki":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_return":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
		case "form_files":
		{
			if (($staff_position == 'secretary') || ($staff_office == BROKER_OFFICE1) || ($staff_office_type == 'broker'))
				$access_granted = false;
			else
				$access_granted = true;
			break;
		}
	}

	return $access_granted;
}

function algoUrlsShow() {
    global $staff_position, $db_connect;
    $str = '';

    $sql = "SELECT * FROM algo_urls WHERE deleted IS NULL OR deleted=0;";

    if ($result = $db_connect->query($sql))
    {
        if ($result->num_rows)
        {
            while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                if ($staff_position == 'manager_tm' && $row['tm'] == 1) {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
                if ($staff_position == 'director_tm' && $row['dirTm'] == 1) {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
                if ($staff_position == 'manager_ozs' && $row['ozs'] == 1) {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
                if ($staff_position == 'director_ozs' && $row['dirOzs'] == 1) {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
                if ($staff_position == 'admin' && $row['main'] == 1) {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
                if ($staff_position == 'super-admin') {
                    $str .= "<li><a href=\"{$row['url']}\" target='_blank'><span>{$row['label']}</span></a></li>";
                }
            }
        }
        $result->close();
    }
    else die($db_connect->errno . " " . $db_connect->error);

    return $str;
}

function check_access($sel)
{
	global $staff_position, $staff_id, $staff_za, $staff_pc, $staff_bc;
	
	$access_granted = false;
	switch($sel)
	{
        case "filters": $access_granted = false;            break;
		case "listsotr": 
		case "staff": 
		case "asettings": 
		case "banks": 
		case "services":
		case "metriks": 
		case "analitics":
		case "settings":
		{
			if (($staff_position == 'super-admin') || ($staff_position == 'admin'))
				$access_granted = true;
			break;
		}
		case "mstat": 
		{
			if (($staff_position == 'super-admin') || ($staff_position == 'admin'))
				$access_granted = true;
			break;
		}
		case "agents": 
		{
			if (($staff_position == 'super-admin') || ($staff_position == 'admin'))
				$access_granted = true;
			break;
		}
		case "bankrequests": 
		{
			if (($staff_position == 'super-admin') || ($staff_position == 'admin') || ($staff_position == 'director_ozs') || ($staff_position == 'manager_ozs'))
				$access_granted = true;
			break;
		}
		case "listmetrika": 
		{
			if (($staff_id + 0 == 1) || ($staff_id + 0 == 13))
				$access_granted = true;
			break;
		}
		case "list_fa": 
		{
			if (($staff_za + 0 == 1) || ($staff_pc + 0 == 1))
				$access_granted = true;
			break;
		}
		case "staff_bc": 
		{
			if ($staff_bc + 0 == 1)
				$access_granted = true;
			break;
		}
		case "bpmn": 
		{
			if ($staff_id + 0 == 1)
				$access_granted = true;
			if ($staff_id + 0 == 5)
				$access_granted = true;
			break;
		}
		default: $access_granted = true; break;
	}
	
	return $access_granted;
}

function update_last_msg_time($id)
{
	global $db_connect;
	$sql = "UPDATE forms SET last_msg_time=NOW() WHERE id='$id';";
	$result = $db_connect->query($sql);
}

function update_last_msg_in_form($id, $msg = false)
{
	global $db_connect;
	if ($msg === false)
		$sql = "UPDATE forms SET last_msg_time=NULL, msgs_text=NULL WHERE id='$id';";
	else
		$sql = "UPDATE forms SET last_msg_time=NOW(), msgs_text=CONCAT(COALESCE(msgs_text, ''),';$msg;') WHERE id='$id';";
	$result = $db_connect->query($sql);
}

function get_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function check_nbki($i, $id, $chid, $cl)	//check_id, form_id, checkidinnbki, class
{
	global $db_connect;
	include_once(SERVICES_DIR.'cPromoney.php');
	$promoney = new cPromoney($id) or die('f');
	$out = $promoney->getCheckStatus($chid, $cl);
	$prom_res = json_decode($out);
	$prom_res_a = (array)$prom_res;//['CLASS|VALUE'];
	
	$hit = false;
	
	if ($prom_res->STATUS != null)
	{
		if ($prom_res->STATUS == 'FINAL')
			$hit = true;
	}
	
	if (($prom_res->ERROR == null) || $hit)
	{
		//echo $prom_res->STATUS . "\n";
		if ($prom_res->STATUS != null)
		{
			if ($prom_res->STATUS == 'FINAL')
			{
				update_last_msg_time($id);
				update_last_msg_in_form($id, 'promoneyfinal');
				if ($prom_res->RESULT_FILE != null)
				{
					
					$res_file = $prom_res->RESULT_FILE->HTTPPATH;
					$res_file2 = $db_connect->real_escape_string($prom_res->RESULT_FILE->HTTPPATH);
				}
				
				$ans = $db_connect->real_escape_string($out);
				
				$sql = "UPDATE promoney_checks SET status='FINAL', file='$res_file', answer='$ans' WHERE id='$i';";
				if ($result = $db_connect->query($sql))
				{
					if ($db_connect->affected_rows)
					{
						$res['status'] = 'ok';
					}
					else
					{
						$res['status'] = 'failed';
						$res['msg'] = $sql;
					}
				}
				else
				{
					$res['status'] = 'failed';
					$res['sql'] = $sql;
					$res['msg'] = "2: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
					print_r("ERROR0:\n");
					print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
					die();
				}
				
				//$dir = get_path($id);
				$dir = FILES_DIR . get_path($id);
				if (!is_dir($dir)) {
					if(!mkdir($dir, 0777, true))
					{
						$res['status'] = 'failed';
						$res['t1'] = $id;
						$res['dir'] = $dir;
						$res['dir2'] = __DIR__;
						$res['msg'] = 'Ошибка создания дирректории';
						print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
						return print_r(json_encode($res,JSON_UNESCAPED_UNICODE), true);
						die();
					}
				}
				$file_name = $prom_res_a['CLASS|VALUE'] . " (" . date('Y-m-d H:i:s') . ").pdf";
				
				$hash_name = md5($file_name);
				$file = "$dir/$hash_name";
				
				//print_r("\n$file\n");
				$get_page = $promoney->get_file($res_file);
				
				if (is_object($get_page) && $get_page->ERROR != null)
				{
					$res['status'] = 'failed';
					$res['msg'] = $get_page->ERROR;
				}
				else
				{
					if(file_put_contents($file, $get_page))
					{
						//$ftype = 'report';
						$ftype = 'usert_21';
						$sql = "INSERT INTO files(date_add, form_id, staff_id, type, hash_name, name) VALUES (NOW(), '$id', '0', '$ftype', '$hash_name', '$file_name');";
						//$res['sql'] = $sql;
						if ($result = $db_connect->query($sql))
						{
							if ($db_connect->affected_rows)
							{
								$res['status'] = 'ok';
							}
							else
							{
								$res['status'] = 'failed';
								$res['msg'] = $sql;
							}
						}
						else
						{
							$res['status'] = 'failed';
							$res['sql'] = $sql;
							$res['msg'] = "8: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
							print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
							die();
						}
					}
					else
					{
						$res['status'] = 'failed';
						$res['msg'] = 'Не удалось записать файл!';
						print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
						die();
					}
				}
			}
		}
	}
	else
	{
		$error_text = '';
		if ($prom_res->ERROR != null)
			$error_text = $prom_res->ERROR;
		
		update_last_msg_time($id);
		update_last_msg_in_form($id, 'promoneyerror');
		$sql = "UPDATE promoney_checks SET status='ERROR_F', error_text='$error_text' WHERE id='$i';";
		//print_r($sql);
		if ($result = $db_connect->query($sql))
		{
			if ($db_connect->affected_rows)
			{
				$res['status'] = 'ok';
			}
			else
			{
				$res['status'] = 'failed';
				$res['msg'] = $sql;
			}
		}
		else
		{
			$res['status'] = 'failed';
			$res['sql'] = $sql;
			$res['prom_res'] = $prom_res;
			$res['msg'] = "2: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
			print_r("ERROR0:\n");
			print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
			die();
		}
		
	}
	return "OK";
	//print_r($prom_res);
}

function insert_into_sql2($table, $data, $is_signal = false)
{
	global $db_connect;
	
	$res = 'OK';
	
	$t_key = "";
	$t_val = "";
	$phone_test = '';
	foreach ($data as $key => $value)
	{
		if ($t_key)
			$t_key .= ", ";
		$t_key .= "$key";
		
		if ($t_val)
			$t_val .= ", ";
		$t_val .= "'$value'";
		
		if ($key == 'phone_history')
			$phone_test = preg_replace("/[^0-9]/", '', $value);
	}
	
	if ($table == "forms")
	{
		if (!$is_signal)
		{
			$t_key .= ", date_add";
			$t_val .= ", NOW()";
		}
		
		$afs = md5(time() . $t_val);
		$t_key .= ", afs_key";
		$t_val .= ", '$afs'";
	}
	
	$test_id = '';
	$d_res = '';
	
	$db_connect->query("INSERT INTO $table($t_key) VALUES ($t_val);") or $res = "Не выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	
	//print_r($res);
	if (($res == 'OK') && ($table == "forms"))
	{
		$sql = "SELECT id, type FROM forms WHERE afs_key = '$afs';";
		$rres = "";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$test_id = $row['id'];
				
				//$res['status'] = 'ok';
				$res = 'OK';
				if ($row['type'] == 'mini')
				{
					$rres = send2TMoffice($row['id']);
					$d_res = check_double($row['id'], $phone_test);
				}
					

			}
			$result->close();
		}
		else
		{
			$res = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
	}
	//print_r($res);
	return $test_id;
}

function insert_into_sql($table, $data, $is_signal = false)
{
	global $db_connect;
	
	$hitComment = false;
	
	$res = 'OK';
	
	$t_key = "";
	$t_val = "";
	$phone_test = '';
	foreach ($data as $key => $value)
	{
		if ($key == 'comment')
		{
			$hitComment = true;
			continue;
		}
		
		if ($t_key)
			$t_key .= ", ";
		$t_key .= "$key";
		
		if ($t_val)
			$t_val .= ", ";
		$t_val .= "'$value'";
		
		if ($key == 'phone_history')
			$phone_test = preg_replace("/[^0-9]/", '', $value);
	}
	
	if ($table == "forms")
	{
		if (!$is_signal)
		{
			$t_key .= ", date_add";
			$t_val .= ", NOW()";
		}
		
		$afs = md5(time() . $t_val);
		$t_key .= ", afs_key";
		$t_val .= ", '$afs'";
	}
	
	$test_id = '';
	$d_res = '';
	
	$db_connect->query("INSERT INTO $table($t_key) VALUES ($t_val);") or $res = "Не выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	
	if (($res == 'OK') && ($table == "forms"))
	{
		$sql = "SELECT id, type FROM forms WHERE afs_key = '$afs';";
		$rres = "";
		if ($result = $db_connect->query($sql))
		{
			if ($result->num_rows)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$test_id = $row['id'];
				
				//$res['status'] = 'ok';
				$res = 'OK';
				if ($row['type'] == 'mini')
				{
					$rres = send2TMoffice($row['id']);
					$d_res = check_double($row['id'], $phone_test);
				}
					

			}
			$result->close();
		}
		else
		{
			$res = "Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		
		$resultsms = 'not send sms';
		if ($res == 'OK')
		{
			if (isset($data['sms']))
			{
				if ($data['sms'] == 'yes')
				{
					include_once SERVICES_DIR . "sms_serv.php";
					$tel = $data['phone_history'];
					//$msg_ = "Спасибо за прохождение теста! Ссылка на чек-лист здесь http://bafk.ru/checklist/ Мы свяжемся с Вами в течение часа.";
					$msg_ = "Спасибо за прохождение теста! Ссылка на чек-лист здесь http://finanskredit.ru/checklist/ Мы свяжемся с Вами в течение часа.";
					if ($sms = new MY_SMS($test_id, 0, $db_connect, $sms_config))
						$resultsms = json_decode($sms->sendSMS($tel, $msg_));
				}
			}
		}
		
		if ($hitComment)
		{
			if (isset($data['comment']) && ($data['comment'] != ''))
			{
				$msg = $data['comment'];
				$db_connect->query("INSERT INTO form_log(form_id, date, type, msg) VALUES('$test_id', NOW(), 'robot', '$msg');");
			}
		}
		

		$fp = fopen(dirname(__FILE__) . "/../logs/robot.log", "a+");
		$data_ = date("Y-m-d H:i:s", time());
		$data_ .= "\n test_id " . print_r($test_id,true);
		$data_ .= "\n phone_test " . print_r($phone_test,true);
		$data_ .= "\n" . print_r($t_key,true);
		$data_ .= "\n" . print_r($t_val,true);
		$data_ .= "\n" . print_r($res,true);
		$data_ .= "\n" . print_r($resultsms,true);
		$data_ .= "\n double search result: " . print_r($d_res,true);
		$data_ .= "\n" . print_r($rres,true) . "\n\n";
		$test = fwrite($fp, $data_);
		fclose($fp);
	}
	return $res;
}

function CBR_XML_Daily_Ru()
{
    $json_daily_file = __DIR__.'/../tmp/daily.json';
    if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 3600) {
        if ($json_daily = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js')) {
            file_put_contents($json_daily_file, $json_daily);
        }
    }

    return json_decode(file_get_contents($json_daily_file));
}


function check_double($id, $phone)
{
	global $db_connect;
	
	$vt = $phone;
	if (strlen($vt) == 11)
		$vt = substr($vt, 1);
	
	$list1 = array();
	$list2 = array();
	//data LIKE '%::$vt<br%' OR data LIKE '%::_$vt<br%'
	$sql = "SELECT id, status FROM forms WHERE (phone_history LIKE '%$vt;;%' OR data LIKE '%::$vt<br%' OR data LIKE '%::_$vt<br%') AND id!='$id' AND type='mini';";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			if ($row['status'] == 'work_complete')
				array_push($list1, $row['id']);
			else
				array_push($list2, $row['id']);
		}
		$result->close();
	}
	else
	{
		return -1;
	}
	
	if (count($list2) > 0)
	{
		$list = '';
		foreach($list2 as $key)
		{
			$list .= ";$key;";
			$sql = "UPDATE forms SET double_sub=CONCAT(COALESCE(double_sub, ''),';$id;') WHERE id='$key';";
			if ($result = $db_connect->query($sql))
			{
				if (!$db_connect->affected_rows)
				{
					return -1;
				}
			}
			else
				return -1;
			
			$msg = "Поступил <a target=_blank href=\"/details/$id/\">дубль</a> текущей анкеты. См. раздел дублей.";
			$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$key', NOW(), 'robot', '$msg');";
			if ($result = $db_connect->query($sql))
			{
				if (!$db_connect->affected_rows)
				{
					return -1;
				}
			}
			else
				return -1;
		}
		
		$sql = "UPDATE forms SET double_main='$list', status='work_complete' WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
		}
		else
			return -1;
        insertIntoFormStatusLog($id, 'work_complete', '0');

		$msg = "Анкета закрыта по причине наличия активной открытой акнеты. См. раздел дублей.";
		$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES('$id', NOW(), 'robot', '$msg');";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
		}
		else
			return -1;
	}
	
	if (count($list1) > 0)
	{
		$list = '';
		foreach($list1 as $key)
		{
			$list .= ";$key;";
			$sql = "UPDATE forms SET double_main=CONCAT(COALESCE(double_main, ''),';$id;') WHERE id='$key';";
			if ($result = $db_connect->query($sql))
			{
				if (!$db_connect->affected_rows)
				{
					return -1;
				}
			}
			else
				return -1;
		}
		
		$sql = "UPDATE forms SET double_sub='$list' WHERE id='$id';";
		if ($result = $db_connect->query($sql))
		{
			if (!$db_connect->affected_rows)
			{
				return -1;
			}
		}
		else
			return -1;
	}
	
	return "pass. work_complete: " . count($list1) . ", in_work: " . count($list2);
	
	/*$phone_list = array();
	
	if ($result = $db_connect->query("SELECT name, type FROM form_fields_settings WHERE type='telephone';"))
	{
		while($val = $result->fetch_array(MYSQLI_ASSOC))
		{
			$phone_list[] = $val['name'];
		}
		
		$result->close();
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	
	$list1 = array();
	$sql = "SELECT id, status FROM forms WHERE id='new' AND type='mini';";
	if ($result = $db_connect->query($sql))
	{
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			array_push($list1, $row['id']);
		}
		$result->close();
	}
	else
	{
		return -1;
	}*/
}

function get_path($id)
{
	$id = (string)$id;
	$id_l = strlen($id);

	for ($i=0; $i<10-$id_l; $i++)
		$id = "0".$id;
	
	$l0=(int)substr($id, 0, 4);
	$l1=(int)substr($id, -6, 2);
	$l2=(int)substr($id, -4, 2);
	$l3=(int)substr($id, -2);
	
	$dir_ = "./../files_upload/$l0/$l1/$l2/$l3";
	return $dir_;
}

function get_files($str)
{
	$id = (string)$str;
	$id_l = strlen($id);
	
	for ($i=0; $i<10-$id_l; $i++)
		$id = "0".$id;
	
	$l0=(int)substr($id, 0, 4);
	$l1=(int)substr($id, -6, 2);
	$l2=(int)substr($id, -4, 2);
	$l3=(int)substr($id, -2);
	
	$files1 = scandir("./../files_upload/$l0/$l1/$l2/$l3");
	print_r($files1);

}

function list_files($str)
{
	//echo "<br>";
	$id = (string)$str;
	//echo $id . "<br>";
	$id_l = strlen($id);

	//echo $id . "<br>";
	
	for ($i=0; $i<10-$id_l; $i++)
		$id = "0".$id;
	
	$l0=(int)substr($id, 0, 4);
	$l1=(int)substr($id, -6, 2);
	$l2=(int)substr($id, -4, 2);
	$l3=(int)substr($id, -2);
	
	$dir = "$l0/$l1/$l2/$l3";
	//echo $dir . "<br>";

	if(is_dir("./../files_upload/$l0/$l1/$l2/$l3"))
	{
		$files1 = scandir("./../files_upload/$l0/$l1/$l2/$l3");
		//print_r($files1);
		foreach ($files1 as $k=>$v)
		{
			
			if (($v == ".") || ($v == ".."))
			{
				//echo "<br>" . $v;
				unset ($files1[$k]);
				//sort($files1);
			}
		}
		//print_r($files1);
		sort($files1);
	}
	if (isset($files1))
		return $files1;
}


function file_force_download($file, $name) {
    $name = '"' . $name . '"';
    if (file_exists($file)) {
    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    // если этого не сделать файл будет читаться в память полностью!
    if (ob_get_level()) {
      ob_end_clean();
    }

	if (preg_match("/\.pdf$/i", $name))
	{
		header ('Content-type: application/pdf');
		header ('Content-disposition: inline; filename=' . $name);
	}
	else if (preg_match("/\.docx$/i", $name))
	{
		header ('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		header ('Content-disposition: inline; filename=' . $name);
	}
	else if (preg_match("/\.png$/i", $name))
	{
		header ('Content-type: image/png');
		header ('Content-disposition: inline; filename=' . $name);
	}
	else if (preg_match("/\.jpg$/i", $name))
	{
		header ('Content-type: image/jpeg');
		header ('Content-disposition: inline; filename=' . $name);
	}
	else
	{
		    // заставляем браузер показать окно сохранения файла
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		//header('Content-Disposition: attachment; filename=' . basename($file));
		header('Content-Disposition: attachment; filename=' . $name);
	}
	
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));

    // читаем файл и отправляем его пользователю
    if ($fd = fopen($file, 'rb')) {
      while (!feof($fd)) {
        print fread($fd, 1024);
      }
      fclose($fd);
    }
    exit;
  }
}

function insert_base64_encoded_image($img, $echo = false)
{
	$imageSize = getimagesize($img);
	$imageData = base64_encode(file_get_contents($img));
	$imageHTML = "<img src='data:{$imageSize['mime']};base64,{$imageData}' {$imageSize[3]} />";
	if($echo == true){
		echo $imageHTML;
	} else {
		return $imageHTML;
	}
}

function send2manager($aid, $office)
{
	global $db_connect;
	$staff_mass = array();
	
	if ($result = $db_connect->query("SELECT * FROM staff WHERE office='$office' AND deleted='0' ORDER BY id ASC;"))
	{
			
		while ($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$staff_mass[$row['id']] = $row;
		}
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}

function send2TMoffice($aid)
{
	global $db_connect;
	$sql = "UPDATE forms SET status='in_tm_office', last_status_time=NOW(), office='2' WHERE id='$aid';";

	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	
	if ($res['status'] == 'failed')
		return $res;

    insertIntoFormStatusLog($aid, 'in_tm_office', '0');
	
	$msg = "send2TMoffice=2;;;new_status=in_tm_office";
	$sql = "INSERT INTO form_log(form_id, date, type, msg) VALUES ('$aid', NOW(), 'robot', '$msg');";
	if ($result = $db_connect->query($sql))
	{
		if ($db_connect->affected_rows)
		{
			$res['status'] = 'ok';
			$res['msg'] = $sql;
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = $sql;
		}
	}
	else
	{
		$res['status'] = 'failed';
		$res['msg'] = "3: Не удалось выполнить запрос: (" . $db_connect->errno . ") " . $db_connect->error;
	}
	
	return $res;
}

function get_ya_balance($cabNum = 0)
{
	$url = 'https://api.direct.yandex.ru/live/v4/json/';
	// OAuth-токен пользователя, от имени которого будут выполняться запросы

    $tokens = [
        0 => ['token' => 'AQAAAAAfdr7tAAU-xXZROH8YBk7VifFEm6ZrwQk', 'login' => 'pavel-i-direct@yandex.ru'],
        1 => ['token' => 'AgAAAABNYDtRAAbQjMSNqXyNx07sg_Dit-waRdI', 'login' => 'krym.paul@yandex.ru'],
    ];

    //$token = 'AQAAAAAfdr7tAAU-xXZROH8YBk7VifFEm6ZrwQk';
	// Логин клиента рекламного агентства
	// Обязательный параметр, если запросы выполняются от имени рекламного агентства	
	//$clientLogin = 'pavel-i-direct@yandex.ru';

	$token = $tokens[$cabNum]['token'];
    $clientLogin = $tokens[$cabNum]['login'];

    if ($cabNum > 1)
        return 0;

	//--- Подготовка и выполнение запроса -----------------------------------//
	// Установка HTTP-заголовков запроса
	$headers = array(
		"Authorization: Bearer $token",                   // OAuth-токен. Использование слова Bearer обязательно
		"Client-Login: $clientLogin",                     // Логин клиента рекламного агентства
		"Accept-Language: ru",                            // Язык ответных сообщений
		"Content-Type: application/json; charset=utf-8"   // Тип данных и кодировка запроса
	);
	
	// Параметры запроса к серверу API Директа

	$params = array(
		'method' => 'AccountManagement',                                // Используемый метод сервиса Campaigns
		'param' => array(
			'Action' => 'Get',
			'SelectionCriteria' => (object) array()
		),
		"token" => $token
	);
	
	// Преобразование входных параметров запроса в формат JSON
	$body = json_encode($params, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

	// Инициализация cURL
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $body);

	/*
	Для полноценного использования протокола HTTPS можно включить проверку SSL-сертификата сервера API Директа.
	Чтобы включить проверку, установите опцию CURLOPT_SSL_VERIFYPEER в true, а также раскомментируйте строку с опцией CURLOPT_CAINFO и укажите путь к локальной копии корневого SSL-сертификата. 
	*/
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	//curl_setopt($curl, CURLOPT_CAINFO, getcwd().'\CA.pem');

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLINFO_HEADER_OUT, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	// Выполнение запроса, получение результата
	$result = curl_exec($curl);
	$bal = 0;
	
	if(!$result) 
	{
		echo ('Ошибка cURL: '.curl_errno($curl).' - '.curl_error($curl));
		curl_close($curl);
		return -1;
	}
	$responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	$responseHeaders = substr($result, 0, $responseHeadersSize);
	$responseBody = substr($result, $responseHeadersSize);
	
	if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200) 
	{ 
		echo "HTTP-ошибка: ".curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return -2;
	}
	
	$responseBody = json_decode($responseBody);

	if (isset($responseBody->error))
	{
		$apiErr = $responseBody->error;
		echo "Ошибка API {$apiErr->error_code}: {$apiErr->error_string} - {$apiErr->error_detail} (RequestId: {$apiErr->request_id})";
		curl_close($curl);
		return -3;
	}
	/*$responseHeadersArr = explode("\r\n", $responseHeaders);
	foreach ($responseHeadersArr as $header) {
	if (preg_match('/(RequestId|Units):/', $header)) { echo "$header <br>"; }
	}*/
	
	/*if (isset($responseBody->data->Accounts))
	{
		//print_r($responseBody->data->Accounts);
		foreach ($responseBody->data->Accounts as $acc)
		{
			if (!isset($acc->AmountAvailableForTransfer))
				continue;
			$bal = $acc->AmountAvailableForTransfer;
			break;
		}
	}*/

    if (isset($responseBody->data->Accounts))
    {
        //print_r($responseBody->data->Accounts);
        foreach ($responseBody->data->Accounts as $acc)
        {
            //if (!isset($acc->AmountAvailableForTransfer))
            if (!isset($acc->Amount))
                continue;
            //$bal = $acc->AmountAvailableForTransfer;
            $bal = $acc->Amount;
            break;
        }
    }
	
	curl_close($curl);
	return $bal;
}

function replace_unicode_escape_sequence($match) {
    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
}
function unicode_decode($str) {
    return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
}

?>