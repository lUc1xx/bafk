<?php

if (!defined('SYSTEM_START_9876543210')) exit;



	echo '<script type="text/javascript" src="/scripts/metriks.js?' . rand() . '"></script>';
?>
<div id="mapp">
<div class="row">
<div class="col">
<div class="q-pa-md">
    <q-table
      title="Метрика"
      :data="data"
      :columns="columns"
      row-key="name"
	  :pagination.sync="pagination"
	  :loading="loading"
    >
		<template v-slot:top>
		<div class="row" style="width:100%">
			<div class="col-2">
				<div class="q-py-sm" style="max-width: 200px">
					<q-input filled v-model="date_from" mask="date" :rules="['date']">
					  <template v-slot:append>
						<q-icon name="event" class="cursor-pointer">
						  <q-popup-proxy ref="qDateProxy" transition-show="scale" transition-hide="scale">
							<q-date v-model="date_from" @input="() => $refs.qDateProxy.hide()" />
						  </q-popup-proxy>
						</q-icon>
					  </template>
					</q-input>
				  </div> 
			</div> 
			<div class="col-2">
				<div class="q-py-sm" style="max-width: 200px">
				<q-input filled v-model="date_to" mask="date" :rules="['date']">
				  <template v-slot:append>
					<q-icon name="event" class="cursor-pointer">
					  <q-popup-proxy ref="qDateProxy" transition-show="scale" transition-hide="scale">
						<q-date v-model="date_to" @input="() => $refs.qDateProxy.hide()" />
					  </q-popup-proxy>
					</q-icon>
				  </template>
				</q-input>
			  </div>
			</div>   
			<div class="col-2">
				<div class="q-py-md" style="max-width: 200px">
					<q-btn styel="q-pt-md" color="primary" label="Загрузить" @click="loaddata()"></q-btn>
				</div> 
			</div> 
		</div>
      </template>
	  <template v-slot:loading>
        <q-inner-loading showing color="primary" />
      </template>
      <template v-slot:header="props">
        <q-tr :props="props">
          <q-th
            v-for="col in props.cols"
            :key="col.name"
            :props="props"
            class="text-italic text-purple"
			style="white-space: pre-line"
          >
            {{ col.label }}
          </q-th>
        </q-tr>
      </template>
    </q-table>
  </div>
</div>
</div>
</div>


