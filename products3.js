var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

//var tab = document.querySelector('#products_table');
//var row_list = tab.querySelectorAll('tr');
var row_list;
var debug = false;
var hide_products = false;
//var view_b = 'Режим работы';
var view_b = 'Режим демонстрации';

var cred_sv = 0;
var zp = 0;

var sort = 1;
var sort_by = true;

var find_results = 0;
var fill_reds = 0;
var all_reds = 32;		//без кредитных историй, но с кр.ист юрлица.

var bg_s_up = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';
var bg_s_d  = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';

if (getCookie2('bank_view') != undefined)
	view_b = getCookie2('bank_view');
/*var tmp_r2 = document.querySelector('#result2');
var tmp_divs = tmp_r2.querySelectorAll('div > .res2');

for (var i = 0; i < tmp_divs.length; i++)
{
	tmp_divs[i].style.display = (view_b == 'Режим работы') ? "none" : "block";
}*/
//console.log(view_b);
window.onload=function(){


	row_list = document.querySelectorAll('#products_table tr>td:nth-of-type(6)');
	//console.log(row_list.length);

	//var div = row_list[0].querySelector('div[name="list_res"]').innerHTML;

	var test = new Array();
	for (var i = 1; i < row_list.length; i++)
	{
		var bank = row_list[i].innerHTML.replace('&nbsp;','');
		test.push(bank);
	}

	var i = test.length;
	test.sort();

	while (i--) {
		if (test[i] == test[i-1]) {
			test.splice(i, 1);
		}
	}

	var selector = document.querySelector('#prod_selector');

	for (var i = 0; i<test.length; i++)
	{
		var newOption = new Option(test[i], test[i]);
		selector.appendChild(newOption);
	}
}

function prod_filter(v)
{
	for (var i = 1; i < row_list.length; i++)
	{
		if (v == "-1")
			row_list[i].parentNode.style.display = '';
		else
		{
			var bank = row_list[i].innerHTML.replace('&nbsp;','');
			if (v == bank)
				row_list[i].parentNode.style.display = '';
			else
				row_list[i].parentNode.style.display = 'none';
		}
		
	}
}

//Функция показа
function show(state)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state; 			
}

function show(state, upd)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state; 			
	if (upd)
		document.forms.be_form.be_type.value = "upd";
}

function send_form(type)
{
	var form = document.forms.be_form;
	form.be_type.value = type;
	var data = getQueryString(form.elements);	
	//ajaxQuery('http://old.smartcredit.online/new_products/ajax.php','POST', data, false, function(req) 
	ajaxQuery('/new_products/ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;

	});
	location.reload();
}

function all_w_types()
{
	var el = document.querySelectorAll('[name*=be_work_type]');
	var ch = true;
	for (i=0; i<el.length; i++)
		if (!el[i].checked)
			ch = false;
	for (i=0; i<el.length; i++)
		if(ch)
			el[i].checked = false;
		else
			el[i].checked = true;
			
}

function all_stop_types()
{
	var el = document.querySelectorAll('[name*=be_stop]');
	var ch = true;
	for (i=0; i<el.length; i++)
		if (!el[i].checked)
			ch = false;
	for (i=0; i<el.length; i++)
		if(ch)
			el[i].checked = false;
		else
			el[i].checked = true;
			
}

function edit(id)
{

	
	var tr = document.getElementById('be_id'+id);
	var td = tr.getElementsByTagName('td');
	document.querySelector('#be_butt').innerHTML = "Изменить";
	document.querySelector('#be_b_copy').style = "";
	
	
	var data = "be_type=read&be_prod_id=" + id;
//	ajaxQuery('http://old.smartcredit.online/new_products/ajax.php','POST', data, true, function(req) 
	ajaxQuery('/new_products/ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
		var data_db = JSON.parse(tmp_page);
		
		var form = document.forms.be_form;
		form.reset();
		form.be_type.value = "upd";
		form.be_prod_id.value = id;
		form.be_bank_name.value = data_db["bank_name"];
		form.be_product_name.value = data_db["product_name"];
		form.be_min_percent.value = data_db["min_percent"];
		if (parseInt(data_db["active"]))
			form.be_active.checked = true;
		form.be_description.value = data_db["description"];
		form.be_pd.value = data_db["pd"];
		form.be_discont.value = data_db["discont"];
		if (parseInt(data_db["res"]))
			form.be_res.checked = true;
		form.be_cr4other.value = data_db["cr4other"];
		form.be_min_credit.value = parseInt(data_db["min_credit"]);
		//console.log(data_db);
		//console.log(data_db["min_credit"]);
		form.be_max_credit.value = data_db["max_credit"];
		form.be_min_income.value = data_db["min_income"];
		form.be_max_age_m.value = data_db["max_age_m"];
		form.be_max_age_w.value = data_db["max_age_w"];
		form.be_max_cre_time.value = data_db["max_cre_time"];
		form.be_max_end_age_m.value = data_db["max_end_age_m"];
		form.be_max_end_age_w.value = data_db["max_end_age_w"];
		form.be_min_age_m.value = data_db["min_age_m"];
		form.be_min_age_w.value = data_db["min_age_w"];
		form.be_lim_time.value = data_db["lim_time"];
		
		form.be_nbki.value = data_db["nbki"];
		/*var nbki_t = data_db["nbki"].split('|');
		for (i = 0; i < nbki_t.length +1; i++)
		{
			if (parseInt(nbki_t[i+1]))
			{
				form["be_nbki[" + i +"]"].checked = true;
			}
		}*/
		
		var arm_t = data_db["arm"].split('|');
		for (i = 0; i < arm_t.length +1; i++)
		{
			if (parseInt(arm_t[i+1]))
			{
				form["be_arm[" + i +"]"].checked = true;
			}
		}


		var adr_t = data_db["adr"].split('|');
		for (i = 0; i < adr_t.length +1; i++)
		{
			if (parseInt(adr_t[i+1]))
			{
				form["be_adr[" + i +"]"].checked = true;
			}
		}
		form.be_adr_time.value = data_db["adr_time"];
		
		var be_work = data_db["work"].split('|');
		//console.log(data_db["work"]);
		//console.log(be_work.length);
		for (i = 0; i < be_work.length +1; i++)
		{
			if (parseInt(be_work[i+1]))
			{
				form["be_work[" + i +"]"].checked = true;
			}
		}
		
		var be_c_work = data_db["c_work"].split('|');
		for (i = 0; i < be_c_work.length +1; i++)
		{
			if (parseInt(be_c_work[i+1]))
			{
				form["be_c_work[" + i +"]"].checked = true;
			}
		}
		
		var be_c_inc = data_db["c_inc"].split('|');
		for (i = 0; i < be_c_inc.length +1; i++)
		{
			if (parseInt(be_c_inc[i+1]))
			{
				form["be_c_inc[" + i +"]"].checked = true;
			}
		}
		
		//form.be_inc_card.value = data_db["inc_card"];
		
		//form.be_exp_now.value = data_db["exp_now"];
		var be_exp_now = data_db["exp_now"].split('|');
		for (i = 0; i < be_exp_now.length +1; i++)
		{
			if (parseInt(be_exp_now[i+1]))
			{
				form["be_exp_now[" + i +"]"].checked = true;
			}
		}
		
		//form.be_reg_date.value = data_db["reg_date"];
		var be_reg_date = data_db["reg_date"].split('|');
		for (i = 0; i < be_reg_date.length +1; i++)
		{
			if (parseInt(be_reg_date[i+1]))
			{
				form["be_reg_date[" + i +"]"].checked = true;
			}
		}
		
		var be_work_type = data_db["work_type"].split('|');
		for (i = 0; i < be_work_type.length +1; i++)
		{
			if (parseInt(be_work_type[i+1]))
			{
				form["be_work_type[" + i +"]"].checked = true;
			}
		}
		
		//form.be_work_adr.value = data_db["work_adr"];
		var be_work_adr = data_db["work_adr"].split('|');
		for (i = 0; i < be_work_adr.length +1; i++)
		{
			if (parseInt(be_work_adr[i+1]))
			{
				form["be_work_adr[" + i +"]"].checked = true;
			}
		}
		
		
		
		//form.be_ent.value = data_db["ent"];
		var be_ent = data_db["ent"].split('|');
		for (i = 0; i < be_ent.length +1; i++)
		{
			if (parseInt(be_ent[i+1]))
			{
				form["be_ent[" + i +"]"].checked = true;
			}
		}

		if (parseInt(data_db["ca"]))
			form.be_ca.checked = true;

		if (parseInt(data_db["pfr"]))
			form.be_pfr.checked = true;
		
		form.be_firm_per.value = data_db["firm_per"];
		
		
		
	//	form.be_nal.value = data_db["nal"];
		var be_nal = data_db["nal"].split('|');
		for (i = 0; i < be_nal.length +1; i++)
		{
			if (parseInt(be_nal[i+1]))
			{
				form["be_nal[" + i +"]"].checked = true;
			}
		}
		
		form.be_proc.value = data_db["proc"];
		
		
		
		//form.be_pr_nal.value = data_db["pr_nal"];
		var be_pr_nal = data_db["pr_nal"].split('|');
		for (i = 0; i < be_pr_nal.length +1; i++)
		{
			if (parseInt(be_pr_nal[i+1]))
			{
				form["be_pr_nal[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_pr_4.value = data_db["pr_4"];
		
		
		//form.be_est.value = data_db["est"];
		var be_est = data_db["est"].split('|');
		for (i = 0; i < be_est.length +1; i++)
		{
			if (parseInt(be_est[i+1]))
			{
				form["be_est[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_co.value = data_db["co"];
		
		if (parseInt(data_db["delay_29"]))
			form.be_delay_29.checked = true;
		
		
	//	form.be_ps.value = data_db["ps"];
		var be_psc = data_db["psc"].split('|');
		for (i = 0; i < be_psc.length +1; i++)
		{
			if (parseInt(be_psc[i+1]))
			{
				form["be_psc[" + i +"]"].checked = true;
			}
		}
		form.be_psc_u0.value = data_db["psc_u0"];
		form.be_psc_u1.value = data_db["psc_u1"];
		form.be_psc_u2.value = data_db["psc_u2"];
		form.be_psc_u3.value = data_db["psc_u3"];
		form.be_psc_u4.value = data_db["psc_u4"];
		
		var be_ps = data_db["ps"].split('|');
		for (i = 0; i < be_ps.length +1; i++)
		{
			if (parseInt(be_ps[i+1]))
			{
				form["be_ps[" + i +"]"].checked = true;
			}
		}
		form.be_ps_u0.value = data_db["ps_u0"];
		form.be_ps_u1.value = data_db["ps_u1"];
		form.be_ps_u2.value = data_db["ps_u2"];
		form.be_ps_u3.value = data_db["ps_u3"];
		form.be_ps_u4.value = data_db["ps_u4"];
		
		
		
		if (parseInt(data_db["delay_act"]))
			form.be_delay_act.checked = true;	
		
		if (parseInt(data_db["cr_his"]))
			form.be_cr_his.checked = true;

		form.be_co_max.value = data_db["co_max"];
		form.be_co_umax.value = data_db["co_umax"];
		if (parseInt(data_db["zaim_mfo"]))
			form.be_zaim_mfo.checked = true;

		form.be_lim_all.value = data_db["lim_all"];
		form.be_lim_val.value = data_db["lim_val"];
		form.be_bal_now.value = data_db["bal_now"];
		form.be_bal_val.value = data_db["bal_val"];
		//form.be_ps_c.value = data_db["ps_c"];
		var be_ps_c = data_db["ps_c"].split('|');
		for (i = 0; i < be_ps_c.length +1; i++)
		{
			if (parseInt(be_ps_c[i+1]))
			{
				form["be_ps_c[" + i +"]"].checked = true;
			}
		}
		form.be_ps_v0.value = data_db["ps_v0"];
		form.be_ps_v1.value = data_db["ps_v1"];
		form.be_ps_v2.value = data_db["ps_v2"];
		form.be_ps_v3.value = data_db["ps_v3"];
		form.be_ps_v4.value = data_db["ps_v4"];
		
		var be_psa_c = data_db["psa_c"].split('|');
		for (i = 0; i < be_psa_c.length +1; i++)
		{
			if (parseInt(be_psa_c[i+1]))
			{
				form["be_psa_c[" + i +"]"].checked = true;
			}
		}
		form.be_psa_v0.value = data_db["psa_v0"];
		form.be_psa_v1.value = data_db["psa_v1"];
		form.be_psa_v2.value = data_db["psa_v2"];
		form.be_psa_v3.value = data_db["psa_v3"];
		form.be_psa_v4.value = data_db["psa_v4"];
		
		
		var be_ps_6 = data_db["ps_6"].split('|');
		for (i = 0; i < be_ps_6.length +1; i++)
		{
			if (parseInt(be_ps_6[i+1]))
			{
				form["be_ps_6[" + i +"]"].checked = true;
			}
		}
		form.be_ps_6v0.value = data_db["ps_6v0"];
		form.be_ps_6v1.value = data_db["ps_6v1"];
		form.be_ps_6v2.value = data_db["ps_6v2"];
		form.be_ps_6v3.value = data_db["ps_6v3"];
		form.be_ps_6v4.value = data_db["ps_6v4"];
		
		
		var be_ps_12 = data_db["ps_12"].split('|');
		for (i = 0; i < be_ps_12.length +1; i++)
		{
			if (parseInt(be_ps_12[i+1]))
			{
				form["be_ps_12[" + i +"]"].checked = true;
			}
		}
		form.be_ps_12v0.value = data_db["ps_12v0"];
		form.be_ps_12v1.value = data_db["ps_12v1"];
		form.be_ps_12v2.value = data_db["ps_12v2"];
		form.be_ps_12v3.value = data_db["ps_12v3"];
		form.be_ps_12v4.value = data_db["ps_12v4"];
		
		if (parseInt(data_db["res_en"]))
			form.be_res_en.checked = true;

		form.be_obr_7.value = data_db["obr_7"];
		form.be_obr_14.value = data_db["obr_14"];
		form.be_obr_mon.value = data_db["obr_mon"];

		if (parseInt(data_db["inc_true"]))
			form.be_inc_true.checked = true;
		//form.be_crim.value = data_db["crim"];
		var be_crim = data_db["crim"].split('|');
		for (i = 0; i < be_crim.length +1; i++)
		{
			if (parseInt(be_crim[i+1]))
			{
				form["be_crim[" + i +"]"].checked = true;
			}
		}
		//form.be_mbank.value = data_db["mbank"];
		var be_mbank = data_db["mbank"].split('|');
		for (i = 0; i < be_mbank.length +1; i++)
		{
			if (parseInt(be_mbank[i+1]))
			{
				form["be_mbank[" + i +"]"].checked = true;
			}
		}
		//form.be_st_cr.value = data_db["st_cr"];
		var be_st_cr = data_db["st_cr"].split('|');
		for (i = 0; i < be_st_cr.length +1; i++)
		{
			if (parseInt(be_st_cr[i+1]))
			{
				form["be_st_cr[" + i +"]"].checked = true;
			}
		}
		//form.be_spark.value = data_db["spark"];
		var be_spark = data_db["spark"].split('|');
		for (i = 0; i < be_spark.length +1; i++)
		{
			if (parseInt(be_spark[i+1]))
			{
				form["be_spark[" + i +"]"].checked = true;
			}
		}
		
		
		
		
		if (parseInt(data_db["bailn"]))
			form.be_bailn.checked = true;
		
		
		//form.be_guarantor.value = data_db["guarantor"];
		var be_guarantor = data_db["guarantor"].split('|');
		for (i = 0; i < be_guarantor.length +1; i++)
		{
			if (parseInt(be_guarantor[i+1]))
			{
				form["be_guarantor[" + i +"]"].checked = true;
			}
		}
		
		
	//	form.be_bail.value = data_db["bail"];
		var be_bail = data_db["bail"].split('|');
		for (i = 0; i < be_bail.length +1; i++)
		{
			if (parseInt(be_bail[i+1]))
			{
				form["be_bail[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_first_deb.value = data_db["first_deb"];
		form.be_mkad.value = data_db["mkad"];
		
		//form.be_doc_sob.value = data_db["doc_sob"];
		var be_doc_sob = data_db["doc_sob"].split('|');
		for (i = 0; i < be_doc_sob.length +1; i++)
		{
			if (parseInt(be_doc_sob[i+1]))
			{
				form["be_doc_sob[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_owner.value = data_db["owner"];
		var be_owner = data_db["owner"].split('|');
		for (i = 0; i < be_owner.length +1; i++)
		{
			if (parseInt(be_owner[i+1]))
			{
				form["be_owner[" + i +"]"].checked = true;
			}
		}
		
		
		
		//form.be_obj_type.value = data_db["obj_type"];
		var be_obj_type = data_db["obj_type"].split('|');
		for (i = 0; i < be_obj_type.length +1; i++)
		{
			if (parseInt(be_obj_type[i+1]))
			{
				form["be_obj_type[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_replan.value = data_db["replan"];
		var be_replan = data_db["replan"].split('|');
		for (i = 0; i < be_replan.length +1; i++)
		{
			if (parseInt(be_replan[i+1]))
			{
				form["be_replan[" + i +"]"].checked = true;
			}
		}
		
		
	//	form.be_house_mat.value = data_db["house_mat"];
		var be_house_mat = data_db["house_mat"].split('|');
		for (i = 0; i < be_house_mat.length +1; i++)
		{
			if (parseInt(be_house_mat[i+1]))
			{
				form["be_house_mat[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_found.value = data_db["found"];
		var be_found = data_db["found"].split('|');
		for (i = 0; i < be_found.length +1; i++)
		{
			if (parseInt(be_found[i+1]))
			{
				form["be_found[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_obj_age.value = data_db["obj_age"];
		form.be_wear.value = data_db["wear"];
		
		//form.be_comm.value = data_db["comm"];
		var be_comm = data_db["comm"].split('|');
		for (i = 0; i < be_comm.length +1; i++)
		{
			if (parseInt(be_comm[i+1]))
			{
				form["be_comm[" + i +"]"].checked = true;
			}
		}
		
		var be_bh_type = data_db["bh_type"].split('|');
		for (i = 0; i < be_bh_type.length +1; i++)
		{
			if (parseInt(be_bh_type[i+1]))
			{
				form["be_bh_type[" + i +"]"].checked = true;
			}
		}
		
		var stop_t = data_db["stop"].split('|');
		for (i = 0; i < stop_t.length +1; i++)
		{
			if (parseInt(stop_t[i+1]))
			{
				form["be_stop[" + i +"]"].checked = true;
			}
		}
		
		var be_goal = data_db["goal"].split('|');
		for (i = 0; i < be_goal.length +1; i++)
		{
			if (parseInt(be_goal[i+1]))
			{
				form["be_goal[" + i +"]"].checked = true;
			}
		}
		
		var be_activs = data_db["activs"].split('|');
		for (i = 0; i < be_activs.length +1; i++)
		{
			if (parseInt(be_activs[i+1]))
			{
				form["be_activs[" + i +"]"].checked = true;
			}
		}
		

		show('block');		
	
	});
}

function del(id)
{
//	var div = document.getElementById('info_div');
//	div.innerHTML = '';
	if (!confirm("Точно удалить?")) {
	  return 0;
	};

	
	var tr = document.getElementById('be_id'+id);
	var td = tr.getElementsByTagName('td');
	
	var form = document.forms.be_del_form;
	form.be_prod_id.value		= id;
	var data = getQueryString(form.elements);
//	ajaxQuery('http://old.smartcredit.online/new_products/ajax.php','POST', data, true, function(req)
	ajaxQuery('/new_products/ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
		location.reload();
		//alert(tmp_page);
	});
	//form.submit();
}


var product = new Object();


function add_space(obj)
{
	var tmp = '' + obj.value;
	tmp = tmp.replace(/\D+/g,"");
	obj.value = tmp.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
}

function set_value(v, type, c)
{
	if(debug)
	console.log('[' + v + '] [' + type + '] [' + c + ']');
	
	if ((type == "income") || (type == "inc_4_cre") || (type == "a_izdeti") || (type == "a_skred") || (type == "credit_summ") || (type == "bd_a"))
	{
		var el = document.querySelector('#' + type + '_text');
		if (v == "")
			el.style.color = "red";
		else
			el.style.color = "black";
	}
	else if ((type == "sex") || (type == "arm") || (type == "adr"))
	{
		var el = document.querySelector('#' + type + '_text');
		if (v == "-1")
			el.style.color = "red";
		else
			el.style.color = "black";
	}
	else if ((type == "k_h_miss") || (type == "delay_act"))
	{
		var el = document.querySelector('#' + type + '_text');
		if (c)
			el.style.color = "red";
		else
			el.style.color = "black";
	}
	
	switch(type)
	{
		case 'res':
			product[type] = (c) ? 1 : 0;
			break;
		default:
			product[type] = v;
	}
}

function prod_vis(id, value)
{
	//console.log(hide_products + " " + id + ' ' + value);
	var div = document.querySelector("#prod" + id).parentNode.parentNode;
	if (!value)
		div.style.display = "block";
	if (hide_products && value)
		div.style.display = "none";
	//console.log();
}

function change_view(b)
{
	//console.log(b.value + " " + b.innerHTML);
	var dd = document.querySelectorAll('#result2 > div');
	//console.log(dd.length);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Показать все';
		b.value = 'show';
		hide_products = true;
		//console.log('dd = ' + dd.length);
		for (var i = 1; i<dd.length; i++)
		{
			//console.log('dd[' + i + '] = ' + dd[i].innerHTML);
			//if (dd[i].querySelector('.tab7 > input').checked)
			if (dd[i].querySelector('.res4 > input').checked)
				dd[i].style.display = "none";
		}
	}
	else
	{
		b.innerHTML = 'Исключить выбранные';
		b.value = 'hide';
		hide_products = false;
		for (var i = 1; i<dd.length; i++)
			dd[i].style.display = "block";
	}
	//console.log("after " + b.value + " " + b.innerHTML);
}

function ch_view_b(b)
{
	//console.log(b.value + " " + b.innerHTML);
	//var dd = document.querySelectorAll('#result2 > div');
	//console.log(dd.length);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Режим демонстрации';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;
		//setCookie('bank_view', 'Режим демонстрации', opt);
		setCookie2('bank_view', 'Режим демонстрации', opt.expires);
		b.value = 'show';
		//console.log("h:" + getCookie2('bank_view'));
		//hide_products = true;
		//console.log('dd = ' + dd.length);
		/*for (var i = 1; i<dd.length; i++)
		{
			//console.log('dd[' + i + '] = ' + dd[i].innerHTML);
			//if (dd[i].querySelector('.tab7 > input').checked)
			if (dd[i].querySelector('.res4 > input').checked)
				dd[i].style.display = "none";
		}*/
	}
	else
	{
		b.innerHTML = 'Режим работы';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;
//		setCookie('bank_view', 'Режим работы', opt);		
		setCookie2('bank_view', 'Режим работы', opt.expires);
		b.value = 'hide';
		
		if(debug)
		console.log("s:" + getCookie2('bank_view'));
		//hide_products = false;
		//for (var i = 1; i<dd.length; i++)
		//	dd[i].style.display = "block";
	}
	//console.log("after " + b.value + " " + b.innerHTML);
	//var tmp_r1 = document.querySelector('#result1');
	var tmp_r2 = document.querySelector('#result2');
	var tmp_divs = tmp_r2.querySelectorAll('div > .res2');
	//console.log(tmp_divs.length);
	for (var i = 0; i < tmp_divs.length; i++)
	{
		tmp_divs[i].style.display = (b.value == 'hide') ? "none" : "block";
	}
}

function sort_pod(td)
{
	console.log(td);
	//background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=') no-repeat right 55%
	//background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=') no-repeat right 55%
	var zal = false;
	if((td == 4) || (td == 7))
		zal = true;

	if((td == 2) || (td == 8) || (td == 9) || (td == 10) || zal)
	{
		if (td == sort)
			sort_by = !sort_by;
		sort = td;
		//console.log(td + ": " + sort_by);
		
		var res = document.querySelector('#result2');
		//console.log(res.innerHTML);
		var div = document.querySelectorAll('#result2 > div');
		var divhead = div[0];
		//var ddd = divhead.getElementsByTagName('div');
		var ddd = divhead.childNodes;
		//console.log(ddd[c].innerHTML);
		//console.log(ddd[td].parentNode.innerHTML);
		for (var c = 0; c < ddd.length; c++)
		{
			//console.log(ddd[c].innerHTML);
			if (c == parseInt(td))
			{
				if (sort_by)
				{
					//console.log('up');
					ddd[c].childNodes[0].className = 'sort_up';
					//ddd[td].style.background = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';
				}
				else
				{
					ddd[c].childNodes[0].className = 'sort_d';
					//console.log('down');
					//ddd[td].style.background = 'url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=") no-repeat right 55%;';
				}
			}
			else
				ddd[c].childNodes[0].className = 'sort_no';
		}
		
		//ddd[1].style.background = '';
		
	//console.log("искомый: " + ddd[td].innerHTML);
		
		//console.log(ddd[td].parentNode.innerHTML);
		//console.log(div[0].innerHTML);
		
		var mass_t = new Array();
		var mass_sort = new Array();
		for (var i = 1; i < div.length; i++)
		{
			var div_in = div[i].querySelectorAll('div');
			mass_sort.push(div_in[sort].innerHTML);
			var tmp_a = {};
			tmp_a.key = i;
			tmp_a.sort_by = div_in[sort].innerHTML;
			tmp_a.data = div[i].innerHTML;
			mass_t.push(tmp_a);
		}
		
		//console.log("массив");
	
		if (zal)
		{
			if (sort_by)
				mass_t.sort(sIncrease);
			else
				mass_t.sort(sDecrease);
		}
		else
		{
			if (sort_by)
				mass_t.sort(dIncrease);
			else
				mass_t.sort(dDecrease);
		}
		
		res.innerHTML = "<div>" + divhead.innerHTML + "</div><br>";
		for (var key in mass_t)
		{
			//console.log(mass_t[key].key + " : " + mass_t[key].sort_by);
			res.innerHTML += "<div>" + mass_t[key].data + "</div>";
		}
	}
	var res2 = document.querySelector('#result2');
	//console.log(res2.innerHTML);
}

function show_products()
{
	fill_reds = 0;
	
	var selectElement = document.querySelectorAll('input[name*="goal"]');
	var el = document.querySelector('#goal_text');
	if(debug)
	console.log("goal = " + selectElement.length);
	
	var test_goal = 0;
	for (var i = 0; i < selectElement.length; i++)
	{
		if (selectElement[i].checked)
			test_goal++;
	}
if(debug)
	console.log("test_goal = " + test_goal);
	
	if (test_goal == 0)
		el.style.color = "red";
	else
	{
		el.style.color = "black";
		fill_reds++;
	}
	
	var ent_m = document.querySelectorAll('input[name*="ent["]');
	var not_show_u = document.querySelector('#not_show_u');
	var test_ent = 0;
	if(debug)
	console.log("ent_m.length = " + ent_m.length);
	for (var i = 0; i < ent_m.length; i++)
	{
		if(debug)
		console.log("ent_m[" + i + "] = " +  ent_m[i].parentNode.innerHTML);
		if (ent_m[i].checked)
			test_ent++;
	}
	if(debug)
	console.log("test_ent = " + test_ent);
	if(debug)
	console.log("ttt1 " + not_show_u.getAttribute('class'));
	var e2 = document.querySelector('#ent_text');
	if (test_ent)
	{
		not_show_u.removeAttribute('class');
		e2.style.color = "red";
	}
	else
	{
		not_show_u.setAttribute('class', 'not_show_u');
		e2.style.color = "black";
	}


	if(debug)	console.log("ttt2 " + not_show_u.getAttribute('class'));
		
	var selectElement = document.querySelectorAll('input[name*="activs"]');
	if(debug) console.log("test = " + selectElement.length);
	
	var test_act = 0;
	for (var i = 0; i < selectElement.length; i++)
	{
		if (selectElement[i].checked)
			test_act++;
	}

if(debug)	console.log("test_act = " + test_act);
	
	
	document.querySelector('#result1').innerHTML = '';
	var f_param = document.querySelector('#f_param');
	f_param.innerHTML = '';
	var msg_4_staff = document.querySelector('#msg_4_staff');
	msg_4_staff.innerHTML = '';
	var msg_4_staff2 = document.querySelector('#msg_4_staff2');
	msg_4_staff2.innerHTML = '';
	
		
	var msg_4_staff3 = document.querySelector('#msg_4_staff3');
	msg_4_staff3.innerHTML = '';
	if (test_act == 0)
	{
		//alert("test");
		msg_4_staff3.innerHTML = '<font color="red">Не выбран ни один АКТИВ!!!<font>';
	}
	
//	document.querySelector('#result1').innerHTML = 'Доступные продукты: <button type="button" id="show_butt" onclick="javascript:change_view(this)" value="hide">Исключить выбранные</button>';
	/*var i = 0;
	for (var key in product)
	{
		if (typeof key != 'undefined')
			document.querySelector('#result1').innerHTML += i++ + ":" + key + ' : ' + product[key] + '<br>';
	}
	*/
	var a_miss_1 = false;
	var a_miss_2 = false;
	
	var form = document.forms.pr_form;
	cred_sv = form.credit_summ.value.replace(/\D+/g,"");
	zp = form.income.value.replace(/\D+/g,"");
	cred_sv = (cred_sv != '') ? parseInt(cred_sv) : 0;
	zp = (zp != '') ? parseInt(zp) : 0;
//	console.log(cred_sv);

	var inc_4_cre = form.inc_4_cre.value.replace(/\D+/g,"");
	inc_4_cre = (inc_4_cre != '') ? parseInt(inc_4_cre) : 0;
	f_param.innerHTML += "Доход для погашения(макс. платеж): " + inc_4_cre + "р<br>";
	
	//var pm = form.pm.value;
	//pm = (pm != '') ? parseInt(pm) : 0;
	
	
	
	var adr = form.adr.value;
	//pm = (pm != '') ? parseInt(pm) : 0;
	adr = (adr == "2") ? true : false;
	//console.log(adr);
	
	var ddate = document.querySelector('#bd_a');
	if (ddate.value != '')
	{
		if(debug) console.log(ddate.value);
		var arr;
		var re = new RegExp("[\.\/]");
		arr = ddate.value.split(re);
		if(debug) console.log(arr[0]);
		a_drd = parseInt(arr[0]);
		if(debug) console.log(arr[1]);
		a_drm = parseInt(arr[1]);
		if(debug) console.log(arr[2]);
		a_dry = parseInt(arr[2]);
		if(debug) console.log("a_drm=" + a_drm);
		if(debug) console.log("a_dry=" + a_dry);
		
		if (a_drd && a_drm && a_dry)
		{
			var d = new Date(a_dry, a_drm-1, a_drd);
			//document.querySelector('#result2').innerHTML += '<br>' + d;
			var now = new Date();
			var now_y = now.getFullYear();
			var now_m = now.getMonth();
			var now_d = now.getDate();
			var nd = now_y-parseInt(a_dry)-1;
			if (now_m+1 > parseInt(a_drm))
				nd +=1;
			else
				if (now_m+1 == parseInt(a_drm) && now_d>=parseInt(a_drd))
					nd +=1;
			form.age.value = nd;
		}
		//a_drm 
		//a_dry
	}

	
	//if (adr)
	//	pm = pm*2;
	
	//f_param.innerHTML += "Прожиточный минимум: " + pm + "р<br>";
	
	
	/*var checkbox = document.getElementsByName('goal[0]');
    var is_ngoal = checkbox[0].checked;
	checkbox = document.getElementsByName('goal[1]');
    var is_ipoteka = checkbox[0].checked;
	checkbox = document.getElementsByName('goal[2]');
    var is_garant = checkbox[0].checked;
	checkbox = document.getElementsByName('goal[3]');
    var is_lizing = checkbox[0].checked;
	checkbox = document.getElementsByName('goal[4]');
    var is_refinans = checkbox[0].checked;
	checkbox = document.getElementsByName('goal[5]');
    var is_card = checkbox[0].checked;
	*/
	//var M23 = zp/2;
	
	
	var pkv = inc_4_cre;
	
	var a_skred = form.a_skred.value;
	a_skred = (a_skred != '') ? parseInt(a_skred) : 0;
	
	f_param.innerHTML += "Кредитные обязательства: " + a_skred + "р<br>";
	
	
	var skred = a_skred;
	
	var pmax = pkv - skred;
	f_param.innerHTML += "Pmax: " + pmax + "р<br>";
	
	var a_izdeti = form.a_izdeti.value;
	a_izdeti = a_izdeti.replace(/\D+/g,"");
	var izdeti = parseInt((a_izdeti == '') ? 0 : a_izdeti);
	
	var a_ds = form.a_ds.value;
	a_ds = (a_ds != '') ? parseInt(a_ds) : 0;
	
	
	
	/*if (is_refinans)
	{
		var pkv;
		var skred;
		var srefv;
		var pmax = pkv - skred + srefv;
	}*/

	
//	var refinans_c = form.querySelectorAll("goal");
	//var refinans_c = form.getElementByName("goal[]");
//	refinans_c = refinans_c[4].checked;
//	for (var key in refinans_c)	
//	console.log("key = " + key + ", refinans_c = " + refinans_c.key);
//	console.log("refinans_c = " + refinans_c.values());
	
	
	
	if (document.querySelector('#cr_his').checked)
		fill_reds++;
	
	if (document.querySelector('#res').checked)
		fill_reds++;
	
	if (document.querySelector('input[name=credit_summ]').value != "")
		fill_reds++;
	
	if (document.querySelector('#client').checked)
		fill_reds++;
		
	if (document.querySelector('input[name=bd_a]').value != "")
		fill_reds++;
		
	if (document.querySelector('input[name=income]').value != "")
		fill_reds++;
		
	if (document.querySelector('input[name=inc_4_cre]').value != "")
		fill_reds++;
		
	if (document.querySelector('input[name=a_izdeti]').value != "")
		fill_reds++;
		
	if (document.querySelector('input[name=a_skred]').value != "")
		fill_reds++;
		
	if (document.querySelector('select[name=sex]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=arm]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=adr]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=work]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=c_work]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=c_inc]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=exp_now]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=reg_date]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=work_type]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=work_adr]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=doc_sob]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=owner]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=replan]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=house_mat]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=found]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=spark]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=nal]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=pr_nal]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=ps]').value != "-1")
		fill_reds++;
		
	if (document.querySelector('select[name=psc]').value != "-1")
		fill_reds++;
	
	/*if (document.querySelector('#cr_his').checked)
		fill_reds++;
	
	if (document.querySelector('#cr_his').checked)
		fill_reds++;
	*/
	
	
	var sex = parseInt(form.sex.value);
	
	form.act.value = "get_prod";
	var data_ = getQueryString(form.elements);
	var data = data_.replace(/\s/g, '');
	document.querySelector('#result1').innerHTML = 'Доступные продукты: <button type="button" id="show_butt" onclick="javascript:change_view(this)" value="hide">Исключить выбранные</button> <button type="button" id="show_bank" onclick="javascript:ch_view_b(this)" value="' + ((view_b == "Режим работы") ? "hide" : "show") + '">' + view_b + '</button> <button type="button" onclick="javascript:print_doc()">Печать</button>';

	ajaxQuery('/get_products.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
//		document.querySelector('#result2').innerHTML = tmp_page;
		/*alert(tmp_page);
		var mass = JSON.parse(tmp_page, function(k, v) {
		  console.log(k); // пишем имя текущего свойства, последним именем будет ""
		  return v;       // возвращаем неизменённое значение свойства
		});
		
		*/
		var Item = eval("obj = " + tmp_page);
		//var list_pr = new Array();
		var count = 0;
		var header_tmp = '<div>';
		header_tmp += "<div class='res0' style='cursor: pointer;' onclick='sort_pod(0)'><div>№</div></div>";
		header_tmp += "<div class='res1' style='cursor: pointer;' onclick='sort_pod(1)'><div>Продукт</div></div>";
		header_tmp += "<div class='res2' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(2)'><div class = 'sort_up' style='width:55px;'>От, %</div></div>";
		header_tmp += "<div class='res3' style='cursor: pointer;' onclick='sort_pod(3)'><div>Банк</div></div>";
		header_tmp += "<div class='res4' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(4)'><div style='width:48px;'>Залог</div></div>";
		header_tmp += "<div class='res5' style='cursor: pointer;' onclick='sort_pod(5)'><div>Видимость</div></div>";
		header_tmp += "<div class='res6' style='cursor: pointer;' onclick='sort_pod(6)'><div>Поруч-во</div></div>";
		header_tmp += "<div class='res7' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(7)'><div style='width:115px;'>Залог от 3 лица</div></div>";
		header_tmp += "<div class='res8' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(8)'><div style='width:90px;'>Макс. срок</div></div>";
		header_tmp += "<div class='res9' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(9)'><div style='width:65px;'>Платеж</div></div>";
		header_tmp += "<div class='resa' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(10)'><div style='width:55px;'>Лимит</div></div></div><br>";
		
		document.querySelector('#result2').innerHTML = header_tmp;

		for (var key in Item)
		{

			var perc;
			var bailn;
			var guarantor;
			var bail;
			var p_max_mon;
			var pay_mon_s;
			var max_lim_s;
			
			calculate_pr(Item[key]['Ставка'], false);
			var bcgc = (count & 1);
			//var bcgc = (count & 1) ? '#F0F0F0;' : '#FFFFFF;';
			//var tmp_test = '<div style="background-color:' + bcgc + '">';
			var tmp_test = '<div class = "row_t' + bcgc + '">';
			tmp_test += '<div class = "tab0">' + Item[key]['id'] + '.</div>';
			tmp_test += '<div class = "res0"><i id="prod' + Item[key]['id'] + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail(this.id)">' 
				+ Item[key]['Наименование продукта'] + '</i></div>';
				
			tmp_test +=	'<div class="res1">' + perc + '</div>';
			tmp_test +=	'<div class="res2">' + Item[key]['Банк'] + '</div>';
			tmp_test +=	'<div class="res3">' + bailn + '</div>';
			tmp_test +=	'<div class="res4" style="margin: 5px 20px;"><input type="checkbox" value="1" onclick="javascript:prod_vis(' 
				+ Item[key]['id'] + ', this.checked);"></input></div>';
			tmp_test +=	'<div class="res5">' + guarantor + '</div>';
			tmp_test +=	'<div class="res6">' + bail	+ '</div>';
			tmp_test +=	'<div class="res7">' + /*Item[key]['max_cre_time']*/p_max_mon + ' мес.</div>';
			tmp_test +=	'<div class="res8">' + pay_mon_s + ' р.</div>';
			tmp_test +=	'<div class="res9 gr">' + max_lim_s + '</div><br>';
			
			calculate_pr(Item[key]['max_percent'], true);
			
			tmp_test += '<div class ="tab0">расчет по макс. ставке:</div>';
			tmp_test += '<div class="res1">' + perc + '</div>';
			tmp_test += '<div class="res7">' + p_max_mon + ' мес.</div>';
			tmp_test += '<div class="res8">' + pay_mon_s + ' р.</div>';
			tmp_test += '<div class="res9 gr">' + max_lim_s + '</div><br></div>';
															
			document.querySelector('#result2').innerHTML += tmp_test;
		}
		
		function calculate_pr(stavka, fix)
		{
			var M23 = zp/2;
			a_miss_2 = false;
			if (!fix)
				count++;
			//list_pr[key]=Item[key]['Наименование продукта'];
		//  document.querySelector('#result2').innerHTML += '<div><div class="tab0"><i id="prod' + key + '" style="cursor: pointer;" onclick="show_detail(this.id)">' + Item[key]['Наименование продукта'] + '</i></div><div class="tab3">' + Item[key]['Ставка'] + '</div><div class="tab4">' + Item[key]['Банк'] + '</div><br>';
			bailn = Item[key]['bailn'] == "1" ? "Залог" : "-";
			//bailn = Item[key]['bailn'] == "1" ? "С зал." : "Без з.";
			var guarantor0 = /2\|1\|\d/.test(Item[key]['guarantor']) ? "Физ" : "";
			var guarantor1 = /2\|\d\|1/.test(Item[key]['guarantor']) ? "Юр" : "";
			guarantor = (guarantor0 != '' && guarantor1 != '') ? guarantor0 + '/' + guarantor1 : ( guarantor0 == '' && guarantor1 == '' ? '-' : guarantor0 + '' + guarantor1);
			//guarantor = (guarantor0 != '' && guarantor1 != '') ? guarantor0 + '/' + guarantor1 : ( guarantor0 == '' && guarantor1 == '' ? 'Без пор.' : guarantor0 + '' + guarantor1);
			//var guarantor = guarantor0 + ', ' + guarantor1;
			var bail0 = /2\|1\|\d/.test(Item[key]['bail']) ? "Физ." : "";
			var bail1 = /2\|\d\|1/.test(Item[key]['bail']) ? "Юрлицо" : "";
			//bail = (bail0 != '' && bail1 != '') ? bail0 + '/' + bail1 : ( (bail0 == '' && bail1 == '') ? 'Без залога' : bail0 + '' + bail1);
			bail = (bail0 != '' && bail1 != '') ? bail0 + '/' + bail1 : ( (bail0 == '' && bail1 == '') ? '-' : bail0 + '' + bail1);
			//var pay_mon = =ОКРУГЛВВЕРХ(E21*((E7/12)/(1-СТЕПЕНЬ(1+E7/12;-(E9))));0)+E21*E11
			//(E7/12)/(1-СТЕПЕНЬ(1+E7/12;-(E9)))
			var max_mon = parseInt(Item[key]['max_cre_time']);
			
			if (sex != -1)
			{
				var age = form.age.value;
				var max_e_e_m = parseInt(Item[key]['max_end_age_m']);
				var max_e_e_w = parseInt(Item[key]['max_end_age_w']);
				var max_e_e = (sex == 0) ? max_e_e_m : max_e_e_w;
				max_e_e = (max_e_e * 12);
				var now_d = new Date();
				var now_m = now_d.getMonth()+1;
				var now_y = 1900 + now_d.getYear();
				var now_age = now_y*12 + now_m - a_drm - 12*a_dry;
				var max_mon_new = max_e_e - now_age;
				var st_tmp = "test: old_max_mon=" + max_mon + " max_mon_new=" + max_mon_new + " max_e_e=" + max_e_e + " now_age=" + now_age;
				max_mon = ((max_mon_new < max_mon) && (max_e_e > 0)) ? max_mon_new : max_mon;
				st_tmp += " max_mon=" + max_mon + " bank=" + Item[key]['Наименование продукта'];
				if(debug) console.log(st_tmp);
			}
			
			
			
			//console.log(count + " :: " + max_mon + ", " + now_age + ", " + max_e_e);
			
			
			max_mon = (max_mon == 0) ? -1 : 0-max_mon;
			
			var pm = parseInt(Item[key]['pm']);
			if (adr)
				pm = pm*2;
	
			var m = Item[key]['goal'].split('|');
			
			var is_ngoal = parseInt(m[1]);
			var is_ipoteka = parseInt(m[2]);
			var is_garant = parseInt(m[3]);
			var is_lizing = parseInt(m[4]);
			var is_refinans = parseInt(m[5]);
			var is_card = parseInt(m[6]);
			
			
			
//			if (!is_refinans && !is_ipoteka && !is_lizing && !is_garant && (is_ngoal || is_card))
			if (is_ngoal || is_card)
			{
				var test = zp - pmax - skred - pm - izdeti*10000;
				f_param.innerHTML += "Д - Pmax - Sкред - PM - (Иждети)*10000: " + test + "р<br>";
				//console.log("test");
				//console.log(zp);
				//console.log(pmax);
				//console.log(skred);
				//console.log(pm);
				//console.log(izdeti);
				//console.log(test);
				if (test <=0)
				{
					msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
					a_miss_1 = true;
				}
				else
					M23 = (pmax > 0) ? pmax : 0;
					//M23 = (pmax < skred) ? 0 : pmax;
				/*if(zp - pmax - skred - pm – a_izdeti*10 000>0)
				{
					
				}*/
			}
			else if (is_ipoteka)
			{
				//var izdeti = parseInt((a_izdeti == '') ? 0 : a_izdeti);
				var test = zp + parseInt((a_ds == "") ? "0" : a_ds) - pmax - skred - pm - izdeti*10000;
				f_param.innerHTML += "Д + ДС - Pmax - Sкред - PM - (Иждети)*10000: " + test + "р<br>";
				//console.log("test");
				//console.log(zp);
				//console.log(pmax);
				//console.log(skred);
				//console.log(pm);
				//console.log(izdeti);
				//console.log(test);
				if (test <=0)
				{
					msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
					a_miss_1 = true;
				}
				else
					M23 = (pmax > 0) ? pmax : 0;
					//M23 = (pmax < skred) ? 0 : pmax;
					//M23 = pmax;
			}
			
			
			
			var min_income = parseInt(Item[key]['min_income']);
			var dmin_t = zp - pmax - skred;
			
			if (!is_refinans)
			{
			
				//console.log(count + " :: " + zp + ", " + pmax + ", " + skred + ", " + dmin_t + ", " + min_income + "");
				if (dmin_t <= min_income)
				{
					msg_4_staff2.innerHTML = '<font color="orange">Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж<font>';
					a_miss_2 = true;
				}
			}
			
			var st = parseFloat(stavka)/100;
			var f_ = st/12;
			var s_ = 1+st/12;
			var t_ = Math.pow(1+st/12, max_mon);
//			var pay_mon = Math.ceil(cred_sv*((st/12)/(1-Math.pow(1+st/12, max_mon))));
//			var max_lim = zp;
			var e15 = 0;
			//var M23 = zp/2;
			
		//	console.log("M23 " + M23);
			
			var m21 = Math.round(M23/(((st/12)/(1-Math.pow((1+st/12),max_mon)))));
			var max_lim = m21/(1-e15);
			//console.log("max_lim0 " + max_lim);
			//console.log("m21 " + m21);
			//console.log("e15 " + e15);
			max_lim = (parseInt(max_lim) < parseInt(Item[key]['max_credit'])) ? max_lim : Item[key]['max_credit'];
			max_lim = (cred_sv < max_lim) ? cred_sv : max_lim;
			
			//console.log("cred_sv " + cred_sv);
			//cred_sv = (cred_sv > max_lim) ? max_lim : cred_sv;
			//var pay_mon = Math.ceil(cred_sv*((st/12)/(1-Math.pow(1+st/12, max_mon))));
			if(debug) console.log("max_lim " + max_lim);
			if(debug) console.log("st " + st);
			if(debug) console.log("max_mon " + max_mon);
			
			var a_test = (1-Math.pow(1+st/12, max_mon));
			
			if(debug) console.log("a_test " + a_test);
			if (st == 0)
				a_test = 1;
			var pay_mon = Math.round(max_lim*((st/12)/a_test));
			
			if (st == 0)
				pay_mon = Math.round(max_lim/(0-max_mon));
			
			if(debug) console.log("pay_mon " + pay_mon);
			
			//Д- Р (платеж по новому кредиту)  - РМ –(Иждети) *10 000>0, или если НЕТ - информационное сообщение: 
			//«Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж»


			//Д- Р(платеж по новому кредиту)    > Дмин, если НЕТ – информационное сообщение:
			//«Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж».  Лимит при этом заменяется значением «НЕТ»,  а в поле Платеж показывается значение Ежемесячного платежа

			if (is_refinans)
			{
			//	console.log("is_refinans " + is_refinans);
			//	console.log("skred " + skred);
			//	console.log("pay_mon " + pay_mon);
				
				if (skred - pay_mon > 0)
				{
					var test_ref = zp - pay_mon - pm - izdeti*10000;
					if (!test_ref)
					{
						msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
						a_miss_1 = true;
					}
					
					dmin_t = zp - pay_mon;
				//	console.log(count + " :: " + zp + ", " + pmax + ", " + skred + ", " + dmin_t + ", " + min_income + "" + pay_mon);
					if (dmin_t <= min_income)
					{
						msg_4_staff2.innerHTML = '<font color="orange">Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж<font>';
						a_miss_2 = true;
					}
				}
			}
			
			p_max_mon = 0 - max_mon;
			//console.log(count + " :: " + st + ", " + max_mon + ", " + s_ + ", " + t_ + ", " + pay_mon + "");
			
			
			//var pay_mon_s = '' + (parseInt(pay_mon) % 1000);
			pay_mon_s = '' + pay_mon;
			pay_mon_s = pay_mon_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			var pmt = pay_mon;
			/*while (parseInt(pmt/1000))
			{
				pay_mon_s = (parseInt(pmt/1000) % 1000) + ' ' + pay_mon_s;
				
				console.log(pay_mon_s + ' :::: ' + pmt);
				pmt = pmt/1000;
			}*/
			
			
			max_lim_s = '' + (parseInt(max_lim) % 1000);
			var mlt = max_lim;
			/*while (parseInt(mlt/1000))
			{
				max_lim_s = (parseInt(mlt/1000) % 1000) + ' ' + max_lim_s;
				mlt = mlt/1000;
			}*/
			
			if (a_miss_1)
			{
				//max_lim = "НЕТ";
				max_lim_s = "НЕТ";
				pay_mon = 0;
			}
			else if (a_miss_2)
			{
				//max_lim = "<font color='red'><b>НЕТ</b><font>";
				max_lim_s = "<font color='red'><b>НЕТ</b></font>";
				//pay_mon = skred;
			}
			else
			{
				//max_lim = max_lim + " р.";
				max_lim_s = mlt + " р.";
				max_lim_s = max_lim_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			}
			
			if (stavka == 0)
				stavka = '<font color="red">0!</font>';
			
			perc = stavka;
			/*document.querySelector('#result2').innerHTML 	+= '<div><div class ="tab0"><i id="prod' + Item[key]['id'] + '" style="cursor: pointer;" onclick="show_detail(this.id)">' 
															+ Item[key]['Наименование продукта'] + '</i></div><div class="res1">' + stavka + '</div><div class="res2">' 
															+ Item[key]['Банк'] + '</div><div class="res3">' + bailn 
															+ '</div><div class="res4" style="margin: 5px 20px;"><input type="checkbox" value="1" onclick="javascript:prod_vis(' 
															+ Item[key]['id'] + ', this.checked);"></input></div><div class="res5">' + guarantor + '</div><div class="res6">' + bail 
															+ '</div><div class="res7">' + p_max_mon + ' мес.</div><div class="res8">' + pay_mon_s 
															+ ' р.</div><div class="res9">' + max_lim_s + '</div><br></div>';*/
		}
		//alert(count);
		//alert(mass);
		
//		alert(Item[1]['Банк']);
		var okon2 = (count == 1) ? "" : ((count > 2 && count < 5) ? "а" : "ов");
		var okon1 = (count == 1) ? "" : "о";
		var tmp_e = '<span id="msg_4_staff3"></span>';
		document.querySelector('#res_num').innerHTML = "Результаты. Найден" + okon1 + " " + count + " продукт" + okon2;// + tmp_e;
		find_results = count;
		
		console.log ("stat: fill_reds=" + fill_reds + ", count=" + find_results);
		
		var tmp_r2 = document.querySelector('#result2');
		var tmp_divs = tmp_r2.querySelectorAll('div > .res2');
		//console.log(tmp_divs.length);
		for (var i = 0; i < tmp_divs.length; i++)
		{
			tmp_divs[i].style.display = (view_b == 'Режим работы') ? "none" : "block";
		}
	});
}

function show_detail(id)
{
	//alert(id);
	id = id.replace("prod", "");
	var data = 'act=detail&pr_id='+id;
	ajaxQuery('/get_products.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
		//document.querySelector('#prod_detail').style.overflow = "auto";
		document.querySelector('#prod_detail').innerHTML = "";
		var Item = eval("obj = " + tmp_page);
		//alert(tmp_page);
		document.querySelector('#prod_detail').innerHTML += "<br>Продукт <b>" + Item['product_name'] + '</b> от банка <i>' + Item['bank_name'] + "</i>";
		document.querySelector('#prod_detail').innerHTML += "<br>Минимальная ставка " + Item['min_percent'] + '%, П/Д ' + Item['pd'] + "%, LTV " + Item['discont'] + "%. ";
		if (Item['res'] == 1)
			document.querySelector('#prod_detail').innerHTML += "Кредитует нерезидентов. ";
		if (Item['cr4other'] == '1')	//not use in search!
			document.querySelector('#prod_detail').innerHTML += "Кредитует \"улицу\".<br>";
		else if (Item['cr4other'] == '0')
			document.querySelector('#prod_detail').innerHTML += "Не кредитует \"улицу\".<br>";
		else //if (Item['res'] == 1)
			document.querySelector('#prod_detail').innerHTML += "<br>";
		document.querySelector('#prod_detail').innerHTML += "Максимальный возраст на момент погашения м/ж : " + Item['max_end_age_m'] + " / " + Item['max_end_age_w'] + " лет<br>";
		document.querySelector('#prod_detail').innerHTML += "Максимальный размер выдаваемого кредита: " + Item['max_credit'] + " руб. Минимальный доход: " + Item['min_income'] + " руб.<br>";
		
		document.querySelector('#prod_detail').innerHTML += "Описание:<br>" + Item['description'];
		/*for (var key in Item)
		{
			
			document.querySelector('#prod_detail').innerHTML += '<br>' + key;
		}*/
		show('block');
	});
}

function import_pod(id)
{
	if (confirm("Импортируем из анкеты?"))
	{
		var form = document.forms.get_form;
		form.act.value = "load_ank";
		var data = getQueryString(form.elements);
		ajaxQuery('/get_products.php','POST', data, false, function(req) 
		{
			var tmp_page = req.responseText;
			document.querySelector('#result2').innerHTML = '';
			var mass = new Array();
			mass = JSON.parse(tmp_page);
			var sum = mass['sum'];
			sum = sum.replace(new RegExp("[^1234567890]",'g'), '');
			
			var form = document.forms.pr_form;
			form.credit_summ.value = sum;
			var str = mass['str'];
			mass = str.split('<br>');
			var res = new Array();
			
			var dr_day, dr_mon, dr_year;
			for (i=1; i<mass.length-1; i++)
			{
				var tmp = mass[i].split(':');
				switch (tmp[0])
				{
					case 'Дети' :
						tmp[1] = ((tmp[1] == "нет") || (tmp[1] == "Нет")) ? 0 : tmp[1];
					case 'ДР_день' :
					case 'ДР_месяц' :
					case 'ДР_год' :
					case 'Стаж_работы_месяц' :
					case 'Стаж_работы_лет' :
					case 'Доход' :
					case 'Доходы_супруга' :
					case 'Сумма_ежемес_плат' :
						tmp[1] = tmp[1].replace(new RegExp("[^1234567890]",'g'), '');
					case 'Гражданство' :
					case 'Прописка' :
					case 'Адрес_организации' :
					case 'Подтверждение_трудоустройства' :
					case 'Действующие_кредиты' :
					case 'Срок_получение' :
						tmp[1] = tmp[1].replace(new RegExp("\s",''), '');
						res[tmp[0]] = tmp[1];
						break;
					default:
						break;
				}
								
				if (tmp[0] == 'ДР_день')
					dr_day = tmp[1];
				if (tmp[0] == 'ДР_месяц')
					dr_mon = tmp[1];
				if (tmp[0] == 'ДР_год')
					dr_year = tmp[1];

			}

			if (dr_day && dr_mon && dr_year)
			{
				var d = new Date(dr_year, dr_mon-1, dr_day);
				document.querySelector('#result2').innerHTML += '<br>' + d;
				var now = new Date();
				var now_y = now.getFullYear();
				var now_m = now.getMonth();
				var now_d = now.getDate();
				var nd = now_y-parseInt(dr_year)-1;
				if (now_m+1 > parseInt(dr_mon))
					nd +=1;
				else
					if (now_m+1 == parseInt(dr_mon) && now_d>=parseInt(dr_day))
						nd +=1;
				form.age.value = nd;
			}
			
			var work_st = 0;
			var work_st_f = false;
			
			if (res['Стаж_работы_лет'] != null)
			{
				work_st = 12*parseInt(res['Стаж_работы_лет']);
				work_st_f = true;
			}

			if (res['Стаж_работы_месяц'] != null)
			{
				work_st_f = true;
				work_st += parseInt(res['Стаж_работы_месяц']);
			}
			if (work_st_f)
			{
				//alert(work_st);
				if (work_st < 3)
					form.exp_now.value = "0";
				else if (work_st < 7)
					form.exp_now.value = "1";
				else if (work_st < 13)
					form.exp_now.value = "2";
				else if (work_st < 25)
					form.exp_now.value = "3";
				else if (work_st < 37)
					form.exp_now.value = "4";
				else
					form.exp_now.value = "5";

			}
			if (res['Доход'] != null)
				form.income.value = res['Доход'];
			if (res['Гражданство'] != null)
			{
				if(/Россия/i.test(res['Гражданство']))
				{
					form.res.checked = true;
				}
				else
					form.res.checked = false;
			}
			if (res['Прописка'] != null)
			{
				if(/Москва/i.test(res['Прописка']))
				{
					form.adr.value = 0;
				}
				else if(/Московская обл/i.test(res['Прописка']) || /МО/i.test(res['Прописка']))
				{
					form.adr.value = 1;
				}
				else
					form.adr.value = 2;
			}
			if (res['Адрес_организации'] != null)
			{
				if(/Москва/i.test(res['Адрес_организации']))
				{
					form.work_adr.value = 0;
				}
				else if(/Московская обл/i.test(res['Адрес_организации']) || /МО/i.test(res['Адрес_организации']))
				{
					form.work_adr.value = 1;
				}
				else
					form.work_adr.value = 2;
			}
			if (res['Подтверждение_трудоустройства'] != null)
			{
				if(/Трудовая_книжка/i.test(res['Подтверждение_трудоустройства']))
				{
					form.c_work.value = 0;
				}
				else if(/Трудовой_договор/i.test(res['Подтверждение_трудоустройства']))
				{
					form.c_work.value = 1;
				}
				else if(/Не подтверждается/i.test(res['Подтверждение_трудоустройства']))
				{
					form.c_work.value = 4;
				}
				else if(/Иное/i.test(res['Подтверждение_трудоустройства']))
				{
					form.c_work.value = 3;
				}
				else
					form.c_work.value = -1;
			}
			if (res['Действующие_кредиты'] != null)
			{
				if(/да/i.test(res['Действующие_кредиты']))
				{
					form.cr_his.checked = true;
				}
				else
					form.cr_his.checked = false;
			}
			if (res['Доходы_супруга'] != null)
			{
				form.a_ds.value = res['Доходы_супруга'];
			}
			if (res['Дети'] != null)
			{
				form.a_izdeti.value = res['Дети'];
			}
			if (res['Сумма_ежемес_плат'] != null)
			{
				form.a_skred.value = res['Сумма_ежемес_плат'];
			}
			if (res['Срок_получение'] != null)
			{
				//
			}
			show_products();
		});
	} else {
		return false;
	}

}
function save_pod(id)
{
	if (confirm("Сохранить данные для анкеты №" + id + "?"))
	{
		var form = document.forms.pr_form;
		form.act.value = "save_pod";
		form.find_results.value = find_results;
		var fill_perc = ((100 * fill_reds) / all_reds).toFixed(2);
		form.fill_reds.value = fill_perc + "%(" + fill_reds + "/" + all_reds + ")";
		//var data = getQueryString(form.elements);
		var data_ = getQueryString(form.elements);
		var data = data_.replace(/\s/g, '');

		ajaxQuery('/get_products.php','POST', data, false, function(req)
		{
			var tmp_page = req.responseText;
			if (tmp_page == "OK")
				alert("Данные успешно сохранены")
			else
				alert("Ошибка!\nСообщите администратору:\n" + tmp_page);
			//document.querySelector('#result2').innerHTML = tmp_page;
		});
	} else {
		return false;
	}

}
function load_pod(id)
{
	if (confirm("Загрузить данные для анкеты №" + id + "?"))
	{
		var form = document.forms.get_form;
		form.act.value = "load_pod";
		var data = getQueryString(form.elements);
		ajaxQuery('/get_products.php','POST', data, false, function(req)
		{
			var tmp_page = req.responseText;
			var mass = new Array();
			mass = JSON.parse(tmp_page);
			var form = document.forms.pr_form;
			for (var key in mass)
			{
				console.log(mass[key]);
				if (mass[key] == '')
					continue;
				else if (mass[key] == 'find_results')
					continue;
				else if (mass[key] == 'fill_reds')
					continue;
				else if ( /\d\|\d/.test(mass[key]) )
				{
					var m = mass[key].split('|');
					for (i=0; i<m.length-1; i++)
					{
						var tr_f;
						if (m[i+1] == 1)
							tr_f = true;
						//	form.elements[key + '[' + i + ']'].checked = true;
						else
							tr_f = false;
						//	form.elements[key + '[' + i + ']'].checked = false;
						document.querySelector('input[name="' + key + '[' + i + ']' + '"]').checked = tr_f;
					}
				}
				else if (key != 'id')// && key != 'anketa_id')
				{
					
					var inp = document.querySelector('[name="' + key + '"]');
						//console.log(key);
						if (inp['type'] == 'checkbox')
							inp.checked = (mass[key] == 0) ? false : true;
						else if (key == 'credit_summ')
							inp.value = mass[key].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
						else if (key == 'income')
							inp.value = mass[key].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
						else if (key == 'inc_4_cre')
							inp.value = mass[key].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
						else
							inp.value = mass[key];
						
					set_value(inp.value, inp['name'], inp.checked);
				}
			}
			show_products();
		});
		
	} else {
		return false;
	}

}

function dIncrease(i, ii)
{ // По возрастанию
	i = parseInt(i.sort_by);
	ii = parseInt(ii.sort_by);
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function dDecrease(i, ii)
{ // По убыванию
	i = parseInt(i.sort_by);
	ii = parseInt(ii.sort_by);
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function sIncrease(i, ii)
{ // По возрастанию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function sDecrease(i, ii)
{ // По убыванию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}




function ajaxQuery(url, method, param, async, onsuccess, onfailure) 
{
	var xmlHttpRequest = new XMLHttpRequest();
	var callback = function(r) 
	{ 
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r)); 
	};
	
	if(async) 
	{ 
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } } 
	}
	xmlHttpRequest.open(method, url, async);
	if(method == 'POST') { xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); }
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}



function getQueryString(tmp) {
	var queryString = '';
	[].forEach.call(tmp, function(x) {
		if(x.type=='radio' && !x.checked) { return; }
		queryString += x.name+'='+(x.type=='checkbox' && !x.checked ? 0 : getInputValue(x))+'&';
	});
	return queryString.slice(0, -1);
}

function getInputValue(input) {

	if(input.tagName.toLowerCase()=='select') { 
		input = input.querySelector('option:checked');	}
	var type = ['input', 'option', 'textarea'].indexOf(input.tagName.toLowerCase())!=-1 ? 0 : 1;
	Types = [function() { return input.value; }, function() {return input.textContent; }];
	return Types[type]();
}

function print_doc()
{
	window.print();
}


function setCookie(name, value, options)
{
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name)
{
  setCookie(name, "", {
    expires: -1
  })
}

function setCookie2(name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie2(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function getCookie(name)
{
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

//not use just next all

function getFormElems(form) 
{
	var x, y, result = [], thisFormElements = form.elements;
	for(y = 0; y < thisFormElements.length; y++)
	{
		result.push(thisFormElements[y]);
	}
	return result;
}

/*function convert(str) 
{
	var win = {1025: 168, 1105: 184, 8470: 185, 8482: 153};
	var ret = [].map.call(str, function(X-) { t = x.charCodeAt(0); t = win[t] ? win[t] : (t > 1039 ? t-848 : t); return (t < 16 ? '%0' : '%') + t.toString(16); });
	return ret.join('');
}*/
function serialize( mixed_val ) 
{    // Generates a storable representation of a value
    // 
    // +   original by: Ates Goral (http://magnetiq.com)
    // +   adapted for IE: Ilia Kantor (http://javascript.ru)
 
    switch (typeof(mixed_val)){
        case "number":
            if (isNaN(mixed_val) || !isFinite(mixed_val)){
                return false;
            } else{
                return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";
            }
        case "string":
            return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";
        case "boolean":
            return "b:" + (mixed_val ? "1" : "0") + ";";
        case "object":
            if (mixed_val == null) {
                return "N;";
            } else if (mixed_val instanceof Array) {
                var idxobj = { idx: -1 };
		var map = []
		for(var i=0; i<mixed_val.length;i++) {
			idxobj.idx++;
                        var ser = serialize(mixed_val[i]);
 
			if (ser) {
                        	map.push(serialize(idxobj.idx) + ser)
			}
		}                                       

                return "a:" + mixed_val.length + ":{" + map.join("") + "}"

            }
            else {
                var class_name = get_class(mixed_val);
 
                if (class_name == undefined){
                    return false;
                }
 
                var props = new Array();
                for (var prop in mixed_val) {
                    var ser = serialize(mixed_val[prop]);
 
                    if (ser) {
                        props.push(serialize(prop) + ser);
                    }
                }
                return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
            }
        case "undefined":
            return "N;";
    }
 
    return false;
}
