<?php
if (!defined('SYSTEM_START_9876543210')) exit;

?>
<script>
let db_users = '<?=$dbListUsers ?>';
</script>

    <div class="row">
        <div class="col-lg-6">
            <table class="table table-striped table-dark" id="wokrlist">
                <thead>
                <tr>
                    <th scope="col">ФИО</th>
                    <th scope="col">Последняя страница</th>
                    <th scope="col">Время последнего действия</th>
                    <th scope="col">Звонки</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Детали</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="col-lg-3">
            <table class="table table-striped table-dark" id="loginlist">
                <thead>
                <tr>
                    <th scope="col">ФИО</th>
                    <th scope="col">Время входа</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="col-lg-3">
        </div>
    </div>

<?php
//echo '<script type="text/javascript" src="/scripts/metriks.js?' . rand() . '"></script>';
echo '<script type="text/javascript" src="/scripts/newWorkTable.js?' . rand() . '"></script>';
?>
