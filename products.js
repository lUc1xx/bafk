var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

//var tab = document.querySelector('#products_table');
//var row_list = tab.querySelectorAll('tr');
var row_list;
var list_month = new Array('3', '6', '12', '18', '24', '36', '60', '60p', 'all');
var fix_mass = new Array();
fix_mass["3"] = 0;
fix_mass["6"] = 1;
fix_mass["12"] = 2;
fix_mass["18"] = 3;
fix_mass["24"] = 4;
fix_mass["36"] = 5;
fix_mass["60"] = 6;
fix_mass["60p"] = 7;
fix_mass["all"] = 8;

var timeout = 1000;

var copy_mass = new Array();
copy_mass["be_psat"] = new Array(5);
copy_mass["be_psct"] = new Array(5);

var hide_products = false;
//var view_b = 'Режим работы';
var view_b = 'Режим демонстрации';

var cred_sv = 0;
var zp = 0;

if (getCookie2('bank_view') != undefined)
	view_b = getCookie2('bank_view');
/*var tmp_r2 = document.querySelector('#result2');
var tmp_divs = tmp_r2.querySelectorAll('div > .res2');

for (var i = 0; i < tmp_divs.length; i++)
{
	tmp_divs[i].style.display = (view_b == 'Режим работы') ? "none" : "block";
}*/
//console.log(view_b);
window.onload=function(){


	row_list = document.querySelectorAll('#products_table tr[id*="be_id"]>td:nth-of-type(6)');
	//console.log(row_list.length);

	//var div = row_list[0].querySelector('div[name="list_res"]').innerHTML;

	var test = new Array();
	for (var i = 1; i < row_list.length; i++)
	{
		var bank = row_list[i].innerHTML.replace('&nbsp;','');
		test.push(bank);
	}

	var i = test.length;
	test.sort();

	while (i--) {
		if (test[i] == test[i-1]) {
			test.splice(i, 1);
		}
	}

	var selector = document.querySelector('#prod_selector');

	for (var i = 0; i<test.length; i++)
	{
		var newOption = new Option(test[i], test[i]);
		selector.appendChild(newOption);
	}
	
	set_psct();
	set_psat();
}


function reg_flags(el, n1, n2)
{
	/*var el1 = document.querySelector('input[name=' + n1 + ']');
	var el2 = document.querySelector('input[name=' + n2 + ']');
	if ((el.name == n1) && el2.checked && el.checked)
		el2.checked = false;
	else if ((el.name == n2) && el1.checked && el.checked)
		el1.checked = false;*/
}

function add_pd_set()
{
	var mass_ent = new Array();
	mass_ent['<'] = '&lt;';
	mass_ent['<='] = '&lt;=';
	mass_ent['='] = '=';
	var be_pd_wrap_data = document.querySelector('#be_pd_wrap_data');
	var set_pd_min = document.querySelector('#set_pd_min').value;
	var set_pd_max = document.querySelector('#set_pd_max').value;
	var set_pd_val = document.querySelector('#set_pd_val').value;
	var set_pd_min_sel = document.querySelector('#set_pd_min_sel');
	set_pd_min_sel = set_pd_min_sel.options[set_pd_min_sel.selectedIndex].value;
	var set_pd_max_sel = document.querySelector('#set_pd_max_sel');
	set_pd_max_sel = set_pd_max_sel.options[set_pd_max_sel.selectedIndex].value;
	
	if (be_pd_wrap_data.innerHTML == '')
	{
		var ul = document.createElement('ul');
		be_pd_wrap_data.appendChild(ul);
	}
	
	var ul = be_pd_wrap_data.querySelector('ul');
	
	if ((set_pd_min_sel == '-1') && (set_pd_max_sel == '-1'))
	{
		alert('Выберите хотя бы одно условие!');
		return;
	}
	
	if (set_pd_val == '')
	{
		alert('Введите значение П/Д!');
		return;
	}
	
	var tmp = '';
	
	if (set_pd_min_sel != '-1')
	{
		if (set_pd_min == '')
		{
			alert('Введите минимальное значение FICO!');
			return;
		}
		tmp += set_pd_min + ' ' + mass_ent[set_pd_min_sel] + ' ';
	}
	
	tmp += 'FICO';
	
	if (set_pd_max_sel != '-1')
	{
		if (set_pd_max == '')
		{
			alert('Введите максимальное значение FICO!');
			return;
		}
		tmp += ' ' + mass_ent[set_pd_max_sel] + ' ' + set_pd_max;
	}
	
	tmp += '; значение: ' + set_pd_val;
	
	console.log(tmp);
	
	var li = document.createElement('li');
		li.innerHTML = '<span name="pd_val">Условие: ' + tmp + '%</span> <span onclick="javascript:del_pd(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
	ul.appendChild(li);
}

function add_fico_set()
{
	var mass_ent = new Array();
	mass_ent['<'] = '&lt;';
	mass_ent['<='] = '&lt;=';
	mass_ent['='] = '=';
	var be_fico_wrap_data = document.querySelector('#be_fico_wrap_data');
	var set_fico_min = document.querySelector('#set_fico_min').value;
	var set_fico_max = document.querySelector('#set_fico_max').value;
	
	var set_fico_min_sel = document.querySelector('#set_fico_min_sel');
	set_fico_min_sel = set_fico_min_sel.options[set_fico_min_sel.selectedIndex].value;
	var set_fico_max_sel = document.querySelector('#set_fico_max_sel');
	set_fico_max_sel = set_fico_max_sel.options[set_fico_max_sel.selectedIndex].value;
	
	if (be_fico_wrap_data.innerHTML == '')
	{
		var ul = document.createElement('ul');
		be_fico_wrap_data.appendChild(ul);
	}
	
	var ul = be_fico_wrap_data.querySelector('ul');
	
	if ((set_fico_min_sel == '-1') && (set_fico_max_sel == '-1'))
	{
		alert('Выберите хотя бы одно условие!');
		return;
	}
	
	var tmp = '';
	
	if (set_fico_min_sel != '-1')
	{
		if (set_fico_min == '')
		{
			alert('Введите минимальное значение FICO!');
			return;
		}
		tmp += set_fico_min + ' ' + mass_ent[set_fico_min_sel] + ' ';
	}
	
	tmp += 'FICO';
	
	if (set_fico_max_sel != '-1')
	{
		if (set_fico_max == '')
		{
			alert('Введите максимальное значение FICO!');
			return;
		}
		tmp += ' ' + mass_ent[set_fico_max_sel] + ' ' + set_fico_max;
	}
	
	console.log(tmp);
	
	var li = document.createElement('li');
		li.innerHTML = '<span name="fico_val">Условие: ' + tmp + '</span> <span onclick="javascript:del_pd(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
	ul.appendChild(li);
}

function add_reg()
{
	var ul = document.querySelector('#reg_params');
	var inp = document.querySelector('#reg_name');
	
	if (inp.value == '')
	{
		alert('Ничего не введено!');
		return 0;
	}
	
	var li = document.createElement('li');
		li.innerHTML = '<span name="reg_value">' + inp.value + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
	ul.appendChild(li);
	
	inp.value = '';
}

function add_reg_adr()
{
	var ul = document.querySelector('#adr_params');
	var inp = document.querySelector('#adr_name');
	
	if (inp.value == '')
	{
		alert('Ничего не введено!');
		return 0;
	}
	
	var li = document.createElement('li');
		li.innerHTML = '<span name="reg_value">' + inp.value + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
	ul.appendChild(li);
	
	inp.value = '';
}

function add_reg_work_adr()
{
	var ul = document.querySelector('#work_adr_params');
	var inp = document.querySelector('#work_adr_name');
	
	if (inp.value == '')
	{
		alert('Ничего не введено!');
		return 0;
	}
	
	var li = document.createElement('li');
		li.innerHTML = '<span name="reg_value">' + inp.value + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
	ul.appendChild(li);
	
	inp.value = '';
}

function del_reg(el)
{
	el.parentNode.parentNode.removeChild(el.parentNode);
}

function del_pd(el)
{
	el.parentNode.parentNode.removeChild(el.parentNode);
}

function reg_help(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	var test_css = "min-width: 420px; border: 1px solid #b3c9ce; border-radius: 4px; text-align: left; font: italic 14px/1.3 arial, sans-serif; color: #333; background: #fff; box-shadow: 3px 3px 3px rgba(0, 0, 0, .3);";
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=reg_help&get='+txt.value + '&rname=' + el.name, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? ('max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block; position:absolute; z-index:99999; ' + test_css) : ('display: block; position:absolute;  z-index:99999; ' + test_css);
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function ch_table_v(node, v)
{
	//console.log(node.parentNode.childNodes[0].innerHTML);
	var tab = document.querySelectorAll('span[id*=cred_tables_]');
	var desc = document.querySelectorAll('span[id*=descr_]');
	
	node.parentNode.childNodes[0].innerHTML = (v) ? "Показать описание" : "Показать таблицы";
	
	for (var i = 0; i < tab.length; i++)
	{
		tab[i].style.display =  (v) ? "inline" : "none";
		desc[i].style.display = (!v) ? "inline" : "none";
	}
	//console.log(v);
}

function copy_column(id, table, v)
{
	var prefix = (table) ? "be_psat" : "be_psct";
	var prefix2 = prefix.replace("be_", "but_");
	var prefix3 = id.replace("but_", "be_");
	id = id.replace(prefix2, "");
	
	//console.log(id + " " + table + " " + v);
	
	var buttons = document.querySelectorAll('button[id*=' + prefix2 + ']');
	//console.log(buttons.length);
	
	if (v == "paste")
	{
		var selectElement = document.querySelectorAll('input[name*="' + prefix3 + '\["]');
		for (var y = 0; y < 5; y++)
		{
			//console.log("copy_mass " + prefix3 + " " + y + " " + copy_mass[prefix][y]);
			selectElement[y].value = copy_mass[prefix][y];
		}
		return;
	}
	
	for (var i = 0; i < buttons.length; i++)
	{
		
		switch(v)
		{
			case "copy" :
			{
				
				if (i == fix_mass[id])
				{
					var selectElement = document.querySelectorAll('input[name*="' + prefix + list_month[i] + '\["]');
					for (var y = 0; y < 5; y++)
					{
						copy_mass[prefix][y] = selectElement[y].value;
						//console.log("copy" + y + "=" + copy_mass[prefix][y]);
					}
					
					buttons[i].innerHTML = "Отменить";
					buttons[i].value = "cancel";
				}
				else
				{
					buttons[i].innerHTML = "Вставить";
					buttons[i].value = "paste";
				}
				break;
			}
			case "cancel" :
			{
				buttons[i].innerHTML = "Скопировать";
				buttons[i].value = "copy";
				break;
			}
			case "paste" :
				break;
			default:
				break;
		}
	}
}

function prod_filter(v)
{
	console.log(v);
	for (var i = 0; i < row_list.length; i++)
	{
		if (v == "-1")
			row_list[i].parentNode.style.display = '';
		else
		{
			var bank = row_list[i].innerHTML.replace('&nbsp;','');
			
			if (v == bank)
			{
				row_list[i].parentNode.style.display = '';	
			}
			else
			{
				row_list[i].parentNode.style.display = 'none';
			}
		}
		
	}
}


var flag_list = new Array();
//Функция показа
function points_editor(state)
{
	document.getElementById('window_be').style.display = state;			
		document.getElementById('wrap_be').style.display = state;
	if (state == 'none')
	{
		location.reload();
		return;
	}
	
	var wrap = document.querySelector('#loading');
	wrap.innerHTML = '<img src="/img/loading.gif">';
	
	var data = "block=banks&action=get_points_list";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			
			var bank_list = Item['bank_list'];
			var wrap1 = document.querySelector('#loading');
			//wrap1.style.display = 'none';
			wrap1.innerHTML = '';
			//wrap1.setAttribute('style', 'height:0px;');
			var wrap = document.querySelector('#banks_list');
			//wrap.parentNode.style.width = "1450px";
			//wrap.parentNode.parentNode.style.width = "1500px";
			wrap.innerHTML = "";
			
			var table = document.createElement('table');
				table.width = '1100px';
				table.border = '1';
				var thead = document.createElement('thead');
					var tr = document.createElement('tr');
						var td = document.createElement('td');
							td.width = "25px";
							td.innerHTML = 'ID';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "110px";
							td.innerHTML = 'Наименование';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "150px";
							td.innerHTML = 'Банк';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "50px";
							td.innerHTML = 'Ком.';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "100px";
							td.innerHTML = 'Тип ком.';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "120px";
							td.innerHTML = 'Тип';
						tr.appendChild(td);
						td = document.createElement('td');
							//td.width = "300px";
							td.innerHTML = 'Комментарий';
						tr.appendChild(td);
						td = document.createElement('td');
							td.width = "100px";
							td.innerHTML = ' ';
						tr.appendChild(td);
					thead.appendChild(tr);
				table.appendChild(thead);
			if (parseInt(Item['len']) > 0)
			{
				var tbody = document.createElement('tbody');
				var data = Item['points_list'];
				for (key in data)
				{
					var row = data[key];
					
					flag_list[row['id']] = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					
					tr = document.createElement('tr');
						tr.id = "pointrowid_" + row['id'];
						td = document.createElement('td');
							td.innerHTML = row['id'];
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.innerHTML = '<input name="name" size="10" value="' + row['name'] + '" oninput="javascript:edit_point_inp(this, 0);">';
						tr.appendChild(td);
						
						td = document.createElement('td');
							//td.innerHTML = 'Банк';
							var sel = document.createElement('select');
								sel.name = 'bank';
								sel.setAttribute('onchange', 'javascript:edit_point_sel(this, 1);');
								var opt = document.createElement('option');
									opt.value = '-1';
									opt.innerHTML = 'Выберите';
								sel.appendChild(opt);
								for (var i=0; i< bank_list.length; i++)
								{
									opt = document.createElement('option');
									opt.value = bank_list[i]['id'];
									if (row['bank'] == bank_list[i]['id'])
										opt.selected = true;
									opt.innerHTML = bank_list[i]['name'];
									sel.appendChild(opt);
								}
							td.appendChild(sel);
							var br = document.createElement('br');
							td.appendChild(br);
							br = document.createElement('br');
							td.appendChild(br);
							var label = document.createElement('label');
								label.innerHTML = "Аналитик ";
								var inp = document.createElement('input');
									inp.name = "analyst";
									inp.type = "checkbox";
									if (row['analyst'] == 1)
										inp.checked = true;
									inp.setAttribute('onclick', 'javascript:edit_point_cb(this, 6);')
								label.appendChild(inp);
							td.appendChild(label);
							//td.innerHTML = '<input name="name" value="' + row['name'] + '" oninput="javascript:edit_point_inp(this, 0);">';
							//td.innerHTML += '<br><label>Аналитик <input type="checkbox"></label>';
						tr.appendChild(td);
						
						if (row['analyst_com'] == null)
							row['analyst_com'] = '';
						td = document.createElement('td');
							td.innerHTML =  '<input name="commission" size="4" value="' + row['commission'] + '" oninput="javascript:edit_point_inp(this, 2);" style="width:40px;"><br><br>';
							td.innerHTML += '<input name="analyst_com" size="4" value="' + row['analyst_com'] + '" oninput="javascript:edit_point_inp(this, 7);" style="width:40px;">';
						tr.appendChild(td);
						
						td = document.createElement('td');
							var per_sel = (row['com_type'] == 'per') ? " selected" : "";
							var abs_sel = (row['com_type'] == 'abs') ? " selected" : "";
							var per_sel_a = (row['analyst_ct'] == 'per') ? " selected" : "";
							var abs_sel_a = (row['analyst_ct'] == 'abs') ? " selected" : "";
							td.innerHTML = '<select name="com_type" onchange="javascript:edit_point_sel(this, 3);"><option value="per"' + per_sel + '>Проценты</option><option value="abs"' + abs_sel + '>Число</option></select><br><br>';
							td.innerHTML += '<select name="analyst_ct" onchange="javascript:edit_point_sel(this, 8);"><option value="-1">Выберите</option><option value="per"' + per_sel_a + '>Проценты</option><option value="abs"' + abs_sel_a + '>Число</option></select>';
						tr.appendChild(td);
						
						td = document.createElement('td');
						
						console.log("test id=" + row['id']);
						console.log("type=" + row['type']);
							var com_sel = (row['type'] == 'com') ? " selected" : "";
						console.log("com_sel=" + com_sel);
							var cred_sel = (row['type'] == 'cred') ? " selected" : "";
						console.log("cred_sel=" + cred_sel);
							var com_sel_a = (row['analyst_type'] == 'com') ? " selected" : "";
							var cred_sel_a = (row['analyst_type'] == 'cred') ? " selected" : "";
							td.innerHTML = '<select name="type" onchange="javascript:edit_point_sel(this, 4);"><option value="cred"' + cred_sel + '>От кредита</option><option value="com"' + com_sel + '>От вознаграж.</option></select><br><br>';
							td.innerHTML += '<select name="analyst_type" onchange="javascript:edit_point_sel(this, 9);"><option value="-1">Выберите</option><option value="cred"' + cred_sel_a + '>От кредита</option><option value="com"' + com_sel_a + '>От вознаграж.</option></select>';
							//td.innerHTML = (row['type'] == 'com') ? 'От вознаграждения' : ((row['type'] == 'cred') ? 'От кредита' : '');
						tr.appendChild(td);
						td = document.createElement('td');
							td.innerHTML = '<textarea name="comment" oninput="javascript:edit_point_inp(this, 5);">' + row['comment'] + '</textarea>';
						tr.appendChild(td);
						td = document.createElement('td');
							td.id = "delid_" + row['id'];
							td.innerHTML = '<span style="cursor:pointer; font-style: italic;" onclick="javascript:del_point(this);">Удалить</span>';
						tr.appendChild(td);
					tbody.appendChild(tr);
				}
				table.appendChild(tbody);
			}
			
			wrap.appendChild(table);
			
			table = document.createElement('table');
				table.width = '1100px';
				table.border = '1';
				tbody = document.createElement('tbody');
					tr = document.createElement('tr');
						td = document.createElement('td');
							td.width = "25px";
							td.innerHTML = ' ';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "110px";
							td.innerHTML = '<input id="add_name" type="text" size="10">';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "150px";
							var sel = document.createElement('select');
								sel.id = 'add_bank';
								var opt = document.createElement('option');
									opt.value = '-1';
									opt.innerHTML = 'Выберите';
								sel.appendChild(opt);
								for (var i=0; i< bank_list.length; i++)
								{
									opt = document.createElement('option');
									opt.value = bank_list[i]['id'];
									opt.innerHTML = bank_list[i]['name'];
									sel.appendChild(opt);
								}
							td.appendChild(sel);
							
							br = document.createElement('br');
							td.appendChild(br);
							br = document.createElement('br');
							td.appendChild(br);
							label = document.createElement('label');
								label.innerHTML = "Аналитик ";
								inp = document.createElement('input');
									inp.type = "checkbox";
									inp.id = "analyst";
								label.appendChild(inp);
							td.appendChild(label);
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "50px";
							td.innerHTML = '<input id="add_comm" type="text" size="4" style="width:40px;"><br><br>';
							td.innerHTML +='<input id="add_comm_a" type="text" size="4" style="width:40px;">';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "100px";
							td.innerHTML = '<select id="add_com_type"><option value="-1">Выберите</option><option value="per">Проценты</option><option value="abs">Число</option></select><br><br>';
							td.innerHTML += '<select id="add_com_type_a"><option value="-1">Выберите</option><option value="per">Проценты</option><option value="abs">Число</option></select>';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "120px";
							td.innerHTML = '<select id="add_type"><option value="-1">Выберите</option><option value="cred">От кредита</option><option value="com">От вознаграж.</option></select><br><br>';
							td.innerHTML += '<select id="add_type_a"><option value="-1">Выберите</option><option value="cred">От кредита</option><option value="com">От вознаграж.</option></select>';
						tr.appendChild(td);
						
						td = document.createElement('td');
							//td.width = "300px";
							td.innerHTML = '<textarea id="add_comment"></textarea>';
						tr.appendChild(td);
						
						td = document.createElement('td');
							td.width = "100px";
							td.innerHTML = '<button onclick="javascript:add_point();">Добавить</button>';
						tr.appendChild(td);
					tbody.appendChild(tr);
				table.appendChild(tbody);
			wrap.appendChild(table);
			//wrap1.style.display = 'none';
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

//var flag_list = new Array(0, 0, 0, 0, 0);

function edit_point_cb (el, num)
{
	console.log(flag_list);
	var id = parseInt(el.parentNode.parentNode.parentNode.id.replace("pointrowid_", ""));
	console.log(id);
	console.log(num);
	flag_list[id][num]++;
	if (el.checked)
		el.value = 1;
	else
		el.value = 0;
	//console.log('edit_point_inp flag_list=' + flag_list[id][num]);
	setTimeout(function(){sent_point_value(id, el, num);}, 2000);
}

function edit_point_inp (el, num)
{
	//console.log(flag_list);
	var id = parseInt(el.parentNode.parentNode.id.replace("pointrowid_", ""));
	flag_list[id][num]++;
	//console.log('edit_point_inp flag_list=' + flag_list[id][num]);
	setTimeout(function(){sent_point_value(id, el, num);}, 2000);
}

function edit_point_sel (el, num)
{
	var id = parseInt(el.parentNode.parentNode.id.replace("pointrowid_", ""));
	flag_list[id][num]++;
	console.log('edit_point_sel flag_list=' + flag_list[id][num]);
	setTimeout(function(){sent_point_value(id, el, num);}, 500);
}

function sent_point_value(id, el, num)
{
	flag_list[id][num]--;
	
	var elname = el.name;
	var elvalue = el.value;
	
	if (el.type == 'checkbox')
		el = el.parentNode;
	
	//console.log('sent_point_value flag_list=' + flag_list[id][num]);
	var old_color = window.getComputedStyle(el).backgroundColor
	
	if (flag_list[id][num] == 0)
	{
		
		var data = "block=banks&action=edit_point&id=" + id + "&name=" + elname + "&val=" + elvalue;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				//points_editor('block');
				el.style.backgroundColor = 'lightgreen';
			}
			else 
			{
				el.style.backgroundColor = 'red';
				alert(Item['msg']);
			}
			setTimeout(function(){ch_color(el, old_color);}, timeout);
		});
		console.log(data);
	}
}

function del_point(el)
{
	
	var id = parseInt(el.parentNode.id.replace("delid_", ""));
	if (!confirm("Удалить точку контроля с id=" + id + "?"))
		return;
	var data = "block=banks&action=del_point";
	data += "&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			points_editor('block');
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function add_point()
{
	var add_name = document.querySelector('#add_name');
	var add_comm = document.querySelector('#add_comm');
	add_comm = add_comm.value.replace(",", ".");
	var add_bank = document.querySelector('#add_bank');
	add_bank = add_bank.options[add_bank.selectedIndex].value;
	var add_com_type = document.querySelector('#add_com_type');
	add_com_type = add_com_type.options[add_com_type.selectedIndex].value;
	var add_type = document.querySelector('#add_type');
	add_type = add_type.options[add_type.selectedIndex].value;
	var add_comment = document.querySelector('#add_comment');
	
	var analyst = document.querySelector('#analyst').checked;
	analyst = (analyst) ? 1 : 0;
	var add_comm_a = document.querySelector('#add_comm_a');
	add_comm_a = add_comm_a.value.replace(",", ".");
	var add_com_type_a = document.querySelector('#add_com_type_a');
	add_com_type_a = add_com_type_a.options[add_com_type_a.selectedIndex].value;
	var add_type_a = document.querySelector('#add_type_a');
	add_type_a = add_type_a.options[add_type_a.selectedIndex].value;
	
	var data = "block=banks&action=add_point";
	data += "&name=" + add_name.value;
	data += "&bank=" + add_bank;
	data += "&comm=" + add_comm;
	data += "&com_type=" + add_com_type;
	data += "&type=" + add_type;
	data += "&analyst=" + analyst;
	data += "&add_comm_a=" + add_comm_a;
	data += "&add_com_type_a=" + add_com_type_a;
	data += "&add_type_a=" + add_type_a;
	data += "&comment=" + add_comment.value;
	console.log(data);
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			points_editor('block');
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function bank_editor(state)
{
	document.getElementById('window_be').style.display = state;			
		document.getElementById('wrap_be').style.display = state;
	if (state == 'none')
	{
		location.reload();
		return;
	}
	var wrap = document.querySelector('#loading');
	wrap.innerHTML = '<img src="/img/loading.gif">';
	
	var data = "block=banks&action=get_bank_list";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var wrap1 = document.querySelector('#loading');
			//wrap1.style.display = 'none';
			wrap1.innerHTML = '';
			//wrap1.setAttribute('style', 'height:0px;');
			var wrap = document.querySelector('#banks_list');
			wrap.innerHTML = "";
			var tmp = '<div class="row">';
			tmp += '<div class="cell"> </div>';
			tmp += '<div class="cell"><input type="text" placeholder="Название" id="new_bank_name"></div>';
			tmp += '<div class="cell"><label>Анкета: <input type="checkbox" value="1" id="new_bank_adata"></label></div>';
			tmp += '<div class="cell"><label>ПРБ: <input type="checkbox" value="1" id="new_bank_prb"></label></div>';
			tmp += '<div class="cell"><button onclick="javascript:add_new_bank();">Добавить</button></div>';

			tmp += '</div><br><br>';
			wrap.innerHTML += tmp;

			tmp = '<div class="row">';
			tmp += '<div class="cell">id</div>';
			tmp += '<div class="cell">Название</div>';
			tmp += '<div class="cell">Анкета</div>';
			tmp += '<div class="cell">ПРБ</div>';
			tmp += '<div class="cell"> </div>';
			tmp += '</div>';
			wrap.innerHTML += tmp;
			if (parseInt(Item['len']) > 0)
			{
				var data0 = Item['bank_list'];
				for (key1 in data0)
				{
					var data = data0[key1];
					var key = data['id'];
					if (parseInt(data['deleted']) == 1)
						continue;
					tmp = '<div class="row">';
					tmp += '<div class="cell">' + key + '</div>';
					tmp += '<div class="cell"><input type="text" value="' + data['name'] + '" onfocus="javascript:focus_b(this);" onkeyup="javascript:keyup_b(this);"><a title="Сохранить" style="display:none; margin-left:10px;  border-spacing:0px;" href="#" onclick="javasccript:save_bank(' + key + ', this)"><img style="margin-top:5px; border-spacing:0px;" src="/img/save.png" width="16" height="16"></a></div>';
					var ch = (parseInt(data['adata'])) ? " checked" : '';
					tmp += '<div class="cell"><input onclick="javascript:ch_bival(\'adata\', this, ' + key + ');" type="checkbox" value="1"' + ch + '></div>';
					ch = (parseInt(data['prb'])) ? " checked" : '';
					tmp += '<div class="cell"><input onclick="javascript:ch_bival(\'prb\', this, ' + key + ');" type="checkbox" value="1"' + ch + '></div>';
					tmp += '<div class="cell"><a title="Удалить" href="#" onclick="javasccript:del_bank(' + key + ', this)"><img src="/img/delete.png" width="16" height="16"></a></div>';
					tmp += "</div>";
					wrap.innerHTML += tmp;
				}
				
				for (key1 in data0)
				{
					var data = data0[key1];
					var key = data['id'];
					if (parseInt(data['deleted']) != 1)
						continue;
					tmp = '<div class="row">';
					tmp += '<div class="cell">' + key + '</div>';
					tmp += '<div class="cell"><input type="text" value="' + data['name'] + '" onfocus="javascript:focus_b(this);" onkeyup="javascript:keyup_b(this);"><a title="Сохранить" style="display:none; margin-left:10px;  border-spacing:0px;" href="#" onclick="javasccript:save_bank(' + key + ', this)"><img style="margin-top:5px; border-spacing:0px;" src="/img/save.png" width="16" height="16"></a></div>';
					var ch = (parseInt(data['adata'])) ? " checked" : '';
					tmp += '<div class="cell"><input onclick="javascript:ch_bival(\'adata\', this, ' + key + ');" type="checkbox" value="1"' + ch + '></div>';
					ch = (parseInt(data['prb'])) ? " checked" : '';
					tmp += '<div class="cell"><input onclick="javascript:ch_bival(\'prb\', this, ' + key + ');" type="checkbox" value="1"' + ch + '></div>';
					tmp += '<div class="cell"><a title="Восстановить" href="#" onclick="javasccript:restore_bank(' + key + ', this)"><img src="/img/restore2.png" width="16" height="16"></a></div>';
					tmp += "</div>";
					wrap.innerHTML += tmp;
				}
			}
			//wrap1.style.display = 'none';
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function add_new_bank()
{
	var new_bank_name = document.querySelector('#new_bank_name').value;
	if (new_bank_name == '')
	{
		alert('Введите название банка!');
		return;
	}
	var new_bank_adata = (document.querySelector('#new_bank_adata').checked) ? 1 : 0;
	var new_bank_prb = (document.querySelector('#new_bank_prb').checked) ? 1 : 0;
	
	var data = "block=banks&action=add_new_bank&new_bank_name=" + new_bank_name + "&new_bank_adata=" + new_bank_adata + "&new_bank_prb=" + new_bank_prb;

	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			bank_editor('block');
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function ch_bival(name, el, id)
{
	var old_color = window.getComputedStyle(el.parentNode).backgroundColor
	var val = (el.checked) ? 1 : 0;
	var data = "block=banks&action=ch_bank&id=" + id + "&name=" + name + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
			el.parentNode.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			el.parentNode.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
	});
}

function focus_b(el)
{
	console.log(el.value);
}
function keyup_b(el)
{
	el.parentNode.querySelector('a').style.display = 'inline';
}

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

function save_bank(id, el)
{
	//alert(id);
	//alert(el.parentNode.querySelector('input').value)
	var old_color = window.getComputedStyle(el.parentNode).backgroundColor
	var val = el.parentNode.querySelector('input').value;
	var data = "block=banks&action=save_bank&id=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.style.backgroundColor = 'lightgreen';
			el.style.display = 'none';
		}
		else 
		{
			el.parentNode.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el.parentNode, old_color);}, timeout);
	});
	//alert('Пока не доступно');
}
function del_bank(id, el)
{
	//alert(id);
	var data = "block=banks&action=del_bank&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
		}
		else 
		{
			alert(Item['msg']);
		}
	});
//	alert('Пока не доступно');
}
function restore_bank(id, el)
{
	//alert(id);
	var data = "block=banks&action=restore_bank&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
			bank_editor('block');
		}
		else 
		{
			alert(Item['msg']);
		}
	});
//	alert('Пока не доступно');
}

function show(state)
{
	
	var be_pd_wrap_data = document.querySelector('#be_pd_wrap_data');
		be_pd_wrap_data.innerHTML = '';
		
			
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state;
	
	
	if (state == 'none')
	{
		console.log("1 " + state);
		document.querySelector('#be_b_copy').style.display = "none";
	}
	else
	{
		console.log("2 " + state);
		var ul = document.querySelector('#reg_params');
		ul.innerHTML = '';
		ul = document.querySelector('#adr_params');
		ul.innerHTML = '';
		ul = document.querySelector('#work_adr_params');
		ul.innerHTML = '';
	}
	
}

function show(state, upd)
{
	
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state; 			
	if (upd)
		document.forms.be_form.be_type.value = "upd";
	
	if (state == 'none')
	{
		console.log("2 " + state);
		document.querySelector('#be_b_copy').style.display = "none";
	}
	else
	{
		console.log("3 " + state);
		var ul = document.querySelector('#reg_params');
		ul.innerHTML = '';
		ul = document.querySelector('#adr_params');
		ul.innerHTML = '';
		ul = document.querySelector('#work_adr_params');
		ul.innerHTML = '';
		var be_pd_wrap_data = document.querySelector('#be_pd_wrap_data');
		be_pd_wrap_data.innerHTML = '';
		
	}
}

function send_form(type)
{
	var form = document.forms.be_form;
	form.be_type.value = type;
	var data = getQueryString(form.elements);
	console.log(data);
	
	var ul = document.querySelector('#reg_params');
	var li = ul.querySelectorAll('li');
	
	//var params = "&be_params=";
	var params = "";
	if ((li) && (li.length > 0))
	{
		for (var i = 0; i < li.length; i++)
		{
			if (params != '')
				params += "|";
			
			var reg_value = li[i].querySelector('span[name="reg_value"]');
			params += reg_value.innerHTML;
		}
	}
	
	data += "&be_params=" + params;
	
	ul = document.querySelector('#adr_params');
	li = ul.querySelectorAll('li');
	params = "";
	if ((li) && (li.length > 0))
	{
		for (var i = 0; i < li.length; i++)
		{
			if (params != '')
				params += "|";
			
			var reg_value = li[i].querySelector('span[name="reg_value"]');
			params += reg_value.innerHTML;
		}
	}
	
	data += "&adr_params=" + params;
	
	ul = document.querySelector('#work_adr_params');
	li = ul.querySelectorAll('li');
	params = "";
	if ((li) && (li.length > 0))
	{
		for (var i = 0; i < li.length; i++)
		{
			if (params != '')
				params += "|";
			
			var reg_value = li[i].querySelector('span[name="reg_value"]');
			params += reg_value.innerHTML;
		}
	}
	
	data += "&work_adr_params=" + params;
	
	var be_pd_wrap_data = document.querySelector('#be_pd_wrap_data');
	ul = be_pd_wrap_data.querySelector('ul');
	
	var params2 = "";
	if (ul)
	{
		li = ul.querySelectorAll('li');
		if ((li) && (li.length > 0))
		{
			for (var i = 0; i < li.length; i++)
			{
				var pd_val = li[i].querySelector('span[name="pd_val"]');
				if (params2 != '')
					params2 += "|";
				
				//var reg_value = li[i].querySelector('span[name="reg_value"]');
				var tmp = pd_val.innerHTML
				tmp = tmp.replace(/\&lt\;/g, "<");
				params2 += tmp;
			}
		}
	}
	data += "&be_pd_set_data=" + params2;
	
	var be_fico_wrap_data = document.querySelector('#be_fico_wrap_data');
	ul = be_fico_wrap_data.querySelector('ul');
	
	params2 = "";
	if (ul)
	{
		li = ul.querySelectorAll('li');
		if ((li) && (li.length > 0))
		{
			for (var i = 0; i < li.length; i++)
			{
				var pd_val = li[i].querySelector('span[name="fico_val"]');
				if (params2 != '')
					params2 += "|";
				
				//var reg_value = li[i].querySelector('span[name="reg_value"]');
				var tmp = pd_val.innerHTML
				tmp = tmp.replace(/\&lt\;/g, "<");
				params2 += tmp;
			}
		}
	}
	data += "&be_fico=" + params2;
	
	
	
	console.log(data);

	//ajaxQuery('http://old.smartcredit.online/new_products_ajax.php','POST', data, false, function(req) 
	ajaxQuery('/new_products_ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;

		if (tmp_page == "")
		{
			//console.log('test');
			location.reload();
		}
		else
			alert(tmp_page);
	});
	//location.reload();
}

function all_w_types()
{
	var el = document.querySelectorAll('[name*=be_work_type]');
	var ch = true;
	for (i=0; i<el.length; i++)
		if (!el[i].checked)
			ch = false;
	for (i=0; i<el.length; i++)
		if(ch)
			el[i].checked = false;
		else
			el[i].checked = true;
			
}

function all_stop_types()
{
	var el = document.querySelectorAll('[name*=be_stop]');
	var ch = true;
	for (i=0; i<el.length; i++)
		if (!el[i].checked)
			ch = false;
	for (i=0; i<el.length; i++)
		if(ch)
			el[i].checked = false;
		else
			el[i].checked = true;
			
}

function edit(id)
{

	document.querySelector('#stop_settings').value="-1";

	var tr = document.getElementById('be_id'+id);
	var td = tr.getElementsByTagName('td');
	document.querySelector('#be_butt').innerHTML = "Изменить";
	//document.querySelector('#be_b_copy').style = "display:inline;";
	
	console.log("3 cp_but_s");
	document.querySelector('#be_b_copy').style.display = "inline";
	
	
	var data = "be_type=read&be_prod_id=" + id;
//	ajaxQuery('http://old.smartcredit.online/new_products_ajax.php','POST', data, true, function(req) 
	ajaxQuery('/new_products_ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
		var data_db = JSON.parse(tmp_page);
		
		show('block');	
		
		var form = document.forms.be_form;
		form.reset();
		form.be_type.value = "upd";
		form.be_prod_id.value = id;
		form.be_bank_name.value = data_db["bank_name"];
		form.be_product_name.value = data_db["product_name"];
		form.be_min_percent.value = data_db["min_percent"];
		form.be_max_percent.value = data_db["max_percent"];
		if (parseInt(data_db["active"]))
			form.be_active.checked = true;
		if (parseInt(data_db["online_av"]))
			form.be_online_av.checked = true;
		if (parseInt(data_db["beznD"]))
			form.be_beznD.checked = true;
		form.be_description.value = data_db["description"];
		form.be_pm.value = data_db["pm"];
		form.be_pd.value = data_db["pd"];
		if (parseInt(data_db["pd_set"]))
			form.be_pd_set.checked = true;


		
		if (parseInt(data_db["pd_miss"]))
			form.be_pd_miss.checked = true;		
		form.be_discont.value = data_db["discont"];
		if (parseInt(data_db["res"]))
			form.be_res.checked = true;
		if (parseInt(data_db["bank_zp"]))
			form.be_bank_zp.checked = true;
		if (parseInt(data_db["bank_acc"]))
			form.be_bank_acc.checked = true;
		form.be_cr4other.value = data_db["cr4other"];
		form.be_min_credit.value = parseInt(data_db["min_credit"]);
		//console.log(data_db);
		//console.log(data_db["min_credit"]);
		form.be_max_credit.value = data_db["max_credit"];
		form.be_min_income.value = data_db["min_income"];
		form.be_max_age_m.value = data_db["max_age_m"];
		form.be_max_age_w.value = data_db["max_age_w"];
		form.be_max_cre_time.value = data_db["max_cre_time"];
		form.be_min_cre_time.value = data_db["min_cre_time"];
		form.be_max_end_age_m.value = data_db["max_end_age_m"];
		form.be_max_end_age_w.value = data_db["max_end_age_w"];
		form.be_min_age_m.value = data_db["min_age_m"];
		form.be_min_age_w.value = data_db["min_age_w"];
		form.be_lim_time.value = data_db["lim_time"];
		
		form.be_nbki.value = data_db["nbki"];
		/*var nbki_t = data_db["nbki"].split('|');
		for (i = 0; i < nbki_t.length +1; i++)
		{
			if (parseInt(nbki_t[i+1]))
			{
				form["be_nbki[" + i +"]"].checked = true;
			}
		}*/
		
		var arm_t = data_db["arm"].split('|');
		for (i = 0; i < arm_t.length +1; i++)
		{
			if (parseInt(arm_t[i+1]))
			{
				form["be_arm[" + i +"]"].checked = true;
			}
		}


		/*var adr_t = data_db["adr"].split('|');
		for (i = 0; i < adr_t.length +1; i++)
		{
			if (parseInt(adr_t[i+1]))
			{
				form["be_adr[" + i +"]"].checked = true;
			}
		}*/
		
		if (parseInt(data_db["adr_nan"]))
			form.be_adr_nan.checked = true;		
		
		form.be_adr_time.value = data_db["adr_time"];
		
		var be_work = data_db["work"].split('|');
		//console.log(data_db["work"]);
		//console.log(be_work.length);
		for (i = 0; i < be_work.length +1; i++)
		{
			if (parseInt(be_work[i+1]))
			{
				form["be_work[" + i +"]"].checked = true;
			}
		}
		
		var be_c_work = data_db["c_work"].split('|');
		for (i = 0; i < be_c_work.length +1; i++)
		{
			if (parseInt(be_c_work[i+1]))
			{
				form["be_c_work[" + i +"]"].checked = true;
			}
		}
		
		var be_c_inc = data_db["c_inc"].split('|');
		for (i = 0; i < be_c_inc.length +1; i++)
		{
			if (parseInt(be_c_inc[i+1]))
			{
				form["be_c_inc[" + i +"]"].checked = true;
			}
		}
		
		//form.be_inc_card.value = data_db["inc_card"];
		
		form.be_exp_now_new.value = data_db["exp_now_new"];
		/*var be_exp_now = data_db["exp_now"].split('|');
		for (i = 0; i < be_exp_now.length +1; i++)
		{
			if (parseInt(be_exp_now[i+1]))
			{
				form["be_exp_now[" + i +"]"].checked = true;
			}
		}*/
		
		form.be_reg_date_new.value = data_db["reg_date_new"];
		/*var be_reg_date = data_db["reg_date"].split('|');
		for (i = 0; i < be_reg_date.length +1; i++)
		{
			if (parseInt(be_reg_date[i+1]))
			{
				form["be_reg_date[" + i +"]"].checked = true;
			}
		}*/
		
		var be_work_type = data_db["work_type"].split('|');
		for (i = 0; i < be_work_type.length +1; i++)
		{
			if (parseInt(be_work_type[i+1]))
			{
				form["be_work_type[" + i +"]"].checked = true;
			}
		}
		
		//form.be_work_adr.value = data_db["work_adr"];
		/*var be_work_adr = data_db["work_adr"].split('|');
		for (i = 0; i < be_work_adr.length +1; i++)
		{
			if (parseInt(be_work_adr[i+1]))
			{
				form["be_work_adr[" + i +"]"].checked = true;
			}
		}*/
		
		
		
		//form.be_ent.value = data_db["ent"];
		var be_ent = data_db["ent"].split('|');
		for (i = 0; i < be_ent.length +1; i++)
		{
			if (parseInt(be_ent[i+1]))
			{
				form["be_ent[" + i +"]"].checked = true;
			}
		}

		if (parseInt(data_db["ca"]))
			form.be_ca.checked = true;

		if (parseInt(data_db["pfr"]))
			form.be_pfr.checked = true;
		
		form.be_firm_per.value = data_db["firm_per"];
		form.be_firm_per_max.value = data_db["firm_per_max"];
		form.be_firm_per_min.value = data_db["firm_per_min"];
		
		
		
	//	form.be_nal.value = data_db["nal"];
		var be_nal = data_db["nal"].split('|');
		for (i = 0; i < be_nal.length +1; i++)
		{
			if (parseInt(be_nal[i+1]))
			{
				form["be_nal[" + i +"]"].checked = true;
			}
		}
		
		form.be_proc.value = data_db["proc"];
		form.be_proc_max.value = data_db["proc_max"];
		
		
		
		//form.be_pr_nal.value = data_db["pr_nal"];
		var be_pr_nal = data_db["pr_nal"].split('|');
		for (i = 0; i < be_pr_nal.length +1; i++)
		{
			if (parseInt(be_pr_nal[i+1]))
			{
				form["be_pr_nal[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_pr_4.value = data_db["pr_4"];
		
		
		//form.be_est.value = data_db["est"];
		var be_est = data_db["est"].split('|');
		for (i = 0; i < be_est.length +1; i++)
		{
			if (parseInt(be_est[i+1]))
			{
				form["be_est[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_co.value = data_db["co"];
		
		if (parseInt(data_db["delay_29"]))
			form.be_delay_29.checked = true;
		
		
	//	form.be_ps.value = data_db["ps"];
		var be_psc = data_db["psc"].split('|');
		for (i = 0; i < be_psc.length +1; i++)
		{
			if (parseInt(be_psc[i+1]))
			{
				form["be_psc[" + i +"]"].checked = true;
			}
		}
		form.be_psc_u0.value = data_db["psc_u0"];
		form.be_psc_u1.value = data_db["psc_u1"];
		form.be_psc_u2.value = data_db["psc_u2"];
		form.be_psc_u3.value = data_db["psc_u3"];
		form.be_psc_u4.value = data_db["psc_u4"];
		
		var be_ps = data_db["ps"].split('|');
		for (i = 0; i < be_ps.length +1; i++)
		{
			if (parseInt(be_ps[i+1]))
			{
				form["be_ps[" + i +"]"].checked = true;
			}
		}
		form.be_ps_u0.value = data_db["ps_u0"];
		form.be_ps_u1.value = data_db["ps_u1"];
		form.be_ps_u2.value = data_db["ps_u2"];
		form.be_ps_u3.value = data_db["ps_u3"];
		form.be_ps_u4.value = data_db["ps_u4"];
		
		
		
		if (parseInt(data_db["delay_act"]))
			form.be_delay_act.checked = true;	
		
		if (parseInt(data_db["cr_his"]))
			form.be_cr_his.checked = true;

		form.be_co_max.value = data_db["co_max"];
		form.be_co_umax.value = data_db["co_umax"];
		if (parseInt(data_db["zaim_mfo"]))
			form.be_zaim_mfo.checked = true;

		form.be_lim_all.value = data_db["lim_all"];
		form.be_lim_val.value = data_db["lim_val"];
		form.be_bal_now.value = data_db["bal_now"];
		form.be_bal_val.value = data_db["bal_val"];
		//form.be_ps_c.value = data_db["ps_c"];
		var be_ps_c = data_db["ps_c"].split('|');
		for (i = 0; i < be_ps_c.length +1; i++)
		{
			if (parseInt(be_ps_c[i+1]))
			{
				form["be_ps_c[" + i +"]"].checked = true;
			}
		}
		form.be_ps_v0.value = data_db["ps_v0"];
		form.be_ps_v1.value = data_db["ps_v1"];
		form.be_ps_v2.value = data_db["ps_v2"];
		form.be_ps_v3.value = data_db["ps_v3"];
		form.be_ps_v4.value = data_db["ps_v4"];
		
		var be_psa_c = data_db["psa_c"].split('|');
		for (i = 0; i < be_psa_c.length +1; i++)
		{
			if (parseInt(be_psa_c[i+1]))
			{
				form["be_psa_c[" + i +"]"].checked = true;
			}
		}
		form.be_psa_v0.value = data_db["psa_v0"];
		form.be_psa_v1.value = data_db["psa_v1"];
		form.be_psa_v2.value = data_db["psa_v2"];
		form.be_psa_v3.value = data_db["psa_v3"];
		form.be_psa_v4.value = data_db["psa_v4"];
		
		
		var be_ps_6 = data_db["ps_6"].split('|');
		for (i = 0; i < be_ps_6.length +1; i++)
		{
			if (parseInt(be_ps_6[i+1]))
			{
				form["be_ps_6[" + i +"]"].checked = true;
			}
		}
		form.be_ps_6v0.value = data_db["ps_6v0"];
		form.be_ps_6v1.value = data_db["ps_6v1"];
		form.be_ps_6v2.value = data_db["ps_6v2"];
		form.be_ps_6v3.value = data_db["ps_6v3"];
		form.be_ps_6v4.value = data_db["ps_6v4"];
		
		
		var be_ps_12 = data_db["ps_12"].split('|');
		for (i = 0; i < be_ps_12.length +1; i++)
		{
			if (parseInt(be_ps_12[i+1]))
			{
				form["be_ps_12[" + i +"]"].checked = true;
			}
		}
		form.be_ps_12v0.value = data_db["ps_12v0"];
		form.be_ps_12v1.value = data_db["ps_12v1"];
		form.be_ps_12v2.value = data_db["ps_12v2"];
		form.be_ps_12v3.value = data_db["ps_12v3"];
		form.be_ps_12v4.value = data_db["ps_12v4"];
		
		if (parseInt(data_db["res_en"]))
			form.be_res_en.checked = true;

		form.be_obr_7.value = data_db["obr_7"];
		form.be_obr_14.value = data_db["obr_14"];
		form.be_obr_mon.value = data_db["obr_mon"];

		if (parseInt(data_db["inc_true"]))
			form.be_inc_true.checked = true;
		//form.be_crim.value = data_db["crim"];
		/*var be_crim = data_db["crim"].split('|');
		for (i = 0; i < be_crim.length +1; i++)
		{
			if (parseInt(be_crim[i+1]))
			{
				form["be_crim[" + i +"]"].checked = true;
			}
		}*/
		//form.be_mbank.value = data_db["mbank"];
		/*var be_mbank = data_db["mbank"].split('|');
		for (i = 0; i < be_mbank.length +1; i++)
		{
			if (parseInt(be_mbank[i+1]))
			{
				form["be_mbank[" + i +"]"].checked = true;
			}
		}*/
		//form.be_st_cr.value = data_db["st_cr"];
		var be_st_cr = data_db["st_cr"].split('|');
		for (i = 0; i < be_st_cr.length +1; i++)
		{
			if (parseInt(be_st_cr[i+1]))
			{
				form["be_st_cr[" + i +"]"].checked = true;
			}
		}
		//form.be_spark.value = data_db["spark"];
		var be_spark = data_db["spark"].split('|');
		for (i = 0; i < be_spark.length +1; i++)
		{
			if (parseInt(be_spark[i+1]))
			{
				form["be_spark[" + i +"]"].checked = true;
			}
		}
		
		
		
		
		if (parseInt(data_db["bailn"]))
			form.be_bailn.checked = true;
		
		if (parseInt(data_db["bail_miss"]))
			form.be_bail_miss.checked = true;
		
		if (parseInt(data_db["ul_miss"]))
			form.be_ul_miss.checked = true;
		
		
		//form.be_guarantor.value = data_db["guarantor"];
		var be_guarantor = data_db["guarantor"].split('|');
		for (i = 0; i < be_guarantor.length +1; i++)
		{
			if (parseInt(be_guarantor[i+1]))
			{
				form["be_guarantor[" + i +"]"].checked = true;
			}
		}
		
		
	//	form.be_bail.value = data_db["bail"];
		var be_bail = data_db["bail"].split('|');
		for (i = 0; i < be_bail.length +1; i++)
		{
			if (parseInt(be_bail[i+1]))
			{
				form["be_bail[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_first_deb.value = data_db["first_deb"];
		form.be_mkad.value = data_db["mkad"];
		
		//form.be_doc_sob.value = data_db["doc_sob"];
		var be_doc_sob = data_db["doc_sob"].split('|');
		for (i = 0; i < be_doc_sob.length +1; i++)
		{
			if (parseInt(be_doc_sob[i+1]))
			{
				form["be_doc_sob[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_owner.value = data_db["owner"];
		var be_owner = data_db["owner"].split('|');
		for (i = 0; i < be_owner.length +1; i++)
		{
			if (parseInt(be_owner[i+1]))
			{
				form["be_owner[" + i +"]"].checked = true;
			}
		}
		
		
		
		//form.be_obj_type.value = data_db["obj_type"];
		var be_obj_type = data_db["obj_type"].split('|');
		for (i = 0; i < be_obj_type.length +1; i++)
		{
			if (parseInt(be_obj_type[i+1]))
			{
				form["be_obj_type[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_replan.value = data_db["replan"];
		var be_replan = data_db["replan"].split('|');
		for (i = 0; i < be_replan.length +1; i++)
		{
			if (parseInt(be_replan[i+1]))
			{
				form["be_replan[" + i +"]"].checked = true;
			}
		}
		
		
	//	form.be_house_mat.value = data_db["house_mat"];
		var be_house_mat = data_db["house_mat"].split('|');
		for (i = 0; i < be_house_mat.length +1; i++)
		{
			if (parseInt(be_house_mat[i+1]))
			{
				form["be_house_mat[" + i +"]"].checked = true;
			}
		}
		
		
		//form.be_found.value = data_db["found"];
		var be_found = data_db["found"].split('|');
		for (i = 0; i < be_found.length +1; i++)
		{
			if (parseInt(be_found[i+1]))
			{
				form["be_found[" + i +"]"].checked = true;
			}
		}
		
		
		form.be_obj_age.value = data_db["obj_age"];
		form.be_wear.value = data_db["wear"];
		
		//form.be_comm.value = data_db["comm"];
		var be_comm = data_db["comm"].split('|');
		for (i = 0; i < be_comm.length +1; i++)
		{
			if (parseInt(be_comm[i+1]))
			{
				form["be_comm[" + i +"]"].checked = true;
			}
		}
		
		var be_bh_type = data_db["bh_type"].split('|');
		for (i = 0; i < be_bh_type.length +1; i++)
		{
			if (parseInt(be_bh_type[i+1]))
			{
				form["be_bh_type[" + i +"]"].checked = true;
			}
		}
		
		var stop_t = data_db["stop"].split('|');
		for (i = 0; i < stop_t.length +1; i++)
		{
			if (parseInt(stop_t[i+1]))
			{
				form["be_stop[" + i +"]"].checked = true;
			}
		}
		
		var be_goal = data_db["goal"].split('|');
		for (i = 0; i < be_goal.length +1; i++)
		{
			if (parseInt(be_goal[i+1]))
			{
				form["be_goal[" + i +"]"].checked = true;
			}
		}
		
		var be_goalc = data_db["goalc"].split('|');
		for (i = 0; i < be_goalc.length +1; i++)
		{
			if (parseInt(be_goalc[i+1]))
			{
				form["be_goalc[" + i +"]"].checked = true;
			}
		}
		
		console.log('be_activs = ' +  data_db["activs"]);
		var be_activs = data_db["activs"].split('|');
		
		for (i = 0; i < be_activs.length +1; i++)
		{
			
			if (parseInt(be_activs[i+1]))
			{
				form["be_activs[" + i +"]"].checked = true;
			}
		}
		
		var be_sf = data_db["sf"].split('|');
		
		for (i = 0; i < be_sf.length +1; i++)
		{
			
			if (parseInt(be_sf[i+1]))
			{
				form["be_sf[" + i +"]"].checked = true;
			}
		}
		
		
		/*if (parseInt(data_db["reg_all"]))
			form.be_reg_all.checked = true;
		
		if (parseInt(data_db["adr_all"]))
			form.be_adr_all.checked = true;
		
		if (parseInt(data_db["work_adr_all"]))
			form.be_work_adr_all.checked = true;
		*/
		
		if (parseInt(data_db["reg_miss"]))
			form.be_reg_miss.checked = true;
		
		if (parseInt(data_db["adr_miss"]))
			form.be_adr_miss.checked = true;
		
		if (parseInt(data_db["wa_miss"]))
			form.be_wa_miss.checked = true;
		
		
		//var be_ps_12 = data_db["ps_12"].split('|');
		
		
		var psct3	= data_db["psct3"].split('|');
		var psct6	= data_db["psct6"].split('|');
		var psct12	= data_db["psct12"].split('|');
		var psct18	= data_db["psct18"].split('|');
		var psct24	= data_db["psct24"].split('|');
		var psct36	= data_db["psct36"].split('|');
		var psct60	= data_db["psct60"].split('|');
		var psct60p	= data_db["psct60p"].split('|');
		var psctall	= data_db["psctall"].split('|');
		
		var psat3	= data_db["psat3"].split('|');
		var psat6	= data_db["psat6"].split('|');
		var psat12	= data_db["psat12"].split('|');
		var psat18	= data_db["psat18"].split('|');
		var psat24	= data_db["psat24"].split('|');
		var psat36	= data_db["psat36"].split('|');
		var psat60	= data_db["psat60"].split('|');
		var psat60p	= data_db["psat60p"].split('|');
		var psatall	= data_db["psatall"].split('|');
		
		
		
//		for (i = 0; i < psct3.length +1; i++)
		for (i = 0; i < 5; i++)
		{
			/*if (parseInt(be_ps_12[i+1]))
			{
				form["be_ps_12[" + i +"]"].checked = true;
			}**/
			//form["be_psct3[" + i +"]"].value = data_db["psct3"];
			var j = i+1;
			form["be_psct3[" + i +"]"].value 	= psct3[j] ? psct3[j] : "";
			form["be_psct6[" + i +"]"].value 	= psct6[j] ? psct6[j] : "";
			form["be_psct12[" + i +"]"].value 	= psct12[j] ? psct12[j] : "";
			form["be_psct18[" + i +"]"].value 	= psct18[j] ? psct18[j] : "";
			form["be_psct24[" + i +"]"].value 	= psct24[j] ? psct24[j] : "";
			form["be_psct36[" + i +"]"].value 	= psct36[j] ? psct36[j] : "";
			form["be_psct60[" + i +"]"].value 	= psct60[j] ? psct60[j] : "";
			form["be_psct60p[" + i +"]"].value 	= psct60p[j] ? psct60p[j] : "";
			form["be_psctall[" + i +"]"].value 	= psctall[j] ? psctall[j] : "";
			
			form["be_psat3[" + i +"]"].value 	= psat3[j] ? psat3[j] : "";
			form["be_psat6[" + i +"]"].value 	= psat6[j] ? psat6[j] : "";
			form["be_psat12[" + i +"]"].value 	= psat12[j] ? psat12[j] : "";
			form["be_psat18[" + i +"]"].value 	= psat18[j] ? psat18[j] : "";
			form["be_psat24[" + i +"]"].value 	= psat24[j] ? psat24[j] : "";
			form["be_psat36[" + i +"]"].value 	= psat36[j] ? psat36[j] : "";
			form["be_psat60[" + i +"]"].value 	= psat60[j] ? psat60[j] : "";
			form["be_psat60p[" + i +"]"].value 	= psat60p[j] ? psat60p[j] : "";
			form["be_psatall[" + i +"]"].value 	= psatall[j] ? psatall[j] : "";
		}
		//form.be_ps_12v0.value = data_db["ps_12v0"];
		
		if (data_db["psct"] != '')
		{
			var be_psct = data_db["psct"].split('|');
			
			for (i = 0; i < be_psct.length +1; i++)
			{
				
				if (parseInt(be_psct[i+1]))
				{
					form["be_psct[" + i +"]"].checked = true;
				}
			}
		}
		
		if (data_db["psat"] != '')
		{
			var be_psat = data_db["psat"].split('|');
			
			for (i = 0; i < be_psat.length +1; i++)
			{
				
				if (parseInt(be_psat[i+1]))
				{
					form["be_psat[" + i +"]"].checked = true;
				}
			}
		}
		
		set_psct();
		set_psat();
		
		var ul = document.querySelector('#reg_params');
		
		if ((data_db["params"] != '') && (data_db["params"] != null))
		{
			
			var params = data_db["params"].split('|');
			
			if (params.length > 0)
			{
				for (var i=0; i < params.length; i++)
				{
					var li = document.createElement('li');
						li.innerHTML = '<span name="reg_value">' + params[i] + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
					ul.appendChild(li);
				}
			}
		}
		
		ul = document.querySelector('#adr_params');
		
		if ((data_db["adr_params"] != '') && (data_db["adr_params"] != null))
		{
			
			var params = data_db["adr_params"].split('|');
			
			if (params.length > 0)
			{
				for (var i=0; i < params.length; i++)
				{
					var li = document.createElement('li');
						li.innerHTML = '<span name="reg_value">' + params[i] + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
					ul.appendChild(li);
				}
			}
		}
		
		ul = document.querySelector('#work_adr_params');
		
		if ((data_db["work_adr_params"] != '') && (data_db["work_adr_params"] != null))
		{
			
			var params = data_db["work_adr_params"].split('|');
			
			if (params.length > 0)
			{
				for (var i=0; i < params.length; i++)
				{
					var li = document.createElement('li');
						li.innerHTML = '<span name="reg_value">' + params[i] + '</span> <span onclick="javascript:del_reg(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
					ul.appendChild(li);
				}
			}
		}
		
		var be_pd_wrap_data = document.querySelector('#be_pd_wrap_data');
		be_pd_wrap_data.innerHTML = '';
		
		ul =  document.createElement('ul');
		
		if ((data_db["pd_set_data"] != '') && (data_db["pd_set_data"] != null))
		{
			
			var params = data_db["pd_set_data"].split('|');
			
			if (params.length > 0)
			{
				for (var i=0; i < params.length; i++)
				{
					var tmp = params[i].replace(/\</g, '&lt;')
					var li = document.createElement('li');
						li.innerHTML = '<span name="pd_val">' + tmp + '</span> <span onclick="javascript:del_pd(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
					ul.appendChild(li);
				}
			}
		}
		be_pd_wrap_data.appendChild(ul);
		
		var be_fico_wrap_data = document.querySelector('#be_fico_wrap_data');
		be_fico_wrap_data.innerHTML = '';
		
		ul =  document.createElement('ul');
		
		if ((data_db["fico"] != '') && (data_db["fico"] != null))
		{
			
			var params = data_db["fico"].split('|');
			
			if (params.length > 0)
			{
				for (var i=0; i < params.length; i++)
				{
					var tmp = params[i].replace(/\</g, '&lt;')
					var li = document.createElement('li');
						li.innerHTML = '<span name="fico_val">' + tmp + '</span> <span onclick="javascript:del_pd(this);" style="cursor:pointer;"><img src="/img/delete.png"></span>';
		
					ul.appendChild(li);
				}
			}
		}
		be_fico_wrap_data.appendChild(ul);

			
	
	});
}



function set_psct()
{
	console.log("ttttttt00");
	var selectElement = document.querySelectorAll('input[name*="be_psct\["]');
	//console.log(selectElement.length);
	for (i = 0; i < selectElement.length; i++)
	{
		//console.log(selectElement[i].name + " =" + selectElement[i].checked);
		var selectElement2 = document.querySelectorAll('input[name*="be_psct' + list_month[i] + '\["]');
		//console.log(selectElement2.length);
		for (j = 0; j < selectElement2.length; j++)
		{
//			console.log("s1=" + selectElement[i].name + ", s2=" + selectElement2[j].name);
			selectElement2[j].disabled = (selectElement[i].checked) ? "" : "true";
//			console.log((parseInt(selectElement[i].checked)) ? "" : "true");
		}
	}
}

function set_psat()
{
	var selectElement = document.querySelectorAll('input[name*="be_psat\["]');
	for (i = 0; i < selectElement.length; i++)
	{
		var selectElement2 = document.querySelectorAll('input[name*="be_psat' + list_month[i] + '\["]');
		for (j = 0; j < selectElement2.length; j++)
		{
			selectElement2[j].disabled = (selectElement[i].checked) ? "" : "true";
		}
	}
}

function del(id)
{
//	var div = document.getElementById('info_div');
//	div.innerHTML = '';
	if (!confirm("Точно удалить?")) {
	  return 0;
	};

	
	var tr = document.getElementById('be_id'+id);
	var td = tr.getElementsByTagName('td');
	
	var form = document.forms.be_del_form;
	form.be_prod_id.value		= id;
	var data = getQueryString(form.elements);
//	ajaxQuery('http://old.smartcredit.online/new_products_ajax.php','POST', data, true, function(req)
	ajaxQuery('/new_products_ajax.php','POST', data, false, function(req) 
	{
		var tmp_page = req.responseText;
		location.reload();
		//alert(tmp_page);
	});
	//form.submit();
}


var product = new Object();

function set_value(v, type, c)
{
	switch(type)
	{
		case 'res':
			product[type] = (c) ? 1 : 0;
			break;
		default:
			product[type] = v;
	}
}
/*
function show_products()
{
	document.querySelector('#results').innerHTML = '';
	var i = 0;
	for (var key in product)
	{
		if (typeof key != 'undefined')
			document.querySelector('#results').innerHTML += i++ + ":" + key + ' : ' + product[key] + '<br>';
	}
}

function show_products()
{
	var form = document.forms.pr_form;
	var data = getQueryString(form.elements);	
	ajaxQuery('http://old.smartcredit.online/new_products/get_products.php','POST', data, false, function(req) 
	//ajaxQuery('http://iska2.pro/new_products_ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		//alert(tmp_page);
		document.querySelector('#results').innerHTML = tmp_page;
	});
}*/

function stop_select(v)
{
	var selectElement = document.querySelectorAll('input[name*="be_stop["]');
	console.log('selected ' + selectElement.length);
	
	for (var i = 0; i < selectElement.length; i++)
	{
		selectElement[i].checked = true;
		var tmp = selectElement[i].parentNode.innerHTML;
		var re = /\>(\d+)/i;
		var found = tmp.match(re)[1];

		if (v == 0)
		{
			//console.log(1);
			switch (parseInt(found))
			{
				case 107 : 
				case 161 : 
				case 180 : 
				case 190 : 
				case 500 : 
				case 307 : 
				case 309 : 
				case 501 :
					//console.log(found);
					selectElement[i].checked = false;
					break;
				default:
					break;
			}	
		}
		else if (v == 1)
		{
			//console.log(1);
			switch (parseInt(found))
			{
				case 101 :
				case 102 :
				case 103 :
				case 104 :
				case 106 :
				case 107 :
				case 108 :
				case 111 :
				case 121 :
				case 161 :
				case 180 :
				case 190 :
				case 500 :
				case 197 :
				case 199 :
				case 208 :
				case 209 :
				case 213 :
				case 214 :
				case 215 :
				case 217 :
				case 218 :
				case 219 :
				case 225 :
				case 227 :
				case 307 :
				case 308 :
				case 309 :
				case 501 :
				case 311 :
					//console.log(found);
					selectElement[i].checked = false;
					break;
				default:
					break;
			}	
		}
		else if (v == 2)
		{
			//console.log(1);
			switch (parseInt(found))
			{
				case 100 :
				case 101 :
				case 102 :
				case 103 :
				case 104 :
				case 105 :
				case 106 :
				case 107 :
				case 108 :
				case 109 :
				case 111 :
				case 121 :
				case 130 :
				case 140 :
				case 141 :
				case 145 :
				case 146 :
				case 147 :
				case 148 :
				case 149 :
				case 200 :
				case 150 :
				case 151 :
				case 152 :
				case 153 :
				case 154 :
				case 160 :
				case 161 :
				case 171 :
				case 174 :
				case 175 :
				case 180 :
				case 190 :
				case 191 :
				case 500 :
				case 195 :
				case 196 :
				case 197 :
				case 198 :
				case 199 :
				case 207 :
				case 208 :
				case 209 :
				case 211 :
				case 212 :
				case 213 :
				case 214 :
				case 215 :
				case 217 :
				case 218 :
				case 219 :
				case 221 :
				case 222 :
				case 225 :
				case 226 :
				case 227 :
				case 303 :
				case 305 :
				case 307 :
				case 308 :
				case 309 :
				case 501 :
				case 311 :
					//console.log(found);
					selectElement[i].checked = false;
					break;
				default:
					break;
			}	
		}
	}
}

function prod_vis(id, value)
{
	//console.log(hide_products + " " + id + ' ' + value);
	var div = document.querySelector("#prod" + id).parentNode.parentNode;
	if (!value)
		div.style.display = "block";
	if (hide_products && value)
		div.style.display = "none";
	//console.log();
}

function change_view(b)
{
	//console.log(b.value + " " + b.innerHTML);
	var dd = document.querySelectorAll('#result2 > div');
	//console.log(dd.length);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Показать все';
		b.value = 'show';
		hide_products = true;
		//console.log('dd = ' + dd.length);
		for (var i = 1; i<dd.length; i++)
		{
			//console.log('dd[' + i + '] = ' + dd[i].innerHTML);
			//if (dd[i].querySelector('.tab7 > input').checked)
			if (dd[i].querySelector('.res4 > input').checked)
				dd[i].style.display = "none";
		}
	}
	else
	{
		b.innerHTML = 'Исключить выбранные';
		b.value = 'hide';
		hide_products = false;
		for (var i = 1; i<dd.length; i++)
			dd[i].style.display = "block";
	}
	//console.log("after " + b.value + " " + b.innerHTML);
}

function ch_view_b(b)
{
	//console.log(b.value + " " + b.innerHTML);
	//var dd = document.querySelectorAll('#result2 > div');
	//console.log(dd.length);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Режим демонстрации';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;
		//setCookie('bank_view', 'Режим демонстрации', opt);
		setCookie2('bank_view', 'Режим демонстрации', opt.expires);
		b.value = 'show';
		console.log("h:" + getCookie2('bank_view'));
		//hide_products = true;
		//console.log('dd = ' + dd.length);
		/*for (var i = 1; i<dd.length; i++)
		{
			//console.log('dd[' + i + '] = ' + dd[i].innerHTML);
			//if (dd[i].querySelector('.tab7 > input').checked)
			if (dd[i].querySelector('.res4 > input').checked)
				dd[i].style.display = "none";
		}*/
	}
	else
	{
		b.innerHTML = 'Режим работы';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;
//		setCookie('bank_view', 'Режим работы', opt);		
		setCookie2('bank_view', 'Режим работы', opt.expires);
		b.value = 'hide';
		
		console.log("s:" + getCookie2('bank_view'));
		//hide_products = false;
		//for (var i = 1; i<dd.length; i++)
		//	dd[i].style.display = "block";
	}
	//console.log("after " + b.value + " " + b.innerHTML);
	//var tmp_r1 = document.querySelector('#result1');
	var tmp_r2 = document.querySelector('#result2');
	var tmp_divs = tmp_r2.querySelectorAll('div > .res2');
	console.log(tmp_divs.length);
	for (var i = 0; i < tmp_divs.length; i++)
	{
		tmp_divs[i].style.display = (b.value == 'hide') ? "none" : "block";
	}
}


function ajaxQuery(url, method, param, async, onsuccess, onfailure, onprogress) 
{
	var xmlHttpRequest = new XMLHttpRequest();
	xmlHttpRequest.onprogress = onprogress;
	var callback = function(r) 
	{ 
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r)); 
	};
	
	if(async) 
	{ 
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } } 
	}
	xmlHttpRequest.open(method, url, async);
	if(method == 'POST') { xmlHttpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); }
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}



function getQueryString(tmp) {
	var queryString = '';
	[].forEach.call(tmp, function(x) {
		if(x.type=='radio' && !x.checked) { return; }
		let tmp = getInputValue(x);
		let reg = new RegExp("\&", "igm")
		let res = tmp.replace(reg, ';amp;');
		queryString += x.name+'='+(x.type=='checkbox' && !x.checked ? 0 : res)+'&';
	});

	return queryString.slice(0, -1);
}

function getInputValue(input) {

	if(input.tagName.toLowerCase()=='select') { 
		input = input.querySelector('option:checked');	}

/*	if(input.tagName.toLowerCase()=='textarea') {
		console.log(input.innerHTML);
		return input.value.replaceAll('&', ';amp;');
	}*/

	var type = ['input', 'option', 'textarea'].indexOf(input.tagName.toLowerCase())!=-1 ? 0 : 1;
	Types = [function() { return input.value; }, function() {return input.textContent }];
	return Types[type]();
}



function setCookie(name, value, options)
{
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name)
{
  setCookie(name, "", {
    expires: -1
  })
}

function setCookie2(name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie2(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function getCookie(name)
{
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

//not use just next all

function getFormElems(form) 
{
	var x, y, result = [], thisFormElements = form.elements;
	for(y = 0; y < thisFormElements.length; y++)
	{
		result.push(thisFormElements[y]);
	}
	return result;
}

/*function convert(str) 
{
	var win = {1025: 168, 1105: 184, 8470: 185, 8482: 153};
	var ret = [].map.call(str, function(X-) { t = x.charCodeAt(0); t = win[t] ? win[t] : (t > 1039 ? t-848 : t); return (t < 16 ? '%0' : '%') + t.toString(16); });
	return ret.join('');
}*/
function serialize( mixed_val ) 
{    // Generates a storable representation of a value
    // 
    // +   original by: Ates Goral (http://magnetiq.com)
    // +   adapted for IE: Ilia Kantor (http://javascript.ru)
 
    switch (typeof(mixed_val)){
        case "number":
            if (isNaN(mixed_val) || !isFinite(mixed_val)){
                return false;
            } else{
                return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";
            }
        case "string":
            return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";
        case "boolean":
            return "b:" + (mixed_val ? "1" : "0") + ";";
        case "object":
            if (mixed_val == null) {
                return "N;";
            } else if (mixed_val instanceof Array) {
                var idxobj = { idx: -1 };
		var map = []
		for(var i=0; i<mixed_val.length;i++) {
			idxobj.idx++;
                        var ser = serialize(mixed_val[i]);
 
			if (ser) {
                        	map.push(serialize(idxobj.idx) + ser)
			}
		}                                       

                return "a:" + mixed_val.length + ":{" + map.join("") + "}"

            }
            else {
                var class_name = get_class(mixed_val);
 
                if (class_name == undefined){
                    return false;
                }
 
                var props = new Array();
                for (var prop in mixed_val) {
                    var ser = serialize(mixed_val[prop]);
 
                    if (ser) {
                        props.push(serialize(prop) + ser);
                    }
                }
                return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
            }
        case "undefined":
            return "N;";
    }
 
    return false;
}


//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////
//////////////////////////