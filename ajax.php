<?php
header('Content-Type: text/html; charset=UTF-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('SYSTEM_START_9876543210', true);

include_once "_bdc.php";
include_once "_functions.php";
include_once "_config.php";

$login_ = "";
$token_ = "";
if (!empty ($_COOKIE['login']))
	$login_ = $_COOKIE['login'];	//Добавить удаление ненужных символов
if (!empty ($_COOKIE['token']))
	$token_ = $_COOKIE['token'];	//Добавить удаление ненужных символов

if (!authorized($login_, $token_))
{
	mysqli_close($db_connect);
	$res['status'] = 'failed';
	$res['msg'] = "Access denied";
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
	//die("Access denied");
}

$staff_position_debug = $staff_position;
$staff_id_debug = $staff_id;

if (!empty ($_COOKIE['debug']))
{
	if ($_COOKIE['debug'] == 1)
	{
		$debug_id = 0 + $_COOKIE['debug_id'];

		if($result2 = mysqli_query($db_connect, "SELECT * FROM staff WHERE id='$debug_id' AND deleted = '0' ORDER BY id DESC LIMIT 1"))
		{
			if (mysqli_num_rows($result2))
			{
				$check = mysqli_fetch_assoc($result2);
				
				$staff_id = $debug_id;
				$staff_login = $check['login'];
				$staff_office = $check['office'];
				$staff_position = $check['position'];
				$staff_lastname = $check['lastname'];
				$staff_firstname = $check['firstname'];
				$staff_email_corp = $check['email_corp'];
				$staff_phone_work = $check['phone_work'];
				$staff_status = $check['status'];
				$staff_za = $check['fin_an'];
				$staff_pc = $check['plan_check'];
				$staff_bc = $check['staff_bc'];
			}
			mysqli_free_result($result2);
		}
		else
		{
			echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
			
			return false;
		}
	}
}


if (isset ($_POST["action"]))
{
	if ($_POST["action"] == 'logout')
	{
		if(logout($staff_id_debug, $login_, $token_))
		{
			$res['status'] = 'ok';
			$res['msg'] = "logout";
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "logout";
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
	else if ($_POST["action"] == 'my_status')
	{
		$st = ($_POST['st'] == 'busy') ? 'busy' : 'ready';
		$sql = "UPDATE staff SET status='$st', st_time_set=NOW() WHERE id='$staff_id'";
		if ($result = $db_connect->query($sql))
		{
			$res['status'] = 'ok';
		}
		else
		{
			$res['status'] = 'failed';
			$res['msg'] = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		}
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		die();
	}
}

if (isset ($_POST["block"]))
	$c = $_POST["block"];

if (isset ($_GET["block"]) && $_GET["block"] == 'anketa')
	$c = $_GET["block"];

if (isset ($_GET["block"]) && $_GET["block"] == 'metriks')
	$c = $_GET["block"];

if (isset ($_GET["block"]) && $_GET["block"] == 'pf')
	$c = $_GET["block"];

if (isset ($_GET["block"]) && $_GET["block"] == 'analitics')
	$c = $_GET["block"];

if (!isset($c) || !check_access($c))
{
	$res['status'] = 'failed';
	$res['msg'] = "Access denied!";
	$res['req'] = $_POST["block"];
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

switch($c)
{
	case "worktable" :
	{
		include_once "ajax/aworktable.php";
		break;
	}
	case "list" :
	{
		include_once "ajax/alist.php";
		break;
	}
	case "signals" :
	{
		include_once "ajax/signals_ajax.php";
		break;
	}
	case "pipeline" :
	{
		include_once "ajax/apipeline.php";
		break;
	}
	case "listsotr" :
	{
		include_once "ajax/alistsotr.php";
		break;
	}
	case "mstat" :
	{
		include_once "ajax/amstat.php";
		break;
	}
	case "listmetrika" :
	{
		include_once "ajax/alistmetrika.php";
		break;
	}
	case "bankrequest" :
	{
		include_once "ajax/bankrequest.php";
		break;
	}
	case "bankrequest2" :
	{
		include_once "ajax/bankrequest2.php";
		break;
	}
	case "asettings" :
	{
		include_once "ajax/asettings.php";
		break;
	}
	case "staff" :
	{
		include_once "ajax/astaff.php";
		break;
	}
	case "agents" :
	{
		include_once "ajax/ajax_agents.php";
		break;
	}
	case "anketa" :
	{
		include_once "ajax/ajax_anketa.php";
		break;
	}
	case "settings" :
	{
		include_once "ajax/ajax_settings.php";
		break;
	}
	case "common" :
	{
		include_once "ajax/ajax_common.php";
		break;
	}
	case "banks" :
	{
		include_once "ajax/abanks.php";
		break;
	}
	case "metriks" :
	{
		include_once "ajax/ametriks.php";
		break;
	}
	case "pf" :
	{
		include_once "ajax/apf.php";
		break;
	}
	case "analitics" :
	{
		include_once "ajax/ajax_analitics.php";
		break;
	}
	default :
	{
		$res['status'] = 'failed';
		$res['msg'] = "Ошибка выбора блока";
		print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
		break;
	}
}

mysqli_close($db_connect);
?>