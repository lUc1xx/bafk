<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

insert_into_action_log('open', $staff_id_debug, array('formid' => $_id));

$error = false;
$in_tm = false;
$in_ozs = false;
$all_fields_mass = array();
$all_fields_mass_or = array();
$subids_mass = array();
$fields_mass = array();
$block_mass = array();
$anketa_mass = array();
$notes_mass_a = array();
$data_mass_a = array();
$mass_for_js = array();
$status_mass = array();
$banks_mass_tmp = array();
$banks_mass = array();
$regions_mass = array();
$fias_mass = array();
$file_mass = array();
$algo_mass = array();

if ($result = $db_connect->query("SELECT id, name FROM old_banks WHERE adata='1' ORDER BY name ASC;"))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		if (mb_strtolower($row['name']) == 'нет')
			$banks_mass[$row['id']] = $row['name'];
		else
			$banks_mass_tmp[$row['id']] = $row['name'];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if (!$error)
{
	foreach($banks_mass_tmp as $k => $v)
		$banks_mass[$k] = $v;
}

if ($result = $db_connect->query("SHOW COLUMNS FROM forms WHERE Field = 'status';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$status_list = $out[1];
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

if ($result = $db_connect->query("SELECT formalname, shortname, code FROM addrobj_city;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		//$regions_mass[$row['id']] = $row['shortname'] . ". " . $row['formalname'];
		$fias_mass[$row['formalname'] . ", " . $row['shortname']] = $row['code'];
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$office_filter = '';
if ($staff_office_type != 'main')
{
	if ($staff_office_type == 'tm')
	{
		if ($staff_position == 'director_tm')
			$office_filter = " AND av_tm_dir='1'";
		else if ($staff_position == 'manager_tm')
			$office_filter = " AND av_tm_man='1'";
	}
	else if ($staff_office_type == 'ozs')
	{
		if ($staff_position == 'director_ozs')
			$office_filter = " AND av_ozs_dir='1'";
		else if ($staff_position == 'manager_ozs')
			$office_filter = " AND av_ozs_man='1'";
	}
}

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block'$office_filter ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($fields_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

$cr_his_0_name = '';

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type!='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		if ($row['name'] == 'field96')
			$cr_his_0_name = $row['description'];
		
		array_push($all_fields_mass, $row);
		$all_fields_mass_or[$row['name']] = $row['description'];
		
		//if ($staff_id != 1)
			//continue;
		
		$val = '';
		
		if ($row['type'] == 'checkbox')
			$val = 'да';
		else if ($row['type'] == 'select')
		{
			switch($row['name'])
			{
				case "field17" : $val = 'мужской'; break;
				case "field30" : $val = 'в браке'; break;
			}
			
		}
		
		if ($row['type'] != 'block')
		{
			if (isset($subids_mass[$row['name']]))
				$subids_mass[$row['name']]["value"] = $val;
			else
				$subids_mass[$row['name']] = array("value" => $val, "subids" => array(), "mainid" => '');
			
			if (isset($row['mainid']))
			{
				if (($row['mainid'] != null) && ($row['mainid'] != ''))
				{
					if (isset($subids_mass[$row['mainid']]["subids"]))
					{
						array_push($subids_mass[$row['mainid']]["subids"], $row['name']);
						$subids_mass[$row['name']]["mainid"] = $row['mainid'];
					}
				}
			}
		}
			
		
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

/*if ($staff_id == 1)
{
	echo "<pre>";
	print_r($subids_mass);
	echo "</pre>";
}*/

$sql = "SELECT * FROM form_fields_settings WHERE en = '1' AND type='block' ORDER BY position ASC;";
if ($result = $db_connect->query($sql))
{
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($block_mass, $row);
	}
	$result->close();
}
else
{
	$error = true;
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error . "<br>" . $sql;
	echo $res;
}

if ($result = $db_connect->query("SELECT position, name_desc, algo_text FROM work_algo;"))
{
	while($val = $result->fetch_array(MYSQLI_ASSOC))
	{
		$algo_mass[$val['position']] = $val;
	}
	
	$result->close();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "1: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

if (!$error)
{
	/*echo "<pre>";
	print_r($fields_mass);
	echo "</pre>";
	*/
	if ($result = $db_connect->query("SELECT * FROM forms WHERE id = '$_id';"))
	{
		if ($result->num_rows)
		{
			$anketa_mass = $result->fetch_array(MYSQLI_ASSOC);
		}
		else
		{
			$res = "Не найдена анкета";
			echo $res;
			$error = true;
		}
		
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}

if (0 + $staff_id == 0 + $anketa_mass['manager'])
{
	update_last_action($_id, 'read_form');
	update_last_msg_in_form($_id);
	
	if ($anketa_mass['last_man'] != $staff_id)
	{
		
		addman2history($_id, $staff_id, $staff_mass[$staff_id]['office']);
		$sql_u = "UPDATE forms SET last_man='$staff_id' WHERE id='$_id';";
		$result = $db_connect->query($sql_u);
	}
}



if (!$error)
{
	$man 		= $anketa_mass['manager'];
	$man_tm 	= $anketa_mass['tm_man'];
	$man_ozs 	= $anketa_mass['ozs_man'];
	
	$man_of = 0;
	$man_of_act = 0;
	
	if ($result = $db_connect->query("SELECT office FROM staff WHERE id = '$man' OR id = '$man_tm';"))
	{
		if ($result->num_rows)
		{
			$of_row = $result->fetch_array(MYSQLI_ASSOC);
			$man_of = $of_row['office'];
		}
		else
		{
			$res = "Не найден сотрудник";
			//echo $res;
			//$error = true;
		}
		
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}

	if ($result = $db_connect->query("SELECT office FROM staff WHERE id = '$man';"))
	{
		if ($result->num_rows)
		{
			$of_row = $result->fetch_array(MYSQLI_ASSOC);
			$man_of_act = $of_row['office'];
		}
		else
		{
			$res = "Не найден сотрудник";
			//echo $res;
			//$error = true;
		}
		
		$result->close();
	}
	else
	{
		$error = true;
		$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
		echo $res;
	}
}

if ($anketa_mass['type'] == 'mini') {
    include_once "mini.php";
} else if ($staff_id == 11111 && $anketa_mass['type'] == 'cooperate') {
    include_once "cooperate.php";
} else if ($anketa_mass['type'] == 'cooperate') {
    include_once "mini.php";
}
