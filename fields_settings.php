<?php
if (!defined('SYSTEM_START_9876543210')) exit;

if ($result = $db_connect->query("SHOW COLUMNS FROM form_fields_settings WHERE Field = 'type';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$type_list = $out[1];
	$result->close();
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$block_list = array();

if ($result = $db_connect->query("SELECT * FROM form_fields_settings WHERE type='block' ORDER BY position ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
		$block_list[] = $row;
}
else
{
	$res = "Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

echo '<div onclick="show(\'none\')" id="wrap"></div>';
echo '<div id="window" class="window">';
echo '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
echo '<div id="values_wrap"></div>';
echo '</div>';

$block_list2 = array();
if ($result = $db_connect->query("SELECT * FROM form_fields_settings WHERE type='block' ORDER BY position ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$block_list2[$row['id']] = $row;
	}
	$result->close();
	
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

//$res = array();

$colormass = array("black", "white", "red", "green", "blue", "yellow", "orange");

$field_mass = array();
if ($result = $db_connect->query("SELECT * FROM form_fields_settings WHERE type != 'block' ORDER BY position ASC;"))
{
	//$res = "Select вернул " . $result->num_rows . " строк.\n";
	

	echo '<div id="cont4anketa">';
	echo "<div class=\"head_table\">";
	echo '<div class="list_cell">ID</div>';
	echo '<div class="list_cell">Поз.</div>';
	echo '<div class="list_cell">Вкл.</div>';
	echo '<div class="list_cell">Блок</div>';
	echo '<div class="list_cell">Связь</div>';
	echo '<div class="list_cell">Тип</div>';
	echo '<div class="list_cell">Текст</div>';
	echo '<div class="list_cell">Фон</div>';
	echo '<div class="list_cell">Описание</div>';
	echo '<div class="list_cell">ТМ рук.</div>';
	echo '<div class="list_cell">ТМ мен.</div>';
	echo '<div class="list_cell">ОЗС рук.</div>';
	echo '<div class="list_cell">ОЗС мен.</div>';
	echo '<div class="list_cell">Агент ред.</div>';
	echo '<div class="list_cell">Агент чит.</div>';
	echo "</div>";
		
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		array_push($field_mass, $row);
	}
	
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}

$row_odd = "row_odd";	//нечет
$row_even = "row_even";	//чет
$c = false;

foreach($block_list2 as $bk => $bv)
{
	$row = $bv;
	/*$fs_id = $row['id'];
	$c = !$c;
	$cl = ($c) ? $row_odd : $row_even;
	echo "<div class=\"$cl\">";*/
	echo "<div class=\"row_block\">";
	echo '<div class="list_cell">' . $row['id'] . '</div>';
	echo '<div class="list_cell"><div style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_position(this, ' . $row['id'] . ');">' . $row['position'] . '</div></div>';
	$ch = ($row['en']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ch_en" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	echo '<div class="list_cell">';
	
	$block = ($row['block'] == null) ? "" : $row['block'];
	echo '<select name="block" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
	echo '<option value="">нет</option>';
	foreach($block_list as $bl)
	{
		$selb = ($block == $bl['id']) ? "selected" : "";
		$vali = $bl['id'];
		$valb = $bl['description'];
		echo "<option value=\"$vali\" $selb>$valb</option>";
	}
	echo '</select>';
	echo '</div>';
	
	$goal = ($row['goal'] == null) ? "нет" : $row['goal'];
	echo '<div class="list_cell" style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_goal(this, ' . $row['id'] . ');" >' . $goal . '</div>';

	echo '<div class="list_cell">';
	echo '<select name="ftype" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
	foreach ($type_list as $k => $v)
	{
		$sel = ($v == $row['type']) ? " selected" : "";
		echo '<option value="' . $v . "\"$sel>" . $field_type_desc[$v] . '</option>';
	}
	echo '</select>';
	if (($row['type'] == 'select') || ($row['type'] == 'multiselect') || ($row['type'] == 'radio') || ($row['type'] == 'kred_prod'))
		echo '<button id="avalues_' . $fs_id . '" style="margin-top:5px;" onclick="javascript:add_val(' . $fs_id . ');">Значения</button>';
	echo '</div>';
	
	
	echo '<div class="list_cell"> </div>';
	echo '<div class="list_cell"> </div>';
	
	echo '<div class="list_cell"><input id="desc_' . $row['id'] . '" onfocus="javascript:focus_field(this);" onkeyup="javascript:blur_field(this);" type="text" value="' . $row['description'] . '"><span style="display:none; color:red; cursor:pointer;" onclick="javascript:save_description(this);" id="sp_' . $row['id'] . "\">Сохранить</span>";
	echo '<div><font style="font-style: italic;">';
	$data = explode('::', $row['av_values']);
	$first = true;
	foreach ($data as $k => $v)
	{
		if ($first)
			$first = false;
		else
			echo ", ";
		echo $v;
	}
	echo '</font></div></div>';
	$ch = ($row['av_tm_dir']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="tm_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_tm_man']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="tm_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_ozs_dir']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ozs_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_ozs_man']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ozs_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';

	$ch = ($row['agent_edit']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="agent_edit" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['agent_read']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="agent_read" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	echo "</div>";
	
	foreach($field_mass as $fk => $row)
	{
		if ($row['block'] != $bv['id'])
			continue;
		$fs_id = $row['id'];
		$c = !$c;
		$cl = ($c) ? $row_odd : $row_even;
		echo "<div class=\"$cl\">";
		echo '<div class="list_cell">' . $row['id'] . '</div>';
		echo '<div class="list_cell"><div style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_position(this, ' . $row['id'] . ');">' . $row['position'] . '</div></div>';
		$ch = ($row['en']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="ch_en" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		echo '<div class="list_cell">';
		
		$block = ($row['block'] == null) ? "" : $row['block'];
		echo '<select name="block" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
		echo '<option value="">нет</option>';
		foreach($block_list as $bl)
		{
			$selb = ($block == $bl['id']) ? "selected" : "";
			$vali = $bl['id'];
			$valb = $bl['description'];
			echo "<option value=\"$vali\" $selb>$valb</option>";
		}
		echo '</select>';
		echo '</div>';
		
		$goal = ($row['goal'] == null) ? "нет" : $row['goal'];
		echo '<div class="list_cell" style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_goal(this, ' . $row['id'] . ');" >' . $goal . '</div>';
	
		echo '<div class="list_cell">';
		echo '<select name="ftype" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
		foreach ($type_list as $k => $v)
		{
			$sel = ($v == $row['type']) ? " selected" : "";
			echo '<option value="' . $v . "\"$sel>" . $field_type_desc[$v] . '</option>';
		}
		echo '</select>';
		if (($row['type'] == 'select') || ($row['type'] == 'multiselect') || ($row['type'] == 'radio') || ($row['type'] == 'kred_prod'))
			echo '<button id="avalues_' . $fs_id . '" style="margin-top:5px;" onclick="javascript:add_val(' . $fs_id . ');">Значения</button>';
		echo '</div>';
		
		$sel_color = '';
		if ($row['font_color'] == null)
			$sel_color = ' style="background-color:black;"';
		else
			$sel_color = ' style="background-color:' . $row['font_color'] . ';"';
		
		$ch_color = ' onchange="javascript:change_type(this, ' . $row['id'] . ');"';
		
		echo "<div class=\"list_cell\"><select$sel_color$ch_color name='font_color'>";
		
		foreach ($colormass as $color_v)
		{
			$color_selected = '';
			if ($color_v == $row['font_color'])
				$color_selected = ' selected';
			else if (($row['font_color'] == null) && $color_v == 'black')
				$color_selected = ' selected';
				
			echo "<option value=\"$color_v\" style='width:23px; background-color: $color_v;'$color_selected>&nbsp;</option>";
		}
		
		echo '</select></div>';
		
		
		
		$sel_color = '';
		if (($row['back_color'] == null) || ($row['back_color'] == 'transparent'))
		{
			$sel_color = ' selected';
		}
		else
			$sel_color = ' style="background-color:' . $row['back_color'] . ';"';
		
		$ch_color = ' onchange="javascript:change_type(this, ' . $row['id'] . ');"';
		
		echo "<div class=\"list_cell\"><select$sel_color$ch_color name='back_color'>";
		echo "<option value=\"transparent\" style='width:23px;'$color_selected>нет</option>";
		foreach ($colormass as $color_v)
		{
			$color_selected = '';
			if ($color_v == $row['back_color'])
				$color_selected = ' selected';
			echo "<option value=\"$color_v\" style='width:23px; background-color: $color_v;'$color_selected>&nbsp;</option>";
		}		
		echo '</select></div>';
				
		echo '<div class="list_cell"><input id="desc_' . $row['id'] . '" onfocus="javascript:focus_field(this);" onkeyup="javascript:blur_field(this);" type="text" value="' . $row['description'] . '"><span style="display:none; color:red; cursor:pointer;" onclick="javascript:save_description(this);" id="sp_' . $row['id'] . "\">Сохранить</span>";
		echo '<div><font style="font-style: italic;">';
		$data = explode('::', $row['av_values']);
		$first = true;
		foreach ($data as $k => $v)
		{
			if ($first)
				$first = false;
			else
				echo ", ";
			echo $v;
		}
		echo '</font></div></div>';
		$ch = ($row['av_tm_dir']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="tm_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		$ch = ($row['av_tm_man']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="tm_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		$ch = ($row['av_ozs_dir']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="ozs_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		$ch = ($row['av_ozs_man']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="ozs_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		$ch = ($row['agent_edit']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="agent_edit" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		$ch = ($row['agent_read']) ? "checked" : "";
		echo '<div class="list_cell"><input type="checkbox" name="agent_read" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
		echo "</div>";
	}
}

echo "<div class=\"row_block\">";
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">Дополнительная информация</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo '<div class="list_cell">-</div>';
echo "</div>";

foreach($field_mass as $fk => $row)
{
	if ($row['block'] != null)
		continue;
	$fs_id = $row['id'];
	$c = !$c;
	$cl = ($c) ? $row_odd : $row_even;
	echo "<div class=\"$cl\">";
	echo '<div class="list_cell">' . $row['id'] . '</div>';
	echo '<div class="list_cell"><div style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_position(this, ' . $row['id'] . ');">' . $row['position'] . '</div></div>';
	$ch = ($row['en']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ch_en" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	echo '<div class="list_cell">';
	
	$block = ($row['block'] == null) ? "" : $row['block'];
	echo '<select name="block" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
	echo '<option value="">нет</option>';
	foreach($block_list as $bl)
	{
		$selb = ($block == $bl['id']) ? "selected" : "";
		$vali = $bl['id'];
		$valb = $bl['description'];
		echo "<option value=\"$vali\" $selb>$valb</option>";
	}
	echo '</select>';
	echo '</div>';
	
	$goal = ($row['goal'] == null) ? "нет" : $row['goal'];
	echo '<div class="list_cell" style="cursor:pointer; font-style:italic; text-align: center;" onclick="javascript:ch_goal(this, ' . $row['id'] . ');" >' . $goal . '</div>';

	echo '<div class="list_cell">';
	echo '<select name="ftype" onchange="javascript:change_type(this, ' . $row['id'] . ');">';
	foreach ($type_list as $k => $v)
	{
		$sel = ($v == $row['type']) ? " selected" : "";
		echo '<option value="' . $v . "\"$sel>" . $field_type_desc[$v] . '</option>';
	}
	echo '</select>';
	if (($row['type'] == 'select') || ($row['type'] == 'multiselect') || ($row['type'] == 'radio') || ($row['type'] == 'kred_prod'))
		echo '<button id="avalues_' . $fs_id . '" style="margin-top:5px;" onclick="javascript:add_val(' . $fs_id . ');">Значения</button>';
	echo '</div>';
	
	$sel_color = '';
	if ($row['font_color'] == null)
		$sel_color = ' style="background-color:black;"';
	else
		$sel_color = ' style="background-color:' . $row['font_color'] . ';"';
		
	$ch_color = ' onchange="javascript:change_type(this, ' . $row['id'] . ');"';
		
	echo "<div class=\"list_cell\"><select$sel_color$ch_color name='font_color'>";
	
	foreach ($colormass as $color_v)
	{
		$color_selected = '';
		if ($color_v == $row['font_color'])
			$color_selected = ' selected';
		else if (($row['font_color'] == null) && $color_v == 'black')
			$color_selected = ' selected';
			
		echo "<option value=\"$color_v\" style='width:23px; background-color:$color_v;'$color_selected>&nbsp;</option>";
	}
	echo '</select></div>';
		
		
	$sel_color = '';
	if (($row['back_color'] == null) || ($row['back_color'] == 'transparent'))
	{
		$sel_color = ' selected';
	}
	else
		$sel_color = ' style="background-color:' . $row['back_color'] . ';"';
	
	$ch_color = ' onchange="javascript:change_type(this, ' . $row['id'] . ');"';
	
	echo "<div class=\"list_cell\"><select$sel_color$ch_color name='back_color'>";
	echo "<option value=\"transparent\" style='width:23px;'$color_selected>нет</option>";
	foreach ($colormass as $color_v)
	{
		$color_selected = '';
		if ($color_v == $row['back_color'])
			$color_selected = ' selected';
		echo "<option value=\"$color_v\" style='width:23px; background-color: $color_v;'$color_selected>&nbsp;</option>";
	}		
	echo '</select></div>';
		
	echo '<div class="list_cell"><input id="desc_' . $row['id'] . '" onfocus="javascript:focus_field(this);" onkeyup="javascript:blur_field(this);" type="text" value="' . $row['description'] . '"><span style="display:none; color:red; cursor:pointer;" onclick="javascript:save_description(this);" id="sp_' . $row['id'] . "\">Сохранить</span>";
	echo '<div><font style="font-style: italic;">';
	$data = explode('::', $row['av_values']);
	$first = true;
	foreach ($data as $k => $v)
	{
		if ($first)
			$first = false;
		else
			echo ", ";
		echo $v;
	}
	echo '</font></div></div>';
	$ch = ($row['av_tm_dir']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="tm_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_tm_man']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="tm_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_ozs_dir']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ozs_dir" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['av_ozs_man']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="ozs_man" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['agent_edit']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="agent_edit" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	$ch = ($row['agent_read']) ? "checked" : "";
	echo '<div class="list_cell"><input type="checkbox" name="agent_read" value="1" ' . $ch . ' onclick="javascript:enable_field(this, this.checked, ' . $row['id'] . ')"></div>';
	echo "</div>";
}

echo "</div>";

?>

