var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;
window.onload = update_all_data;

var events = [
    {title: 'Business Lunch', start: '2018-06-13T13:00:00'},
    {title: 'Meeting', start: '2018-06-13T11:00:00'},
    {title: 'Conference', start: '2018-06-18', end: '2017-12-20'},
];

function update_all_data()
{
	get_events_count();
	get_tasks_count();
	
	window.quasarConfig = {
    brand: { // this will NOT work on IE 11
      primary: '#e46262',
      // ... or all other brand colors
    },
    notify: {}, // default set of options for Notify Quasar plugin
    loading: {}, // default set of options for Loading Quasar plugin
    loadingBar: {}, // settings for LoadingBar Quasar plugin
    // ..and many more
  }
	Quasar.lang.set(Quasar.lang.ru);
	new Vue({
        el: '#mapp',
        data: function () {
          return {
			alert: false,
			msgalert: '',
			loading: false,
			date_from: '2020/01/01',
			//date_to: '2020/03/01',
			date_to: Quasar.utils.date.formatDate(Date.now(), 'YYYY/MM/DD'),
			pagination: {
				sortBy: 'sum',
				descending: true,
				rowsPerPage: 50
			},
			columns: [
			/*{ 
				name: 'new', 		
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
			  	label: "Новые", field: 'new', sortable: true },
			{ 
				name: 'site', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Сайт', field: 'site', sortable: true },
			{ 
				name: 'men', 		
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
			  	label: "Мэн", field: 'men', sortable: true },
			{ 
				name: 'tel', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Тел', field: 'tel', sortable: true },
			{ 
				name: 'api', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'API', field: 'api', sortable: true },
			{ 
				name: 'want', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'want', field: 'want', sortable: true },
			{ 
				name: 'sum', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Всего', field: 'sum', sortable: true },
			{ 
				name: 'dsite', style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Сайт Д.', field: 'dsite', sortable: true },
			{ 
				name: 'dmen', 		
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
			  	label: "Мэн Д.", field: 'dmen', sortable: true },
			{ 
				name: 'dtel', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Тел Д.', field: 'dtel', sortable: true },
			{ 
				name: 'dapi', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'API Д.', field: 'dapi', sortable: true },
			{ 
				name: 'dwant', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'want Д.', field: 'dwant', sortable: true },
			{ 
				name: 'dsum', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Всего Д.', field: 'dsum', sortable: true },
			{ 
				name: 'ksite', style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Сайт К.', field: 'ksite', sortable: true },
			{ 
				name: 'kmen', 		
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
			  	label: "Мэн К.", field: 'kmen', sortable: true },
			{ 
				name: 'ktel', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Тел К.', field: 'ktel', sortable: true },
			{ 
				name: 'kapi', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'API К.', field: 'kapi', sortable: true },
			{ 
				name: 'kwant', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'want К.', field: 'kwant', sortable: true },
			{ 
				name: 'ksum', 	
				style: 'width: 100px;max-width: 100px;', 
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Всего К.', field: 'ksum', sortable: true },*/
		  ],
		  data: [],
			}
        },
        methods: {
			/*mounted() {
				let timeStamp = Date.now();
				this.date_to = date.formatDate(timeStamp, 'YYYY/MM/DD');
				console.log(this.date_to);
			},*/
			loaddata () {
				this.loading = true;
				let url = `/ajax.php?block=pf&action=getdata&df=${this.date_from}&dto=${this.date_to}`;
				//let response = await fetch(url);
				//alert(response);
				fetch(url)
				.then(response => response.json())
				.then(data => {
					//console.log(data)
					if(data.status == 'ok') {
						this.data = new Array();
						this.columns = new Array();
						this.setcol(data.resultArrayC);
						console.log(data.resultArray);
						this.setdata(data.resultArray);
					}
					else if (data.status == 'failed') {
						this.msgalert = data.msg;
						this.alert = true;
					}
					this.loading = false;
				});
			},
			setcol(d) {
				for(col of d) {
					this.columns[this.columns.length] = {
						name: col, 
						style: 'width: 100px;max-width: 100px;', 
						headerStyle: 'width: 100px; max-width: 100px;',
						label: col=='name' ? '' : col,
						field: col
					}

				}
				console.log('update Columns');
				console.log(this.columns);
				//this.data = d;
			},
			setdata(d) {
				console.log('update data');
				this.data = d;
				console.log(this.data);
			}
		},
		
        // ...etc
      })
}









//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds