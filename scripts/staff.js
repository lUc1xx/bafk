var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

var timeout = 500;
var new_worker = true;
var new_office = true;
var worker_id = 0;
var office_id = 0;

window.onload = update_all_data;

function update_all_data()
{
	//update_data();
	get_tasks_count();
	get_events_count();
}

document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
        isEscape = (evt.keyCode == 27);
    }
    if (isEscape) {
        //alert("Escape");
		show('none');
    }
};

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

var flag_list_wadd = new Array();

function change_wadd(el, id)
{
	console.log(id);
	if (flag_list_wadd[id] != 0)
		flag_list_wadd[id] = 0;
	flag_list_wadd[id]++;
	console.log(flag_list_wadd[id]);
	setTimeout(function(){update_tel_add(id, 'edit_phone_add', el);}, 500);
}

function update_tel_add(id, name, el)
{
	flag_list_wadd[id]--;
	var old_color = window.getComputedStyle(el).backgroundColor
	
	console.log(flag_list_wadd[id]);
	if (flag_list_wadd[id] == 0)
	{
		
		var data = "block=staff&action=edit_staff_param&id=" + id + "&name=" + name + "&val=" + el.value;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				//points_editor('block');
				el.style.backgroundColor = 'lightgreen';
			}
			else 
			{
				el.style.backgroundColor = 'red';
				alert(Item['msg']);
			}
			setTimeout(function(){ch_color(el, old_color);}, timeout);
		});
		console.log(data);
	}
}

function ch_tm_sort(el, id)
{
	var img = el.querySelector('img');
	var new_src = "/img/ball_green.png";
	var src = img.src;
	var re = new RegExp("ball_green\.png", "");
	var st = '1';
	if (re.test(src))
	{
		st = '0';
		new_src = "/img/ball_red.png";
	}
	
	var data = "block=staff&action=tm_sort_en&st=" + st + "&stid=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			img.src = new_src;
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_fin_an(el, id)
{
	var img = el.querySelector('img');
	var new_src = "/img/ball_green.png";
	var src = img.src;
	var re = new RegExp("ball_green\.png", "");
	var st = '1';
	if (re.test(src))
	{
		st = '0';
		new_src = "/img/ball_red.png";
	}
	
	var data = "block=staff&action=fin_an_en&st=" + st + "&stid=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			img.src = new_src;
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_bc_check(el, id)
{
	var img = el.querySelector('img');
	var new_src = "/img/ball_green.png";
	var src = img.src;
	var re = new RegExp("ball_green\.png", "");
	var st = '1';
	if (re.test(src))
	{
		st = '0';
		new_src = "/img/ball_red.png";
	}
	
	var data = "block=staff&action=bc_check_en&st=" + st + "&stid=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			img.src = new_src;
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}
function ch_plan_check(el, id)
{
	var img = el.querySelector('img');
	var new_src = "/img/ball_green.png";
	var src = img.src;
	var re = new RegExp("ball_green\.png", "");
	var st = '1';
	if (re.test(src))
	{
		st = '0';
		new_src = "/img/ball_red.png";
	}
	
	var data = "block=staff&action=plan_check_en&st=" + st + "&stid=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			img.src = new_src;
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function update_data()
{
	
	var cont4staff = document.querySelector("#cont4staff");
	var divs = cont4staff.querySelectorAll("div[class=row_odd], div[class=row_even]");
	
	//console.log(divs.length);
	
	for (var i = 0; i < divs.length; i++)
	{
		//console.log(divs[i].innerHTML);
		cont4staff.removeChild(divs[i]);
	}
	
	var data = "block=staff&action=update_data";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//butt.style.backgroundColor = 'lightgreen';
			//console.log(Item['msg']);
			//setTimeout(function(){update_data_after_add();}, timeout);
			
			//console.log(Item['data_len']);
			//console.log(Item['data']);
			
			var data_ = Item['data'];
			
			var row_odd = "row_odd";	//нечет
			var row_even = "row_even";	//чет
			var c = 0;
			//for (var i = 1; i < Item['data_len'] + 1; i++)
			for (var key in data_)
			{
				var i = key;
				var data = data_[key];
				//console.log(data);
				c++;
				//console.log(key + " ::: " + Item);
				var tmp_add = "";
				var row_c = (c & 1) ? row_odd : row_even;
				tmp_add += '<div id="staff_row_' + i + '" class="' + row_c + '">';
				
				tmp_add += '<div class="list_cell">' + i + '</div>';
				tmp_add += '<div class="list_cell"><a title="Редактировать" href="#" onclick="javasccript:edit_staff(' + i + ')"><img src="/img/edit.png" width="16" height="16"></a></div>';
				tmp_add += '<div class="list_cell"><a title="Удалить" href="#" onclick="javasccript:del_staff(' + i + ')"><img src="/img/delete.png" width="16" height="16"></a></div>';
				tmp_add += '<div class="list_cell">' + data['login'] + '</div>';
				tmp_add += '<div class="list_cell">Сбросить</div>';
				tmp_add += '<div class="list_cell">' + data['lastname'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['firstname'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['patronymic'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['office'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['position'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['email'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['email_corp'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['phone_mob'] + '</div>';
				tmp_add += '<div class="list_cell">' + data['phone_work'] + '</div>';
				tmp_add += '<div class="list_cell">Настроить</div>';
				
				tmp_add += '</div>';
				cont4staff.innerHTML += tmp_add;
			}
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function update_page()
{
	location.reload();
}

function update_data_after_add()
{
	location.reload();
	return;
	show('none');
	update_data();
}


function add_ip_field()
{
	var div = document.querySelector('#ip_values_cont');
	var d = document.createElement('div');
		d.setAttribute("style", "margin-bottom:5px;");
		
		var i = document.createElement('input');
			i.type="text";
			i.setAttribute("class", "av_val_sel");
		d.appendChild(i);
		
		var img = document.createElement('img');
			img.src = "/img/delete.png";
			img.style = "cursor:pointer;";
			img.setAttribute("onclick", "javascript:del_ip_val(this);");
		d.appendChild(img);
			
	div.appendChild(d);
}

function del_ip_val(el)
{
	el.parentNode.removeChild(el.previousSibling);
	el.parentNode.removeChild(el);
}

function save_ip_values(id)
{
	var cont = document.querySelector('#ip_values_cont');
	var inp = cont.querySelectorAll('input');
	/*for (var key in inp)
	{
		console.log("start new iteration: key=" + key);
		console.log(inp[key]);
		console.log(inp[key].value);
	}*/
	var data = "block=staff&action=set_ip_avalues&id=" + id;
	inp.forEach(function(item2, i, arr){
		//console.log("start new iteration: item=" + arr[i].value);
		data += "&key" + i + "=" + arr[i].value;
	});
	//alert('save_values');
	console.log(data);
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert("Сохранено успешно");
			//show('none');
			location.reload();
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ip_access_settings(id)
{
	console.log(id);
	var data = "block=staff&action=get_worker_ip&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_ip('block');
			var div = document.querySelector('#ip_values_wrap');
			
			console.log(div);
			var tmp_val = '<div id="ip_values_cont">';
			
			if (Item['len'])
			{
				var data = Item['data'];
				for (var key in data)
				{
					tmp_val += '<div><input class="av_val_sel" type="text" value="' + data[key] + '"><img src="/img/delete.png" style="cursor:pointer;" onclick="javascript:del_ip_val(this);"></div>';
				}
			}
			tmp_val += '</div><div><button onclick="javascript:add_ip_field();">Добавить поле</button> <button onclick="javascript:save_ip_values(' + id + ');">Сохранить</button></div>';
			console.log(tmp_val);
			div.innerHTML = tmp_val;
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_staff(id)
{
	new_worker = false;
	worker_id = id;
	var nworker_add_b = document.querySelector("#nworker_add_b");
	nworker_add_b.innerHTML = "Сохранить";
	console.log(id);
		
	var new_worker_wrap = document.querySelector("#new_worker_wrap");
	var butt = document.querySelector("#nworker_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	
	var data = "block=staff&action=get_worker&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'][id];
			console.log(data);
			
			var error = false;
			for (var key in inputs)
			{
				console.log(inputs[key].name);
				if (data.hasOwnProperty(inputs[key].name))
				{
					inputs[key].value = data[inputs[key].name];
				}
			}

			
			for (var key in sel)
			{
				var name = sel[key].name;
				if ((name == 'office') || (name == 'position'))
				{
					if (data.hasOwnProperty(sel[key].name))
					{
						for(i=0;i<sel[key].options.length;i++)
						{
							if (sel[key].options[i].text == data[sel[key].name])
							{
								sel[key].options.selectedIndex = i;
								break;
							}
						}

						
					}
				}
			}
			show('block');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_office(id)
{
	new_office = false;
	office_id = id;
	var noffice_add_b = document.querySelector("#noffice_add_b");
	noffice_add_b.innerHTML = "Сохранить";
	console.log(id);
	
	var new_office_wrap = document.querySelector("#new_office_wrap");
	//var butt = document.querySelector("#noffice_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	
	var data = "block=staff&action=get_office&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'][id];
			console.log(data);
			
			var error = false;
			for (var key in inputs)
			{
				console.log(inputs[key].name);
				if (data.hasOwnProperty(inputs[key].name))
				{
					inputs[key].value = data[inputs[key].name];
				}
			}

			
			for (var key in sel)
			{
				var name = sel[key].name;
				if (name == 'type')
				{
					if (data.hasOwnProperty(sel[key].name))
					{
						for(i=0;i<sel[key].options.length;i++)
						{
							if (sel[key].options[i].text == data[sel[key].name])
							{
								sel[key].options.selectedIndex = i;
								break;
							}
						}

						
					}
				}
			}
			show_of('block');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function del_office(id)
{
	console.log(id);
	var cont4offices = document.querySelector("#cont4offices");
	var divs = cont4offices.querySelectorAll("div[class=row_odd], div[class=row_even]");
	var dd = divs[parseInt(id)-1].querySelectorAll("div[class=list_cell]");
	
	var st = dd[2].innerHTML;
	if (confirm("Вы точно хотите удалить запись об оффисе " + st + " (id=" + id + ")?"))
	{
		var data = "block=staff&action=del_office&id=" + id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert("Запись с id " + id + " удалена!");
				update_page();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function del_staff(id)
{
	console.log(id);
	var cont4staff = document.querySelector("#cont4staff");
	//var divs = cont4staff.querySelectorAll("div[class=row_odd], div[class=row_even]");
	//var dd = divs[parseInt(id)-1].querySelectorAll("div[class=list_cell]");
	var div = cont4staff.querySelector('#staff_row_' + id);
	var dd = div.querySelectorAll("div[class=list_cell]");
	
	var st = dd[5].innerHTML + " " + dd[6].innerHTML + " " + dd[7].innerHTML;
	if (confirm("Вы точно хотите удалить запись о сотруднике " + st + " (id=" + id + ")?"))
	{
		var data = "block=staff&action=del_worker&id=" + id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert("Запись с id " + id + " удалена!");
				update_data();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function add_wordker()
{
	new_worker = true;
	let new_worker_wrap = document.querySelector('#new_worker_wrap');
	let inputs = new_worker_wrap.querySelectorAll('input');

	for (let i=0; i<inputs.length; i++) {
		inputs[i].value = '';
	}

	let sel = new_worker_wrap.querySelectorAll('select');

	for (let i=0; i<sel.length; i++) {
		console.log(sel[i]);
		sel[i].selectedIndex = 0;
	}

	var nworker_add_b = document.querySelector("#nworker_add_b");
	nworker_add_b.innerHTML = "Добавить";
	show('block');
}

function add_office()
{
	console.log(1);
	new_office = true;
	var nworker_add_b = document.querySelector("#nworker_add_b");
	//nworker_add_b.innerHTML = "Добавить";
	show_of('block');
}

function nworker_add()
{
	var new_worker_wrap = document.querySelector("#new_worker_wrap");
	var butt = document.querySelector("#nworker_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	var error = false;
	for (var key in inputs)
	{
		var name = inputs[key].name;
		if ((name == 'login') || ((name == 'passwd') && new_worker) || (name == 'firstname') || (name == 'lastname'))
		{
			if(inputs[key].value == '')
			{
				error = true;
				inputs[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				inputs[key].setAttribute('style', 'border:none;');
		}
	}

	
	for (var key in sel)
	{
		var name = sel[key].name;
		if ((name == 'office') || (name == 'position'))
		{
			if(sel[key].options.selectedIndex == 0)
			{
				error = true;
				sel[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				sel[key].setAttribute('style', 'border:none;');
		}
	}
	
	if (!error)
	{
		var data = getQueryString(inputs);
		var nw = (new_worker) ? "add_worker" : ("edit_worker&id=" + worker_id);
		data += "&" + getQueryString(sel) + "&block=staff&action=" + nw;
		console.log(data);
		
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				butt.style.backgroundColor = 'lightgreen';
				console.log(Item['msg']);
				setTimeout(function(){update_data_after_add();}, timeout);
			}
			else 
			{
				butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function office_add()
{
	var new_office_wrap = document.querySelector("#new_office_wrap");
	var butt = document.querySelector("#noffice_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	var error = false;
	for (var key in inputs)
	{
		var name = inputs[key].name;
		if (name == 'name')
		{
			if(inputs[key].value == '')
			{
				error = true;
				inputs[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				inputs[key].setAttribute('style', 'border:none;');
		}
	}

	
	for (var key in sel)
	{
		var name = sel[key].name;
		if (name == 'type')
		{
			if(sel[key].options.selectedIndex == 0)
			{
				error = true;
				sel[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				sel[key].setAttribute('style', 'border:none;');
		}
	}
	
	if (!error)
	{
		var data = getQueryString(inputs);
		var nw = (new_office) ? "add_office" : ("edit_office&id=" + office_id);
		data += "&" + getQueryString(sel) + "&block=staff&action=" + nw;
		console.log(data);
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				butt.style.backgroundColor = 'lightgreen';
				console.log(Item['msg']);
				setTimeout(function(){update_page();}, timeout);
			}
			else 
			{
				butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function nworker_clear()
{
	var new_worker_wrap = document.querySelector("#new_worker_wrap");
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	
	var butt = document.querySelector("#nworker_add_b");
	butt.style.backgroundColor = '';
	
	for (var key in inputs)
	{
		inputs[key].value = "";
		var name = inputs[key].name;
		if ((name == 'login') || (name == 'passwd') || (name == 'firstname') || (name == 'lastname'))
			inputs[key].setAttribute('style', 'border:none;');
	}
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	for (var key in sel)
	{
		
		var name = sel[key].name;
		if ((name == 'office') || (name == 'position'))
		{
			sel[key].options.selectedIndex = 0;
			sel[key].setAttribute('style', 'border:none;');
		}
	}
}

function office_clear()
{
	var new_office_wrap = document.querySelector("#new_office_wrap");
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	
	var butt = document.querySelector("#noffice_add_b");
	butt.style.backgroundColor = '';
	
	for (var key in inputs)
	{
		inputs[key].value = "";
		var name = inputs[key].name;
		if (name == 'name')
			inputs[key].setAttribute('style', 'border:none;');
	}
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	for (var key in sel)
	{
		
		var name = sel[key].name;
		if (name == 'type')
		{
			sel[key].options.selectedIndex = 0;
			sel[key].setAttribute('style', 'border:none;');
		}
	}
}

function show_of(state)
{
	document.getElementById('window_of').style.display = state;			
	document.getElementById('wrap').style.display = state;
	
	
	if (state == 'none')
	{
		document.getElementById('window').style.display = state;
		document.getElementById('window_ip').style.display = state;		
		document.querySelector('#nworker_add_b').style.backgroundColor = '';
	}
}

function show_ip(state)
{
	document.getElementById('window_ip').style.display = state;			
	document.getElementById('wrap').style.display = state;
}

function show(state)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state;
	
	
	if (state == 'none')
	{
		document.getElementById('window_of').style.display = state;
		document.querySelector('#nworker_add_b').style.backgroundColor = '';
		document.getElementById('window_ip').style.display = state;		
	}
}
