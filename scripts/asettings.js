var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

window.onload = update_all_data;

var timeout = 1000;

var old_input_value = new Array();
var sec_input_value = new Array();

function update_all_data()
{
	get_tasks_count();
	get_events_count();
}

var maxposition = 174;
function ch_position(el, id)
{
	var divs = el.parentNode.parentNode.querySelectorAll('div[class="list_cell"]');
	console.log(divs);
	var inp = divs[8].querySelector('input').value;
	var position = el.innerHTML;
	console.log(el);
	console.log(id);
	var sel = document.createElement('select');
		sel.setAttribute('onchange', 'javascript:ch_pos(this, "' + inp + '", "' + position + '", ' + id + ');');
			
	for (var i = 1; i < maxposition; i++)
	{
		var opt = document.createElement('option');
			opt.value = i;
			opt.innerHTML = i;
			if (i == position)
				opt.setAttribute('selected', true);
		sel.appendChild(opt);
	}
	el.innerHTML = '';
	el.parentNode.appendChild(sel);
}

function ch_pos(el, name, oldp, id)
{
	if(!confirm("Утановить для поля " + name + " позицию " + el.value + "?"))
		return;
	
	var data = "block=asettings&action=ch_position&id=" + id + "&oldposition=" + oldp + "&newposition=" + el.value;
	console.log(data);
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'OK')
		{
			location.reload();
			//inp.style.backgroundColor = 'lightgreen';
			//document.querySelector("#sp_" + id).style.display = 'none';
			//sec_input_value[id] = false;
			//delete old_input_value[id];
		}
		else 
		{
			//inp.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		//setTimeout(function(){ch_color(inp, '');}, timeout);
		
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function focus_field(el)
{
	//console.log(el.id);
	var id = el.id.replace(/[^0-9]/gim,'');
	
	if (id in sec_input_value)
	{
		if (old_input_value[id] == el.value)
			document.querySelector("#sp_" + id).style.display = 'none';
	}
	else
	{
		sec_input_value[id] = true;
		old_input_value[id] = el.value;
	}
	
}

function blur_field(el)
{
	var id = el.id.replace(/[^0-9]/gim,'');
	//console.log("out");
	if (old_input_value[id] != el.value)
		document.querySelector("#sp_" + id).style.display = '';
	else
		document.querySelector("#sp_" + id).style.display = 'none';
}

function save_description(el)
{
	var id = el.id.replace(/[^0-9]/gim,'');
	var inp = document.querySelector("#desc_" + id);
	var old_color = window.getComputedStyle(inp).backgroundColor
	console.log(inp.value);
	var data = "block=asettings&action=save_description&id=" + id + "&val=" + inp.value;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			inp.style.backgroundColor = 'lightgreen';
			document.querySelector("#sp_" + id).style.display = 'none';
			sec_input_value[id] = false;
			delete old_input_value[id];
		}
		else 
		{
			inp.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(inp, '');}, timeout);
		
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show(state)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state;
}

function enable_field(block, en, id)
{
	var old_color = window.getComputedStyle(block.parentNode).backgroundColor	
	block.parentNode.classList.remove('animated');
	
	var act = block.name;
	
	var data = "block=asettings&action=" + act + "&id=" + id + "&en=" + en;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			block.parentNode.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			block.parentNode.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(block.parentNode, old_color);}, timeout);
		
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function change_type(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor	
	console.log(id + ":" + el.options[el.selectedIndex].value);
	
	if ((el.name == 'font_color') || (el.name == 'back_color'))
	{
		old_color = el.options[el.selectedIndex].value;
	}
	
	var data = "block=asettings&action=" + el.name + "&id=" + id + "&val=" + el.options[el.selectedIndex].value;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.style.backgroundColor = 'lightgreen';
			if ((el.options[el.selectedIndex].value == "select") || (el.options[el.selectedIndex].value == "multiselect") || (el.options[el.selectedIndex].value == "radio") || (el.options[el.selectedIndex].value == "kred_prod"))
				add_values(el,id, true);
			else
				add_values(el,id, false);
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
		
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_values(el, id, add)
{
	var pn = el.parentNode;
	
	if (add)
	{
		if (pn.querySelector('button'))
			return;
		var b = document.createElement('button');
		b.innerHTML = "Значения";
		b.id = "avalues_" + id;
		b.style = "margin-top:5px;";
		b.setAttribute("onclick", "javascript:add_val(" + id + ");");
		pn.appendChild(b);
	}
	else
	{
		var ttt;
		if (ttt = pn.querySelector("button"))
		{
			pn.removeChild(ttt);
		}
	}
	
}
function add_val(id)
{
	show('block');
	var data = "block=asettings&action=getvalues&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var div = document.querySelector('#values_wrap');
			var tmp_val = '<div id="values_cont">';
			
			if (Item['len'])
			{
				var data = Item['data'];
				for (var key in data)
				{
					tmp_val += '<div><input class="av_val_sel" type="text" value="' + data[key] + '"><img src="/img/delete.png" style="cursor:pointer;" onclick="javascript:del_val(this);"></div>';
				}
			}
			tmp_val += '</div><div><button onclick="javascript:add_value_field();">Добавить поле</button> <button onclick="javascript:save_values(' + id + ');">Сохранить</button></div>';
			div.innerHTML = tmp_val;
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_value_field()
{
	var div = document.querySelector('#values_cont');
	var d = document.createElement('div');
		d.setAttribute("style", "margin-bottom:5px;");
		
		var i = document.createElement('input');
			i.type="text";
			i.setAttribute("class", "av_val_sel");
		d.appendChild(i);
		
		var img = document.createElement('img');
			img.src = "/img/delete.png";
			img.style = "cursor:pointer;";
			img.setAttribute("onclick", "javascript:del_val(this);");
		d.appendChild(img);
			
	div.appendChild(d);
}

function del_val(el)
{
	el.parentNode.removeChild(el.previousSibling);
	el.parentNode.removeChild(el);
}

function save_values(id)
{
	var cont = document.querySelector('#values_cont');
	var inp = cont.querySelectorAll('input');
	/*for (var key in inp)
	{
		console.log("start new iteration: key=" + key);
		console.log(inp[key]);
		console.log(inp[key].value);
	}*/
	var data = "block=asettings&action=set_avalues&id=" + id;
	inp.forEach(function(item2, i, arr){
		//console.log("start new iteration: item=" + arr[i].value);
		data += "&key" + i + "=" + arr[i].value;
	});
	//alert('save_values');
	console.log(data);
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert("Сохранено успешно");
			//show('none');
			location.reload();
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

//window.onload = get_list;

function get_list()
{
	//tabl = document.querySelector('#res_table');
	//row = tabl.querySelectorAll('table');
	//console.log(row.length);
	var data = "block=list";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		//console.log(tmp_page);
		
		document.querySelector('#list_table').innerHTML = "";
		
		var Item = eval("obj = " + tmp_page);
		
		var row_odd = "row_odd";	//нечет
		var row_even = "row_even";	//чет
		var c = 0;
		for (var key in Item)
		{
			c++;
			//console.log(key + " ::: " + Item);
			var tmp_add = "";
			var row_c = (c & 1) ? row_odd : row_even;
			tmp_add += '<div class="' + row_c + '">';
			
			var row = Item[key];
			for (var key2 in row)
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
			
			tmp_add += '</div>';
			document.querySelector('#list_table').innerHTML += tmp_add;
		}
		
		//document.querySelector('#main');
	});
}