var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

//window.onload = list_main;
window.onload = update_all_data;

var sel_status_i = 0;
var sel_office_i = 0;
var sel_staff_i = 0;

var sort = 0;
var sort_by = false;

function update_all_data()
{
	list_main();
	get_tasks_count();
	get_events_count();
}

function dIncrease(i, ii)
{ // По возрастанию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function dDecrease(i, ii)
{ // По убыванию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function sIncrease(i, ii)
{ // По возрастанию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function sDecrease(i, ii)
{ // По убыванию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function filterApply()
{
	var t = document.querySelector('#list_table');
	var r1 = t.querySelectorAll('.row_odd');
	for (var key = 0; key < r1.length; key++)
		t.removeChild(r1[key]);
	var r2 = t.querySelectorAll('.row_even');
	for (var key = 0; key < r2.length; key++)
		t.removeChild(r2[key]);
	
	var dfrom = document.querySelector('#dfrom').value;
	var dto = document.querySelector('#dto').value;
	
	var filter = "&dfrom=" + dfrom + "&dto=" + dto;
	
	get_list(filter);
}

function ch_list_stat(el)
{
	var sel = el.options[el.selectedIndex].value;
	var sel2 = document.querySelector('#select_staff');
	
	var opt = sel2.querySelectorAll('option');
	if (sel == '-1')
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].style.display = '';
			opt[key].selected = false;
		}
	}
	else
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].selected = false;
			if (opt[key].className == ('office_' + sel))
				opt[key].style.display = '';
			else
				opt[key].style.display = 'none';
		}
	}
	filterApply();
}

function list_main()
{
	filterApply();
}


function get_list(filter)
{
	//tabl = document.querySelector('#res_table');
	//row = tabl.querySelectorAll('table');
	//console.log(row.length);

	var data = "block=mstat&action=get_list" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item["status"] == 'OK')
		{
			var data = Item["data"];
			add_data(data);
		}
	});
}

function add_data(mass)
{
	var row_odd = "row_odd";	//нечет
	var row_even = "row_even";	//чет
	var c = 0;
	var tmp_add = "";
	for (var key in mass)
	{
		
		if (key == '')
			continue;
		c++;
		//console.log(key + " ::: " + Item);
		
		var row_c = (c & 1) ? row_odd : row_even;
		tmp_add += '<div class="' + row_c + '">';
		
		var row = mass[key];
		
		tmp_add += '<div class="list_cell">' + key + '</div>';
		tmp_add += '<div class="list_cell">' + row['fio'] + '</div>';
		var sum_a = "" + row['sum_a'];
		sum_a = sum_a.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		var sum = "" + row['sum'];
		sum = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		tmp_add += '<div class="list_cell">' + sum_a + ' р. </div>';
		tmp_add += '<div class="list_cell">' + sum + ' р. </div>';
		var conv = 100 * parseInt(row['sum_a']) / parseInt(row['sum']);
		tmp_add += '<div class="list_cell">' + conv.toFixed(2) + '</div>';
		tmp_add += '<div class="list_cell">' + row['contr_s'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['count'] + '</div>';
		conv = 100 * parseInt(row['contr_s']) / parseInt(row['count']);
		tmp_add += '<div class="list_cell">' + conv.toFixed(2) + '</div>';
		
		
		tmp_add += '</div>';
		
	}
	
	document.querySelector('#list_table').innerHTML += tmp_add;

}


//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds