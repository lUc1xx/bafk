var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;


var point_list = new Array();
window.onload = update_all_data;

function list_main() {
	get_list('');
}

function update_all_data()
{
	list_main();
	get_tasks_count();
	get_events_count();
}


function fio_filter(filter)
{
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[2].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
}

function get_list(filter) {
	var data = "block=bankrequest2&action=get_list" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['point_list'])
				point_list = Item['point_list'];
			if (Item['len'] > 0)
			{
				add_data(Item['data']);
			}
		}
		
	});
}

function ch_breq_status(id, el) {
	var data = "block=bankrequest2&action=ch_breq_status&req_id=" + id + "&new_st=" + el.value;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		
	});
}


function ch_bans_status(id, el) {
	
	var val = 0;
	if(el.value == 'reject')
	{
		var ddd = el.parentNode.parentNode.querySelectorAll('div[class="list_cell"]');
		val = ddd[4].innerHTML.replace(/\D+/g,"");
		el.parentNode.parentNode.querySelector('input[name="sum_val_ans"]').value = val;
	}
	else
	{
		val = el.parentNode.parentNode.querySelector('input[name="sum_val_ans"]').value;
		if (val == '')
		{
			alert('Введите сумму ответа!');
			el.options[0].selected = true;
			return;
		}
		
		if (el.value == "")
			return;
	}
	
	var data = "block=bankrequest2&action=ch_bans_status&bans=" + el.value + "&req_id=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		
	});
}

function ch_point(el, id)
{
	console.log(el);
	var data = "block=bankrequest2&action=ch_point&val=" + el.options[el.selectedIndex].value + "&req_id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else
			alert(Item['msg']);
		
	});
}

function ch_analyst(el, id)
{
	console.log(el);
	var val = el.checked ? 1 : 0
	var data = "block=bankrequest2&action=ch_analyst&val=" + val + "&req_id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else
			alert(Item['msg']);
		
	});
}


function add_data(mass) {
	var row_odd = "row_odd";	//нечет
	var row_even = "row_even";	//чет
	var c = 0;
	var tmp_add = "";
	for (var key in mass)
	{
		c++;
		//console.log(key + " ::: " + Item);
		var row = mass[key];
		
		var row_c = (c & 1) ? row_odd : row_even;
		tmp_add += '<div class="' + row_c + '">';
		
		tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['date_send'] + '</a></div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['form_id'] + '</a></div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['ln'] + ' ' + row['fn'] + ' ' + row['mn'] + '</a></div>';
		tmp_add += '<div class="list_cell">' + row['bankname'] + '</div>';
		
		tmp_add += '<div class="list_cell">';
		tmp_add += '<select onchange="javascript:ch_point(this, ' + parseInt(row['bid']) + ');"><option value="">Выберите</option>';
		for(var p in point_list)
		{
			var ps = (point_list[p]['id'] == row['point']) ? " selected" : "";
			tmp_add += '<option value="' + point_list[p]['id'] + '"' + ps + '>' + point_list[p]['name'] + '</option>';
		}
		tmp_add += '</select><br>';
		var inpc = '';
		if (row['analyst'])
			inpc = (parseInt(row['analyst'])) ? " checked" : "";
		tmp_add += '<label><input value="1" type="checkbox"' + inpc + ' onclick="javascript:ch_analyst(this, ' + parseInt(row['bid']) + ');"> Аналитик</label>';
		tmp_add += '</div>';
		
		var ssum = row['sum_send'] + " р.";
		tmp_add += '<div class="list_cell">' + ssum + '</div>';
		
		var st = new Array();
		st[0] = (row['st'] == 'ready') ? ' selected' : '';
		st[1] = (row['st'] == 'sent') ? ' selected' : '';
		//st[2] = (row['st'] == 'get_answer') ? ' selected' : '';
		
		tmp_add += '<div class="list_cell">';
		if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))
		{
			switch(row['st'])
			{
				case "get_answer" : tmp_add += 'Получен ответ'; break;
				case "ready" : tmp_add += 'Ждет отправления'; break;
				case "sent" : tmp_add += 'Отправлена в банк'; break;
				//case "ready" : tmp_add += ''; break;
			}
			
		}
		else if (row['st'] == 'get_answer')
		{
			tmp_add += 'Получен ответ';
		}
		else
		{
			tmp_add += '<select onchange="javasript:ch_breq_status(' + parseInt(row['bid']) + ', this);">';
			tmp_add += '<option value="ready"' + st[0] + '>Ждет отправления</option>';
			tmp_add += '<option value="sent"' + st[1] + '>Отправлена в банк</option>';
			//tmp_add += '<option value="get_answer"' + st[2] + '>Получен ответ</option>';
			tmp_add += '</select>';
		}
		
		tmp_add += '</div><div class="list_cell">';
		var dis = (row['st'] == 'sent') ? '' : ' disabled';
		
		
		if (row['bank_answer'] != null)
		{
			var ba = (row['bank_answer'] == 'approve') ? '<font color="green">Одобрено</font>' : ((row['bank_answer'] == 'reject') ? '<font color="red">Отклонил</font>' : '');
			tmp_add += ba;
		}
		else if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))
		{
			if (row['st'] == 'sent')
				tmp_add += 'Ожидается ответ банка';
			else
				tmp_add += '-';
			
		}
		else
		{
			
			tmp_add += '<select' + dis + ' onchange="javasript:ch_bans_status(' + parseInt(row['bid']) + ', this);">';
			tmp_add += '<option value="">Выберите</option>';
			tmp_add += '<option value="approve">Банк одобрил</option>';
			tmp_add += '<option value="reject">Банк отклонил</option>';
			tmp_add += '</select>';
		}
		
		var as = "";
		if (row['sum_answer'] != null)
			as = row['sum_answer'];
		
		if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))		
			tmp_add += '</div><div class="list_cell">' + as + '</div>';
		else
			tmp_add += '</div><div class="list_cell"><input name="sum_val_ans" type="text" value="' + as + '"' + dis + '></div>';
		
		tmp_add += '<div class="list_cell">' + '<input type="text" size="4">' + '</div>';
		tmp_add += '<div class="list_cell">' + '<input type="text" size="4">' + '</div>';
		tmp_add += '<div class="list_cell">' + '<input type="text" size="4">' + '</div>';
		tmp_add += '<div class="list_cell">' + '<input type="text" size="4">' + '</div>';
		
		
		//var ssum = row[key2] + ((row['sum_val'] == "rub") ? " р." : "");
		//tmp_add += '<div class="list_cell">' + ssum + '</div>';
		
		
		/*for (var key2 in row)
		{
			if ((key2 == 'id') || (key2 == 'firstname') || (key2 == 'middlename') || (key2 == 'sum_val') || (key2 == 'status') || (key2 == 'office') || (key2 == 'manager') || (key2 == 'source_param'))
				continue;
			
			if (key2 == 'debug')
			{
				console.log('debug:' + row[key2]);
				continue;
			}
			
			if ((key2 == 'meet_event') || (key2 == 'call_event'))
				continue;
			
			if (key2 == 'lastname')
			{
				tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row[key2] + ' ' + row['firstname'] + ' ' + row['middlename'] + '</a></div>';
			}
			else if (key2 == 'source')
			{
				//tmp_add += '<div class="list_cell">' + row[key2] + ':' + row['source_param'] + '</div>';
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
			}
			else if (key2 == 'sum')
			{
				var ssum = row[key2] + ((row['sum_val'] == "rub") ? " р." : "");
				tmp_add += '<div class="list_cell">' + ssum + '</div>';
			}
			else
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
		}
		
		var ev = '';
		if ('meet_event' in row) 
		{
			if (row['meet_event'])
				ev += row['meet_event'];
		}
		
		if ('call_event' in row)
		{
			if (row['call_event'])
			{
				if (ev != '')
					ev += ', ';
				ev += row['call_event'];
			}
		}
		
		tmp_add += '<div class="list_cell">' + ev + '</div></div>';
		*/
		
		tmp_add += '</div>';
	}
	
	document.querySelector('#list_table').innerHTML += tmp_add;
	
			
	
}



//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds