var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;
window.onload = update_all_data;

var events = [
    {title: 'Business Lunch', start: '2018-06-13T13:00:00'},
    {title: 'Meeting', start: '2018-06-13T11:00:00'},
    {title: 'Conference', start: '2018-06-18', end: '2017-12-20'},
];

function update_all_data()
{
	get_events_count();
	get_tasks_count();
	
	window.quasarConfig = {
    brand: { // this will NOT work on IE 11
      primary: '#e46262',
      // ... or all other brand colors
    },
    notify: {}, // default set of options for Notify Quasar plugin
    loading: {}, // default set of options for Loading Quasar plugin
    loadingBar: {}, // settings for LoadingBar Quasar plugin
    // ..and many more
  }
	Quasar.lang.set(Quasar.lang.ru);
	new Vue({
        el: '#mapp',
        data: function () {
          return {
			loading: false,
			date_from: '2020/01/01',
			//date_to: '2020/03/01',
			date_to: Quasar.utils.date.formatDate(Date.now(), 'YYYY/MM/DD'),
			pagination: {
				sortBy: 'sum',
				descending: true,
				rowsPerPage: 50
			},
			columns: [
			{
			  name: 'track',
			  required: true,
			  label: 'UTM Source',
			  align: 'left',
			  field: row => row.track,
			  format: val => `${val}`,
			  style: 'width: 100px;max-width: 100px;',
			  headerStyle: 'width: 100px; max-width: 100px;',
			  sortable: true
			},
			{ name: 'num', style: 'width: 100px;max-width: 100px;',
			  headerStyle: 'width: 100px; max-width: 100px;',
			  align: 'center', label: 'Всего заявок', field: 'num', sortable: true },
			{
				name: 'second',
				style: 'width: 100px;max-width: 100px;',
				headerStyle: 'width: 100px; max-width: 100px;',
			  	label: "Вторичная обработка", field: 'second', sortable: true
			},
			{
				name: 'cm_24',
				style: 'width: 100px; max-width: 100px;',
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Закрыто по "Иной регион"',
				field: 'cm_24',
				sortable: true,
				sort: (a, b) => parseInt(a, 10) - parseInt(b, 10)
			},
			{
				name: 'dogovor',
				style: 'width: 100px;max-width: 100px;',
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Заключен договор', field: 'dogovor', sortable: true },
			{
				name: 'kredit',
				style: 'width: 100px;max-width: 100px;',
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Кредит выдан', field: 'kredit', sortable: true },
			{
				name: 'sum',
				style: 'width: 100px;max-width: 100px;',
				headerStyle: 'width: 100px; max-width: 100px;',
				label: 'Сумма выданных кредитов', field: 'sum', sortable: true },
			{ name: 'comm', 	style: 'width: 100px; max-width: 100px;',
								headerStyle: 'width: 100px; max-width: 100px;',label: 'Сумма полученной комиссии', field: 'comm', sortable: true, sort: (a, b) => parseInt(a, 10) - parseInt(b, 10) },
			{ name: 'k1', 		style: 'width: 100px; max-width: 100px;',
								headerStyle: 'width: 100px; max-width: 100px;', label: 'Конверсия из "всего заявок" во "вторичная"', field: 'k1', sortable: true, sort: (a, b) => parseInt(a, 10) - parseInt(b, 10) },
			{ name: 'k2', 	style: 'width: 100px; max-width: 100px;',
								headerStyle: 'width: 100px; max-width: 100px;',
								label: 'Конверсия из "всего заявок" в "договор заключен"', field: 'k2', sortable: true, sort: (a, b) => parseInt(a, 10) - parseInt(b, 10) },
			{ name: 'k3', 	style: 'width: 100px; max-width: 100px;',
								headerStyle: 'width: 100px; max-width: 100px;', label: 'Конверсия из "всего заявок" в "кредит выдан"', field: 'k3', sortable: true, sort: (a, b) => parseInt(a, 10) - parseInt(b, 10) },

		  ],
		  data: [],
		  dat: [
			{
			  name: 'Frozen Yogurt',
			  calories: 159,
			  fat: 6.0,
			  carbs: 24,
			  protein: 4.0,
			  sodium: 87,
			  calcium: '14%',
			  iron: '1%'
			},
			{
			  name: 'Ice cream sandwich',
			  calories: 237,
			  fat: 9.0,
			  carbs: 37,
			  protein: 4.3,
			  sodium: 129,
			  calcium: '8%',
			  iron: '1%'
			},
			{
			  name: 'Eclair',
			  calories: 262,
			  fat: 16.0,
			  carbs: 23,
			  protein: 6.0,
			  sodium: 337,
			  calcium: '6%',
			  iron: '7%'
			},
			{
			  name: 'Cupcake',
			  calories: 305,
			  fat: 3.7,
			  carbs: 67,
			  protein: 4.3,
			  sodium: 413,
			  calcium: '3%',
			  iron: '8%'
			},
			{
			  name: 'Gingerbread',
			  calories: 356,
			  fat: 16.0,
			  carbs: 49,
			  protein: 3.9,
			  sodium: 327,
			  calcium: '7%',
			  iron: '16%'
			},
			{
			  name: 'Jelly bean',
			  calories: 375,
			  fat: 0.0,
			  carbs: 94,
			  protein: 0.0,
			  sodium: 50,
			  calcium: '0%',
			  iron: '0%'
			},
			{
			  name: 'Lollipop',
			  calories: 392,
			  fat: 0.2,
			  carbs: 98,
			  protein: 0,
			  sodium: 38,
			  calcium: '0%',
			  iron: '2%'
			},
			{
			  name: 'Honeycomb',
			  calories: 408,
			  fat: 3.2,
			  carbs: 87,
			  protein: 6.5,
			  sodium: 562,
			  calcium: '0%',
			  iron: '45%'
			},
			{
			  name: 'Donut',
			  calories: 452,
			  fat: 25.0,
			  carbs: 51,
			  protein: 4.9,
			  sodium: 326,
			  calcium: '2%',
			  iron: '22%'
			},
			{
			  name: 'KitKat',
			  calories: 518,
			  fat: 26.0,
			  carbs: 65,
			  protein: 7,
			  sodium: 54,
			  calcium: '12%',
			  iron: '6%'
			}
		  ]
			}
        },
        methods: {
			/*mounted() {
				let timeStamp = Date.now();
				this.date_to = date.formatDate(timeStamp, 'YYYY/MM/DD');
				console.log(this.date_to);
			},*/
			loaddata () {
				this.loading = true;
				let url = `/ajax.php?block=metriks&action=getmetriks&df=${this.date_from}&dto=${this.date_to}`;
				//let response = await fetch(url);
				//alert(response);
				fetch(url)
				.then(response => response.json())
				.then(data => {
					//console.log(data)
					if(data.status == 'ok')
						this.setdata(data.data);
					this.loading = false;
				});
			},
			setdata(d) {
				console.log('update data');
				this.data = d;
				console.log(d);
			}
		},

        // ...etc
      })
}









//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds