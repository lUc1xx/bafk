var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

window.onload = update_head_data;

var list_algo = new Array();
var list_sms_templ = new Array();
var staff_mass_endcode = new Array();
var staff_mass_endcode_length = 0;
function show_set_algo()
{
	show_set('block');
	var set_wrap_b = document.querySelector('#set_wrap_b');
	set_wrap_b.style = '';
	set_wrap_b.innerHTML = '<button id="sb0" onclick="javascript:listalgo();">Список алгоритмов</button> <button id="sb1" onclick="javascript:make_new_algo();">Создать алгоритм</button>'
	var data = "&block=settings&action=get_algo_set";
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			list_algo.length = 0;
			list_algo = Item['block_list'];
			listalgo();
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show_set_algo_urls()
{
	show_set('block');
	var set_wrap_b = document.querySelector('#set_wrap_b');
	set_wrap_b.style = '';
	set_wrap_b.innerHTML = '<button onclick="javascript:make_new_url();">Новая ссылка</button>';
	var data = "&block=settings&action=get_algo_url";

	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.style = '';
	var tmp_str = '<table>';

	ajaxQuery('/ajax.php','POST', data, true, function(req)
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			tmp_str += '<tr>';
			tmp_str += '<td>Название</td>';
			tmp_str += '<td>Cсылка</td>';
			tmp_str += '<td>ТМ</td>';
			tmp_str += '<td>Рук ТМ</td>';
			tmp_str += '<td>ОЗС</td>';
			tmp_str += '<td>Рук ОЗС</td>';
			tmp_str += '<td>Руководство</td>';
			tmp_str += '<td>Cохранить</td>';
			tmp_str += '<td>Удалить</td>';
			tmp_str += '</tr>';



			for(let key in Item['list']) {
				let row = Item['list'][key];
				let tm = row['tm'] ? ' checked' : '';
				let dtm = row['dirTm'] ? ' checked' : '';
				let ozs = row['ozs'] ? ' checked' : '';
				let dozs = row['dirOzs'] ? ' checked' : '';
				let admin = row['main'] ? ' checked' : '';
				tmp_str += '<tr name="' + key + '">';
				tmp_str += '<td><input name="name" placeholder="Максимум 30 символов" value="' + row['label'] + '"></td>';
				tmp_str += '<td><input name="url" value="' + row['url'] + '" size="60"></td>';
				tmp_str += '<td align="center"><input name="tm" type="checkbox"' + tm + '></td>';
				tmp_str += '<td align="center"><input name="dtm" type="checkbox"' + dtm + '></td>';
				tmp_str += '<td align="center"><input name="ozs" type="checkbox"' + ozs + '></td>';
				tmp_str += '<td align="center"><input name="dozs" type="checkbox"' + dozs + '></td>';
				tmp_str += '<td align="center"><input name="admin" type="checkbox"' + admin + '></td>';
				tmp_str += '<td align="center"><span style="cursor:pointer" onclick="saveUrl(this);"><img src="/img/save.png" height="16px"></span></td>';
				tmp_str += '<td align="center"><span style="cursor:pointer" onclick="delUrl(this);"><img src="/img/delete.png"></span></td>';
				tmp_str += '</tr>';
			}
			tmp_str += '</table>';
			set_wrap.innerHTML = tmp_str;
		}
		else
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function make_new_url() {

	var set_wrap = document.querySelector('#set_wrap table');
	let tmp_str = '<tr name="new">';
	tmp_str += '<td><input name="name" placeholder="Максимум 30 символов"></td>';
	tmp_str += '<td><input name="url" size="60"></td>';
	tmp_str += '<td align="center"><input name="tm" type="checkbox"></td>';
	tmp_str += '<td align="center"><input name="dtm" type="checkbox"></td>';
	tmp_str += '<td align="center"><input name="ozs" type="checkbox"></td>';
	tmp_str += '<td align="center"><input name="dozs" type="checkbox"></td>';
	tmp_str += '<td align="center"><input name="admin" type="checkbox"></td>';
	tmp_str += '<td align="center"><span style="cursor:pointer" onclick="saveUrl(this);"><img src="/img/save.png" height="16px"></span></td>';
	tmp_str += '<td align="center"><span style="cursor:pointer" onclick="delUrl(this);"><img src="/img/delete.png"></span></td>';
	tmp_str += '</tr>';

	set_wrap.innerHTML += tmp_str;
}

function saveUrl(el) {
	let tr = el.parentNode.parentNode;

	let id = tr.getAttribute('name');
	let inp = tr.querySelectorAll('input');
	let data = '&block=settings&action=saveurl&id=' + id + '&' + encodeURI(getQueryString(inp));
	ajaxQuery('/ajax.php','POST', data, true, function(req)
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Сохранено');
			}
			else
			{
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});

}
function delUrl(el) {
	el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
}

function show_set_filetypes()
{
	var set_wrap_b = document.querySelector('#set_wrap_b');
	set_wrap_b.innerHTML = '';
	set_wrap_b.style = '';
		
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.style = '';
	set_wrap.innerHTML = '';
	//set_wrap_b.innerHTML = '<button id="sb0" onclick="javascript:listalgo();">Список алгоритмов</button> <button id="sb1" onclick="javascript:make_new_algo();">Создать алгоритм</button>'
	var data = "&block=settings&action=get_list_filetypes";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//console.log(Item['data']);
			show_set('block');
			let table = document.createElement('table');
				let tr = document.createElement('tr');
					let td = document.createElement('td');
					td.innerHTML = '';
					td.width = '25px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.innerHTML = '<input style="width:400px;" type="text" placeHolder="Описание" id="new_ftype_text">';
					td.width = '425px';
					tr.appendChild(td);
					
					
					td = document.createElement('td');
					td.innerHTML = '';
					td.width = '20px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.innerHTML = '<button onclick="javascript:add_ftype();">Добавить</button>';
					td.width = '125px';
					tr.appendChild(td);
				table.appendChild(tr);
			set_wrap_b.appendChild(table);
	
			table = document.createElement('table');
			table.id = 'code_table';
			tr = document.createElement('tr');
				td = document.createElement('td');
				td.innerHTML = '№';
				td.width = '25px';
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'Описание';
				td.width = '425px';
				tr.appendChild(td);

				td = document.createElement('td');
				td.innerHTML = '';
				td.width = '20px';
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'Удалить';
				td.width = '20px';
				tr.appendChild(td);
			table.appendChild(tr);
			
			if (Item['data'])
			{
				console.log(Item['data']);
				
				//for (let i=0; i<Item['data'].length; i++)
				for (let str of Item['data'])
				{
		//			let str = Item['data'][i];
					tr = document.createElement('tr');
						td = document.createElement('td');
						td.innerHTML = str['id'];
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '<input style="width:400px;" type="text" value="' + str['description'] + '" oninput="javascript:edit_ftype(this, ' + str['id'] + ');">';
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '';
						td.width = '20px';
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '<button onclick="javascript:del_ftype(this, ' + str['id'] + ');">Удалить</button>';
						td.width = '50px';
						tr.appendChild(td);
					table.appendChild(tr);
				}
			}
			
			set_wrap_b.appendChild(table);
			
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function del_ftype(el, id)
{
	var data = "&block=settings&action=del_ftype&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
		}
		else 
		{
			
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_ftype(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor;
	//var text_ = el.parentNode.parentNode.querySelector('input').value;
	var text_ = el.value;
	var data = "&block=settings&action=edit_ftype&id=" + id + "&val=" + text_;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_ftype()
{
	var text_ = document.querySelector('#new_ftype_text').value;
	var data = "&block=settings&action=add_ftype&val=" + text_;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_set_filetypes();
		}
		else 
		{
			
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show_set_endcodes()
{
	
	var set_wrap_b = document.querySelector('#set_wrap_b');
	set_wrap_b.innerHTML = '';
	set_wrap_b.style = '';
		
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.style = '';
	set_wrap.innerHTML = '';
	//set_wrap_b.innerHTML = '<button id="sb0" onclick="javascript:listalgo();">Список алгоритмов</button> <button id="sb1" onclick="javascript:make_new_algo();">Создать алгоритм</button>'
	var data = "&block=settings&action=get_list_endcodes";
	
	
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var list_of = Item['list_of'];
			var list_man = Item['list_man'];
			list_sms_templ = Item['list_sms_templ'];
			var ml = list_man.length;
			
			for (var i in list_of)
			{
				//staff_mass_endcode.push(Array(list_of[i]['id'], 'of', 0, list_of[i]['name']));
				let man_in_of_mass = Array();
				for (var k=0; k<ml; k++)
				{
					let st = list_man[k];
					if (st === undefined)
						continue;
					//console.log(st);
					if ( parseInt(st['office']) == parseInt(list_of[i]['id']) )
					{
						//staff_mass_endcode.push(Array(st['id'], 'man', st['office'], st['lastname']));
						man_in_of_mass[st['id']] = st['lastname'];
						//list_man.splice(k, 1);
					}
				}
				staff_mass_endcode[list_of[i]['id']] = Array(list_of[i]['name'], man_in_of_mass);
			}
			
			console.log('===============staff_mass_endcode================');
			console.log(staff_mass_endcode);
			console.log('===============staff_mass_endcode================');
			staff_mass_endcode_length = staff_mass_endcode.length;
			
			let table = document.createElement('table');
				let tr = document.createElement('tr');
					let td = document.createElement('td');
					td.innerHTML = '';
					td.width = '25px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.innerHTML = '<input style="width:400px;" type="text" placeHolder="Описание" id="new_code_text">';
					td.width = '425px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					td = document.createElement('td');
					td.innerHTML = '<input type="checkbox">';
					td.width = '45px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					let select_text = document.createElement('select');
					select_text.id = "new_code_send";
						let opt = document.createElement('option');
						opt.innerHTML = 'Не перенаправлять';
						opt.value = "-1";
						select_text.appendChild(opt);
						
						//for (k=0; k<staff_mass_endcode_length; k++)
						for (var k in staff_mass_endcode)
						{
							let optg = document.createElement('optgroup');
							console.log(staff_mass_endcode[k]);
							optg.label = staff_mass_endcode[k][0];
								opt = document.createElement('option');
								opt.innerHTML = 'Отправить в ' + staff_mass_endcode[k][0];
								opt.style = "font-style:italic;"
								opt.value = "of_" + k;
								optg.appendChild(opt);
								
								let sm = staff_mass_endcode[k][1];
								for (var sk in sm)
								{
									opt = document.createElement('option');
									opt.innerHTML = sm[sk];
									opt.value = "man_" + sk;
									optg.appendChild(opt);
								}
							
							select_text.appendChild(optg);
						}

						let optg = document.createElement('optgroup');
							optg.label = 'Сотрудничество'
							opt = document.createElement('option');
							opt.innerHTML = 'Отправить в Сотрудничество';
							opt.style = "font-style:italic;"
							opt.value = "sotrud";
							optg.appendChild(opt);
						select_text.appendChild(optg);



					td.appendChild(select_text);
					td.width = '225px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					select_text = document.createElement('select');
					select_text.id = "new_sms_send";
						opt = document.createElement('option');
						opt.innerHTML = 'Не отправлять';
						opt.value = "-1";
						select_text.appendChild(opt);
						
						for (var k in list_sms_templ)
						{
							let sms = list_sms_templ[k];
							opt = document.createElement('option');
							opt.innerHTML = sms['name'];
							opt.value = sms['id'];
							select_text.appendChild(opt);
						}
					td.appendChild(select_text);
					td.width = '225px';
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.innerHTML = '<button onclick="javascript:add_code();">Добавить</button>';
					td.width = '125px';
					tr.appendChild(td);
				table.appendChild(tr)
			set_wrap_b.appendChild(table);
	
			show_set('block');
			table = document.createElement('table');
			table.id = 'code_table';
			tr = document.createElement('tr');
				td = document.createElement('td');
				td.innerHTML = '№';
				td.setAttribute('rowspan', '2');
				td.width = '25px';
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'Описание';
				td.width = '425px';
				td.setAttribute('rowspan', '2');
				tr.appendChild(td);

				td = document.createElement('td');
				td.innerHTML = 'Кто закрыл';
				td.width = '20px';
				td.setAttribute('colspan', '6');
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'Отправить при завершении';
				td.width = '225px';
				td.setAttribute('rowspan', '2');
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'СМС при завершении';
				td.width = '225px';
				td.setAttribute('rowspan', '2');
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = 'Удалить';
				td.width = '125px';
				td.setAttribute('rowspan', '2');
				tr.appendChild(td);
			table.appendChild(tr);
			
			tr = document.createElement('tr');
				td = document.createElement('td');
				td.innerHTML = 'м.ТМ';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = 'д.ТМ';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = 'м.ОЗС';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = 'д.ОЗС';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = 'Бр';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = 'Рук';
				td.width = '45px';
				tr.appendChild(td);
			table.appendChild(tr);
			if (Item['data'])
			{
				console.log(Item['data']);
				
				for (let i=0; i<Item['data'].length; i++)
				{
					
					let str = Item['data'][i];
					console.log(str);
					let tr = document.createElement('tr');
						let td = document.createElement('td');
						td.innerHTML = str['id'];
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '<input style="width:400px;" type="text" value="' + str['description'] + '" oninput="javascript:edit_code(this, ' + str['id'] + ');">';
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input type="checkbox">';
						td.width = '45px';
						tr.appendChild(td);
						
						
						td = document.createElement('td');
						let select_text = document.createElement('select');
						select_text.setAttribute('onchange', 'javascript:edit_code_send(this, ' + str['id'] + ');');
							let opt = document.createElement('option');
							opt.innerHTML = 'Не перенаправлять';
							opt.value = "-1";
							select_text.appendChild(opt);
							
							//for (k=0; k<staff_mass_endcode_length; k++)
							for (var k in staff_mass_endcode)
							{
								let optg = document.createElement('optgroup');
								console.log(staff_mass_endcode[k]);
								optg.label = staff_mass_endcode[k][0];
									opt = document.createElement('option');
									opt.innerHTML = 'Отправить в ' + staff_mass_endcode[k][0];
									if (str['send_to'] == ("of_" + k))
										opt.selected = true;
									opt.style = "font-style:italic;"
									opt.value = "of_" + k;
									optg.appendChild(opt);
									
									let sm = staff_mass_endcode[k][1];
									for (var sk in sm)
									{
										opt = document.createElement('option');
										opt.innerHTML = sm[sk];
										opt.value = "man_" + sk;
										if (str['send_to'] == ("man_" + sk))
											opt.selected = true;
										optg.appendChild(opt);
									}
								
								select_text.appendChild(optg);
							}
							let optg = document.createElement('optgroup');
							optg.label = 'Сотрудничество'
							opt = document.createElement('option');
							opt.innerHTML = 'Отправить в Сотрудничество';
							if (str['send_to'] == 'sotrud')
								opt.selected = true;
							opt.style = "font-style:italic;"
							opt.value = "sotrud";
							optg.appendChild(opt);
							select_text.appendChild(optg);
						td.appendChild(select_text);
						td.width = '225px';
						tr.appendChild(td);
						
						td = document.createElement('td');
						select_text = document.createElement('select');
						select_text.setAttribute('onchange', 'javascript:edit_sms_send(this, ' + str['id'] + ');');
							opt = document.createElement('option');
							opt.innerHTML = 'Не отправлять';
							opt.value = "-1";
							select_text.appendChild(opt);
							
							for (var k in list_sms_templ)
							{
								let sms = list_sms_templ[k];
								opt = document.createElement('option');
								opt.innerHTML = sms['name'];
								opt.value = sms['id'];
								if (str['send_sms'] == sms['id'])
									opt.selected = true;
								select_text.appendChild(opt);
							}
							
						td.appendChild(select_text);
						td.width = '225px';
						
						tr.appendChild(td);
						
						td = document.createElement('td');
						td.innerHTML = '<button onclick="javascript:del_code(this, ' + str['id'] + ');">Удалить</button>';
						tr.appendChild(td);
					table.appendChild(tr);
				}
				
			}
			set_wrap.appendChild(table);
			//list_algo.length = 0;
			//list_algo = Item['block_list'];
			//listalgo();
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_code()
{
	var text_ = document.querySelector('#new_code_text').value;
	
	var new_code_send = document.querySelector('#new_code_send');
	var new_code_send_s = new_code_send.selectedIndex;
	new_code_send = new_code_send.options[new_code_send.selectedIndex].value;
	
	var new_sms_send = document.querySelector('#new_sms_send');
	var new_sms_send_s = new_sms_send.selectedIndex;
	new_sms_send = new_sms_send.options[new_sms_send.selectedIndex].value;
	
	var data = "&block=settings&action=add_endcode&val=" + text_ + "&send=" + new_code_send + "&smsto=" + new_sms_send;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			let tr = document.createElement('tr');
				let td = document.createElement('td');
				td.innerHTML = Item['id'];
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = '<input style="width:400px;" type="text" value="' + Item['description'] + '" oninput="javascript:edit_code(this, ' + Item['id'] + ');">';
				tr.appendChild(td);
				
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				td = document.createElement('td');
				td.innerHTML = '<input type="checkbox">';
				td.width = '45px';
				tr.appendChild(td);
				
				td = document.createElement('td');
				let select_text = document.createElement('select');
				select_text.setAttribute('onchange', 'javascript:edit_code_send(this, ' + Item['id'] + ');');
					let opt = document.createElement('option');
					opt.innerHTML = 'Не перенаправлять';
					opt.value = "-1";
					select_text.appendChild(opt);
					
					//for (k=0; k<staff_mass_endcode_length; k++)
					for (var k in staff_mass_endcode)
					{
						let optg = document.createElement('optgroup');
						console.log(staff_mass_endcode[k]);
						optg.label = staff_mass_endcode[k][0];
							opt = document.createElement('option');
							opt.innerHTML = 'Отправить в ' + staff_mass_endcode[k][0];
							opt.value = "of_" + k;
							if (new_code_send == "of_" + k)
								opt.selected = true;
							opt.style = "font-style:italic;"
							optg.appendChild(opt);
							
							let sm = staff_mass_endcode[k][1];
							for (var sk in sm)
							{
								opt = document.createElement('option');
								opt.innerHTML = sm[sk];
								opt.value = "man_" + sk;
								if (new_code_send == "man_" + sk)
									opt.selected = true;
								optg.appendChild(opt);
							}
						
						select_text.appendChild(optg);
					}
					let optg = document.createElement('optgroup');
					optg.label = 'Сотрудничество'
					opt = document.createElement('option');
					opt.innerHTML = 'Отправить в Сотрудничество';
					opt.style = "font-style:italic;"
					opt.value = "sotrud";
					optg.appendChild(opt);
					select_text.appendChild(optg);
				td.appendChild(select_text);
				td.width = '225px';
				tr.appendChild(td);
				
				td = document.createElement('td');
				select_text = document.createElement('select');
				select_text.setAttribute('onchange', 'javascript:edit_sms_send(this, ' + Item['id'] + ');');
					opt = document.createElement('option');
					opt.innerHTML = 'Не отправлять';
					opt.value = "-1";
					select_text.appendChild(opt);
					
					for (var k in list_sms_templ)
					{
						let sms = list_sms_templ[k];
						opt = document.createElement('option');
						opt.innerHTML = sms['name'];
						opt.value = sms['id'];
						if (new_sms_send == sms['id'])
							opt.selected = true;
						select_text.appendChild(opt);
					}
					
				td.appendChild(select_text);
				td.width = '225px';
				
				tr.appendChild(td);
						
				td = document.createElement('td');
				td.innerHTML = '<button onclick="javascript:del_code(this, ' + Item['id'] + ');">Удалить</button>';
				tr.appendChild(td);
			document.querySelector('#code_table').appendChild(tr);
		}
		else 
		{
			
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function del_code(el, id)
{
	var data = "&block=settings&action=del_endcode&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
		}
		else 
		{
			
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_code(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor;
	//var text_ = el.parentNode.parentNode.querySelector('input').value;
	var text_ = el.value;
	var data = "&block=settings&action=edit_endcode&id=" + id + "&val=" + text_;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_code_send(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor;
	var el_s = el.selectedIndex;

	var text_ = el.options[el_s].value;
	var data = "&block=settings&action=edit_endcode_send&id=" + id + "&val=" + text_;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_sms_send(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor;
	var el_s = el.selectedIndex;

	var text_ = el.options[el_s].value;
	var data = "&block=settings&action=edit_sms_send&id=" + id + "&val=" + text_;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

var timeout = 1000;
var flag_list = new Array();
function show_set_smst()
{
	var list_types = new Array();
	list_types['greeting']		= 'Приветствие';
	list_types['in_tm']			= 'Поступил в ТМ';
	list_types['tm_manual']		= 'Для менеджеров ТМ';
	list_types['in_ozs']		= 'Поступил в ОЗС';
	list_types['ozs_manual']	= 'Для менеджеров ОЗС';
	list_types['bye']			= 'Прощание';
	list_types['spam']			= 'Спам';
	list_types['close_spam']	= 'Спам при закрытии';
	
	var data = "&block=settings&action=get_smst_list";
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_set('block');
			var set_wrap_b = document.querySelector('#set_wrap_b');
			set_wrap_b.innerHTML = 'Используемые ключи:<br>';
			set_wrap_b.innerHTML += '{WORK_TEL} - Телефон фирмы<br>';
			set_wrap_b.innerHTML += '{STAFF_NAME} - Имя сотрудника<br>';
			set_wrap_b.innerHTML += '{STAFF_LASTNAME} - Фамилия сотрудника<br>';
			set_wrap_b.innerHTML += '{STAFF_TEL} - Рабочий телефон сотрудника<br>';
			set_wrap_b.innerHTML += '{STAFF_MOB} - Мобильный телефон сотрудника<br>';
			set_wrap_b.innerHTML += '{STAFF_MAIL} - Почта сотрудника<br>';
			set_wrap_b.innerHTML += '{CLIENT_NAME} - Имя клиента<br>';
			set_wrap_b.innerHTML += '{CLIENT_MIDNAME} - Отчетство клиента<br>';
			set_wrap_b.innerHTML += '{MEET_TIME} - Время назначенной встречи<br>';
			set_wrap_b.style.height = '25%';
			set_wrap_b.style.width = '50%';
			set_wrap_b.style.overflow = 'auto';
			var set_wrap = document.querySelector('#set_wrap');
			set_wrap.style.height = '70%';
			set_wrap.style.overflow = 'auto';
			set_wrap.style.width = '1200px';
			set_wrap.innerHTML = '';
			
			var table = document.createElement('table');
				table.border ='1';
				table.style.width = '1150px';
				var th = document.createElement('thead');
					th.setAttribute('class', 'thead_sms');
					//th.width = '700px';
					//th.style.display = 'block';
					var tr = document.createElement('tr');
						var td = document.createElement('td');
						td.innerHTML = 'ID';
						td.width = '25px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'Тип';
						td.width = '175px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'Вкл';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'ТМ';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'ОЗС';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '225px';
						td.innerHTML = 'Название';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'Текст';
						td.width = '425px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = 'Удаление';
						td.width = '75px';
					tr.appendChild(td);
				th.appendChild(tr);
			table.appendChild(th);
			
			var tb = document.createElement('tbody');
				tb.setAttribute('class', 'tbody_sms');
				/*tb.style.display = 'block';
				tb.style.overflow = 'auto';
				tb.style.height = '70%';
			*/
			var data = Item['data'];
			
			for (var key in data)
			{
				var row = data[key];
				
				var rowid = parseInt(row['id']);
				flag_list[row['id']] = new Array(0, 0, 0, 0, 0, 0);
				
				tr = document.createElement('tr');
					td = document.createElement('td');
						td.innerHTML = row['id'];
						td.width = '25px';
					tr.appendChild(td);
						td = document.createElement('td');
							td.width = '175px';
							var sel = document.createElement('select');
								sel.setAttribute('onchange', 'javascript:update_smst_type(0, ' + rowid + ', this);');
								var opt = document.createElement('option');
									opt.value = '-1';
									opt.innerHTML = 'Выберите';
								sel.appendChild(opt);
								for (var k in list_types)
								{
									opt = document.createElement('option');
										opt.value = k;
										opt.innerHTML = list_types[k];
										if (k == row['type'])
											opt.selected = true;
									sel.appendChild(opt);
								}
						td.appendChild(sel);
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '30px';
						//td.innerHTML = row['en'];
						var checked = (row['en'] == 1) ? ' checked' : '';
						td.innerHTML = '<input name="en" onclick="javascript:update_smst_ch(1, ' + rowid + ', this);" type="checkbox" value="1"' + checked + '>';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '30px';
						//td.innerHTML = row['av_tm'];
						checked = (row['av_tm'] == 1) ? ' checked' : '';
						td.innerHTML = '<input name="av_tm" onclick="javascript:update_smst_ch(2, ' + rowid + ', this);" type="checkbox" value="1"' + checked + '>';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '30px';
						//td.innerHTML = row['av_ozs'];
						checked = (row['av_ozs'] == 1) ? ' checked' : '';
						td.innerHTML = '<input name="av_ozs" onclick="javascript:update_smst_ch(3, ' + rowid + ', this);" type="checkbox" value="1"' + checked + '>';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '225px';
						td.innerHTML = '<input style="width:200px;" oninput="javascript:update_smst_name(4, ' + rowid + ', this);" type="text" value="' + row['name'] + '">';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '425px';
						td.innerHTML = '<textarea style="width:400px; height:70px;" oninput="javascript:update_smst_msg(5, ' + rowid + ', this);">' + row['msg'] + '</textarea>';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<span style="cursor:pointer; font-style:italic;" onclick="javascript:del_smst(this, ' + rowid + ');">Удаление<span>';
						td.width = '75px';
					tr.appendChild(td);
				tb.appendChild(tr);
			}
			table.appendChild(tb);
			set_wrap.appendChild(table);	
			
			table = document.createElement('table');
				table.border ='1';
				table.style.width = '1150px';
				th = document.createElement('tbody');
					th.setAttribute('class', 'thead_sms');
					//th.width = '700px';
					//th.style.display = 'block';
					tr = document.createElement('tr');
						td = document.createElement('td');
						td.innerHTML = ' ';
						td.width = '25px';
					tr.appendChild(td);
						td = document.createElement('td');
							td.width = '175px';
							sel = document.createElement('select');
								//sel.setAttribute('onchange', 'javascript:update_smst_type(0, ' + rowid + ', this);');
								opt = document.createElement('option');
									opt.value = '-1';
									opt.innerHTML = 'Выберите';
								sel.appendChild(opt);
								for (var k in list_types)
								{
									opt = document.createElement('option');
										opt.value = k;
										opt.innerHTML = list_types[k];
									sel.appendChild(opt);
								}
						td.appendChild(sel);
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input name="en" type="checkbox" value="1">';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input name="av_tm" type="checkbox" value="1">';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<input name="av_ozs" type="checkbox" value="1">';
						td.width = '30px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '225px';
						td.innerHTML = '<input name="name" style="width:200px;" type="text">';
					tr.appendChild(td);
						td = document.createElement('td');
						td.innerHTML = '<textarea style="width:400px; height:50px;"></textarea>';
						td.width = '425px';
					tr.appendChild(td);
						td = document.createElement('td');
						td.width = '75px';
						td.innerHTML = '<button onclick="javascript:add_smst(this);">Добавить</button>';
					tr.appendChild(td);
				th.appendChild(tr);
			table.appendChild(th);
			set_wrap.appendChild(table);
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function update_smst_msg(num, id, el)
{
	flag_list[id][num]++;
	setTimeout(function(){sent_smst_val(id, num, 'msg', el.value, el);}, 2000);
}

function update_smst_name(num, id, el)
{
	flag_list[id][num]++;
	setTimeout(function(){sent_smst_val(id, num, 'name', el.value, el);}, 2000);
}

function update_smst_ch(num, id, el)
{
	flag_list[id][num]++;
	var name = el.name;
	var val = 0;
	if (el.checked)
		val = 1;
	setTimeout(function(){sent_smst_val(id, num, name, val, el.parentNode);}, 100);
}

function update_smst_type(num, id, el)
{
	flag_list[id][num]++;
	setTimeout(function(){sent_smst_val(id, num, 'type', el.options[el.selectedIndex].value, el);}, 2000);
}

function sent_smst_val(id, num, name, val, el)
{
	flag_list[id][num]--;
	
	//console.log('sent_point_value flag_list=' + flag_list[id][num]);
	var old_color = window.getComputedStyle(el).backgroundColor
	
	if (flag_list[id][num] == 0)
	{
		
		var data = "block=settings&action=edit_smst&id=" + id + "&name=" + name + "&val=" + encodeURI(val);
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				//points_editor('block');
				el.style.backgroundColor = 'lightgreen';
			}
			else 
			{
				el.style.backgroundColor = 'red';
				alert(Item['msg']);
			}
			setTimeout(function(){ch_color(el, old_color);}, timeout);
		});
		console.log(data);
	}
}

function del_smst(el, id)
{
	if (!confirm('Удалить шаблон №' + id + '?'))
		return;
	el = el.parentNode;
	var old_color = window.getComputedStyle(el).backgroundColor;

	var data = "block=settings&action=del_smst&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_set_smst();
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	});
}

function add_smst(el)
{

	
	var old_color = window.getComputedStyle(el).backgroundColor;

	var data = "block=settings&action=add_smst";
	var inps = el.parentNode.parentNode.querySelectorAll('input');
	for (var i=0; i<inps.length; i++)
	{
		if (inps[i].type == 'checkbox')
		{
			var val = (inps[i].checked) ? 1 : 0;
			data += "&" + inps[i].name + "=" + val;
		}
		else
			data += "&" + inps[i].name + "=" + encodeURI(inps[i].value);
	}
	
	var sel = el.parentNode.parentNode.querySelector('select');
	sel = sel.options[sel.selectedIndex].value;
	
	if (sel == "-1")
	{
		alert("Выберите тип!");
		return;
	}
	
	data += "&type=" + sel;
	
	var msg = el.parentNode.parentNode.querySelector('textarea');
	data += "&msg=" + encodeURI(msg.value);
	
	console.log(data);
	//return;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_set_smst();
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, timeout);
	});
}

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

function show_set_blanks()
{
	show_set('block');
	var set_wrap_b = document.querySelector('#set_wrap_b');
	set_wrap_b.style = '';
	set_wrap_b.innerHTML = '';
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.style = '';
	var tmp = '<br><br><div class="table">';
		tmp += '<div class="row_t">';
			tmp += '<div class="cell_t" data-tooltip="blank_0"><a href="/files/forms/0" data-tooltip="blank_0">Контракт физлица</a></div>';
			tmp += '<div class="cell_t">Заменить файлом: <input type="file" name="blank_0" id="blank_0"> <button onclick="javascript:save_new_blank(0);">Сохранить</button></div>';
		tmp += '</div>';
		tmp += '<div class="row_t">';
			tmp += '<div class="cell_t" data-tooltip="blank_1"><a href="/files/forms/1" data-tooltip="blank_1">Контракт юрлица</a></div>';
			tmp += '<div class="cell_t">Заменить файлом: <input type="file" name="blank_1" id="blank_1"> <button onclick="javascript:save_new_blank(1);">Сохранить</button></div>';
		tmp += '</div>';
		tmp += '<div class="row_t">';
			tmp += '<div class="cell_t" data-tooltip="blank_2"><a href="/files/forms/2" data-tooltip="blank_2">Акт выполненых работ</a></div>';
			tmp += '<div class="cell_t">Заменить файлом: <input type="file" name="blank_2" id="blank_2"> <button onclick="javascript:save_new_blank(2);">Сохранить</button></div>';
		tmp += '</div>';
	tmp += '</div>'
	set_wrap.innerHTML = tmp;
}

function save_new_blank(fileid)
{
	var formData = new FormData();
	formData.append("block", "settings");
	formData.append("action", "update_blank");
	formData.append("bid", fileid);
	formData.append("num_files", 1);
	var file = document.getElementById("blank_" + fileid).files[0];
	formData.append("uploadfile" + fileid, file);
	
	ajaxQuery2('/ajax.php','POST', formData, false, function(req)
	{
		//alert(1);
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Файл(ы) загружен(ы)');
			//show('none');
			document.getElementById("blank_" + fileid).value="";
		//	location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
		//document.getElementById("result").innerHTML = tmp_page;
		//alert(tmp_page);
		//location.reload();
	});
}

function make_new_algo()
{
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.innerHTML = '<br>Выберите позицию: ';
	
	console.log(list_algo);
	
	var sel = document.createElement('select');
		sel.id='new_algo_position';
		
		var opt = document.createElement('option');
		opt.value = '-1';
		opt.innerHTML = 'Выберите';
		sel.appendChild(opt);
		
	for (var key in list_algo)
	{
		if (!list_algo.hasOwnProperty(key))
			continue;
		var opt = document.createElement('option');
		opt.value = key;
		opt.innerHTML = list_algo[key].description;
		sel.appendChild(opt);
		
		console.log(list_algo[key]);
	}
	
	set_wrap.appendChild(sel);

	set_wrap.innerHTML += '<br><br>Введите название для алгоритма: <input type="text" id="new_algo_name" size="64"><br><br>';
	
	var text_inp = document.createElement('textarea');
		text_inp.id = "new_algo_text";
		text_inp.name = "new_algo_text";
		text_inp.rows = "30";
		text_inp.cols = "100";
	
	set_wrap.appendChild(text_inp);
	
	set_wrap.innerHTML += '<br><br><button style="float:right;" onclick="javascript:save_algo(false);">Сохранить</button>';
	
	$(document).ready(function () { $("#new_algo_text").cleditor(); });
}
function edit_algo(position)
{
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.innerHTML = '<br>Выберите позицию: ';
	
	console.log(position);
	console.log(list_algo);
	
	var sel = document.createElement('select');
		sel.id='new_algo_position';
		
		var opt = document.createElement('option');
		opt.value = '-1';
		opt.innerHTML = 'Выберите';
		sel.appendChild(opt);
	
	for (var key in list_algo)
	{
		if (!list_algo.hasOwnProperty(key))
			continue;
		
		//sel.appendChild(option = new Option(text, value, defaultSelected, selected));
		
		var opt = document.createElement('option');
		opt.value = key;
		console.log(key);
		if (key == position)
		{
			console.log('hit');
			opt.selected = true;
			opt.defaultSelected = true;
			
		}
			
		opt.innerHTML = list_algo[key].description;
		sel.appendChild(opt);
		
		console.log(list_algo[key]);
	}
	
	set_wrap.appendChild(sel);

	set_wrap.innerHTML += '<br><br>Введите название для алгоритма: <input type="text" id="new_algo_name" size="64" value="' + list_algo[position]['algo_name'] + '"><br><br>';
	
	var text_inp = document.createElement('textarea');
		text_inp.id = "new_algo_text";
		text_inp.name = "new_algo_text";
		text_inp.rows = "50";
		text_inp.cols = "100";
		text_inp.innerHTML = list_algo[position]['algo_text'];
	
	set_wrap.appendChild(text_inp);
	
	set_wrap.innerHTML += '<br><br><button style="float:right;" onclick="javascript:save_algo(true);">Сохранить</button>';
	
	$(document).ready(function () { $("#new_algo_text").cleditor(); });
}

function save_algo(edit)
{
	var new_algo_position = document.querySelector('#new_algo_position');
	new_algo_position = new_algo_position.options[new_algo_position.selectedIndex].value;
	
	var new_algo_name = document.querySelector('#new_algo_name').value;
	
	var new_algo_text = document.querySelector('#new_algo_text');
	console.log(new_algo_position);
	console.log(new_algo_name);
	console.log(new_algo_text);
	
	if (new_algo_position == "-1")
		alert('Выберите позицию!');
	else if (new_algo_position == "-1")
		alert('Введите название!');
	else
	{
		var ed = (edit) ? 1 : 0;
		var tmp_algo_text = encodeURI(getInputValue(new_algo_text));
		
		var data = "&new_algo_text=" + tmp_algo_text.replace(/\&/g, "*and*");
		data += "&block=settings&action=make_new_algo&pos=" + new_algo_position + "&name=" + new_algo_name + "&edit=" + ed;
		
		console.log(data);
		
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				//list_algo.length = 0;
				//list_algo = Item['block_list'];
				show_set_algo();
			}
			else 
			{
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
	
}

function listalgo()
{
	var set_wrap = document.querySelector('#set_wrap');
	set_wrap.style = '';
	var tmp_str = '<div class="table">';
	
	for(var key in list_algo)
	{
		var tmp = list_algo[key];
		
		console.log(tmp);
		if (tmp['algo_text'] == null)
			continue;
		
		tmp_str += '<div class="row_t">';
		tmp_str += '<div class="cell_t"><a title="Редактировать" href="#" onclick="javasccript:edit_algo(\'' + key + '\')"><img src="/img/edit.png" width="16" height="16"></a></div>';
		tmp_str += '<div class="cell_t">' + tmp['description'] + '</div>';
		tmp_str += '<div class="cell_t">' + tmp['algo_name'] + '</div>';
		tmp_str += '<div class="cell_t">' + tmp['algo_text'] + '</div>';
		tmp_str += '</div>';
		
		
	}
	tmp_str += '</div>';
	
	set_wrap.innerHTML = tmp_str;
}

function ajaxQuery2(url, method, param, async, onsuccess, onfailure) 
{
	var xmlHttpRequest = new XMLHttpRequest();
	var callback = function(r) 
	{ 
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r)); 
	};
	if(async) 
	{ 
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } } 
	}
	xmlHttpRequest.open(method, url, async);
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}

function show_set(state)
{
	document.getElementById('wrap_setl').style.display = state;
	document.getElementById('window_set').style.display = state;
}

function update_head_data()
{
	get_tasks_count();
	get_events_count();
}

var showingTooltip;
document.onmouseover = function(e)
{
	var target = e.target;

	var tooltip = target.getAttribute('data-tooltip');
	if (!tooltip) return;
	
	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	
	console.log(tooltip);

	var tmp_str = '';
	switch(tooltip)
	{
			
		case 'blank_0':
		{
			tmp_str += '<b>Доступные ключи:</b>';
			tmp_str += '<ul>';
			tmp_str += '<li>NUMBER - Номер анкеты</li>';
			tmp_str += '<li>DATE - Дата</li>';
			tmp_str += '<li>FULLNAME - Полное ФИО</li>';
			tmp_str += '<li>LASTNAME - Фамилия</li>';
			tmp_str += '<li>FIRSTNAME - Имя</li>';
			tmp_str += '<li>FN_S - Первая буква имени</li>';
			tmp_str += '<li>MIDDLENAME - Отчество</li>';
			tmp_str += '<li>MN_S - Первая буква отчества</li>';
			tmp_str += '<li>PASSPORT - Паспорт</li>';
			tmp_str += '<li>REALADDRESS - Фактический адрес</li>';
			tmp_str += '<li>ADDRESS - Адрес регистрации</li>';
			tmp_str += '<li>BIRTHDAY - День рождения</li>';
			tmp_str += '<li>HOMEPHONE - Телефон по месту жительства</li>';
			tmp_str += '<li>MOBILEPHONE - Личный телефон</li>';
			tmp_str += '<li>EMAIL - Почта</li>';
			tmp_str += '<li>CREDIT_PROGRAMM - Кредитная программа</li>';
			tmp_str += '<li>CREDIT_GOAL - Цель кредита</li>';
			tmp_str += '<li>CREDIT_MIN_SUM - Минимальная сумма кредита</li>';
			tmp_str += '<li>CREDIT_MAX_SUM - Максимальная сумма кредита</li>';
			tmp_str += '<li>COMISSION - Комиссия по залоговым</li>';
			tmp_str += '<li>COM_ZALOG - Комиссия по беззалогу</li>';
			tmp_str += '<li>MAX_CRED_NUM - Число продуктов</li>';
			tmp_str += '<li>CREDIT_MIN_TIME - Минимальный срок кредита</li>';
			tmp_str += '<li>CREDIT_MAX_TIME - Максимальный срок кредита</li>';
			tmp_str += '<li>MAX_FIRST_PAY - Максимальный первоначальный взнос</li>';
			tmp_str += '<li>MAX_MONTH_PAY - Максимальный платеж</li>';
			tmp_str += '<li>INWORK - В работе у</li>';
			tmp_str += '<li>STAFFDET - Детали сотрудника</li>';
			tmp_str += '<li>OTHERINFO - Прочая информация</li>';
			tmp_str += '</ul><br>';
			
			tooltipElem.innerHTML = tmp_str;
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		case 'blank_1':
		{
			tmp_str += '<b>Доступные ключи:</b>';
			tmp_str += '<ul>';
			tmp_str += '<li>NUMBER - Номер анкеты</li>';
			tmp_str += '<li>DATE - Дата</li>';
			tmp_str += '<li>FULLNAME - Полное ФИО</li>';
			tmp_str += '<li>LASTNAME - Фамилия</li>';
			tmp_str += '<li>FIRSTNAME - Имя</li>';
			tmp_str += '<li>FN_S - Первая буква имени</li>';
			tmp_str += '<li>MIDDLENAME - Отчество</li>';
			tmp_str += '<li>MN_S - Первая буква отчества</li>';
			tmp_str += '<li>PASSPORT - Паспорт</li>';
			tmp_str += '<li>PASSP_FULL - Полный паспорт</li>';
			tmp_str += '<li>REALADDRESS - Фактический адрес</li>';
			tmp_str += '<li>ADDRESS - Адрес регистрации</li>';
			tmp_str += '<li>BIRTHDAY - День рождения</li>';
			tmp_str += '<li>HOMEPHONE - Телефон по месту жительства</li>';
			tmp_str += '<li>MOBILEPHONE - Личный телефон</li>';
			tmp_str += '<li>EMAIL - Почта</li>';
			tmp_str += '<li>CNAME - Название фирмы</li>';
			tmp_str += '<li>CPOS - Должность</li>';
			tmp_str += '<li>FACT_WORK_ADR - Фактический адрес компании</li>';
			tmp_str += '<li>INN_COMPANY - Инн компании</li>';
			tmp_str += '<li>WORK_PHONE - Рабочий телефон</li>';
			tmp_str += '<li>CREDIT_PROGRAMM - Кредитная программа</li>';
			tmp_str += '<li>CREDIT_GOAL - Цель кредита</li>';
			tmp_str += '<li>CREDIT_MIN_SUM - Минимальная сумма кредита</li>';
			tmp_str += '<li>CREDIT_MAX_SUM - Максимальная сумма кредита</li>';
			tmp_str += '<li>COMISSION - Комиссия по залоговым</li>';
			tmp_str += '<li>COMISS1ON - Комиссия по залоговым с текстом</li>';
			tmp_str += '<li>COM_ZALOG/COMISSZAL - Комиссия по беззалогу</li>';
			tmp_str += '<li>COMISSFZAL - Комиссия по беззалогу с текстом</li>';
			tmp_str += '<li>COM_ZALOG - Комиссия по беззалогу</li>';
			tmp_str += '<li>MAX_CRED_NUM - Число продуктов</li>';
			tmp_str += '<li>CREDIT_MIN_TIME - Минимальный срок кредита</li>';
			tmp_str += '<li>CREDIT_MAX_TIME - Максимальный срок кредита</li>';
			tmp_str += '<li>MAX_FIRST_PAY - Максимальный первоначальный взнос</li>';
			tmp_str += '<li>MAX_MONTH_PAY - Максимальный платеж</li>';
			tmp_str += '<li>INWORK - В работе у</li>';
			tmp_str += '<li>STAFFDET - Детали сотрудника</li>';
			tmp_str += '<li>OTHERINFO - Прочая информация</li>';
			tmp_str += '</ul><br>';
			
			
			
			tooltipElem.innerHTML = tmp_str;
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		case 'blank_2':
		{
			tmp_str += '<b>Доступные ключи:</b>';
			tmp_str += '<ul>';
			tmp_str += '<li>NUMBER - Номер анкеты</li>';
			tmp_str += '<li>DATE - Дата</li>';
			tmp_str += '<li>FULLNAME - Полное ФИО</li>';
			tmp_str += '<li>LASTNAME - Фамилия</li>';
			tmp_str += '<li>FIRSTNAME - Имя</li>';
			tmp_str += '<li>MIDDLENAME - Отчество</li>';
			tmp_str += '<li>PASSPORT - Паспорт</li>';
			tmp_str += '<li>REALADDRESS - Фактический адрес</li>';
			tmp_str += '<li>ADDRESS - Адрес регистрации</li>';
			tmp_str += '<li>BIRTHDAY - День рождения</li>';
			tmp_str += '<li>HOMEPHONE - Телефон по месту жительства</li>';
			tmp_str += '<li>MOBILEPHONE - Личный телефон</li>';
			tmp_str += '<li>EMAIL - Почта</li>';
			tmp_str += '<li>CREDIT_PROGRAMM - Кредитная программа</li>';
			tmp_str += '<li>CREDIT_GOAL - Цель кредита</li>';
			tmp_str += '<li>CREDIT_MIN_SUM - Минимальная сумма кредита</li>';
			tmp_str += '<li>CREDIT_MAX_SUM - Максимальная сумма кредита</li>';
			tmp_str += '<li>COMISSION - Комиссия по залоговым</li>';
			tmp_str += '<li>COM_ZALOG - Комиссия по беззалогу</li>';
			tmp_str += '<li>MAX_CRED_NUM - Число продуктов</li>';
			tmp_str += '<li>CREDIT_MIN_TIME - Минимальный срок кредита</li>';
			tmp_str += '<li>CREDIT_MAX_TIME - Максимальный срок кредита</li>';
			tmp_str += '<li>MAX_FIRST_PAY - Максимальный первоначальный взнос</li>';
			tmp_str += '<li>MAX_MONTH_PAY - Максимальный платеж</li>';
			tmp_str += '<li>CONTRACTD - Дата заключения контракта</li>';
			
			
			tooltipElem.innerHTML = tmp_str;
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		
		default:
			tooltipElem.innerHTML = tooltip;
			break;
	}
	//if (tooltip != 'algo_1')
		document.body.appendChild(tooltipElem);
	//else
		//target.appendChild(tooltipElem);

	var coords = target.getBoundingClientRect();

	var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
	if (left < 0) left = 0; // не вылезать за левую границу окна

	//var top = coords.top - tooltipElem.offsetHeight - 5;
	var top = coords.top + target.offsetHeight + 5;
	if (top < 0) { // не вылезать за верхнюю границу окна
	top = coords.top + target.offsetHeight + 5;
	}

	tooltipElem.style.left = left + 'px';
	tooltipElem.style.top = top + 'px';

	console.log(tooltipElem);
		

	showingTooltip = tooltipElem;
};

document.onmouseout = function(e)
{
	if (showingTooltip)
	{

		document.body.removeChild(showingTooltip);
		showingTooltip = null;

	}
	

};

//gfdsgfdg
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
//gfdgfdgfdffffffffffffffffffffff
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////