var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

window.onload = update_all_data;

function list_main() {
	get_list('');
}
	

function update_all_data()
{
	
	get_tasks_count();
	get_events_count();
	list_main();
	//document.multiselect('#sel_bank_answer');
	//$('#sel_bank_answer').multiselect();
	//$('#sel_bank_answer').multiselect();
}

function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); } 

function form_filter(el)
{
	var filter = el.value;
	if (filter == '')
	{
		br_filer();
		return;
	}
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		if (!isNumber(key))
			continue;

		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[1].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
}

function fio_filter(filter)
{
	br_filer();
	return;
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		if (!isNumber(key))
			continue;

		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[2].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
}

function bank_filter(filter)
{
	br_filer();
	return;
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		if (!isNumber(key))
			continue;
		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[3].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
	
}

function get_list(filter) {
	var data = "block=bankrequest&action=get_list" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'] > 0)
			{
				add_data(Item['data']);
			}
		}
		
	});
}

function ch_breq_status(id, el) {
	var data = "block=bankrequest&action=ch_breq_status&req_id=" + id + "&new_st=" + el.value;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		
	});
}


function ch_bans_status(id, el) {
	
	var val = 0;
	if(el.value == 'reject')
	{
		var ddd = el.parentNode.parentNode.querySelectorAll('div[class="list_cell"]');
		val = ddd[4].innerHTML.replace(/\D+/g,"");
		el.parentNode.parentNode.querySelector('input[name="sum_val_ans"]').value = val;
	}
	else
	{
		val = el.parentNode.parentNode.querySelector('input[name="sum_val_ans"]').value;
		if (val == '')
		{
			alert('Введите сумму ответа!');
			el.options[0].selected = true;
			return;
		}
		
		if (el.value == "")
			return;
	}
	
	var data = "block=bankrequest&action=ch_bans_status&bans=" + el.value + "&req_id=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		
	});
}


function add_data(mass) {
	var row_odd = "row_odd";	//нечет
	var row_even = "row_even";	//чет
	var c = 0;
	var tmp_add = "";
	for (var key in mass)
	{
		c++;
		//console.log(key + " ::: " + Item);
		var row = mass[key];
		
		var row_c = (c & 1) ? row_odd : row_even;
		tmp_add += '<div class="' + row_c + '">';
		let a = row['date_send'].split(/[^0-9]/);
		var d = new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
		d.setUTCHours(a[3]);
		//var d = new Date(Date.parse(row['date_send'] + 'Z'));
		//console.log(row['date_send']);
		var dhour = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
		var dmin = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
		var ddate = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
		var dmon = 1 + parseInt(d.getMonth());
		dmon = dmon < 10 ? "0" + dmon : dmon;
		var dyear = parseInt(d.getFullYear()) - 2000;
		var nd = ddate + '.' + dmon + '.' + dyear + ' <b>' + dhour + ':' + dmin + '</b>';
		//console.log(nd);
		
		//tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['date_send'] + '</a></div>';
		tmp_add += '<div class="list_cell" name="date_cell"><a href="/details/' + row['form_id'] + '/">' + nd + '</a></div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['form_id'] + '</a></div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['form_id'] + '/">' + row['ln'] + ' ' + row['fn'] + ' ' + row['mn'] + '</a></div>';
		tmp_add += '<div class="list_cell">' + row['bankname'] + '</div>';
		var ssum = row['sum_send'].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + " р.";
		tmp_add += '<div class="list_cell">' + ssum + '</div>';
		
		var st = new Array();
		st[0] = (row['st'] == 'ready') ? ' selected' : '';
		st[1] = (row['st'] == 'sent') ? ' selected' : '';
		//st[2] = (row['st'] == 'get_answer') ? ' selected' : '';
		
		tmp_add += '<div class="list_cell" name="status_ans">';
		
		
		switch(row['st'])
		{
			case "get_answer" : tmp_add += 'Получен ответ'; break;
			case "ready" : tmp_add += 'Ждет отправления'; break;
			case "sent" : tmp_add += 'Отправлена'; break;
			case "canceled" : tmp_add += 'Отказ клиента'; break;
			//case "ready" : tmp_add += ''; break;
		}
		
		if (row['st'] == "sent")
		{
			let a = row['sent2bank'].split(/[^0-9]/);
			d = new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
			d.setUTCHours(a[3]);
			//d = new Date(Date.parse(row['sent2bank'] + 'Z'));
			dhour = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
			dmin = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
			ddate = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			dmon = 1 + parseInt(d.getMonth());
			dmon = dmon < 10 ? "0" + dmon : dmon;
			dyear = parseInt(d.getFullYear()) - 2000;
			nd = ddate + '.' + dmon + '.' + dyear + ' <b>' + dhour + ':' + dmin + '</b>';
			tmp_add += ' (' + nd + ')';
		}
		else if ((row['st'] != "ready") && (row['st'] != "canceled"))
		{
			let a = row['date_answer'].split(/[^0-9]/);
			d = new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
			d.setUTCHours(a[3]);
			//d = new Date(Date.parse(row['date_answer'] + 'Z'));
			dhour = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
			dmin = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
			ddate = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			dmon = 1 + parseInt(d.getMonth());
			dmon = dmon < 10 ? "0" + dmon : dmon;
			dyear = parseInt(d.getFullYear()) - 2000;
			nd = ddate + '.' + dmon + '.' + dyear + ' <b>' + dhour + ':' + dmin + '</b>';
			tmp_add += ' (' + nd + ')';
		}
		
		/*if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))
		{
			switch(row['st'])
			{
				case "get_answer" : tmp_add += 'Получен ответ'; break;
				case "ready" : tmp_add += 'Ждет отправления'; break;
				case "sent" : tmp_add += 'Отправлена в банк'; break;
				case "canceled" : tmp_add += 'Отказ клиента'; break;
				//case "ready" : tmp_add += ''; break;
			}
			
		}
		else if (row['st'] == 'canceled')
		{
			tmp_add += 'Отказ клиента';
		}
		else if (row['st'] == 'get_answer')
		{
			tmp_add += 'Получен ответ';
		}
		else
		{
			tmp_add += '<select onchange="javasript:ch_breq_status(' + parseInt(row['bid']) + ', this);">';
			tmp_add += '<option value="ready"' + st[0] + '>Ждет отправления</option>';
			tmp_add += '<option value="sent"' + st[1] + '>Отправлена в банк</option>';
			//tmp_add += '<option value="get_answer"' + st[2] + '>Получен ответ</option>';
			tmp_add += '</select>';
		}*/
		
		tmp_add += '</div><div class="list_cell">';
		
		if (row['bank_answer'] != null)
		{
			var ba = (row['bank_answer'] == 'approve') ? '<font color="green">Одобрено</font>' : ((row['bank_answer'] == 'reject') ? '<font color="red">Отклонил</font>' : ((row['bank_answer'] == 'preapprove') ? '<font color="green">Предодобрено</font>' : ''));
			tmp_add += ba;
		}
		else
			tmp_add += '-';
		
		
		/*var dis = (row['st'] == 'sent') ? '' : ' disabled';
		
		
		if (row['bank_answer'] != null)
		{
			var ba = (row['bank_answer'] == 'approve') ? '<font color="green">Одобрено</font>' : ((row['bank_answer'] == 'reject') ? '<font color="red">Отклонил</font>' : '');
			tmp_add += ba;
		}
		else if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))
		{
			if (row['st'] == 'sent')
				tmp_add += 'Ожидается ответ банка';
			else
				tmp_add += '-';
			
		}
		else
		{
			
			tmp_add += '<select' + dis + ' onchange="javasript:ch_bans_status(' + parseInt(row['bid']) + ', this);">';
			tmp_add += '<option value="">Выберите</option>';
			tmp_add += '<option value="approve">Банк одобрил</option>';
			tmp_add += '<option value="reject">Банк отклонил</option>';
			tmp_add += '</select>';
		}*/
		
		tmp_add += '</div>';
		
		var as = "";
		if (row['on_arm'] != null)
		{
			as = row['on_arm'];
			as = as.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			tmp_add += '<div class="list_cell">' + as + '</div>';
		}
		else if (row['sum_answer'] != null)
		{
			as = row['sum_answer'];
			as = as.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			tmp_add += '<div class="list_cell">' + as + '</div>';
		}
		else
			tmp_add += '<div class="list_cell"><font color="red">не установлено</font></div>';
		
		var com_fact = '';
		if (row['com_fact'] != null)
		{
			com_fact = row['com_fact'];
			com_fact = com_fact.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		}
		tmp_add += '<div class="list_cell">' + com_fact + '</div>';
		tmp_add += '<div class="list_cell" style="display:none;">' + row['form_status'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['tm_man'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['ozs_man'] + '</div>';
			
		
		/*if ((staff_position == 'manager_ozs') || (staff_position == 'director_ozs'))		
			tmp_add += '<div class="list_cell">' + as + '</div>';
		else
			tmp_add += '<div class="list_cell"><input name="sum_val_ans" type="text" value="' + as + '"' + dis + '></div>';
		*/
		//var ssum = row[key2] + ((row['sum_val'] == "rub") ? " р." : "");
		//tmp_add += '<div class="list_cell">' + ssum + '</div>';
		
		
		/*for (var key2 in row)
		{
			if ((key2 == 'id') || (key2 == 'firstname') || (key2 == 'middlename') || (key2 == 'sum_val') || (key2 == 'status') || (key2 == 'office') || (key2 == 'manager') || (key2 == 'source_param'))
				continue;
			
			if (key2 == 'debug')
			{
				console.log('debug:' + row[key2]);
				continue;
			}
			
			if ((key2 == 'meet_event') || (key2 == 'call_event'))
				continue;
			
			if (key2 == 'lastname')
			{
				tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row[key2] + ' ' + row['firstname'] + ' ' + row['middlename'] + '</a></div>';
			}
			else if (key2 == 'source')
			{
				//tmp_add += '<div class="list_cell">' + row[key2] + ':' + row['source_param'] + '</div>';
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
			}
			else if (key2 == 'sum')
			{
				var ssum = row[key2] + ((row['sum_val'] == "rub") ? " р." : "");
				tmp_add += '<div class="list_cell">' + ssum + '</div>';
			}
			else
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
		}
		
		var ev = '';
		if ('meet_event' in row) 
		{
			if (row['meet_event'])
				ev += row['meet_event'];
		}
		
		if ('call_event' in row)
		{
			if (row['call_event'])
			{
				if (ev != '')
					ev += ', ';
				ev += row['call_event'];
			}
		}
		
		tmp_add += '<div class="list_cell">' + ev + '</div></div>';
		*/
		
		tmp_add += '</div>';
	}
	
	
	tmp_add += '<div class="row_itogo">';
		tmp_add += '<div class="list_cell">Итого</div>';
		tmp_add += '<div class="list_cell" id="it_forms"></div>';
		tmp_add += '<div class="list_cell"></div>';
		tmp_add += '<div class="list_cell"></div>';
		tmp_add += '<div class="list_cell" id="it_sum"></div>';
		tmp_add += '<div class="list_cell"></div>';
		tmp_add += '<div class="list_cell"></div>';
		tmp_add += '<div class="list_cell" id="it_answ"></div>';
		tmp_add += '<div class="list_cell" id="it_com"></div>';
		tmp_add += '<div class="list_cell"></div>';
		tmp_add += '<div class="list_cell"></div>';
	tmp_add += '</div>';
	document.querySelector('#list_table').innerHTML += tmp_add;
	
			
	//console.log('test111');
	if (getCookie('br_filer'))
	{
		//console.log('test2222');
		setCookie('br_filer', '', { expires: -1, path:"/"});
		document.querySelector('input[name="get_answer"]').checked = true;
		document.querySelector('input[name="answ_ap"]').checked = true;
		document.querySelector('input[name="answ_preap"]').checked = true;
		document.querySelector('input[name="com_set"]').checked = false;
		document.querySelector('input[name="closed_forms"]').checked = false;
		//br_filer();
	}
	br_filer();
}


function br_filer()
{
	var wait_sent 		= document.querySelector('input[name="wait_sent"]').checked;
	var sent 			= document.querySelector('input[name="sent"]').checked;
	var get_answer 		= document.querySelector('input[name="get_answer"]').checked;
	var client_can 		= document.querySelector('input[name="client_can"]').checked;
	var answ_ap 		= document.querySelector('input[name="answ_ap"]').checked;
	var answ_preap 		= document.querySelector('input[name="answ_preap"]').checked;
	var answ_re 		= document.querySelector('input[name="answ_re"]').checked;
	var answ_not 		= document.querySelector('input[name="answ_not"]').checked;
	var com_set 		= document.querySelector('input[name="com_set"]').checked;
	var closed_forms 	= document.querySelector('input[name="closed_forms"]').checked;
	
	var fio 			= document.querySelector('input[name="fio"]').value;
	var bank 			= document.querySelector('input[name="bank"]').value;
	var tm 				= document.querySelector('input[name="tm"]').value;
	var ozs 			= document.querySelector('input[name="ozs"]').value;
	
	var show_all1 = false;
	var show_all2 = false;
	if (!wait_sent && !sent && !get_answer && !client_can)
		show_all1 = true;
	if (!answ_ap && !answ_preap && !answ_re && !answ_not)
	{
		show_all2 = true;
	}

	
	var table = document.querySelector("#list_table");
	var rows = table.querySelectorAll(".row_odd, .row_even");
	var sum_sum = 0;
	var sum_ans = 0;
	var sum_com = 0;
	var cc = 0;
	
	for(var i=0; i<rows.length; i++)
	{
		var col = rows[i].querySelectorAll('div');
		var show = false;
		
		//console.log(rows[i].innerHTML);
		//console.log(col[9].innerHTML);
		//console.log(get_answer);
		
		var _wait_sent 			= wait_sent 	&& /Ждет отправления/.test(col[5].innerHTML);
		var _sent 				= sent 			&& /Отправлена/.test(col[5].innerHTML);
		var _get_answer 		= get_answer 	&& /Получен ответ/.test(col[5].innerHTML);
		var _client_can 		= client_can 	&& /Отказ клиента/.test(col[5].innerHTML);
		var _answ_ap 			= answ_ap 		&& /Одобрено/.test(col[6].innerHTML);
		var _answ_preap 		= answ_preap	&& /Предодобрено/.test(col[6].innerHTML);
		var _answ_re 			= answ_re 		&& /Отклонил/.test(col[6].innerHTML);
		var _answ_not 			= answ_not 		&& /\-/.test(col[6].innerHTML);
		var _closed_forms 		= closed_forms	&& /work_complete/.test(col[9].innerHTML);
		var _com_set 			= com_set		&& (col[8].innerHTML == '') ? true : false;
		
		//console.log(/Получен ответ/.test(col[5].innerHTML));
		//console.log(_closed_forms);
		
		if (show_all1)
			show = true;
		else if (_wait_sent || _sent || _get_answer || _client_can)
			show = true;
		
		if (show && !show_all2)
		{
			if (!_answ_ap && !_answ_preap && !_answ_re && !_answ_not)
				show = false;
		}
		
		if (show && !com_set)
		{
			if (col[8].innerHTML != '')
				show = false;
		}
		
		if (show && !closed_forms)
		{
			if (col[9].innerHTML == 'work_complete')
				show = false;
		}
		
		if (show && (col[2].innerHTML != ''))
		{
			var reg = new RegExp(fio, "ig");
			if (!reg.test(col[2].innerHTML))
				show = false;
		}
		
		if (show && (col[3].innerHTML != ''))
		{
			var reg = new RegExp(bank, "ig");
			if (!reg.test(col[3].innerHTML))
				show = false;
		}
		
		if (show && (col[10].innerHTML != ''))
		{
			var reg = new RegExp(tm, "ig");
			if (!reg.test(col[10].innerHTML))
				show = false;
		}
		
		if (show && (col[11].innerHTML != ''))
		{
			var reg = new RegExp(ozs, "ig");
			if (!reg.test(col[11].innerHTML))
				show = false;
		}
		
		if (show)
		{
			sum_sum += parseInt(col[4].innerHTML.replace(/\D+/g,""));
			sum_ans += parseInt(col[7].innerHTML.replace(/\D+/g,"")) ? parseInt(col[7].innerHTML.replace(/\D+/g,"")) : 0;
			sum_com += parseInt(col[8].innerHTML.replace(/\D+/g,"")) ? parseInt(col[8].innerHTML.replace(/\D+/g,"")) : 0;
			rows[i].style.display = 'table-row';
			cc++;
			if (cc & 1)
				rows[i].className = 'row_odd';
			else
				rows[i].className = 'row_even';
		}
		else
			rows[i].style.display = 'none';
	}
	document.querySelector("#it_forms").innerHTML 	= cc;
	document.querySelector("#it_sum").innerHTML 	= ("" + sum_sum).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	document.querySelector("#it_answ").innerHTML 	= ("" + sum_ans).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	document.querySelector("#it_com").innerHTML 	= ("" + sum_com).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	//console.log(rows);
}



//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds