var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;
window.onload = update_all_data;


var events = [
    {title: 'Business Lunch', start: '2018-06-13T13:00:00'},
    {title: 'Meeting', start: '2018-06-13T11:00:00'},
    {title: 'Conference', start: '2018-06-18', end: '2017-12-20'},
];

let staff = {};



function update_all_data()
{
	get_events_count();
	get_tasks_count();
	let setLoadData = setInterval(() => {
//		loaddata();
//		loadcalls();
	}, 3000);

	let intervalUpdateLogins = setInterval(() => {
//		updateLogins();
	}, 10000)
//	loaddata();
//	loadcalls();
//	updateLogins();
}

let servTime;

function loaddata () {
	let url = "http://" + location.hostname + ":9777/list";
	ajaxJson(url,'POST', '', true, (req) => {
		var tmp_page = req.responseText;
		let text_ = '';
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{

			if (Item['staff'])
				staff = Item['staff'];

			if (Item['servTime']) {
				let datum = Item['servTime'].match(/(\d\d)\-(\d\d)-(\d\d\d\d) (\d\d):(\d\d):(\d\d)/);
				servTime = parseInt(datum[6]) + parseInt(datum[5] * 60) + parseInt(datum[4] * 3600)
					+ parseInt(datum[1] * 24 * 3600) + parseInt(datum[2] * 31 * 24 * 3600) + parseInt(datum[3] * 12 * 31 * 24 * 3600);
			}

			let data_ = [];
			for (key in Item['data']) {
				Item['data'][key]['name'] = key;
				data_.push(Item['data'][key]);
			}

			addrows2table(data_);
		}
		else
		{
			text_= Item['msg'];
			console.log(text_);
		}
	},
	function(req)
	{
		let text_= "Ошибка исполнения. Свяжитесь с администратором!";
		console.log(text_);
	});
};

function updateLogins() {
	let url = "/ajax.php";
	let data = "block=worktable&action=get_login_his";
	ajaxQuery(url,'POST', data, true, (req) => {
		var tmp_page = req.responseText;
		let text_ = '';
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{

			console.log('---------------');
			console.log(Item['data']);

			let data = Item['data'] ? Item['data'] : [];

			let table = document.querySelector('#loginlist');
			let tbody = table.querySelector('tbody');
			tbody.innerHTML = '';

			data.forEach(element => {
				if (db_users.indexOf(element.lastname) == -1)
					return;
				let row = document.createElement('tr');
				let td = document.createElement('td');
				td.setAttribute('height', '35px');
				td.innerHTML = element.lastname + ' ' + element.firstname;
				row.appendChild(td);

				td = document.createElement('td');
				td.innerHTML = element.adate;
				row.appendChild(td);
				tbody.appendChild(row);
			});
		}
		else
		{
			text_= Item['msg'];
			console.log(text_);
		}
	},
	function(req)
	{
		let text_= "Ошибка исполнения. Свяжитесь с администратором!";
		console.log(text_);
	});
}

function loadcalls() {
	let url = "http://" + location.hostname + ":9666/getcalls";
	ajaxJson(url,'GET', '', true, (req) => {
		var tmp_page = req.responseText;
		let text_ = '';
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			let table = document.querySelector('#wokrlist');
			let tbody = table.querySelector('tbody');
			let th = tbody.querySelectorAll('tr');

			for (let tri of th) {
				//console.log(tri.id);
				let login = tri.id.split('_')[1];
				//console.log(login);

				let tdm = tri.querySelectorAll('td');
				if (!Item['staff'][login])
					continue;
				let innum = Item['staff'][login].pwork;
				console.log(innum);
			}
		}
		else
		{
			text_= Item['msg'];
			console.log(text_);
		}
	},
	function(req)
	{
		let text_= "Ошибка исполнения. Свяжитесь с администратором!";
		console.log(text_);
	});
};

function addrows2table(data) {
	//console.table(data);
	let table = document.querySelector('#wokrlist');
	let tbody = table.querySelector('tbody');
	tbody.innerHTML = '';

	for (let i=0; i<data.length; i++) {
		if (data[i].lastActionTime) {
			let datum = data[i].lastActionTime.match(/(\d\d)\-(\d\d)-(\d\d\d\d) (\d\d):(\d\d):(\d\d)/);
			data[i].datum = parseInt(datum[6]) + parseInt(datum[5] * 60) + parseInt(datum[4] * 3600)
				+ parseInt(datum[1] * 24 * 3600) + parseInt(datum[2] * 31 * 24 * 3600) + parseInt(datum[3] * 12 * 31 * 24 * 3600);

			let delta = parseInt(servTime) - parseInt(data[i].datum);
			data[i].lastActionTime = data[i].lastActionTime + ', ' + formatDelta(delta);
		}
		else
			data[i].lastActionTime = 'неизвестно';
	}

	data.sort(function(a,b){
		// Turn your strings into dates, and then subtract them
		// to get a value that is either negative, positive, or zero.
		return new Date(b.datum) - new Date(a.datum);
	});

	for (let i=0; i<data.length; i++){

		if (!staff[data[i].name.toLowerCase()])
			continue;

		if (db_users.indexOf(staff[data[i].name.toLowerCase()].lastname) == -1)
			continue;

		let row = document.createElement('tr');
		row.id = 'rowid_' + data[i]['name'].toLowerCase();

		let td = document.createElement('td');
		td.setAttribute('height', '35px');
		td.innerHTML = staff[data[i].name.toLowerCase()].lastname;
		row.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = '<a href="' + data[i].lastActionPage + '">' + data[i].lastActionPage + '</a>';
		row.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = data[i].lastActionTime;
		row.appendChild(td);

		td = document.createElement('td');
		td.innerHTML = 'нет данных';
		row.appendChild(td);

		td = document.createElement('td');
		let isOnline = staff[data[i].name.toLowerCase()].online;
		let staffSt = staff[data[i].name.toLowerCase()].status;
		if (!isOnline)
			td.innerHTML = '<img height="16px" src="/img/ball_grey.png">';
		else if (staffSt == 'ready')
			td.innerHTML = '<img height="16px" src="/img/ball_green.png">';
		else
			td.innerHTML = '<img height="16px" src="/img/ball_red.png">';
		row.appendChild(td);

		td = document.createElement('td');
		/*let bdiv = document.createElement('div');
		bdiv.classList.add('btn-group');
		bdiv.classList.add('btn-group-sm');
		bdiv.innerHTML = '<button class="btn btn-primary btn-block">test</button>';
		td.appendChild(bdiv);
		*/row.appendChild(td);

		addRow2Table(tbody, row);
	}

	//console.table(data);

}

function formatDelta(delta) {
	if (delta < 300) {
		return delta + ' секунд назад';
	}
	else if (delta < 3600) {
		delta = Math.round(delta/60);
		return delta + ' минут назад';
	}
	else if (delta < 3600 * 49){
		delta = Math.round(delta/3600);
		return delta + ' часов назад';
	}
	else {
		delta = Math.round(delta/(3600 * 24));
		return delta + ' дней назад';
	}
}

function addRow2Table(tbody, row) {
	tbody.appendChild(row);
}

function ajaxQ(url, method, param, async, onsuccess, onfailure)
{
	var xmlHttpRequest = new XMLHttpRequest();
	var callback = function(r)
	{
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r));
	};

	if(async)
	{
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } }
	}
	xmlHttpRequest.open(method, url, async);
	if(method == 'POST') { xmlHttpRequest.setRequestHeader('Content-Type', 'application/json'); }
	xmlHttpRequest.setRequestHeader('Content-Type', 'application/json');
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}





//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
