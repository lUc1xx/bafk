var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

var timeout = 500;
var new_worker = true;
var new_office = true;
var worker_id = 0;
var office_id = 0;

window.onload = update_all_data;

function update_all_data()
{
	//update_data();
	get_tasks_count();
	get_events_count();
}

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
        isEscape = (evt.keyCode == 27);
    }
    if (isEscape) {
        //alert("Escape");
		show('none');
    }
};

function add_agent()
{
	show('block');
	/*var cont4agents = document.querySelector('#cont4agents');
	var div = cont4agents.querySelectorAll('div[class*=row]');
	var next_row;
	if ((div != null) && (div.length > 0))
	{
		var last_div = div[div.length-1];
		next_row = (last_div.className == 'row_odd') ? 'row_odd' : 'row_odd';
	}
	console.log(last_div);
	var tmp = '<div id="new_agent" class="' + next_row + '">';
	tmp += '<div class="list_cell"></div>';
	tmp += '<div class="list_cell"><input type="text" name="login"></div>';
	tmp += '<div class="list_cell"><input type="text" name="pass"></div>';
	tmp += '<div class="list_cell"></div>';
	tmp += '<div class="list_cell"><input type="text" name="name"></div>';
	tmp += '<div class="list_cell"><input type="text" name="phone"></div>';
	tmp += '<div class="list_cell"><input type="text" name="email"></div>';
	tmp += '<div class="list_cell"></div>';
	tmp += '<div class="list_cell"></div>';
	tmp += '<div class="list_cell"></div>';
	tmp += '<div class="list_cell"></div>';
	tmp += '</div>';
	cont4agents.innerHTML += tmp;
	*/
}

function nagent_add()
{
	var new_worker_wrap = document.querySelector("#new_agent_wrap");
	var butt = document.querySelector("#nagent_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	var error = false;
	for (var key in inputs)
	{
		var name = inputs[key].name;
		if ((name == 'login') || ((name == 'passwd') && new_worker) || (name == 'name'))
		{
			if(inputs[key].value == '')
			{
				error = true;
				inputs[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				inputs[key].setAttribute('style', 'border:none;');
		}
	}

	
	for (var key in sel)
	{
		var name = sel[key].name;
		if ((name == 'office') || (name == 'position'))
		{
			if(sel[key].options.selectedIndex == 0)
			{
				error = true;
				sel[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				sel[key].setAttribute('style', 'border:none;');
		}
	}
	
	if (!error)
	{
		var data = getQueryString(inputs);
		var nw = (new_worker) ? "add_worker" : ("edit_worker&id=" + worker_id);
		data += "&" + getQueryString(sel) + "&block=agents&action=" + nw;
		console.log(data);
		
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				butt.style.backgroundColor = 'lightgreen';
				console.log(Item['msg']);
				setTimeout(function(){location.reload();}, timeout);
			}
			else 
			{
				butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function nagent_clear()
{
	var new_worker_wrap = document.querySelector("#new_agent_wrap");
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	
	var butt = document.querySelector("#nagent_add_b");
	butt.style.backgroundColor = '';
	
	for (var key in inputs)
	{
		inputs[key].value = "";
		var name = inputs[key].name;
		if ((name == 'login') || (name == 'passwd') || (name == 'firstname') || (name == 'lastname'))
			inputs[key].setAttribute('style', 'border:none;');
	}
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	for (var key in sel)
	{
		
		var name = sel[key].name;
		if ((name == 'office') || (name == 'position'))
		{
			sel[key].options.selectedIndex = 0;
			sel[key].setAttribute('style', 'border:none;');
		}
	}
}

function make_acab(el)
{
	var row = el.parentNode.parentNode;
	
	var cells = row.querySelectorAll('div');
	
	var id = cells[1].innerHTML;
	var name = cells[2].innerHTML;
	var phone = cells[3].innerHTML;
	
	if (!confirm("Создать кабинет для " + name + "?"))
		return 0;
	show_agent_list();
	add_agent();
	
	var wr = document.querySelector('#new_agent_wrap');
	wr.querySelector('input[name="name"]').value = name;
	wr.querySelector('input[name="phone"]').value = phone;
}

function ch_areq_status(el, id)
{
	var old_color = window.getComputedStyle(el).backgroundColor;
	el.classList.remove('animated');
	var val = el.options[el.selectedIndex].value;
	
	var data = "block=agents&action=ch_areq_status&id=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'];
			//console.log(data);
			el.style.backgroundColor = 'lightgreen';
		
		}
		else 
		{
			el.style.backgroundColor = 'red';
			alert(Item['msg']);
		}
		setTimeout(function(){ch_color(el, old_color);}, 1000);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function edit_agent(id)
{
	new_worker = false;
	worker_id = id;
	var nworker_add_b = document.querySelector("#nagent_add_b");
	nworker_add_b.innerHTML = "Сохранить";
	console.log(id);
		
	var new_worker_wrap = document.querySelector("#new_agent_wrap");
	//var butt = document.querySelector("#nagent_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_worker_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_worker_wrap.querySelectorAll("div[class=fval]>select");
	
	
	var data = "block=agents&action=get_worker&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'][id];
			console.log(data);
			
			var error = false;
			for (var key in inputs)
			{
				console.log(inputs[key].name);
				if (data.hasOwnProperty(inputs[key].name))
				{
					inputs[key].value = data[inputs[key].name];
				}
			}

			
			for (var key in sel)
			{
				var name = sel[key].name;
				if ((name == 'office') || (name == 'position'))
				{
					if (data.hasOwnProperty(sel[key].name))
					{
						for(i=0;i<sel[key].options.length;i++)
						{
							if (sel[key].options[i].text == data[sel[key].name])
							{
								sel[key].options.selectedIndex = i;
								break;
							}
						}

						
					}
				}
			}
			show('block');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show(state)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state;
	
	
	if (state == 'none')
	{
		document.getElementById('window_of').style.display = state;
		document.querySelector('#nagent_add_b').style.backgroundColor = '';
		document.getElementById('window_ip').style.display = state;		
	}
}

function del_agent(id)
{
	console.log(id);
	var cont4staff = document.querySelector("#cont4agents");
	var div = cont4staff.querySelector('#agent_row_' + id);
	var dd = div.querySelectorAll("div[class=list_cell]");
	
	var st = dd[5].innerHTML + " " + dd[6].innerHTML + " " + dd[7].innerHTML;
	if (confirm("Вы точно хотите удалить запись об агенте " + st + " (id=" + id + ")?"))
	{
		var data = "block=agents&action=del_agent&id=" + id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert("Запись с id " + id + " удалена!");
				location.reload();
				//update_data();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}


function edit_office(id)
{
	new_office = false;
	office_id = id;
	var noffice_add_b = document.querySelector("#noffice_add_b");
	noffice_add_b.innerHTML = "Сохранить";
	console.log(id);
	
	var new_office_wrap = document.querySelector("#new_office_wrap");
	//var butt = document.querySelector("#noffice_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	
	var data = "block=agents&action=get_office&id=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'][id];
			console.log(data);
			
			var error = false;
			for (var key in inputs)
			{
				console.log(inputs[key].name);
				if (data.hasOwnProperty(inputs[key].name))
				{
					inputs[key].value = data[inputs[key].name];
				}
			}

			
			for (var key in sel)
			{
				var name = sel[key].name;
				if (name == 'type')
				{
					if (data.hasOwnProperty(sel[key].name))
					{
						for(i=0;i<sel[key].options.length;i++)
						{
							if (sel[key].options[i].text == data[sel[key].name])
							{
								sel[key].options.selectedIndex = i;
								break;
							}
						}

						
					}
				}
			}
			show_of('block');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function del_office(id)
{
	console.log(id);
	var cont4offices = document.querySelector("#cont4offices");
	var divs = cont4offices.querySelectorAll("div[class=row_odd], div[class=row_even]");
	var dd = divs[parseInt(id)-1].querySelectorAll("div[class=list_cell]");
	
	var st = dd[2].innerHTML;
	if (confirm("Вы точно хотите удалить запись об оффисе " + st + " (id=" + id + ")?"))
	{
		var data = "block=agents&action=del_office&id=" + id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert("Запись с id " + id + " удалена!");
				location.reload();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}


function add_office()
{
	console.log(1);
	new_office = true;
	var nworker_add_b = document.querySelector("#nworker_add_b");
	//nworker_add_b.innerHTML = "Добавить";
	show_of('block');
}

function office_add()
{
	var new_office_wrap = document.querySelector("#new_office_wrap");
	var butt = document.querySelector("#noffice_add_b");
	//butt.style.backgroundColor = '';
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	var error = false;
	for (var key in inputs)
	{
		var name = inputs[key].name;
		if (name == 'name')
		{
			if(inputs[key].value == '')
			{
				error = true;
				inputs[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				inputs[key].setAttribute('style', 'border:none;');
		}
	}

	
	for (var key in sel)
	{
		var name = sel[key].name;
		if (name == 'type')
		{
			if(sel[key].options.selectedIndex == 0)
			{
				error = true;
				sel[key].setAttribute('style', 'border:1px solid red;');
			}
			else
				sel[key].setAttribute('style', 'border:none;');
		}
	}
	
	if (!error)
	{
		var data = getQueryString(inputs);
		var nw = (new_office) ? "add_office" : ("edit_office&id=" + office_id);
		data += "&" + getQueryString(sel) + "&block=agents&action=" + nw;
		console.log(data);
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				butt.style.backgroundColor = 'lightgreen';
				console.log(Item['msg']);
				setTimeout(function(){location.reload();}, timeout);
			}
			else 
			{
				butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function office_clear()
{
	var new_office_wrap = document.querySelector("#new_office_wrap");
	var inputs = new_office_wrap.querySelectorAll("div[class=fval]>input");
	
	var butt = document.querySelector("#noffice_add_b");
	butt.style.backgroundColor = '';
	
	for (var key in inputs)
	{
		inputs[key].value = "";
		var name = inputs[key].name;
		if (name == 'name')
			inputs[key].setAttribute('style', 'border:none;');
	}
	var sel = new_office_wrap.querySelectorAll("div[class=fval]>select");
	
	for (var key in sel)
	{
		
		var name = sel[key].name;
		if (name == 'type')
		{
			sel[key].options.selectedIndex = 0;
			sel[key].setAttribute('style', 'border:none;');
		}
	}
}


function show_of(state)
{
	document.getElementById('window_of').style.display = state;			
	document.getElementById('wrap').style.display = state;
	
	
	if (state == 'none')
	{
		document.getElementById('window').style.display = state;
		document.getElementById('window_ip').style.display = state;		
		document.querySelector('#nworker_add_b').style.backgroundColor = '';
	}
}

function show_agent_list()
{
	document.querySelector('#arequests_wrap').style.display = 'none';
	document.querySelector('#alogs_wrap').style.display = 'none';
	document.querySelector('#offices_wrap').style.display = 'block';
	document.querySelector('#agent_wrap').style.display = 'block';
}

function show_agent_requests()
{
	document.querySelector('#arequests_wrap').style.display = 'block';
	document.querySelector('#alogs_wrap').style.display = 'none';
	document.querySelector('#offices_wrap').style.display = 'none';
	document.querySelector('#agent_wrap').style.display = 'none';
}

function show_agent_logs()
{
	document.querySelector('#arequests_wrap').style.display = 'none';
	document.querySelector('#alogs_wrap').style.display = 'block';
	document.querySelector('#offices_wrap').style.display = 'none';
	document.querySelector('#agent_wrap').style.display = 'none';
}
