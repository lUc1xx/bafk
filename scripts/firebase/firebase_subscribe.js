const messaging = firebase.messaging();

messaging.usePublicVapidKey('BKjh1n565rlzKYmQIXyKEvh8yMmeA1uXUbfa5z9RvMea-fO7WjEMGxmBS3lOARU732mwkDkpYz_avQFNh9JUtp0');

messaging.onTokenRefresh(function () {
    messaging.getToken().then(function (refreshedToken) {
        console.log('Token refreshed.');
        setTokenSentToServer(false);
        sendTokenToServer(refreshedToken);
        resetUI();
    }).catch(function (err) {
        console.log('Unable to retrieve refreshed token ', err);
    });
});


function resetUI() {
    messaging.getToken().then(function (currentToken) {
        if (currentToken) {
            sendTokenToServer(currentToken);
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
            setTokenSentToServer(false);
        }
    }).catch(function (err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });
}


function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        $.post(
            "/ajax.php",
            {
                block: 'worktable',
                action: 'sendTokenToServer',
                token: currentToken
            },
            onAjaxSuccess
        );

        function onAjaxSuccess(data) {
            console.log(data);
        }
        setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again ' +
            'unless it changes');
    }
}

function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
}

function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}


function requestPermission() {
    console.log('Requesting permission...');
    messaging.requestPermission().then(function () {
        console.log('Notification permission granted.');
        resetUI();
    }).catch(function (err) {
        console.log('Unable to get permission to notify.', err);
    });
}

function deleteToken() {
    messaging.getToken().then(function (currentToken) {
        messaging.deleteToken(currentToken).then(function () {
            console.log('Token deleted.');
            setTokenSentToServer(false);
            resetUI();
        }).catch(function (err) {
            console.log('Unable to delete token. ', err);
        });
    }).catch(function (err) {
        console.log('Error retrieving Instance ID token. ', err);
    });
}

resetUI();

requestPermission();