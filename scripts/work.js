let keydownEvents = new Array();
let mouseEvents = new Array();
let openPage = true;

let wasKeyDown = false;
let wasKeyClick = false;
let wasMouseEvent = false;

document.addEventListener("keydown", (event) => {
    wasKeyDown = true;
    /*keydownEvents.push({
        key: event.key,
        code: event.code,
        ctrlKey: event.ctrlKey,
        shiftKey: event.shiftKey,
        altKey: event.altKey,
        targetName: event.target.name,
        targetId: event.target.hasAttribute('id') ? event.target.getAttribute('id') : '',
        time: event.timeStamp
    });*/
});

document.addEventListener("click", (event) => {
    wasMouseEvent = true;
    /*mouseEvents.push({
        type: event.type,
        screenX: event.screenX,
        screenY: event.screenY,
        clientX: event.clientX,
        clientY: event.clientY,
        offsetX: event.offsetX,
        offsetY: event.offsetY,
        time: event.timeStamp,
        targetName: event.target.hasAttribute('name') ? event.target.name : '',
        targetId: event.target.hasAttribute('id') ? event.target.getAttribute('id') : '',
    });*/
});

document.addEventListener("mousemove", (event) => {
    wasMouseEvent = true;
    /*mouseEvents.push({
        type: event.type,
        screenX: event.screenX,
        screenY: event.screenY,
        clientX: event.clientX,
        clientY: event.clientY,
        offsetX: event.offsetX,
        offsetY: event.offsetY,
        time: event.timeStamp,
        targetName: '',
        targetId: ''
    });*/
});

function collectEvents() {
    let data = JSON.stringify({
        login: getCookie('login'),
        token: getCookie('token'),
        openPage: openPage,
        location: location.pathname,
        keydownEvents: keydownEvents,
        mouseEvents: mouseEvents,
        wasMouseEvent: wasMouseEvent,
        wasKeyDown: wasKeyDown
    });

    let url = "http://" + location.hostname + ":9777";
    //console.log('sending data:', data);
    //ajaxJson('http://b2cbafk.club:9777','POST', data, true, function(req)
    ajaxJson(url,'POST', data, true, function(req)
    {
        var tmp_page = req.responseText;
        let text_ = '';
        var Item = eval("obj = " + tmp_page);
        if (Item['status'] == 'ok')
        {
            openPage = false;
            wasKeyDown = false;
            wasMouseEvent = false;
            text_ = Item['data'];
            //console.log(text_);
        }
        else
        {
            text_= Item['msg'];
            console.log(text_);
        }
    },
    function(req)
    {
        let text_= "Ошибка исполнения. Свяжитесь с администратором!";
        console.log(text_);
    });

    /*postData('http://b2cbafk.club:9777', {
        'login': getCookie('login'),
        'token': getCookie('token'),
        location: location.pathname,
        keydownEvents: keydownEvents,
        mouseEvents: mouseEvents
    })
        .then((data) => {
            console.log(data); // JSON data parsed by `response.json()` call
        });*/
//    console.table(keydownEvents);
    keydownEvents = new Array();
//    console.table(mouseEvents);
    mouseEvents = new Array();
}

//let timerCollectEvents = setInterval(collectEvents, 2000);


async function postData(url = '', data = {}) {

    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'no-cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            //'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
}


function ajaxJson(url, method, param, async, onsuccess, onfailure)
{
    var xmlHttpRequest = new XMLHttpRequest();
    var callback = function(r)
    {
        r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r));
    };

    if(async)
    {
        xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } }
    }
    xmlHttpRequest.open(method, url, async);
    if(method == 'POST') { xmlHttpRequest.setRequestHeader('Content-Type', 'application/json'); }
    xmlHttpRequest.send(param);
    if(!async) { callback(xmlHttpRequest); }
}



////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
