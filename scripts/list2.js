var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

//window.onload = list_main;
window.onload = update_all_data;

var sel_status_i = 0;
var sel_office_i = 0;
var sel_staff_i = 0;

var sort = 0;
var sort_by = false;

function update_all_data()
{
	list_main();
	get_tasks_count();
	get_events_count();
}

function sort_list(td)
{
	var list_table = document.querySelector('#list_table');
	var tabhead = list_table.querySelector('div[class="row_head"]');
	var tabrow = list_table.querySelectorAll('div[class*="row_odd"], div[class*="row_even"]');
	
	if (td == sort)
		sort_by = !sort_by;
	sort = td;

	var colhead = tabhead.querySelectorAll('div[class="list_cell"]');
	
	for (var c = 0; c < colhead.length; c++)
	{
		//console.log(ddd[c].innerHTML);
		var child = colhead[c].childNodes[0];
		if (child == null)
			continue;
		if (c == parseInt(td))
		{
			if (sort_by)
			{
				//console.log('up');
				child.className = 'sort_up';
				//ddd[td].style.background = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';
			}
			else
			{
				child.className = 'sort_d';
				//console.log('down');
				//ddd[td].style.background = 'url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=") no-repeat right 55%;';
			}
		}
		else
			child.className = 'sort_no';
	}
	
	if (td == 0)
		td = 1;
	
	
	var list_array = new Array();
	for (var c = 0; c < tabrow.length; c++)
	{
		var colrow = tabrow[c].querySelectorAll('div[class="list_cell"]');
		var t = colrow[td].innerHTML;
		
		if (td == 9)
		{
			//console.log('hit');
			if (/\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d/.test(t))
			{
				var str;
				if (/,/.test(t))
				{
					var m = t.split(', ');
					
					var n0 = m[0].split(' ');
					var n1 = m[1].split(' ');
					
					var str0 = n0[0] + "T" + n0[1] + ".000";
					var str1 = n1[0] + "T" + n1[1] + ".000";
					
					var str0n = Date.parse(str0);
					var str1n = Date.parse(str1);
					
					//console.log(str0n + ', ' + str1n);
					
					if (sort_by)
						str = (str0n < str1n) ? str0n : str1n;
					else
						str = (str0n > str1n) ? str0n : str1n;
				}
				else
				{
									
					//var regexp = new RegExp("(\d\d\d\d-\d\d-\d\d) (\d\d:\d\d:\d\d)");
					var n0 = t.match(/(\d\d\d\d-\d\d-\d\d) (\d\d:\d\d:\d\d)/);
					//console.log(matches);
					
					var str_ = n0[1] + "T" + n0[2] + ".000";
					
					
					
					str = Date.parse(str_);
					
					//console.log(str);
				}
				t = str;
			}
			else
				t = parseInt("9535112000000");
			console.log(t);
		}
		else
			t = parseInt(colrow[td].innerHTML);
		/*else if (colrow[td].innerHTML == '')
		{
			t = 
		}*/
		
		var tmp_a = {};
			//tmp_a.key = i;
			tmp_a.sort_by = t;
			tmp_a.data = tabrow[c];
		
		list_array.push(tmp_a);
		//list_array.push(new Array(t, tabrow[c]));
		list_table.removeChild(tabrow[c]);
	}
	
	//console.log(tabrow.length);
	/*console.log(list_array);
	*/
	if (sort_by)
		list_array.sort(dIncrease);
	else
		list_array.sort(dDecrease);
			
	console.log(list_array);
	
	for (var c = 0; c < list_array.length; c++)
	{
		list_table.appendChild(list_array[c].data);
	}
}

function dIncrease(i, ii)
{ // По возрастанию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function dDecrease(i, ii)
{ // По убыванию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function sIncrease(i, ii)
{ // По возрастанию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function sDecrease(i, ii)
{ // По убыванию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function filterApply()
{
	var dfrom = document.querySelector('#dfrom').value;
	var dto = document.querySelector('#dto').value;
	
	var s_status = document.querySelector('#select_status');
	sel_status_i = s_status.selectedIndex;
	s_status = s_status.options[s_status.selectedIndex].value;
	
	var s_office = document.querySelector('#select_office');
	sel_office_i = s_office.selectedIndex;
	s_office = s_office.options[s_office.selectedIndex].value;
	
	var s_staff = document.querySelector('#select_staff');
	sel_staff_i = s_staff.selectedIndex;
	s_staff = s_staff.options[s_staff.selectedIndex].value;
	
	console.log(s_status);
	console.log(s_office);
	console.log(s_staff);
	
	
	var filter = "&dfrom=" + dfrom + "&dto=" + dto + "&s_status=" + s_status + "&s_office=" + s_office + "&s_staff=" + s_staff;
	
	var t = document.querySelector('#list_table');
	var r1 = t.querySelectorAll('.row_odd');
	for (var key = 0; key < r1.length; key++)
		t.removeChild(r1[key]);
	var r2 = t.querySelectorAll('.row_even');
	for (var key = 0; key < r2.length; key++)
		t.removeChild(r2[key]);
	//var h = t.querySelector('.row_head');
	//t.innerHTML = "";
	//t.appendChild(h);

	setCookie('list_dfrom', dfrom, { expires: 36000000, path:"/"});
	//setCookie('list_dto', dto, { expires: 3600*8, path:"/"});
	setCookie('list_s_status', s_status, { expires: 36000000, path:"/"});
	setCookie('list_sel_status_i', sel_status_i, { expires: 36000000, path:"/"});
	setCookie('list_sel_office_i', sel_office_i, { expires: 36000000, path:"/"});
	setCookie('list_sel_staff_i', sel_staff_i, { expires: 36000000, path:"/"});
	setCookie('list_s_office', s_office, { expires: 36000000, path:"/"});
	setCookie('list_s_staff', s_staff, { expires: 36000000, path:"/"});
	get_list(filter);
}

function ch_list_stat(el)
{
	var sel = el.options[el.selectedIndex].value;
	var sel2 = document.querySelector('#select_staff');
	
	var opt = sel2.querySelectorAll('option');
	if (sel == '-1')
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].style.display = '';
			opt[key].selected = false;
		}
	}
	else
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].selected = false;
			if (opt[key].className == ('office_' + sel))
				opt[key].style.display = '';
			else
				opt[key].style.display = 'none';
		}
	}
	filterApply();
}

function list_main()
{
	sel_status_i = 1;
	var dto = document.querySelector('#dto');
	var dfrom = document.querySelector('#dfrom');
	var select_status = document.querySelector('#select_status');
	var select_status_val = select_status.options[select_status.selectedIndex].value;
	if (getCookie('list_dfrom'))
	{
		dfrom.value = getCookie('list_dfrom');
	}
	/*if (getCookie('list_dto'))
	{
		dto.value = getCookie('list_dto');
	}*/
	
	/*console.log(select_status_val);
	if (getCookie('list_s_status'))
	{
		select_status_val = getCookie('list_s_status');
		console.log(select_status_val);
		console.log(select_status);
		console.log(getCookie('list_sel_office_i'));
		select_status.options[getCookie('list_sel_office_i')].selected = true;
		console.log(select_status);
	}*/
	
	
	if (getCookie('list_back'))
	{
		sel_status_i = getCookie('list_sel_status_i');
		sel_office_i = getCookie('list_sel_office_i');
		sel_staff_i = getCookie('list_sel_staff_i');

		var s_status = document.querySelector('#select_status');
		s_status.options[sel_status_i].selected = true;
		
		var s_office = document.querySelector('#select_office');
		s_office.options[sel_office_i].selected = true;
			
		var s_staff = document.querySelector('#select_staff');
		s_staff.options[sel_staff_i].selected = true;
		
		setCookie('list_back', '', { expires: 1, path:"/"});
		
		
	}
	//else
		//get_list("&dfrom=" + dfrom.value + "&dto=" + dto.value + '&s_status=' + select_status_val);
	
	filterApply();
}


function get_list(filter)
{
	//tabl = document.querySelector('#res_table');
	//row = tabl.querySelectorAll('table');
	//console.log(row.length);

	var data = "block=list&action=get_list" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var totalBytes  = req.getResponseHeader('Content-length');
		console.log(totalBytes);
		var lll = tmp_page.length;
		console.log(lll);
		//console.log(tmp_page);
		
		//document.querySelector('#list_table').innerHTML = "";
		
		var Item = eval("obj = " + tmp_page);
		if (Item["status"] == 'OK')
		{
			var data = Item["data"];
			add_data(data);
		}
		
		//document.querySelector('#main');
	});
}

function add_data(mass)
{
	var row_odd = "row_odd";	//нечет
	var row_even = "row_even";	//чет
	var c = 0;
	var tmp_add = "";
	for (var key in mass)
	{
		c++;
		//console.log(key + " ::: " + Item);
		
		var row_c = (c & 1) ? row_odd : row_even;
		tmp_add += '<div class="' + row_c + '">';
		
		var row = mass[key];
		
		tmp_add += '<div class="list_cell">' + row['date_add'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['id'] + '</div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row['lastname'] + ' ' + row['firstname'] + ' ' + row['middlename'] + '</a></div>';
		
		var tmp_sum = row['sum'];
		tmp_sum = tmp_sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		
		var ssum = tmp_sum + ((row['sum_val'] == "rub") ? " р." : "");
		tmp_add += '<div class="list_cell" style="text-align:right; padding-right:10px;">' + ssum + '</div>';
				
		tmp_add += '<div class="list_cell">' + row['city'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['source'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['status_desc'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['officce_desc'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['manager_desc'] + '</div>';
		
		
		
		/*for (var key2 in row)
		{
			if ((key2 == 'id') || (key2 == 'firstname') || (key2 == 'middlename') || (key2 == 'sum_val') || (key2 == 'status') || (key2 == 'office') || (key2 == 'manager') || (key2 == 'source_param'))
				continue;
			
			if (key2 == 'debug')
			{
				console.log('debug:' + row[key2]);
				continue;
			}
			
			if ((key2 == 'meet_event') || (key2 == 'call_event'))
				continue;
			
			if ((key2 == 'type'))
				continue;
			
			if (key2 == 'lastname')
			{
				tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row[key2] + ' ' + row['firstname'] + ' ' + row['middlename'] + '</a></div>';
			}
			else if (key2 == 'source')
			{
				//tmp_add += '<div class="list_cell">' + row[key2] + ':' + row['source_param'] + '</div>';
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
			}
			else if (key2 == 'sum')
			{
				var tmp_sum = row[key2];
				tmp_sum = tmp_sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				
				var ssum = tmp_sum + ((row['sum_val'] == "rub") ? " р." : "");
				tmp_add += '<div class="list_cell" style="text-align:right; padding-right:10px;">' + ssum + '</div>';
			}
			else
				tmp_add += '<div class="list_cell">' + row[key2] + '</div>';
		}*/
		
		var ev = '';
		if ('meet_event' in row) 
		{
			if (row['meet_event'])
				ev += row['meet_event'];
		}
		
		if ('call_event' in row)
		{
			if (row['call_event'])
			{
				if (ev != '')
					ev += ', ';
				ev += row['call_event'];
			}
		}
		
		tmp_add += '<div class="list_cell">' + ev + '</div></div>';
		
	}
	
	document.querySelector('#list_table').innerHTML += tmp_add;
	
	var s_status = document.querySelector('#select_status');
	s_status.options[sel_status_i].selected = true;
	
	var s_office = document.querySelector('#select_office');
	s_office.options[sel_office_i].selected = true;
		
	var s_staff = document.querySelector('#select_staff');
	s_staff.options[sel_staff_i].selected = true;

}

function gsearch()
{
	var filter = document.querySelector('#search_input').value;
	
	if(filter.length < 3)
		return;
	
	var data = "block=list&action=show_result&val=" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var t = document.querySelector('#list_table');
			var h = t.querySelector('.row_head');
			t.innerHTML = "";
			t.appendChild(h);
			add_data(Item['data']);
		}
		//console.log(Item);
	});
}

function calc_result(filter)
{
	if(filter.length < 3)
		return;
	var data = "block=list&action=calc_result&val=" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var sb = document.querySelector('#search_button');
			sb.innerHTML = "Поиск(" + Item['count'] + ")";
		}
		//console.log(Item);
	});
}

function fio_filter(filter)
{
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[2].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
}

//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds