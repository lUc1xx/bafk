var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;
window.onload = update_all_data;
window.quasarConfig = {
    brand: { // this will NOT work on IE 11
        //primary: '#e46262',
        // ... or all other brand colors
    },
    notify: {}, // default set of options for Notify Quasar plugin
    loading: {}, // default set of options for Loading Quasar plugin
    loadingBar: {
        skipHijack: true
        //position: 'left'
    }, // settings for LoadingBar Quasar plugin
    // ..and many more
}

function update_all_data()
{
    get_events_count();
    get_tasks_count();


    Quasar.lang.set(Quasar.lang.ru);
    new Vue({
        el: '#mapp',
        data: function () {
            return {
                tab: 'dailyConv',
                dailyConvTable : {
                    loading: false,
                    pagination: {
                        rowsPerPage: 0
                    },
                    columns: [
                        {
                            name: 'date',
                            required: true,
                            label: 'Дата',
                            align: 'left',
                            field: 'date',
                            sortable: true
                        },
                        { name: 'incoming', align: 'center', label: 'Поступило', field: 'incoming', sortable: true },
                        { name: 'closed', label: 'Закрыто', field: 'closed', sortable: true, style: 'width: 10px' },
                        { name: 'closedAll', label: 'Закрыто всего', field: 'closedAll' },
                        { name: 'conv', label: 'Конверсия', field: 'conv' },
                    ],
                    data: [
                        {
                            name: 'Frozen Yogurt',
                            calories: 159,
                            fat: 6.0,
                            carbs: 24,
                            protein: 4.0,
                            sodium: 87,
                            calcium: '14%',
                            iron: '1%'
                        },
                        {
                            name: 'Ice cream sandwich',
                            calories: 237,
                            fat: 9.0,
                            carbs: 37,
                            protein: 4.3,
                            sodium: 129,
                            calcium: '8%',
                            iron: '1%'
                        },
                        {
                            name: 'Eclair',
                            calories: 262,
                            fat: 16.0,
                            carbs: 23,
                            protein: 6.0,
                            sodium: 337,
                            calcium: '6%',
                            iron: '7%'
                        },
                        {
                            name: 'Cupcake',
                            calories: 305,
                            fat: 3.7,
                            carbs: 67,
                            protein: 4.3,
                            sodium: 413,
                            calcium: '3%',
                            iron: '8%'
                        },
                        {
                            name: 'Gingerbread',
                            calories: 356,
                            fat: 16.0,
                            carbs: 49,
                            protein: 3.9,
                            sodium: 327,
                            calcium: '7%',
                            iron: '16%'
                        },
                        {
                            name: 'Jelly bean',
                            calories: 375,
                            fat: 0.0,
                            carbs: 94,
                            protein: 0.0,
                            sodium: 50,
                            calcium: '0%',
                            iron: '0%'
                        },
                        {
                            name: 'Lollipop',
                            calories: 392,
                            fat: 0.2,
                            carbs: 98,
                            protein: 0,
                            sodium: 38,
                            calcium: '0%',
                            iron: '2%'
                        },
                        {
                            name: 'Honeycomb',
                            calories: 408,
                            fat: 3.2,
                            carbs: 87,
                            protein: 6.5,
                            sodium: 562,
                            calcium: '0%',
                            iron: '45%'
                        },
                        {
                            name: 'Donut',
                            calories: 452,
                            fat: 25.0,
                            carbs: 51,
                            protein: 4.9,
                            sodium: 326,
                            calcium: '2%',
                            iron: '22%'
                        },
                        {
                            name: 'KitKat',
                            calories: 518,
                            fat: 26.0,
                            carbs: 65,
                            protein: 7,
                            sodium: 54,
                            calcium: '12%',
                            iron: '6%'
                        }
                    ]
                }
            }
        },
        mounted() {
            console.log('mountes');
            this.loaddata();
        },
        methods : {
            loaddata () {
                console.log('loaddata');
                this.dailyConvTable.loading = true;
                let url = `/ajax.php?block=analitics&action=getClosedConv`;
                //let response = await fetch(url);
                //alert(response);
                fetch(url)
                    .then(response => response.json())
                    .then(data => {
                        //console.log(data)
                        if(data.status == 'ok')
                            this.setdata(data.data);
                        this.dailyConvTable.loading = false;
                    });
            },
            setdata(d) {
                console.log('update data');
                console.table(d);
                this.dailyConvTable.data = d;
                console.log(d);
            }
        }
    })
}