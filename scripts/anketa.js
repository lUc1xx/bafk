var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

window.onload = update_all_data;
var old_input_value = new Array();
var old_radio_value = new Array();
var sec_input_value = new Array();
var num_file = 0;

var offer = new Array();

var last_wind = '';

var checks_prom = new Array();
var point_list = new Array();
checks_prom['CWICreditHistoryIndividual'] = "НБКИ-отчет для ФЛ";
checks_prom['CWICreditRatingIndividual'] = "Кредитный рейтинг для ФЛ";
checks_prom['CWIOkbReportPrivate'] = "Аналитический отчет";
checks_prom['CWICreditHistoryPrivate'] = "Кредитная история физического лица НБКИ";

function back2list()
{
	if(anketa_type == 'cooperate')
		location.href = "/listsotr/";
	else
	{
		setCookie('list_back', '1', { expires: 5, path:"/"});
		location.href = "/list/";
	}
}

function contract()
{
	//var data = "block=anketa&action=get_contract&id=" + anketa_id;
	document.location.href='/ajax.php?block=anketa&action=get_contract&id=' + anketa_id;
	/*ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		//location = tmp_page;
		//window.open(req,'','…');
		return req.responseText;
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});*/
}

function show_photo_wrap()
{
	document.getElementById('wrap').style.display = 'block';
	document.getElementById('make_photo_wrap').style.display = 'block';
}

function show_za_files()
{
	document.getElementById('wrap').style.display = 'block';
	document.getElementById('za_files_wrap').style.display = 'block';
	
	var wind = document.querySelector('#za_files_wrap');
	wind.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
	
	var data = "block=anketa&action=get_za_files&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var filemass = Item['files'];
			var tab = document.createElement('table');
				tab.border = '1px solid black';
			for (var k in filemass)
			{
				var tr = document.createElement('tr');
					var td = document.createElement('td');
						td.innerHTML = k + 1;
						td.innerHTML += '.';
					tr.appendChild(td);
					
					td = document.createElement('td');
						td.innerHTML = filemass[k]['name'];
					tr.appendChild(td);
					
					
					td = document.createElement('td');
						td.innerHTML = '<a href="/files/' + anketa_id + '/' + filemass[k]['id'] + '" target=_blank>Скачать</a>';
					tr.appendChild(td);
				tab.appendChild(tr);
			}	
			wind.appendChild(tab);
			
			wind.innerHTML += '<br><br>';
			
			wind.innerHTML += '<button onclick="javascript:download_all_za();">Скачать все файлы</button>'
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function download_all_za()
{
	var wind = document.querySelector('#za_files_wrap');
	var table = wind.querySelector('table');
	var tr = table.querySelectorAll('tr');
	
	for (var i = 0; i < tr.length; i++)
	{
		var link = tr[i].querySelector('a');
		link.setAttribute('download', true);
		link.click();
	}
}

function act()
{
	document.location.href='/ajax.php?block=anketa&action=get_act&id=' + anketa_id;
}

function contract_ur()
{
	document.location.href='/ajax.php?block=anketa&action=get_contract_ur&id=' + anketa_id;
}
var bnum = 0;

function set_favorite(add)
{
	var data = "block=anketa&action=set_favorite&id=" + anketa_id + "&add=" + add;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//location.reload();
			var favorite_b = document.querySelector('#favorite_b');
			add = (add == 1) ? 0 : 1;
			favorite_b.setAttribute('onclick', 'javascript:set_favorite(' + add + ');');
			if (add)
				favorite_b.querySelector('button').innerHTML = "Добавить в избранное";
			else
				favorite_b.querySelector('button').innerHTML = "Удалить из избранного";
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function send2ozs()
{
	if (confirm("Отправить в офисы ОЗС?"))
	{
		var data = "block=anketa&action=send2ozs&id=" + anketa_id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				location.reload();
			}
			else 
			{
				alert(Item['msg']);
			}
		});
	}
}

function sent2fa()
{
	if (confirm("Отправить анкету на заключительный анализ? \n\nУбедитесь что все необходимые для анализа файлы помечены галочкой!"))
	{
		var data = "block=anketa&action=sent2fa&id=" + anketa_id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				location.reload();
			}
			else 
			{
				alert(Item['msg']);
			}
		});
	}
}

function fin_fa()
{
	var el = document.querySelector('#finfasel');
	var sel = el.options[el.selectedIndex].value;
	
	if (sel == "-1")
	{
		alert("Выберите статус завершения анализа!");
		return;
	}
	
	if (confirm("Завершить Анализ?"))
	{
		var data = "block=anketa&action=fin_fa&id=" + anketa_id + "&st=" + sel;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				location.reload();
			}
			else 
			{
				alert(Item['msg']);
			}
		});
	}
}

function show_res_fssp(e, id)
{
	console.log(id);
	var target = e.target;

	//var tooltip = target.getAttribute('data-tooltip');
	//if (!tooltip) return;

	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	

	var tmp_str = '';
	tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';

	tooltipElem.id = 'fssp_res';
	tooltipElem.style.width = '900px';
	tooltipElem.style.height = '600px';
	tooltipElem.style.overflow = 'auto';
	//var promoney_id = target.getAttribute('promoney_id');
	tmp_str += 'Ошибка';
	
	var data = "&block=anketa&action=show_res_fssp&id=" + id;
	console.log(data);

	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'];
			tmp_str = '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			
			var result = data['response']['result'][0]['result'];
			
			if ((result == null) || (result == ''))
				tmp_str += 'Не найдено данных';
			else
			{
				//tmp_str += result;
				for (var key in result)
				{
					var res = result[key];
					tmp_str += 'ФИО: ' + res['name'] + '<br>';
					tmp_str += 'Детали: ' + res['details'] + '<br>';
					tmp_str += 'Причина: ' + res['subject'] + '<br><br>';
				}
			}
			tooltipElem.innerHTML = tmp_str;
		}
		else 
		{
			alert(Item['msg']);
			tooltipElem.innerHTML = tmp_str;
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
		tooltipElem.innerHTML = tmp_str;

	});
			
	
	//tooltipElem.innerHTML = tmp_str;
	document.body.appendChild(tooltipElem);


	var availWidth = window.screen.availWidth;
	var availHeight = window.screen.availHeight;
	//alert(tooltipElem.offsetWidth);
	
	tooltipElem.style.top = (availHeight/2 - 325) + 'px';
	tooltipElem.style.left = (availWidth/2 - 450) + 'px';


	showingTooltip2 = tooltipElem;
					
}

function format_numbers(inp)
{
	var num = inp.value.replace(/\D+/g,"");
	inp.value = num.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	
	if (inp.name == 'sum')
	{
		var msum = document.querySelector('input[name="field163"]');
		msum.value = inp.value;
		if (inp.value != '')
		{
			if (document.querySelector('#span_field163'))
				document.querySelector('#span_field163').style.display = 'none';
			msum.style.display = '';
		}
	}
}

function contract_wroted()
{
	if (confirm("Контракт заключен?"))
	{
		var data = "block=anketa&action=contract_wroted&id=" + anketa_id;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				location.reload();
			}
			else 
			{
				alert(Item['msg']);
			}
		});
	}
}

function add_bank_send_fields()
{
	var blw = document.querySelector("#bank_add_wrap");
	var div = document.createElement('div');
		
					
		var inp_b = document.createElement('input');
			inp_b.name = 'bank_name';
			inp_b.id = 'bank_send_id_' + bnum;
			inp_b.size = '50';
			inp_b.setAttribute('class', 'fil2');
			inp_b.setAttribute('oninput', 'javascript:bank_help(this, "' + bnum + '");');
		div.appendChild(inp_b);
		
		div.innerHTML += '<i class="fil5"></i><ul name="rsl" class="fil2"></ul> ';
		
		var inp_s = document.createElement('input');
			inp_s.name = 'bank_summ';
			inp_s.size = '10';
		div.appendChild(inp_s);
		
		div.innerHTML += ' ';
	
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			b.setAttribute("onclick", "javascript:bank_send(this);");
		div.appendChild(b);
		
	blw.appendChild(div);
	bnum++;
}

function del_file(fid, fname, el)
{
	if (!confirm("Удалить файл " + fname + "?"))
		return;
	
	console.log(el.parentNode);
	var data = "block=anketa&action=del_file&id=" + anketa_id + "&fid=" + fid;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Файл ' + fname + ' удален!');
			el.parentNode.parentNode.removeChild(el.parentNode);
			var cc = document.querySelector('#files_num_s');
			cc.innerHTML = parseInt(cc.innerHTML) - 1;
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function set_for_za(el, fid)
{
	var val = (el.checked) ? 1 : 0;
	var old_color = window.getComputedStyle(el.parentNode).backgroundColor
	var data = "block=anketa&action=set_for_za&id=" + anketa_id + "&fid=" + fid + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			el.parentNode.style.backgroundColor = 'red';
		}
		setTimeout(function(){ch_color(el.parentNode, '');}, 1000);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_av4tm(el, fid)
{
	var val = (el.checked) ? 1 : 0;
	var old_color = window.getComputedStyle(el.parentNode).backgroundColor
	var data = "block=anketa&action=ch_av4tm&id=" + anketa_id + "&fid=" + fid + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.style.backgroundColor = 'lightgreen';
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			el.parentNode.style.backgroundColor = 'red';
		}
		setTimeout(function(){ch_color(el.parentNode, '');}, 1000);
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_color(b, c)
{
	b.classList.add('animated');
	b.style.backgroundColor = c;
}

function bank_help(el, num)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=bank_help&get='+txt.value + '&bnum=' + num, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function del_event(ev, el)
{
	console.log(el);
	console.log(el.parentNode);
	var sp = el.parentNode.querySelector('span').innerHTML;
	console.log(sp);

	var data = "block=anketa&action=save_event&id=" + anketa_id + "&" + ev + "=del&ev_time=" + sp;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Событие удалено!');
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function reg_help(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=reg_help&get='+txt.value + '&rname=' + el.name, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function reg_help_obr(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=reg_help_a&get='+txt.value + '&rname=' + el.name, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function add_details(id, Item)
{
	if (document.querySelector('#details_' + id))
	{
		var node = document.getElementById("details_" + id);
		if (node.parentNode) {
		  node.parentNode.removeChild(node);
		}
		return;
	}
	console.log(id);
	var row = document.querySelector('#row_prb_' + id);
	console.log(row);
	
	var tmp = "<br>Продукт <b>" + Item['product_name'] + '</b> от банка <i>' + Item['bank_name'] + "</i>";
		tmp += "<br>Минимальная ставка " + Item['min_percent'] + '%, П/Д ' + Item['pd'] + "%, LTV " + Item['discont'] + "%. ";
		if (Item['res'] == 1)
			tmp += "Кредитует нерезидентов. ";
		if (Item['cr4other'] == '1')	//not use in search!
			tmp += "Кредитует \"улицу\".<br>";
		else if (Item['cr4other'] == '0')
			tmp += "Не кредитует \"улицу\".<br>";
		else //if (Item['res'] == 1)
			tmp += "<br>";
		tmp += "Максимальный возраст на момент погашения м/ж : " + Item['max_end_age_m'] + " / " + Item['max_end_age_w'] + " лет<br>";
		tmp += "Максимальный размер выдаваемого кредита: " + Item['max_credit'] + " руб. Минимальный доход: " + Item['min_income'] + " руб.<br>";
		tmp += "Описание:<br>" + Item['description'];
	console.log(tmp);
	
	var div = document.createElement('div');
		div.id = "details_" + id;
		div.innerHTML = tmp;
	
	var nextel = row.nextSibling;
	if (nextel)
		row.parentNode.insertBefore(div,nextel);
	else
		row.parentNode.appendChild(div);
}

function add_details_m(id, Item)
{
	if (document.querySelector('#details_' + id))
	{
		var node = document.getElementById("details_" + id);
		if (node.parentNode) {
		  node.parentNode.removeChild(node);
		}
		return;
	}
	console.log(id);
	var row = document.querySelector('#row_prb_' + id);
	console.log(row);
	
	var tmp = "<br>Продукт <b>" + Item['product_name'] + '</b> от банка <i>' + Item['bank_name'] + "</i>";
		tmp += "<br>Минимальная ставка " + Item['min_percent'] + '%, П/Д ' + Item['pd'] + "%, LTV " + Item['discont'] + "%. ";
		if (Item['res'] == 1)
			tmp += "Кредитует нерезидентов. ";
		if (Item['cr4other'] == '1')	//not use in search!
			tmp += "Кредитует \"улицу\".<br>";
		else if (Item['cr4other'] == '0')
			tmp += "Не кредитует \"улицу\".<br>";
		else //if (Item['res'] == 1)
			tmp += "<br>";
		tmp += "Максимальный возраст на момент погашения м/ж : " + Item['max_end_age_m'] + " / " + Item['max_end_age_w'] + " лет<br>";
		tmp += "Максимальный размер выдаваемого кредита: " + Item['max_credit'] + " руб. Минимальный доход: " + Item['min_income'] + " руб.<br>";
		tmp += "Описание:<br>" + Item['description'];
	console.log(tmp);
	
	var div = document.createElement('div');
		div.id = "details_" + id;
		div.innerHTML = tmp;
	
	var nextel = row.nextSibling;
	console.log(nextel);
	if (nextel)
		row.parentNode.insertBefore(div,nextel);
	else
		row.parentNode.appendChild(div);
}

function show_detail(el)
{
	var prbid = el.replace("prod", "");
	var data = "block=anketa&action=show_detail&id=" + anketa_id + "&prbid=" + prbid;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);

		console.log(Item['data']);
		if (Item['status'] == 'ok')
		{
			add_details(prbid, Item['data']);
		}
		else 
		{
			alert(Item['msg']);
		}
		
		/*var tmp_page = req.responseText;
		//document.querySelector('#prod_detail').style.overflow = "auto";
		document.querySelector('#prod_detail').innerHTML = "";
		var Item = eval("obj = " + tmp_page);
		//alert(tmp_page);
		document.querySelector('#prod_detail').innerHTML += "<br>Продукт <b>" + Item['product_name'] + '</b> от банка <i>' + Item['bank_name'] + "</i>";
		document.querySelector('#prod_detail').innerHTML += "<br>Минимальная ставка " + Item['min_percent'] + '%, П/Д ' + Item['pd'] + "%, LTV " + Item['discont'] + "%. ";
		if (Item['res'] == 1)
			document.querySelector('#prod_detail').innerHTML += "Кредитует нерезидентов. ";
		if (Item['cr4other'] == '1')	//not use in search!
			document.querySelector('#prod_detail').innerHTML += "Кредитует \"улицу\".<br>";
		else if (Item['cr4other'] == '0')
			document.querySelector('#prod_detail').innerHTML += "Не кредитует \"улицу\".<br>";
		else //if (Item['res'] == 1)
			document.querySelector('#prod_detail').innerHTML += "<br>";
		document.querySelector('#prod_detail').innerHTML += "Максимальный возраст на момент погашения м/ж : " + Item['max_end_age_m'] + " / " + Item['max_end_age_w'] + " лет<br>";
		document.querySelector('#prod_detail').innerHTML += "Максимальный размер выдаваемого кредита: " + Item['max_credit'] + " руб. Минимальный доход: " + Item['min_income'] + " руб.<br>";
		
		document.querySelector('#prod_detail').innerHTML += "Описание:<br>" + Item['description'];

		show('block');*/
	});
}

function show_detail_m(el)
{
	var prbid = el.replace("prod", "");
	var data = "block=anketa&action=show_detail&id=" + anketa_id + "&prbid=" + prbid;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			add_details_m(prbid, Item['data']);
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function contract_prepare()
{
	document.getElementById('window_cp').style.display = 'block';
	document.getElementById('wrap').style.display = 'block';
}

function show_prb()
{
	var data = "block=anketa&action=show_prb&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show_miss_products(Item['miss_mass'], Item['miss_mass2']);
			document.getElementById('window_prb').style.display = 'block';
			document.getElementById('wrap').style.display = 'block';
			//if (parseInt(Item['len']) > 0)
			//{
				show_products(Item['data']);
			//}
			//else
		}
		else 
		{
			alert(Item['msg']);
		}
	});
}

var hide_products = false;
var view_b = 'Режим демонстрации';
if (getCookie2('bank_view') != undefined)
	view_b = getCookie2('bank_view');

function ch_view_b(b)
{
	var max_p_t = document.querySelectorAll('span.max_p_t');
	//console.log(max_p_t);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Режим демонстрации';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;
		setCookie2('bank_view', 'Режим демонстрации', opt.expires);
		b.value = 'show';
		
		for (var i = 0; i < max_p_t.length; i++)
			max_p_t[i].style.display = '';
	}
	else
	{
		b.innerHTML = 'Режим работы';
		var opt = new Object();
		opt.expires = 3600*24*30*365;
		opt.path = "";
		opt.domain = "/";
		opt.secure = false;	
		setCookie2('bank_view', 'Режим работы', opt.expires);
		b.value = 'hide';
		
		for (var i = 0; i < max_p_t.length; i++)
			max_p_t[i].style.display = 'none';
	}
	var tmp_r2 = document.querySelector('#prod_result');
	var tmp_divs = tmp_r2.querySelectorAll('div > .res3');
	var tmp_divs_res12 = tmp_r2.querySelectorAll('div > .res12');
	for (var i = 0; i < tmp_divs.length; i++)
	{
		tmp_divs[i].style.display = (b.value == 'hide') ? "none" : "block";
		tmp_divs_res12[i].style.display = (b.value == 'hide') ? "none" : "block";
	}
}

function show_miss(el)
{
	if (el.innerHTML == 'Показать отклоненные')
	{
		el.innerHTML = 'Показать продукты';
		document.querySelector('#prod_result').style.display = "none";
		document.querySelector('#prod_miss').style.display = "block";
		document.querySelector('#prod_miss2').style.display = "block";
	}
	else
	{
		document.querySelector('#prod_result').style.display = "block";
		document.querySelector('#prod_miss').style.display = "none";
		document.querySelector('#prod_miss2').style.display = "none";
		el.innerHTML = 'Показать отклоненные';
	}
}

function show_miss_products(data, data2)
{
//	console.log(data);
	var field = new RegExp("field\\d+", "i");
	var wind = document.getElementById('prod_miss');
	var header_tmp = '<div class="prb_head">';
		header_tmp += "<div class='prbcell resm0'><div>Параметр</div></div>";
		header_tmp += "<div class='prbcell resm1'><div>Кол-во</div></div>";
		header_tmp += "</div>";
		wind.innerHTML = header_tmp;
	var count = 0;
	for (key in data)
	{
		//console.log(key + "::" + field);
		if (field.test(key) || (key == 'birth_day') || (key == 'sum'))
		{
			var v = data[key];
			//console.log(v);
			if (v.c > 0)
			{
//				console.log(v.desc + ":" + v.c);
				var bcgc = (count & 1) ? " prb_row1" : " prb_row0" ;
				var tmp_test = '<div class = "prb_row' + bcgc + '">';
				tmp_test += '<div class = "prbcell resm0">' + v.desc + '</div>';
				tmp_test += '<div class = "prbcell resm1">' + v.c + '</div>';
				tmp_test += "</div>";
				wind.innerHTML += tmp_test;
				count++;
			}
				
		}
	}
	
	wind = document.getElementById('prod_miss2');
	var header_tmp = '<div class="prb_head">';
		header_tmp += "<div class='prbcell resm20'><div>ID</div></div>";
		header_tmp += "<div class='prbcell resm21'><div>Продукт</div></div>";
		header_tmp += "<div class='prbcell resm22'><div>Банк</div></div>";
		header_tmp += "<div class='prbcell resm23'><div>Причины отклонения</div></div>";
		header_tmp += "<div class='prbcell resm24'><div>В работе</div></div>";
		header_tmp += "</div>";
		wind.innerHTML = header_tmp;
	
	count = 0;
	for (key in data2)
	{

		var v = data2[key];
		var bcgc = (count & 1) ? " prb_row1" : " prb_row0" ;
		var tmp_test = '<div class = "prb_row' + bcgc + '" id="row_prb_' + v.id + '">';
		tmp_test += '<div class = "prbcell resm20">' + v.id + '</div>';
		//tmp_test += '<div class = "prbcell resm21">' + v.product_name + '</div>';
		if (data2[key]['online_av'] && parseInt(data2[key]['online_av'])) {
			tmp_test += '<div class = "prbcell resm21" style="color: deepskyblue"><i id="prod' + v.id + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail_m(this.id)">' + v.product_name + '</i></div>';
		} else {
			tmp_test += '<div class = "prbcell resm21"><i id="prod' + v.id + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail_m(this.id)">' + v.product_name + '</i></div>';
		}
		tmp_test += '<div class = "prbcell resm22">' + v.bank_name_descr + '</div>';
		tmp_test += '<div class = "prbcell resm23">' + v.miss_values + '</div>';
		
		var tmp_id = parseInt(v.id);
		if (parseInt(v.prb_in_work) == 1)
			tmp_test += "<div class='prbcell resm24' style=\"cursor:pointer;\" onclick=\"javascript:del_prb2form(this, " + tmp_id + ");\"><div style=\"color:green;\">В работе</div></div>";
		else
			tmp_test += "<div class='prbcell resm24' style=\"cursor:pointer;\" onclick=\"javascript:add_prb2form(this, " + tmp_id + ");\"><div style=\"color:red;\">Не в работе</div></div>";
		
		tmp_test += "</div>";
		wind.innerHTML += tmp_test;
		count++;

	}
}

function copy_in_buf(text2copy)
{
	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
	
	if (iOS || is_safari)
	{
		fallbackCopyTextToClipboard(text2copy);
	}
	else if (navigator.clipboard)
	{
		navigator.clipboard.writeText(text2copy)
		.then(() => {
		alert('Скопировано');
		})
		.catch(err => {
		console.log('Something went wrong ', err);
		});
	}
	else
	{
		fallbackCopyTextToClipboard(text2copy);
	}
}

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Результат: ' + msg);
	alert('Скопировано сообщение ' + text);
  } catch (err) {
    console.error('Ошибка копирования: ', err);
  }

  document.body.removeChild(textArea);
}

function create_offer(el)
{
	
	//console.log(offer);
	var wind = document.getElementById('prod_result');
	var rows = wind.querySelectorAll('div[class*="prb_row"]');
	
	var offer_data = "";
	
	for (var i = 0; i < rows.length; i++)
	{
		var divs = rows[i].querySelectorAll('div[class*="prbcell"]');
		var vis = rows[i].querySelector('div > input');
		var id = divs[0].innerHTML;
		id = parseInt(id.replace(".", ""));
		console.log(id);
		//console.log(vis);
		//console.log(vis.checked);
		
		if (!vis.checked)
		{
			//console.log(vis.checked);
			//console.log(offer.indexOf(id));
			//console.log(offer[id]);
			if (id in offer)
			{
				var tmp_pr = offer[id];
				
				if (offer_data == '')
					offer_data += "&offer=";
				else
					offer_data += "***end***";
				
				offer_data += "ofid=" + id;
				offer_data += ";;;name=" + tmp_pr[0];
				offer_data += ";;;lmin=" + tmp_pr[1];
				offer_data += ";;;lmax=" + tmp_pr[3];
				offer_data += ";;;pmin=" + tmp_pr[2];
				offer_data += ";;;pmax=" + tmp_pr[4];
				offer_data += ";;;vskm=" + tmp_pr[5];
				offer_data += ";;;minp=" + tmp_pr[6];
				offer_data += ";;;maxp=" + tmp_pr[7];
				
			}
		}
		
	}
	
	var data = "block=anketa&action=offer_send&id=" + anketa_id + offer_data;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//alert('Запись добавлена!');
			//blw.removeChild(el.parentNode);
			//get_banks_work();
			
			var url = 'http://prb.bafk.ru/o/' + Item['url'];
			//var url = 'http://prb.finanskredit.ru/o/' + Item['url'];
			var list_phones = Item['list_phones'];
			add_form_sent_offer(el, url, list_phones);
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
		
	//console.log(offer_data);
	
}

function add_form_sent_offer(el, url, list_phones)
{
	var target = el;

	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	

	var tmp_str = '';
	tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
	tooltipElem.id = 'send_prb_form';
	tooltipElem.style.width = '500px';
	tooltipElem.style.height = '200px';
	
	
	tmp_str += '<br><br><textarea id="send_prb_form_text" style="width:400px; height:100px; margin-right:50px; float:right;">';
	tmp_str += 'Ваш кредит одобрен! Условия: ' + url + ' . Предложение действует 3 рабочих дня. Тел: 84993508419';
	tmp_str += '</textarea>';
	
	tooltipElem.innerHTML = tmp_str;
	
	
	var sel = document.createElement('select');
		//sel.name = "selphone";
		sel.id = "selphone_prb";
		sel.setAttribute("style", "margin-top:10px; margin-left:50px; float:left;");
		//sel.setAttribute("onchange", "javascript:sel_phone(this);");
		var opt = document.createElement('option');
			opt.value = "-1";
			opt.innerHTML = "Выберите";
		sel.appendChild(opt);
	for (var key in list_phones)
	{
		var opt = document.createElement('option');
			opt.value = list_phones[key];
			opt.innerHTML = list_phones[key];
		sel.appendChild(opt);
	}
	tooltipElem.appendChild(sel);
	
	tmp_str = '<button style="margin-top:10px; margin-right:50px; float:right;" onclick="javascript:sms_send_prb(this);">Отправить СМС</button>';
	
	tooltipElem.innerHTML += tmp_str;
	document.body.appendChild(tooltipElem);

	var coords = target.getBoundingClientRect();

	var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
	if (left < 0) left = 0; // не вылезать за левую границу окна

	//var top = coords.top - tooltipElem.offsetHeight - 5;
	var top = coords.top + target.offsetHeight + 5;
	if (top < 0) { // не вылезать за верхнюю границу окна
	top = coords.top + target.offsetHeight + 5;
	}

	tooltipElem.style.left = left + 'px';
	tooltipElem.style.top = top + 'px';

	//showingTooltip = tooltipElem;
}

function sms_send_prb(el)
{
	var tmp_num = document.querySelector("#selphone_prb").value;
	
	if (tmp_num == '-1')
	{
		alert('Выберите номер!');
		return;
	}
	var tmp_text = document.querySelector("#send_prb_form_text").value;
	console.log(tmp_num);
	console.log(tmp_text);
	var data = "block=anketa&action=send_sms&id=" + anketa_id + "&tel=" + tmp_num + "&msg=" + tmp_text;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('СМС отправлена на номер ' + tmp_num);
			//show('none');
			document.body.removeChild(el.parentNode);
			reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show_products(data)
{
	offer.length = 0;
	
	if (getCookie2('bank_view') != undefined)
		view_b = getCookie2('bank_view');
	document.querySelector('#prod_result').style.display = "block";
	document.querySelector('#prod_miss').style.display = "none";
	//console.log('view_b :' + view_b);
	var tmp_c = document.querySelector('#prod_ctrl');
	console.log(tmp_c);
	console.log(tmp_c.style.display);
	var coords = tmp_c.getBoundingClientRect();
	console.log(coords);
	document.querySelector('#prod_ctrl').innerHTML = 'Доступные продукты: <button type="button" id="show_butt" onclick="javascript:change_view(this)" value="hide">Исключить выбранные</button> <button type="button" id="show_bank" onclick="javascript:ch_view_b(this)" value="' + ((view_b == "Режим работы") ? "hide" : "show") + '">' + view_b + '</button> <button type="button" onclick="javascript:print_doc()">Печать</button>';
	document.querySelector('#prod_ctrl').innerHTML += ' <button type="button" onclick="javascript:show_miss(this)">Показать отклоненные</button>';
	
	//if (staff_id_js == 1)
		document.querySelector('#prod_ctrl').innerHTML += ' <button type="button" onclick="javascript:create_offer(this)" data-tooltip="<font color=\'red\'>Внимание!</font><br>Предложение формируется только из видимых продуктов!">Сформировать предложение</button>';

	//document.querySelector('#prod_ctrl').innerHTML += '<br><div id="prb_message"></div>';
	var wind = document.getElementById('prod_result');
	var header_tmp = '<div class="prb_head">';
		header_tmp += "<div class='prbcell res0' style='cursor: pointer;' onclick='sort_pod(0)'><div>№</div></div>";
		header_tmp += "<div class='prbcell res1' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(1)'><div>Продукт</div></div>";
		header_tmp += "<div class='prbcell res2' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(2)'><div class = 'sort_up' style='width:55px;'>От, %</div></div>";
		header_tmp += "<div class='prbcell res3' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(3)'><div>Банк</div></div>";
		header_tmp += "<div class='prbcell res4' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(4)'><div style='width:65px;'>Платеж</div></div>";
		header_tmp += "<div class='prbcell res5' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(5)'><div style='width:55px;'>Лимит</div></div>";
		header_tmp += "<div class='prbcell res6' style='cursor: pointer;' onclick='sort_pod(6)'><div>Вид</div></div>";
		//header_tmp += "<div class='res7' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(7)'><div style='width:115px;'>Залог от 3 лица</div></div>";
		header_tmp += "<div class='prbcell res8' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(7)'><div style='width:90px;'>Макс. срок</div></div>";
		header_tmp += "<div class='prbcell res9' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(8)'><div data-tooltip='Возможный срок кредита в месяцах'>ВСК, мес</div></div>";
		header_tmp += "<div class='prbcell res10' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(9)'><div style='width:48px;'>Залог</div></div>";
		header_tmp += "<div class='prbcell res11' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(10)'><div>Поруч-во</div></div>";
		
		//header_tmp += "<div class='res9' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(9)'><div style='width:65px;'>Платеж</div></div>";
		//header_tmp += "<div class='resa' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(10)'><div style='width:55px;'>Лимит</div></div></div><br>";
		/*if (staff_office_type == 'main')
		{
			header_tmp += "<div class='prbcell res11' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(11)'><div style='width:65px;'>Платеж</div></div>";
			header_tmp += "<div class='prbcell res12' style='cursor: pointer; border-bottom: 1px dashed #000080;' onclick='sort_pod(12)'><div style='width:55px;'>Лимит</div></div>";
		
		}*/
		
		header_tmp += "<div class='prbcell res12'><div>В работу</div></div>";
		header_tmp += "</div>";
		wind.innerHTML = header_tmp;
		var count = 0;
		
		var zp = document.querySelector('input[name="field75"]').value.replace(/\D+/g,"");
		if (zp == '')
			document.querySelector('#prod_ctrl').innerHTML += '<br><font color="red">Не заполнен Совокупный доход!</font>';
		zp = (zp != '') ? parseInt(zp) : 0;
		
		var cred_sv = document.querySelector('input[name="sum"]').value.replace(/\D+/g,"");
		if (cred_sv == '')
			document.querySelector('#prod_ctrl').innerHTML += '<br><font color="red">Не заполнена сумма!</font>';
		cred_sv = (cred_sv != '') ? parseInt(cred_sv) : 0;
		
		var inc_4_cre = document.querySelector('input[name="field6"]').value.replace(/\D+/g,"");
		if (inc_4_cre == '')
			document.querySelector('#prod_ctrl').innerHTML += '<br><font color="red">Не заполнен максимальный платеж!</font>';
		inc_4_cre = (inc_4_cre != '') ? parseInt(inc_4_cre) : 0;
		
		var pkv = inc_4_cre;
	
		var a_skred = document.querySelector('input[name="field94"]').value;
		if (a_skred == '')
			document.querySelector('#prod_ctrl').innerHTML += '<br><font color="red">Не заполнена сумма ежемесячных выплат по текущим кредитам!</font>';		
		a_skred = (a_skred != '') ? parseInt(a_skred) : 0;
		
		
		var a_ds = document.querySelector('input[name="field36"]').value;
		//if (a_ds == '')
			//document.querySelector('#prod_ctrl').innerHTML += '<br><font color="red">Не заполнен cовокупный доход супруга!</font>';		
		a_ds = (a_ds != '') ? parseInt(a_ds) : 0;
		
		var adr = false;//form.adr.value;
		//adr = (adr == "2") ? true : false;
				
		var skred = a_skred;
		
		pkv = pkv + skred;
		
		var pmax = pkv - skred;
		
		var M23_ = zp/2;
		
		var a_izdeti = document.querySelector('input[name="field38"]').value;
		a_izdeti = a_izdeti.replace(/\D+/g,"");
		var izdeti = parseInt((a_izdeti == '') ? 0 : a_izdeti);
		
		var sex = document.querySelector('select[name="field17"]').value;
		
		
		/*console.log("Расчет прб!");
		console.log("zp:" + zp);
		console.log("cred_sv:" + cred_sv);
		console.log("inc_4_cre:" + inc_4_cre);
		console.log("pkv:" + pkv);
		console.log("a_skred:" + a_skred);
		console.log("skred:" + skred);
		console.log("pmax:" + pmax);
		console.log("M23_:" + M23_);
		console.log("sex:" + sex);*/
		
		var birth_day = document.querySelector('input[name="birth_day"]').value;
		var reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
		var mass;
		var a_drm = 21;
		var a_dry = 1980;
		if (reg.test(birth_day))
		{
			mass = reg.exec(birth_day);
			a_drm = mass[2];
			a_dry = mass[3];
		}
		
		
		for (var key in data)
		{
			var perc;
			var perc2;
			var pay_mon_s;
			var max_lim_s;
			var pay_mon_s2;
			var max_lim_s2;
			var bailn = data[key]['bailn'] == "1" ? "Залог" : "-";
			
			var guarantor0 = /2\|1\|\d/.test(data[key]['guarantor']) ? "Физ" : "";
			var guarantor1 = /2\|\d\|1/.test(data[key]['guarantor']) ? "Юр" : "";
			var guarantor = (guarantor0 != '' && guarantor1 != '') ? guarantor0 + '/' + guarantor1 : ( guarantor0 == '' && guarantor1 == '' ? '-' : guarantor0 + '' + guarantor1);
		
			
			calculate_pr(data[key]['min_percent'], false);
			calculate_pr(data[key]['max_percent'], true);
			var bcgc = (count & 1) ? " prb_row1" : " prb_row0" ;
			var tmp_test = '<div class = "prb_row' + bcgc + '" id="row_prb_' + data[key]['id'] + '">';
			tmp_test += '<div class = "prbcell res0">' + data[key]['id'] + '.</div>';
			if (data[key]['online_av'] && parseInt(data[key]['online_av'])) {
				tmp_test += '<div class = "prbcell res1" style="color:deepskyblue"><i id="prod' + data[key]['id'] + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail(this.id)">'
					+ data[key]['product_name'] + '</i><br><span class="max_p_t">Расчет по максимальному проценту</span></div>';
			} else {
				tmp_test += '<div class = "prbcell res1"><i id="prod' + data[key]['id'] + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail(this.id)">'
					+ data[key]['product_name'] + '</i><br><span class="max_p_t">Расчет по максимальному проценту</span></div>';
			}

			tmp_test +=	'<div class="prbcell res2">' + perc + '<br><span class="max_p_t">' + perc2 + '</span></div>';
			tmp_test +=	'<div class="prbcell res3">' + data[key]['bank_name_descr'] + '</div>';
			//tmp_test +=	'<div class="prbcell res4">' + pay_mon_s + '<br>' + pay_mon_s2 + '</div>';
			//tmp_test +=	'<div class="prbcell res5">' + max_lim_s + '<br>' + max_lim_s2 + '</div>';
			/*tmp_test +=	'<div class="prbcell res6" style="margin: 5px 20px;"><input type="checkbox" value="1" onclick="javascript:prod_vis(' 
				+ data[key]['id'] + ', this.checked);"></input></div>';
			//tmp_test +=	'<div class="res7">' + bail	+ '</div>';
			tmp_test +=	'<div class="prbcell res8">' + data[key]['max_cre_time'] + ' мес.</div>';
			tmp_test +=	'<div class="prbcell res9">' + bailn + '</div>';
			tmp_test +=	'<div class="prbcell res10">' + guarantor + '</div>';
			*/
			
			//if (staff_office_type == 'main')
			if (true)
			{
				var lim_min = parseInt(data[key]['lim_min']);
				var lim_max = parseInt(data[key]['lim_max']);
				var pay_min = parseInt(data[key]['pay_min']);
				var pay_max = parseInt(data[key]['pay_max']);
				
				var lim_min_s = lim_min + ' р.';
				var lim_max_s = lim_max + ' р.';
				var pay_min_s = pay_min + ' р.';
				var pay_max_s = pay_max + ' р.';
				
				lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				lim_max_s = lim_max_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				pay_min_s = pay_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				pay_max_s = pay_max_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				
				if (data[key]['age_error'] != null)
				{
					lim_min_s = '<font color="red">Возраст</font>';
					lim_max_s = '<font color="red">Возраст</font>';
					pay_min_s = '<font color="red">Возраст</font>';
					pay_max_s = '<font color="red">Возраст</font>';
				}
				else if (data[key]['vsk_error'] != null)
				{
					lim_min_s = '<font color="red">ВСК=0</font>';
					lim_max_s = '<font color="red">ВСК=0</font>';
					pay_min_s = '<font color="red">ВСК=0</font>';
					pay_max_s = '<font color="red">ВСК=0</font>';
				}
				else
				{
					var lim_min_s_tmp = lim_min_s;
					var pay_min_s_tmp = pay_min_s;
					var lim_max_s_tmp = lim_max_s;
					var pay_max_s_tmp = pay_max_s;
					
					var lmin_msg = '';
					var pmin_msg = '';
					var lmax_msg = '';
					var pmax_msg = '';
					
					
					if (data[key]['k_treb_error'] != null)
					{
						lmin_msg += '<div>Сумма ограничена максимальным размером кредита банка</div>';
						pmin_msg += '<div>Платеж ограничен максимальным размером кредита банка</div>';
						lmax_msg += '<div>Сумма ограничена максимальным размером кредита банка</div>';
						pmax_msg += '<div>Платеж ограничен максимальным размером кредита банка</div>';
						
						lim_min_s_tmp = '<span style="color:brown;" data-tooltip="' + lmin_msg + '">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:brown;" data-tooltip="' + pmin_msg + '">' + pay_min_s + '</span>';
						lim_max_s_tmp = '<span style="color:brown;" data-tooltip="' + lmax_msg + '">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:brown;" data-tooltip="' + pmax_msg + '">' + pay_max_s + '</span>';
					
					}
					
					if (data[key]['fico_pd_message'] != null)
					{
						lmin_msg += '<div>Ошибка настройки продукта! FICO вне предела. Используется П/Д по умолчанию!</div>';
						pmin_msg += '<div>Ошибка настройки продукта! FICO вне предела. Используется П/Д по умолчанию!</div>';
						lmax_msg += '<div>Ошибка настройки продукта! FICO вне предела. Используется П/Д по умолчанию!</div>';
						pmax_msg += '<div>Ошибка настройки продукта! FICO вне предела. Используется П/Д по умолчанию!</div>';
						
						lim_min_s_tmp = '<span style="color:red;" data-tooltip="' + lmin_msg + '">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:red;" data-tooltip="' + pmin_msg + '">' + pay_min_s + '</span>';
						lim_max_s_tmp = '<span style="color:red;" data-tooltip="' + lmax_msg + '">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:red;" data-tooltip="' + pmax_msg + '">' + pay_max_s + '</span>';
					}
					else
					{
					
					
					if (data[key]['pd_min_error'] != null)
					{
						lmin_msg += '<div>Не проходит по П/Д. Сумма расчитана исходя из максимально возможного платежа по П/Д</div>';
						pmin_msg += '<div>Не проходит по П/Д. Платеж ограничен максимально возможным платежом с учетом П/Д</div>';
						lim_min_s_tmp = '<span style="color:orange;" data-tooltip="' + lmin_msg + '">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:orange;" data-tooltip="' + pmin_msg + '">' + pay_min_s + '</span>';
					}
					else if (data[key]['calc_min_error'] != null)
					{
						lmin_msg += '<div>Сумма расчитана исходя из максимально возможного платежа</div>';
						pmin_msg += '<div>Платеж ограничен максимально возможным платежом</div>';
						lim_min_s_tmp = '<span style="color:red;" data-tooltip="' + lmin_msg + '">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:red;" data-tooltip="' + pmin_msg + '">' + pay_min_s + '</span>';
					}
					else if (data[key]['pd_miss_warning'] != null)
					{
						lmin_msg += '<div>Сумма расчитана исходя из максимально возможного платежа, П/Д игнорируется!</div>';
						pmin_msg += '<div>Платеж ограничен максимально возможным платежом, П/Д игнорируется!</div>';
						lim_min_s_tmp = '<span style="color:OrangeRed;" data-tooltip="' + lmin_msg + '">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:OrangeRed;" data-tooltip="' + pmin_msg + '">' + pay_min_s + '</span>';
					}
					
					
					if (data[key]['pd_max_error'] != null)
					{
						lmax_msg += '<div>Не проходит по П/Д. Сумма расчитана исходя из максимально возможного платежа по П/Д</div>';
						pmax_msg += '<div>Не проходит по П/Д. Платеж ограничен максимально возможным платежом с учетом П/Д</div>';
						lim_max_s_tmp = '<span style="color:orange;" data-tooltip="' + lmax_msg + '">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:orange;" data-tooltip="' + pmax_msg + '">' + pay_max_s + '</span>';
					}
					else if (data[key]['calc_max_error'] != null)
					{
						lmax_msg += '<div>Сумма расчитана исходя из максимально возможного платежа</div>';
						pmax_msg += '<div>Платеж ограничен максимально возможным платежом</div>';
						lim_max_s_tmp = '<span style="color:red;" data-tooltip="' + lmax_msg + '">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:red;" data-tooltip="' + pmax_msg + '">' + pay_max_s + '</span>';
					}
					else if (data[key]['pd_miss_warning'] != null)
					{
						lmax_msg += '<div>Сумма расчитана исходя из максимально возможного платежа, П/Д игнорируется!</div>';
						pmax_msg += '<div>Платеж ограничен максимально возможным платежом, П/Д игнорируется!</div>';
						lim_max_s_tmp = '<span style="color:OrangeRed;" data-tooltip="' + lmax_msg + '">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:OrangeRed;" data-tooltip="' + pmax_msg + '">' + pay_max_s + '</span>';
					}
					}
					
					
					offer[parseInt(data[key]['id'])] = Array(data[key]['product_name'], lim_min_s, pay_min_s, lim_max_s, pay_max_s, data[key]['vsk'], data[key]['min_percent'], data[key]['max_percent']);
					
					lim_min_s = lim_min_s_tmp;
					lim_max_s = lim_max_s_tmp;
					pay_min_s = pay_min_s_tmp;
					pay_max_s = pay_max_s_tmp;
					
					/*
					
					var lim_min_s_tmp = lim_min_s;
					var pay_min_s_tmp = pay_min_s;
					var lim_max_s_tmp = lim_max_s;
					var pay_max_s_tmp = pay_max_s;
					if (data[key]['k_treb_error'] != null)
					{
						lim_min_s_tmp = '<span style="color:brown;" data-tooltip="Сумма ограничена лимитом банка">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:brown;" data-tooltip="Платеж ограничен лимитом банка">' + pay_min_s + '</span>';
						lim_max_s_tmp = '<span style="color:brown;" data-tooltip="Сумма ограничена лимитом банка">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:brown;" data-tooltip="Платеж ограничен лимитом банка">' + pay_max_s + '</span>';
					
					}
					
					
					if (data[key]['pd_min_error'] != null)
					{
						lim_min_s_tmp = '<span style="color:orange;" data-tooltip="Не проходит по П/Д. Сумма расчитана исходя из максимально возможного платежа по П/Д">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:orange;" data-tooltip="Не проходит по П/Д. Платеж ограничен максимально возможным платежом с учетом П/Д">' + pay_min_s + '</span>';
					}
					else if (data[key]['calc_min_error'] != null)
					{
						lim_min_s_tmp = '<span style="color:red;" data-tooltip="Сумма расчитана исходя из максимально возможного платежа">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:red;" data-tooltip="Платеж ограничен максимально возможным платежом">' + pay_min_s + '</span>';
					}
					else if (data[key]['pd_miss_warning'] != null)
					{
						lim_min_s_tmp = '<span style="color:OrangeRed;" data-tooltip="Сумма расчитана исходя из максимально возможного платежа, П/Д игнорируется!">' + lim_min_s + '</span>';
						pay_min_s_tmp = '<span style="color:OrangeRed;" data-tooltip="Платеж ограничен максимально возможным платежом, П/Д игнорируется!">' + pay_min_s + '</span>';
					}
					
					
					if (data[key]['pd_max_error'] != null)
					{
						lim_max_s_tmp = '<span style="color:orange;" data-tooltip="Не проходит по П/Д. Сумма расчитана исходя из максимально возможного платежа по П/Д">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:orange;" data-tooltip="Не проходит по П/Д. Платеж ограничен максимально возможным платежом с учетом П/Д">' + pay_max_s + '</span>';
					}
					else if (data[key]['calc_max_error'] != null)
					{
						lim_max_s_tmp = '<span style="color:red;" data-tooltip="Сумма расчитана исходя из максимально возможного платежа">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:red;" data-tooltip="Платеж ограничен максимально возможным платежом">' + pay_max_s + '</span>';
					}
					else if (data[key]['pd_miss_warning'] != null)
					{
						lim_max_s_tmp = '<span style="color:OrangeRed;" data-tooltip="Сумма расчитана исходя из максимально возможного платежа, П/Д игнорируется!">' + lim_max_s + '</span>';
						pay_max_s_tmp = '<span style="color:OrangeRed;" data-tooltip="Платеж ограничен максимально возможным платежом, П/Д игнорируется!">' + pay_max_s + '</span>';
					}
					
					offer[parseInt(data[key]['id'])] = Array(data[key]['product_name'], lim_min_s, pay_min_s, lim_max_s, pay_max_s, data[key]['vsk'], data[key]['min_percent'], data[key]['max_percent']);
					
					lim_min_s = lim_min_s_tmp;
					lim_max_s = lim_max_s_tmp;
					pay_min_s = pay_min_s_tmp;
					pay_max_s = pay_max_s_tmp;
				*/	
				}
				
				tmp_test +=	'<div class="prbcell res4">' + pay_min_s + '<br><span class="max_p_t">' + pay_max_s + '</span></div>';
				tmp_test +=	'<div class="prbcell res5">' + lim_min_s + '<br><span class="max_p_t">' + lim_max_s + '</span></div>';
				
				
			}
			tmp_test +=	'<div class="prbcell res6" style="margin: 5px 20px;"><input type="checkbox" value="1" onclick="javascript:prod_vis(' 
				+ data[key]['id'] + ', this.checked);"></input></div>';
			//tmp_test +=	'<div class="res7">' + bail	+ '</div>';
			tmp_test +=	'<div class="prbcell res8">' + /*data[key]['max_cre_time']*/data[key]['max_cre_time'] + ' мес.</div>';
			var vsk = data[key]['vsk'] + ' мес.';
			if (data[key]['tK_max_warning'] != null)
				vsk = '<span style="color:green;" data-tooltip="Возможный срок кредита ограничен параметром продукта">' + vsk + '</span>';
			else
				vsk = '<span style="color:red;" data-tooltip="Возможный срок кредита ограничен возрастом клиента">' + vsk + '</span>';
			tmp_test +=	'<div class="prbcell res9">' + vsk + '</div>';
			tmp_test +=	'<div class="prbcell res10">' + bailn + '</div>';
			tmp_test +=	'<div class="prbcell res11">' + guarantor + '</div>';
			
			
			
			//tmp_test +=	'<div class="res9">' + pay_mon_s + ' р.</div>';
			//tmp_test +=	'<div class="resa gr">' + max_lim_s + '</div><br>';
			
			//calculate_pr(Item[key]['max_percent'], true);
			
			var tmp_id = parseInt(data[key]['id']);
			if (data[key]['prb_in_work'] == 1)
				tmp_test += "<div class='prbcell res12' style=\"cursor:pointer;\" onclick=\"javascript:del_prb2form(this, " + tmp_id + ");\"><div style=\"color:green;\">В работе</div></div>";
			else
				tmp_test += "<div class='prbcell res12' style=\"cursor:pointer;\" onclick=\"javascript:add_prb2form(this, " + tmp_id + ");\"><div style=\"color:red;\">Не в работе</div></div>";
			tmp_test += "</div>";
			wind.innerHTML += tmp_test;
			
			
			
			/*
			if (data[key]['max_percent'] != data[key]['min_percent'])
			{
			
				
				
				bcgc = (count & 1) ? " prb_row1" : " prb_row0" ;
				tmp_test = '<div class = "prb_row' + bcgc + '" id="row_prb_' + data[key]['id'] + '">';
				tmp_test += '<div class = "prbcell res0">' + data[key]['id'] + '.</div>';
				tmp_test += '<div class = "prbcell res1"><i id="prod' + data[key]['id'] + '" style="cursor: pointer; font-weight: bold;" onclick="show_detail(this.id)">' 
					+ data[key]['product_name'] + '</i></div>';
					
				tmp_test +=	'<div class="prbcell res2">' + perc2 + '</div>';
				tmp_test +=	'<div class="prbcell res3">' + data[key]['bank_name_descr'] + '</div>';
				tmp_test +=	'<div class="prbcell res4">' + pay_mon_s2 + '</div>';
				tmp_test +=	'<div class="prbcell res5">' + max_lim_s2 + '</div>';
				tmp_test +=	'<div class="prbcell res6" style="margin: 5px 20px;"><input type="checkbox" value="1" onclick="javascript:prod_vis(' 
					+ data[key]['id'] + ', this.checked);"></input></div>';
				//tmp_test +=	'<div class="res7">' + bail	+ '</div>';
				tmp_test +=	'<div class="prbcell res8">' + data[key]['max_cre_time'] + ' мес.</div>';
				tmp_test +=	'<div class="prbcell res9">' + bailn + '</div>';
				tmp_test +=	'<div class="prbcell res10">' + guarantor + '</div>';
				tmp_test += "</div>";
				wind.innerHTML += tmp_test;
			}*/
			
			count++;
		}
		
		function calculate_pr(stavka, fix)
		{
			stavka = stavka.replace(/\,/g,".");
			var M23 = M23_;

			var a_miss_1 = false;;
			var a_miss_2 = false;;
			var max_mon = parseInt(data[key]['max_cre_time']);
						
			if ((sex != -1) && (sex != ''))
			{
				//var age = form.age.value;
				
				var max_e_e_m = parseInt(data[key]['max_end_age_m']);
				var max_e_e_w = parseInt(data[key]['max_end_age_w']);
				var max_e_e = (sex == 'Мужской') ? max_e_e_m : max_e_e_w;
				max_e_e = (max_e_e * 12);
				var now_d = new Date();
				var now_m = now_d.getMonth()+1;
				var now_y = 1900 + now_d.getYear();
				var now_age = now_y*12 + now_m - a_drm - 12*a_dry;
				var max_mon_new = max_e_e - now_age;
				var st_tmp = "test: old_max_mon=" + max_mon + " max_mon_new=" + max_mon_new + " max_e_e=" + max_e_e + " now_age=" + now_age;
				max_mon = ((max_mon_new < max_mon) && (max_e_e > 0)) ? max_mon_new : max_mon;
				st_tmp += " max_mon=" + max_mon + " bank=" + data[key]['product_name'];
				//console.log(st_tmp);
			}
			
			
			
			//console.log(count + " :: " + max_mon + ", " + now_age + ", " + max_e_e);
			
			
			max_mon = (max_mon == 0) ? -1 : 0-max_mon;
			
			var pm = parseInt(data[key]['pm']);
			if (adr)
				pm = pm*2;
	
			var m = data[key]['goal'].split('|');
			
			var is_ngoal = parseInt(m[1]);
			var is_ipoteka = parseInt(m[2]);
			var is_garant = parseInt(m[3]);
			var is_lizing = parseInt(m[4]);
			var is_refinans = parseInt(m[5]);
			var is_card = parseInt(m[6]);
			
			/*console.log("Продукт:" + data[key]['product_name']);
			console.log("is_ngoal " + is_ngoal);
			console.log("is_ipoteka " + is_ipoteka);
			console.log("is_garant " + is_garant);
			console.log("is_lizing " + is_lizing);
			console.log("is_refinans " + is_refinans);
			console.log("is_card " + is_card);*/
			
			if (is_ngoal || is_card)
			{
				var test = zp - pmax - skred - pm - izdeti*10000;
				//console.log("test " + test);
				//f_param.innerHTML += "Д - Pmax - Sкред - PM - (Иждети)*10000: " + test + "р<br>";

				if (test <=0)
				{
					//msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
					a_miss_1 = true;
				}
				else
					M23 = (pmax > 0) ? pmax : 0;
			}
			else if (is_ipoteka)
			{
				var test = zp + parseInt((a_ds == "") ? "0" : a_ds) - pmax - skred - pm - izdeti*10000;
//				console.log("test " + test);
//				console.log("pmax " + pmax);
				//f_param.innerHTML += "Д + ДС - Pmax - Sкред - PM - (Иждети)*10000: " + test + "р<br>";

				if (test <=0)
				{
					//msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
					a_miss_1 = true;
				}
				else
					M23 = (pmax > 0) ? pmax : 0;
			}
			
			var min_income = parseInt(data[key]['min_income']);
			var dmin_t = zp - pmax - skred;
			
			if (!is_refinans)
			{
			
				//console.log(count + " :: " + zp + ", " + pmax + ", " + skred + ", " + dmin_t + ", " + min_income + "");
				if (dmin_t < min_income)
				{
					//msg_4_staff2.innerHTML = '<font color="orange">Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж<font>';
					a_miss_2 = true;
				}
			}
			//var stavka = data[key]['max_percent'];
//			console.log("расчет платежа");
//			console.log("Продукт:" + data[key]['product_name']);
//			console.log("stavka " + stavka);
			//console.log("stavka " + parseFloat(stavka));
			var st = parseFloat(stavka)/100;
			//console.log("st " + st);
			var f_ = st/12;
			var s_ = 1+st/12;
			var t_ = Math.pow(1+st/12, max_mon);
			var e15 = 0;
			
			
//			console.log("M23 " + M23);
//			console.log("st " + st);
//			console.log("max_mon " + max_mon);
			var m21 = Math.round(M23/(((st/12)/(1-Math.pow((1+st/12),max_mon)))));
//			console.log("m21 " + m21);
			var max_lim = m21/(1-e15);
			//console.log("max_lim " + max_lim);
			max_lim = (parseInt(max_lim) < parseInt(data[key]['max_credit'])) ? max_lim : data[key]['max_credit'];
			max_lim = (cred_sv < max_lim) ? cred_sv : max_lim;
			
			
			
			var a_test = (1-Math.pow(1+st/12, max_mon));
			
			//console.log("a_test " + a_test);
			if (st == 0)
				a_test = 1;
			var pay_mon = Math.round(max_lim*((st/12)/a_test));
			

			/*console.log("pay_mon " + pay_mon);
			console.log("max_lim " + max_lim);
			
			console.log("a_test " + a_test);
			console.log("max_mon " + max_mon);
			*/
			
			if (st == 0)
				pay_mon = Math.round(max_lim/(0-max_mon));
			
			//console.log("pay_mon " + pay_mon);
			
			if (is_refinans)
			{
				if (skred - pay_mon > 0)
				{
					var test_ref = zp - pay_mon - pm - izdeti*10000;
					if (!test_ref)
					{
						//msg_4_staff.innerHTML = '<font color="red">Необходимо уменьшить максимальный платеж<font>';
						a_miss_1 = true;
					}
					
					dmin_t = zp - pay_mon;
				
					if (dmin_t <= min_income)
					{
						//msg_4_staff2.innerHTML = '<font color="orange">Недостаточный  ежемесячный доход.  Использовать дополнительные источники дохода, участие Созаемщика или уменьшить Максимально возможный платеж<font>';
						a_miss_2 = true;
					}
				}
			}
			
			p_max_mon = 0 - max_mon;
			var tmp_pay_mon_s = '' + pay_mon;
			tmp_pay_mon_s = tmp_pay_mon_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			
			if (fix)
				pay_mon_s2 = tmp_pay_mon_s;
			else
				pay_mon_s = tmp_pay_mon_s;
			
			var pmt = pay_mon;
			
			var tmp_max_lim_s = '' + (parseInt(max_lim) % 1000);
			var mlt = max_lim;
			
			if (a_miss_1)
			{
				//max_lim = "НЕТ";
				tmp_max_lim_s = "НЕТ";
				pay_mon = 0;
			}
			else if (a_miss_2)
			{
				tmp_max_lim_s = "<font color='red'><b>НЕТ</b></font>";
			}
			else
			{
				tmp_max_lim_s = mlt + " р.";
				tmp_max_lim_s = tmp_max_lim_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			}
			
			if ((max_lim == 0) && (inc_4_cre == 0))
			{
				max_lim = cred_sv;
				tmp_max_lim_s = max_lim + " р.";
				tmp_max_lim_s = '<font color="orange">' + tmp_max_lim_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</font>';
				var tmp_per = parseFloat(stavka) / 1200;
//				console.log("test new formuls!!!!!");
//				console.log('tmp_per ' + tmp_per);
//				console.log('max_mon ' + max_mon);
				var tmp_max_mon = 0 - max_mon;
				var kef = (tmp_per*pow(1+tmp_per,tmp_max_mon))/(pow(1+tmp_per, tmp_max_mon) - 1);
//				console.log('kef ' + kef);
				pay_mon = parseInt(kef*max_lim);
				tmp_pay_mon_s = '' + pay_mon;
				tmp_pay_mon_s = '<font color="orange">' + tmp_pay_mon_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</font>';
				
				if (fix)
					pay_mon_s2 = tmp_pay_mon_s;
				else
					pay_mon_s = tmp_pay_mon_s;
			}
			
			if (fix)
				max_lim_s2 = tmp_max_lim_s;
			else
				max_lim_s = tmp_max_lim_s;
			
			if (stavka == 0)
				stavka = '<font color="red">0!</font>';
			
			if (fix)
				perc2 = stavka;
			else
				perc = stavka;
		}
		
		var tmp_r2 = document.querySelector('#prod_result');
		var tmp_divs = tmp_r2.querySelectorAll('div > .res3');
		var tmp_divs_res12 = tmp_r2.querySelectorAll('div > .res12');
		//console.log(tmp_divs.length);
		for (var i = 0; i < tmp_divs.length; i++)
		{
			tmp_divs[i].style.display 		= (view_b == 'Режим работы') ? "none" : "block";
			tmp_divs_res12[i].style.display = (view_b == 'Режим работы') ? "none" : "block";
		}
		
		var max_p_t = document.querySelectorAll('span.max_p_t');
		for (var i = 0; i < max_p_t.length; i++)
			max_p_t[i].style.display = (view_b == 'Режим работы') ? "none" : "block";
		
		sort_pod(5);
		sort_pod(5);
}

function pow(a, n) {
    var b = a;

    for (var i = 1; i < n; i++) {
        b *= a;
    }

    return b;
}


function add_prb2form(el, id)
{
	ch_prb2form(el, id, 1);
}

function del_prb2form(el, id)
{
	ch_prb2form(el, id, 0);
}

function ch_prb2form(el, id, add)
{
	var data = "block=anketa&action=ch_prb2form&id=" + anketa_id + "&prb=" + id + "&add=" + add;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (add)
			{
				el.setAttribute('onclick', 'javascript:del_prb2form(this, ' + id + ')');
				el.innerHTML = "<div style=\"color:green;\">В работе</div>";
			}
			else
			{
				el.setAttribute('onclick', 'javascript:add_prb2form(this, ' + id + ')');
				el.innerHTML = "<div style=\"color:red;\">Не в работе</div>";
			}
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
	

	
	
}

function bank_send(el)
{
	var bank = el.parentNode.querySelector('input[name=bank_name]').value;
	var sum = el.parentNode.querySelector('input[name=bank_summ]').value;
	var blw = document.querySelector("#bank_add_wrap");
	
	if (bank == '')
	{
		alert('Не заполено название банка');
	}
	else if (sum == '')
	{
		alert('Не заполена сумма');
	}
	else
	{
		var data = "block=anketa&action=bank_send&id=" + anketa_id + "&bank=" + bank + "&sum=" + sum;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Запись добавлена!');
				blw.removeChild(el.parentNode);
				get_banks_work();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function get_banks_work()
{
	var blw = document.querySelector("#bank_list_wrap");
	if (!blw)
		return;
	blw.innerHTML = '';
	var data = "block=anketa&action=get_banks_work&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'];
			point_list = Item['point_list'];
			
			var table_str = '';
			
			var hit_com = false;
			
			if (Item['staff_bc'] == null)
				hit_com = false;
			else
				hit_com = (Item['staff_bc'] == 1) ? true : false;
			//if (staff_id_js == 1)
				//hit_com = true;
			
			//if (staff_id_js == 1)
			if ((staff_office_type == 'main') || (staff_office_type == 'ozs'))
			{
				table_str += '<table border="1px solid black">';
				table_str += '<tr>';
					table_str += '<td>№</td>';
					table_str += '<td>Банк/точка/сумма/продукт</td>';
					//table_str += '<td>Сумма</td>';
					//table_str += '<td>Точка выдачи</td>';
					table_str += '<td>Статус</td>';
					//table_str += '<td>Действие</td>';
					
					table_str += '<td>Параметры</td>';
					if (hit_com)
						table_str += '<td>Комиссия</td>';
					if (staff_office_type == 'main')
					{
						table_str += '<td>Удалить</td>';
					}
				table_str += '</tr>';
				var cc = 0;
				for (var key in data)
				{
					var val = data[key];
					cc++;
					table_str += '<tr>';
						table_str += '<td>' + cc + '</td>';
						table_str += '<td>Банк: ' + val['bankname'];
						if (val['product_name'])
							table_str += '<br>Продукт: ' + val['product_name'];
						
						table_str += '<br>Сумма: ' + (val['sum_send'] + ' р.').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
						
						if (parseInt(val['point']) > 0)
						{

							let point_tmp = (point_list[val['bank_id']][val['point']] != undefined) ? point_list[val['bank_id']][val['point']]['name'] : 'Точка удалена';
							table_str += '<br>Точка:' + point_tmp;
							if (parseInt(val['analyst']) > 0)
								table_str += ', с аналитиком';
						}
						table_str += '</td>';
						//table_str += '<td><input type="text" value="' + val['sum_send'] + '"></td>';
						/*if (val['have_point'])
						{
							var point_list = val['point_list'];
							table_str += '<td>';
							table_str += '<select onchange="javascript:ch_point();"><option value="">Выберите</option>';
							for(var p in point_list)
							{
								var ps = '';//(point_list[p]['id'] == row['point']) ? " selected" : "";
								table_str += '<option value="' + point_list[p]['id'] + '"' + ps + '>' + point_list[p]['name'] + '</option>';
							}
							table_str += '</select>';
							
							
							var inpc = '';
							table_str += '<br>';
							table_str += '<label><input value="1" type="checkbox"' + inpc + ' onclick="javascript:ch_analyst();"> Аналитик</label>';
							
							table_str += '</td>';
							//table_str += '<br>';
							//table_str += '<label><input value="1" type="checkbox"' + inpc + ' onclick="javascript:ch_analyst(this, ' + parseInt(row['bid']) + ');"> Аналитик</label></td>';
						}
						else
							table_str += '<td></td>';
							*/					
						/*if (val['status'] == 'ready')
							table_str += '<td>Ожидает подачи</td>';
						else if(val['status'] == 'sent')
							table_str += '<td>Отправлена</td>';
						*/
						if (val['status'] == 'ready')
							table_str += '<td><button onclick="sent2bank(this, ' + val['id'] + ');">Подать</button></td>';
						/*else if ((staff_office_type == 'ozs') && (val['bank_answer'] == 'preapprove'))
						{
							var tmp = '<select onchange="javascript:ch_banksent_st2(this, ' + val['id'] + ", '" + val['bank_answer'] + "');\">";
								tmp += '<option value="preapprove" selected>Предодобрено</option>';
								tmp += '<option value="approve">Одобрено</option>';
							tmp += '</select>';
							table_str += '<td>' + tmp;
							if (val['date_answer'])
								table_str += '<br>' + val['date_answer'];
							table_str += '</td>';
						}*/
						else if ((staff_office_type == 'main') || ((staff_office_type == 'ozs') && (val['bank_answer'] == 'preapprove'))) 
						{
							//var tmp = 'Подано в банк ' + val['date_send'] + '<br>';
							var sel_se = '';
							var sel_ap = '';
							var sel_re = '';
							var sel_tre = '';
							var sel_pre = '';
							var sel_cn = (val['status'] == 'canceled') ? " selected" : '';
							if (sel_cn == '')
							{
								sel_se = (val['status'] == 'sent') ? " selected" : '';
								if (sel_se == '')
								{
									sel_ap = (val['bank_answer'] == 'approve') ? " selected" : '';
									sel_re = (val['bank_answer'] == 'reject') ? " selected" : '';
									sel_tre = (val['bank_answer'] == 'techReject') ? " selected" : '';
									sel_pre = (val['bank_answer'] == 'preapprove') ? " selected" : '';
								}
							}
							console.log(table_str);
							console.log(val['status']);
							console.log(val['bank_answer']);
							console.log(sel_cn);
							
							var tmp = '<select onchange="javascript:ch_banksent_st2(this, ' + val['id'] + ", '" + val['bank_answer'] + "');\">";
								tmp += '<option value="-1">Выберите</option>';
								tmp += '<option' + sel_se + ' value="sent">Отправлена в банк</option>';
								tmp += '<option' + sel_pre + ' value="preapprove">Предодобрено</option>';
								tmp += '<option' + sel_ap + ' value="approve">Одобрено</option>';
								tmp += '<option' + sel_re + ' value="reject">Отклонено</option>';
								tmp += '<option' + sel_tre + ' value="techReject">Отклонено по тех. причинам</option>';
								tmp += '<option' + sel_cn + ' value="canceled">Отказ клиента</option>';
							tmp += '</select>';
							table_str += '<td>' + tmp;
							if (val['date_answer'] && (sel_se == ''))
								table_str += '<br>' + val['date_answer'];
							table_str += '</td>';
						}
						else if(val['status'] == 'sent')
						{
							var tmp = '<select onchange="javascript:ch_banksent_st(this, ' + val['id'] + ');">';
								tmp += '<option value="-1">Выберите</option>';
								tmp += '<option value="preapprove">Предодобрено</option>';
								tmp += '<option value="approve">Одобрено</option>';
								tmp += '<option value="reject">Отклонено</option>';
								tmp += '<option value="techReject">Отклонено по тех. причинам</option>';
								tmp += '<option value="canceled">Отказ клиента</option>';
							tmp += '</select>';
							table_str += '<td>' + tmp + '</td>';
						}
						else if(val['status'] == 'get_answer')
						{
							var ans = (val['bank_answer'] == 'approve') ? "Одобрено" : ((val['bank_answer'] == 'preapprove') ? "Предодобрено" : "Отклонено");
							table_str += '<td>' + ans + '<br>' + val['date_answer'];
							if ((val['bank_answer'] == 'approve') || (val['bank_answer'] == 'preapprove'))
							{
								//if (staff_id_js == 1)
								table_str += '<br><br><button onclick="javascript:client_cancel(' + val['id'] + ');">Отказ клиента?</button>';
							}
							table_str += '</td>';
						}
						else if(val['status'] == 'canceled')
							table_str += '<td>Отказ клиента</td>';
							
							//table_str += '<td><select><option>Выберите ответ</option><option>Одобрен банком</option><option>Отклонен банком</option><option>Отказ клиента</option></select></td>';
						
						if((val['status'] == 'get_answer') && ((val['bank_answer'] == 'approve') || (val['bank_answer'] == 'preapprove')))
						{
							//val['sum_answer'] = parseInt(val['sum_answer']);
							if (val['on_arm'] == null)
								val['on_arm'] = '';
							if (val['per'] == null)
								val['per'] = '';
							if (val['cretime'] == null)
								val['cretime'] = '';
							if (val['sum_answer'] == null)
								val['sum_answer'] = '';
							//val['per'] = parseFloat(val['per']);
							//val['cretime'] = parseInt(val['cretime']);
							var tmp =  '<div style="width:250px;"><div class="bank_d1">Одобрено, р:</div><div class="bank_d2"><input name="odob" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + val['sum_answer'] + '"></div></div>';
								tmp += '<div style="width:250px;"><div class="bank_d1">На руки, р:</div><div class="bank_d2"><input name="arm" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + 	val['on_arm'] + '"></div></div>';
								tmp += '<div style="width:250px;"><div class="bank_d1">Ставка, %:</div><div class="bank_d2"><input name="per" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + 	val['per'] + '"></div></div>';
								tmp += '<div style="width:250px;"><div class="bank_d1">Срок, мес:</div><div class="bank_d2"><input name="time" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + 	val['cretime'] + '"></div></div>';
							table_str += '<td>' + tmp + '</td>';
						}
						else if(val['status'] == 'sent')
						{
							table_str += '<td>Подано в банк<br>' + val['sent2bank'] + '</td>';
						}
						else
							table_str += '<td></td>';
						
						if (hit_com)
						{
							if (val['com_fact'] == null)
								val['com_fact'] = '';
							if (val['com_dop'] == null)
								val['com_dop'] = '';
							
							if((val['status'] == 'get_answer') && ((val['bank_answer'] == 'approve') || (val['bank_answer'] == 'preapprove')))
							//if((val['status'] == 'get_answer') && (val['bank_answer'] == 'approve'))
							{
								table_str += '<td>Планируемая: <br>';
								var field165 = document.querySelector('input[name="field165"]').value;
								//var field165 = document.querySelector('input[name="arm"]').value;
								field165 = field165.replace(',', '.');
								field165 = (field165 == '') ? 0 : parseFloat(field165);
								var field179 = document.querySelector('input[name="field179"]').value;
								field179 = field179.replace(',', '.');
								field179 = (field179 == '') ? 0 : parseFloat(field179);
								
								var zal = val['product_name'];
								var sum_com = 0;
								var sum_del = 0;
								
								/*if (parseInt(val['on_arm']) > 0) {
									//sum_com = parseInt(val['on_arm']);
									sum_del = 1;
								}
								else if (parseInt(val['sum_answer']) > 0) {
									//sum_com = parseInt(val['sum_answer']);
									sum_del = 1;
								}
								else */if (zal == 'беззалог')
								{
									sum_com = field165;
									sum_del = 1;
								}
								else if (zal == 'залог')
								{
									sum_com = field179;
									sum_del = 1;
								}
								else
								{
									sum_com = field165 + field179;
									sum_del = ((field165) ? 1 : 0) + ((field179) ? 1 : 0);
								}

								
								if (sum_del == 0)
									sum_com = '<font color="red">Не заполнена</font>';
								else if (parseInt(val['on_arm']) > 0)
								{
									console.log(parseInt(val['on_arm']), sum_del);
									sum_com = 0.01 * parseInt(val['on_arm']) * sum_com / sum_del;
									console.log(sum_com);
									sum_com = (sum_com.toFixed(2) + ' р.').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
								}
								else if (val['sum_answer'] == '')
								{
									sum_com = 0.01 * parseInt(val['sum_send']) * sum_com/sum_del;
									sum_com = (sum_com.toFixed(2) + ' р.').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
								}
								else
								{
									sum_com = 0.01 * parseInt(val['sum_answer']) * sum_com/sum_del;
									sum_com = (sum_com.toFixed(2) + ' р.').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
								}

								
								table_str += sum_com + '<br>';
								
								table_str += 'Фактическая:<br><input name="com_fact" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + val['com_fact'] + '">';
								table_str += 'Доп.комиссия:<br><input name="com_dop" oninput="javascript:ch_banksent_data(this, ' + val['id'] + ');" style="width:100px;" type="text" value="' + val['com_dop'] + '">';
								table_str += '</td>';
							}
							else
								table_str += '<td></td>';
						}
						
						if (staff_office_type == 'main')
						{
							table_str += '<td align="center"><span onclick="javascript:delete_row_b(this, ' + val['id'] + ');" style="cursor:pointer;"><img src="/img/delete.png" height="30px"></span></td>';
						}
					table_str += '</tr>';
				}
				table_str += '</table>';
				
				blw.innerHTML += table_str;
			}
			
			
			/*for (var key in data)
			{
				var val = data[key];
				blw.innerHTML += 'Заявка в ' + val['bankname'] + ' на сумму ' + val['sum_send'];
				
				if (val['status'] == 'ready')
					blw.innerHTML += ' готова к отправке.';
				else if(val['status'] == 'sent')
					blw.innerHTML += ' отправлена.';
				else if(val['status'] == 'get_answer')
				{
					if (val['bank_answer'] != null)
					{
						if (val['bank_answer'] == 'approve')
						{
							blw.innerHTML += ' одобрена на сумму ' + val['sum_answer'] + '.';
						}
						else if (val['bank_answer'] == 'reject')
						{
							blw.innerHTML += ' отклонена.';
						}
					}
				}
				blw.innerHTML += '<br>';
			}*/
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	}/*,
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	}*/
	);
}

function client_cancel(id)
{
	if (!confirm("Клиент точно отказался от этого продукта?"))
		return;
	var val = 'canceled';
	var data = "block=anketa&action=ch_banksent_st&id=" + anketa_id + "&prid=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Статус изменен!');
			
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function delete_row_b(el, id)
{
	if (!confirm("Вы точно хотите удалить эту строку?"))
		return;
	var val = 'delete';
	var data = "block=anketa&action=ch_banksent_st&id=" + anketa_id + "&prid=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Удалено!');
			el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function sent2bank(el, id)
{
	var data = "block=anketa&action=sent2bank&id=" + anketa_id + "&prid=" + id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Заявка в банк отправлена!\nНе забудьте заполнить параметры ответа, когда он придет!');
			
			var tmp = '<select onchange="javascript:ch_banksent_st(this, ' + id + ');">';
				tmp += '<option value="-1">Выберите</option>';
				tmp += '<option value="preapprove">Предодобрено</option>';
				tmp += '<option value="approve">Одобрено</option>';
				tmp += '<option value="reject">Отклонено</option>';
				tmp += '<option value="techReject">Отклонено по тех. причинам</option>';
				tmp += '<option value="canceled">Отказ клиента</option>';
			tmp += '</select>';
			el.parentNode.innerHTML = tmp;
			//show('none');
			//reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_banksent_data(el, id)
{
	var val = el.value;
	var name = el.name;
	var data = "block=anketa&action=ch_banksent_data&id=" + anketa_id + "&prid=" + id + "&val=" + val + "&name=" + name;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//alert('Статус изменен!');
			console.log(name + ":" + val);
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_banksent_st(el, id)
{
	var val = el.options[el.selectedIndex].value;
	
	if ((val == 'approve') || (val == 'preapprove'))
	{
		var tmp =  '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Одобрено, р:</div><div class="bank_d2" style="width:120px;"><input  name="odob" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
			tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">На руки, р:</div><div class="bank_d2" style="width:120px;"><input name="arm" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
			tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Ставка, %:</div><div class="bank_d2" style="width:120px;"><input name="per" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
			tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Срок, мес:</div><div class="bank_d2" style="width:120px;"><input name="time" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
			el.parentNode.nextSibling.innerHTML = tmp;
	}
	
	var data = "block=anketa&action=ch_banksent_st&id=" + anketa_id + "&prid=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Статус изменен!');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_banksent_st2(el, id, oldst)
{
	var val = el.options[el.selectedIndex].value;
	
	if (oldst != 'preapprove') {
	
		if ((val == 'approve') || (val == 'preapprove'))
		{
			var tmp =  '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Одобрено, р:</div><div class="bank_d2" style="width:120px;"><input  name="odob" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
				tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">На руки, р:</div><div class="bank_d2" style="width:120px;"><input name="arm" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
				tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Ставка, %:</div><div class="bank_d2" style="width:120px;"><input name="per" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
				tmp += '<div style="width:250px;"><div class="bank_d1" style="width:100px;">Срок, мес:</div><div class="bank_d2" style="width:120px;"><input name="time" oninput="javascript:ch_banksent_data(this, ' + id + ');" style="width:100px;" type="text"></div></div>';
				el.parentNode.nextSibling.innerHTML = tmp;
		}
		else if (val == 'sent')
		{
			el.parentNode.nextSibling.innerHTML = '';
			sent2bank(el, id);
			return;
		}
	}
	
	var data = "block=anketa&action=ch_banksent_st&id=" + anketa_id + "&prid=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Статус изменен!');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function set_work_plan()
{
	show('block');
	var data = "block=anketa&action=show_prb&id=" + anketa_id + "&pr=1&set=1";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
	
	//if (last_wind != 'set_work_plan')
	//{
		last_wind = 'set_work_plan';
		var wind_ = document.querySelector("#window");
		wind_.style.width = "1200px";
		wind_.style.height = "700px";
		wind_.style.margin = "20px auto";
		wind_.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
		
		var wind = document.createElement('div');
		wind.style = "position:absolute; margin: 0px 25px; width:1000px; height:250px; overflow: auto; border: solid 1px black;";
		
		var div = document.createElement('div');
		if (anketa_main_form == 0)
			div.innerHTML = "Статус анеты: основная" ;
		else
			div.innerHTML = "Статус анеты: поданкета. Основная - <a target=_blank href=\"/details/" + anketa_main_form + "/\">#" + anketa_main_form + "</a>" ;
		wind.appendChild(div);
		
		var lname = document.querySelector("input[name=lastname]").value;
		var fname = document.querySelector("input[name=firstname]").value;
		var mname = document.querySelector("input[name=middlename]").value;
		div = document.createElement('div');
			div.innerHTML = "ФИО: " + lname + " " + fname + " " + mname;
		wind.appendChild(div);
		
		
		div = document.createElement('div');
		var birth_day = document.querySelector('input[name="birth_day"]').value;
		var reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
		var mass;
		var a_drm = 21;
		var a_dry = 1980;
		if (reg.test(birth_day))
		{
			mass = reg.exec(birth_day);
			a_drm = mass[2];
			a_dry = mass[3];
			var now_d = new Date();
			var now_m = now_d.getMonth()+1;
			var now_y = 1900 + now_d.getYear();
			var now_age = now_y*12 + now_m - a_drm - 12*a_dry;
			//now_age = now_age/12;
			var mna = now_age % 12;
		
		
			div.innerHTML = "Возраст: " + Math.floor(now_age/12) + " лет и " + mna + " мес.";
		}
		else
			div.innerHTML = "Возраст: не заполнена дата рождения";
		wind.appendChild(div);
		
		
		var field1 = document.querySelector("input[name=field1]").value;
		div = document.createElement('div');
			div.innerHTML = "Регион обращения: " + field1;
		wind.appendChild(div);
		
		var field3 = document.querySelectorAll('input[name*="field3["]');
		var tmp_f3 = "";
		for(key in field3)
		{
			if (field3[key].checked)
			{
				if (tmp_f3 != '')
					tmp_f3 += ', ';
				tmp_f3 += field3[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Цель кредита: " + tmp_f3;
		wind.appendChild(div);
		
		
		//
		var span_field24 = document.querySelector("#span_field24").innerHTML;
		div = document.createElement('div');
			div.innerHTML = "Адрес регистрации: " + span_field24;
		wind.appendChild(div);
		
		var field176 = document.querySelectorAll("input[name=field176]");
		var field176_t = 'не указано';
		for(key in field176)
		{
			if (field176[key].checked)
				field176_t = field176[key].value;
		}
		div = document.createElement('div');
			div.innerHTML = "Заинтересованные лица: " + field176_t;
		wind.appendChild(div);
		
		var field30 = document.querySelector("select[name=field30]");
		console.log(field30);
		field30 = field30.options[field30.selectedIndex].value;
		
		if (field30 == '')
			field30 = "Не заполнено";
		div = document.createElement('div');
			div.innerHTML = "Семейное положение: " + field30;
		wind.appendChild(div);
		
		var field75 = document.querySelector("input[name=field75]").value;
		div = document.createElement('div');
			div.innerHTML = "Общая сумма доходов: " + field75;
		wind.appendChild(div);
		
		var field94 = document.querySelector("input[name=field94]").value;
		div = document.createElement('div');
			div.innerHTML = "Сумма ежемесячных платежей по текущим кредитам: " + field94;
		wind.appendChild(div);
		
		var field47 = document.querySelector("select[name=field47]");
		field47 = field47.options[field47.selectedIndex].value;
		if (field47 == '')
			field47 = "Не заполнено";
		div = document.createElement('div');
			div.innerHTML = "Трудоустройство: " + field47;
		wind.appendChild(div);
		
		var field77 = document.querySelectorAll('input[name*="field77["]');
		var tmp_f77 = "";
		for(key in field77)
		{
			if (field77[key].checked)
			{
				if (tmp_f77 != '')
					tmp_f77 += ', ';
				tmp_f77 += field77[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Вариант подтверждения дохода: " + tmp_f77;
		wind.appendChild(div);
		
		var field8 = document.querySelectorAll('input[name*="field8["]');
		var tmp_f8 = "";
		for(key in field8)
		{
			if (field8[key].checked)
			{
				if (tmp_f8 != '')
					tmp_f8 += ', ';
				tmp_f8 += field8[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Активы клиента: " + tmp_f8;
		wind.appendChild(div);
		
		var field106 = document.querySelector("div[name=field106]");
		var inps = field106.querySelectorAll('input');
		console.log(inps);
		var tmp_f106 = "";
		
		for (var i = 0; i < inps.length; i++)
		{
			if (tmp_f106 != '')
				tmp_f106 += ', ';
			tmp_f106 += inps[i].value;
		}
		div = document.createElement('div');
			div.innerHTML = "Банки, где есть з/п счета: " + tmp_f106;
		wind.appendChild(div);
		
		var field92 = document.querySelector("div[id=field92]");
		var inps = field92.querySelectorAll('span');
		console.log(inps);
		var tmp_f92 = "";
		
		for (var i = 0; i < inps.length; i++)
		{
			if (tmp_f92 != '')
				tmp_f92 += ', ';
			tmp_f92 += inps[i].innerHTML;
		}
		div = document.createElement('div');
			div.innerHTML = "Банки-кредиторы: " + tmp_f92;
		wind.appendChild(div);
		
		
		var field49 = document.querySelector("input[name=field49]").value;
		div = document.createElement('div');
			div.innerHTML = "Название компании: " + field49;
		wind.appendChild(div);
		
		var span_field55 = document.querySelector("#span_field55").innerHTML;
		div = document.createElement('div');
			div.innerHTML = "Фактический адрес компании: " + span_field55;
		wind.appendChild(div);
		
		var field50 = document.querySelector("input[name=field50]").value;
		div = document.createElement('div');
			div.innerHTML = "ИНН: " + field50;
		wind.appendChild(div);
		
		var field54 = document.querySelector("input[name=field54]").value;
		div = document.createElement('div');
			div.innerHTML = "Сайт компании: " + field54;
		wind.appendChild(div);
		
		var field57 = document.querySelector("input[name=field57]").value;
		div = document.createElement('div');
			div.innerHTML = "Должность: " + field57;
		wind.appendChild(div);
		
		var field82 = document.querySelector("input[name=field82]").value;
		div = document.createElement('div');
			div.innerHTML = "Общая фактическая выручка за последние 12 мес: " + field82;
		wind.appendChild(div);
		
		var field86 = document.querySelector("input[name=field86]").value;
		div = document.createElement('div');
			div.innerHTML = "Общие обороты компании за последние 12 мес: " + field86;
		wind.appendChild(div);
		
		var field162 = document.querySelector("input[name=field162]").value;
		div = document.createElement('div');
			div.innerHTML = "Мин сумма кредита со слов клиента: " + field162;
		wind.appendChild(div);
		
		var field163 = document.querySelector("input[name=field163]").value;
		div = document.createElement('div');
			div.innerHTML = "Макс сумма кредита из заявления: " + field163;
		wind.appendChild(div);
		
		div = document.createElement('div');
			div.innerHTML = "КИ";
		wind.appendChild(div);
		
		
		wind_.appendChild(wind);
		
		var d = Item['data'];
		
		wind = document.createElement('div');
		wind.id = "plan_list";
		wind.style = "position:absolute; margin: 265px 25px; width:1000px; height:250px; overflow: auto; border: solid 1px black;";
		
		if (only_online_banks)
		{
			var span = document.createElement('span');
				span.innerHTML = "До завершения ЗА доступны только онлайн-подачи!";
				span.style = "font-weight:bold; color:red;";
			wind.appendChild(span);
		}
		
		var table = document.createElement('table');
			table.id = "table_plans";
		//var tmp_text = '<table id="table_plans"><tr><td>Послед.</td><td>Банк</td><td>Точка выдачи</td><td>Аналитик</td><td>Сумма</td><td>Залог</td></tr>';
		//var tmp_text = '<tr><td>Послед.</td><td>Банк</td><td>Продукт</td><td>Точка контроля</td><td>Аналитик</td><td>Сумма</td><td>Залог</td></tr>';
		var tmp_text = '<tr><td>Послед.</td><td>Банк</td><td>Точка контроля</td><td>Аналитик</td><td>Сумма</td><td>Залог</td></tr>';
		var cc = 0;
		
		
		point_list = Item['point_list'];
		
		var pr_check = false;
		var pr_rework = false;
		
		if (Item['pr_check'] && Item['pr_check'] == 1)
		{
			d = Item['pr_mass'];
			pr_check = true;
		}
		
		var pr_approve = false;
		if (Item['pr_approve'] && Item['pr_approve'] == 1)
		{
			pr_approve = true;
		}
		
		var rework = new Array();
		var reject = new Array();
		var bankplist = new Array();
		var zal_mass = new Array();
		
		
		if (Item['pr_rework'] && Item['pr_rework'] == 1)
		{
			pr_rework = true;

			var dr = Item['pr_rej'];
			for (var key in dr)
			{
				var dd = dr[key];
				console.log('online_av: ' + dd['online_av'] + '/' + only_online_banks + '/' + parseInt(dd['online_av']));
			
				//if ((!parseInt(dd['online_av']) && only_online_banks) || !parseInt(dd['online_av']))
					//continue;
				
				if (!parseInt(dd['online_av']) && only_online_banks)
					continue;
				
				
				
				reject.push(dr[key]['bank']);
				dd['bank_name'] = dd['bank'];
				dd['bailn'] = (dd['product_name'] == 'беззалог') ? 0 : 1;
				if (!(dd['bank_name'] in zal_mass))
					zal_mass[dd['bank_name']] = new Array();
			
				if (zal_mass[dd['bank_name']].indexOf(parseInt(dd['bailn'])) == -1)
					zal_mass[dd['bank_name']].push(parseInt(dd['bailn']));
			}
				
		
			d = Item['pr_mass'];
			for (var key in d)
			{
				var dd = d[key];

				dd['prb_in_work'] = 1;
				dd['lim_min'] = dd['sum'];
				dd['bank_name'] = dd['bank'];
				dd['bank_name_descr'] = dd['bank_description'];
				dd['bailn'] = (dd['product_name'] == 'беззалог') ? 0 : 1;
				
				console.log('pr_rework');
				console.log(dd);
				console.log(dd['bailn']);
				
				if (!(dd['bank_name'] in zal_mass))
					zal_mass[dd['bank_name']] = new Array();
			
				if (zal_mass[dd['bank_name']].indexOf(parseInt(dd['bailn'])) == -1)
					zal_mass[dd['bank_name']].push(parseInt(dd['bailn']));
					
				rework.push(dd['bank']);
				cc++;
				var lim_min = parseInt(dd['lim_min']);				
				var lim_min_s = lim_min + ' р.';				
				lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				tmp_text += '<tr style="background-color:lightgreen;">';
				tmp_text += '<td><span>' + cc + '</span> <span onclick="javascript:upstr(this);"><img src="/img/green_arrow.png" style="cursor:pointer; height:16px;"></span> <span onclick="javascript:downstr(this);"><img src="/img/green_arrow.png" class="turn180" style="cursor:pointer; height:16px;"></span></td>';
				//tmp_text += '<td>' + dd['id'] + '</td>';
				//tmp_text += '<td>' + dd['bank_name_descr'] + '</td>';
				//tmp_text += '<td name="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';
				tmp_text += '<td name="bank_' + dd['bank_name'] + '" id="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';
				//tmp_text += '<td name="bankProduct">' + dd['product_name'] + '</td>';
				var an_en = false;
				if (dd['bank_name'] in point_list)
				{
					var pl = point_list[dd['bank_name']];
					var js_a = ' onchange="javscript:ch_point(this, ' + dd['bank_name'] + ');"';
					tmp_text += '<td><select' + js_a + '><option value="-1">Выберите</option>';// + dd['id'] + '</td>';
					for (var kpl in pl)
					{
						var selected_ = '';
						if (dd['point'] == pl[kpl]['id'])
						{
							selected_ = ' selected';
							if (parseInt(pl[kpl]['analyst']))
								an_en = true;
						}
						
						tmp_text += '<option value="' + pl[kpl]['id'] + '"' + selected_ + '>' + pl[kpl]['name'] + '</option>';
						
						
					}
					tmp_text += '</select></td>';
					var checked_ = '';
					if (dd['analytic'] == 1)
						checked_ = ' checked';
					
					var diabled_ = ' disabled';
					if (an_en)
						diabled_ = '';
					else
						checked_ = '';
					tmp_text += '<td><input type="checkbox" value="1"' + checked_ + ' onclick="javascript:check_set_point(this);"' + diabled_ + '></td>';
				}
				else
				{
					tmp_text += '<td><select><option value="-1">Нет точек</option></select></td>';
					tmp_text += '<td><input type="checkbox" value="0" disabled></td>';
				}
				
				tmp_text += '<td>' + '<input type="text" value="' + lim_min_s + '"></td>';
				tmp_text += '<td name="zal">' + dd['product_name'] + '</td>';
				tmp_text += '<td><span style="cursor:pointer;" onclick="remove_str(this);"><img src="/img/delete.png"></span></td>';
				tmp_text += '</tr>';
			}
		}
		
	//	console.log("rework:");
	//	console.log(rework);
		
		if (pr_rework)
			d = Item['data'];
	//	console.log("data:");
		
		for (var key in d)
		{
			var dd = d[key];
			
	//		console.log(dd);
			
			if (pr_check)
			{
				dd['prb_in_work'] = 1;
				dd['lim_min'] = dd['sum'];
				dd['bank_name'] = dd['bank'];
				dd['bank_name_descr'] = dd['bank_description'];
				dd['bailn'] = (dd['product_name'] == 'беззалог') ? 0 : 1;
			}
			
			if (parseInt(dd['prb_in_work']) == 0)
				continue;
			
			console.log('====== ' + dd['bank_name_descr'] + '====== ' + dd['bailn']);
			console.log('online_av: ' + dd['online_av'] + '/' + only_online_banks + '/' + parseInt(dd['online_av']));
			if (!parseInt(dd['online_av']) && only_online_banks)
				continue;
			
			//if ((!parseInt(dd['online_av']) && only_online_banks) || !parseInt(dd['online_av']))
				//	continue;
			//{
				
				//if ((parseInt(dd['online_av']) != 1) && only_online_banks)
					//continue;
			//}
			
			
			
			if (!(dd['bank_name'] in zal_mass))
				zal_mass[dd['bank_name']] = new Array();
			
			if (zal_mass[dd['bank_name']].indexOf(parseInt(dd['bailn'])) == -1)
				zal_mass[dd['bank_name']].push(parseInt(dd['bailn']));
			
			if (bankplist.indexOf(dd['bank_name']) != -1)
				continue;
			console.log('bankplist');
			
			bankplist.push(dd['bank_name']);
			
			if (pr_rework)
			{
				if (rework.indexOf(dd['bank_name']) != -1)
					continue;
			}
			
			console.log('not miss');
			cc++;
			var lim_min = parseInt(dd['lim_min']);				
			var lim_min_s = lim_min + ' р.';				
			lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			if (reject.indexOf(dd['bank_name']) != -1)
				tmp_text += '<tr style="background-color:#FF6060;">';
			else
				tmp_text += '<tr>';
			tmp_text += '<td><span>' + cc + '</span> <span onclick="javascript:upstr(this);"><img src="/img/green_arrow.png" style="cursor:pointer; height:16px;"></span> <span onclick="javascript:downstr(this);"><img src="/img/green_arrow.png" class="turn180" style="cursor:pointer; height:16px;"></span></td>';
			//tmp_text += '<td>' + dd['id'] + '</td>';
			//tmp_text += '<td>' + dd['bank_name_descr'] + '</td>';
			tmp_text += '<td name="bank_' + dd['bank_name'] + '" id="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';
			//tmp_text += '<td name="bankProduct">' + dd['product_name'] + '</td>';

			if (dd['bank_name'] in point_list)
			{
				var an_en = false;
				var pl = point_list[dd['bank_name']];
				var js_a = ' onchange="javscript:ch_point(this, ' + dd['bank_name'] + ');"';
				tmp_text += '<td><select' + js_a + '><option value="-1">Выберите</option>';// + dd['id'] + '</td>';
				for (var kpl in pl)
				{
					var selected_ = '';
					if (dd['point'] == pl[kpl]['id'])
					{
						selected_ = ' selected';
						if (parseInt(pl[kpl]['analyst']))
							an_en = true;
					}
					
					tmp_text += '<option value="' + pl[kpl]['id'] + '"' + selected_ + '>' + pl[kpl]['name'] + '</option>';
					
					
				}
				tmp_text += '</select></td>';
				var checked_ = '';
				if (dd['analytic'] == 1)
					checked_ = ' checked';
				
				var diabled_ = ' disabled';
				if (an_en)
					diabled_ = '';
				else
					checked_ = '';
				tmp_text += '<td><input type="checkbox" value="1"' + checked_ + ' onclick="javascript:check_set_point(this);"' + diabled_ + '></td>';
			}
			else
			{
				tmp_text += '<td><select><option value="-1">Нет точек</option></select></td>';
				tmp_text += '<td><input type="checkbox" value="0" disabled></td>';
			}
			tmp_text += '<td>' + '<input type="text" value="' + lim_min_s + '"></td>';
			//tmp_text += '<td name="zal"></td>';
			tmp_text += '<td name="zal">' + dd['product_name'] + '</td>';
			tmp_text += '<td><span style="cursor:pointer;" onclick="remove_str(this);"><img src="/img/delete.png"></span></td>';
			tmp_text += '</tr>';
			
			
		}
		
		d = Item['miss_mass2'];
		for (var key in d)
		{
			if (pr_check)
				continue;
			var dd = d[key];
			if (parseInt(dd['prb_in_work']) == 0)
				continue;
			
			console.log('====== ' + dd['bank_name_descr'] + '====== ' + dd['bailn']);
			console.log('online_av: ' + dd['online_av'] + '/' + only_online_banks + '/' + parseInt(dd['online_av']));
			
			if (!parseInt(dd['online_av']) && only_online_banks)
				continue;
			
			if (!(dd['bank_name'] in zal_mass))
				zal_mass[dd['bank_name']] = new Array();
			
			if (zal_mass[dd['bank_name']].indexOf(parseInt(dd['bailn'])) == -1)
				zal_mass[dd['bank_name']].push(parseInt(dd['bailn']));
			
			if (bankplist.indexOf(dd['bank_name']) != -1)
				continue;
			console.log('bankplist');
			
			bankplist.push(dd['bank_name']);
			
			if (pr_rework)
			{
				if (rework.indexOf(dd['bank_name']) != -1)
					continue;
			}
			cc++;
			var lim_min = parseInt(dd['lim_min']);				
			var lim_min_s = lim_min + ' р.';				
			lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			if (reject.indexOf(dd['bank_name']) != -1)
				tmp_text += '<tr style="background-color:#FF6060;">';
			else
				tmp_text += '<tr>';
			tmp_text += '<td><span>' + cc + '</span> <span onclick="javascript:upstr(this);"><img src="/img/green_arrow.png" style="cursor:pointer; height:16px;"></span> <span onclick="javascript:downstr(this);"><img src="/img/green_arrow.png" class="turn180" style="cursor:pointer; height:16px;"></span></td>';
			//tmp_text += '<td>' + dd['id'] + '</td>';
			tmp_text += '<td name="bank_' + dd['bank_name'] + '" id="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';
			//tmp_text += '<td name="bankProduct">' + dd['product_name'] + '</td>';

			if (dd['bank_name'] in point_list)
			{
				var an_en = false;
				var pl = point_list[dd['bank_name']];
				var js_a = ' onchange="javscript:ch_point(this, ' + dd['bank_name'] + ');"';
				tmp_text += '<td><select' + js_a + '><option value="-1">Выберите</option>';// + dd['id'] + '</td>';
				for (var kpl in pl)
				{
					var selected_ = '';
					if (dd['point'] == pl[kpl]['id'])
					{
						selected_ = ' selected';
						if (parseInt(pl[kpl]['analyst']))
							an_en = true;
					}
					
					tmp_text += '<option value="' + pl[kpl]['id'] + '"' + selected_ + '>' + pl[kpl]['name'] + '</option>';
					
					
				}
				tmp_text += '</select></td>';
				var checked_ = '';
				if (dd['analytic'] == 1)
					checked_ = ' checked';
				
				var diabled_ = ' disabled';
				if (an_en)
					diabled_ = '';
				else
					checked_ = '';
				tmp_text += '<td><input type="checkbox" value="1"' + checked_ + ' onclick="javascript:check_set_point(this);"' + diabled_ + '></td>';
			}
			else
			{
				tmp_text += '<td><select><option value="-1">Нет точек</option></select></td>';
				tmp_text += '<td><input type="checkbox" value="0" disabled></td>';
			}
			tmp_text += '<td>' + '<input type="text" value="' + lim_min_s + '"></td>';
			tmp_text += '<td name="zal"></td>';
			tmp_text += '<td><span style="cursor:pointer;" onclick="remove_str(this);"><img src="/img/delete.png"></span></td>';
			tmp_text += '</tr>';
		}
		
		//tmp_text += '</table>';
		table.innerHTML = tmp_text;
		wind.appendChild(table);
		
		tmp_text = '';
		if (pr_rework)
		{
			tmp_text += '<br>Список отклоненных банков:<br>';
			d = Item['pr_rej'];
			for (var key in d)
			{
				var dd = d[key];
				dd['prb_in_work'] = 1;
				dd['lim_min'] = dd['sum'];
				dd['bank_name'] = dd['bank'];
				dd['bank_name_descr'] = dd['bank_description'];
				var lim_min = parseInt(dd['lim_min']);				
				var lim_min_s = lim_min + ' р.';				
				lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				tmp_text += dd['bank_name_descr'] + ' на сумму ' + lim_min_s + '<br>';				
			}
		}
		
		wind.innerHTML += tmp_text;
		
		
		
		
		/*div = document.createElement('div');
			div.innerHTML = "<br>Текст:<br>";
			
			var text_inp = document.createElement('textarea');
				text_inp.id = "text_for_plan";
				text_inp.rows = "5";
				text_inp.cols = "50";
			
			div.appendChild(text_inp);
		wind.appendChild(div);
		wind.innerHTML += "<br>";
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			b.setAttribute("onclick", "javascript:plan_send();");
			
			
		
		
		wind.appendChild(b);
		*/
		wind_.appendChild(wind);
		
		wind = document.createElement('div');
		wind.style = "position:absolute; margin: 530px 25px; width:1000px; height:250px;";
		if (pr_approve)
			wind.innerHTML = '<font color="green">План работ одобрен!</font> ';
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			if (pr_check)
			{
				b.setAttribute("disabled", "true");
				b.innerHTML = "Ожидайте проверки";
			}
			if (pr_approve)
			{
				//b.setAttribute("disabled", "true");
				b.innerHTML = "Отправить на согласование новый план работ";
			}
				
			b.setAttribute("onclick", "javascript:plan_send2();");
		wind.appendChild(b);
		wind_.appendChild(wind);
	//}
		console.log('start fill zalog');
		console.log(zal_mass);
		for(var b in zal_mass)
		{
			var bb = zal_mass[b];
			var bbb = 'td[name="bank_' + b + '"]';
			var td_b = document.querySelector('#bank_' + b);
			console.log(td_b);
			
			if (td_b)
			{
				console.log(bb);
				if (bb.length == 1)
				{
					var tmp_rrr = ((parseInt(bb[0]) == 0) ? "беззалог" : ((parseInt(bb[0]) == 1) ? "залог" : '-'));
					console.log(tmp_rrr);
					td_b.parentNode.querySelector('td[name="zal"]').innerHTML = tmp_rrr;
				}
				else if (bb.length == 2)
				{
					var td_v = td_b.parentNode.querySelector('td[name="zal"]').innerHTML;
					var bez_s = (td_v == 'беззалог') ? ' selected' : '';
					var zal_s = (td_v == 'залог') ? ' selected' : '';
					td_b.parentNode.querySelector('td[name="zal"]').innerHTML = '<select><option value="">Выберите</option><option value="беззалог"' + bez_s + '>беззалог</option><option value="залог"' + zal_s + '>залог</option></select>';
				}
				else
					td_b.parentNode.querySelector('td[name="zal"]').innerHTML = '-';
			}
			
		}
	
	}
		else 
		{
			alert(Item['msg']);
		}
	});
}


function check_set_point(el)
{
	if (el.checked)
	{
		var sel = el.parentNode.parentNode.querySelector('select');
		if (sel.options[sel.selectedIndex].value == -1)
		{
			alert('Сначала выберите точку контроля!');
			el.checked = false;
		}
	}
}

function online_sents()
{
	show('block');
	var data = "block=anketa&action=show_prb&id=" + anketa_id + "&get_online_history=1";
	ajaxQuery('/ajax.php','POST', data, true, function(req)
	{
		var wind_ = document.querySelector("#window");
		wind_.style.width = "1200px";
		wind_.style.height = "700px";
		wind_.style.margin = "50px auto";
		wind_.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
		
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var d = Item['data'];
			var tmp_str = '';
			tmp_str += 'Доступны для подачи:<br><br>';
			tmp_str += '<table cellspacing="15px">';
			tmp_str += '<tr><td>№</td><td>Банк</td><td>Продукт</td><td>Сумма</td><td></td></tr>';
			for (var i in d)
			{
				var product = d[i];
				if (product['online_av'] && parseInt(product['online_av']) == 1)
				{
					//tmp_str += product['bank_name_descr'];
					var ttt = '<td>' + product['id'] + '</td><td>' + product['product_name'] + '</td><td>' + product['bank_name_descr'] + '</td>';
					//console.log(ttt);
					tmp_str += '<tr><td>' + product['id'] + '</td><td>' + product['product_name'] + '</td><td>' + product['bank_name_descr'] + '</td><td><input type="text" name="sum" value="' + product['lim_max'] + '"></td><td><span style="cursor:pointer; font-style:italic;" onclick="javascript:sent_bank_online(' + product['id'] + ', ' + product['bank_name'] + ', \'' + ttt + '\', this);">Подать</span></td></tr>';
				}
			}
			tmp_str += '</table>';
			
			
			tmp_str += '<br>История подач:<br>';
			tmp_str += '<table id="table_his_online" cellspacing="15px">';
			tmp_str += '<tr><td>№</td><td>Банк</td><td>Продукт</td><td>Сумма</td><td>Ответ</td></tr>';
			if (Item['online_history'])
			{
				
				var online_history = Item['online_history'];
				for (var i in online_history)
				{
					var str = online_history[i];
					tmp_str += '<tr><td>' + str['product'] + '</td><td>' + str['product_name'] + '</td><td>' + str['bank_name_descr'] + '</td><td>' + str['sum'] + '</td><td>';
					if (str['answer'] == 'approve')
						tmp_str += '<font color="green">Одобрено</font>';
					else if (str['answer'] == 'preapprove')
						tmp_str += '<font color="green">Предодобрено</font>';
					else if (str['answer'] == 'reject')
						tmp_str += '<font color="red">отклонено</font>';
					else if (str['answer'] == 'techReject')
						tmp_str += '<font color="red">Отклонено по тех.причинам</font>';
					else
					{
						tmp_str += '<select onchange="javascript:ch_online_b_answer(this, ' + str['id'] + ');"><option value="sent">Выберите</option><option value="approve">Одобрено</option><option value="reject">Отклонено</option></select>';
					}
					tmp_str += '</td></tr>';
				}
			}
			tmp_str += '</table>';
			wind_.innerHTML += tmp_str;
		}
	});
}

function ch_online_b_answer(el, id)
{
	var val = el.options[el.selectedIndex].value;
	var data = "block=anketa&action=ch_online_b_answer&formid=" + anketa_id + "&id=" + id + "&val=" + val;
	ajaxQuery('/ajax.php','POST', data, true, function(req)
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Изменено успешно!');
		}
		else
		{
			alert('Ошибка');
		}
	});
}

function sent_bank_online(prid, bid, str, el)
{
	var sum = el.parentNode.parentNode.querySelector('input[name="sum"]').value;
	console.log(sum);
	var data = "block=anketa&action=sent_bank_online&id=" + anketa_id + "&prid=" + prid + "&bid=" + bid + "&sum=" + sum;
	ajaxQuery('/ajax.php','POST', data, true, function(req)
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var t = document.querySelector('#table_his_online');
			var str_ = str + '<td>' + sum + '</td><td data-tooltip="Для установки ответа обновите страницу" style="font-style:italic;">Отправлено</td>';
			//console.log(str_);
			var tr = document.createElement('tr');
			tr.innerHTML = str_;
			t.appendChild(tr);
		}
		else
		{
			alert('Ошибка');
		}
	});
}

function check_work_plan()
{
	show('block');
	var data = "block=anketa&action=show_prb&id=" + anketa_id + "&pr=1$check=1";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
	
	//if (last_wind != 'set_work_plan')
	//{
		last_wind = 'set_work_plan';
		var wind_ = document.querySelector("#window");
		wind_.style.width = "1200px";
		wind_.style.height = "700px";
		wind_.style.margin = "10px auto";
		wind_.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
		
		var wind = document.createElement('div');
		wind.style = "position:absolute; margin: 0px 25px; width:1000px; height:300px; overflow: auto; border: solid 1px black;";
		
		var div = document.createElement('div');
		if (anketa_main_form == 0)
			div.innerHTML = "Статус анеты: основная" ;
		else
			div.innerHTML = "Статус анеты: поданкета. Основная - <a target=_blank href=\"/details/" + anketa_main_form + "/\">#" + anketa_main_form + "</a>" ;
		wind.appendChild(div);
		
		var lname = document.querySelector("input[name=lastname]").value;
		var fname = document.querySelector("input[name=firstname]").value;
		var mname = document.querySelector("input[name=middlename]").value;
		div = document.createElement('div');
			div.innerHTML = "ФИО: " + lname + " " + fname + " " + mname;
		wind.appendChild(div);
		
		
		div = document.createElement('div');
		var birth_day = document.querySelector('input[name="birth_day"]').value;
		var reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
		var mass;
		var a_drm = 21;
		var a_dry = 1980;
		if (reg.test(birth_day))
		{
			mass = reg.exec(birth_day);
			a_drm = mass[2];
			a_dry = mass[3];
			var now_d = new Date();
			var now_m = now_d.getMonth()+1;
			var now_y = 1900 + now_d.getYear();
			var now_age = now_y*12 + now_m - a_drm - 12*a_dry;
			//now_age = now_age/12;
			var mna = now_age % 12;
		
		
			div.innerHTML = "Возраст: " + Math.floor(now_age/12) + " лет и " + mna + " мес.";
		}
		else
			div.innerHTML = "Возраст: не заполнена дата рождения";
		wind.appendChild(div);
		
		
		var field1 = document.querySelector("input[name=field1]").value;
		div = document.createElement('div');
			div.innerHTML = "Регион обращения: " + field1;
		wind.appendChild(div);
		
		var field3 = document.querySelectorAll('input[name*="field3["]');
		var tmp_f3 = "";
		for(key in field3)
		{
			if (field3[key].checked)
			{
				if (tmp_f3 != '')
					tmp_f3 += ', ';
				tmp_f3 += field3[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Цель кредита: " + tmp_f3;
		wind.appendChild(div);
		
		
		//
		var span_field24 = document.querySelector("#span_field24").innerHTML;
		div = document.createElement('div');
			div.innerHTML = "Адрес регистрации: " + span_field24;
		wind.appendChild(div);
		
		var field176 = document.querySelectorAll("input[name=field176]");
		var field176_t = 'не указано';
		for(key in field176)
		{
			if (field176[key].checked)
				field176_t = field176[key].value;
		}
		div = document.createElement('div');
			div.innerHTML = "Заинтересованные лица: " + field176_t;
		wind.appendChild(div);
		
		var field30 = document.querySelector("select[name=field30]");
		console.log(field30);
		field30 = field30.options[field30.selectedIndex].value;
		
		if (field30 == '')
			field30 = "Не заполнено";
		div = document.createElement('div');
			div.innerHTML = "Семейное положение: " + field30;
		wind.appendChild(div);
		
		var field75 = document.querySelector("input[name=field75]").value;
		div = document.createElement('div');
			div.innerHTML = "Общая сумма доходов: " + field75;
		wind.appendChild(div);
		
		var field94 = document.querySelector("input[name=field94]").value;
		div = document.createElement('div');
			div.innerHTML = "Сумма ежемесячных платежей по текущим кредитам: " + field94;
		wind.appendChild(div);
		
		var field47 = document.querySelector("select[name=field47]");
		field47 = field47.options[field47.selectedIndex].value;
		if (field47 == '')
			field47 = "Не заполнено";
		div = document.createElement('div');
			div.innerHTML = "Трудоустройство: " + field47;
		wind.appendChild(div);
		
		var field77 = document.querySelectorAll('input[name*="field77["]');
		var tmp_f77 = "";
		for(key in field77)
		{
			if (field77[key].checked)
			{
				if (tmp_f77 != '')
					tmp_f77 += ', ';
				tmp_f77 += field77[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Вариант подтверждения дохода: " + tmp_f77;
		wind.appendChild(div);
		
		var field8 = document.querySelectorAll('input[name*="field8["]');
		var tmp_f8 = "";
		for(key in field8)
		{
			if (field8[key].checked)
			{
				if (tmp_f8 != '')
					tmp_f8 += ', ';
				tmp_f8 += field8[key].value;
			}
		}
		div = document.createElement('div');
			div.innerHTML = "Активы клиента: " + tmp_f8;
		wind.appendChild(div);
		
		var field106 = document.querySelector("div[name=field106]");
		var inps = field106.querySelectorAll('input');
		console.log(inps);
		var tmp_f106 = "";
		
		for (var i = 0; i < inps.length; i++)
		{
			if (tmp_f106 != '')
				tmp_f106 += ', ';
			tmp_f106 += inps[i].value;
		}
		div = document.createElement('div');
			div.innerHTML = "Банки, где есть з/п счета: " + tmp_f106;
		wind.appendChild(div);
		
		var field92 = document.querySelector("div[id=field92]");
		var inps = field92.querySelectorAll('span');
		console.log(inps);
		var tmp_f92 = "";
		
		for (var i = 0; i < inps.length; i++)
		{
			if (tmp_f92 != '')
				tmp_f92 += ', ';
			tmp_f92 += inps[i].innerHTML;
		}
		div = document.createElement('div');
			div.innerHTML = "Банки-кредиторы: " + tmp_f92;
		wind.appendChild(div);
		
		
		var field49 = document.querySelector("input[name=field49]").value;
		div = document.createElement('div');
			div.innerHTML = "Название компании: " + field49;
		wind.appendChild(div);
		
		var span_field55 = document.querySelector("#span_field55").innerHTML;
		div = document.createElement('div');
			div.innerHTML = "Фактический адрес компании: " + span_field55;
		wind.appendChild(div);
		
		var field50 = document.querySelector("input[name=field50]").value;
		div = document.createElement('div');
			div.innerHTML = "ИНН: " + field50;
		wind.appendChild(div);
		
		var field54 = document.querySelector("input[name=field54]").value;
		div = document.createElement('div');
			div.innerHTML = "Сайт компании: " + field54;
		wind.appendChild(div);
		
		var field57 = document.querySelector("input[name=field57]").value;
		div = document.createElement('div');
			div.innerHTML = "Должность: " + field57;
		wind.appendChild(div);
		
		var field82 = document.querySelector("input[name=field82]").value;
		div = document.createElement('div');
			div.innerHTML = "Общая фактическая выручка за последние 12 мес: " + field82;
		wind.appendChild(div);
		
		var field86 = document.querySelector("input[name=field86]").value;
		div = document.createElement('div');
			div.innerHTML = "Общие обороты компании за последние 12 мес: " + field86;
		wind.appendChild(div);
		
		var field162 = document.querySelector("input[name=field162]").value;
		div = document.createElement('div');
			div.innerHTML = "Мин сумма кредита со слов клиента: " + field162;
		wind.appendChild(div);
		
		var field163 = document.querySelector("input[name=field163]").value;
		div = document.createElement('div');
			div.innerHTML = "Макс сумма кредита из заявления: " + field163;
		wind.appendChild(div);
		
		div = document.createElement('div');
			div.innerHTML = "КИ";
		wind.appendChild(div);
		
		
		wind_.appendChild(wind);
		
		var d = Item['data'];
		var bankplist = new Array();
		
		wind = document.createElement('div');
		wind.id = "plan_list";
		wind.style = "position:absolute; margin: 325px 25px; width:1000px; height:200px; overflow: auto; border: solid 1px black;";
		
		//var tmp_text = '<table><tr><td>Послед.</td><td>Банк</td><td>Сумма</td><td>Уд.</td></tr>';
		var tmp_text = '<table><tr><td>Послед.</td><td>Банк</td><td>Точка контроля</td><td>Аналитик</td><td>Сумма</td><td>Залог</td><td>Уд.</td></tr>';
		var cc = 0;
		
		var pr_check = false;
		if (Item['pr_check'] && Item['pr_check'] == 1)
		{
			d = Item['pr_mass'];
			pr_check = true;
		}
		
		point_list = Item['point_list'];
		
		var pr_approve = false;
		if (Item['pr_approve'] && Item['pr_approve'] == 1)
		{
			d = Item['pr_mass'];
			pr_approve = true;
		}
			
		
		for (var key in d)
		{
			var dd = d[key];
			
			if (dd['status'] == 'reject')
				continue;
			
			if (pr_check || pr_approve)
			{
				dd['prb_in_work'] = 1;
				dd['lim_min'] = dd['sum'];
				dd['bank_name'] = dd['bank'];
				dd['bank_name_descr'] = dd['bank_description'];
			}
			
			
			
			if (parseInt(dd['prb_in_work']) == 0)
				continue;
			
			
			if (bankplist.indexOf(dd['bank_name']) != -1)
				continue;
			
			bankplist.push(dd['bank_name']);
			
			cc++;
			var lim_min = parseInt(dd['lim_min']);				
			var lim_min_s = lim_min + ' р.';				
			lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			
			var rejected = '';
			if (dd['status'] == 'reject')
				rejected = ' style="background:OrangeRed;"';
			tmp_text += '<tr' + rejected + '>';
			tmp_text += '<td><span>' + cc + '</span> <span onclick="javascript:upstr(this);"><img src="/img/green_arrow.png" style="cursor:pointer; height:16px;"></span> <span onclick="javascript:downstr(this);"><img src="/img/green_arrow.png" class="turn180" style="cursor:pointer; height:16px;"></span></td>';
			//tmp_text += '<td>' + dd['id'] + '</td>';
			//tmp_text += '<td>' + dd['bank_name_descr'] + '</td>';
			tmp_text += '<td name="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';

			if (dd['bank_name'] in point_list)
			{
				var an_en = false;
				var pl = point_list[dd['bank_name']];
				var js_a = ' onchange="javscript:ch_point(this, ' + dd['bank_name'] + ');"';
				tmp_text += '<td><select' + js_a + '><option value="-1">Выберите</option>';// + dd['id'] + '</td>';
				for (var kpl in pl)
				{
					var selected_ = '';
					if (dd['point'] == pl[kpl]['id'])
					{
						selected_ = ' selected';
						if (parseInt(pl[kpl]['analyst']))
							an_en = true;
					}
					
					tmp_text += '<option value="' + pl[kpl]['id'] + '"' + selected_ + '>' + pl[kpl]['name'] + '</option>';
					
					
				}
				tmp_text += '</select></td>';
				var checked_ = '';
				if (dd['analytic'] == 1)
					checked_ = ' checked';
				
				var diabled_ = ' disabled';
				if (an_en)
					diabled_ = '';
				else
					checked_ = '';
				tmp_text += '<td><input type="checkbox" value="1"' + checked_ + ' onclick="javascript:check_set_point(this);"' + diabled_ + '></td>';
			}
			else
			{
				tmp_text += '<td><select><option value="-1">Нет точек</option></select></td>';
				tmp_text += '<td><input type="checkbox" value="0" disabled></td>';
			}
			tmp_text += '<td>' + '<input type="text" value="' + lim_min_s + '"></td>';
			if (dd['product_name'])
				tmp_text += '<td>' + dd['product_name'] + '</td>';
			else
				tmp_text += '<td>-</td>';
			tmp_text += '<td><span style="cursor:pointer;" onclick="remove_str(this);"><img src="/img/delete.png"></span></td>';
			
			if (rejected != '')
				tmp_text += '<td>отклонен</td>';
			else
				tmp_text += '<td></td>';
			tmp_text += '</tr>';
		}
		
		d = Item['miss_mass2'];
		for (var key in d)
		{
			if (pr_check || pr_approve)
				continue;
			var dd = d[key];
			
			if (parseInt(dd['prb_in_work']) == 0)
				continue;
			cc++;
			var lim_min = parseInt(dd['lim_min']);				
			var lim_min_s = lim_min + ' р.';				
			lim_min_s = lim_min_s.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			tmp_text += '<tr>';
			tmp_text += '<td><span>' + cc + '</span> <span onclick="javascript:upstr(this);"><img src="/img/green_arrow.png" style="cursor:pointer; height:16px;"></span> <span onclick="javascript:downstr(this);"><img src="/img/green_arrow.png" class="turn180" style="cursor:pointer; height:16px;"></span></td>';
			//tmp_text += '<td>' + dd['id'] + '</td>';
			tmp_text += '<td name="bank_' + dd['bank_name'] + '">' + dd['bank_name_descr'] + '</td>';

			if (dd['bank_name'] in point_list)
			{
				var an_en = false;
				var pl = point_list[dd['bank_name']];
				var js_a = ' onchange="javscript:ch_point(this, ' + dd['bank_name'] + ');"';
				tmp_text += '<td><select' + js_a + '><option value="-1">Выберите</option>';// + dd['id'] + '</td>';
				for (var kpl in pl)
				{
					var selected_ = '';
					if (dd['point'] == pl[kpl]['id'])
					{
						selected_ = ' selected';
						if (parseInt(pl[kpl]['analyst']))
							an_en = true;
					}
					
					tmp_text += '<option value="' + pl[kpl]['id'] + '"' + selected_ + '>' + pl[kpl]['name'] + '</option>';
					
					
				}
				tmp_text += '</select></td>';
				var checked_ = '';
				if (dd['analytic'] == 1)
					checked_ = ' checked';
				
				var diabled_ = ' disabled';
				if (an_en)
					diabled_ = '';
				else
					checked_ = '';
				tmp_text += '<td><input type="checkbox" value="1"' + checked_ + ' onclick="javascript:check_set_point(this);"' + diabled_ + '></td>';
			}
			else
			{
				tmp_text += '<td><select><option value="-1">Нет точек</option></select></td>';
				tmp_text += '<td><input type="checkbox" value="0" disabled></td>';
			}
			tmp_text += '<td>' + '<input type="text" value="' + lim_min_s + '"></td>';
			if (dd['product_name'])
				tmp_text += '<td>' + dd['product_name'] + '</td>';
			else
				tmp_text += '<td>-</td>';
			tmp_text += '<td><span style="cursor:pointer;" onclick="remove_str(this);"><img src="/img/delete.png"></span></td>';
			tmp_text += '</tr>';
		}
		
		tmp_text += '</table>';
		
		wind.innerHTML = tmp_text;
		
		
		/*div = document.createElement('div');
			div.innerHTML = "<br>Текст:<br>";
			
			var text_inp = document.createElement('textarea');
				text_inp.id = "text_for_plan";
				text_inp.rows = "5";
				text_inp.cols = "50";
			
			div.appendChild(text_inp);
		wind.appendChild(div);
		wind.innerHTML += "<br>";
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			b.setAttribute("onclick", "javascript:plan_send();");
			
			
		
		
		wind.appendChild(b);
		*/
		wind_.appendChild(wind);
		
		wind = document.createElement('div');
		wind.style = "position:absolute; margin: 550px 25px; width:1000px; height:75px;";
		wind.innerHTML = "Текст:<br>";
		var text_inp = document.createElement('textarea');
				text_inp.id = "text_for_plan";
				text_inp.style = "height:100%; width: 600px;";
				/*text_inp.rows = "5";
				text_inp.cols = "100";*/
			
			wind.appendChild(text_inp);
		
		wind.innerHTML += " ";
		
		
		if (pr_approve)
		{
			var b = document.createElement('button');
				b.innerHTML = "План утвержден";
				b.style = "background-color:lightgreen; color:black;";
				b.setAttribute("disabled", "true");
				//b.setAttribute("onclick", "javascript:plan_approve();");
			wind.appendChild(b);
		}
		else
		{
			
			if (!pr_check)
			{
				var b = document.createElement('button');
					b.setAttribute("disabled", "true");
					b.innerHTML = "План работ еще не сформирован";
					b.setAttribute("onclick", "javascript:plan_approve();");
				wind.appendChild(b);
			}
			else
			{
		
				var b = document.createElement('button');
					b.innerHTML = "Утвердить";
					b.setAttribute("onclick", "javascript:plan_approve();");
				wind.appendChild(b);
				
				wind.innerHTML += " ";
				
				b = document.createElement('button');
					b.innerHTML = "На доработку";
					b.setAttribute("onclick", "javascript:plan_reject();");
				wind.appendChild(b);
			}
		}
		wind_.appendChild(wind);
	//}
	
	}
		else 
		{
			alert(Item['msg']);
		}
	});
}

function ch_point(el, bid)
{
	var sel = el.options[el.selectedIndex].value;
	var next = el.parentNode.nextSibling;
	console.log(next.innerHTML);
	console.log(bid);
	var points = point_list[bid];
	console.log(points);
	var hit = false;
	var point;
	for (var kpl in points)
	{
		if (sel == points[kpl]['id'])
		{
			hit = true;
			point = points[kpl];
			break;
		}
	}
	console.log(hit);
	console.log(point);
	console.log(point['analyst']);
	
	if (hit)
	{
		var inp = next.querySelector('input');
		if (parseInt(point['analyst']))
			inp.disabled = false;
		else
		{
			inp.disabled = true;
			inp.checked = false;
		}
	}
}

function plan_approve()
{
	
	var plan_list = document.querySelector('#plan_list');
	var table = plan_list.querySelector('table');
	var list = table.querySelectorAll('tr');

	var tmp_str = '';
	
	for (var i = 1; i<list.length; i++)
	{
		var tdl = list[i].querySelectorAll('td');
		var spid = tdl[0].querySelector('span').innerHTML;
		console.log(tdl[1]);
		
		var bankid = tdl[1].getAttribute('name');
		bankid = bankid.replace('bank_', '');
		
		
		
		var sel = tdl[2].querySelector('select');
		var sel_val = sel.options[sel.selectedIndex].value;
		
		var an = (tdl[3].querySelector('input').checked) ? 1 : 0;
		
		var sum = tdl[4].querySelector('input').value.replace(/\D+/g,"");
		var zal = tdl[5].innerHTML;
		
		
		if (tmp_str != '')
			tmp_str += '|';
		
		tmp_str += spid + ',' + bankid + ',' + sum + ',' + sel_val + ',' + an + ',' + zal;
	}
	
	var msg = document.querySelector('#text_for_plan').value;
	
	var data = "block=anketa&action=plan_approve&id=" + anketa_id + "&msg=" + msg + "&plan=" + tmp_str;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('План одобрен');
			//show('none');
			//reload_log_data();
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function plan_reject()
{
	var plan_list = document.querySelector('#plan_list');
	var table = plan_list.querySelector('table');
	var list = table.querySelectorAll('tr');

	var tmp_str = '';
	
	for (var i = 1; i<list.length; i++)
	{
		var tdl = list[i].querySelectorAll('td');
		var spid = tdl[0].querySelector('span').innerHTML;
		//console.log(tdl[1]);
		
		var bankid = tdl[1].getAttribute('name');
		bankid = bankid.replace('bank_', '');
		
		
		var sel = tdl[2].querySelector('select');
		var sel_val = sel.options[sel.selectedIndex].value;
		
		var an = (tdl[3].querySelector('input').checked) ? 1 : 0;
		var sum = tdl[4].querySelector('input').value.replace(/\D+/g,"");
		var zal = tdl[5].innerHTML;
		
		if (tmp_str != '')
			tmp_str += '|';
		
		tmp_str += spid + ',' + bankid + ',' + sum + ',' + sel_val + ',' + an + ',' + zal;
	}
	
	
	
	var msg = document.querySelector('#text_for_plan').value;
	var data = "block=anketa&action=plan_reject&id=" + anketa_id + "&msg=" + msg + "&plan=" + tmp_str;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('План отправлен на доработку');
			//show('none');
			//reload_log_data();
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function remove_str(el)
{
	var tr = el.parentNode.parentNode;
	if (tr.nextSibling)
	{
		do
		{
			tr = tr.nextSibling;
			var sp = tr.querySelector('span');
			sp.innerHTML = parseInt(sp.innerHTML) - 1;
			
		}
		while(tr.nextSibling);
	}
	el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
}

function upstr(el)
{
	var tr = el.parentNode.parentNode;
	var sp = el.parentNode.querySelector('span');
	
	if (parseInt(sp.innerHTML) == 1)
		return;
	
	sp.innerHTML = parseInt(sp.innerHTML) - 1;
	
	var sp2 = tr.previousSibling.querySelector('span');
	sp2.innerHTML = parseInt(sp2.innerHTML) + 1;

	var table = tr.parentNode;
	
	console.log(tr.previousSibling);
	if (!tr.previousSibling)
		return;
	table.insertBefore(tr, tr.previousSibling);
}

function downstr(el)
{
	var tr = el.parentNode.parentNode;
	
	if (!tr.nextSibling)
		return;
	
	var sp = el.parentNode.querySelector('span');
	sp.innerHTML = parseInt(sp.innerHTML) + 1;
	
	var sp2 = tr.nextSibling.querySelector('span');
	sp2.innerHTML = parseInt(sp2.innerHTML) - 1;

	var table = tr.parentNode;

	//table.insertBefore(tr, tr.nextSibling);
	insertAfter(tr, tr.nextSibling);
}

function insertAfter(elem, refElem) {
  return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
}

function set_pre_decision()
{
	show('block');
	if (last_wind != 'set_pre_decision')
	{
		last_wind = 'set_pre_decision';
		var wind = document.querySelector("#window");
		wind.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
		
		var div = document.createElement('div');
			div.innerHTML = "<br>Текст:<br>";
			
			var text_inp = document.createElement('textarea');
				text_inp.id = "text_for_dec";
				text_inp.rows = "5";
				text_inp.cols = "50";
			
			div.appendChild(text_inp);
		wind.appendChild(div);
		wind.innerHTML += "<br>";
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			b.setAttribute("onclick", "javascript:decision_send();");
		
		wind.appendChild(b);
	}
}

function set_work_plan_dayli()
{
	show('block');
	if (last_wind != 'set_work_plan_dayli')
	{
		last_wind = 'set_work_plan_dayli';
		var wind = document.querySelector("#window");
		wind.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
		
		var div = document.createElement('div');
			div.innerHTML = "<br>Текст:<br>";
			
			var text_inp = document.createElement('textarea');
				text_inp.id = "text_for_wpd";
				text_inp.rows = "5";
				text_inp.cols = "50";
			
			div.appendChild(text_inp);
		wind.appendChild(div);
		wind.innerHTML += "<br>";
		var b = document.createElement('button');
			b.innerHTML = "Отправить";
			b.setAttribute("onclick", "javascript:wpd_send();");
		
		wind.appendChild(b);
	}
}

function plan_send()
{
		
	var tmp_text = document.querySelector("#text_for_plan").value;

	var data = "block=anketa&action=plan_send&id=" + anketa_id + "&msg=" + tmp_text;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('План добавлен');
			show('none');
			reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function plan_send2()
{
	var plan_list = document.querySelector('#plan_list');
	
	var table = plan_list.querySelector('table');
	
	var list = table.querySelectorAll('tr');
	
	
	
	var tmp_str = '';
	for (var i = 1; i<list.length; i++)
	{
		var tdl = list[i].querySelectorAll('td');
		var spid = tdl[0].querySelector('span').innerHTML;
		console.log(tdl[1]);
		
		var bankid = tdl[1].getAttribute('name');
		bankid = bankid.replace('bank_', '');
		
		
		
		var sel = tdl[2].querySelector('select');
		var sel_val = sel.options[sel.selectedIndex].value;
		
		var an = (tdl[3].querySelector('input').checked) ? 1 : 0;
		var sum = tdl[4].querySelector('input').value.replace(/\D+/g,"");
		
		var prname = tdl[5];
		if (/select/i.test(prname.innerHTML))
		{
			prname = prname.querySelector('select');
			prname = prname.options[prname.selectedIndex].value;
		}
		else
			prname = prname.innerHTML;
		
		if (tmp_str != '')
			tmp_str += '|';
		
		tmp_str += spid + ',' + bankid + ',' + sum + ',' + sel_val + ',' + an + ',' + prname;
	}
	console.log(tmp_str);
	
	//var tmp_text = document.querySelector("#text_for_plan").value;

	var data = "block=anketa&action=plan_send2&id=" + anketa_id + "&msg=" + tmp_str;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('План добавлен');
			show('none');
			reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function decision_send()
{
	var tmp_text = document.querySelector("#text_for_dec").value;

	var data = "block=anketa&action=decision_send&id=" + anketa_id + "&msg=" + tmp_text;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Запись о верификации ТМ добавлена');
			show('none');
			reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function wpd_send()
{
	var tmp_text = document.querySelector("#text_for_wpd").value;

	var data = "block=anketa&action=wpd_send&id=" + anketa_id + "&msg=" + tmp_text;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('План на день добавлен');
			show('none');
			reload_log_data();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

var sms_staff_list = new Array();
function send_sms()
{
	show('block');
	last_wind = 'send_sms';
	var wind = document.querySelector("#window");
	wind.style.marginTop = "100px";

	wind.innerHTML = '<img class="close" onclick="show(\'none\')" src="/img/close.png">';
	var data = "block=anketa&action=get_data4sms&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			sms_staff_list.length = 0
			var list_phones = Item['list_phones'];
			var sel = document.createElement('select');
				sel.name = "selphone";
				sel.setAttribute("onchange", "javascript:sel_phone(this);");
				var opt = document.createElement('option');
					opt.value = "-1";
					opt.innerHTML = "Выберите";
				sel.appendChild(opt);
			for (var key in list_phones)
			{
				var opt = document.createElement('option');
					opt.value = list_phones[key];
					opt.innerHTML = list_phones[key];
				sel.appendChild(opt);
			}
			
			opt = document.createElement('option');
				opt.value = "inp";
				opt.innerHTML = "Иной";
			sel.appendChild(opt);
			wind.appendChild(sel);
			
			wind.innerHTML += " ";
			
			var inp = document.createElement('input');
				inp.id = "selphone";
				inp.disabled = "true";
			wind.appendChild(inp);
			
			
			var div = document.createElement('div');
				div.innerHTML = "<br>";
				
			if('sub_status' in Item)
			{
				if (Item['sub_status'] == 'main')
				{
					div.innerHTML += " Сотрудник: ";
					sel = document.createElement('select');
						sel.id = 'select_staff_sms';
					sel.setAttribute("onchange", "javascript:write_template2(this);");
					
						opt = document.createElement('option');
						opt.innerHTML = "Выберите";
						opt.value = "";
					sel.appendChild(opt);
					
					if('staff_list' in Item)
					{
						var staff_list = Item['staff_list'];
						for (var key in staff_list)
						{
							sms_staff_list[key] = staff_list[key];
							opt = document.createElement('option');
								//opt.value = staff_list[key]['id'];
								opt.value = key;
								opt.innerHTML = staff_list[key]['lastname'] + ' ' + staff_list[key]['firstname'];
							sel.appendChild(opt);
						}
					}
					div.appendChild(sel);
				}
				div.innerHTML += " ";
			}
				
				div.innerHTML += "Шаблон: ";
				sel = document.createElement('select');
					sel.id = 'select_sms_templ';
					sel.setAttribute("onchange", "javascript:write_template(this);");
				
					opt = document.createElement('option');
					opt.innerHTML = "Выберите";
					opt.value = "";
				sel.appendChild(opt);
				
				if('sms_templates' in Item)
				{
					var sms_templates = Item['sms_templates'];
					for (var key in sms_templates)
					{
						opt = document.createElement('option');
							opt.value = sms_templates[key]['sms'];
							opt.innerHTML = sms_templates[key]['name'];
						sel.appendChild(opt);
					}
				}
				
				
					
			div.appendChild(sel);
			
			
		
			wind.appendChild(div);
			
			div = document.createElement('div');
				div.innerHTML = "<br>Сообщение:<br>";
				
				var text_inp = document.createElement('textarea');
					text_inp.id = "text_for_sms";
					text_inp.rows = "5";
					text_inp.cols = "50";
				
				div.appendChild(text_inp);
			wind.appendChild(div);
			
			div = document.createElement('div');
				div.innerHTML = "<br>";
				
				var b = document.createElement('button');
					b.innerHTML = "Отправить";
					b.id = "send_sms_button";
					b.setAttribute("onclick", "javascript:sms_send();");
				
				div.appendChild(b);
			wind.appendChild(div);
			
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
	
	
}

function write_template2(el)
{
	//console.log(123145);
	var sel_templ = document.querySelector('#select_sms_templ').value;

	if (sel_templ == '')
	{
		//alert('Не выбран шаблон!');
		return;
	}
	
	
	var sel_st = el.options[el.selectedIndex].value;
	if (sel_st != '')
	{
		var sms = sel_templ.replace(new RegExp("{WORK_TEL}", ""), sms_staff_list[sel_st]['WORK_TEL']);
		sms = sms.replace(new RegExp("{WORK_ADDRESS}", ""), sms_staff_list[sel_st]['WORK_ADDRESS']);
		sms = sms.replace(new RegExp("{STAFF_NAME}", ""), sms_staff_list[sel_st]['firstname']);
		sms = sms.replace(new RegExp("{STAFF_LASTNAME}", ""), sms_staff_list[sel_st]['lastname']);
		sms = sms.replace(new RegExp("{STAFF_TEL}", ""), sms_staff_list[sel_st]['phone_work']);
		sms = sms.replace(new RegExp("{STAFF_MOB}", ""), sms_staff_list[sel_st]['phone_mob']);
		sms = sms.replace(new RegExp("{STAFF_MAIL}", ""), sms_staff_list[sel_st]['email_corp']);
	
				
		document.querySelector("#text_for_sms").value = sms;

	}
}

function write_template(el)
{
	//console.log(123145);
	var sel = el.options[el.selectedIndex].value;
	console.log(sel);
	var sel_st = document.querySelector('#select_staff_sms');
	if (sel_st)
	{
		var sel_st_v = sel_st.options[sel_st.selectedIndex].value;
		if(sel_st_v != '')
		{
			var sms = sel.replace(new RegExp("{WORK_TEL}", ""), sms_staff_list[sel_st_v]['WORK_TEL']);
			sms = sms.replace(new RegExp("{WORK_ADDRESS}", ""), sms_staff_list[sel_st_v]['WORK_ADDRESS']);
			sms = sms.replace(new RegExp("{STAFF_NAME}", ""), sms_staff_list[sel_st_v]['firstname']);
			sms = sms.replace(new RegExp("{STAFF_LASTNAME}", ""), sms_staff_list[sel_st_v]['lastname']);
			sms = sms.replace(new RegExp("{STAFF_TEL}", ""), sms_staff_list[sel_st_v]['phone_work']);
			sms = sms.replace(new RegExp("{STAFF_MOB}", ""), sms_staff_list[sel_st_v]['phone_mob']);
			sms = sms.replace(new RegExp("{STAFF_MAIL}", ""), sms_staff_list[sel_st_v]['email_corp']);
		
					
			document.querySelector("#text_for_sms").value = sms;
		}
		else
		{
			el.options[0].selected = true;
			alert("Не выбран сотрудник!");
		}
			//document.querySelector("#text_for_sms").value = sel;
	}
	else
		document.querySelector("#text_for_sms").value = sel;
	
}

function show_link_form(el, type)
{
	var inp = document.createElement('input');
		inp.type = "text";
		inp.name = type;
		inp.setAttribute("oninput", "javascript:findforms(this);");
		inp.setAttribute("style", "border: 1px solid black; width:50%;");
	el.parentNode.insertBefore(inp,el);
	
	var i = document.createElement('i');
		i.className = "fil5";
	el.parentNode.insertBefore(i,el);
	
	var ul = document.createElement('ul');
		ul.className = "fil2";
		ul.setAttribute('name', "rsl");
	el.parentNode.insertBefore(ul,el);
	
	el.innerHTML = ' Сохранить';
	el.setAttribute("onclick", "javascript:savelinkform(this, '', 0);");
}

function findforms(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=findforms&get='+txt.value + '&rname=' + el.name + "&id=" + anketa_id, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function savelinkform(el, del, id)
{
	var msg = (del != '') ? ("Отвязать анкету " + id + "?") : ("Прикрепить анкету "  + "?");
	if (!confirm(msg))
		return;
	var ank;
	var val;
	var name;
	var fid = 0;
	if (del != '')
	{
		name = del;
		val = 'del';
		fid = id;
		ank = val;
	}
	else
	{
		var inp = el.parentNode.querySelector('input');
		val = inp.value;
		name = inp.name;
		ank = inp.value.replace(/\D+/g,"");
	}
	
	if (val != ank)
	{
		alert('В поле введены неверный данные!');
		
	}
	else
	{
		var data = "block=anketa&action=" + name + "&id=" + anketa_id + "&val=" + val + "&fid=" + fid;
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				if (val == 'del')
					alert('Анкета отвязана');
				else
					alert('Анкета прикреплена');
				location.reload();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function save_event(el)
{
	var meet_out = '';
	var form_id = '';
	var id = el.id;
	if (el.id == 'call_b')
		form_id = 'call_event';
	else if (el.id == 'meet_b') {
		var mo = document.querySelector('#meet_out_b');
		if (mo && mo.checked) {
			meet_out = '&meet_out=1';
		}
		form_id = 'meet_event';
	}
		
	
	var val = document.querySelector('#' + form_id).value;
	
	if (val == '')
	{
		alert('Не выбрана дата!');
		return;
	}
	console.log(val);
	var data = "block=anketa&action=save_event&id=" + anketa_id + "&" + form_id + "=" + val + meet_out;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Событие добавлено');
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function show_cal_form(el)
{
	console.log('staff_id_js=' + staff_id_js);
	console.log('is_piter=' + is_piter);
	console.log('is_tumen=' + is_tumen);
	var reg = new RegExp("Запланировать встречу", "i");
	var meet = false;
	if (reg.test(el.innerHTML))
		meet = true;
		
	if (((staff_office_type == 'tm') && (is_piter == 1) && meet) || ((staff_office_type == 'tm') && (is_tumen == 1) && meet) || ((staff_office_type == 'tm') && (is_kazan == 1) && meet))
	//if ((staff_id_js == 1) && (is_piter == 1))
	{
		var text_conf = "У ТЕБЯ будет ШТРАФ 1000р,\nесли ты не выполнил хоть один пункт из этого списка:\n";
		text_conf += "\n1. Согласовать с клиентом ежемесячный платеж (клиент должен быть согласен)";
		text_conf += "\n2. Вписать все активные кредиты в анкету и со слов закрытые кредит";
		text_conf += "\nНажми ОК чтобы назначить встречу и";
		text_conf += "\nОтмена чтобы доработать!";
		if (!confirm(text_conf))
			return;
	}
	el.style.display = "none";
	var inp = el.parentNode.querySelector('input');
	inp.style.display = "";
	var but = el.parentNode.querySelector('button[name="save_ev_b"]');
	but.style.display = "";
	var mo = document.querySelector('#meet_out_b');
	if (mo)
		mo.parentNode.style.display = "";
}

function ch_file_type(el)
{
	var fid = el.id.replace(/[^0-9]/gim,'');
	var sel = el.options[el.selectedIndex].value;
	console.log(sel);
	
	var data = "block=anketa&action=ch_file_type&id=" + anketa_id + "&fid=" + fid + "&type=" + sel;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Тип изменен');
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_file_form(el)
{
	console.log('add_file');
	var files_mass = document.querySelector("#files_mass");
	var add_file_wrap = document.querySelector("#add_file_wrap");
	var send_file_button = document.querySelector("#send_file_button");
	var div = document.createElement('div');
		var sel = document.createElement('select');
		sel.name = "upload_file_type_" + num_file;
		sel.id = "upload_file_type_" + num_file;
		sel.innerHTML = '';
		console.table(fileTypesMass);
		for (var elfi in fileTypesMass) {
			let elf = fileTypesMass[elfi];
			console.log(elf);
			sel.innerHTML += '<option value="' + elf.name + '">' + elf.description + '</option>';
		}
		/*sel.innerHTML = '<option value="passport_scan">Скан паспорта</option>';
		sel.innerHTML += '<option value="passport_reg">Скан прописки</option>';
		sel.innerHTML += '<option value="agree_scan">Скан согласия</option>';
		sel.innerHTML += '<option value="passport_all">Паспорт (полный)</option>';		
		sel.innerHTML += '<option value="photo">Фото</option>';
		sel.innerHTML += '<option value="report">Отчет</option>';
		sel.innerHTML += '<option value="tk">Копия ТК</option>';
		sel.innerHTML += '<option value="dohod">Справка о доходе</option>';
		sel.innerHTML += '<option value="2ndfl">2-НДФЛ</option>';
		sel.innerHTML += '<option value="declar">Декларация</option>';
		sel.innerHTML += '<option value="rs">Выписка по р/с</option>';
		sel.innerHTML += '<option value="ls">Выписка по л/с</option>';
		sel.innerHTML += '<option value="vdk">ВДК</option>';
		sel.innerHTML += '<option value="td">Трудовой Договор</option>';
		sel.innerHTML += '<option value="company_card">Карточка компании</option>';
		sel.innerHTML += '<option value="verification">Файл верификации</option>';
		sel.innerHTML += '<option value="other" selected>Другой</option>';*/
	div.appendChild(sel);
	div.innerHTML += " ";
		var inp = document.createElement('input');
		inp.type = "file";
		inp.name = "upload_file_" + num_file;
		inp.id = "upload_file_" + num_file;
		num_file++;
	div.appendChild(inp);
		
	//add_file_wrap.insertBefore(div, el);
	files_mass.appendChild(div);
	if (send_file_button.style.display == "none")
		send_file_button.style.display = "";
}

function send_files()
{
	//var form = document.forms.test_form;
	
	var formData = new FormData();
	formData.append("block", "anketa");
	formData.append("action", "add_files");
	formData.append("id", anketa_id);
	formData.append("num_files", num_file);
	for (var n = 0; n < num_file; n++)
	{
		var filetype = document.getElementById("upload_file_type_" + n);
		filetype = filetype.options[filetype.selectedIndex].value;
		formData.append("filetype_" + n, filetype);
		var file = document.getElementById("upload_file_" + n).files[0];
		formData.append("uploadfile" + n, file);
	}
	//ajaxQuery2('http://alekskh.tmweb.ru/upload/upload.php','POST', 'id=123&'+getQueryString(form), false, function(req)
	//ajaxQuery2('http://alekskh.tmweb.ru/upload/upload.php','POST', formData, false, function(req)
	ajaxQuery2('/ajax.php','POST', formData, false, function(req)
	{
		//alert(1);
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Файл(ы) загружен(ы)');
			//show('none');
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
		//document.getElementById("result").innerHTML = tmp_page;
		//alert(tmp_page);
		//location.reload();
	});
	
	/*
	var file = document.getElementById("upload_file").files[0];
	var formData = new FormData();
	formData.append("uploadfile", file);
	formData.append("block", "anketa");
	formData.append("action", "add_files");
	formData.append("id", anketa_id);
	var data = "?block=anketa&action=add_files&id=" + anketa_id + "&rand=" + Math.random();
	
	ajaxQuery('/upload.php' + data,'POST', formData, false, function(req)
	{
		var tmp_page = req.responseText;
		//document.getElementById("result").innerHTML = tmp_page;
		//alert(tmp_page);
		//location.reload();
	});*/
	
	
	/*var files_mass = document.querySelector("#files_mass");
	var formData = new FormData(files_mass);
	//formData.append("block", "anketa");
	//formData.append("id", anketa_id);
	var data = "?block=anketa&action=add_files&id=" + anketa_id + "&rand=" + Math.random();
	ajaxQueryFiles('/ajax.php' + data,'POST', formData, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			//alert('СМС отправлена на номер ' + tmp_num);
			//show('none');
			alert(Item['msg']);
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});*/
}

function sms_send()
{
	var tmp_num = document.querySelector("#selphone").value;
	var tmp_text = document.querySelector("#text_for_sms").value;
	document.querySelector("#send_sms_button").disabled = true;
	console.log(tmp_num);
	console.log(tmp_text);
	tmp_text = encodeURIComponent(tmp_text);
	var data = "block=anketa&action=send_sms&id=" + anketa_id + "&tel=" + tmp_num + "&msg=" + tmp_text;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('СМС отправлена на номер ' + tmp_num);
			show('none');
			reload_log_data();
			document.querySelector("#send_sms_button").disabled = false;
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			document.querySelector("#send_sms_button").disabled = false;
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function sel_phone(el)
{
	//console.log(el.value);
	
	if (el.value == "inp")
	{
		document.querySelector("#selphone").disabled = "";
		document.querySelector("#selphone").value = "";
	}
	else
	{
		document.querySelector("#selphone").disabled = "true";
		if (el.value != "-1")
			document.querySelector("#selphone").value = el.options[el.selectedIndex].value;
	}
}

function check_idx_phone()
{
	if (!confirm("Выполнить проверку телефона?"))
		return;
	
	var data = "block=anketa&action=check_idx_phone&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var tmp = 'Проверка проведена!';
			//alert('Проверка проведена!');
			var data = Item['data'];
			tmp += '\n Результат:' + data['operationResult'];
			//tmp += '\n Подробности:' + data['ValidationScorePhone'];
			
			alert(tmp);
			
			location.reload();
		}
		else if (Item['status'] == 'failed')
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			//show_promoney_fields(cl);
		}
		else
			alert('Неизвестная ошибка');
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function personDebt() {
	if (!confirm("Выполнить проверку налоговой задолженности?"))
		return;

	var data = "block=anketa&action=personDebt&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req)
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Проверка создана');
				location.reload();
			}
			else if (Item['status'] == 'failed')
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
				//show_promoney_fields(cl);
			}
			else
				alert('Неизвестная ошибка');
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
}


function finScoringOkbQiwi() {
	if (!confirm("Выполнить проверку финансового скоринга через ОКБ?"))
		return;

	var data = "block=anketa&action=finScoringOkbQiwi&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req)
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Проверка создана');
				//location.reload();
			}
			else if (Item['status'] == 'failed')
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
				//show_promoney_fields(cl);
			}
			else
				alert('Неизвестная ошибка');
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
}

function personLoanRating() {
	if (!confirm("Выполнить проверку краткого кредитного отчета?"))
		return;

	var data = "block=anketa&action=personLoanRating&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req)
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Проверка создана');
				location.reload();
			}
			else if (Item['status'] == 'failed')
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
				//show_promoney_fields(cl);
			}
			else
				alert('Неизвестная ошибка');
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
}

function finScoring()
{
	if (!confirm("Выполнить проверку FICO?"))
		return;
	
	var data = "block=anketa&action=check_finScoring&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var tmp = 'Проверка проведена!';
			//alert('Проверка проведена!');
			var data = Item['data'];
			if (Item['showdetails']) {
				tmp += '\n Балл:' + data['score'];
				tmp += '\n Сообщение:' + data['resultMessage'];
				tmp += '\n Подробности:';
				
				if (data['reasonCode0Desc'])
					tmp += '\n -' + data['reasonCode0Desc'];
				if (data['reasonCode1Desc'])
					tmp += '\n -' + data['reasonCode1Desc'];

				if (data['reasonCode2Desc'])
					tmp += '\n -' + data['reasonCode2Desc'];

				if (data['reasonCode3Desc'])
					tmp += '\n -' + data['reasonCode3Desc'];

				if (data['reasonCode4Desc'])
					tmp += '\n -' + data['reasonCode4Desc'];
				if (data['reasonCode5Desc'])
					tmp += '\n -' + data['reasonCode5Desc'];
				if (data['reasonCode6Desc'])
					tmp += '\n -' + data['reasonCode6Desc'];
			}
			
			//alert(tmp);
			
			console.log(data);
			console.log(data['exclusionCode']);
			
			var field180 = document.querySelector("input[name=field180]");
			console.log(field180.value);
			if (Item['showdetails'])
				field180.style.display = '';

			if (parseInt(data['exclusionCode']) == 3)
				field180.value = 1;
			else
				field180.value = data['score'];
			
			console.log(document.querySelector("input[name=field180]").value);
			
			var spfield180 = document.querySelector('#span_field180');
			if (spfield180)
				spfield180.style.display = 'none';
			
			document.querySelector("#sp_field180").innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
			
			document.querySelector("#save_data_button").style.display = 'block';
			document.querySelector("#save_data_button_top").style.display = '';
			
			alert(tmp);
			if (confirm("Сохранить анкету?"))
				save_data();
			//else
				//save_data();
			
			//location.reload();
		}
		else if (Item['status'] == 'exception3')
		{
			var tmp = 'Проверка проведена!';

			var data = Item['data'];
			tmp += '\n Сообщение:Подозрение на мошенничество.';
			
			var field180 = document.querySelector("input[name=field180]");
			console.log(field180.value);
			if (Item['showdetails'])
				field180.style.display = '';
			field180.value = 1;
					
			console.log(document.querySelector("input[name=field180]").value);
			
			var spfield180 = document.querySelector('#span_field180');
			if (spfield180)
				spfield180.style.display = 'none';
			
			document.querySelector("#sp_field180").innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
			
			document.querySelector("#save_data_button").style.display = 'block';
			document.querySelector("#save_data_button_top").style.display = '';
			
			alert(tmp);
			if (confirm("Сохранить анкету?"))
				save_data();
		}
		else if (Item['status'] == 'failed')
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			//show_promoney_fields(cl);
		}
		else
			alert('Неизвестная ошибка');
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function searchphysical()
{
	if (!confirm("Выполнить проверку физического лица в ФССП?"))
		return;
	
	var data = "block=anketa&action=searchphysicalm&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Проверка создана');
			location.reload();
		}
		else if (Item['status'] == 'failed')
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			//show_promoney_fields(cl);
		}
		else
			alert('Неизвестная ошибка');
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function check_iidx_fssp()
{
	if (!confirm("Выполнить проверку физического лица в ФССП?"))
		return;
	
	var data = "block=anketa&action=check_iidx_fssp&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Проверка создана');
			location.reload();
		}
		else if (Item['status'] == 'failed')
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
			//show_promoney_fields(cl);
		}
		else
			alert('Неизвестная ошибка');
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function promoney_check(cl)
{
	if (!confirm("Выполнить проверку " + checks_prom[cl] + "?"))
		return;
		
	var fields = new Array();
	var promoney_fail = false;
	
	var ccf_c = 0;
	var ccf_p = 0;
	var ccf_r = 0;
	
	switch(cl)
	{
		case 'CWIOkbReportPrivate':
		{
			var sum = document.querySelector("input[name=sum]").value;
			if (sum == '')
				promoney_fail = true;
			else
				fields['AMOUNT'] = sum;
			
			var income = document.querySelector("input[name=field75]").value;
			if (income == '')
				promoney_fail = true;
			else
				fields['INCOME'] = income;
			
			//var term_loan = document.querySelector("input[name=field121]").value;
			//if (term_loan == '')
				//promoney_fail = true;
			//else
			//{
				//fields['TERM_LOAN'] = term_loan;
				fields['TERM_LOAN'] = '60';
				fields['TERM_LOAN_UNIT'] = 'MONTH';
			//}
		}
		case 'CWICreditHistoryPrivate':
		{
			var birthplace = document.querySelector("input[name=field23]").value;
			if (birthplace == '')
				promoney_fail = true;
			else
				fields['BIRTHPLACE'] = birthplace;
			
			var passport_place = document.querySelector("input[name=field20]").value;
			if (passport_place == '')
				promoney_fail = true;
			else
				fields['PASSPORT_PLACE'] = passport_place;
			
			var passport_code = document.querySelector("input[name=field22]").value;
			if (passport_code == '')
				promoney_fail = true;
			else
				fields['PASSPORT_CODE'] = passport_code;
			
			var ar = document.querySelectorAll("input[name*=field24]");
			var address_registered = '';
			if (ar)
			{
				for (var l=0; l < ar.length; l++)
				{
					if (ar[l].value != '')
					{
						if (address_registered != '')
							address_registered += ', ';
						address_registered += ar[l].value;
					}	
				}
			}

			if (address_registered == '')
				promoney_fail = true;
			else
				fields['ADDRESS_REGISTERED'] = address_registered;
			
			if(cl == 'CWICreditHistoryPrivate')
			{
				var ar = document.querySelectorAll("input[name*=field123]");
				var address_residence = '';
				if (ar)
				{
					console.log('address_residence');
					console.log(ar);
					for (var l=0; l < ar.length; l++)
					{
						if (ar[l].value != '')
						{
							if (address_residence != '')
								address_residence += ', ';
							address_residence += ar[l].value;
						}
					}
				}
				
				if (address_residence == '')
					promoney_fail = true;
				else
					fields['ADDRESS_RESIDENCE'] = address_residence;
			}
			
			var loaded_file = document.querySelectorAll("div.loaded_file")
			var consent = false;
			var passport_photo = false;
			var passport_registration = false;
			for(i=0; i<loaded_file.length; i++)
			{
				var sel = loaded_file[i].querySelector('select');
				var type = sel.options[sel.selectedIndex].value
				//var file = loaded_file[i].querySelector('a').innerHTML;
				var file_a = loaded_file[i].querySelector('a');
				if (file_a == null)
					continue;
				
				
				var file = file_a.id.replace("fileid_", "");
				if (type == 'agree_scan')
				{
					consent = true;
					fields['CONSENT'] = file;
					ccf_c++;
				}
				if (type == 'passport_scan')
				{
					passport_photo = true;
					fields['PASSPORT_PHOTO'] = file;
					ccf_p++;
				}
				if (type == 'passport_reg')
				{
					passport_registration = true;
					fields['PASSPORT_REGISTRATION'] = file;
					ccf_r++;
				}
			}
			
			if (!consent || (ccf_c > 1))
				promoney_fail = true;
			if (!passport_photo || (ccf_p > 1))
				promoney_fail = true;
			if (!passport_registration || (ccf_r > 1))
				promoney_fail = true;
		}
		case 'CWICreditHistoryIndividual':
		{
			var passport_issue = document.querySelector("input[name=field21]").value;
			if (passport_issue == '')
				promoney_fail = true;
			else
				fields['PASSPORT_ISSUE'] = passport_issue;
		}

		case 'CWICreditRatingIndividual':
		{
			var birth_date = document.querySelector("input[name=birth_day]").value;
			if (birth_date == '')
				promoney_fail = true;
			else
				fields['BIRTH_DATE'] = birth_date;
			
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				promoney_fail = true;
			else
				fields['LAST_NAME'] = lastname;
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				promoney_fail = true;
			else
				fields['FIRST_NAME'] = firstname;
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				promoney_fail = true;
			else
				fields['MIDDLE_NAME'] = middlename;
			
			var passport_series = document.querySelector("input[name=field18]").value;
			if (passport_series == '')
				promoney_fail = true;
			else
				fields['PASSPORT_SERIES'] = passport_series;
			
			var passport_number = document.querySelector("input[name=field19]").value;
			if (passport_number == '')
				promoney_fail = true;
			else
				fields['PASSPORT_NUMBER'] = passport_number;

			break;
		}
	}
	
	if (promoney_fail)
	{
		if (ccf_c > 1)
			alert('Более одного скана согласия прикреплено!');
		else if (ccf_p > 1)
			alert('Более одного скана паспорта прикреплено!');
		else if (ccf_r > 1)
			alert('Более одного скана прописки прикреплено!');
		else
			alert('Не все поля заполнены!');
	}
	else
	{
		console.log(fields);
		var data = "block=anketa&action=promoney&suba=make_check&class=" + cl + "&id=" + anketa_id;
		for (var key in fields)
		{
			data += "&" + key + "=" + fields[key];
		}
		console.log(data);
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Анкета отправлена');
				location.reload();
			}
			else if (Item['ERROR'] != null)
			{
				//butt.style.backgroundColor = '#FFa0a0';
				//alert(Item['ERROR']);
				var msg = Item['ERROR'];
				if (Item['FIELDS'] != null) 
				{
					var fields = Item['FIELDS'];
					msg = "";
					for (var key in fields)
						msg += key + ": " + fields[key] + "\n";
				}
				show_promoney_fields(cl);
				alert(msg);
			}
			else if (Item['status'] == 'failed')
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
				show_promoney_fields(cl);
			}
			else
				alert('Неизвестная ошибка');
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function show_promoney_fields(cl)
{
	document.querySelector('#clear_filter_button').style.display = '';
	var divs = document.querySelectorAll(".block_wrap > .row_a");
	for (var n=0; n < divs.length; n++)
	{
		var show = false;
		switch(cl)
		{
			case 'CWIOkbReportPrivate':
			{
				if(divs[n].querySelector("input[name=sum]"))
					show = true;
				if(divs[n].querySelector("input[name=field75]"))
					show = true;				
				//if(divs[n].querySelector("input[name=field121]"))
					//show = true;				
			}
			case 'CWICreditHistoryPrivate':
			{
				if(divs[n].querySelector("input[name=field23]"))
					show = true;				
				if(divs[n].querySelector("input[name=field20]"))
					show = true;				
				if(divs[n].querySelector("input[name=field22]"))
					show = true;				
				if(divs[n].querySelectorAll("input[name*=field24]").length)
					show = true;
				
				if(cl == 'CWICreditHistoryPrivate')
				{
					if(divs[n].querySelectorAll("input[name*=field123]").length)
						show = true;				
				}
			}
			case 'CWICreditRatingIndividual':
			case 'CWICreditHistoryIndividual':
			{
				if(divs[n].querySelector("input[name=field21]"))
					show = true;				
			}
			case 'CWICreditRatingIndividual':
			{
				if(divs[n].querySelector("input[name=birth_day]"))
					show = true;
				if(divs[n].querySelector("input[name=lastname]"))
					show = true;				
				if(divs[n].querySelector("input[name=firstname]"))
					show = true;				
				if(divs[n].querySelector("input[name=middlename]"))
					show = true;				
				if(divs[n].querySelector("input[name=field18]"))
					show = true;				
				if(divs[n].querySelector("input[name=field19]"))
					show = true;
				
				break;
			}
		}		
		if (show)
		{
			divs[n].style.display = '';
		}
		else
			divs[n].style.display = 'none';
	}
}


function send_msg2log()
{
	var msg_text = document.querySelector("#input_window>textarea").value;
	
	
	if (msg_text == '')
		return;

	msg_text = encodeURIComponent(msg_text);

	
	var toagent = 0;
	var ta = document.querySelector('#msgtoagent');
	if (ta)
	{
		if (ta.checked)
			toagent = 1;
	}
	var data = "block=anketa&action=add_msg&id=" + anketa_id + "&msg=" + msg_text + "&toagent=" + toagent;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			reload_log_data();
			document.querySelector("#input_window>textarea").value = "";
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function update_all_data()
{
	get_tasks_count();
	get_events_count();
	reload_log_data();
	search_d_fio();
	search_d_phone();
	double_ip();
	get_banks_work();
	console.log(staff_office_type);
	//if (staff_office_type != 'main')
		//check_blocks();
	
	var selects = document.querySelectorAll("div[class=fval]>select");
	//console.log(selects.length);
	for (var i=0; i < selects.length; i++)
	{
		var item2 = selects[i];
		old_input_value[item2.name] = item2.options[item2.selectedIndex].value;
		sec_input_value[item2.name] = true;
	}
	
	var radio_b = document.querySelectorAll("div[class=fval] input[type=radio]");
	//console.log(selects.length);
	for (var i=0; i < radio_b.length; i++)
	{
		var item2 = radio_b[i];
		if(item2.checked)
		{
			old_input_value[item2.name] = item2.value;
			old_radio_value[item2.name] = item2.value;
			sec_input_value[item2.name] = true;
		}
		
	}
	
	/*selects.forEach(function(item2, i, arr){
		old_input_value[item2.name] = item2.options[item2.selectedIndex].value;
		sec_input_value[item2.name] = true;
	});*/
	
	check_ur();
	check_sovm();
}

var double_fio_m = new Array();

function search_d_fio()
{
	var sp = document.querySelector("#double_fio");
	var data = "block=anketa&action=double_fio&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'] > 0)
			{
				var data = Item['data'];
				//console.log(data);
				for (var key in data)
				{
					var ddd = data[key];
					//console.log(ddd);
					double_fio_m[ddd['id']] = 'от ' + ddd['date_add'] + ' <a target=_blank href="/details/' + ddd['id'] + '/">' + ddd['lastname'] + ' ' + ddd['firstname'] + ' ' + ddd['middlename'] + '</a>';
				}
				sp.innerHTML = "Найдены дубли (" + Item['len'] + ")";
				sp.setAttribute("data-tooltip", "double_fio");
				sp.setAttribute("class", "blink1");
				sp.setAttribute("style", "cursor:pointer");
				//console.log(double_fio_m);
			}
			else if ('type' in Item)
			{
				if (Item['type'] == 'empty')
					sp.innerHTML = "Дублей не найдено. Не заполнены ФИО";
			}
			else
				sp.innerHTML = "Дублей не найдено";
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	}/*,
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	}*/
	);
}


var double_phone_m = new Array();

function search_d_phone()
{
	var sp = document.querySelector("#double_phone");
	var data = "block=anketa&action=double_phone&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'] > 0)
			{
				var data = Item['data'];
				for (var key in data)
				{
					var ddd = data[key];
					double_phone_m[ddd['id']] = 'от ' + ddd['date_add'] + ' <a target=_blank href="/details/' + ddd['id'] + '/">' + ddd['lastname'] + ' ' + ddd['firstname'] + ' ' + ddd['middlename'] + '</a>';
				}
				
				sp.innerHTML = "Найдены дубли (" + Item['len'] + ")";
				sp.setAttribute("data-tooltip", "double_phone");
				sp.setAttribute("class", "blink1");
				sp.setAttribute("style", "cursor:pointer");
			}
			else
				sp.innerHTML = "Дублей не найдено";
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	}/*,
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	}*/
	);
}

var double_ip_m = new Array();

function double_ip()
{
	var sp = document.querySelector("#double_ip");
	var data = "block=anketa&action=double_ip&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'] > 0)
			{
				var data = Item['data'];
				for (var key in data)
				//for(var key = 0; key < data.length; key++)
				{
					var ddd = data[key];
					double_ip_m[ddd['id']] = 'от ' + ddd['date_add'] + ' <a target=_blank href="/details/' + ddd['id'] + '/">' + ddd['lastname'] + ' ' + ddd['firstname'] + ' ' + ddd['middlename'] + '</a>';
				}
				if (parseInt(Item['len']) == 100) {
					Item['len'] = Item['len'] + '+';
				}
				sp.innerHTML = "Найдены дубли (" + Item['len'] + ")";
				sp.setAttribute("data-tooltip", "double_ip");
				sp.setAttribute("style", "cursor:pointer");
			}
			else
				sp.innerHTML = "Дублей не найдено";
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	}/*,
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	}*/
	);
}

var log = new Array();
var work_plan = new Array();
var pre_decision = new Array();
var wpd = new Array();

function select_man_task(man)
{
	//console.log('selected');
	//console.log(man);
	var sel = document.querySelector('#task_sel_staff_id');
	for (var n=0; n < sel.options.length; n++)
	{
		//console.log(sel.options[n]);
		if (sel.options[n].value == ('st_' + man))
			sel.options[n].selected = true;
	}
}

function check_blocks()
{
	var divs = document.querySelectorAll(".block_wrap");
	
	var rows = divs[1].querySelectorAll(".row_a");
	var hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	
	if (hit)
		divs[2].style.display = "table";
	else
		divs[2].style.display = "none";
	
	console.log("Показывать 2 блок: " + hit);
	
	rows = divs[2].querySelectorAll(".row_a");
	hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	
	if (hit)
		divs[3].style.display = "table";
	else
		divs[3].style.display = "none";
	
	console.log("Показывать 3 блок: " + hit);
	
	rows = divs[3].querySelectorAll(".row_a");
	hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	for(var i = 4; i < divs.length; i++)
	{
		if (hit)
			divs[i].style.display = "table";
		else
			divs[i].style.display = "none";
	}
	
	
	
	
	console.log("Показывать 4 блок: " + hit);
}

function ch_log_filter(el)
{
	var elid = el.id;
	console.log(elid);
	
	var inp = el.querySelector('input');
	inp.checked = !inp.checked;
	
	var val = (inp.checked) ? 1 : 0;
	
	setCookie(elid, val, { expires: 36000000, path:"/"});
	
	log_filter_apply();
}

function log_filter_apply()
{
	var log_msg = 1;
	if (getCookie('log_msg')) log_msg = parseInt(getCookie('log_msg'));
	document.querySelector('#log_msg').querySelector('input').checked = log_msg;
	
	var log_calls = 1;
	if (getCookie('log_calls')) log_calls = parseInt(getCookie('log_calls'));
	document.querySelector('#log_calls').querySelector('input').checked = log_calls;
	
	var log_robot = 1;
	if (getCookie('log_robot')) log_robot = parseInt(getCookie('log_robot'));
	document.querySelector('#log_robot').querySelector('input').checked = log_robot;
	
	var log_sms = 1;
	if (getCookie('log_sms')) log_sms = parseInt(getCookie('log_sms'));
	document.querySelector('#log_sms').querySelector('input').checked = log_sms;
	
	var log_other = 1;
	if (getCookie('log_other')) log_other = parseInt(getCookie('log_other'));
	document.querySelector('#log_other').querySelector('input').checked = log_other;
	
	var cla = new Array();
	cla['log_msg'] = log_msg;
	cla['log_calls'] = log_calls;
	cla['log_robot'] = log_robot;
	cla['log_sms'] = log_sms;
	cla['log_other'] = log_other;
	
	var clacc = new Array();
	clacc['log_msg'] = 0;
	clacc['log_calls'] = 0;
	clacc['log_robot'] = 0;
	clacc['log_sms'] = 0;
	clacc['log_other'] = 0;
	
	console.log(cla);
	
	
	var log = document.querySelector('#log_window');
	var divs = log.querySelectorAll('div');
	
	for (var i=0; i<divs.length; i++)
	{
		if(divs[i].className in cla)
		{
			clacc[divs[i].className]++;
			if (cla[divs[i].className])
				divs[i].style.display = 'block';
			else
				divs[i].style.display = 'none';
			document.querySelector('#' + divs[i].className).querySelector('span').innerHTML = "(" + clacc[divs[i].className] + ")";
		}
		else
		{
			clacc['log_other']++;
			if (cla['log_other'])
				divs[i].style.display = 'block';
			else
				divs[i].style.display = 'none';
			document.querySelector('#log_other').querySelector('span').innerHTML = "(" + clacc['log_other'] + ")";
		}
		
		
	}
}

function reload_log_data()
{
	log = [];
	var msg_text = document.querySelector("#log_window");
	if (!msg_text)
		return;
	msg_text.innerHTML = "";
	var data = "block=anketa&action=get_log&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var data = Item['data'];
			
			for (var key in data)
			{
				var val = data[key];
				if (val['type'] == 'robot')
				{
					val['creator'] = 'Робот';
				}
				else							
					val['type'] = 'msg';
				
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['creator'] + ' :<br>' + val['msg'] + '<br><br>';
			}
			
			data = Item['work_plan'];
			
			for (var key in data)
			{
				var val = data[key];
				val['type'] = 'work_plan';
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['st_name'] + " План работ:<br>" + val['msg'] + '<br><br>';
			}
			
			data = Item['pre_decision'];
			
			for (var key in data)
			{
				var val = data[key];
				val['type'] = 'pre_decision';
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['st_name'] + " Предварительное решение:<br>" + val['msg'] + '<br><br>';
			}
			
			data = Item['wpd'];
			
			for (var key in data)
			{
				var val = data[key];
				val['type'] = 'wpd';
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['st_name'] + " Предварительное решение:<br>" + val['msg'] + '<br><br>';
			}
			
			data = Item['sms_log'];
			
			for (var key in data)
			{
				var val = data[key];
				val['type'] = 'sms';
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['st_name'] + " Предварительное решение:<br>" + val['msg'] + '<br><br>';
			}
			
			data = Item['call_log'];
			
			for (var key in data)
			{
				var val = data[key];
				val['type'] = 'calls';
				var date = new Date(val['date']);
				//val['dmil'] = date.valueOf();
				val['dmil'] = parseInt(val['rawtime']);
				log.push(val);
				//msg_text.innerHTML += val['date'] + " " + val['st_name'] + " План работ:<br>" + val['msg'] + '<br><br>';
			}
			
//			console.log(log);
			//log.sort(sortFuncTime);
			//log.sort(sortFuncTime);
			log.sort(function(a, b){
				return a['dmil'] - b['dmil'];
				});
			
			/*if (staff_office_type == 'main')
			{
				log.sort(function(a,b){
					return (b.dmil > a.dmil) ? 1 : (b.dmil < a.dmil) ? -1 : 0;
				});
			}*/
//			console.log(log);
			for (var key in log)
			//for (var key = 0; key < log.length; key++)
			{
				var tmp_t = '';
				var val = log[key];
				tmp_t += '<div class="log_' + val['type'] + '">';
				console.log(val);
				
				if (val['type'] == 'msg')
					tmp_t += val['date'] + " " + val['creator'] + ' :<br>' + val['msg'];
				else if (val['type'] == 'work_plan')
					tmp_t += val['date'] + " " + val['st_name'] + " План работ:<br>" + val['msg'];
				else if (val['type'] == 'pre_decision')
					tmp_t += val['date'] + " " + val['st_name'] + " <font style='font-weight:bold;'>Верификация ТМ:</font><br>" + val['msg'];
				else if (val['type'] == 'wpd')
					tmp_t += val['date'] + " " + val['st_name'] + " <font style='font-weight:bold;'>План на день:</font><br>" + val['msg'];
				else if (val['type'] == 'robot')
					tmp_t += val['date'] + " Робот:<br>" + val['msg'];
				else if (val['type'] == 'sms')
				{
					if(val['status'] == 'sent')
						tmp_t += val['date'] + " " + val['creator'] + ' : Сообщение на номер ' + val['phone'] + ' <font color="green">Отправлено</font>. Текст:<br>' + val['text'];
					else if(val['status'] == 'error')
						tmp_t += val['date'] + " " + val['creator'] + ' : Сообщение на номер ' + val['phone'] + ' <font color="red">НЕ ОТПРАВЛЕНО!</font>. Текст:<br>' + val['text'];
				}
				else if (val['type'] == 'calls')
				{
					var flow = '';
					if (val['flow'] == 'in')
						flow = 'входящий звонок';
					else if (val['flow'] == 'out')						
						flow = 'исходящий звонок';
					
					if (val['creator'] == 'Робот')
					{
						if ((val['staff_phone'] != null) && (val['staff_phone'] != ''))
							val['creator'] += " (принял " + val['staff_phone'] + ")";
					}
					
					var bridged = false;
					if (val['result'] == 'bridged')
						bridged = true;
					
					if (val['result'] == 'bridged')
						val['result'] = '<font color="green">Успешно (' + val['duration'] + 'с)</font>';
					else if ((val['result'] == 'not answered') || ((val['flow'] == 'in') && (val['result'] == 'answered') && (val['hangup_disposition'] == 'caller_bye')))
					{
						val['result'] = '<font color="red">Отказ звонящего</font>';
					}
					else if (val['result'] == 'busy')
						val['result'] = '<font color="orange">Занято</font>';
					else if (val['result'] == 'failed')
						val['result'] = '<font color="red">Вызов отклонен</font>';
					else if ((val['result'] == 'answered') && ((val['hangup_cause'] == 'NO_USER_RESPONSE') || (val['hangup_cause'] == 'NORMAL_UNSPECIFIED')))
						val['result'] = '<font color="red">Не отвечен</font>';
					else if ((val['result'] == 'answered') && (val['hangup_cause'] == 'NORMAL_CLEARING'))
					{
						val['result'] = '<font color="green">Успешно (' + val['duration'] + 'с)</font>';
						bridged = true;
					}
					
					tmp_t += val['date'] + " " + val['creator'] + ' : ' + flow + " " + val['result'];
					//if (staff_id_js == 1)
					if (bridged)
					{
						//tmp_t += ' <audio controls><source src="/test.mp3" type="audio/mpeg"></audio>';
						if ((staff_office_type == 'main') || (staff_id_js == 19) || (may_see_audio_js))
							tmp_t += ' <img src="/img/play_black.png" style="height:1.3em; cursor:pointer;" onclick="javascript:show_player(' + val['id'] + ', this);">';
						/*if (staff_office_type == 'main')
						{
							//tmp_t += ' <img src="/img/download-png-2.png" style="height:1.3em; cursor:pointer;" onclick="javascript:download_audio(' + val['id'] + ');">';
							tmp_t += ' <a href="/audio/' + anketa_id + '/' + val['id'] + '" download><img src="/img/download-png-2.png" style="height:1.3em; cursor:pointer;"></a>';
						}*/
							
					}
					//tmp_t += '<br><br>';
					
				}
				tmp_t += "</div>";
				msg_text.innerHTML += tmp_t;
			}
			log_filter_apply();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
		
		
	}/*,
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	}*/
	);
	
	
}

function delelparent(el)
{
	el.parentNode.parentNode.removeChild(el.parentNode);
}

function show_player(id, el)
{
	//document.getElementById('wrap').style.display = 'block';
	
	var coords = el.getBoundingClientRect();
	var left = coords.left - 350;	
	var top = coords.top;
	
	var div = document.createElement('div');
		div.setAttribute('class', "note_wind");
		div.style = "background-color:transparent;";		
		div.style.position = "absolute";
		div.style.width = "420px";
		//div.style.width = "300px";
		div.style.height = "90px";
		div.style.left = left + 'px';
		div.style.top = top + 'px';
		//div.innerHTML = 'fdsfds';
		
		var img = document.createElement('img');
			img.src = "/img/close.png";
			img.setAttribute("class", "close");
			img.style.width = "20px";
			img.setAttribute("onclick", "javascript:delelparent(this);");
		div.appendChild(img);
		
		
		var div2 = document.createElement('div');
			div2.setAttribute('style', "margin-top:35px;");
			div2.style.position = "absolute";
			//div2.innerHTML += '<audio controls><source src="/test.mp3" type="audio/mpeg"></audio>';
			
			var audio = document.createElement('audio');
				audio.setAttribute('controls', "controls");
				audio.setAttribute('preload', "auto");
				
				var source = document.createElement('source');
					//source.src = "/test.mp3";
					source.src = "/audio/" + anketa_id + "/" + id;
					source.type="audio/mpeg";
				audio.appendChild(source);
			div2.appendChild(audio);
			
		div.appendChild(div2);
		
/*		var audio = document.createElement('audio');
			audio.src = "/test.mp3";
			audio.type="audio/mpeg";
		div.appendChild(audio);
	*/
		/*var tearea = document.createElement('textarea');
			tearea.style = "margin-top:5px; margin-left:1%;";
			//tearea.style.width = "280px";
			tearea.style.width = "97%";
			tearea.style.height = "170px"
			tearea.value = '';
		div.appendChild(tearea);
	
		var sb = document.createElement('button');	
			sb.innerHTML = 'Сохранить';
			sb.style = "margin:5px 8px;";
			sb.style.float = "right";
			//sb.setAttribute("onclick", "javascript:save_note(this, '" + name + "');");
			//sb.onclick = "javascript:save_note(this, '" + name + "');";
		div.appendChild(sb);*/
	
	document.body.appendChild(div);
	/*alert(id);
	var data = "block=anketa&action=get_log&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
		}
	}*/
}

function sortFuncTime(a, b)
{
    if (a['dmil'] === b['dmil']) {
        return 0;
    }
    else {
        return (a['dmil'] < b['dmil']) ? -1 : 1;
    }
}

function select_field_new(el, subs, val)
{
	console.log(subs);
	console.log(val);
	
	if (subs != '')
	{
		var fmass = new Array();
		
		if ( /;/.test(subs) )
		{
			fmass = subs.split(';')
		}
		else
			fmass.push(subs);
		
		for (var i = 0; i < fmass.length; i++)
		{
			if (el.checked)
				document.querySelector('#tab_row_' + fmass[i]).style.display = 'table-row';
			else
				document.querySelector('#tab_row_' + fmass[i]).style.display = 'none';
			
		}

	}
	select_field(el);
}

function select_field(el)
{
	console.log(el.name + " : " + old_input_value[el.name] + " : " + el.checked);
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.checked)
			//document.querySelector("#sp_" + el.name).innerHTML = '';
			document.getElementById("sp_" + el.name).innerHTML = '';
		else
		{
			console.log("selected1 show save butt");
			document.querySelector("#save_data_button").style.display = 'block';
			document.querySelector("#save_data_button_top").style.display = '';
			//document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
			document.getElementById("sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
		}
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = !el.checked;
		console.log("selected2 show save butt");
		document.querySelector("#save_data_button").style.display = 'block';
		document.querySelector("#save_data_button_top").style.display = '';
		var elname = el.name.replace("\[","\\\[");
		elname = elname.replace("\]","\\\]");
		//console.log("elname: " + elname);
		document.querySelector("#sp_" + elname).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
	}
	
	if (el.name == 'field177')
	{
		if (el.checked)
		{
			document.querySelector('input[name="field123_reg"]').value = document.querySelector('input[name="field24_reg"]').value;
			document.querySelector('input[name="field123_ray"]').value = document.querySelector('input[name="field24_ray"]').value;
			document.querySelector('input[name="field123_city"]').value = document.querySelector('input[name="field24_city"]').value;
			document.querySelector('input[name="field123_street"]').value = document.querySelector('input[name="field24_street"]').value;
			document.querySelector('input[name="field123_dom"]').value = document.querySelector('input[name="field24_dom"]').value;
			document.querySelector('input[name="field123_str"]').value = document.querySelector('input[name="field24_str"]').value;
			document.querySelector('input[name="field123_corp"]').value = document.querySelector('input[name="field24_corp"]').value;
			document.querySelector('input[name="field123_apar"]').value = document.querySelector('input[name="field24_apar"]').value;
		}
	}
	else if (el.name == 'field85[1]')
	{
		console.log('hiiiit');
		check_ur();
	}
	else if (el.name == 'field85[1]')
	{
		console.log('hit sovm');
		check_sovm();
	}
		

	console.log(document.querySelector("#save_data_button"));
}

function change_select_new(el, subs, val)
{
	console.log(subs);
	console.log(val);
	if (subs != '')
	{
		var fmass = new Array();
		
		if ( /;/.test(subs) )
		{
			fmass = subs.split(';')
		}
		else
			fmass.push(subs);
		
		for (var i = 0; i < fmass.length; i++)
		{
			if (el.options[el.selectedIndex].value.toLowerCase() == val)
				document.querySelector('#tab_row_' + fmass[i]).style.display = 'table-row';
			else
				document.querySelector('#tab_row_' + fmass[i]).style.display = 'none';
			
		}

	}
	change_select(el);
}

function change_select(el)
{
	console.log(el.name + " : " + old_input_value[el.name] + " : " + el.options[el.selectedIndex].value);
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.options[el.selectedIndex].value)
			document.querySelector("#sp_" + el.name).innerHTML = '';
		else
		{
			console.log("selected3 show save butt");
			document.querySelector("#save_data_button").style.display = 'block';
			document.querySelector("#save_data_button_top").style.display = '';
			document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
		}
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = el.options[el.selectedIndex].value;
	}
	console.log(document.querySelector("#save_data_button"));
}

function focus_m(el)
{
	el.size = 5;
}

function blur_m(el)
{
	el.size = 1;
}

function del_form_prod(el)
{
	el.parentNode.removeChild(el);
	document.querySelector("#save_data_button").style.display = 'block';
	document.querySelector("#save_data_button_top").style.display = '';
}

function add_form_prod(el, name)
{
	var d = document.querySelector("#" + name);
	
	
	var sels = el.parentNode.querySelectorAll("select");
	var val1 = sels[0].options[sels[0].selectedIndex].value;
	var val2 = sels[1].options[sels[1].selectedIndex].value;
	var val3 = el.parentNode.querySelector('input[name=test_summmm]').value;
	sels[0].options[0].selected = true;
	sels[1].options[0].selected = true;
	el.parentNode.querySelector('input[name=test_summmm]').value = '';

	if ((val1 != "") && (val2 != ""))
	{
		d.innerHTML += '<div><span>' + val1 + ',' + val2 + ',' + val3 + '</span><a title="Удалить" href="#" onclick="javasccript:del_form_prod(this.parentNode);"><img src="/img/delete.png" width="16" height="16"></a></div>';
		//bank_inp_s(el.parentNode.previousSibling.lastChild.previousSibling);
		//console.log(wr);
	}
	document.querySelector("#save_data_button").style.display = 'block';
	document.querySelector("#save_data_button_top").style.display = '';
}

function add_bp(el)
{
	var sel = el.parentNode.querySelector("select");
	var val = sel.options[sel.selectedIndex].value;
	sel.options[0].selected = true;
	console.log(val);
	if (val != "")
	{
		var wr = el.parentNode.parentNode.querySelector(".bank_val");
		wr.innerHTML += '<input size="40" name="test" type="text" value="' + val + '"><br>';
		bank_inp_s(el.parentNode.previousSibling.lastChild.previousSibling);
		console.log(wr);
	}
}

function show_field4edit(el)
{
	var name = el.id.replace(new RegExp("span_", ""), "");
	
	if (el.className == 'banks')
	{
		var inp = el.parentNode.querySelector('div[name="bank_p"]');
		inp.style.display = "";
		inp = el.parentNode.querySelector('div[class="bank_val"]');
		inp.style.display = "";
	}
	else if (el.className == 'addr_span')
	{
		var inp = el.parentNode.querySelectorAll("input[name*=" + name + "]");
		console.log(inp);
		for (var i=0; i< inp.length; i++)
			inp[i].parentNode.style.display = "";
		inp[0].focus();
	}
	else
	{
	
		var inp = el.parentNode.querySelector("#inp_sp_" + name);
		if (inp == null)
			inp = el.parentNode.querySelector("input[name=" + name + "]");
		
		if (inp == null)
			inp = el.parentNode.querySelector("select[name=" + name + "]");
		
	//	if (inp == null)
		//	inp = el.parentNode.querySelector("#inp_sp_" + name);
		
		inp.style.display = "";
		inp.focus();
	}
	el.style.display = "none";
}

function clear_filer()
{
	document.querySelector('#filter_input').value = '';
	filter_mass('');
}

function filter_mass(el)
{
	//var v = new RegExp(el.value, "i");
	var v = new RegExp(el, "i");
	var has_hide = false;

	var divs = document.querySelectorAll(".block_wrap > .row_a");
	for (var n=0; n < divs.length; n++)
	{
		if (v.test(divs[n].innerHTML))
		{
			divs[n].style.display = '';
		}
		else
		{
			has_hide = true;
			divs[n].style.display = 'none';
		}
	}
	
	if (has_hide)
		document.querySelector('#clear_filter_button').style.display = '';
	else
	{
		document.querySelector('#clear_filter_button').style.display = 'none';
		//document.querySelector('#all_filkos_button').style.display = 'none';
	}
	//console.log(divs);
}

function bank_inp_s(el)
{
	document.querySelector("#save_data_button").style.display = 'block';
	document.querySelector("#save_data_button_top").style.display = '';
	var img = document.createElement("img");
		img.src = "/img/emblem-important.png";
		img.setAttribute("height", "13px");
		img.alt = "Данные изменились";
		img.title="Данные изменились";
		console.log(el.nextSibling);
		console.log(el.nextSibling.tagName.toLowerCase());
	if(el.nextSibling.tagName.toLowerCase() != 'img') 
	el.parentNode.insertBefore(img, el.nextSibling);
}

function focus_field(el)
{
	//console.log(el.name);
	
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.value)
			document.querySelector("#sp_" + el.name).innerHTML = '';
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = el.value;
	}
	
}

function ch_radio(el)
{
	console.log("ch_radio:" + old_input_value[el.name] + ", new: " + el.value + ", old_radio_value: " + old_radio_value[el.name] + ". el.checked:" + el.checked);
	
	
	if (el.name in old_radio_value)
	{
		if (old_radio_value[el.name] == el.value)
		{
			el.checked = false;
			old_radio_value[el.name] = '-1';
		}
		else
			old_radio_value[el.name] = el.value;
	}
	else
		old_radio_value[el.name] = el.value;
	
	var v = old_radio_value[el.name];

	if (old_input_value[el.name] != v)
	{
		console.log("blur show save butt");
		document.querySelector("#save_data_button").style.display = 'block';
		document.querySelector("#save_data_button_top").style.display = '';
		document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
	}
	else
	{
		document.querySelector("#sp_" + el.name).innerHTML = '';
	}
	
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == v)
		{
			document.querySelector("#sp_" + el.name).innerHTML = '';
		}
			
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = el.value;
	}
	
}

function blur_field(el)
{
	//console.log("out");
	if (old_input_value[el.name] != el.value)
	{
		console.log("blur show save butt");
		document.querySelector("#save_data_button").style.display = 'block';
		document.querySelector("#save_data_button_top").style.display = '';
		document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
	}
	else
	{
		document.querySelector("#sp_" + el.name).innerHTML = '';
	}
	
	console.log(document.querySelector("#save_data_button"));
}

function change_ast()
{
	var ch_astatus = document.querySelector("#ch_astatus");
	ch_astatus.setAttribute('class', 'hide_ch_st');
	var select_status = document.querySelector("#select_status");
	select_status.className = '';
}

function move_anketa()
{
	var move_a = document.querySelector("#move_a");
	move_a.setAttribute('class', 'hide_ch_st');
	var select_staff = document.querySelector("#select_staff");
	select_staff.className = '';
}

function ret_anketa()
{
	var ret_a = document.querySelector("#ret_a");
	ret_a.setAttribute('class', 'hide_ch_st');
	var ret_select_staff = document.querySelector("#ret_select_staff");
	ret_select_staff.className = '';
}

function show_close_form()
{
	var data = "block=anketa&action=get_close_st_list";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var close_mass = Item['close_mass'];
			var wind = document.querySelector('#window');
			wind.innerHTML = '';
			
			var div = document.createElement('div');
			div.id = "close_params";
			div.style = "overflow: auto;";
			div.style.border = "1px solid black";
			div.style.height = "400px";
			div.style.margin = "30px 30px 30px 30px";
			
			for (var i in close_mass)
			{
				let cm = close_mass[i];
				let d = document.createElement('div');
				d.innerHTML = '<label><input type="checkbox" name="cm_' + cm['id'] + '"> ' + cm['description'] + '</label>';
				div.appendChild(d);
			}
			
			let d = document.createElement('div');
			d.innerHTML = '<label><input type="checkbox" name="other"> иная:</label><br><textarea id="text_close_other" cols="50" rows="5" style="margin-left:20px;"></textarea>';
			div.appendChild(d);
			wind.appendChild(div);
			
			var b1 = document.createElement('button');
			b1.style.margin = "auto auto 30px 30px";
			b1.innerHTML = "Закрыть анкету";
			b1.setAttribute('onclick', 'javascript:formclose();');
			wind.appendChild(b1);
			
			var b2 = document.createElement('button');
			b2.style.float = "right";
			b2.style.margin = "auto 30px 30px auto";
			b2.innerHTML = "Отменить";
			b2.setAttribute('onclick', 'javascript:show("none");');
			wind.appendChild(b2);
			show('block');
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function formclose()
{
	var close_params = '';
	var div = document.querySelector('#close_params');
	var inps = div.querySelectorAll('input');
	
	var other_inp = (document.querySelector('#text_close_other').value) ? true : false;
	
	var hit = false;
	var hit2 = false;
	for (let i=0; i<inps.length; i++)
	{
		if ((inps[i].name == 'other') && other_inp)
			inps[i].checked = true;
		
		if (inps[i].checked)
		{
			hit = true;
			if (close_params != '')
				close_params += '|';
			close_params += inps[i].name;
			
			if (inps[i].name == 'other')
			{
				close_params += ':' + document.querySelector('#text_close_other').value;
				
			}
			else
				hit2 = true;
		}
	}
	
	if (!hit)
	{
		alert('Выберите причину закрытия!');
		return;
	}
	
	if (!hit2) {
		alert('Выберите причину закрытия!\n\nВыбирать "Иная" можно лишь вместе с подходящей причиной из списка!');
		return;
	}
	
	
	var data = "block=anketa&action=ch_status&id=" + anketa_id + "&new_status=work_complete" + "&close_params=" + close_params;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function close_form()
{
	var text_msg = "Дорогой друг! Ты уверен, что заявка не просрана?\n Ты спросил про заинтересованных лиц и возможные активы?";
	text_msg += " Возможно у клиента есть уже кредит и его можно рефинансировать по более низкой ставке. Ты спросил? ";
	text_msg += "А может быть ты считаешь, что не смог сам продать услугу и стоит попробовать коллеге? ";
	text_msg += "Короче, если уверен, то смело отправляй заявку в Валхаллу, но если не уверен - не удаляй, а то попадешь сам в АД!\n\n";
	if (!confirm(text_msg + "Закрыть анкету " + anketa_id + "?"))
		return;
	
	var data = "block=anketa&action=ch_status&id=" + anketa_id + "&new_status=work_complete";
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}
function select_status()
{
	var ch_astatus = document.querySelector("#ch_astatus");
	ch_astatus.className = '';
	
	var select_status = document.querySelector("#select_status");
	select_status.setAttribute('class', 'hide_ch_st');
	
	var st = select_status.options[select_status.selectedIndex].value;
	
	if (st == 'work_complete')
	{
		show_close_form();
		return;
	}
	
	var data = "block=anketa&action=ch_status&id=" + anketa_id + "&new_status=" + st;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function select_staff(id = 'select_staff')
{
	var move_a = document.querySelector("#move_a");
	if(move_a)
		move_a.className = '';
	
	var select_staff = document.querySelector("#" + id);
	select_staff.setAttribute('class', 'hide_ch_st');
	
	var st = select_staff.options[select_staff.selectedIndex].value;
	
	
	var data = "block=anketa&action=move_anketa&id=" + anketa_id + "&new_st=" + st;

	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function check_ur()
{
	let ur_sp = document.querySelector("#inp_sp_field85");
	let ch_b = ur_sp.querySelectorAll('input');
	
	var ch = false;
	for (let i in ch_b)
	{
		if ((ch_b[i].value == 'Безналичными на ЮЛ') && ch_b[i].checked)
			ch = true;
	}
	show_ur_fields(ch);
}

function check_sovm()
{
	let ch_b = document.querySelector('input[name=field48]');
	
	var ch = false;
	
	if (ch_b)
		ch = ch_b.checked;
	
	show_sovm_fields(ch);
}

function show_ur_fields(ch)
{
	let tab_row_field50 = document.querySelector('#tab_row_field50');
	let tab_row_field83 = document.querySelector('#tab_row_field83');
	let tab_row_field82 = document.querySelector('#tab_row_field82');
	let tab_row_field39 = document.querySelector('#tab_row_field39');
	
	if (ch)
	{
		tab_row_field50.style.display = '';
		tab_row_field83.style.display = '';
		tab_row_field82.style.display = '';
		tab_row_field39.style.display = '';
	}
	else
	{
		tab_row_field50.style.display = 'none';
		tab_row_field83.style.display = 'none';
		tab_row_field82.style.display = 'none';
		tab_row_field39.style.display = 'none';
	}
}

function show_sovm_fields(ch)
{
	let tab_row_field67 = document.querySelector('#tab_row_field67');
	let tab_row_field117 = document.querySelector('#tab_row_field117');
	let tab_row_field62 = document.querySelector('#tab_row_field62');
	let tab_row_field64 = document.querySelector('#tab_row_field64');
	let tab_row_field68 = document.querySelector('#tab_row_field68');
	let tab_row_field70 = document.querySelector('#tab_row_field70');
	let tab_row_field66 = document.querySelector('#tab_row_field66');
	let tab_row_field71 = document.querySelector('#tab_row_field71');
	let tab_row_field73 = document.querySelector('#tab_row_field73');
	let tab_row_field172 = document.querySelector('#tab_row_field172');
	let tab_row_field173 = document.querySelector('#tab_row_field173');
	
	console.log('2312312312312312');
	console.log(tab_row_field62.innerHTML);
	console.log(tab_row_field67.innerHTML);
	console.log(tab_row_field117.innerHTML);
	
	if (ch)
	{
		tab_row_field67.style.display = '';
		tab_row_field117.style.display = '';
		tab_row_field62.style.display = '';
		tab_row_field64.style.display = '';
		tab_row_field68.style.display = '';
		tab_row_field70.style.display = '';
		tab_row_field66.style.display = '';
		tab_row_field71.style.display = '';
		tab_row_field73.style.display = '';
		tab_row_field172.style.display = '';
		tab_row_field173.style.display = '';
	}
	else
	{
		tab_row_field67.style.display 	= 'none';
		tab_row_field117.style.display 	= 'none';
		tab_row_field62.style.display 	= 'none';
		tab_row_field64.style.display 	= 'none';
		tab_row_field68.style.display 	= 'none';
		tab_row_field70.style.display 	= 'none';
		tab_row_field66.style.display 	= 'none';
		tab_row_field71.style.display 	= 'none';
		tab_row_field73.style.display 	= 'none';
		tab_row_field172.style.display 	= 'none';
		tab_row_field173.style.display 	= 'none';
	}
}

function save_data()
{
	console.log("test");
	var inp = document.querySelector('input[name="sum"]');
	var num = inp.value.replace(/\D+/g,"");
	var num2 = num.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	
	if (inp.name == 'sum')
	{
		var msum = document.querySelector('input[name="field163"]');
		msum.value = num2;
		if (inp.value != '')
		{
			if (document.querySelector('#span_field163'))
				document.querySelector('#span_field163').style.display = 'none';
			msum.style.display = '';
		}
	}
	
	
	var inputs = document.querySelectorAll("div[class=fval]>input");
	
	for (var i in inputs)
	{
		if ((inputs[i].type != 'radio') && (inputs[i].type != 'checkbox'))
			inputs[i].value = encodeURIComponent(inputs[i].value);
	}
	console.log(inputs.length);
	//console.log(inputs);
	var data = getQueryString(inputs);
	for (var i in inputs)
	{
		if ((inputs[i].type != 'radio') && (inputs[i].type != 'checkbox'))
			inputs[i].value = decodeURIComponent(inputs[i].value);
	}
	/*var data = '';
	for (var n=0; n < inputs.length; n++)	
	{
		if (inputs.value)
			data += "&" + inputs[n].name + "=" + inputs.value;
	}*/
	var radio_b = document.querySelectorAll("div[class=fval] input[type=radio]");
	console.log(radio_b);
	data += "&" + getQueryString(radio_b);
	console.log(data);
	
	var selects = document.querySelectorAll("div[class=fval]>select");
	//console.log(selects);
	for (var n=0; n < selects.length; n++)
		data += "&" + selects[n].name + "=" + getSelection(selects[n]);
	
	inputs = document.querySelectorAll("div[class=fval] span input");
	//console.log(inputs);
	
	data += "&" + getQueryString(inputs);
	//for (var n=0; n < inputs.length; n++)	
		//data += "&" + inputs[n].name + "=" + getSelection(selects[n]);
	
	//data += "&" + getQueryString(selects);
	
	inputs = document.querySelectorAll("div[class=fval] div[class=addr_div] input");
	
	var addr_mass = new Array();
	for (var n=0; n < inputs.length; n++)
	{
		var name = inputs[n].name.split('_');
		if (name && name.length > 1)
		{
			name = name[0];
			if (!addr_mass[name])
				addr_mass[name] = new Array();
			addr_mass[name].push(inputs[n].value);
			
		}
		//data += "&" + selects[n].name + "=" + getSelection(selects[n]);
	}
	
	console.log(addr_mass);
	for (var key in addr_mass)
	{
		var vals = addr_mass[key];
		var datav = vals.join(';;;');
		data += "&" + key + "=" + datav;
	}
	
	var divs = document.querySelectorAll("div[class=bank_val]");
	console.log("test");
	console.log(divs);
	for (var n=0; n < divs.length; n++)
	{
		var tmp_b = "";
		console.log("test2");
		var inps = divs[n].querySelectorAll('input');
		console.log(inps);
		for (var k=0; k < inps.length; k++)
		{
			if(tmp_b == "")
				tmp_b = "&" + divs[n].getAttribute("name") + "=";
			else
				tmp_b += ",";
			
			tmp_b += inps[k].value;
		}
		
		data += tmp_b;
		console.log(tmp_b);
	}
	
	var divs = document.querySelectorAll("div[class=kred_prod]");
	
	for (var n=0; n < divs.length; n++)
	{
		var div_data = divs[n].querySelectorAll('div>span');
		
		var tmp_b = "";
		for (var k=0; k < div_data.length; k++)
		{
			if(tmp_b == "")
				tmp_b = "&" + divs[n].id + "=";
			else
				tmp_b += ";;;";
			
			tmp_b += div_data[k].innerHTML;
		}
		
		data += tmp_b;
		console.log(tmp_b);
	}
	
	//data = encodeURIComponent(data);
	data += "&block=anketa&action=save_data&id=" + anketa_id;
	console.log(data);
	//return;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			alert('Данные успешно сохранены');
			location.reload();
		}
		else 
		{
			//butt.style.backgroundColor = '#FFa0a0';
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function ch_name(sel)
{
	//console.log(sel.parentNode.parentNode.innerHTML);
	var name = sel.options[sel.options.selectedIndex].value;
	var inp = sel.parentNode.parentNode.querySelector('input');
	//console.log(inp.parentNode.innerHTML);
	//console.log(name);
	inp.setAttribute("name", name);
}

function add_field_value(sel)
{
	//console.log(sel.parentNode.parentNode.innerHTML);
	var name = sel.options[sel.options.selectedIndex].value;
	
	/*var inp = sel.parentNode.parentNode.querySelector('input');
	inp.setAttribute("name", name);*/
	
	var div_cell = sel.parentNode.parentNode.querySelector('.fval');
	
	var inp;
	var multi = false;
	switch (field_types[name])
	{
		case 'multiselect':
			multi = true;
		case 'select':
		{
			inp = document.createElement('select');
			//inp.type = "select";
			inp.id = "select_" + name;
			if (multi)
				inp.setAttribute("multiple", true);
			
				var opt = document.createElement('option');
				opt.innerHTML = 'Выберите';
				opt.value = '';
			inp.appendChild(opt);
			
			break;
		}
		case 'checkbox':
		{
			inp = document.createElement('input');
			inp.type = "checkbox";
			break;
		}
		default:
		{
			inp = document.createElement('input');
			inp.type = "text";
			inp.placeholder = "Поле для ввода";
			break;
		}
	}
	inp.setAttribute("name", name);
/*	var inp = document.createElement('input');
	inp.setAttribute("name", name);
				inp.type = "text";
				inp.placeholder = "Поле для ввода";
	*/			
	div_cell.appendChild(inp);
	
	if ((field_types[name] == 'select') || (field_types[name] == 'multiselect'))
		add_options(name);
}

function add_options(name)
{
	var sel = document.querySelector('#select_' + name);
	var data = "&block=anketa&action=getvalues&name=" + name;
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'])
			{
				var data = Item['data'];
				for (var key in data)
				{
					var opt = document.createElement('option');
						opt.innerHTML = data[key];
						opt.value = data[key];
					sel.appendChild(opt);
				}
			}
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_field()
{
	//console.log("rere");
	//console.log(mass_fields);
	var tab = document.querySelector('#anketa_data');
	var row = document.querySelector('#add_field_r');
	
	
	var sell = document.querySelectorAll('.sel_a');
	//console.log(sell.length);
	if (sell.length)
	{
		for (i = 0; i < sell.length; i++)
		{
			var sel_ind = sell[i].options.selectedIndex;
			var sel_v = sell[i].options[sel_ind].value;
			var sel_t = sell[i].options[sel_ind].text;
			//console.log(sel_ind + ' :: ' + sel_v + " :: " + sel_t);
			if (sel_v in mass_fields)
				delete mass_fields[sel_v];
			
		}
		//console.log(mass_fields);
	}
	
	var div_row = document.createElement('div');
		div_row.className = "row_a";
		
		var div_cell = document.createElement('div');
			div_cell.className = "fname";
			//div_cell.innerHTML = "test";
			
			var sel = document.createElement('select');
				sel.className = "sel_a";
				//sel.onchange = "javascript:ch_name(this)";
				//sel.setAttribute("onchange", "javascript:ch_name(this)");
				sel.setAttribute("onchange", "javascript:add_field_value(this)");
				var opt = document.createElement('option');
					opt.value = "-1";
					opt.innerHTML = "Выберите поле";
			
				sel.appendChild(opt);
				
				for (var key in mass_fields) 
				{
					//console.log( key + ": " + mass_fields[key] );
					opt = document.createElement('option');
					opt.name = key;
					opt.value = key;
					opt.innerHTML = mass_fields[key];
					sel.appendChild(opt);
				};
			div_cell.appendChild(sel);
			
			var inpp = document.createElement('input');
				inpp.className = "sel_i";
				inpp.setAttribute("placeholder", "Начните набирать тут для фильтра");
				inpp.setAttribute("oninput", "javascript:select_filter(this)");
				inpp.style = "width:98%";
			div_cell.appendChild(inpp);
			
		div_row.appendChild(div_cell);
		
		div_cell = document.createElement('div');
			div_cell.className = "fval";
			
			
			/*var inp = document.createElement('input');
				inp.type = "text";
				inp.placeholder = "Поле для ввода";
				
			div_cell.appendChild(inp);*/
		
		div_row.appendChild(div_cell);
	tab.insertBefore(div_row, row);
}

function select_filter(el)
{
	var sel = el.previousSibling;
	console.log(el.value);
//	console.log(Object.keys(sel.options));
	var reg = new RegExp(el.value, "i");
	
	//console.log(reg);
	var first = true;
	for (var n=0; n < sel.options.length; n++)
	{
		//console.log(sel.options[n].innerHTML);
		if(reg.test(sel.options[n].innerHTML))
		{
			//console.log('yes');
			//console.log(sel.options[n].innerHTML);
			sel.options[n].style.display = '';
			if (first)
			{
				sel.selectedIndex = n;
				first = false;
			}
		}			
		else
		{
			sel.options[n].style.display = 'none';
		}
			
	}
		
	/*for (var key in sel.options)
	{
		//if (sel.options[key].type == 'option')
		console.log(sel.options[key]);
		console.log(sel.options[key].type);
	}*/
}

function close_tooltip(el)
{
	document.body.removeChild(el.parentNode);
}

var old_tel_c = new Array();

function tel_format(inp)
{
	var num = inp.value.replace(/\D+/g,"");
	
	if ((num.length < 11) && (inp.value[0] != '7') && (inp.value[0] != '8'))
		num = "7" + num;
		
	
	if (num.length < 2)
		inp.value = "7(";
	else if (num.length < 5)
	{
		var re = new RegExp("\\d(\\d\+)", "");
		var mass = re.exec(num);
		console.log(mass);
		inp.value = "7(" + mass[1];
	}
	else if (num.length < 8)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2];
	}
	/*else if ((num.length == 9) && (inp[0] != '7') && (inp[0] != '8'))
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\\d\\d)(\\d\\d)(\\d\\d\?)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2] + '-' + mass[3] + '-' + mass[4];
	}*/
	else if (num.length < 10)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2] + '-' + mass[3];
	}
	else// if (num.length < 12)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\\d\\d)(\\d\\d)(\\d\\d\?)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2] + '-' + mass[3] + '-' + mass[4];
	}
	//var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
	//var mass = reg.exec(tel);
}

function date_format(inp)
{
	var re = new RegExp("(\\d+)\.(\\d+)\.(\\d\\d+)", "");
	var mass = re.exec(inp.value);
	
	if (mass)
	{
		console.log(inp.value);
		if (parseInt(mass[1]) < 10)
			mass[1] = "0" + parseInt(mass[1]);
		
		if (parseInt(mass[2]) < 10)
			mass[2] = "0" + parseInt(mass[2]);
		
		
		/*if (parseInt(mass[3]) < 50)
			mass[3] = "20" + mass[3];
		else if (parseInt(mass[3]) < 100)
			mass[3] = "19" + mass[3];*/
		
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
		console.log(inp.value);
	}
	//console.log(mass);
		
	var d = new Date;
	var y = d.getFullYear();
	var num = inp.value.replace(/\D+/g,"");
	
	if (num.length < 2)
	{
		inp.value = (num > 31) ? 31 : num;
	}
	/*else if (num.length == 2)
	{
		inp.value = (num > 31) ? 31 : num + ".";
		//inp.value = inp.value + ".";
	}*/
	else if ((num.length > 2) && (num.length < 5))
	{
		var re = new RegExp("(\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		inp.value = mass[1] + "." + mass[2];
	}
	else if (num.length > 8) 
	{
		var re = new RegExp("(\\d\\d)(\\d\\d)(\\d\\d\\d\\d)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		mass[3] = (mass[3] > y) ? y : mass[3];
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
	}
	else 
	{
		var re = new RegExp("(\\d\\d)(\\d\\d)(\\d+)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		mass[3] = (mass[3] > y) ? y : mass[3];
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
	}
	//var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
	//var mass = reg.exec(tel);
}

var sort = 2;
var sort_by = true;

function sort_pod(td)
{
	//console.log(td);
	//background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=') no-repeat right 55%
	//background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=') no-repeat right 55%
	
	var zal = false;
	if((td == 9) || (td == 10))
		zal = true;

	
	var res = document.querySelector('#prod_result');
	var divhead = res.querySelector('div[class="prb_head"]');
	var div = res.querySelectorAll('div[class*="prb_row"]');
	//console.log(divhead);
	var ddd = divhead.childNodes;
	//console.log(div);
	
	if (td == sort)
		sort_by = !sort_by;
	sort = td;
	
	for (var c = 0; c < ddd.length; c++)
	{
		//console.log(ddd[c].innerHTML);
		if (c == parseInt(td))
		{
			if (sort_by)
			{
				//console.log('up');
				ddd[c].childNodes[0].className = 'sort_up';
				//ddd[td].style.background = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';
			}
			else
			{
				ddd[c].childNodes[0].className = 'sort_d';
				//console.log('down');
				//ddd[td].style.background = 'url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=") no-repeat right 55%;';
			}
		}
		else
			ddd[c].childNodes[0].className = 'sort_no';
	}
	
	
	var mass_t = new Array();
	var mass_sort = new Array();
	for (var i = 0; i < div.length; i++)
	{
		var div_in = div[i].querySelectorAll('div[class*="prbcell"]');
		mass_sort.push(div_in[sort].innerHTML);
		var tmp_a = {};
		tmp_a.key = i;
		if (td == 2)
		{
			var arr = div_in[sort].innerHTML.split('<br>')
			tmp_a.sort_by = arr[0].replace(/,/g,".");
		}
		else if ((td == 4) || (td == 5))
		{
			var arr = div_in[sort].innerHTML.split('<br>')
			tmp_a.sort_by = arr[0].replace(/\D+/g,"");
		}
		else if ((td == 0) || (td == 6))
			tmp_a.sort_by = div_in[sort].innerHTML.replace(/\D+/g,"");
		else
			tmp_a.sort_by = div_in[sort].innerHTML;
		tmp_a.data = div[i];
		mass_t.push(tmp_a);
	}
	
	if (zal)
	{
		if (sort_by)
			mass_t.sort(sIncrease);
		else
			mass_t.sort(sDecrease);
	}
	else
	{
		if (sort_by)
			mass_t.sort(dIncrease);
		else
			mass_t.sort(dDecrease);
	}
	
	//res.innerHTML = divhead;
	res.innerHTML = '';
	res.appendChild(divhead);
	for (var key in mass_t)
	{
		//console.log(mass_t[key].key + " : " + mass_t[key].sort_by);
		//res.innerHTML += mass_t[key].data;
		res.appendChild(mass_t[key].data);
	}
	
	return 0;
	
	var zal = false;
	if((td == 4) || (td == 7))
		zal = true;

	if((td == 2) || (td == 8) || (td == 9) || (td == 10) || zal)
	{
		if (td == sort)
			sort_by = !sort_by;
		sort = td;
		//console.log(td + ": " + sort_by);
		
		var res = document.querySelector('#prod_result');
		//console.log(res.innerHTML);
		var div = document.querySelectorAll('#prod_result > div');
		console.log(div);
		var divhead = res.querySelector('div[class=prb_head]');
		//var ddd = divhead.getElementsByTagName('div');
		var ddd = divhead.childNodes;
		//console.log(ddd[c].innerHTML);
		//console.log(ddd[td].parentNode.innerHTML);
		for (var c = 0; c < ddd.length; c++)
		{
			//console.log(ddd[c].innerHTML);
			if (c == parseInt(td))
			{
				if (sort_by)
				{
					//console.log('up');
					ddd[c].childNodes[0].className = 'sort_up';
					//ddd[td].style.background = 'url(\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDBoNHYyaC00ek0wIDNoNnYyaC02ek0wIDZoOHYyaC04eiIvPjwvc3ZnPgo=\") no-repeat right 55%;';
				}
				else
				{
					ddd[c].childNodes[0].className = 'sort_d';
					//console.log('down');
					//ddd[td].style.background = 'url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiIGZpbGw9IiM2NTY1NjUiPjxwYXRoIGQ9Ik0wIDZoNHYyaC00ek0wIDNoNnYyaC02ek0wIDBoOHYyaC04eiIvPjwvc3ZnPgo=") no-repeat right 55%;';
				}
			}
			else
				ddd[c].childNodes[0].className = 'sort_no';
		}
		
		var mass_t = new Array();
		var mass_sort = new Array();
		for (var i = 1; i < div.length; i++)
		{
			var div_in = div[i].querySelectorAll('div');
			mass_sort.push(div_in[sort].innerHTML);
			var tmp_a = {};
			tmp_a.key = i;
			tmp_a.sort_by = div_in[sort].innerHTML;
			tmp_a.data = div[i].innerHTML;
			mass_t.push(tmp_a);
		}
		
		//console.log("массив");
	
		if (zal)
		{
			if (sort_by)
				mass_t.sort(sIncrease);
			else
				mass_t.sort(sDecrease);
		}
		else
		{
			if (sort_by)
				mass_t.sort(dIncrease);
			else
				mass_t.sort(dDecrease);
		}
		
		res.innerHTML = "<div>" + divhead.innerHTML + "</div><br>";
		for (var key in mass_t)
		{
			//console.log(mass_t[key].key + " : " + mass_t[key].sort_by);
			res.innerHTML += "<div>" + mass_t[key].data + "</div>";
		}
	}
	var res2 = document.querySelector('#prod_result');
	//console.log(res2.innerHTML);
}

function dIncrease(i, ii)
{ // По возрастанию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function dDecrease(i, ii)
{ // По убыванию
	i = parseFloat(i.sort_by);
	ii = parseFloat(ii.sort_by);
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function sIncrease(i, ii)
{ // По возрастанию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return 1;
    else if (i < ii)
        return -1;
    else
        return 0;
}

function sDecrease(i, ii)
{ // По убыванию
	i = i.sort_by;
	ii = ii.sort_by;
    if (i > ii)
        return -1;
    else if (i < ii)
        return 1;
    else
        return 0;
}

function show(state)
{
	document.getElementById('window').style.display = state;
	document.getElementById('wrap').style.display = state;
	
	if (state == 'none')
	{
		document.getElementById('window_prb').style.display = state;
		document.getElementById('window_cp').style.display = state;
		document.getElementById('za_files_wrap').style.display = state;
		var tmp_d = document.querySelector('div.note_wind');
		if (tmp_d)
		{
			document.body.removeChild(tmp_d);
		}
	}
}

function ajaxQuery2(url, method, param, async, onsuccess, onfailure) 
{
	var xmlHttpRequest = new XMLHttpRequest();
	var callback = function(r) 
	{ 
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r)); 
	};
	if(async) 
	{ 
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } } 
	}
	xmlHttpRequest.open(method, url, async);
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}


var showingTooltip;
var showingTooltip2;

var credit_history = new Array();

document.onclick = function(e)
{
	var target = e.target;

	var tooltip = target.getAttribute('data-tooltip');
	if (!tooltip) return;

	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	

	console.log('tttt');
	var tmp_str = '';
	switch(tooltip)
	{
		case 'double_phone':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			//for (var key in double_phone_m)
			for(var key = double_phone_m.length-1; key >= 0; key--)
			{
				if(double_phone_m[key])
				tmp_str += 'Анкета <a target=_blank href="/details/' + key + '/">№' + key + "</a> " + double_phone_m[key] + "<br>";
			}
			tooltipElem.id = 'double_phone';
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'double_fio':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			//for (var key in double_fio_m)
			for(var key = double_fio_m.length-1; key >= 0; key--)
			{
				if(double_fio_m[key])
				tmp_str += 'Анкета <a target=_blank href="/details/' + key + '/">№' + key + "</a> " + double_fio_m[key] + "<br>";
			}
			tooltipElem.id = 'double_fio';
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'double_ip':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			//for (var key in double_ip_m)
			for(var key = double_ip_m.length-1; key >= 0; key--)
			{
				if(double_ip_m[key])
				tmp_str += 'Анкета <a target=_blank href="/details/' + key + '/">№' + key + "</a> " + double_ip_m[key] + "<br>";
			}
			tooltipElem.id = 'double_fio';
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'get_promoney_res':
		{
			if (document.getElementById('get_promoney_res'))
				document.body.removeChild(document.getElementById('get_promoney_res'));
			
			console.log('Click:get_promoney_res');
				
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';

			tooltipElem.id = 'get_promoney_res';
			tooltipElem.style.width = '650px';
			tooltipElem.style.height = '600px';
			tooltipElem.style.overflow = 'auto';
			var promoney_id = target.getAttribute('promoney_id');
			tmp_str += 'test ' + promoney_id;
			
			var data = "&block=anketa&action=get_promoney_res&pr_id=" + promoney_id;
	
			ajaxQuery('/ajax.php','POST', data, true, function(req) 
			{
				var tmp_page = req.responseText;
				var Item = eval("obj = " + tmp_page);
				if (Item['status'] == 'ok')
				{
					
					var cr_reit_close = Item['data']['cr_reit_close'];
					var cr_reit_active = Item['data']['cr_reit_active'];
					var cr_reit_6m = Item['data']['cr_reit_6m'];
					var cr_reit_12m = Item['data']['cr_reit_12m'];
					
					credit_history.length = 0;
					var te = document.getElementById('get_promoney_res');
					te.innerHTML = '<img class="close_tooltip" onclick="javascript:close_tooltip(this); window.location.hash = \'\';" src="/img/close.png">';
					
					var la = Item['data']['lim_all'] + '';
					
					//var la = la.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					console.log('la=' + la);
					//la = la.replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
					la = la.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					console.log('la=' + la);
					
					credit_history['field131'] = [la, ''];
											
					var bn = Item['data']['bal_now'] + '';
					//bn = bn.replace(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g, " $1 ");
					bn = bn.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					
					//bn = bn.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					
					credit_history['field132'] = [bn, ''];
					credit_history['field96'] = [Item['data']['miss_credit_acc'], ''];
					credit_history['field129'] = [Item['data']['totalActiveBalanceAccounts'], ''];
					credit_history['field130'] = [Item['data']['mfo'], ''];
					credit_history['field94'] = [Item['data']['next_pay'], ''];
					
					//cr_reit_close
					//Просрочка от 1 до 29::Просрочка от 30 до 59::Просрочка от 60 до 89::Просрочка от 90 до 119::Просрочка более 120
					var tmp_cr_his = '';
					var tmp_cr_reit = parseInt(cr_reit_close['5']) + parseInt(cr_reit_close['6']) + parseInt(cr_reit_close['7']) + parseInt(cr_reit_close['8']) + parseInt(cr_reit_close['9']);
					if (tmp_cr_reit > 0)
						tmp_cr_his = 'Просрочка более 120';
					else if (parseInt(cr_reit_close['4']) > 0)
						tmp_cr_his = 'Просрочка от 90 до 119';
					else if (parseInt(cr_reit_close['3']) > 0)
						tmp_cr_his = 'Просрочка от 60 до 89';
					else if (parseInt(cr_reit_close['2']) > 0)
						tmp_cr_his = 'Просрочка от 30 до 59';
					else if (parseInt(cr_reit_close['A']) > 0)
						tmp_cr_his = 'Просрочка от 1 до 29';
					
					credit_history['field133'] = [tmp_cr_his, ''];					
					credit_history['field134'] = [cr_reit_close['A'], ''];	//1-29		B	A
					credit_history['field135'] = [cr_reit_close['2'], ''];	//30-59		C	2
					credit_history['field136'] = [cr_reit_close['3'], ''];	//60-89		D	3
					credit_history['field137'] = [cr_reit_close['4'], ''];	//90-119	E	4
					credit_history['field138'] = [parseInt(cr_reit_close['5']) + parseInt(cr_reit_close['7']) + parseInt(cr_reit_close['8']) + parseInt(cr_reit_close['9']), ''];	//120+		F	5
					
					//cr_reit_active
					tmp_cr_his = '';
					tmp_cr_reit = parseInt(cr_reit_active['5']) + parseInt(cr_reit_active['6']) + parseInt(cr_reit_active['7']) + parseInt(cr_reit_active['8']) + parseInt(cr_reit_active['9']);
					if (tmp_cr_reit > 0)
						tmp_cr_his = 'Просрочка более 120';
					else if (parseInt(cr_reit_active['4']) > 0)
						tmp_cr_his = 'Просрочка от 90 до 119';
					else if (parseInt(cr_reit_active['3']) > 0)
						tmp_cr_his = 'Просрочка от 60 до 89';
					else if (parseInt(cr_reit_active['2']) > 0)
						tmp_cr_his = 'Просрочка от 30 до 59';
					else if (parseInt(cr_reit_active['A']) > 0)
						tmp_cr_his = 'Просрочка от 1 до 29';
					
					credit_history['field139'] = [tmp_cr_his, ''];
					credit_history['field140'] = [cr_reit_active['A'], ''];
					credit_history['field141'] = [cr_reit_active['2'], ''];
					credit_history['field142'] = [cr_reit_active['3'], ''];
					credit_history['field143'] = [cr_reit_active['4'], ''];
					//credit_history['field144'] = [cr_reit_active['5'], ''];
					credit_history['field144'] = [parseInt(cr_reit_active['5']) + parseInt(cr_reit_active['7']) + parseInt(cr_reit_active['8']) + parseInt(cr_reit_active['9']), ''];
					
					//cr_reit_6m
					tmp_cr_his = '';
					tmp_cr_reit = parseInt(cr_reit_6m['5']) + parseInt(cr_reit_6m['6']) + parseInt(cr_reit_6m['7']) + parseInt(cr_reit_6m['8']) + parseInt(cr_reit_6m['9']);
					if (tmp_cr_reit > 0)
						tmp_cr_his = 'Просрочка более 120';
					else if (parseInt(cr_reit_6m['4']) > 0)
						tmp_cr_his = 'Просрочка от 90 до 119';
					else if (parseInt(cr_reit_6m['3']) > 0)
						tmp_cr_his = 'Просрочка от 60 до 89';
					else if (parseInt(cr_reit_6m['2']) > 0)
						tmp_cr_his = 'Просрочка от 30 до 59';
					else if (parseInt(cr_reit_6m['A']) > 0)
						tmp_cr_his = 'Просрочка от 1 до 29';
					
					credit_history['field145'] = [tmp_cr_his, ''];
					credit_history['field146'] = [cr_reit_6m['A'], ''];
					credit_history['field147'] = [cr_reit_6m['2'], ''];
					credit_history['field148'] = [cr_reit_6m['3'], ''];
					credit_history['field149'] = [cr_reit_6m['4'], ''];
					//credit_history['field150'] = [cr_reit_6m['5'], ''];
					credit_history['field150'] = [parseInt(cr_reit_6m['5']) + parseInt(cr_reit_6m['7']) + parseInt(cr_reit_6m['8']) + parseInt(cr_reit_6m['9']), ''];
					
					//cr_reit_12m
					tmp_cr_his = '';
					tmp_cr_reit = parseInt(cr_reit_12m['5']) + parseInt(cr_reit_12m['6']) + parseInt(cr_reit_12m['7']) + parseInt(cr_reit_12m['8']) + parseInt(cr_reit_12m['9']);
					if (tmp_cr_reit > 0)
						tmp_cr_his = 'Просрочка более 120';
					else if (parseInt(cr_reit_12m['4']) > 0)
						tmp_cr_his = 'Просрочка от 90 до 119';
					else if (parseInt(cr_reit_12m['3']) > 0)
						tmp_cr_his = 'Просрочка от 60 до 89';
					else if (parseInt(cr_reit_12m['2']) > 0)
						tmp_cr_his = 'Просрочка от 30 до 59';
					else if (parseInt(cr_reit_12m['A']) > 0)
						tmp_cr_his = 'Просрочка от 1 до 29';
					
					credit_history['field151'] = [tmp_cr_his, ''];
					credit_history['field152'] = [cr_reit_12m['A'], ''];
					credit_history['field153'] = [cr_reit_12m['2'], ''];
					credit_history['field154'] = [cr_reit_12m['3'], ''];
					credit_history['field155'] = [cr_reit_12m['4'], ''];
					//credit_history['field156'] = [cr_reit_12m['5'], ''];
					credit_history['field156'] = [parseInt(cr_reit_12m['5']) + parseInt(cr_reit_12m['7']) + parseInt(cr_reit_12m['8']) + parseInt(cr_reit_12m['9']), ''];
					
					
					
					
					
					credit_history['field157'] = [Item['data']['restr'], ''];
					credit_history['field158'] = [Item['data']['last_7'], ''];
					credit_history['field159'] = [Item['data']['last_14'], ''];
					credit_history['field160'] = [Item['data']['last_mon'], ''];
					
					
					var cred_his_data = document.querySelector('#cred_his_data');
					var inps = cred_his_data.querySelectorAll('.fval>input');
					
					//console.log(inps);
					
					//inps.push(next_pay);
					
					for (key in inps) 
					{
						var inp = inps[key];
						if (inp.type == 'checkbox')
						{
							if (credit_history[inp.name] != null)
							{
								var tmp_i = (inp.checked) ? 1 : 0;
							
								if (tmp_i ^ credit_history[inp.name][0])
									credit_history[inp.name][1] = ' <font color="red">Не совпадают! (' + tmp_i + ')</font>';
							}
						}
						else if (inp.type == 'text')
						{
							if ((credit_history[inp.name] != null) && (inp.value != ''))
							{
								/*var tmp_inp_v = inp.value.replace(/\s/g, '');
								var tmp_inp_v = inp.value.replace(/\s/g, '');
								
								console.log('inp.value=' + inp.value);
								console.log('credit_history[inp.name][0]=' + credit_history[inp.name][0]);
								console.log('tmp_inp_v=' + tmp_inp_v);
								if (tmp_inp_v != credit_history[inp.name][0])*/
								if (inp.value != credit_history[inp.name][0])
									credit_history[inp.name][1] = ' <font color="red">Не совпадают! (' + inp.value + ')</font>';
							}
						}

					}
					
					var inp = document.querySelector('input[name="field94"]');
					if ((credit_history[inp.name] != null) && (inp.value != ''))
					{
						var test1 = "" + inp.value;
							test1 = parseInt(test1.replace(/\D+/g,""));
							
							console.log('test1=' + test1);
							console.log('test2=' + inp.value);
						if (test1 != credit_history[inp.name][0])
							credit_history[inp.name][1] = ' <font color="red">Не совпадают! (' + inp.value + ')</font>';
					}
					
					//console.log(inps);
					
					var inps = document.querySelectorAll('input[name="field96"]');
					var inp_tmp = '';
					console.log("==============");
					for (var i = 0; i < inps.length; i++)
					{
						if (inps[i].checked)
							inp_tmp = inps[i].value;
							//console.log(inps[i].value);
					}
					
					inp_tmp = (inp_tmp.toLowerCase() == 'да') ? 1 : 0;
					
					if (((inp_tmp > 0) && (credit_history['field96'][0] == 0)) || ((inp_tmp == 0) && (credit_history['field96'][0] > 0)))
						credit_history['field96'][1] = ' <font color="red">Не совпадают! (' + inp_tmp + ')</font>';
					
					//console.log(inp.name);
					console.log(inps);
					console.log("==============");
					
					
					var sel = cred_his_data.querySelectorAll('.fval>select');
					for (key in sel) 
					{
						var inp = sel[key];
						
						/*if (credit_history[inp.name] != null)
						{
						console.log(inp.name);
						console.log(inp.value);
						console.log(credit_history[inp.name][0]);
						}*/
						//if ((credit_history[inp.name] != null) && (inp.value != ''))
						if (credit_history[inp.name] != null)
						{
							if (inp.value != credit_history[inp.name][0])
								credit_history[inp.name][1] = ' <font color="red">Не совпадают! (' + inp.value + ')</font>';
						}
					}
					
					
					
					
					var cr_his_det = cred_his_data.parentNode.parentNode.open = true;
					//cred_his_data.top = '0px';;
					location.hash = 'cred_his_det'
					
					if (Item['data']['nbki_error'] == 1)
						te.innerHTML += '<div style="color:red; font-weight:bold;">В отчете могут быть ошибки!</div>';
					
					te.innerHTML += '<div>Наличие текущих просроченных платежей: ' + Item['data']['miss_credit_acc'] + 			credit_history['field96'][1] + '</div>';
					te.innerHTML += '<div>Активных кредитных обязательств: ' + Item['data']['totalActiveBalanceAccounts'] + 	credit_history['field129'][1] + '</div>';
					te.innerHTML += '<div>Есть активные займы МФО (на суммы до 100 т.р): ' + Item['data']['mfo'] + 				credit_history['field130'][1] + '</div>';
					te.innerHTML += '<div>Общий лимит: ' + la + ' р.' + 														credit_history['field131'][1] + '</div>';
					te.innerHTML += '<div>Текущий баланс: ' + bn + ' р.' + 														credit_history['field132'][1] + '</div>';
					te.innerHTML += '<div>Сумма ежемесячных выплат по текущим кредитам: ' + Item['data']['next_pay'] + ' р.' + 	credit_history['field94'][1] + '</div>';
					
					te.innerHTML += '<br>Количество просрочек по закрытым:';
					te.innerHTML += '<div>до 29: ' + 		credit_history['field134'][0] + 	credit_history['field134'][1] + '</div>';
					te.innerHTML += '<div>от 30 до 59: ' + 	credit_history['field135'][0] + 	credit_history['field135'][1] + '</div>';
					te.innerHTML += '<div>от 60 до 89: ' + 	credit_history['field136'][0] + 	credit_history['field136'][1] + '</div>';
					te.innerHTML += '<div>от 90 до 119: ' + credit_history['field137'][0] + 	credit_history['field137'][1] + '</div>';
					te.innerHTML += '<div>больше 120: ' + 	credit_history['field138'][0] + 	credit_history['field138'][1] + '</div>';
					te.innerHTML += '<div>Постоянные несвоевременные платежи: ' 		  + 	cr_reit_close['7'] + 	'</div>';
					te.innerHTML += '<div>Изъятие: ' 									  + 	cr_reit_close['8'] + 	'</div>';
					te.innerHTML += '<div>Безнадежный долг/переданна взыскание: ' 		  + 	cr_reit_close['9'] + 	'</div>';
					
					te.innerHTML += '<div>Наихудший платежный статус: ' + 	credit_history['field133'][0] + 	credit_history['field133'][1] + '</div>';
					
					te.innerHTML += '<br>Количество просрочек по активным:';
					te.innerHTML += '<div>до 29: ' + 		credit_history['field140'][0] + 	credit_history['field140'][1] + '</div>';
					te.innerHTML += '<div>от 30 до 59: ' + 	credit_history['field141'][0] + 	credit_history['field141'][1] + '</div>';
					te.innerHTML += '<div>от 60 до 89: ' + 	credit_history['field142'][0] + 	credit_history['field142'][1] + '</div>';
					te.innerHTML += '<div>от 90 до 119: ' + credit_history['field143'][0] + 	credit_history['field143'][1] + '</div>';
					te.innerHTML += '<div>больше 120: ' + 	credit_history['field144'][0] + 	credit_history['field144'][1] + '</div>';
					te.innerHTML += '<div>Постоянные несвоевременные платежи: ' 		  + 	cr_reit_active['7'] + 	'</div>';
					te.innerHTML += '<div>Изъятие: ' 									  + 	cr_reit_active['8'] + 	'</div>';
					te.innerHTML += '<div>Безнадежный долг/переданна взыскание: ' 		  + 	cr_reit_active['9'] + 	'</div>';
					te.innerHTML += '<div>Наихудший платежный статус: ' + 	credit_history['field139'][0] + 	credit_history['field139'][1] + '</div>';
					
					te.innerHTML += '<br>Количество просрочек за 6 месяцев:';
					te.innerHTML += '<div>до 29: ' + 		credit_history['field146'][0] + 	credit_history['field146'][1] + '</div>';
					te.innerHTML += '<div>от 30 до 59: ' + 	credit_history['field147'][0] + 	credit_history['field147'][1] + '</div>';
					te.innerHTML += '<div>от 60 до 89: ' + 	credit_history['field148'][0] + 	credit_history['field148'][1] + '</div>';
					te.innerHTML += '<div>от 90 до 119: ' + credit_history['field149'][0] + 	credit_history['field149'][1] + '</div>';
					te.innerHTML += '<div>больше 120: ' + 	credit_history['field150'][0] + 	credit_history['field150'][1] + '</div>';
					te.innerHTML += '<div>Постоянные несвоевременные платежи: ' 		  + 	cr_reit_6m['7'] + 	'</div>';
					te.innerHTML += '<div>Изъятие: ' 									  + 	cr_reit_6m['8'] + 	'</div>';
					te.innerHTML += '<div>Безнадежный долг/переданна взыскание: ' 		  + 	cr_reit_6m['9'] + 	'</div>';
					te.innerHTML += '<div>Наихудший платежный статус: ' + 	credit_history['field145'][0] + 	credit_history['field145'][1] + '</div>';
					
					te.innerHTML += '<br>Количество просрочек за 12 месяцев:';
					te.innerHTML += '<div>до 29: ' + 		credit_history['field152'][0] + 	credit_history['field152'][1] + '</div>';
					te.innerHTML += '<div>от 30 до 59: ' + 	credit_history['field153'][0] + 	credit_history['field153'][1] + '</div>';
					te.innerHTML += '<div>от 60 до 89: ' + 	credit_history['field154'][0] + 	credit_history['field154'][1] + '</div>';
					te.innerHTML += '<div>от 90 до 119: ' + credit_history['field155'][0] + 	credit_history['field155'][1] + '</div>';
					te.innerHTML += '<div>больше 120: ' + 	credit_history['field156'][0] + 	credit_history['field156'][1] + '</div>';
					te.innerHTML += '<div>Постоянные несвоевременные платежи: ' 		  + 	cr_reit_12m['7'] + 	'</div>';
					te.innerHTML += '<div>Изъятие: ' 									  + 	cr_reit_12m['8'] + 	'</div>';
					te.innerHTML += '<div>Безнадежный долг/переданна взыскание: ' 		  + 	cr_reit_12m['9'] + 	'</div>';
					te.innerHTML += '<div>Наихудший платежный статус: ' + 	credit_history['field151'][0] + 	credit_history['field151'][1] + '</div>';
					te.innerHTML += '<br>';
					
					te.innerHTML += '<div>Была реструктуризация: ' + Item['data']['restr'] + 									credit_history['field157'][1] + '</div>';
					te.innerHTML += '<div>Количество обращений за последние 7 дней: ' + Item['data']['last_7'] + 				credit_history['field158'][1] + '</div>';
					te.innerHTML += '<div>Количество обращений за последние 14 дней: ' + Item['data']['last_14'] + 				credit_history['field159'][1] + '</div>';
					te.innerHTML += '<div>Количество обращений за последний месяц: ' + Item['data']['last_mon'] + 				credit_history['field160'][1] + '</div><br>';
					
					te.innerHTML += '<button style="float:right;" onclick="javascript:import_nbki(this)">Импорт данных</button>';
					
					
					
					
					
					//te.innerHTML += 'Текущий: ' + Item['data']['bal_now'] + '<br>';
					//te.innerHTML += Item['data'];
					//tmp_str += Item['data'];
				}
				else 
				{
					alert(Item['msg']);
				}
			},
			function(req)
			{
				alert("Ошибка исполнения. Свяжитесь с администратором!");
			});
			
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'algo':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			var algo_pos = target.getAttribute('tooltip-id');
			var text_ = '';
			
			var data = "&block=anketa&action=get_algo&pos=" + algo_pos;
			ajaxQuery('/ajax.php','POST', data, true, function(req) 
			{
				var tmp_page = req.responseText;
				var Item = eval("obj = " + tmp_page);
				if (Item['status'] == 'ok')
				{
					text_ = Item['data'];
					tmp_str += text_;
				}
				else 
				{
					text_= Item['msg'];
					tmp_str += text_;
				}
				tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
				tooltipElem.style.width = '90%';
				tooltipElem.style.left = '5%';
				tooltipElem.style.top = '10%';
				tooltipElem.style.height = '80%';
				tooltipElem.style.overflow = 'auto';
				tooltipElem.id = 'algo_wrap';
				//console.log(tmp_str);
				tooltipElem.innerHTML = tmp_str;
				document.body.appendChild(tooltipElem);
				showingTooltip = tooltipElem;
			},
			function(req)
			{
				text_= "Ошибка исполнения. Свяжитесь с администратором!";
				tmp_str += text_;
				tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
				tooltipElem.style.width = '1200px';
				tooltipElem.id = 'algo_wrap';
				console.log(tmp_str);
				tooltipElem.innerHTML = tmp_str;
				document.body.appendChild(tooltipElem);

				var coords = target.getBoundingClientRect();

				var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
				if (left < 0) left = 0; // не вылезать за левую границу окна

				//var top = coords.top - tooltipElem.offsetHeight - 5;
				var top = coords.top + target.offsetHeight + 5;
				if (top < 0) { // не вылезать за верхнюю границу окна
				top = coords.top + target.offsetHeight + 5;
				}

				if (tooltip == 'get_promoney_res')
				{
					var availWidth = window.screen.availWidth;
					var availHeight = window.screen.availHeight;
					//alert(tooltipElem.offsetWidth);
					
					tooltipElem.style.top = (availHeight/2 - 325) + 'px';
					tooltipElem.style.left = (availWidth/2 - 275) + 'px';
				}
				else
				{
					tooltipElem.style.left = left + 'px';
					tooltipElem.style.top = top + 'px';
				}

				showingTooltip = tooltipElem;
			});
			
			
			
			return;
			break;
		}
		default:
			return;
	}
	document.body.appendChild(tooltipElem);

	var coords = target.getBoundingClientRect();

	var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
	if (left < 0) left = 0; // не вылезать за левую границу окна

	//var top = coords.top - tooltipElem.offsetHeight - 5;
	var top = coords.top + target.offsetHeight + 5;
	if (top < 0) { // не вылезать за верхнюю границу окна
	top = coords.top + target.offsetHeight + 5;
	}

	if (tooltip == 'get_promoney_res')
	{
		var availWidth = window.screen.availWidth;
		var availHeight = window.screen.availHeight;
		//alert(tooltipElem.offsetWidth);
		
		tooltipElem.style.top = (availHeight/2 - 325) + 'px';
		tooltipElem.style.left = (availWidth/2 - 275) + 'px';
	}
	else
	{
		tooltipElem.style.left = left + 'px';
		tooltipElem.style.top = top + 'px';
	}

	showingTooltip = tooltipElem;
};

function save_note(el, name)
{
	console.log('save_note');
	var new_val = el.parentNode.querySelector('textarea').value;
	console.log('new_val');
	document.querySelector('#' + name).setAttribute('data-tooltip', new_val);
	document.querySelector('#' + name).src = "/img/editred.png";
	document.querySelector('#' + name).parentNode.setAttribute("onclick", "javascript:edit_note(this, '" + name + "', true);");
	
	new_val = encodeURIComponent(new_val);
	
	var data = "&block=anketa&action=save_note&name=" + name + "&val=" + new_val + "&id=" + anketa_id;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			show('none');
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
	
}

function edit_note(el, name, edit)
{
	console.log('edit_note');
	var old_val = '';
	if (edit)
	{
		old_val = el.querySelector('img').getAttribute('data-tooltip');
	}
	
	document.getElementById('wrap').style.display = 'block';
	
	var coords = el.getBoundingClientRect();
	var left = coords.left + 50;	
	var top = coords.top;
	
	var div = document.createElement('div');
		div.setAttribute('class', "note_wind");
		div.style = "background-color:lightgreen;";		
		div.style.position = "absolute";
		div.style.width = "40%";
		//div.style.width = "300px";
		div.style.height = "220px";
		div.style.left = left + 'px';
		div.style.top = top + 'px';
	
		var tearea = document.createElement('textarea');
			tearea.style = "margin-top:5px; margin-left:1%;";
			//tearea.style.width = "280px";
			tearea.style.width = "97%";
			tearea.style.height = "170px"
			tearea.value = old_val;
		div.appendChild(tearea);
	
		var sb = document.createElement('button');	
			sb.innerHTML = 'Сохранить';
			sb.style = "margin:5px 8px;";
			sb.style.float = "right";
			sb.setAttribute("onclick", "javascript:save_note(this, '" + name + "');");
			//sb.onclick = "javascript:save_note(this, '" + name + "');";
		div.appendChild(sb);
	
	document.body.appendChild(div);
}

function import_nbki(el)
{
	console.log(credit_history);
	var cred_his_data = document.querySelector('#cred_his_data');
	var inps = cred_his_data.querySelectorAll('.fval>input');
	
	for (key in inps) 
	{
		if (!inps.hasOwnProperty(key))
			continue;
		var inp = inps[key];
		//console.log(inp);
		if (inp.type == 'checkbox')
		{
			if (credit_history[inp.name] != null)
			{
				
				if (credit_history[inp.name][0])
					inp.checked = true;
				else
					inp.checked = false;
			}
		}
		else if (inp.type == 'text')
		{
			if (credit_history[inp.name] != null)
			{
				inp.style.display = '';
				var sp = document.querySelector('#span_' + inp.name);
				if (sp)
					sp.style.display = 'none';
				
				if (parseInt(inp.value) != parseInt(credit_history[inp.name][0]))
					blur_field(inp);
				inp.value = credit_history[inp.name][0];
			}
		}
	}
	
	
	var inp = document.querySelector('input[name="field94"]');
	
	if (credit_history[inp.name] != null)
	{
		
		inp.style.display = '';
		var sp = document.querySelector('#span_' + inp.name);
		if (sp)
			sp.style.display = 'none';
		
		if (parseInt(inp.value) != parseInt(credit_history[inp.name][0]))
			blur_field(inp);
		
		inp.value = credit_history[inp.name][0];
	}
	
	
	var inps = document.querySelectorAll('input[name="field96"]');
	var inp_tmp = (parseInt(credit_history['field96'][0]) > 0) ? 'да' : 'нет';
	
	console.log(inps);
	console.log(inp_tmp);
	//var inp_tmp = (inp_tmp.toLowerCase() == 'да') ? 1 : 0;
	for (var i = 0; i < inps.length; i++)
	{
		var ttttmp = inps[i].value;
		if (ttttmp.toLowerCase() == inp_tmp)
			inps[i].checked = true;
	}
	
	//console.log("selects");
	var sel = cred_his_data.querySelectorAll('.fval>select');
	//console.log(sel);
	//for (key in sel) 
	for (var key = 0; key < sel.length; key++)
	{
		//if (!inps.hasOwnProperty(key))
			//continue;
		
		//if(typeof key != 'number')
			//continue;
			 
		var inp = sel[key];
		//console.log(inp);
		var options = inp.options;
		var inp_tmp = credit_history[inp.name][0];
		
		if (credit_history[inp.name] == null)
			continue;
		
		//if (credit_history[inp.name][0] == '')
			//continue;
		
		var sp = document.querySelector('#span_' + inp.name);
		if (sp)
			sp.style.display = 'none';
				
		inp.style.display = '';
		
		for (var i = 0; i < options.length; i++)
		{
			//console.log('************');
			//console.log(sel[key].selectedIndex);
			//console.log(ttttmp.toLowerCase());
			//console.log(inp_tmp.toLowerCase());
			var ttttmp = options[i].value
			if (ttttmp.toLowerCase() == inp_tmp.toLowerCase())
			{
				//options[i].selected = true;
				sel[key].selectedIndex = i;
			}
			else if ((inp_tmp.toLowerCase() == '') && (ttttmp == '-1'))
				sel[key].selectedIndex = i;
			//else
				//options[i].selected = false;
		}
		
		/*if ((credit_history[inp.name] != null) && (inp.value != ''))
		{
			if (inp.value != credit_history[inp.name][0])
				credit_history[inp.name][1] = ' <font color="red">Не совпадают! (' + inp.value + ')</font>';
		}*/
	}

	close_tooltip(el);
	window.location.hash = '';
	document.querySelector("#save_data_button").style.display = 'block';
	document.querySelector("#save_data_button_top").style.display = '';
}

function prod_vis(id, value)
{
	//console.log(hide_products + " " + id + ' ' + value);
	var div = document.querySelector("#prod" + id).parentNode.parentNode;
	if (!value)
		div.style.display = "table-row";
	if (hide_products && value)
		div.style.display = "none";
	
	if (document.querySelector('#details_' + id))
	{
		var node = document.getElementById("details_" + id);
		if (node.parentNode) {
		  node.parentNode.removeChild(node);
		}
		return;
	}
}

function change_view(b)
{
	//console.log(b.value + " " + b.innerHTML);
	//var dd = document.querySelectorAll('#result2 > div');
	var prod_result = document.querySelector('#prod_result');
	var dd = prod_result.querySelectorAll('div[class*="prb_row"]');
	//console.log(dd.length);
	if (b.value == 'hide')
	{
		b.innerHTML = 'Показать все';
		b.value = 'show';
		hide_products = true;
		//console.log('dd = ' + dd.length);
		for (var i = 0; i<dd.length; i++)
		{
			//console.log('dd[' + i + '] = ' + dd[i].innerHTML);
			//if (dd[i].querySelector('.tab7 > input').checked)
			if (dd[i].querySelector('.res6 > input').checked)
				dd[i].style.display = "none";
			
			var c2_id = dd[i].querySelector('.res1 > i').id;
			console.log(c2_id);
			if (c2_id)
				c2_id = c2_id.replace('prod', '');
			if (c2_id)
			{
				if (document.querySelector('#details_' + c2_id))
				{
					var node = document.getElementById("details_" + c2_id);
					if (node.parentNode) {
					  node.parentNode.removeChild(node);
					}
				}
			}
		}
	}
	else
	{
		b.innerHTML = 'Исключить выбранные';
		b.value = 'hide';
		hide_products = false;
		for (var i = 0; i<dd.length; i++)
			dd[i].style.display = "table-row";
	}
	//console.log("after " + b.value + " " + b.innerHTML);
}

document.onmouseover = function(e)
{
	var target = e.target;

	var tooltip = target.getAttribute('data-tooltip');
	if (!tooltip) return;
	
	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	

	var tmp_str = '';
	switch(tooltip)
	{
		case 'double_phone':
		case 'double_fio':
		case 'double_ip':
		case 'get_promoney_res':
		case 'fssp_res':
		case 'algo':
			return;
		case 'algo':
		{
			var algo_pos = target.getAttribute('tooltip-id');
			var text_ = '';
			
			var data = "&block=anketa&action=get_algo&pos=" + algo_pos;
			ajaxQuery('/ajax.php','POST', data, true, function(req) 
			{
				var tmp_page = req.responseText;
				var Item = eval("obj = " + tmp_page);
				if (Item['status'] == 'ok')
				{
					text_ = Item['data'];
					tooltipElem.innerHTML = text_;
				}
				else 
				{
					text_= Item['msg'];
					tooltipElem.innerHTML = text_;
				}
			},
			function(req)
			{
				text_= "Ошибка исполнения. Свяжитесь с администратором!";
				tooltipElem.innerHTML = text_;
			});
			
			
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		case 'algo_1':
		{
			/*tmp_str += 'Блок "Знакомство с клиентом"';
			tmp_str += '<ul>';
			tmp_str += '<li>Звоним клиенту "Знакомимся", выясняем у него имя и отчество, задавая вопрос: "Как я могу к вам обращаться? "  Если клиент просит сразу сделать ему предложение, а потом знакомство то говорим ему, что у меня определенный алгоритм работы и мне не нужна ваша фамилия и данные документов, а для предложения мне нужна эта минимальная информация. Начинаем заполнять первый блок и после этого при необходимости делаем ПРБ и предложение.</li>';
			tmp_str += '<li>Выясняем регион обращения (находим в списке существующий код ФИАС, в случае его отсутствия спрашиваем у клиента ближайший большой населенный пункт)</li>';
			tmp_str += '<li>Выясняем необходимую сумму кредита и для себя понимаем цель на что берутся денежные средства (от цели кредита зависит по какому пути идти дальше )</li>';
			tmp_str += '<li>Выясняем общую сумму дохода клиента (официальный и неофициальный доход и пишем общую совокупность дохода)</li>';
			tmp_str += '<li>Гражданство</li>';
			tmp_str += '<li>Наличие текущий просроченных платежей со слов клиента.</li>';
			tmp_str += '<li>Наличие заинтересованных лиц в получении кредита (в качестве заемщика, поручителя, залогодателя и тд) да/нет</li>';
			tmp_str += '</ul><br>';
			tmp_str += 'При наличии текущей просрочки и отсутствия заинтересованных лиц, клиенту высылается предложение по залоговому кредиту от инвестора. Шаблон "Предложение"';
			tmp_str += '<br><br><font color="red">На этом этапе заканчиваем работу с клиентом, сохраняем запись в анкете и если запрос клиента не решаем с учетом возможностей компании, отправляем заявку в статус "Заявка завершена"</font>';
			tmp_str += '<br>';
			tmp_str += '<br>Работа с недозвоном.';
			tmp_str += '<ul>';
			tmp_str += '<li>При первом недозвоне отправляем клиенту СМС "Одобрение" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторном недозвоне отправляем клиенту СМС, шаблон: "Истекает срок предварительного решения" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторных недозвонах отправляем анкету на удаление.</li>';
			tmp_str += '</ul>';
			*/
			tmp_str += 'Блок <b>"Знакомство с клиентом"</b>';
			tmp_str += '<ul>';
			tmp_str += '<li>Звоним клиенту, "знакомимся", выясняем у него имя и отчество, задавая вопрос: "Как я могу к Вам обращаться? ".  Если клиент просит сразу сделать ему предложение, а потом совершить «знакомство»,  то говорим ему, что у меня определенный алгоритм работы, и мне не нужна Ваша фамилия и данные документов, а для предложения мне нужна ЛИШЬ эта минимальная информация. Начинаем заполнять первый блок и после этого при необходимости делаем ПРБ и предложение.</li>';
			tmp_str += '<li>Выясняем регион обращения (находим в списке существующий код ФИАС, а в случае его отсутствия спрашиваем у клиента ближайший значимый населенный пункт).</li>';
			tmp_str += '<li>Выясняем необходимую сумму кредита и для себя понимаем цель, на что берутся денежные средства (от цели кредита зависит по какому пути идти дальше).</li>';
			tmp_str += '<li>Выясняем общую сумму дохода клиента (официальный и неофициальный доход и пишем общий совокупный доход).</li>';  
			tmp_str += '<li>Гражданство.</li>';
			tmp_str += '<li>Наличие текущих просроченных платежей со слов клиента.</li>';
			tmp_str += '<li>Наличие заинтересованных лиц в получении кредита (в качестве заемщика, поручителя, залогодателя и т.д.). Да/нет?</li>';
			tmp_str += '</ul><br>';
			tmp_str += 'При наличии текущей просрочки и отсутствия заинтересованных лиц, клиенту высылается предложение по залоговому кредиту от инвестора. Шаблон "Предложение".<br>';
			tmp_str += '<br><font color="red">На этом этапе заканчиваем работу с клиентом, сохраняем запись в анкете, и если запрос клиента не решаем с учетом возможностей компании, отправляем заявку в статус "Заявка завершена".</font><br>';
			tmp_str += '<br><b>Работа с недозвоном.</b><br>';
			tmp_str += '<ul>';
			tmp_str += '<li>При первом недозвоне отправляем клиенту СМС "Одобрение" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторном недозвоне отправляем клиенту СМС шаблон: "Истекает срок предварительного решения" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторных недозвонах отправляем анкету на удаление.</li>';
			tmp_str += '</ul><br>';
			
			
			
			tooltipElem.innerHTML = tmp_str;
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		/*case 'double_phone':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			for (var key in double_phone_m)
			{
				tmp_str += "Анкета номер " + key + ": " + double_phone_m[key] + "<br>";
			}
			tooltipElem.id = 'double_phone';
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'double_fio':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			for (var key in double_fio_m)
			{
				tmp_str += "Анкета номер " + key + ": " + double_fio_m[key] + "<br>";
			}
			tooltipElem.id = 'double_fio';
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'double_ip':
		{
			tmp_str += '<img class="close_tooltip" onclick="close_tooltip(this)" src="/img/close.png">';
			for (var key in double_ip_m)
			{
				tmp_str += "Анкета номер " + key + ": " + double_ip_m[key] + "<br>";
			}
			tooltipElem.id = 'double_fio';
			tooltipElem.innerHTML = tmp_str;
			break;
		}*/
		case 'filkos':
		{
			tmp_str += '/*********************************1 шаг*********************************/<br>';
			
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				tmp_str += 'Фамилия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Фамилия: ' + lastname + '<br>';
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				tmp_str += 'Имя: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Имя: ' + firstname + '<br>';
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				tmp_str += 'Отчество: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Отчество: ' + middlename + '<br>';
			
			var field29 = document.querySelector("input[name=field29]").value;
			if (field29 == '')
				tmp_str += 'Телефон: <font color="red">Не заполнено!</font><br>';
			else
			{
				var tel = field29.replace(/[^0-9]/gim,'');
				var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
				var mass = reg.exec(tel);
				if (mass)
				{
					tel = mass[1] + "(" + mass[2] + ") " + mass[3] + "-" + mass[4];
					tmp_str += 'Телефон: ' + tel + '<br>';
				}
				else
					tmp_str += 'Телефон: <font color="red">Телефон заполнен некорректно!</font><br>';
			}

			var sum = document.querySelector("input[name=sum]").value;
			if (sum == '')
				tmp_str += 'Сумма: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Сумма: ' + sum + '<br>';

			var email = document.querySelector("input[name=email]").value;
			
			var reg = new RegExp(".*@.*\\..*", "ig");
			
			if (email == '')
				tmp_str += 'E-mail: <font color="red">Не заполнено!</font><br>';
			else if (!reg.test(email))
				tmp_str += 'E-mail: ' + email + ' <font color="red"><i>Заполнен неверно!</i></font><br>';
			else
				tmp_str += 'E-mail: ' + email + '<br>';

			var fias_code = document.querySelector("#field1").innerHTML;
			if (fias_code == '')
				tmp_str += 'Код ФИАС: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Код ФИАС: ' + fias_code + '<br>';
			
			var birth_day = document.querySelector("input[name=birth_day]").value;
			reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
			
			if (reg.test(birth_day))
			{
				mass = reg.exec(birth_day);
				console.log(reg);
				console.log(birth_day);
				console.log(mass);
				tmp_str += 'День рождения: ' + mass[1] + '<br>';
				tmp_str += 'Месяц рождения: ' + mass[2] + '<br>';
				tmp_str += 'Год рождения: ' + mass[3] + '<br>';
			}
			else
			{
				tmp_str += 'День рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Месяц рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Год рождения: <font color="red">Не заполнено!</font><br>';
			}

			tmp_str += '/*********************************2 шаг*********************************/<br>';
			
			var field75 = document.querySelector("input[name=field75]").value;
			if (field75 == '')
				tmp_str += 'Доход: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Доход: ' + field75 + '<br>';
			
			var field47 = document.querySelector("select[name=field47]");
			field47 = field47.options[field47.selectedIndex].value;
			
			if (field47 == '')
				tmp_str += 'Трудоустройство: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Трудоустройство: ' + field47 + '<br>';

			
			var field77 = document.querySelectorAll("input[name*=field77]");
			var missl = true;
			var f77 = "";
			console.log(field77);
			for (var i = 0; i < field77.length; i++)
			{
				if (field77[i].checked)
				{
					console.log(field77[i].checked)
					missl = false;
					f77 = field77[i].value;
					break;
				}
			}
			
			if (missl)
				tmp_str += 'Подтверждение дохода: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Подтверждение дохода: ' + f77 + '<br>';

			
			var field59 = document.querySelector("input[name=field59]").value;
			reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
			if (field59 == '')
				tmp_str += 'Стаж: <font color="red">Не заполнено!</font><br>';
			else if (!reg.test(field59))
				tmp_str += 'Стаж: <font color="red">Заполнено неверно!</font><br>';
			else
			{
				mass = reg.exec(field59);
				var oToday = new Date();			
				var st_date = new Date(mass[3], parseInt(mass[2]) - 1, mass[1], oToday.getHours(), oToday.getMinutes(), oToday.getSeconds());
				var year1=st_date.getFullYear();
				var year2=oToday.getFullYear();
				var month1=st_date.getMonth();
				var month2=oToday.getMonth();
				
				var numberOfMonths = (year2 - year1) * 12 + (month2 - month1);
				console.log(numberOfMonths);
				
				tmp_str += 'Стаж: ' + numberOfMonths + '<br>';
			}

			
			var field87 = document.querySelector("select[name=field87]");
			field87 = field87.options[field87.selectedIndex].value;
			if (field87 == '')
				tmp_str += 'Кредитная история: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Кредитная история: ' + field87 + '<br>';

			
			var field119 = document.querySelector("select[name=field119]");
			field119 = field119.options[field119.selectedIndex].value;
			if (field119 == '')
				tmp_str += 'Образование: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Образование: ' + field119 + '<br>';
			
			/*var field8 = document.querySelector("select[name=field8]");
			field8 = field8.options[field8.selectedIndex].value;
			if (field8 == '')
				tmp_str += 'Собственность: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Собственность: ' + field8 + '<br>';
			*/
			
			var field8 = document.querySelectorAll("#inp_sp_field8 input[name*=field8]");
			var miss2 = true;
			console.log('активы');
			console.log(field8);
			var f8 = "Нет";
			for (var i = 0; i < field8.length; i++)
			{
				if (field8[i].checked)
				{
					miss2 = false;
					if (field8[i].value == 'Нет')
						continue;
					f8 = field8[i].value;
					//break;
				}
			}
			
			if (miss2)
				tmp_str += 'Собственность: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Собственность: ' + f8 + '<br>';
		
			var field40 = document.querySelector("select[name=field40]");
			field40 = field40.options[field40.selectedIndex].value;
			if (field40 == '')
				tmp_str += 'Загранпаспорт: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Загранпаспорт: ' + field40 + '<br>';
			
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		
		case 'verifyPhone':
		{
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				tmp_str += 'Фамилия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Фамилия: ' + lastname + '<br>';
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				tmp_str += 'Имя: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Имя: ' + firstname + '<br>';
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				tmp_str += 'Отчество: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Отчество: ' + middlename + '<br>';
			
			var birth_day = document.querySelector("input[name=birth_day]").value;
			reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
			
			if (reg.test(birth_day))
			{
				mass = reg.exec(birth_day);
				console.log(reg);
				console.log(birth_day);
				console.log(mass);
				tmp_str += 'День рождения: ' + mass[1] + '<br>';
				tmp_str += 'Месяц рождения: ' + mass[2] + '<br>';
				tmp_str += 'Год рождения: ' + mass[3] + '<br>';
			}
			else
			{
				tmp_str += 'День рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Месяц рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Год рождения: <font color="red">Не заполнено!</font><br>';
			}
			
			var field29 = document.querySelector("input[name=field29]").value;
			if (field29 == '')
				tmp_str += 'Телефон: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Телефон: ' + field29 + '<br>';
			
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'personLoanRating':
		case 'finScoring':
		case 'finScoringOkbQiwi':
		{
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				tmp_str += 'Фамилия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Фамилия: ' + lastname + '<br>';
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				tmp_str += 'Имя: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Имя: ' + firstname + '<br>';
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				tmp_str += 'Отчество: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Отчество: ' + middlename + '<br>';
			
			var birth_day = document.querySelector("input[name=birth_day]").value;
			reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
			
			if (reg.test(birth_day))
			{
				mass = reg.exec(birth_day);
				console.log(reg);
				console.log(birth_day);
				console.log(mass);
				tmp_str += 'День рождения: ' + mass[1] + '<br>';
				tmp_str += 'Месяц рождения: ' + mass[2] + '<br>';
				tmp_str += 'Год рождения: ' + mass[3] + '<br>';
			}
			else
			{
				tmp_str += 'День рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Месяц рождения: <font color="red">Не заполнено!</font><br>';
				tmp_str += 'Год рождения: <font color="red">Не заполнено!</font><br>';
			}
			
			var passport_series = document.querySelector("input[name=field18]").value;
			if (passport_series == '')
				tmp_str += 'Паспорт серия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Паспорт серия: ' + passport_series + '<br>';
			
			var passport_number = document.querySelector("input[name=field19]").value;
			if (passport_number == '')
				tmp_str += 'Паспорт номер: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Паспорт номер: ' + passport_number + '<br>';
			
			var passport_issue = document.querySelector("input[name=field21]").value;
			if (passport_issue == '')
				tmp_str += 'Дата выдачи паспорта: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Дата выдачи паспорта: ' + passport_issue + '<br>';
			
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'personDebt': {
			var inn = document.querySelector("input[name=field168]").value;
			if (inn == '')
				tmp_str += 'ИНН: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'ИНН: ' + inn + '<br>';

			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'searchphysical':
		{
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				tmp_str += 'Фамилия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Фамилия: ' + lastname + '<br>';
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				tmp_str += 'Имя: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Имя: ' + firstname + '<br>';
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				tmp_str += 'Отчество: <font color="red">Не заполнено!</font><br>';
			else if (middlename.toLowerCase() == 'нет')
				tmp_str += 'Отчество: - <br>';
			else
				tmp_str += 'Отчество: ' + middlename + '<br>';
			
			var birth_date = document.querySelector("input[name=birth_day]").value;
			if (birth_date == '')
				tmp_str += 'Дата рождения: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Дата рождения: ' + birth_date + '<br>';
			
			/*var field1 = document.querySelector("input[name=field1]").value;
			var tmpf1 = field1.split(' регион: ');
			
			if (tmpf1.length != 2)
				tmp_str += 'Регион: <font color="red">Не заполнено!</font><br>';
			else
			{
				field1 = tmpf1[1];
				if (field1 == '')
					tmp_str += 'Регион: <font color="red">Не заполнено!</font><br>';
				else
					tmp_str += 'Регион: ' + field1 + '<br>';
			}*/
			
			
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		case 'CWIOkbReportPrivate':
		{
			var sum = document.querySelector("input[name=sum]").value;
			if (sum == '')
				tmp_str += 'Сумма: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Сумма: ' + sum + '<br>';
			
			var income = document.querySelector("input[name=field75]").value;
			if (income == '')
				tmp_str += 'Доход: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Доход: ' + income + '<br>';
			
			/*var term_loan = document.querySelector("input[name=field121]").value;
			if (term_loan == '')
				tmp_str += 'Срок кредита: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Срок кредита: ' + term_loan + ' MONTH<br>';*/
				
		}
		case 'CWICreditHistoryPrivate':
		{
			var birthplace = document.querySelector("input[name=field23]").value;
			if (birthplace == '')
				tmp_str += 'Место рождения: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Место рождения: ' + birthplace + '<br>';
			
			var passport_place = document.querySelector("input[name=field20]").value;
			if (passport_place == '')
				tmp_str += 'Место выдачи паспорта: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Место выдачи паспорта: ' + passport_place + '<br>';
			
			var passport_code = document.querySelector("input[name=field22]").value;
			if (passport_code == '')
				tmp_str += 'Код подразделения паспорта: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Код подразделения паспорта: ' + passport_code + '<br>';
			
			var ar = document.querySelectorAll("input[name*=field24]");
			var address_registered = '';
			if (ar)
			{
				for (var l=0; l < ar.length; l++)
				{
					if (ar[l].value != '')
					{
						if (address_registered != '')
							address_registered += ', ';
						address_registered += ar[l].value;
					}	
				}
			}
			if (address_registered == '')
				tmp_str += 'Адрес регистрации: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Адрес регистрации: ' + address_registered + '<br>';
			if(tooltip == 'CWICreditHistoryPrivate')
			{
				
				var ar = document.querySelectorAll("input[name*=field123]");
				var address_residence = '';
				if (ar)
				{
					console.log('address_residence');
					console.log(ar);
					for (var l=0; l < ar.length; l++)
					{
						if (ar[l].value != '')
						{
							if (address_residence != '')
								address_residence += ', ';
							address_residence += ar[l].value;
						}
					}
				}

				if (address_residence == '')
					tmp_str += 'Адрес проживания: <font color="red">Не заполнено!</font><br>';
				else
					tmp_str += 'Адрес проживания: ' + address_residence + '<br>';
			}
			
			var loaded_file = document.querySelectorAll("div.loaded_file")
			var consent = false;
			var passport_photo = false;
			var passport_registration = false;
			for(i=0; i<loaded_file.length; i++)
			{
				var sel = loaded_file[i].querySelector('select');
				var type = sel.options[sel.selectedIndex].value;
				var file_a = loaded_file[i].querySelector('a');
				if (file_a == null)
					continue;
				var file = file_a.innerHTML;
				if (type == 'agree_scan')
				{
					consent = true;
					tmp_str += 'Согласие на обработку и хранение данных: ' + file + '<br>';
				}
				if (type == 'passport_scan')
				{
					passport_photo = true;
					tmp_str += 'Паспорт (разворот с фотографией): ' + file + '<br>';
				}
				if (type == 'passport_reg')
				{
					passport_registration = true;
					tmp_str += 'Паспорт (разворот с пропиской): ' + file + '<br>';
				}
			}
			
			if (!consent)
				tmp_str += 'Согласие на обработку и хранение данных: <font color="red">Не заполнено!</font><br>';
			if (!passport_photo)
				tmp_str += 'Паспорт (разворот с фотографией): <font color="red">Не заполнено!</font><br>';
			if (!passport_registration)
				tmp_str += 'Паспорт (разворот с пропиской): <font color="red">Не заполнено!</font><br>';
		}		
		case 'CWICreditHistoryIndividual':
		{
			var passport_issue = document.querySelector("input[name=field21]").value;
			if (passport_issue == '')
				tmp_str += 'Дата выдачи паспорта: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Дата выдачи паспорта: ' + passport_issue + '<br>';
		}
		case 'CWICreditRatingIndividual':
		{
			var birth_date = document.querySelector("input[name=birth_day]").value;
			if (birth_date == '')
				tmp_str += 'Дата рождения: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Дата рождения: ' + birth_date + '<br>';
			
			var lastname = document.querySelector("input[name=lastname]").value;
			if (lastname == '')
				tmp_str += 'Фамилия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Фамилия: ' + lastname + '<br>';
			
			var firstname = document.querySelector("input[name=firstname]").value;
			if (firstname == '')
				tmp_str += 'Имя: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Имя: ' + firstname + '<br>';
			
			var middlename = document.querySelector("input[name=middlename]").value;
			if (middlename == '')
				tmp_str += 'Отчество: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Отчество: ' + middlename + '<br>';
			
			var passport_series = document.querySelector("input[name=field18]").value;
			if (passport_series == '')
				tmp_str += 'Паспорт серия: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Паспорт серия: ' + passport_series + '<br>';
			
			var passport_number = document.querySelector("input[name=field19]").value;
			if (passport_number == '')
				tmp_str += 'Паспорт номер: <font color="red">Не заполнено!</font><br>';
			else
				tmp_str += 'Паспорт номер: ' + passport_number + '<br>';
			
			tooltipElem.innerHTML = tmp_str;
			break;
		}
		default:
			tooltipElem.innerHTML = tooltip;
			break;
	}
	//if (tooltip != 'algo_1')
		document.body.appendChild(tooltipElem);
	//else
		//target.appendChild(tooltipElem);

	var coords = target.getBoundingClientRect();

	var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
	if (left < 0) left = 0; // не вылезать за левую границу окна

	//var top = coords.top - tooltipElem.offsetHeight - 5;
	var top = coords.top + target.offsetHeight + 5;
	if (top < 0) { // не вылезать за верхнюю границу окна
	top = coords.top + target.offsetHeight + 5;
	}

	tooltipElem.style.left = left + 'px';
	tooltipElem.style.top = top + 'px';
	
	
	//console.log(
	//var h1 = document.getElementById('menu_bottom').clientHeight;
	//var h1 = tooltipElem.clientHeight;
	var h2 = tooltipElem.offsetHeight;
	//var h3 = tooltipElem.scrollHeight;
	//clientHeight высота содержимого вместе с полями padding, но без полосы прокрутки.
	//offsetHeight «внешняя» высота блока, включая рамки.
	//scrollHeight полная внутренняя высота, включая прокрученную область.
	//console.log("clientHeight:" + h1);
	
	//console.log("scrollHeight:" + h3);
	
	/*console.log("offsetHeight:" + h2);
	console.log("top:" + top);
	console.log("maxheigh:" + screen.height);
	var bot = screen.height - 100;
	if ((top + h2) > screen.height)
		tooltipElem.style.bottom = bot - 'px';
	else*/
		
		

	showingTooltip = tooltipElem;
};

function show_all_filkos()
{
	var divs = document.querySelectorAll(".block_wrap > .row_a");
	var filkos_mass = new Array();
	filkos_mass.push("lastname", "firstname", "middlename", "field29", "sum", "email", "field1", "birth_day", "field75", "field47", "field77", "field59", "field87", "field119", "field8", "field40");
	for (var n=0; n < divs.length; n++)
	{
		var show = false;
		for (var k in filkos_mass)
		{
			var key = filkos_mass[k];
			var iiin;
			iiin = divs[n].querySelector("input[name=" + key + ']');
			
			if (!iiin)
				iiin = divs[n].querySelector("#inp_sp_" + key);
			
			if (!iiin)
				iiin = divs[n].querySelector("select[name=" + key + ']');
			
			if (iiin)
			{
				show = true;
			}
				
		}

		if (show)
		{
			divs[n].style.display = '';
		}
		else
			divs[n].style.display = 'none';
	}
}

function edit_contract_fields()
{
	
	var contract_fields = new Array();
	contract_fields['lastname'] = true;
	contract_fields['firstname'] = true;
	contract_fields['middlename'] = true;
	contract_fields['birth_day'] = true;
	contract_fields['field27'] = true;
	contract_fields['field29'] = true;
	contract_fields['email'] = true;
	contract_fields['field18'] = true;
	contract_fields['field19'] = true;
	contract_fields['field21'] = true;
	contract_fields['field20'] = true;
	contract_fields['field24'] = true;
	contract_fields['field123'] = true;
	contract_fields['field164'] = true;
	contract_fields['field3'] = true;
	contract_fields['field162'] = true;
	contract_fields['field163'] = true;
	contract_fields['field165'] = true;
	contract_fields['field6'] = true;
	contract_fields['field127'] = true;
	contract_fields['field166'] = true;
	contract_fields['field167'] = true;
	contract_fields['field168'] = true;
	var divs = document.querySelectorAll(".block_wrap > .row_a");
	for (var n=0; n < divs.length; n++)
	{
		var show = false;

		for (var key in contract_fields)
		{
			var iiin;
			iiin = divs[n].querySelector("input[name=" + key + ']');
			
			if (!iiin)
				iiin = divs[n].querySelector("#inp_sp_" + key);
			
			if (!iiin)
				iiin = divs[n].querySelector("select[name=" + key + ']');
			
			if (iiin && contract_fields[key])
			{
				filkos_fail_a = true;
				show = true;
			}
				
		}
		if (show)
		{
			divs[n].style.display = '';
		}
		else
			divs[n].style.display = 'none';
	}
	document.getElementById('window_cp').style.display = 'none';
	document.getElementById('wrap').style.display = 'none';
	document.querySelector('#clear_filter_button').style.display = '';
}

function send_filkos()
{
	var fields = new Array();
	var filkos_fail = new Array();
	var lastname = document.querySelector("input[name=lastname]").value;
	if (lastname == '')
		filkos_fail['lastname'] = true;
	fields['surname'] = lastname;
	
	var firstname = document.querySelector("input[name=firstname]").value;
	if (firstname == '')
		filkos_fail['firstname'] = true;
	fields['name'] = firstname;
	
	var middlename = document.querySelector("input[name=middlename]").value;
	if (middlename == '')
		filkos_fail['middlename'] = true;
	fields['middlename'] = middlename;
	
	var field29 = document.querySelector("input[name=field29]").value;
	if (field29 == '')
		filkos_fail['field29'] = true;
	else
	{
		var tel = field29.replace(/[^0-9]/gim,'');
		var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
		var mass = reg.exec(tel);
		
		var mass = reg.exec(tel);
		if (mass)		
			field29 = mass[1] + "(" + mass[2] + ") " + mass[3] + "-" + mass[4];
		else
		{
			filkos_fail['field29'] = true;
			field29 = '';
		}
	}
	fields['telefon'] = field29;

	var sum = document.querySelector("input[name=sum]").value;
	if (sum == '')
		filkos_fail['sum'] = true;
	fields['summa'] = sum;

	var email = document.querySelector("input[name=email]").value;	
	var reg = new RegExp(".*@.*\\..*", "ig");	
	if (email == '')
		filkos_fail['email'] = true;
	else if (!reg.test(email))
		filkos_fail['email'] = true;
	fields['email'] = email;
	
	var fias_code = document.querySelector("#field1").innerHTML;
	if (fias_code == '')
		filkos_fail['field1'] = true;
	fields['fias_code'] = fias_code;
	
	var birth_day = document.querySelector("input[name=birth_day]").value;
	reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");	
	var birth_month;
	var birth_year;	
	if (reg.test(birth_day))
	{
		mass = reg.exec(birth_day);
		birth_day = mass[1];
		birth_month = mass[2];
		birth_year = mass[3];
	}
	else
		filkos_fail['birth_day'] = true;
	fields['birth_day'] = birth_day;
	fields['birth_month'] = birth_month;
	fields['birth_year'] = birth_year;
	
	var field75 = document.querySelector("input[name=field75]").value;
	if (field75 == '')
		filkos_fail['field75'] = true;
	fields['dohod'] = field75;
	
	
	var field47 = document.querySelector("select[name=field47]");
	field47 = field47.options[field47.selectedIndex].value;	
	if (field47 == '')
		filkos_fail['field47'] = true;
	fields['trud'] = field47;
	
	var field77 = document.querySelectorAll("input[name*=field77]");
	var missl = true;
	var f77 = "";
	for (var i = 0; i < field77.length; i++)
	{
		if (field77[i].checked)
		{
			missl = false;
			f77 = field77[i].value;
			break;
		}
	}
		
	if (missl)
		filkos_fail['field77'] = true;

	fields['podtverjdenie'] = f77;
	
	var field59 = document.querySelector("input[name=field59]").value;
	reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
	if (field59 == '')
		filkos_fail['field59'] = true;
	else if (!reg.test(field59))
		filkos_fail['field59'] = true;
	else
	{
		mass = reg.exec(field59);
		var oToday = new Date();			
		var st_date = new Date(mass[3], parseInt(mass[2]) - 1, mass[1], oToday.getHours(), oToday.getMinutes(), oToday.getSeconds());
		var year1=st_date.getFullYear();
		var year2=oToday.getFullYear();
		var month1=st_date.getMonth();
		var month2=oToday.getMonth();
		
		field59 = (year2 - year1) * 12 + (month2 - month1);
	}
	fields['stazh'] = field59;

	
	var field87 = document.querySelector("select[name=field87]");
	field87 = field87.options[field87.selectedIndex].value;
	if (field87 == '')
		filkos_fail['field87'] = true;
	fields['credit_history'] = field87;
	
	var field119 = document.querySelector("select[name=field119]");
	field119 = field119.options[field119.selectedIndex].value;
	if (field119 == '')
		filkos_fail['field119'] = true;
	fields['education'] = field119;
	
	/*var field8 = document.querySelector("select[name=field8]");
	field8 = field8.options[field8.selectedIndex].value;
	if (field8 == '')
		filkos_fail['tttttttttt'] = true;
	fields['property'] = field8;
	*/
	
	
	var field8 = document.querySelectorAll("#inp_sp_field8 input[name*=field8]");
	var miss2 = true;
	var f8 = "Нет";
	for (var i = 0; i < field8.length; i++)
	{
		if (field8[i].checked)
		{
			
			miss2 = false;
			if (field8[i].value == 'Нет')
				continue;
			f8 = field8[i].value;
			//break;
		}
	}
	
	if (miss2)
		filkos_fail['field8'] = true;

	fields['property'] = f8;

	var field40 = document.querySelector("select[name=field40]");
	field40 = field40.options[field40.selectedIndex].value;
	if (field40 == '')
		filkos_fail['field40'] = true;
	fields['zagranpass'] = field40;
	
	
	filkos_fail_a = false;
	var divs = document.querySelectorAll(".block_wrap > .row_a");
	for (var n=0; n < divs.length; n++)
	{
		var show = false;
		console.log(divs[n]);
		for (var key in filkos_fail)
		{
			var iiin;
			iiin = divs[n].querySelector("input[name=" + key + ']');
			
			if (!iiin)
				iiin = divs[n].querySelector("#inp_sp_" + key);
			
			if (!iiin)
				iiin = divs[n].querySelector("select[name=" + key + ']');
			
			console.log(iiin);
	//		if (filkos_fail[key])
			if (iiin && filkos_fail[key])
			{
				filkos_fail_a = true;
				show = true;
			}
				
		}
		
		console.log(show);
		if (show)
		{
			divs[n].style.display = '';
		}
		else
			divs[n].style.display = 'none';
	}
	
	
	if (filkos_fail_a)
	{
		alert("Есть ошибки!");
		location.href = "#anketa_data";
		
		document.querySelector('#clear_filter_button').style.display = '';	
		//document.querySelector('#all_filkos_button').style.display = '';
	}
	else
	{
		console.log(fields);
		var data = "block=anketa&action=send2filkos&id=" + anketa_id;
		for (var key in fields)
		{
			data += "&" + key + "=" + fields[key];
		}
		console.log(data);
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Анкета отправлена');
				location.reload();
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
		});
	}
}

function getDaysLeft(oDeadLineDate, oToday){
  return oDeadLineDate > oToday ? Math.ceil((oDeadLineDate - oToday) / (1000 * 60 * 60 * 24)) : null;
}

/*window.onbeforeunload = function (evt) {
	
	if(document.querySelector("#save_data_button").style.display == '')
	{
		var message = "Анкета не сохранена!";
		if (typeof evt == "undefined") {
			evt = window.event;
		}
		if (evt) {
			evt.returnValue = message;
		}
		return message;
	}
}*/

document.onmouseout = function(e)
{
	if (showingTooltip)
	{
		if (showingTooltip.hasAttribute('id'))
		{
			if ((showingTooltip.getAttribute('id') != 'double_phone') && (showingTooltip.getAttribute('id') != 'double_fio') && (showingTooltip.getAttribute('id') != 'double_ip') && (showingTooltip.getAttribute('id') != 'get_promoney_res') && (showingTooltip.getAttribute('id') != 'send_prb_form') && (showingTooltip.getAttribute('id') != 'algo_wrap'))
			/*if (showingTooltip.getAttribute('id') == 'double_phone')
				setTimeout(function() { document.body.removeChild(showingTooltip); showingTooltip = null; }, 5000);
			else if (showingTooltip.getAttribute('id') == 'double_fio')
				setTimeout(function() { document.body.removeChild(showingTooltip); showingTooltip = null; }, 5000);
			else*/
			{
				document.body.removeChild(showingTooltip);
				showingTooltip = null;
			}
		}
		else
		{
			document.body.removeChild(showingTooltip);
			showingTooltip = null;
		}
	}
	

};


(function() {

//main();

function main() {
	if(document.readyState == 'complete') {
		var txt = document.querySelector('#txt');
		var rsl = document.querySelector('#rsl');

		txt.addEventListener('keyup', function(e) {
			if(txt.value.length>2) {
				ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=get_city_help&get='+txt.value, true, function(r) {
					rsl.style.display = 'block';
					rsl.innerHTML = r.responseText;
					rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
				}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
			} else {
				rsl.style.display = 'none';
			}
		}, false);
	} else {
		setTimeout(function() { main(); }, 1000);
	}
}

})();

function print_doc()
{
	window.print();
}

function setCookie2(name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie2(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}


/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////