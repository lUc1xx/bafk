var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

var old_input_value = new Array();
var sec_input_value = new Array();
var num_file = 0;

window.onload = update_all_data;
function update_all_data()
{
	get_tasks_count();
	get_events_count();
}

function format_numbers(inp)
{
	var num = inp.value.replace(/\D+/g,"");
	inp.value = num.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
}


function date_format(inp)
{
	var re = new RegExp("(\\d+)\.(\\d+)\.(\\d\\d+)", "");
	var mass = re.exec(inp.value);
	
	if (mass)
	{
		console.log(inp.value);
		if (parseInt(mass[1]) < 10)
			mass[1] = "0" + parseInt(mass[1]);
		
		if (parseInt(mass[2]) < 10)
			mass[2] = "0" + parseInt(mass[2]);
		
		
		/*if (parseInt(mass[3]) < 50)
			mass[3] = "20" + mass[3];
		else if (parseInt(mass[3]) < 100)
			mass[3] = "19" + mass[3];*/
		
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
		console.log(inp.value);
	}
	//console.log(mass);
		
	var d = new Date;
	var y = d.getFullYear();
	var num = inp.value.replace(/\D+/g,"");
	
	if (num.length < 2)
	{
		inp.value = (num > 31) ? 31 : num;
	}
	else if (num.length == 2)
	{
		inp.value = (num > 31) ? 31 : num + ".";
		//inp.value = inp.value + ".";
	}
	else if ((num.length > 2) && (num.length < 5))
	{
		var re = new RegExp("(\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		inp.value = mass[1] + "." + mass[2];
	}
	else if (num.length > 8) 
	{
		var re = new RegExp("(\\d\\d)(\\d\\d)(\\d\\d\\d\\d)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		mass[3] = (mass[3] > y) ? y : mass[3];
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
	}
	else 
	{
		var re = new RegExp("(\\d\\d)(\\d\\d)(\\d+)", "");
		var mass = re.exec(num);
		mass[1] = (mass[1] > 31) ? 31 : mass[1];
		mass[2] = (mass[2] > 12) ? 12 : mass[2];
		mass[3] = (mass[3] > y) ? y : mass[3];
		inp.value = mass[1] + "." + mass[2] + "." + mass[3];
	}
	//var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
	//var mass = reg.exec(tel);
}


function check_blocks()
{
	var divs = document.querySelectorAll(".block_wrap");
	
	var rows = divs[0].querySelectorAll(".row_a");
	var hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	
	if (hit)
		divs[1].style.display = "table";
	else
		divs[1].style.display = "none";
	
	console.log("Показывать 2 блок: " + hit);
	
	rows = divs[1].querySelectorAll(".row_a");
	hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	
	if (hit)
		divs[2].style.display = "table";
	else
		divs[2].style.display = "none";
	
	console.log("Показывать 3 блок: " + hit);
	
	rows = divs[2].querySelectorAll(".row_a");
	hit = true;
	for (var n=0; n < rows.length; n++)
	{
		var inp = rows[n].querySelector("input");
		if (inp)
		{
			if(inp.getAttribute('type') != 'checkbox')
			{
				if (inp.getAttribute('name') == 'field11')
					continue;
				else if (inp.value == "")
				{
					hit = false;
					break;
				}
			}
		}
	}
	
	if (hit)
		divs[3].style.display = "table";
	else
		divs[3].style.display = "none";
	
	console.log("Показывать 4 блок: " + hit);
}

function select_field(el)
{
	console.log(el.name + " : " + old_input_value[el.name] + " : " + el.checked);
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.checked)
			document.querySelector("#sp_" + el.name).innerHTML = '';
		else
		{
			//document.querySelector("#save_data_button").style.display = '';
			document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
		}
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = !el.checked;
		//document.querySelector("#save_data_button").style.display = '';
		document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
	}
}
function change_select(el)
{
	console.log(el.name + " : " + old_input_value[el.name] + " : " + el.options[el.selectedIndex].value);
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.options[el.selectedIndex].value)
			document.querySelector("#sp_" + el.name).innerHTML = '';
		else
		{
			//document.querySelector("#save_data_button").style.display = '';
			document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
		}
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = el.options[el.selectedIndex].value;
	}
}

function focus_m(el)
{
	el.size = 5;
}

function blur_m(el)
{
	el.size = 1;
}

function tel_format(inp)
{
	var num = inp.value.replace(/\D+/g,"");
	
	if (num.length < 2)
		inp.value = "7(";
	else if (num.length < 5)
	{
		var re = new RegExp("\\d(\\d\+)", "");
		var mass = re.exec(num);
		console.log(mass);
		inp.value = "7(" + mass[1];
	}
	else if (num.length < 8)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2];
	}
	else if (num.length < 10)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\\d\\d)(\\d\+)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2] + '-' + mass[3];
	}
	else// if (num.length < 12)
	{
		var re = new RegExp("\\d(\\d\\d\\d)(\\d\\d\\d)(\\d\\d)(\\d\\d\?)", "");
		var mass = re.exec(num);
		inp.value = "7(" + mass[1] + ') ' + mass[2] + '-' + mass[3] + '-' + mass[4];
	}
	//var reg = new RegExp("(\\d)(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)", "ig");
	//var mass = reg.exec(tel);
}

function show_field4edit(el)
{
	var name = el.id.replace(new RegExp("span_", ""), "");
	
	if (el.className == 'addr_span')
	{
		var inp = el.parentNode.querySelectorAll("input[name*=" + name + "]");
		console.log(inp);
		for (var i=0; i< inp.length; i++)
			inp[i].parentNode.style.display = "";
		inp[0].focus();
	}
	else
	{
	
		var inp = el.parentNode.querySelector("input[name=" + name + "]");
		
		if (inp == null)
			inp = el.parentNode.querySelector("select[name=" + name + "]");
		
		if (inp == null)
			inp = el.parentNode.querySelector("#inp_sp_" + name);
		
		inp.style.display = "";
		inp.focus();
	}
	el.style.display = "none";
}

function filter_mass(el)
{
	var v = new RegExp(el.value, "i");

	var divs = document.querySelectorAll(".block_wrap > .row_a");
	console.log(divs);
	for (var n=0; n < divs.length; n++)
	{
		if (v.test(divs[n].innerHTML))
		{
			divs[n].style.display = '';
		}
		else
			divs[n].style.display = 'none';
	}
	//console.log(divs);
}

function focus_field(el)
{
	//console.log(el.name);
	
	if (el.name in sec_input_value)
	{
		if (old_input_value[el.name] == el.value)
			document.querySelector("#sp_" + el.name).innerHTML = '';
	}
	else
	{
		sec_input_value[el.name] = true;
		old_input_value[el.name] = el.value;
	}
	
}

function blur_field(el)
{
	//console.log("out");
	if (old_input_value[el.name] != el.value)
	{
		//document.querySelector("#save_data_button").style.display = '';
		document.querySelector("#sp_" + el.name).innerHTML = '<img src="/img/emblem-important.png" height="13px" alt="Данные изменились" title="Данные изменились">';
	}
	else
	{
		document.querySelector("#sp_" + el.name).innerHTML = '';
	}
	
	//if (staff_office_type != 'main')
		//check_blocks();
}

function reg_help(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=reg_help&get='+txt.value + '&rname=' + el.name, true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

//import {Spinner} from 'spin.js';
function save_data()
{
	//import {Spinner} from 'spin.js';

	var opts = {
	  lines: 13, // The number of lines to draw
	  length: 38, // The length of each line
	  width: 17, // The line thickness
	  radius: 45, // The radius of the inner circle
	  scale: 1, // Scales overall size of the spinner
	  corners: 1, // Corner roundness (0..1)
	  color: '#000000', // CSS color or array of colors
	  fadeColor: 'transparent', // CSS color or array of colors
	  speed: 1, // Rounds per second
	  rotate: 0, // The rotation offset
	  animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  className: 'spinner', // The CSS class to assign to the spinner
	  top: '50%', // Top position relative to parent
	  left: '50%', // Left position relative to parent
	  shadow: '0 0 1px transparent', // Box-shadow for the lines
	  position: 'absolute' // Element positioning
	};

	//var target = document.getElementById('foo');
	//var spinner = new Spinner(opts).spin(target);
	
	

	console.log("test");
	var inputs = document.querySelectorAll("div[class=fval]>input");
	//console.log(inputs.length);
	//console.log(inputs);
	var data = getQueryString(inputs);
	/*var data = '';
	for (var n=0; n < inputs.length; n++)	
	{
		if (inputs.value)
			data += "&" + inputs[n].name + "=" + inputs.value;
	}*/
	
	
	var selects = document.querySelectorAll("div[class=fval]>select");
	//console.log(selects);
	for (var n=0; n < selects.length; n++)	
		data += "&" + selects[n].name + "=" + getSelection(selects[n]);
	
	inputs = document.querySelectorAll("div[class=fval] span input");
	console.log(inputs);
	
	data += "&" + getQueryString(inputs);
	//for (var n=0; n < inputs.length; n++)	
		//data += "&" + inputs[n].name + "=" + getSelection(selects[n]);
	
	//data += "&" + getQueryString(selects);
	//console.log(data);
	
	inputs = document.querySelectorAll("div[class=fval] div input");
	
	var addr_mass = new Array();
	for (var n=0; n < inputs.length; n++)
	{
		var name = inputs[n].name.split('_');
		if (name && name.length > 1)
		{
			name = name[0];
			if (!addr_mass[name])
				addr_mass[name] = new Array();
			addr_mass[name].push(inputs[n].value);
			
		}
		//data += "&" + selects[n].name + "=" + getSelection(selects[n]);
	}
	
	console.log(addr_mass);
	for (var key in addr_mass)
	{
		var vals = addr_mass[key];
		var datav = vals.join(';;;');
		data += "&" + key + "=" + datav;
	}
	
	data += "&block=anketa&action=create_form";
	if ((staff_office_type == 'main') || (staff_office_type == 'ozs') || (staff_position == 'director_tm'))
	{
		var el = document.querySelector('#cr_form_sel_staff');
		var sssel = el.options[el.selectedIndex].value;
		data += "&sssel=" + sssel;
	}
	else
	{
		var el = document.querySelector('#cr_form_sel_staff');
		if (el)
		{
			var sssel = el.options[el.selectedIndex].value;
			data += "&sssel=" + sssel;
		}
	}
	console.log(data);
	//return;
	
	var tel = document.querySelector('input[name="field29"]').value;
	var sum = document.querySelector('input[name="sum"]').value;
	var mainform = document.querySelector('input[name="mainform"]').value;
	var ch = document.querySelector('input[name="f_pod_ch"]').checked;
	
	var num = tel.replace(/\D+/g,"");
	if ((num.length != 11) &&(!ch))
		alert('Не заполнен телефон!');
	else if (sum == '')
		alert('Не заполнена сумма!');
	else if (ch && mainform == '')
	{
		alert('Вы пытаетесь создать поданкету, но не указали основную!');
	}
	else
	{
		var spinner = new Spinner(opts).spin();
		document.body.appendChild(spinner.el);
		
		document.querySelector('#b_create_f').disabled = true;
	
		ajaxQuery('/ajax.php','POST', data, true, function(req) 
		{
			var tmp_page = req.responseText;
			var Item = eval("obj = " + tmp_page);
			if (Item['status'] == 'ok')
			{
				alert('Данные успешно сохранены');
				location = "/details/" + Item['id'] + "/";
			}
			else 
			{
				//butt.style.backgroundColor = '#FFa0a0';
				alert(Item['msg']);
				document.querySelector('#b_create_f').disabled = false;
			}
		},
		function(req)
		{
			alert("Ошибка исполнения. Свяжитесь с администратором!");
			document.querySelector('#b_create_f').disabled = false;
		});
	}
}

function ch_name(sel)
{
	//console.log(sel.parentNode.parentNode.innerHTML);
	var name = sel.options[sel.options.selectedIndex].value;
	var inp = sel.parentNode.parentNode.querySelector('input');
	//console.log(inp.parentNode.innerHTML);
	//console.log(name);
	inp.setAttribute("name", name);
}

function add_field_value(sel)
{
	//console.log(sel.parentNode.parentNode.innerHTML);
	var name = sel.options[sel.options.selectedIndex].value;
	
	/*var inp = sel.parentNode.parentNode.querySelector('input');
	inp.setAttribute("name", name);*/
	
	var div_cell = sel.parentNode.parentNode.querySelector('.fval');
	
	var inp;
	var multi = false;
	switch (field_types[name])
	{
		case 'multiselect':
			multi = true;
		case 'select':
		{
			inp = document.createElement('select');
			//inp.type = "select";
			inp.id = "select_" + name;
			if (multi)
				inp.setAttribute("multiple", true);
			
				var opt = document.createElement('option');
				opt.innerHTML = 'Выберите';
				opt.value = '';
			inp.appendChild(opt);
			
			break;
		}
		case 'checkbox':
		{
			inp = document.createElement('input');
			inp.type = "checkbox";
			break;
		}
		default:
		{
			inp = document.createElement('input');
			inp.type = "text";
			inp.placeholder = "Поле для ввода";
			break;
		}
	}
	inp.setAttribute("name", name);
/*	var inp = document.createElement('input');
	inp.setAttribute("name", name);
				inp.type = "text";
				inp.placeholder = "Поле для ввода";
	*/			
	div_cell.appendChild(inp);
	
	if ((field_types[name] == 'select') || (field_types[name] == 'multiselect'))
		add_options(name);
}

function add_options(name)
{
	var sel = document.querySelector('#select_' + name);
	var data = "&block=anketa&action=getvalues&name=" + name;
	
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			if (Item['len'])
			{
				var data = Item['data'];
				for (var key in data)
				{
					var opt = document.createElement('option');
						opt.innerHTML = data[key];
						opt.value = data[key];
					sel.appendChild(opt);
				}
			}
		}
		else 
		{
			alert(Item['msg']);
		}
	},
	function(req)
	{
		alert("Ошибка исполнения. Свяжитесь с администратором!");
	});
}

function add_field()
{
	//console.log("rere");
	//console.log(mass_fields);
	var tab = document.querySelector('#anketa_data');
	var row = document.querySelector('#add_field_r');
	
	
	var sell = document.querySelectorAll('.sel_a');
	//console.log(sell.length);
	if (sell.length)
	{
		for (i = 0; i < sell.length; i++)
		{
			var sel_ind = sell[i].options.selectedIndex;
			var sel_v = sell[i].options[sel_ind].value;
			var sel_t = sell[i].options[sel_ind].text;
			//console.log(sel_ind + ' :: ' + sel_v + " :: " + sel_t);
			if (sel_v in mass_fields)
				delete mass_fields[sel_v];
			
		}
		//console.log(mass_fields);
	}
	
	var div_row = document.createElement('div');
		div_row.className = "row_a";
		
		var div_cell = document.createElement('div');
			div_cell.className = "fname";
			//div_cell.innerHTML = "test";
			
			var sel = document.createElement('select');
				sel.className = "sel_a";
				//sel.onchange = "javascript:ch_name(this)";
				//sel.setAttribute("onchange", "javascript:ch_name(this)");
				sel.setAttribute("onchange", "javascript:add_field_value(this)");
				var opt = document.createElement('option');
					opt.value = "-1";
					opt.innerHTML = "Выберите поле";
			
				sel.appendChild(opt);
				
				for (var key in mass_fields) 
				{
					//console.log( key + ": " + mass_fields[key] );
					opt = document.createElement('option');
					opt.name = key;
					opt.value = key;
					opt.innerHTML = mass_fields[key];
					sel.appendChild(opt);
				};
			div_cell.appendChild(sel);
			
			var inpp = document.createElement('input');
				inpp.className = "sel_i";
				inpp.setAttribute("placeholder", "Начните набирать тут для фильтра");
				inpp.setAttribute("oninput", "javascript:select_filter(this)");
				inpp.style = "width:98%";
			div_cell.appendChild(inpp);
			
		div_row.appendChild(div_cell);
		
		div_cell = document.createElement('div');
			div_cell.className = "fval";
			
			
			/*var inp = document.createElement('input');
				inp.type = "text";
				inp.placeholder = "Поле для ввода";
				
			div_cell.appendChild(inp);*/
		
		div_row.appendChild(div_cell);
	tab.insertBefore(div_row, row);
}

function select_filter(el)
{
	var sel = el.previousSibling;
	console.log(el.value);
//	console.log(Object.keys(sel.options));
	var reg = new RegExp(el.value, "i");
	
	//console.log(reg);
	var first = true;
	for (var n=0; n < sel.options.length; n++)
	{
		//console.log(sel.options[n].innerHTML);
		if(reg.test(sel.options[n].innerHTML))
		{
			//console.log('yes');
			//console.log(sel.options[n].innerHTML);
			sel.options[n].style.display = '';
			if (first)
			{
				sel.selectedIndex = n;
				first = false;
			}
		}			
		else
		{
			sel.options[n].style.display = 'none';
		}
			
	}
		
	/*for (var key in sel.options)
	{
		//if (sel.options[key].type == 'option')
		console.log(sel.options[key]);
		console.log(sel.options[key].type);
	}*/
}


function show_poda_field(el)
{
	var row = document.querySelector('#mainform_row');
	if (el.checked)
		row.style.display = 'table-row';
	else
		row.style.display = 'none';
}

function findforms(el)
{
	var txt = el;
	var rsl = el.parentNode.querySelector('ul[name=rsl]');
	if(txt.value.length>2)
	{
		ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=findforms&get='+txt.value + '&rname=' + el.name + "&id=0", true, function(r) {
				rsl.style.display = 'block';
				rsl.innerHTML = r.responseText;
				rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
			}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
	} 
	else 
		rsl.style.display = 'none';
}

function close_tooltip(el)
{
	document.body.removeChild(el.parentNode);
}

function show(state)
{
	document.getElementById('window').style.display = state;			
	document.getElementById('wrap').style.display = state;
}

function ajaxQuery2(url, method, param, async, onsuccess, onfailure) 
{
	var xmlHttpRequest = new XMLHttpRequest();
	var callback = function(r) 
	{ 
		r.status==200 ? (typeof(onsuccess)=='function' && onsuccess(r)) : (typeof(onfailure)=='function' && onfailure(r)); 
	};
	if(async) 
	{ 
		xmlHttpRequest.onreadystatechange = function() { if(xmlHttpRequest.readyState==4) { callback(xmlHttpRequest); } } 
	}
	xmlHttpRequest.open(method, url, async);
	xmlHttpRequest.send(param);
	if(!async) { callback(xmlHttpRequest); }
}

/*window.onbeforeunload = function (evt) {
	
	if(document.querySelector("#save_data_button").style.display == '')
	{
		var message = "Анкета не сохранена!";
		if (typeof evt == "undefined") {
			evt = window.event;
		}
		if (evt) {
			evt.returnValue = message;
		}
		return message;
	}
}*/

function getDaysLeft(oDeadLineDate, oToday){
  return oDeadLineDate > oToday ? Math.ceil((oDeadLineDate - oToday) / (1000 * 60 * 60 * 24)) : null;
}

var showingTooltip;

document.onmouseover = function(e)
{
	var target = e.target;

	var tooltip = target.getAttribute('data-tooltip');
	if (!tooltip) return;

	var tooltipElem = document.createElement('div');
	tooltipElem.className = 'tooltip';
	

	var tmp_str = '';
	switch(tooltip)
	{
		case 'algo_1':
		{
			tmp_str += 'Блок "Знакомство с клиентом"';
			tmp_str += '<ul>';
			tmp_str += '<li>Звоним клиенту "Знакомимся", выясняем у него имя и отчество, задавая вопрос: "Как я могу к вам обращаться? "  Если клиент просит сразу сделать ему предложение, а потом знакомство то говорим ему, что у меня определенный алгоритм работы и мне не нужна ваша фамилия и данные документов, а для предложения мне нужна эта минимальная информация. Начинаем заполнять первый блок и после этого при необходимости делаем ПРБ и предложение.</li>';
			tmp_str += '<li>Выясняем регион обращения (находим в списке существующий код ФИАС, в случае его отсутствия спрашиваем у клиента ближайший большой населенный пункт)</li>';
			tmp_str += '<li>Выясняем необходимую сумму кредита и для себя понимаем цель на что берутся денежные средства (от цели кредита зависит по какому пути идти дальше )</li>';
			tmp_str += '<li>Выясняем общую сумму дохода клиента (официальный и неофициальный доход и пишем общую совокупность дохода)</li>';
			tmp_str += '<li>Гражданство</li>';
			tmp_str += '<li>Наличие текущий просроченных платежей со слов клиента.</li>';
			tmp_str += '<li>Наличие заинтересованных лиц в получении кредита (в качестве заемщика, поручителя, залогодателя и тд) да/нет</li>';
			tmp_str += '</ul><br>';
			tmp_str += 'При наличии текущей просрочки и отсутствия заинтересованных лиц, клиенту высылается предложение по залоговому кредиту от инвестора. Шаблон "Предложение"';
			tmp_str += '<br><br><font color="red">На этом этапе заканчиваем работу с клиентом, сохраняем запись в анкете и если запрос клиента не решаем с учетом возможностей компании, отправляем заявку в статус "Заявка завершена"</font>';
			tmp_str += '<br>';
			tmp_str += '<br>Работа с недозвоном.';
			tmp_str += '<ul>';
			tmp_str += '<li>При первом недозвоне отправляем клиенту СМС "Одобрение" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторном недозвоне отправляем клиенту СМС, шаблон: "Истекает срок предварительного решения" и выставляем напоминание о звонке. (отправляем 1 раз)</li>';
			tmp_str += '<li>При повторных недозвонах отправляем анкету на удаление.</li>';
			tmp_str += '</ul>';
			tooltipElem.innerHTML = tmp_str;
			tooltipElem.setAttribute("style", "line-height:1.2em; font-size:110%;");
			tooltipElem.style.width = '1200px';
			break;
		}
		default:
			tooltipElem.innerHTML = tooltip;
			break;
	}
	//if (tooltip != 'algo_1')
		document.body.appendChild(tooltipElem);
	//else
		//target.appendChild(tooltipElem);

	var coords = target.getBoundingClientRect();

	var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
	if (left < 0) left = 0; // не вылезать за левую границу окна

	//var top = coords.top - tooltipElem.offsetHeight - 5;
	var top = coords.top + target.offsetHeight + 5;
	if (top < 0) { // не вылезать за верхнюю границу окна
	top = coords.top + target.offsetHeight + 5;
	}

	tooltipElem.style.left = left + 'px';
	tooltipElem.style.top = top + 'px';
	

	showingTooltip = tooltipElem;
};

document.onmouseout = function(e)
{
	if (showingTooltip)
	{
		if (showingTooltip.hasAttribute('id'))
		{
			if ((showingTooltip.getAttribute('id') != 'double_phone') && (showingTooltip.getAttribute('id') != 'double_fio') && (showingTooltip.getAttribute('id') != 'double_ip'))
			/*if (showingTooltip.getAttribute('id') == 'double_phone')
				setTimeout(function() { document.body.removeChild(showingTooltip); showingTooltip = null; }, 5000);
			else if (showingTooltip.getAttribute('id') == 'double_fio')
				setTimeout(function() { document.body.removeChild(showingTooltip); showingTooltip = null; }, 5000);
			else*/
			{
				document.body.removeChild(showingTooltip);
				showingTooltip = null;
			}
		}
		else
		{
			document.body.removeChild(showingTooltip);
			showingTooltip = null;
		}
	}
	

};

(function() {

//main();

function main() {
	if(document.readyState == 'complete') {
		var txt = document.querySelector('#txt');
		var rsl = document.querySelector('#rsl');

		txt.addEventListener('keyup', function(e) {
			if(txt.value.length>2) {
				ajaxQuery('/ajax.php', 'POST', 'block=anketa&action=get_city_help&get='+txt.value, true, function(r) {
					rsl.style.display = 'block';
					rsl.innerHTML = r.responseText;
					rsl.style.cssText = rsl.querySelectorAll('li').length>5 ? 'max-height: 100px; overflow-y: scroll; overflow-x: hidden; display: block;' : 'display: block;';
				}, function() { alert('\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a.'); });
			} else {
				rsl.style.display = 'none';
			}
		}, false);
	} else {
		setTimeout(function() { main(); }, 1000);
	}
}

})();


(function() {

cr_search_double();
var fname_mass = new Array();
	fname_mass['firstname']		= 'Имя';
	fname_mass['middlename']	= 'Отчество';
	fname_mass['lastname']		= 'Фамилия';
	fname_mass['birth_day']		= 'Дата рождения';
	fname_mass['field49']		= 'Название компании основного места трудоустройства';
	fname_mass['field29']		= 'Номер личного мобильного телефона';
	fname_mass['field60']		= 'Рабочий телефон';
var old_cr_value_mass = new Array();
	//old_cr_value_mass['firstname']	= '';
	//old_cr_value_mass['middlename']	= '';
	//old_cr_value_mass['lastname']	= '';
	//old_cr_value_mass['birth_day']	= '';
	//old_cr_value_mass['field49']	= '';
	//old_cr_value_mass['field29']	= '';
	//old_cr_value_mass['field60']	= '';

function cr_search_double() {
	if(document.readyState == 'complete') {
		//var iii = document.querySelector('#anketa_data ');
		//console.log(iii);
		
		var inputs = document.querySelectorAll('#anketa_data .fval > input');
		
		var hit = false;
		var hit_array = new Array();
		for (var i = 0; i < inputs.length; i++)
		{
			
			var name = inputs[i].name;
			var value = inputs[i].value;
			if (name == 'sum')
				continue;
			if (name == 'f_pod_ch')
				continue;
			if (name == 'mainform')
				continue;
			if (old_cr_value_mass[name] != null)
			{
				if (old_cr_value_mass[name] != value)
				{
					hit = true;
					hit_array[hit_array.length] = name;
				}
			}
			old_cr_value_mass[name] = value;
			
			//console.log(inputs[i].name + '=' + inputs[i].value);
		}
		
		console.log(hit);
		if (hit)
		{
			var cr_double_list_h = document.querySelector('#cr_double_list_h');
			cr_double_list_h.innerHTML = "Поиск анкет по параметрам:";
			var tmp_h = '';
			var search_str = '';
			for(var m in old_cr_value_mass)
			{
				var v = old_cr_value_mass[m];
				if (old_cr_value_mass[m] == "")
					continue;
				
				if (m == 'birth_day')
				{
					var reg = new RegExp("(\\d\\d).(\\d\\d).(\\d\\d\\d\\d)", "i");
					if (!reg.test(v))
						continue;
				}
				
				if ((m == 'lastname') || (m == 'firstname') || (m == 'middlename'))
				{
					if (v.length < 3)
						continue;
					
					if (v.toLowerCase() == 'нет')
						continue;
				}
				
				if ((m == 'field29') || (m == 'field60'))
				{
					var num = v.replace(/\D+/g,"");
					
					console.log(num.length);
					if (num.length != 11)
						continue;
				}
				
				tmp_h += "<br>" + fname_mass[m] + ": " +  old_cr_value_mass[m];
				search_str += "&" + m + "=" + v;
			}
			cr_double_list_h.innerHTML += tmp_h;
			console.log(search_str);
			
			var cr_double_list = document.querySelector('#cr_double_list');
			
			if (search_str == '')
			{
				cr_double_list.innerHTML = '';
			}
			else
			{
			
				cr_double_list.innerHTML = 'Загрузка...';
				
				var data = "&block=common&action=search_forms" + search_str;
		
				ajaxQuery('/ajax.php','POST', data, true, function(req) 
				{
					var tmp_page = req.responseText;
					var Item = eval("obj = " + tmp_page);
					if (Item['status'] == 'ok')
					{
						if (Item['len'])
						{
							
							cr_double_list.innerHTML = '';
							var data = Item['data'];
							for (var key in data)
							{
								
								var row = data[key];
								cr_double_list.innerHTML += 'Анкета <a href="/details/' + row['id'] + '/" target=_blank>№' + row['id'] + "</a> " + row['lastname'] + ' ' + row['firstname'] + ' ' + row['middlename'] + "<br>";
							}
							
							if (parseInt(Item['len']) > 24)
								cr_double_list.innerHTML += '<font color="red" style="font-style:italic;">Показаны только 25 последних совпадений...</font>'
						}
						else
							cr_double_list.innerHTML = 'Анкет не найдено';
					}
				});
			}
			
		}
			
		
	}
	
	setTimeout(function() { cr_search_double(); }, 3000);
}

})();




//gfdsgfdg
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgdf
//gfdgfdgfdffffffffffffffffffffff