var root = typeof unsafeWindow != 'undefined' ? unsafeWindow : window;

//window.onload = list_main;
window.onload = update_all_data;

var sel_status_i = 0;
var sel_office_i = 0;
var sel_staff_i = 0;

function update_all_data()
{
	list_main();
	get_tasks_count();
	get_events_count();
}

function filterApply()
{
	var dfrom = document.querySelector('#dfrom').value;
	var dto = document.querySelector('#dto').value;
	
	/*var s_status = document.querySelector('#select_status');
	sel_status_i = s_status.selectedIndex;
	s_status = s_status.options[s_status.selectedIndex].value;
	
	var s_office = document.querySelector('#select_office');
	sel_office_i = s_office.selectedIndex;
	s_office = s_office.options[s_office.selectedIndex].value;
	
	var s_staff = document.querySelector('#select_staff');
	sel_staff_i = s_staff.selectedIndex;
	s_staff = s_staff.options[s_staff.selectedIndex].value;
	
	console.log(s_status);
	console.log(s_office);
	console.log(s_staff);
	*/
	
	var filter = "&dfrom=" + dfrom + "&dto=" + dto;// + "&s_status=" + s_status + "&s_office=" + s_office + "&s_staff=" + s_staff;
	
	var t = document.querySelector('#list_table');
	var r1 = t.querySelectorAll('.row_odd');
	for (var key = 0; key < r1.length; key++)
		t.removeChild(r1[key]);
	var r2 = t.querySelectorAll('.row_even');
	for (var key = 0; key < r2.length; key++)
		t.removeChild(r2[key]);
	//var h = t.querySelector('.row_head');
	//t.innerHTML = "";
	//t.appendChild(h);

	setCookie('sotrlist_dfrom', dfrom, { expires: 36000000, path:"/"});
	//setCookie('sotrlist_dto', dto, { expires: 3600*8, path:"/"});
	//setCookie('sotrlist_s_status', s_status, { expires: 36000000, path:"/"});
	//setCookie('sotrlist_sel_office_i', sel_office_i, { expires: 36000000, path:"/"});
	//setCookie('sotrlist_s_office', s_office, { expires: 36000000, path:"/"});
	//setCookie('sotrlist_s_staff', s_staff, { expires: 36000000, path:"/"});
	get_list(filter);
}

function ch_list_stat(el)
{
	var sel = el.options[el.selectedIndex].value;
	var sel2 = document.querySelector('#select_staff');
	
	var opt = sel2.querySelectorAll('option');
	if (sel == '-1')
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].style.display = '';
			opt[key].selected = false;
		}
	}
	else
	{
		for (var key = 1; key < opt.length; key++)
		{
			opt[key].selected = false;
			if (opt[key].className == ('office_' + sel))
				opt[key].style.display = '';
			else
				opt[key].style.display = 'none';
		}
	}
	filterApply();
}

function list_main()
{
	sel_status_i = 1;
	var dto = document.querySelector('#dto');
	var dfrom = document.querySelector('#dfrom');
	//var select_status = document.querySelector('#select_status');
	//var select_status_val = select_status.options[select_status.selectedIndex].value;
	var select_status_val = 0;//select_status.options[select_status.selectedIndex].value;
	if (getCookie('sotrlist_dfrom'))
	{
		dfrom.value = getCookie('sotrlist_dfrom');
	}
	/*if (getCookie('sotrlist_dto'))
	{
		dto.value = getCookie('sotrlist_dto');
	}*/
	
	/*console.log(select_status_val);
	if (getCookie('sotrlist_s_status'))
	{
		select_status_val = getCookie('sotrlist_s_status');
		console.log(select_status_val);
		console.log(select_status);
		console.log(getCookie('sotrlist_sel_office_i'));
		select_status.options[getCookie('sotrlist_sel_office_i')].selected = true;
		console.log(select_status);
	}*/
	
	get_list("&dfrom=" + dfrom.value + "&dto=" + dto.value + '&s_status=' + select_status_val);
}


function get_list(filter)
{
	//tabl = document.querySelector('#res_table');
	//row = tabl.querySelectorAll('table');
	//console.log(row.length);

	var data = "block=listsotr&action=get_list" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var totalBytes  = req.getResponseHeader('Content-length');
		console.log(totalBytes);
		var lll = tmp_page.length;
		console.log(lll);
		//console.log(tmp_page);
		
		//document.querySelector('#list_table').innerHTML = "";
		
		var Item = eval("obj = " + tmp_page);
		if (Item["status"] == 'OK')
		{
			var data = Item["data"];
			add_data(data);
		}
		
		//document.querySelector('#main');
	});
}

function add_data(mass)
{
	var row_odd = "row_odd";	//нечет
	var row_even = "row_even";	//чет
	var c = 0;
	var tmp_add = "";
	for (var key in mass)
	{
		c++;
		//console.log(key + " ::: " + Item);
		
		var row_c = (c & 1) ? row_odd : row_even;
		tmp_add += '<div class="' + row_c + '">';
		
		var row = mass[key];
		
		tmp_add += '<div class="list_cell">' + row['date_add'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['id'] + '</div>';
		tmp_add += '<div class="list_cell"><a href="/details/' + row['id'] + '/">' + row['lastname'] + '</a></div>';
		
		//var tmp_sum = row['sum'];
		//tmp_sum = tmp_sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		
		//var ssum = tmp_sum + ((row['sum_val'] == "rub") ? " р." : "");
		//tmp_add += '<div class="list_cell" style="text-align:right; padding-right:10px;">' + ssum + '</div>';
				
		tmp_add += '<div class="list_cell">' + row['middlename'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['firstname'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['city'] + '</div>';
		tmp_add += '<div class="list_cell">' + row['source'] + '</div>';
		tmp_add += '<div class="list_cell">' + decodeStatus(row['status']) + '</div>';
		tmp_add += '<div class="list_cell" onclick="javascript:delSotrud(' + row['id'] + ', this);"><img width="20px" src="/img/del2.png"' +  '></div>';
		tmp_add += '<div class="list_cell">' + row['proposal_content'] + '</div>';
		
		
		tmp_add += '</div>';
		
	}
	
	document.querySelector('#list_table').innerHTML += tmp_add;
	
			
}

function delSotrud(id, el) {
	var data = "block=listsotr&action=delete&val=" + id;

	ajaxQuery('/ajax.php','POST', data, true, function(req)
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			el.parentNode.parentNode.removeChild(el.parentNode);
		}

	});
}

function decodeStatus(st) {
	switch (st) {
		case "new" : return "Новая";
		case "work_complete" : return "Закрыта";
		default: return "Неизвестный";
	}
}

function gsearch()
{
	var filter = document.querySelector('#search_input').value;
	
	if(filter.length < 3)
		return;
	
	var data = "block=listsotr&action=show_result&val=" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var t = document.querySelector('#list_table');
			var h = t.querySelector('.row_head');
			t.innerHTML = "";
			t.appendChild(h);
			add_data(Item['data']);
		}
		//console.log(Item);
	});
}

function calc_result(filter)
{
	if(filter.length < 3)
		return;
	var data = "block=listsotr&action=calc_result&val=" + filter;
	ajaxQuery('/ajax.php','POST', data, true, function(req) 
	{
		var tmp_page = req.responseText;
		var Item = eval("obj = " + tmp_page);
		if (Item['status'] == 'ok')
		{
			var sb = document.querySelector('#search_button');
			sb.innerHTML = "Поиск(" + Item['count'] + ")";
		}
		//console.log(Item);
	});
}

function fio_filter(filter)
{
	var t = document.querySelector('#list_table');
	var r = t.querySelectorAll('.row_odd, .row_even');
	//console.log(r.length);
	
	for (var key in r)
	{
		var d = r[key].querySelectorAll('.list_cell');
		
		var reg = new RegExp(filter, "ig");
		if (filter == '')
			r[key].style.display = "table-row";
		else if (reg.test(d[2].innerHTML))
			r[key].style.display = "table-row";
		else
			r[key].style.display = "none";
	}
}

//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds
//dsfdsfdsfds