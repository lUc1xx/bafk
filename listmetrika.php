<?php
if (!defined('SYSTEM_START_9876543210')) exit; 

if ($result = $db_connect->query("SHOW COLUMNS FROM forms WHERE Field = 'status';"))
{
	$row = $result->fetch_array(MYSQLI_ASSOC);
	preg_match_all("/\'(.*?)\'/i", $row["Type"], $out);
	$status_list = $out[1];
	$result->close();
}
else
{
	$res['status'] = 'failed';
	$res['msg'] = "0: Не удалось получить данные: (" . $db_connect->errno . ") " . $db_connect->error;
	print_r(json_encode($res,JSON_UNESCAPED_UNICODE));
	die();
}

//print_r($status_list);

echo '<div>Фильтры: с <input id="dfrom" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d", mktime(0, 0, 0, date('m')-6, date('d'), date('Y'))) . '"> по <input id="dto" size="11" type="text" class="datepicker-here" data-position="right top" value="' . Date("Y-m-d") . '">';

echo ' <button onclick="javascript:filterApply();">Применить фильтр</button>';
echo '</div>';
echo '<div id="list_table"><div class="row_head">';
echo '<div class="list_cell"><div onclick="javascript:sort_list(0);" class = "sort_d" style="cursor: pointer; border-bottom: 1px dashed #000080; width:50px; float:right; padding-right:15px;">Дата</div></div>';
echo '<div class="list_cell">№ анкеты</div>';
echo '<div class="list_cell"><input type="text" placeholder="ФИО" oninput="javascript:fio_filter(this.value)"></div>';
echo '<div class="list_cell"><div onclick="javascript:sort_list(3);" style="cursor: pointer; border-bottom: 1px dashed #000080; width:30px; float:left; padding-right:15px;">FICO</div></div>';
echo '<div class="list_cell">Сумма</div>';
echo '<div class="list_cell">Источник</div>';
//echo '<div class="list_cell">from description</div>';
echo '<div class="list_cell">';
echo '<select id="select_status" onchange="javascript:filterApply();">';
echo '<option value="-1">Все статусы</option>';
echo '<option value="in_work" selected>В работе</option>';
foreach ($status_list as $val)
{
	echo "<option value=\"$val\">$status_description[$val]</option>";
}
echo '</select></div>';
echo '<div class="list_cell">';
echo '<select id="select_office" onchange="javascript:ch_list_stat(this);">';
echo '<option value="-1">Все офисы</option>';
if ($result = $db_connect->query("SELECT id, name FROM offices WHERE type != 'main' AND deleted='0' ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$id = $row['id'];
		$name = $row['name'];
		echo "<option value=\"$id\">$name</option>";
	}
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
echo '</select></div>';
echo '<div class="list_cell">';
echo '<select id="select_staff" onchange="javascript:filterApply();">';
echo '<option value="-1">Все сотрудники</option>';
if ($result = $db_connect->query("SELECT id, lastname, firstname, patronymic, office FROM staff WHERE deleted = '0' AND position != 'admin' AND position != 'super-admin' ORDER BY id ASC;"))
{
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$id = $row['id'];
		$fio_a = $row['lastname'];
		if ($row['firstname'] != '')
			$fio_a .= ' ' . substr($row['firstname'], 0, 2) . '.';
		if ($row['patronymic'] != '')
			$fio_a .= ' ' . substr($row['patronymic'], 0, 2) . '.';
		$of = $row['office'];
		echo "<option class=\"office_$of\" value=\"$id\">$fio_a</option>";
	}
	$result->close();
}
else
{
	$res = "Не удалось создать таблицу: (" . $db_connect->errno . ") " . $db_connect->error;
	echo $res;
}
echo '</select></div>';
echo '<div class="list_cell"><span>Метрика</span></div>';
echo '</div></div>';
//echo '<div id="list_table"></div>';
?>

